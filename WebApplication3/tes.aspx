﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tes.aspx.cs" Inherits="WebApplication3.tes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="bootswatch/bootstrap.css" media="screen"/>
    <link rel="stylesheet" href="bootswatch/custom.min.css"/>
    
    <!--
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    -->

    <link rel="stylesheet" href="Script/jquery-ui.css" />
    <script src="Script/jquery-2.2.4.js"></script>
    <script src="Script/jquery-ui.js"></script>

    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script type="text/javascript">
        function closedialog(dialogname) {
            $("#" + dialogname).dialog("close");
        }

        function opendialog(dialogname) {
            $("#" + dialogname).dialog({
                title: "Summary",
                modal: true,
                resizable: false,
                width: 'auto',
                create: function (event, ui) { },
                buttons: {
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#" + dialogname).parent().appendTo($("#form1"));
        }

  </script>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div id="tes1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
            <asp:Panel runat="server">
                <asp:Button ID="Button2" runat="server" Text="Button" data-toggle="modal" data-target="#tes2" data-dismiss="modal"/>
            </asp:Panel>
          </div>
       
        </div>
      </div>
        
    </div>
    <div id="tes2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <asp:Panel ID="panel2" runat="server" Visible="false">
                <asp:Button ID="Button3" runat="server" Text="Button" data-dismiss="modal"/>
        </asp:Panel>
    </div>
        </div>
      </div>
        </div>
        <asp:Button ID="Button1" runat="server" Text="Button" data-toggle="modal" data-target="#tes1" />
    </form>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#tes1">Open Modal</button>
    <script src="Script/jquery-2.2.4.min.js"></script>
    <script src="bootswatch/bootstrap.min.js"></script>
    <script src="bootswatch/custom.js"></script>
</body>
</html>
