USE [master]
GO
/****** Object:  Database [PFNATT]    Script Date: 6/10/2016 5:47:41 PM ******/
CREATE DATABASE [PFNATT] ON  PRIMARY 
( NAME = N'PFNATT', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.PCDEBUG\MSSQL\DATA\PFNATT.mdf' , SIZE = 526336KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PFNATT_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.PCDEBUG\MSSQL\DATA\PFNATT.ldf' , SIZE = 757248KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PFNATT] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PFNATT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PFNATT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PFNATT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PFNATT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PFNATT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PFNATT] SET ARITHABORT OFF 
GO
ALTER DATABASE [PFNATT] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PFNATT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PFNATT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PFNATT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PFNATT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PFNATT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PFNATT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PFNATT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PFNATT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PFNATT] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PFNATT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PFNATT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PFNATT] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PFNATT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PFNATT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PFNATT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PFNATT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PFNATT] SET RECOVERY FULL 
GO
ALTER DATABASE [PFNATT] SET  MULTI_USER 
GO
ALTER DATABASE [PFNATT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PFNATT] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PFNATT', N'ON'
GO
USE [PFNATT]
GO
/****** Object:  Synonym [dbo].[PFNATT_EMAIL]    Script Date: 6/10/2016 5:47:41 PM ******/
CREATE SYNONYM [dbo].[PFNATT_EMAIL] FOR [tbl_email]
GO
/****** Object:  UserDefinedFunction [dbo].[Funct_FindDeptManager_ByEmp]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_FindDeptManager_ByEmp]
(@EMP_ID char(4))
RETURNS char(4)
WITH EXEC AS CALLER
AS
BEGIN
--Khusus buat Leave Application
DECLARE @MGR_ID AS CHAR(4)
DECLARE @SECT_ID AS CHAR(4)
SET @SECT_ID = (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)


-- edit by Indra, 22 Juli 2014
--<< Previous >>
/*SET @MGR_ID = (SELECT     dbo.TBL_SECTION.MGR_EMP_ID
		FROM         dbo.TBL_DEPT INNER JOIN
	                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
		WHERE     (dbo.TBL_SECTION.SECT_ID = 
		(SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
		))*/
--<< end of Prev >>
-- as HRD inform, for POD & DED div manager, 1st rater = TGM, 2nd rater = Pak Nurdin
IF exists (select '' from TBL_SECTION WHERE SECT_ID in ('E100','E200') AND MGR_EMP_ID=@EMP_ID)
begin
  --SET @MGR_ID = (SELECT MGR_EMP_ID FROM TBL_DEPT WHERE DEPT_ID='E400')
  SET @MGR_ID = (SELECT MGR_EMP_ID FROM TBL_DEPT WHERE DEPT_ID=@SECT_ID)
end
ELSE
BEGIN
    SET @MGR_ID = (SELECT     dbo.TBL_SECTION.MGR_EMP_ID
		FROM         dbo.TBL_DEPT INNER JOIN
	                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
		WHERE     (dbo.TBL_SECTION.SECT_ID = 
		(SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
		))
END

RETURN (@MGR_ID)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_FindDivManager_ByEmp]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_FindDivManager_ByEmp]
(@EMP_ID char(4))
RETURNS char(4)
WITH EXEC AS CALLER
AS
BEGIN
--RETURN
--(
--SELECT     dbo.TBL_DEPT.MGR_EMP_ID
--FROM         dbo.TBL_DEPT INNER JOIN
--                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
--WHERE     (dbo.TBL_SECTION.SECT_ID = 
--		(SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
--)
--)

DECLARE @SECT_ID AS CHAR(4)
SET @SECT_ID = (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
--N300 dan E220
DECLARE @MGR_ID AS CHAR(4)

/* EDIT BY INDRA, 18 Oct 2013
IF @SECT_ID <> 'N300' and @SECT_ID <> 'N220' --and @SECT_ID <> 'E300'
SET @MGR_ID = (SELECT     dbo.TBL_DEPT.MGR_EMP_ID
			FROM         dbo.TBL_DEPT INNER JOIN
	                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
		WHERE     (dbo.TBL_SECTION.SECT_ID = 
		(SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
)
)

-- IF QHSE SET TO PAK HUSIN
IF (@SECT_ID like 'N300%'  or @SECT_ID like 'N220%') Set @MGR_ID = '9022'  -- JIKA QHSE PAK HUSIN
--ELSE SET @MGR_ID = '9030'
--IF @EMP_ID = '0658' SET @MGR_ID = '9030' */

IF @SECT_ID = 'E300' OR @SECT_ID = 'N220' OR @SECT_ID = 'E100' OR @SECT_ID = 'E200' OR @SECT_ID = 'N100' 
BEGIN
  SET @MGR_ID = (SELECT     dbo.TBL_DEPT.MGR_EMP_ID
		               FROM         dbo.TBL_DEPT INNER JOIN
	                 dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
		               WHERE     (dbo.TBL_SECTION.SECT_ID = 
		                  (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)))
                      
  if @EMP_ID = @MGR_ID  SET @MGR_ID = (SELECT DPT_MGR_EMP_ID FROM TBL_DEPT WHERE DEPT_ID=@SECT_ID)
END
ELSE
BEGIN
  IF EXISTS (SELECT '' FROM TBL_DEPT WHERE DEPT_ID=@SECT_ID AND MGR_EMP_ID=@EMP_ID)
  BEGIN
    SET @MGR_ID = (SELECT DPT_MGR_EMP_ID FROM TBL_DEPT WHERE DEPT_ID=@SECT_ID AND MGR_EMP_ID=@EMP_ID)
  END
  ELSE
  BEGIN
    SET @MGR_ID = (SELECT     dbo.TBL_DEPT.MGR_EMP_ID
		               FROM         dbo.TBL_DEPT INNER JOIN
	                 dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
		               WHERE     (dbo.TBL_SECTION.SECT_ID = 
		                  (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)))
    
  END
END

RETURN(@MGR_ID)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_GetEmpDeptCode]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_GetEmpDeptCode]
(@EMP_ID char(4))
RETURNS varchar(50)
WITH EXECUTE AS CALLER
AS
BEGIN
RETURN
(SELECT SECT_ID FROM TBL_SECTION WHERE SECT_ID = (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID= @emp_Id)
)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_GetEmpPensionDate]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_GetEmpPensionDate]
(@EMP_ID varchar(6))
RETURNS datetime
WITH EXEC AS CALLER
AS
BEGIN
RETURN
(
--SELECT DATEADD(year,55,TBL_EMP.BIRTHDATE) AS PENSION FROM TBL_EMP WHERE EMP_ID= @emp_Id
-- refer to IOM 020/HRD/IOM/2015, retirement age changes to be 58
SELECT DATEADD(year,58,TBL_EMP.BIRTHDATE) AS PENSION FROM TBL_EMP WHERE EMP_ID= @emp_Id
)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_GetMaxProjectRevision]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_GetMaxProjectRevision]
(@Jobcode_id varchar(20), @ClientJobcode varchar(50), @Version int)
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN

Declare @MaxRevision as int

select @MaxRevision = max(revision) from tbl_project where jobcode_id = @Jobcode_id and client_jobcode=@ClientJobcode and version = @Version

RETURN (@MaxRevision)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_GetMaxProjectVersion]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_GetMaxProjectVersion]
(@Jobcode_id varchar(20), @ClientJobcode varchar(50))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN

Declare @MaxVersion as int

select @MaxVersion = max(version) from tbl_project where jobcode_id = @Jobcode_id and client_jobcode=@ClientJobcode

RETURN (@MaxVersion)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_IsAL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE  FUNCTION [dbo].[Funct_IsAL]
(@Str as Char(10))
RETURNS char(2) AS  
BEGIN 
Declare @Str1 as char(2)
If LTRIM(RTRIM(ISNULL(@Str,''))) = 'AL'
	set @str1 =  'AL'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'SI'
	set @str1 =  'SI'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'SL'
	set @str1 =  'SL'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'OL'
	set @str1 =  'OL'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'UL'
	set @str1 =  'UL'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'AB'
	set @str1 =  'AB'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'HL'
	set @str1 =  'HL'
else If LTRIM(RTRIM(ISNULL(@Str,''))) = 'AD'
	set @str1 =  'AD'

--ELSE
--	set @str1 =  ''


RETURN @STR1
END






GO
/****** Object:  UserDefinedFunction [dbo].[FUNCT_ISHOLIDAY1]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[FUNCT_ISHOLIDAY1]
(
@i as int,
@month as int,
@Year as int,
@Loc_Id as char(2)

)

RETURNS int AS  
BEGIN 


DECLARE @Date1 as int
DECLARE @Date2 as int
DECLARE @Harinya as int
DECLARE @CDate1 as varchar(10)
DECLARE @CDate2 as varchar(10)
DEclare @CMonth as varchar(2)
Declare @CYear as varchar(2)
Declare @Tanggal as varchar(10)
DEclare @Tanggal1 as datetime
DEclare @Tanggal2 as datetime
DEclare @Dayname as char(3)
Declare @VarTanggal as datetime
DEclare @Hol as int
Declare @Ci as varchar(2)

DECLARE @DateNya as int
DECLARE @Libur as int
DECLARE @Weekend as int
DECLARE @JumlahHol as int
DECLARE @JumlahHol2 as int
DECLARE @Type as char(2)

SET @TYPE = (SELECT DISTINCT WORKTYPE FROM TBL_WORK_HOUR WHERE LOC_ID = @LOC_ID)

SET @Ci = Cast(@i as varchar(2))
SET @CMonth = Cast(@Month as varchar(2))
SET @CYear = Cast(@Year as varchar(4))


SET @TANGGAL = (
		RTRIM(LTRIM(@CMonth)) + '/' + 
		RTRIM(LTRIM(@Ci)) + '/' + 
		RTRIM(LTRIM(@Year)))

--select @Tanggal1

--select Left(datename(weekday, @Tanggal1 ), 3)

if Isdate (@Tanggal) = 1
	Begin
		SET @Tanggal1 = Cast(@Tanggal as datetime)

		--TUBAN & TOC
--		IF @LOC_ID = '26' or @LOC_ID = '23' 
		IF @TYPE = 2
		BEGIN
			if Left(datename(weekday, @Tanggal1 ), 3) = 'SUN'
			Begin
				SET @Hol = 1

			End
			else
			begin
				set @Hol = (select isnull(count(HOL_Date),0) from TBL_HOL_TR 
					where Hol_date = @Tanggal1
					And Loc_Id = @Loc_Id)
			end
		END
--		IF @LOC_ID = '00' or @LOC_ID = '24' or @LOC_ID = ''
		IF @TYPE = 1
		BEGIN
			if Left(datename(weekday, @Tanggal1 ), 3) = 'SAT' or Left(datename(weekday, @Tanggal1 ), 3) = 'SUN'
			Begin
				SET @Hol = 1

			End
			else
			begin
				set @Hol = (select isnull(count(HOL_Date),0) from TBL_HOL_TR 
					where Hol_date = @Tanggal1
					And Loc_Id = @Loc_Id)
			end
		END

		
			



	End

--select @JumlahHol, @JumlahHol2, @JumlahHol + @JumlahHol2
--return (isnull(@JumlahHol,0) + isnull(@JumlahHol2,0))
Return Isnull(@Hol,0)


END





















GO
/****** Object:  UserDefinedFunction [dbo].[Funct_LeaveDiffDays]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[Funct_LeaveDiffDays]
(
@STARTDAY AS DATETIME,
@ENDDAY AS DATETIME,
@EMP_ID AS CHAR(4)
)

RETURNS INT AS  
BEGIN 
DECLARE @TR_DATE AS DATETIME
DECLARE @LOC_ID AS CHAR(2)
DECLARE @WORKTYPE AS INT
DECLARE @DIFFDAY AS INT
DECLARE @INSERT_OK AS INT
DECLARE @POS_ID AS CHAR(3)

SET @LOC_ID = (SELECT ISNULL(LOC_ID,'00') AS LOC_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
--SET @WORKTYPE = 2
SET @WORKTYPE = (SELECT DISTINCT WORKTYPE FROM TBL_WORK_HOUR WHERE LOC_ID = @LOC_ID)
SET @TR_DATE = @STARTDAY
SET @DIFFDAY = 0
SET @INSERT_OK = 0

SET @POS_ID = (SELECT ISNULL(POSITION_ID,'') AS LOC_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)

WHILE @TR_DATE <= @ENDDAY
BEGIN
	IF @WORKTYPE = 1
	BEGIN
		IF @POS_ID = 'OSS' 
			SET @DIFFDAY = @DIFFDAY + 1
		ELSE
			IF DATEPART(WEEKDAY,@TR_DATE) not in (1,7) AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID = @LOC_ID)  SET @DIFFDAY = @DIFFDAY + 1

	END
	IF @WORKTYPE = 2
	BEGIN
		IF @POS_ID = 'OSS' 
			SET @DIFFDAY = @DIFFDAY + 1
		ELSE
			IF DATEPART(WEEKDAY,@TR_DATE) not in (1) AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID = @LOC_ID)  SET @DIFFDAY = @DIFFDAY + 1
	END
	SET @TR_DATE = DATEADD(DAY, 1, @TR_DATE)	
END

RETURN (ISNULL(@DIFFDAY,0))

END





GO
/****** Object:  UserDefinedFunction [dbo].[Funct_LeaveQuota]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--print dbo.Funct_LeaveQuota ('0527',2009,'AL','12/31/2009')
CREATE FUNCTION [dbo].[Funct_LeaveQuota]
(@Emp_Id char(4), @YearTr int, @Type char(2), @Date datetime)
RETURNS int
WITH EXEC AS CALLER
AS
BEGIN
--SET @EMP_ID = '0859'
--SET @YEARTR = 2009
--SET @TYPE = 'OL'
--SET @DATE = '2009-9-18'
Declare @LastYearRemain as int
Declare @ComingOL as int
Declare @ComingOLNotTaken as int
Declare @ComingOLTaken AS int
Declare @ComingOLTaken_Case2 AS int
Declare @LeaveTaken as int
Declare @Result as int
Declare @OTWRequest as int
Declare @OTWRequest_AD as int
Declare @OTWRequest_HL as int
Declare @AL as int
Declare @AD as int
Declare @HL as int
Declare @END_OL_DATE AS DATETIME
--Tambahan Perhitungan yang belum 1 tahun belum bisa ngambil 
--Tambahan per tanggal 2 feb 09
-- Tambahan untuk menghitung leave request yang belum di approve -- Ary
DECLARE @LEAVERESERVED AS INT
SET @LEAVERESERVED = 0
SELECT @LEAVERESERVED = SUM(TOTAL_DAY) FROM TBL_LEAVE_REQUEST WHERE STATUS IN (1,2) AND ISNULL(PROCESS,0) = 0 AND EMP_ID = @EMP_ID AND CLASS_TYPE = 'AL'
----------------------------------- 
-- Tambahan untuk menghitung leave request yang belum EXPIRED -- Ary
DECLARE @AL_EXP AS INT
DECLARE @HL_EXP AS INT
----------------------
--SELECT @END_OL_DATE = MAX(END_DATE) FROM TBL_LEAVE_REQUEST WHERE EMP_ID = @EMP_ID AND CLASS_TYPE = 'OL' AND YEAR(END_DATE) = @YEARTR
				--Tambahan kalo OL ada yang gag diambil, 4 Nov 2009
				--AND END_DATE >= @DATE
				--End Tambahan kalo OL ada yang gag diambil, 4 Nov 2009
--IF @END_OL_DATE IS NULL SET @END_OL_DATE = @Date
SELECT @END_OL_DATE = MAX(END_DATE) FROM TBL_LEAVE_REQUEST A WHERE EMP_ID = @EMP_ID AND CLASS_TYPE IN ('AL','OL') AND YEAR(END_DATE) = @YEARTR
AND END_DATE < @DATE
AND EXISTS (SELECT '' FROM TBL_OL B WHERE B.hol_date BETWEEN A.START_DATE AND A.START_DATE)
IF @END_OL_DATE IS NULL SET @END_OL_DATE = GETDATE()
SET @AL = (SELECT  Isnull(AL,0) FROM         TBL_LEAVE_ALLOCATION 
			Where EMp_ID = @Emp_Id and YearTr = @YearTR)
Set @LastYearRemain = (SELECT  Isnull(LastYearRemain,0) FROM         TBL_LEAVE_ALLOCATION 
			Where EMp_ID = @Emp_Id and YearTr = @YearTR)
			
-- edit by indra, 20/11/2012, antisipasi emp yg entry leave request backward & ada upcomingOL tapi emp masuk kerja			
--Set @ComingOL = (select count(*) From TBL_OL Where Hol_Date >= @END_OL_DATE and Year(Hol_Date) = @YearTr)
Set @ComingOL = (select ISNULL(count(*),0) From TBL_OL Where Hol_Date > @END_OL_DATE and Year(Hol_Date) = @YearTr)
set @ComingOLNotTaken = (select ISNULL(count(*),0) from tbl_card_tr a where emp_id=@Emp_Id and tr_date >= @END_OL_DATE and year(tr_date)=@YearTr AND CARD_IN IS NOT NULL and exists (select '' from tbl_ol b where a.TR_DATE=b.hol_date))
Set @ComingOL = @ComingOL - @ComingOLNotTaken
--end of edit 20/11/2012
-- additional check, for upcoming OL taken, 11 dec 2014
Set @ComingOLTaken = (select  ISNULL(count(*),0) from dbo.tbl_OL a where year(hol_date)=@yeartr AND hol_date > @END_OL_DATE
                                and exists (select '' from TBL_LEAVE_REQUEST b
                                where b.emp_id=@Emp_Id and a.hol_date between b.start_date and b.end_date
                                and b.class_type IN ('AL','OL') and b.status < 100))
                                
Set @ComingOL = @ComingOL - @ComingOLTaken

Set @ComingOLTaken_Case2 = (select  ISNULL(count(*),0) from dbo.tbl_OL a where year(hol_date)=@yeartr AND hol_date > @END_OL_DATE
                                and exists (select '' from TBL_CARD_TR b
                                where b.emp_id=@Emp_Id and a.hol_date = b.TR_DATE and b.ATT_TYPE_ID in ('HL','AD')))
                                
Set @ComingOL = @ComingOL - @ComingOLTaken_Case2

-- end of additional check, for upcoming OL taken, 11 dec 2014
Set @LeaveTaken = (select ISNULL(dbo.Funct_SumLeave3(@Emp_Id, @YearTr, 'AL',@date),0) as LastYearRemain)--+ isnull(@LEAVERESERVED,0) --tambahan leave reserve
Set @OTWRequest = (select ISNULL(SUM(total_DAY),0) From tbl_Leave_Request Where emp_id = @EMP_ID AND year(start_date) = @YEARTR and End_date < @Date and (Class_Type = 'AL' or Class_Type = 'OL')  AND ISNULL(PROCESS,0) = 0 And Status <> 4 AND STATUS <= 99)
Set @OTWRequest_AD = (select ISNULL(SUM(total_DAY),0) From tbl_Leave_Request Where emp_id = @EMP_ID AND year(start_date) = @YEARTR and End_date < @Date and Class_Type = 'AD' AND ISNULL(PROCESS,0) = 0 And Status <> 4 AND STATUS <= 99)
Set @OTWRequest_HL = (select ISNULL(SUM(total_DAY),0) From tbl_Leave_Request Where emp_id = @EMP_ID AND year(start_date) = @YEARTR and End_date < @Date and Class_Type = 'HL' AND ISNULL(PROCESS,0) = 0 And Status <> 4 AND STATUS <= 99)
SET @AL_EXP = (SELECT  Isnull(EXP_AL,0) FROM         TBL_LEAVE_ALLOCATION 
			Where EMp_ID = @Emp_Id and YearTr = @YearTR)
SET @HL_EXP = (SELECT  Isnull(EXP_HL,0) FROM         TBL_LEAVE_ALLOCATION 
  		Where EMp_ID = @Emp_Id and YearTr = @YearTR)
--SELECT Isnull(@LastYearRemain,0), Isnull(@LEaveTaken,0), Isnull(@ComingOL,0), Isnull(@OTWRequest,0) 
--- ADDED BY ARY
IF @TYPE = 'AD'
BEGIN
	SET @AD = (SELECT  Isnull(AD,0) FROM         TBL_LEAVE_ALLOCATION 
			Where EMp_ID = @Emp_Id and YearTr = @YearTR)
	SET @LeaveTaken = (select dbo.Funct_SumLeave3(@Emp_Id, @YearTr, 'AD',@date) as LastYearRemain)
	SET @RESULT = @AD - @LEAVETAKEN - @OTWRequest_AD
END
--- ADDED BY ARY
ELSE IF @TYPE = 'AL'
BEGIN
	IF @YEARTR = 2008 
		Set @Result = ((Isnull(@LastYearRemain,0)+ ISNULL(@AL,0)) - (Isnull(@LEaveTaken,0) + Isnull(@ComingOL,0)  + Isnull(@OTWRequest,0) ))
	ELSE
		Set @Result = (Isnull(@LastYearRemain,0) - (Isnull(@LEaveTaken,0) + Isnull(@ComingOL,0) + Isnull(@OTWRequest,0)+ISNULL(@AL_EXP,0) ))
END
ELSE IF @TYPE = 'OL'
BEGIN
	Set @Result = (Isnull(@LastYearRemain,0) - (Isnull(@LEaveTaken,0) + Isnull(@OTWRequest,0)+ISNULL(@AL_EXP,0) ))
END
ELSE IF @TYPE = 'HL'
BEGIN
  SET @HL = (SELECT  Isnull(HL,0) FROM         TBL_LEAVE_ALLOCATION 
			Where EMp_ID = @Emp_Id and YearTr = @YearTR)
	SET @LeaveTaken = (select dbo.Funct_SumLeave3(@Emp_Id, @YearTr, 'HL',@date) as LastYearRemain)
	SET @RESULT = Isnull(@HL,0) - (Isnull(@LEAVETAKEN,0) + Isnull(@OTWRequest_HL,0) + Isnull(@HL_EXP,0))
END
--SELECT @ComingOL,@Result
RETURN (@RESULT)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_LeaveQuota_1]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--print dbo.Funct_LeaveQuota ('0527',2009,'AL','12/31/2009')
CREATE FUNCTION [dbo].[Funct_LeaveQuota_1]
(@Emp_Id char(4), @YearTr int, @Type char(2), @Date datetime)
RETURNS int
WITH EXEC AS CALLER
AS
BEGIN
	Declare @LastYearRemain as int
	Declare @ComingOL as int
	Declare @ComingOLNotTaken as int
	Declare @LeaveTaken as int
	Declare @Result as int
	Declare @OTWRequest as int
	Declare @AL as int
	Declare @AD as int
	Declare @END_OL_DATE AS DATETIME
	DECLARE @LEAVERESERVED AS INT
	DECLARE @AL_EXP AS INT

	SET @LEAVERESERVED = 0
	SELECT	@LEAVERESERVED = SUM(TOTAL_DAY) 
	FROM	TBL_LEAVE_REQUEST 
	WHERE	STATUS IN (1,2) AND ISNULL(PROCESS,0) = 0 AND EMP_ID = @EMP_ID AND CLASS_TYPE = 'AL'

	SELECT	@END_OL_DATE = MAX(END_DATE) 
	FROM	TBL_LEAVE_REQUEST A 
	WHERE	EMP_ID = @EMP_ID AND CLASS_TYPE IN ('AL','OL') AND YEAR(END_DATE) = @YEARTR AND END_DATE < @DATE
			AND EXISTS (SELECT '' FROM TBL_OL B WHERE B.hol_date BETWEEN A.START_DATE AND A.START_DATE)
	IF @END_OL_DATE IS NULL 
		SET @END_OL_DATE = GETDATE()
		
	SELECT @AL = Isnull(AL, 0), @LastYearRemain = Isnull(LastYearRemain, 0), @AL_EXP = Isnull(EXP_AL, 0)
	FROM TBL_LEAVE_ALLOCATION 
	Where	EMp_ID = @Emp_Id and YearTr = @YearTR
	
	--SET @AL = (SELECT  Isnull(AL,0) FROM TBL_LEAVE_ALLOCATION Where	EMp_ID = @Emp_Id and YearTr = @YearTR)
	--Set @LastYearRemain = (SELECT  Isnull(LastYearRemain,0) FROM TBL_LEAVE_ALLOCATION Where	EMp_ID = @Emp_Id and YearTr = @YearTR)

	Set @ComingOL = (select ISNULL(count(1),0) From TBL_OL Where Hol_Date > @END_OL_DATE and Year(Hol_Date) = @YearTr)
	set @ComingOLNotTaken = (select ISNULL(count(1),0) from tbl_card_tr a where emp_id=@Emp_Id and tr_date >= @END_OL_DATE and year(tr_date)=@YearTr AND CARD_IN IS NOT NULL and exists (select '' from tbl_ol b where a.TR_DATE=b.hol_date))
	Set @ComingOL = @ComingOL - @ComingOLNotTaken
	Set @LeaveTaken = 0 --(select ISNULL(dbo.Funct_SumLeave3(@Emp_Id, @YearTr, 'AL',@date),0) as LastYearRemain)--+ isnull(@LEAVERESERVED,0) --tambahan leave reserve
	Set @OTWRequest = (select ISNULL(SUM(total_DAY),0) From tbl_Leave_Request Where emp_id = @EMP_ID  AND End_date < @Date and (Class_Type = 'AL' or Class_Type = 'OL')  AND ISNULL(PROCESS,0) = 0 And Status <> 4 AND STATUS <= 99)
	--SET @AL_EXP = (SELECT  Isnull(EXP_AL,0) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)

	IF @TYPE = 'AD'
	BEGIN
		SET @AD = (SELECT  Isnull(AD,0) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)
		SET @LeaveTaken = (select dbo.Funct_SumLeave3(@Emp_Id, @YearTr, 'AD',@date) as LastYearRemain)
		SET @RESULT = @AD - @LEAVETAKEN
	END
	ELSE IF @TYPE = 'AL'
	BEGIN
		IF @YEARTR = 2008 
			Set @Result = ((Isnull(@LastYearRemain,0)+ ISNULL(@AL,0)) - (Isnull(@LEaveTaken,0) + Isnull(@ComingOL,0)  + Isnull(@OTWRequest,0) ))
		ELSE
			Set @Result = (Isnull(@LastYearRemain,0) - (Isnull(@LEaveTaken,0) + Isnull(@ComingOL,0) + Isnull(@OTWRequest,0)+ISNULL(@AL_EXP,0) ))
	END
	ELSE IF @TYPE = 'OL'
	BEGIN
		Set @Result = (Isnull(@LastYearRemain,0) - (Isnull(@LEaveTaken,0) + Isnull(@OTWRequest,0)+ISNULL(@AL_EXP,0) ))
	END
	
	RETURN (@RESULT)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_LeaveQuota_ExpAL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_LeaveQuota_ExpAL]
(@Emp_Id varchar(6), @Start_Date datetime, @End_Date datetime, @Total_Day int, @CLASS_TYPE char(2))
RETURNS int
WITH EXEC AS CALLER
AS
BEGIN
DECLARE @LastYearRemain as int
Declare @Exp_AL as int
Declare @ExpiredAL as datetime
Declare @LeaveTaken_Before_ExpAL as int
Declare @Result as int

	set @LastYearRemain = (select isnull(LastYearRemain,0)-isnull(AL,0) as LastYearRemain from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	set @ExpiredAL = (select isnull(ExpiredAL,'1/1/1900') as ExpiredAL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	set @Exp_AL = (select isnull(Exp_AL,0) as Exp_AL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	--set @LeaveTaken_Before_ExpAL = (select dbo.Funct_SumLeave3(@Emp_Id, year(@Start_Date), 'AL', @ExpiredAL) as LeaveTaken_Before_ExpAL)
	--set @LeaveTaken_Before_ExpAL = (SELECT count(*) as LeaveTakenBfrExpAL FROM TBL_CARD_TR WHERE EMP_ID=@Emp_id AND TR_DATE <= @ExpiredAL and att_type_id in ('AL','OL'))
	
	if (@CLASS_TYPE='AL' or @CLASS_TYPE='OL') and (@ExpiredAL is not null) and (isnull(@ExpiredAL,'1/1/1900') <> '1/1/1900')
	begin
	  if (@END_DATE <= @ExpiredAL)
				 set  @RESULT = dbo.Funct_LeaveDiffDays(@START_DATE,@END_DATE,@Emp_ID)
		else
				 set  @RESULT = dbo.Funct_LeaveDiffDays(@START_DATE,@ExpiredAL,@Emp_ID)
	end
RETURN (@RESULT)
END

GO
/****** Object:  UserDefinedFunction [dbo].[Funct_LeaveQuota_ExpHL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_LeaveQuota_ExpHL]
(@Emp_Id varchar(6), @Start_Date datetime, @End_Date datetime, @Total_Day int, @CLASS_TYPE char(2))
RETURNS int
WITH EXEC AS CALLER
AS
BEGIN
Declare @Exp_HL as int
Declare @ExpiredHL as datetime
Declare @Result as int

  SET @RESULT = 0
	set @ExpiredHL = (select isnull(ExpiredHL,'1/1/1900') as ExpiredHL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	set @Exp_HL = (select isnull(Exp_HL,0) as Exp_HL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	
	--if (@CLASS_TYPE='HL') and (@ExpiredHL is not null)
  if (@CLASS_TYPE='HL') and (isnull(@ExpiredHL,'1/1/1900') <> '1/1/1900')
	begin
	  if (@END_DATE <= @ExpiredHL)
				 set  @RESULT = dbo.Funct_LeaveDiffDays(@START_DATE,@END_DATE,@Emp_ID)
		else
				 set  @RESULT = dbo.Funct_LeaveDiffDays(@START_DATE,@ExpiredHL,@Emp_ID)
	end
RETURN (@RESULT)
END

GO
/****** Object:  UserDefinedFunction [dbo].[FUNCT_NAME]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[FUNCT_NAME]
(
@EMP_ID AS CHAR(4)
)
RETURNS VARCHAR(50)
AS  
BEGIN 
RETURN
--	(SELECT     ISNULL(FIRSTNAME,'') + ' ' + ISNULL(MIDDLENAME,'') +' ' + ISNULL(LASTNAME,'') AS EMP_NAME
--	FROM         dbo.TBL_EMP
--	WHERE EMP_ID = @EMP_ID)

(select emp_name = case
		when len(ltrim(rtrim(ISNULL(middlename,'')))) = 0 and len(ltrim(rtrim(ISNULL(lastname,'')))) <> 0 then isnull(ltrim(rtrim(firstname)),'') +  ' ' + isnull(ltrim(rtrim(lastname)),'')
		when len(ltrim(rtrim(ISNULL(middlename,'')))) = 0 and len(ltrim(rtrim(ISNULL(lastname,'')))) = 0 then isnull(ltrim(rtrim(firstname)),'')
		when len(ltrim(rtrim(ISNULL(middlename,'')))) >= 1 then isnull(ltrim(rtrim(firstname)),'') + ' ' + isnull(LTRIM(RTRIM(middlename)),'') + ' ' + isnull(ltrim(rtrim(lastname)),'')  

	end
from tbl_emp 
where emp_id = @Emp_Id
)
END




GO
/****** Object:  UserDefinedFunction [dbo].[FUNCT_NAME2]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[FUNCT_NAME2]
(
@EMP_ID AS CHAR(4)
)
RETURNS VARCHAR(50)
AS  
BEGIN 
RETURN
--	(SELECT     ISNULL(FIRSTNAME,'') + ' ' + ISNULL(MIDDLENAME,'') +' ' + ISNULL(LASTNAME,'') AS EMP_NAME
--	FROM         dbo.TBL_EMP
--	WHERE EMP_ID = @EMP_ID)

(select emp_name = case
		when len(ltrim(rtrim(ISNULL(middlename,'')))) = 0 and len(ltrim(rtrim(ISNULL(lastname,'')))) <> 0 then '[' + emp_id + '] ' + isnull(ltrim(rtrim(firstname)),'') +  ' ' + isnull(ltrim(rtrim(lastname)),'')
		when len(ltrim(rtrim(ISNULL(middlename,'')))) = 0 and len(ltrim(rtrim(ISNULL(lastname,'')))) = 0 then '[' + emp_id + '] ' + isnull(ltrim(rtrim(firstname)),'')
		when len(ltrim(rtrim(ISNULL(middlename,'')))) >= 1 then '[' + emp_id + '] ' + isnull(ltrim(rtrim(firstname)),'') + ' ' + isnull(LTRIM(RTRIM(middlename)),'') + ' ' + isnull(ltrim(rtrim(lastname)),'')  

	end
from tbl_emp 
where emp_id = @Emp_Id
)
END






GO
/****** Object:  UserDefinedFunction [dbo].[Funct_SumLeave]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Select dbo.Funct_SumLeave('1082',2007,'AL')  as SumLeaveAL

CREATE  FUNCTION [dbo].[Funct_SumLeave]
(

@Emp_ID as char(4),
@YearTr as int,
@Type as Char(2)
)

RETURNS int AS  
BEGIN 

Declare @Total as int
Declare @x as int
Declare @DATE1 as char(2), @DATE2 as char(2), @DATE3 as char(2), @DATE4 as char(2), @DATE5 as char(2), @DATE6 as char(2), @DATE7 as char(2), @DATE8 as char(2), @DATE9 as char(2), @DATE10 as char(2), @DATE11 as char(2), @DATE12 as char(2), @DATE13 as char(2), @DATE14 as char(2), @DATE15 as char(2), @DATE16 as char(2), @DATE17 as char(2),
                      @DATE18 as char(2), @DATE19 as char(2), @DATE20 as char(2), @DATE21 as char(2), @DATE22 as char(2), @DATE23 as char(2), @DATE24 as char(2), @DATE25 as char(2), @DATE26 as char(2), @DATE27 as char(2), @DATE28 as char(2), @DATE29 as char(2), @DATE30 as char(2), @DATE31 as char(2)
set @total=0
set @x=1


Declare CC cursor for 
SELECT     DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, DATE8, DATE9, DATE10, DATE11, DATE12, DATE13, DATE14, DATE15, DATE16, DATE17, 
                      DATE18, DATE19, DATE20, DATE21, DATE22, DATE23, DATE24, DATE25, DATE26, DATE27, DATE28, DATE29, DATE30, DATE31
FROM         TBL_LEAVE_STATUS
WHERE EMP_ID = @Emp_Id and YearTr = @YearTr

OPEN CC
fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31

WHILE @@FETCH_STATUS=0
BEGIN
IF REPLACE(RTRIM(LTRIM(@DATE1)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE2)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE3)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE4)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE5)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE6)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE7)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE8)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE9)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE10)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF REPLACE(RTRIM(LTRIM(@DATE11)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE12)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE13)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE14)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE15)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE16)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE17)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE18)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE19)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE20)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF REPLACE(RTRIM(LTRIM(@DATE21)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE22)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE23)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE24)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE25)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE26)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE27)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE28)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE29)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE30)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF REPLACE(RTRIM(LTRIM(@DATE31)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1


fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31
end
CLOSE CC
deallocate CC
RETURN isnull(@Total,0)
END











GO
/****** Object:  UserDefinedFunction [dbo].[Funct_SumLeave3]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Funct_SumLeave3]
(

@Emp_ID as char(4),
@YearTr as int,
@Type as Char(2),
@date as datetime
)

RETURNS int AS  
BEGIN 

Declare @Total as int
Declare @x as int
DECLARE @MONTHTR AS INT
Declare @DATE1 as char(2), @DATE2 as char(2), @DATE3 as char(2), @DATE4 as char(2), @DATE5 as char(2), @DATE6 as char(2), @DATE7 as char(2), @DATE8 as char(2), @DATE9 as char(2), @DATE10 as char(2), @DATE11 as char(2), @DATE12 as char(2), @DATE13 as char(2), @DATE14 as char(2), @DATE15 as char(2), @DATE16 as char(2), @DATE17 as char(2),
                      @DATE18 as char(2), @DATE19 as char(2), @DATE20 as char(2), @DATE21 as char(2), @DATE22 as char(2), @DATE23 as char(2), @DATE24 as char(2), @DATE25 as char(2), @DATE26 as char(2), @DATE27 as char(2), @DATE28 as char(2), @DATE29 as char(2), @DATE30 as char(2), @DATE31 as char(2)
set @total=0
set @x=1


Declare CC cursor for 
SELECT   MONTHTR,  DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, DATE8, DATE9, DATE10, DATE11, DATE12, DATE13, DATE14, DATE15, DATE16, DATE17, 
                      DATE18, DATE19, DATE20, DATE21, DATE22, DATE23, DATE24, DATE25, DATE26, DATE27, DATE28, DATE29, DATE30, DATE31
FROM         TBL_LEAVE_STATUS
WHERE EMP_ID = @Emp_Id and YearTr = @YearTr --and monthtr <= month(@date)

OPEN CC
fetch next from CC into @MONTHTR, @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31

WHILE @@FETCH_STATUS=0
BEGIN
--IF @MONTHTR <= MONTH(@DATE)
--BEGIN
	IF RTRIM(LTRIM(@TYPE)) = 'AL' OR RTRIM(LTRIM(@TYPE)) = 'OL'
	BEGIN
		IF REPLACE(RTRIM(LTRIM(@DATE1)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE2)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE3)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE4)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE5)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE6)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE7)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE8)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE9)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE10)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		
		IF REPLACE(RTRIM(LTRIM(@DATE11)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE12)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE13)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE14)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE15)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE16)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE17)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE18)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE19)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE20)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		
		IF REPLACE(RTRIM(LTRIM(@DATE21)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE22)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE23)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE24)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE25)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE26)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE27)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE28)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE29)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE30)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF REPLACE(RTRIM(LTRIM(@DATE31)),'OL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
	END
	IF RTRIM(LTRIM(@TYPE)) = 'AD'
	BEGIN
		IF RTRIM(LTRIM(@DATE1))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE2))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE3))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE4))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE5))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE6))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE7))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE8))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE9))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE10))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		
		IF RTRIM(LTRIM(@DATE11))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE12))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE13))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE14))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE15))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE16))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE17))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE18))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE19))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE20))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		
		IF RTRIM(LTRIM(@DATE21))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE22))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE23))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE24))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE25))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE26))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE27))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE28))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE29))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE30))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
		IF RTRIM(LTRIM(@DATE31))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
	END

--END
-- IF @MONTHTR = MONTH(@DATE) AND @YEARTR = YEAR(@DATE)
-- BEGIN
-- 	IF REPLACE(RTRIM(LTRIM(@DATE1)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 1 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE2)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 2 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE3)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 3 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE4)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 4 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE5)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 5 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE6)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 6 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE7)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 7 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE8)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 8 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE9)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 9 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE10)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 10 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	
-- 	IF REPLACE(RTRIM(LTRIM(@DATE11)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 11 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE12)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 12 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE13)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 13 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE14)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 14 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE15)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 15 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE16)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 16 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE17)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 17 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE18)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 18 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE19)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 19 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE20)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 20 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	
-- 	IF REPLACE(RTRIM(LTRIM(@DATE21)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 21 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE22)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 22 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE23)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 23 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE24)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 24 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE25)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 25 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE26)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 26 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE27)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 27 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE28)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 28 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE29)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 29 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE30)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 30 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 	IF REPLACE(RTRIM(LTRIM(@DATE31)),'OL','AL')=RTRIM(LTRIM(@TYPE))  AND 31 <= DAY(@DATE) SET @TOTAL=@TOTAL+1
-- 
-- END

fetch next from CC into @MONTHTR,@DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31
end
CLOSE CC
deallocate CC
RETURN @Total
END















GO
/****** Object:  UserDefinedFunction [dbo].[Funct_SumLeaveMonth]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- SELECT dbo.Funct_SumLeaveMonth('0842',2008,01,'AL')

CREATE   FUNCTION [dbo].[Funct_SumLeaveMonth]
(

@Emp_ID as char(4),
@YearTr as int,
@MonthTr as int,
@Type as Char(2)
)

RETURNS int AS  
BEGIN 

Declare @Total as int
Declare @x as int
Declare @DATE1 as char(2), @DATE2 as char(2), @DATE3 as char(2), @DATE4 as char(2), @DATE5 as char(2), @DATE6 as char(2), @DATE7 as char(2), @DATE8 as char(2), @DATE9 as char(2), @DATE10 as char(2), @DATE11 as char(2), @DATE12 as char(2), @DATE13 as char(2), @DATE14 as char(2), @DATE15 as char(2), @DATE16 as char(2), @DATE17 as char(2),
                      @DATE18 as char(2), @DATE19 as char(2), @DATE20 as char(2), @DATE21 as char(2), @DATE22 as char(2), @DATE23 as char(2), @DATE24 as char(2), @DATE25 as char(2), @DATE26 as char(2), @DATE27 as char(2), @DATE28 as char(2), @DATE29 as char(2), @DATE30 as char(2), @DATE31 as char(2)
set @total=0
set @x=1


Declare CC cursor for 
SELECT     DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, DATE8, DATE9, DATE10, DATE11, DATE12, DATE13, DATE14, DATE15, DATE16, DATE17, 
                      DATE18, DATE19, DATE20, DATE21, DATE22, DATE23, DATE24, DATE25, DATE26, DATE27, DATE28, DATE29, DATE30, DATE31
FROM         TBL_LEAVE_STATUS
--WHERE EMP_ID = @Emp_Id and YearTr = @YearTr 
--WHERE EMP_ID = '0842' and YearTr = 2008 and MonthTr=01
WHERE EMP_ID = @Emp_Id and YearTr = @YearTr And MonthTr=@MonthTr

OPEN CC
fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31

WHILE @@FETCH_STATUS=0
BEGIN
IF (REPLACE(RTRIM(LTRIM(@DATE1)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE1)),'HL','AL') = RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE2)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE2)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE3)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE3)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE4)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE4)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE5)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE5)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE6)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE6)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE7)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE7)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE8)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE8)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE9)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE9)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE10)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE10)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1

IF (REPLACE(RTRIM(LTRIM(@DATE11)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE11)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE12)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE12)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE13)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE13)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE14)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE14)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE15)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE15)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE16)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE16)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE17)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE17)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE18)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE18)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE19)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE19)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE20)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE20)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1

IF (REPLACE(RTRIM(LTRIM(@DATE21)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE21)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE22)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE22)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE23)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE23)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE24)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE24)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE25)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE25)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE26)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE26)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE27)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE27)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE28)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE28)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE29)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE29)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE30)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE30)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1
IF (REPLACE(RTRIM(LTRIM(@DATE31)),'OL','AL')= RTRIM(LTRIM(@TYPE))) OR (REPLACE(RTRIM(LTRIM(@DATE31)),'HL','AL')=RTRIM(LTRIM(@TYPE))) SET @TOTAL=@TOTAL+1



-- -- Gabung dengan HL
-- IF REPLACE(RTRIM(LTRIM(@DATE1)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE2)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE3)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE4)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE5)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE6)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE7)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE8)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE9)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE10)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- 
-- IF REPLACE(RTRIM(LTRIM(@DATE11)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE12)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE13)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE14)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE15)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE16)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE17)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE18)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE19)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE20)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- 
-- IF REPLACE(RTRIM(LTRIM(@DATE21)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE22)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE23)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE24)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE25)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE26)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE27)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE28)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE29)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE30)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
-- IF REPLACE(RTRIM(LTRIM(@DATE31)),'HL','AL')=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1


fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31
end
CLOSE CC
deallocate CC
RETURN isnull(@Total,0)
END













GO
/****** Object:  UserDefinedFunction [dbo].[Funct_SumLeaveTaken]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select dbo.Funct_SumLeave('1082',2007,'AL')  as SumLeaveAL

CREATE  FUNCTION [dbo].[Funct_SumLeaveTaken]
(

@Emp_ID as char(4),
@YearTr as int,
@Type as Char(2)
)

RETURNS int AS  
BEGIN 

Declare @Total as int
Declare @x as int
Declare @DATE1 as char(2), @DATE2 as char(2), @DATE3 as char(2), @DATE4 as char(2), @DATE5 as char(2), @DATE6 as char(2), @DATE7 as char(2), @DATE8 as char(2), @DATE9 as char(2), @DATE10 as char(2), @DATE11 as char(2), @DATE12 as char(2), @DATE13 as char(2), @DATE14 as char(2), @DATE15 as char(2), @DATE16 as char(2), @DATE17 as char(2),
                      @DATE18 as char(2), @DATE19 as char(2), @DATE20 as char(2), @DATE21 as char(2), @DATE22 as char(2), @DATE23 as char(2), @DATE24 as char(2), @DATE25 as char(2), @DATE26 as char(2), @DATE27 as char(2), @DATE28 as char(2), @DATE29 as char(2), @DATE30 as char(2), @DATE31 as char(2)
set @total=0
set @x=1


Declare CC cursor for 
SELECT     DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, DATE8, DATE9, DATE10, DATE11, DATE12, DATE13, DATE14, DATE15, DATE16, DATE17, 
                      DATE18, DATE19, DATE20, DATE21, DATE22, DATE23, DATE24, DATE25, DATE26, DATE27, DATE28, DATE29, DATE30, DATE31
FROM         TBL_LEAVE_STATUS
WHERE EMP_ID = @Emp_Id and YearTr = @yeartr

OPEN CC
fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31

WHILE @@FETCH_STATUS=0
BEGIN
IF (RTRIM(LTRIM(@DATE1)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE2)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE3)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE4)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE5)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE6)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE7)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE8)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE9)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE10)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF (RTRIM(LTRIM(@DATE11)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE12)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE13)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE14)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE15)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE16)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE17)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE18)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE19)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE20)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF (RTRIM(LTRIM(@DATE21)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE22)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE23)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE24)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE25)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE26)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE27)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE28)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE29)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE30)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE31)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1


fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31
end
CLOSE CC
deallocate CC
RETURN isnull(@Total,0)
END
















GO
/****** Object:  UserDefinedFunction [dbo].[Funct_SumOL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  FUNCTION [dbo].[Funct_SumOL]
(

@Emp_ID as char(4),
@YearTr as int,
@Type as Char(2)
)

RETURNS int AS  
BEGIN 

Declare @Total as int
Declare @x as int
Declare @DATE1 as char(2), @DATE2 as char(2), @DATE3 as char(2), @DATE4 as char(2), @DATE5 as char(2), @DATE6 as char(2), @DATE7 as char(2), @DATE8 as char(2), @DATE9 as char(2), @DATE10 as char(2), @DATE11 as char(2), @DATE12 as char(2), @DATE13 as char(2), @DATE14 as char(2), @DATE15 as char(2), @DATE16 as char(2), @DATE17 as char(2),
                      @DATE18 as char(2), @DATE19 as char(2), @DATE20 as char(2), @DATE21 as char(2), @DATE22 as char(2), @DATE23 as char(2), @DATE24 as char(2), @DATE25 as char(2), @DATE26 as char(2), @DATE27 as char(2), @DATE28 as char(2), @DATE29 as char(2), @DATE30 as char(2), @DATE31 as char(2)
set @total=0
set @x=1


Declare CC cursor for 
SELECT     DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, DATE8, DATE9, DATE10, DATE11, DATE12, DATE13, DATE14, DATE15, DATE16, DATE17, 
                      DATE18, DATE19, DATE20, DATE21, DATE22, DATE23, DATE24, DATE25, DATE26, DATE27, DATE28, DATE29, DATE30, DATE31
FROM         TBL_LEAVE_STATUS
WHERE EMP_ID = @Emp_Id and YearTr = @YearTr

OPEN CC
fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31

WHILE @@FETCH_STATUS=0
BEGIN
IF (RTRIM(LTRIM(@DATE1)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE2)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE3)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE4)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE5)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE6)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE7)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE8)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE9)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE10)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF (RTRIM(LTRIM(@DATE11)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE12)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE13)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE14)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE15)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE16)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE17)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE18)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE19)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE20)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1

IF (RTRIM(LTRIM(@DATE21)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE22)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE23)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE24)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE25)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE26)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE27)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE28)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE29)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE30)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1
IF (RTRIM(LTRIM(@DATE31)))=RTRIM(LTRIM(@TYPE)) SET @TOTAL=@TOTAL+1


fetch next from CC into @DATE1, @DATE2, @DATE3, @DATE4, @DATE5, @DATE6, @DATE7, @DATE8, @DATE9, @DATE10, @DATE11, @DATE12, @DATE13, @DATE14, @DATE15, @DATE16, @DATE17,
                      @DATE18, @DATE19, @DATE20, @DATE21, @DATE22, @DATE23, @DATE24, @DATE25, @DATE26, @DATE27, @DATE28, @DATE29, @DATE30, @DATE31
end
CLOSE CC
deallocate CC
RETURN isnull(@Total,0)
END


GO
/****** Object:  UserDefinedFunction [dbo].[Funct_WhoMustApproved]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Funct_WhoMustApproved]
(@Emp_Id char(4), @Jobcode_id varchar(20), @Client_Jobcode varchar(20), @Sect_id varchar(20), @Category_id varchar(20), @Type int)
RETURNS char(4)
WITH EXEC AS CALLER
AS
BEGIN
--Declare @Type as int
--Set @Type = (Select dbo.Funct_ProjectAppType (@jobcode_Id, @category_Id))
DECLARE @ApprovedBy as Char(4)
	--IF RTRIM(LTRIM(@Jobcode_Id)) Not in ('7-5000-00-0000', '7-6000-00-0000', '8-0000-00-0000', '8-6000-00-0000','7-6000-00-0001')
	IF RTRIM(LTRIM(@Jobcode_Id)) Not in (SELECT DISTINCT JOBCODE_ID FROM TBL_PROJECT_GENERAL) --AND RTRIM(LTRIM(@Jobcode_Id)) NOT LIKE '0-0505-0%'
	BEGIN
		IF @Type = 1 Set @ApprovedBy = (Select FirstRater From MICADEBUG.dbo.tbl_OvertimeReq_App 
							Where RTRIM(LTRIM(Jobcode_id)) = RTRIM(LTRIM(@Jobcode_Id)) 
							and RTRIM(LTRIM(Client_Jobcode)) = RTRIM(LTRIM(@Client_Jobcode))
							and RTRIM(LTRIM(Sect_Id)) = RTRIM(LTRIM(@Sect_Id)) 
							and RTRIM(LTRIM(Category_Id)) = RTRIM(LTRIM(@Category_Id)))
		IF @Type = 2 Set @ApprovedBy = (Select SecondRater From MICADEBUG.dbo.tbl_OvertimeReq_App 
							Where RTRIM(LTRIM(Jobcode_id)) = RTRIM(LTRIM(@Jobcode_Id)) 
							and RTRIM(LTRIM(Client_Jobcode)) = RTRIM(LTRIM(@Client_Jobcode))
							and RTRIM(LTRIM(Sect_Id)) = RTRIM(LTRIM(@Sect_Id)) 
							and RTRIM(LTRIM(Category_Id)) = RTRIM(LTRIM(@Category_Id)))
	END
	--ELSE IF RTRIM(LTRIM(@Jobcode_Id)) LIKE '0-0505-0%'
	--	 BEGIN
	--	 IF @Type = 1 
	--		BEGIN
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-0102' SET @ApprovedBy = '0448'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-5001' SET @ApprovedBy = '1309'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-5002' SET @ApprovedBy = '0641'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1003' SET @ApprovedBy = '0388'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1004' SET @ApprovedBy = '0388'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1005' SET @ApprovedBy = '0584'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5007' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5008' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-0102' SET @ApprovedBy = '0151'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5101' SET @ApprovedBy = '0819'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5103' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-6102' SET @ApprovedBy = '0693'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-7003' SET @ApprovedBy = '1302'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-8001' SET @ApprovedBy = '1289'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-8102' SET @ApprovedBy = '0932'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-X001' SET @ApprovedBy = '0603'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-06-0002' SET @ApprovedBy = '1207'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-06-0003' SET @ApprovedBy = '1207'
	--		END
				


	--	 IF @Type = 2 
	--		BEGIN
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-0102' SET @ApprovedBy = '0448'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-5001' SET @ApprovedBy = '0819'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-00-5002' SET @ApprovedBy = '0819'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1003' SET @ApprovedBy = '0388'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1004' SET @ApprovedBy = '0388'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-1005' SET @ApprovedBy = '0388'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5007' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5008' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-0102' SET @ApprovedBy = '0151'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5101' SET @ApprovedBy = '0819'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-5103' SET @ApprovedBy = '1237'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-6102' SET @ApprovedBy = '0693'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-7003' SET @ApprovedBy = '1394'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-8001' SET @ApprovedBy = '0932'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-8102' SET @ApprovedBy = '0932'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-05-X001' SET @ApprovedBy = '0537'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-06-0002' SET @ApprovedBy = '1184'
	--			IF RTRIM(LTRIM(@Jobcode_Id)) = '0-0505-06-0003' SET @ApprovedBy = '1184'
	--		END
	--	 END
	ELSE
    -- edit by Indra, 11 juni 2013
	BEGIN -- jika jobcode_id ada di tbl_project_general tapi category_id tidak ada, atau jobcode_id dgn category_id tsb ada di tbl_project
      IF RTRIM(LTRIM(@Jobcode_Id)) Not in (SELECT DISTINCT JOBCODE_ID FROM TBL_PROJECT_GENERAL where CATEGORY_ID=@Category_Id)
      BEGIN
        IF @Type = 1 Set @ApprovedBy = (Select FirstRater From MICADEBUG.dbo.tbl_OvertimeReq_App 
							Where RTRIM(LTRIM(Jobcode_id)) = RTRIM(LTRIM(@Jobcode_Id)) 
							and RTRIM(LTRIM(Client_Jobcode)) = RTRIM(LTRIM(@Client_Jobcode))
							and RTRIM(LTRIM(Sect_Id)) = RTRIM(LTRIM(@Sect_Id)) 
							and RTRIM(LTRIM(Category_Id)) = RTRIM(LTRIM(@Category_Id)))
		    IF @Type = 2 Set @ApprovedBy = (Select SecondRater From MICADEBUG.dbo.tbl_OvertimeReq_App 
							Where RTRIM(LTRIM(Jobcode_id)) = RTRIM(LTRIM(@Jobcode_Id)) 
							and RTRIM(LTRIM(Client_Jobcode)) = RTRIM(LTRIM(@Client_Jobcode))
							and RTRIM(LTRIM(Sect_Id)) = RTRIM(LTRIM(@Sect_Id)) 
							and RTRIM(LTRIM(Category_Id)) = RTRIM(LTRIM(@Category_Id)))
      END
      ELSE
			BEGIN -- jika jobcode_id & category_id ada di tbl_project_general
			  IF @Type = 1 --and @Sect_Id Not like 'N300%'  
				  set @ApprovedBy = (Select Mgr_Emp_Id From TBL_SECTION WHERE SECT_ID = @SECT_ID)
			  IF @Type = 2 and @Sect_Id Not like 'E300%'  
			        and @Sect_Id Not like 'N220%'  
			        set @ApprovedBy = (SELECT     dbo.TBL_DEPT.MGR_EMP_ID
							FROM         dbo.TBL_DEPT INNER JOIN
					                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
							WHere TBL_SECTION.SECT_ID = @SECT_ID
							)
  --		IF @Type = 1 and @SECT_ID like 'N300%'  Set @ApprovedBy = '9030'
	  		IF @Type = 2 and (@SECT_ID like 'N300%'  or @SECT_ID like 'N220%') Set @ApprovedBy = '9022'
		  	IF @Type = 2 and (@SECT_ID like 'E300%') 
			  	set @ApprovedBy = (SELECT     dbo.TBL_DEPT.MGR_EMP_ID
							FROM         dbo.TBL_DEPT INNER JOIN
					                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID
							WHere TBL_SECTION.SECT_ID = @SECT_ID
							)
		  END
	END
	RETURN Isnull(@ApprovedBy, '')
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetAge]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetAge('2076')
CREATE FUNCTION [dbo].[GetAge]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)
	DECLARE @BirthDate DATETIME
	DECLARE @AgeTemp INT
	
	SELECT	@BirthDate = ISNULL(a.BIRTHDATE, '01/01/1900')
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	IF @BirthDate = '01/01/1900'
	BEGIN
		SET @Result = ''
	END
	ELSE
	BEGIN
		SELECT @AgeTemp = DATEDIFF(YEAR, @BirthDate, GETDATE())
		IF DATEADD(YEAR, @AgeTemp, @BirthDate) > GETDATE()
		BEGIN
			SET @AgeTemp = @AgeTemp -1
		END
		SET @Result = CAST(@AgeTemp AS VARCHAR(50)) + ' Years'
	END
	
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetAgeInt]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetAgeInt('2076')
CREATE FUNCTION [dbo].[GetAgeInt]
(
	@EmployeeId as Varchar(10)
)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT
	DECLARE @BirthDate DATETIME
	DECLARE @AgeTemp INT
	
	SELECT	@BirthDate = ISNULL(a.BIRTHDATE, '01/01/1900')
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	IF @BirthDate = '01/01/1900'
	BEGIN
		SET @Result = 0
	END
	ELSE
	BEGIN
		SELECT @AgeTemp = DATEDIFF(YEAR, @BirthDate, GETDATE())
		IF DATEADD(YEAR, @AgeTemp, @BirthDate) > GETDATE()
		BEGIN
			SET @AgeTemp = @AgeTemp -1
		END
		SET @Result = @AgeTemp
	END
	
	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetCurrentExperience]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetCurrentExperience('2076')
CREATE FUNCTION [dbo].[GetCurrentExperience]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)
	
	DECLARE @RefId INT
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)

	SELECT @Years = 0, @Months = 0, @Days = 0
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId 
	
	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.START_DATE, CASE 
								WHEN a.status = 1 THEN DATEADD(D, 1, GETDATE() )
								WHEN ISNULL(a.END_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.END_DATE)
								WHEN ISNULL(a.EXP_DATE1, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE1)
								WHEN ISNULL(a.EXP_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE)
							  END
		FROM	TBL_EMP a
		WHERE	a.REF_ID = @RefId
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += isnull(@YearsTemp,0)
			SET @Months += isnull(@MonthsTemp,0)
			SET @Days += isnull(@DaysTemp,0)
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	SELECT @YearString = CASE @Years WHEN 0 THEN '' WHEN 1 THEN '1 Year ' ELSE CAST(@Years AS VARCHAR(10)) + ' Years ' END
	SELECT @MonthString = CASE @Months WHEN 0 THEN '' WHEN 1 THEN '1 Month ' ELSE CAST(@Months AS VARCHAR(10)) + ' Months ' END
	SELECT @DayString = CASE @Days WHEN 0 THEN '' WHEN 1 THEN '1 Day' ELSE CAST(@Days AS VARCHAR(10)) + ' Days' END
	
	SET @Result = @YearString + @MonthString + @DayString

	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetCurrentExperienceInYear]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select dbo.GetCurrentExperience('2076')
CREATE FUNCTION [dbo].[GetCurrentExperienceInYear]
(
	@EmployeeId as Varchar(10)
)
RETURNS int
AS
BEGIN
	DECLARE @Result int
	
	DECLARE @RefId INT
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)

	SELECT @Years = 0, @Months = 0, @Days = 0
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId 
	
	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.START_DATE, CASE 
								WHEN a.status = 1 THEN DATEADD(D, 1, GETDATE() )
								WHEN ISNULL(a.END_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.END_DATE)
								WHEN ISNULL(a.EXP_DATE1, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE1)
								WHEN ISNULL(a.EXP_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE)
							  END
		FROM	TBL_EMP a
		WHERE	a.REF_ID = @RefId
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += isnull(@YearsTemp,0)
			SET @Months += isnull(@MonthsTemp,0)
			SET @Days += isnull(@DaysTemp,0)
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	IF (@Months >=8) 
	BEGIN
		SET @Years += 1
	END 
	
	SET @Result = @Years

	RETURN @Result
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetDeptManagerName]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetDeptManagerName]
(
	@DeptId varchar(50)
)
RETURNS varchar(512)
AS
BEGIN
	Declare @Result varchar(512)
	select  @Result = '[' + a.MGR_EMP_ID + '] ' + c.FULL_NAME 
	from 
			TBL_SECTION a 
			left join TBL_EMP c on a.MGR_EMP_ID = c.EMP_ID
	where a.SECT_ID= @DeptId
	
	return @Result 
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeName]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeName]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @EmployeeName Varchar(512)
	
	SELECT @EmployeeName = CASE 
								WHEN LTRIM(RTRIM(ISNULL(MIDDLENAME, ''))) = '' AND LTRIM(RTRIM(ISNULL(LASTNAME, ''))) = '' THEN FIRSTNAME 
								WHEN LTRIM(RTRIM(ISNULL(MIDDLENAME, ''))) = '' THEN FIRSTNAME + ' ' + LASTNAME 
								WHEN LTRIM(RTRIM(ISNULL(LASTNAME, ''))) = '' THEN FIRSTNAME + ' ' + MIDDLENAME 
								ELSE FIRSTNAME + ' ' + MIDDLENAME + ' ' + LASTNAME 
							END
	FROM TBL_EMP
	WHERE EMP_ID = @EmployeeId
	
	RETURN @EmployeeName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetLastGroupId]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetLastGroupId] 
(
	@EmployeeId varchar(50)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)

	SELECT top 1  @Result=a.GROUP_ID
	FROM	TBL_EMP_GROUP_MEMBER a
	left outer join TBL_EMP_GROUP b on a.GROUP_ID = b.GROUP_ID
	where EMP_ID=@EmployeeId 
	order by YEARTR desc, MONTHTR desc

	-- Return the result of the function
	RETURN @Result

END


GO
/****** Object:  UserDefinedFunction [dbo].[GetLastGroupName]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetLastGroupName]
(
	@EmployeeId varchar(50)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)

	SELECT top 1  @Result=b.GROUP_NAME
	FROM	TBL_EMP_GROUP_MEMBER a
	left outer join TBL_EMP_GROUP b on a.GROUP_ID = b.GROUP_ID
	where EMP_ID=@EmployeeId 
	order by YEARTR desc, MONTHTR desc

	-- Return the result of the function
	RETURN @Result

END


GO
/****** Object:  UserDefinedFunction [dbo].[GetPreviousExperience]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetPreviousExperience('0244')
CREATE FUNCTION [dbo].[GetPreviousExperience]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)
	DECLARE @DateStartP datetime, @DateEndP datetime
	
	SELECT @Years = 0, @Months = 0, @Days = 0

	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.STARTDATE, DATEADD(D, 1, a.ENDDATE)
		FROM	TBL_JOB_EX a
		WHERE	a.EMP_ID = @EmployeeId and ISNULL(a.IS_DELETED, 0) = 0
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (@DateStart > @DateEnd) AND (YEAR(@DateEnd) <> 1900)
			BEGIN
				SET @DateStartP = @DateEnd
				SET @DateEndP =  @DateStart
				SET @DateStart = @DateStartP
				SET @DateEnd = @DateEndP
			END
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += @YearsTemp
			SET @Months += @MonthsTemp
			SET @Days += @DaysTemp
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	SELECT @YearString = CASE @Years WHEN 0 THEN '' WHEN 1 THEN '1 Year ' ELSE CAST(@Years AS VARCHAR(10)) + ' Years ' END
	SELECT @MonthString = CASE @Months WHEN 0 THEN '' WHEN 1 THEN '1 Month ' ELSE CAST(@Months AS VARCHAR(10)) + ' Months ' END
	SELECT @DayString = CASE @Days WHEN 0 THEN '' WHEN 1 THEN '1 Day' ELSE CAST(@Days AS VARCHAR(10)) + ' Days' END
	
	SET @Result = @YearString + @MonthString + @DayString

	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPreviousExperienceInYear]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetPreviousExperienceInYear] 
(
	-- Add the parameters for the function here
	@EmployeeId as Varchar(10)
)
RETURNS int
AS
BEGIN
	DECLARE @Result int
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)
	DECLARE @DateStartP datetime, @DateEndP datetime
	
	SELECT @Years = 0, @Months = 0, @Days = 0

	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.STARTDATE, DATEADD(D, 1, a.ENDDATE)
		FROM	TBL_JOB_EX a
		WHERE	a.EMP_ID = @EmployeeId and ISNULL(a.IS_DELETED, 0) = 0
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (@DateStart > @DateEnd) AND (YEAR(@DateEnd) <> 1900)
			BEGIN
				SET @DateStartP = @DateEnd
				SET @DateEndP =  @DateStart
				SET @DateStart = @DateStartP
				SET @DateEnd = @DateEndP
			END
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += @YearsTemp
			SET @Months += @MonthsTemp
			SET @Days += @DaysTemp
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	IF (@Months >=8) 
	BEGIN
		SET @Years += 1
	END 
	
	SET @Result = @Years

	RETURN @Result

END

GO
/****** Object:  UserDefinedFunction [dbo].[GetStatusLeave]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetStatusLeave('1653')
CREATE FUNCTION [dbo].[GetStatusLeave]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)
	DECLARE @Duration VARCHAR(50), @LeaveTypeString VARCHAR(50), @LeaveStartString VARCHAR(50), @LeaveEndString VARCHAR(50)
	
	SET @Result = ''
	
	DECLARE db_cursor CURSOR
	FOR
		SELECT	a.CLASS_TYPE, a.TOTAL_DAY, REPLACE(CONVERT(NVARCHAR, a.START_DATE, 106), ' ', '-'), REPLACE(CONVERT(NVARCHAR, a.END_DATE, 106), ' ', '-')
		FROM	TBL_LEAVE_REQUEST a
		WHERE	a.EMP_ID = @EmployeeId AND a.STATUS = 3 AND (GETDATE() BETWEEN a.START_DATE AND DATEADD(D, 1, a.END_DATE) OR a.START_DATE > GETDATE())
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @LeaveTypeString, @Duration, @LeaveStartString, @LeaveEndString

	WHILE @@FETCH_STATUS = 0
	BEGIN
			SET @Result += @LeaveTypeString + ' ' + @Duration + ' Days' + ' (' + @LeaveStartString + ' s/d ' + @LeaveEndString + ')<BR>'
			FETCH NEXT FROM db_cursor INTO @LeaveTypeString, @Duration, @LeaveStartString, @LeaveEndString
	END

	CLOSE db_cursor
	DEALLOCATE db_cursor

	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTotalExperience]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.GetTotalExperience('0244')
CREATE FUNCTION [dbo].[GetTotalExperience]
(
	@EmployeeId as Varchar(10)
)
RETURNS varchar(512)
AS
BEGIN
	DECLARE @Result Varchar(512)
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)
	DECLARE @DateStartP datetime, @DateEndP datetime
	DECLARE @RefId INT
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId

	SELECT @Years = 0, @Months = 0, @Days = 0

	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.STARTDATE, DATEADD(D, 1, a.ENDDATE)
		FROM	TBL_JOB_EX a
		WHERE	a.EMP_ID = @EmployeeId and ISNULL(a.IS_DELETED, 0) = 0
		UNION
		SELECT	a.START_DATE, CASE 
								WHEN a.status = 1 THEN DATEADD(D, 1, GETDATE() )
								WHEN ISNULL(a.END_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.END_DATE)
								WHEN ISNULL(a.EXP_DATE1, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE1)
								WHEN ISNULL(a.EXP_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE)
							  END
		FROM	TBL_EMP a
		WHERE	a.REF_ID = @RefId
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (@DateStart > @DateEnd) AND (YEAR(@DateEnd) <> 1900)
			BEGIN
				SET @DateStartP = @DateEnd
				SET @DateEndP =  @DateStart
				SET @DateStart = @DateStartP
				SET @DateEnd = @DateEndP
			END
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += isnull(@YearsTemp,0)
			SET @Months += isnull(@MonthsTemp,0)
			SET @Days += isnull(@DaysTemp,0)
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	SELECT @YearString = CASE @Years WHEN 0 THEN '' WHEN 1 THEN '1 Year ' ELSE CAST(@Years AS VARCHAR(10)) + ' Years ' END
	SELECT @MonthString = CASE @Months WHEN 0 THEN '' WHEN 1 THEN '1 Month ' ELSE CAST(@Months AS VARCHAR(10)) + ' Months ' END
	SELECT @DayString = CASE @Days WHEN 0 THEN '' WHEN 1 THEN '1 Day' ELSE CAST(@Days AS VARCHAR(10)) + ' Days' END
	
	SET @Result = @YearString + @MonthString + @DayString

	RETURN @Result
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTotalExperienceinYear]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select dbo.GetTotalExperience('0244')
CREATE FUNCTION [dbo].[GetTotalExperienceinYear]
(
	@EmployeeId as Varchar(10)
)
RETURNS int
AS
BEGIN
	DECLARE @Result int
	
	DECLARE @DateStart DATETIME, @DateEnd DATETIME, @TempDate DATETIME
	DECLARE @YearsTemp INT, @MonthsTemp INT, @DaysTemp INT
	DECLARE @Years INT, @Months INT, @Days INT
	DECLARE @YearString VARCHAR(50), @MonthString VARCHAR(50), @DayString VARCHAR(50)
	DECLARE @DateStartP datetime, @DateEndP datetime
	DECLARE @RefId INT
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId

	SELECT @Years = 0, @Months = 0, @Days = 0

	DECLARE db_cursor CURSOR 
	FOR
		SELECT	a.STARTDATE, DATEADD(D, 1, a.ENDDATE)
		FROM	TBL_JOB_EX a
		WHERE	a.EMP_ID = @EmployeeId and ISNULL(a.IS_DELETED, 0) = 0
		UNION
		SELECT	a.START_DATE, CASE 
								WHEN a.status = 1 THEN DATEADD(D, 1, GETDATE() )
								WHEN ISNULL(a.END_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.END_DATE)
								WHEN ISNULL(a.EXP_DATE1, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE1)
								WHEN ISNULL(a.EXP_DATE, '01/01/1900') <> '01/01/1900' THEN DATEADD(D, 1, a.EXP_DATE)
							  END
		FROM	TBL_EMP a
		WHERE	a.REF_ID = @RefId
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF (@DateStart > @DateEnd) AND (YEAR(@DateEnd) <> 1900)
			BEGIN
				SET @DateStartP = @DateEnd
				SET @DateEndP =  @DateStart
				SET @DateStart = @DateStartP
				SET @DateEnd = @DateEndP
			END
			IF (Year(@DateStart) = 1900) OR (Year(@DateEnd) = 1900)
			BEGIN
				SET @DateStart = '1900-01-01'
				SET @DateEnd = '1900-01-01'
			END
			
			SET @TempDate = @DateStart
			SET @YearsTemp = DATEDIFF(yy, @TempDate, @DateEnd) - CASE WHEN (MONTH(@DateStart) > MONTH(@DateEnd)) OR (MONTH(@DateStart) = MONTH(@DateEnd) AND DAY(@DateStart) > DAY(@DateEnd)) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(yy, @YearsTemp, @TempDate)
			SET @MonthsTemp = DATEDIFF(m, @TempDate, @DateEnd) - CASE WHEN DAY(@DateStart) > DAY(@DateEnd) THEN 1 ELSE 0 END
			SET @TempDate = DATEADD(m, @MonthsTemp, @TempDate)
			SET @DaysTemp = DATEDIFF(d, @TempDate, @DateEnd)
			
			SET @Years += isnull(@YearsTemp,0)
			SET @Months += isnull(@MonthsTemp,0)
			SET @Days += isnull(@DaysTemp,0)
			
			SET @Months += @Days / 30
			SET @Years += @Months / 12
			SET @Months = @Months % 12
			SET @Days = @Days % 30
			
			FETCH NEXT FROM db_cursor INTO @DateStart, @DateEnd
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
	
	IF (@Months >=8) 
	BEGIN
		SET @Years += 1
	END 
	
	SET @Result = @Years

	RETURN @Result
END

GO
/****** Object:  UserDefinedFunction [dbo].[ProperCase]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ProperCase](@Text as varchar(8000))
returns varchar(8000)
as
begin
   declare @Reset bit;
   declare @Ret varchar(8000);
   declare @i int;
   declare @c char(1);

   select @Reset = 1, @i=1, @Ret = '';

   while (@i <= len(@Text))
    select @c= substring(@Text,@i,1),
               @Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
               @Reset = case when @c like '[a-zA-Z]' then 0 else 1 end,
               @i = @i +1
   return @Ret
end
GO
/****** Object:  UserDefinedFunction [dbo].[udf_DecodeBitmask]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_DecodeBitmask](
      @Tablename        VARCHAR(100),
      @ColumnsUpdated VARBINARY(255)
)
RETURNS @tblColumns TABLE ( ColumnsUpdated      VARCHAR(255) )
AS
BEGIN
      DECLARE @x XML
 
      SET @x = (
      SELECT      COLUMN_NAME
      FROM  INFORMATION_SCHEMA.COLUMNS Field
      WHERE       TABLE_NAME = @Tablename AND
                  sys.fn_IsBitSetInBitmask(
                  @ColumnsUpdated,
                  COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID')
                  ) <> 0      FOR XML AUTO, ROOT('Fields')
      )
 
      INSERT    @tblColumns
      SELECT    REPLACE(CAST(T.c.query('.') AS varchar(100)), '', '')
      FROM		@x.nodes('/Fields/Field') T(c)
 
      RETURN      
 
END
GO
/****** Object:  Table [dbo].[TBL_ACTIVE_DIRECTORY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ACTIVE_DIRECTORY](
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[DisplayName] [varchar](200) NULL,
	[Email] [varchar](100) NULL,
	[Description] [varchar](50) NOT NULL,
	[JobTitle] [varchar](100) NULL,
	[Department] [varchar](100) NULL,
	[Path] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ACTIVITIES]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ACTIVITIES](
	[ACT_ID] [char](3) NOT NULL,
	[ACT_DESC] [varchar](30) NULL,
	[DETAIL] [varchar](50) NULL,
	[STATUS] [int] NULL,
 CONSTRAINT [PK_TBL_ACTIVITY] PRIMARY KEY CLUSTERED 
(
	[ACT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_APPRAISAL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_APPRAISAL](
	[EMPNO] [varchar](5) NOT NULL,
	[EMPNAME] [nvarchar](255) NULL,
	[MONTH] [varchar](30) NOT NULL,
	[OVERTIME] [float] NULL,
	[LL] [float] NULL,
	[AL] [float] NULL,
	[OL] [float] NULL,
	[HL] [float] NULL,
	[SI] [float] NULL,
	[SL] [float] NULL,
	[EL] [float] NULL,
	[UL] [float] NULL,
	[EF] [float] NULL,
	[CL] [float] NULL,
	[AD] [float] NULL,
	[AB] [float] NULL,
	[CLATE] [float] NULL,
	[EFINISH] [float] NULL,
	[ILLACCESS] [float] NULL,
	[MONTHTR] [float] NULL,
	[YEARTR] [float] NULL,
	[TOTALWD] [float] NULL,
	[TOTALACTUALWD] [float] NULL,
	[SECT_ID] [nvarchar](255) NULL,
 CONSTRAINT [PK_TBL_APPRAISAL] PRIMARY KEY CLUSTERED 
(
	[EMPNO] ASC,
	[MONTH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BUSINESSLINE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_BUSINESSLINE](
	[BL_id] [int] NOT NULL,
	[BusinessLine] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_BUSINESSLINE] PRIMARY KEY CLUSTERED 
(
	[BL_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CARD_TR]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CARD_TR](
	[CARD_ID] [varchar](6) NOT NULL,
	[TR_DATE] [smalldatetime] NOT NULL,
	[CARD_IN] [smalldatetime] NULL,
	[CARD_OUT] [smalldatetime] NULL,
	[CARD_STATUS] [char](10) NULL,
	[CARDR_IN] [smalldatetime] NULL,
	[CARDR_OUT] [smalldatetime] NULL,
	[CARDR_STATUS] [char](2) NULL,
	[OT] [int] NULL,
	[CL] [int] NULL,
	[EF] [int] NULL,
	[IA] [int] NULL,
	[AL] [int] NULL,
	[OL] [int] NULL,
	[HL] [int] NULL,
	[SL] [int] NULL,
	[EL] [int] NULL,
	[UL] [int] NULL,
	[SI] [int] NULL,
	[AB] [int] NULL,
	[BT] [int] NULL,
	[CLSTAT] [int] NULL,
	[EFSTAT] [int] NULL,
	[REG] [int] NULL,
	[HOL] [int] NULL,
	[EXTTRANS] [tinyint] NULL,
	[TOTAL] [int] NULL,
	[ATT_TYPE_ID] [char](2) NULL,
	[EMP_ID] [varchar](6) NOT NULL,
	[OVERTIME] [smalldatetime] NULL,
	[LOC_ID] [char](10) NULL,
	[OT150] [int] NULL,
	[OT200] [int] NULL,
	[OT300] [int] NULL,
	[OT400] [int] NULL,
	[LOCK] [varchar](10) NULL,
	[WORKTYPE] [char](1) NULL,
	[TOTALDEDUCT] [int] NULL,
	[TOTALDEDUCT_EQUIV] [int] NULL,
	[ALLOWANCE] [int] NULL,
	[SA] [int] NULL,
	[COMPLETED] [int] NULL,
	[LL] [int] NULL,
	[PERMISSION] [int] NULL,
	[TAXIALL] [int] NULL,
	[MEALALL] [int] NULL,
	[MEALRP] [int] NULL,
	[TAXIRP] [int] NULL,
	[TAXIHOL] [int] NULL,
	[YOCWD] [int] NULL,
	[YOCHD1] [int] NULL,
	[YOCHD2] [int] NULL,
	[CREATED_BY] [varchar](6) NULL,
	[TYPE_ATT] [varchar](15) NULL,
	[CREATED_DATE] [smalldatetime] NULL,
	[AD] [int] NULL,
	[DEDUCT150] [int] NULL,
	[DEDUCT200] [int] NULL,
	[DEDUCT300] [int] NULL,
	[DEDUCT400] [int] NULL,
	[SALGRADE] [varchar](10) NULL,
	[UPDATED_BY] [varchar](6) NULL,
	[LASTUPDATE] [smalldatetime] NULL,
	[TIME_IN] [smalldatetime] NULL,
	[TIME_OUT] [smalldatetime] NULL,
	[DEDUCT_DRIVER] [int] NULL,
	[IA_DEDUCT] [int] NULL,
	[OT_PROCESS] [varchar](1) NULL,
	[SECT_ID] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CATEGORY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CATEGORY](
	[CATEGORY_ID] [varchar](255) NOT NULL,
	[FWBS] [varchar](10) NULL,
	[CATEGORY_NAME] [varchar](100) NULL,
	[SECT_ID] [varchar](10) NOT NULL,
	[REMARKS] [varchar](100) NULL,
	[STATUS] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CHANGE_HISTORY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CHANGE_HISTORY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KEY_ID] [varchar](20) NULL,
	[TABLE_NAME] [varchar](512) NULL,
	[COLUMN_NAME] [varchar](512) NULL,
	[COLUMN_VALUE] [varchar](8000) NULL,
	[CREATION_DATE] [datetime] NULL,
	[CREATED_BY] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_CHANGE_HISTORY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CL_ISSUE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CL_ISSUE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MONTH_TR] [int] NOT NULL,
	[YEAR_TR] [int] NOT NULL,
	[EMP_ID] [varchar](6) NOT NULL,
	[CL_CNT] [int] NULL,
	[CL_TTL] [int] NULL,
	[ConselingDate] [datetime] NULL,
	[EmailSent] [datetime] NULL,
	[SP_Date] [datetime] NULL,
	[SP_No] [int] NULL,
	[Remarks] [varchar](255) NULL,
 CONSTRAINT [PK__TBL_CL_I__209932E7431CF94A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[MONTH_TR] ASC,
	[YEAR_TR] ASC,
	[EMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_COUNTRY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_COUNTRY](
	[COUNTRY_ID] [varchar](2) NOT NULL,
	[COUNTRY_NAME] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_COURSE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_COURSE](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[SUBJECT] [varchar](250) NULL,
	[TRAINER] [varchar](250) NULL,
	[GRADE] [char](10) NULL,
	[PROVIDER] [varchar](250) NULL,
	[C_FROM] [datetime] NULL,
	[C_TO] [datetime] NULL,
	[REMARKS] [varchar](250) NULL,
	[PURPOSE] [varchar](1000) NULL,
	[AMOUNT] [int] NULL,
	[CURRENCY] [char](10) NULL,
	[PLACEOFTRAINING] [varchar](100) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_COURSE_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_COURSE_IS_DELETED]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_COURSE_INT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_COURSE_INT](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[SUBJECT] [varchar](500) NULL,
	[TRAINER] [varchar](500) NULL,
	[GRADE] [varchar](500) NULL,
	[PROVIDER] [varchar](500) NULL,
	[C_FROM] [datetime] NULL,
	[C_TO] [datetime] NULL,
	[REMARKS] [varchar](500) NULL,
	[PURPOSE] [varchar](1000) NULL,
	[AMOUNT] [int] NULL,
	[CURRENCY] [char](10) NULL,
	[PLACEOFTRAINING] [varchar](250) NULL,
	[EXPENSES] [int] NULL,
	[PAIDOUT] [int] NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_COURSE_INT_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_COURSE_INT_IS_DELETED]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_DELEGATION_APP]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_DELEGATION_APP](
	[TR_ID] [int] NOT NULL,
	[TYPE] [char](2) NULL,
	[EMP_ID] [char](4) NULL,
	[REP_OF] [char](4) NULL,
	[STARTDATE] [smalldatetime] NULL,
	[ENDDATE] [smalldatetime] NULL,
	[CREATE_BY] [char](4) NULL,
	[CREATE_DATE] [smalldatetime] NULL,
	[MODIFIED_BY] [char](4) NULL,
	[LAST_MODIFIED] [smalldatetime] NULL,
	[SECT_ID] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_DEPT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_DEPT](
	[DEPT_ID] [char](4) NOT NULL,
	[DEPT_NAME] [varchar](100) NULL,
	[MGR_EMP_ID] [char](4) NULL,
	[DPT_MGR_EMP_ID] [char](4) NULL,
 CONSTRAINT [PK_TBL_DEPT] PRIMARY KEY CLUSTERED 
(
	[DEPT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EDU_TYPE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EDU_TYPE](
	[EDU_ID] [char](2) NOT NULL,
	[EDU_DESC] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_EDU] PRIMARY KEY CLUSTERED 
(
	[EDU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EDUCATION]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EDUCATION](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[S_YEAR] [datetime] NULL,
	[GRAD_YEAR] [datetime] NULL,
	[INSTITUTE] [varchar](255) NOT NULL,
	[MAJORING] [varchar](100) NULL,
	[LOCATION] [varchar](100) NULL,
	[EDU_ID] [char](2) NULL,
	[GRADE] [char](4) NULL,
	[REMARKS] [varchar](50) NULL,
	[TITLE_DEGREE] [varchar](255) NULL,
	[ATTACHMENT] [varchar](512) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EDUCATION_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EDUCATION_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBl_EDUCATION] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[TR_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMAIL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMAIL](
	[Emp_Id] [varchar](6) NOT NULL,
	[Email] [varchar](255) NULL,
	[UserName] [varchar](50) NULL,
	[SendNotification] [int] NULL,
	[JPMASNotification] [int] NULL,
	[GeneratedOn] [datetime] NULL,
	[IS_ALLOW_IMPERSONATE] [bit] NULL CONSTRAINT [DF_TBL_EMAIL_IS_ALLOW_IMPERSONATE]  DEFAULT ((0)),
 CONSTRAINT [PK__TBL_EMAIL_AD__7675D217] PRIMARY KEY NONCLUSTERED 
(
	[Emp_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP](
	[EMP_ID] [char](4) NOT NULL,
	[EMP_TYPE_ID] [char](3) NULL,
	[FIRSTNAME] [varchar](50) NULL,
	[LASTNAME] [varchar](50) NULL,
	[GRADE] [char](2) NULL,
	[SECT_ID] [char](4) NULL,
	[SECT_EFFECTIVE_DATE] [datetime] NULL,
	[POSITION_ID] [char](3) NULL,
	[POSITION_EFFECTIVE_DATE] [datetime] NULL,
	[FRONTTITLE] [varchar](10) NULL,
	[LASTTITTLE] [varchar](10) NULL,
	[BIRTHDATE] [datetime] NULL,
	[BIRTHLOC] [varchar](50) NULL,
	[BIRTH_CERTIFICATE] [varchar](512) NULL,
	[SEX] [char](1) NULL,
	[MAR_STAT_ID] [char](2) NULL,
	[RELIGION_ID] [char](1) NULL,
	[ADDRESS_TYPE_ID] [varchar](50) NULL,
	[ADDRESS] [varchar](100) NULL,
	[CITY] [varchar](50) NULL,
	[REGION] [varchar](50) NULL,
	[POSTALCODE] [char](5) NULL,
	[COUNTRY] [varchar](50) NULL,
	[EMAIL] [varchar](50) NULL,
	[NATIONALITY] [varchar](50) NULL,
	[LOC_ID] [char](2) NULL,
	[PHONE] [varchar](20) NULL,
	[TEST1] [varchar](50) NULL,
	[TEST2] [varchar](50) NULL,
	[JOIN_DATE] [datetime] NULL,
	[PERMANENT_START_DATE] [datetime] NULL,
	[START_DATE_CONTRACT] [datetime] NULL,
	[END_DATE_CONTRACT] [datetime] NULL,
	[START_DATE] [datetime] NULL,
	[EXP_DATE] [datetime] NULL,
	[EXP_DATE1] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[ANUAL_LEAVE] [int] NULL,
	[BANKNAME] [varchar](30) NULL,
	[BRANCH] [varchar](30) NULL,
	[ACC_NUMBER] [varchar](50) NULL,
	[ACC_NAME] [varchar](50) NULL,
	[BANK_ATTACHMENT] [varchar](512) NULL,
	[MOVECOMPANY] [bit] NULL,
	[PENSION] [bit] NULL,
	[DIED] [bit] NULL,
	[OTHERS] [varchar](50) NULL,
	[MOVETO] [varchar](50) NULL,
	[MOVEADDRESS] [varchar](50) NULL,
	[CIPNAME] [varchar](20) NULL,
	[CIPTELP] [varchar](15) NULL,
	[CIPEMAIL] [varchar](50) NULL,
	[REMARKS] [varchar](50) NULL,
	[NEWEMP_ID] [char](4) NULL,
	[SALARY_GRADE] [char](10) NULL,
	[SALARY_GRADE_EFFECTIVE_DATE] [datetime] NULL,
	[WIN_LOGIN] [varchar](50) NULL,
	[LOCAL_EMP_ID] [char](15) NULL,
	[NEW_EMP_TYPE] [char](3) NULL,
	[HP] [varchar](50) NULL,
	[HP1] [varchar](50) NULL,
	[MIDDLENAME] [varchar](50) NULL,
	[PADDRESS_TYPE_ID] [varchar](50) NULL,
	[PADDRESS] [varchar](100) NULL,
	[PCITY] [varchar](50) NULL,
	[PPOSTALCODE] [varchar](50) NULL,
	[PREGION] [varchar](255) NULL,
	[PCOUNTRY] [varchar](255) NULL,
	[IDNUMBER] [varchar](20) NULL,
	[IDNUMBER_EXPIRE] [datetime] NULL,
	[IDNUMBER_ATTACHMENT] [varchar](512) NULL,
	[KK_NUMBER] [varchar](50) NULL,
	[KITAS_NUMBER] [varchar](50) NULL,
	[KITAS_EXPIRE] [datetime] NULL,
	[KITAS_POSITION] [varchar](512) NULL,
	[KITAS_ATTACHMENT] [varchar](512) NULL,
	[PASSPORT] [varchar](20) NULL,
	[PASSPORTV] [datetime] NULL,
	[PASSPORT_ATTACHMENT] [varchar](512) NULL,
	[EXT] [char](10) NULL,
	[MODIFY_BY] [char](4) NULL,
	[RESIGNED_BY] [char](4) NULL,
	[MODIFIED] [datetime] NULL,
	[FULL_NAME] [varchar](512) NULL,
	[REF_ID] [int] NULL,
	[EMERGENCY_PHONE] [varchar](50) NULL,
	[EMERGENCY_PHONE_NAME] [varchar](512) NULL,
	[EMERGENCY_PHONE_RELATION_ID] [varchar](50) NULL,
	[BLOOD_TYPE] [varchar](10) NULL,
	[TAX_STATUS_ID] [varchar](50) NULL,
	[NPWP_NUMBER] [varchar](50) NULL,
	[NPWP_ADDRESS] [varchar](512) NULL,
	[NPWP_ATTACHMENT] [varchar](512) NULL,
	[PREFER_SEND_LETTER_ID] [varchar](50) NULL,
	[JAMSOSTEK_NO] [varchar](255) NULL,
	[JAMSOSTEK_STATUS_ID] [varchar](50) NULL,
	[JAMSOSTEK_START_DATE] [datetime] NULL,
	[JAMSOSTEK_ATTACHMENT] [varchar](512) NULL,
	[BPJS_KESEHATAN_NO] [varchar](255) NULL,
	[BPJS_KESEHATAN_STATUS_ID] [varchar](50) NULL,
	[BPJS_KESEHATAN_START_DATE] [datetime] NULL,
	[BPJS_KESEHATAN_ATTACHMENT] [varchar](512) NULL,
	[BPJS_KESEHATAN_FASKES_CODE] [varchar](50) NULL,
	[CREATION_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_CREATION_DATE]  DEFAULT (getdate()),
	[CREATION_BY] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_EMP] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_ASSIGNMENT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_ASSIGNMENT](
	[ASSIGN_ID] [int] IDENTITY(1,1) NOT NULL,
	[LOC_ID] [char](2) NULL,
	[JOBCODE_ID] [varchar](20) NULL,
	[CLIENT_JOBCODE] [varchar](20) NULL,
	[PROJECT_SECT_ID] [char](4) NULL,
	[CATEGORY_ID] [varchar](8) NULL,
	[FWBS] [varchar](10) NULL,
	[EMP_ID] [char](4) NULL,
	[ASSIGN_TYPE] [int] NULL,
	[DOCTYPE] [char](10) NULL,
	[WORFA] [varchar](200) NULL,
	[START_WO] [datetime] NULL,
	[END_WO] [datetime] NULL,
	[START_MOB] [datetime] NULL,
	[END_MOB] [datetime] NULL,
	[REMARKS] [varchar](50) NULL,
	[PASSPORT] [smallint] NULL,
	[VISA] [smallint] NULL,
	[COE_GL] [smallint] NULL,
	[MCU] [smallint] NULL,
	[MCU_DATE] [datetime] NULL,
	[AIRTICKET] [smallint] NULL,
	[PA_START] [datetime] NULL,
	[PA_END] [datetime] NULL,
	[STATUS] [int] NULL,
	[TYPE1] [bit] NULL,
	[TYPE2] [int] NULL,
	[MEALS] [bit] NULL,
	[CRITERIA] [int] NULL,
	[GRADE] [char](1) NULL,
	[SITE] [decimal](18, 0) NULL,
	[DAILY] [decimal](18, 0) NULL,
	[DAYS] [int] NULL,
	[ADVANCE] [decimal](18, 0) NULL,
	[ADVANCEDATE] [datetime] NULL,
	[LIQ_ID] [char](9) NULL,
	[MODIFIED] [datetime] NULL,
	[POSNAME] [varchar](50) NULL,
	[SECTNAME] [varchar](50) NULL,
	[MODIFIEDBY] [char](4) NULL,
	[RFNE] [varchar](30) NULL,
	[CURRENT_SECT] [char](4) NULL,
	[PURPOSE] [varchar](255) NULL,
	[WDAYS] [char](2) NULL,
	[WHOURS] [char](2) NULL,
	[CURRENT_EMP_TYPE] [char](3) NULL,
	[CURRENT_POS_ID] [char](3) NULL,
	[MCU_STATUS] [int] NULL,
	[AREA] [int] NULL,
	[RFA_DATE] [datetime] NULL,
	[SITE_TYPE] [int] NULL,
 CONSTRAINT [PK_TBL_EMP_ASSIGNMENT] PRIMARY KEY CLUSTERED 
(
	[ASSIGN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_CONFIDENTIAL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_CONFIDENTIAL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[TYPE_ID] [varchar](50) NULL,
	[TYPE_NAME] [varchar](512) NULL,
	[DETAIL_TYPE_ID] [varchar](50) NULL,
	[DETAIL_TYPE_NAME] [varchar](512) NULL,
	[ACTIVITY_DATE] [datetime] NULL,
	[CONFIDENTIAL_ATTACHMENT] [varchar](512) NULL,
	[REMARK] [varchar](1024) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_CONFIDENTIAL_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_CONFIDENTIAL_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_EMP_CONFIDENTIAL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_ENGLISH_ASSESSMENT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_ENGLISH_ASSESSMENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NOT NULL,
	[ENGLISH_ASSESSMENT_TYPE_ID] [varchar](50) NULL,
	[ENGLISH_ASSESSMENT_TYPE_NAME] [varchar](512) NULL,
	[ENGLISH_ASSESSMENT_SCORE] [int] NOT NULL,
	[ENGLISH_ASSESSMENT_DATE] [datetime] NOT NULL,
	[ENGLISH_ASSESSMENT_ATTACHMENT] [varchar](512) NULL,
	[UPDATED_DATE] [datetime] NOT NULL CONSTRAINT [DF_TBL_TOEIC_CREATION_DATE]  DEFAULT (getdate()),
	[UPDATED_BY] [varchar](10) NOT NULL,
	[IS_DELETED] [bit] NOT NULL CONSTRAINT [DF_TBL_TOEIC_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_TOEIC] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_GRADE_CLASS]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_GRADE_CLASS](
	[GRADE_CLASSIFICATION] [varchar](100) NULL,
	[GRADE_POS] [char](10) NULL,
	[GRADECLASS_ID] [char](10) NOT NULL,
	[DEPT_ID] [char](4) NULL,
 CONSTRAINT [PK__TBL_EMP_GRADE_CL__09D91A06] PRIMARY KEY CLUSTERED 
(
	[GRADECLASS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_GRADE_CLASS_MEMBER]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_GRADE_CLASS_MEMBER](
	[GRADECLASS_ID] [char](10) NULL,
	[EMP_ID] [char](10) NULL,
	[GRADE_POS] [char](10) NULL CONSTRAINT [DF__TBL_EMP_G__STATU__4D8F25D5]  DEFAULT ((0)),
	[DEPT_ID] [char](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_GROUP]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_GROUP](
	[SECT_ID] [char](10) NOT NULL,
	[GROUP_ID] [char](10) NOT NULL,
	[GROUP_NAME] [varchar](100) NULL,
 CONSTRAINT [PK_TBL_EMP_GROUP] PRIMARY KEY CLUSTERED 
(
	[SECT_ID] ASC,
	[GROUP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_GROUP_MEMBER]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_EMP_GROUP_MEMBER](
	[GROUP_ID] [char](10) NULL,
	[MONTHTR] [int] NULL,
	[YEARTR] [int] NULL,
	[EMP_ID] [char](10) NULL,
	[STATUS] [int] NULL CONSTRAINT [DF_TBL_EMP_GROUP_MEMBER_STATUS]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_HISTORY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_EMP_HISTORY](
	[EMP_ID] [char](4) NOT NULL,
	[NUMB] [int] IDENTITY(1,1) NOT NULL,
	[TR_DATE] [datetime] NULL,
	[TR_DATE1] [datetime] NULL,
	[ACT] [char](3) NULL,
	[LOC_ID] [char](2) NULL,
	[SECT_ID] [char](4) NULL,
	[POSITION_ID] [char](3) NULL,
	[GRADE_ID] [char](2) NULL,
	[EMP_TYPE_ID] [char](3) NULL,
	[SALARY_GRADE] [char](10) NULL,
	[WAGE] [decimal](18, 0) NULL,
	[DETAILS] [varchar](50) NULL,
	[INC$] [decimal](18, 0) NULL,
	[INC] [decimal](18, 0) NULL,
	[PREVWAGE] [decimal](18, 0) NULL,
	[LASTINC] [decimal](18, 0) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[TBL_EMP_HISTORY] ADD [UPDATED_BY] [varchar](10) NULL
ALTER TABLE [dbo].[TBL_EMP_HISTORY] ADD [UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_UPDATED_DATE]  DEFAULT (getdate())
ALTER TABLE [dbo].[TBL_EMP_HISTORY] ADD [IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_IS_DELETED]  DEFAULT ((0))
 CONSTRAINT [PK_TBL_EMP_HISTORY] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[NUMB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_HISTORY_CAREER_PATH]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_HISTORY_CAREER_PATH](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[ACTIVITY_ID] [varchar](50) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[TABLE_NAME] [varchar](255) NULL,
	[COLUMN_NAME] [varchar](255) NULL,
	[COLUMN_VALUE] [varchar](512) NULL,
	[ATTACHMENT] [varchar](512) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_CAREER_PATH_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_CAREER_PATH_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_EMP_HISTORY_CAREER_PATH] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_HISTORY_RECRUITMENT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_HISTORY_RECRUITMENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[APPLICATION_ID] [varchar](50) NULL,
	[ISDELETED] [bit] NULL,
 CONSTRAINT [PK_TBL_EMP_HISTORY_RECRUITMENTID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_HISTORY_STATUS]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_HISTORY_STATUS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[ACTIVITY_ID] [varchar](50) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[TABLE_NAME] [varchar](255) NULL,
	[COLUMN_NAME] [varchar](255) NULL,
	[COLUMN_VALUE_ID] [varchar](255) NULL,
	[COLUMN_VALUE_NAME] [varchar](512) NULL,
	[COLUMN_VALUE_DESCRIPTION] [varchar](512) NULL,
	[ATTACHMENT] [varchar](512) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_STATUS_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_HISTORY_STATUS_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_EMP_HISTORY_STATUS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_LANGUAGE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_LANGUAGE](
	[EMP_ID] [char](4) NOT NULL,
	[LANG_ID] [int] IDENTITY(1,1) NOT NULL,
	[LANGUAGE_ID] [varchar](50) NULL,
	[LANGUAGE_] [varchar](50) NULL,
	[CLASSIFICATION] [varchar](50) NULL,
	[SKILL_READ] [varchar](50) NULL,
	[SKILL_WRITE] [varchar](50) NULL,
	[SKILL_SPEAK] [varchar](50) NULL,
	[UPDATED_BY] [varchar](50) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_LANGUAGE_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_LANGUAGE_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_EMP_LANGUAGE] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[LANG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_TRAINING]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_TRAINING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[SUBJECT] [varchar](1024) NULL,
	[OBJECTIVE] [varchar](8000) NULL,
	[PROVIDER] [varchar](512) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[CURRENCY] [varchar](10) NULL,
	[AMOUNT] [decimal](18, 2) NULL,
	[PLACE] [varchar](512) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_EMP_TRAINING_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_EMP_TRAINING_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_EMP_TRAINING] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_TYPE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_TYPE](
	[EMP_TYPE_ID] [char](3) NOT NULL,
	[TYPE_NAME] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_EMP_TYPE] PRIMARY KEY CLUSTERED 
(
	[EMP_TYPE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMP_WORKDONE_RS]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMP_WORKDONE_RS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMP_ID] [varchar](6) NOT NULL,
	[EMPLOYEE_NAME] [varchar](100) NULL,
	[SECT_ID] [varchar](5) NOT NULL,
	[DEPT_ID] [varchar](5) NULL,
	[START_PJ] [varchar](50) NOT NULL,
	[END_PJ] [varchar](50) NULL,
	[POSITION_NAME] [varchar](50) NOT NULL,
	[JOBCODE_ID] [varchar](50) NOT NULL,
	[PROJECT_NAME] [varchar](250) NULL,
	[PPA_CRITERIA] [varchar](5) NULL,
	[CRITERIA_NAME] [varchar](250) NULL,
	[PPA_SCOPE] [varchar](5) NULL,
	[PROJECT_VALUE] [varchar](250) NULL,
	[TOTALTSALLBYJOBCODE] [int] NULL,
	[TOTALHOURALL] [int] NULL,
	[TOTALMINUTEALL] [int] NULL,
	[MHSPENT] [varchar](250) NOT NULL,
	[SECT_NAME] [varchar](250) NULL,
	[DEPT_NAME] [varchar](250) NULL,
	[FLAG] [int] NULL CONSTRAINT [DF__TBL_EMP_WO__FLAG__412EB0B6]  DEFAULT ((0)),
	[CREATED_ON] [datetime] NULL,
	[CREATED_BY] [varchar](6) NULL,
 CONSTRAINT [PK_TBL_EMP_WORKDONE_RS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMPLOYEE_KTP_NPWP_JOIN_DATE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMPLOYEE_KTP_NPWP_JOIN_DATE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[EMPLOYEE_NAME] [varchar](512) NULL,
	[JOIN_DATE] [datetime] NULL,
	[KTP_NUMBER] [varchar](50) NULL,
	[NPWP_NUMBER] [varchar](50) NULL,
	[NPWP_ADDRESS] [varchar](512) NULL,
 CONSTRAINT [PK_TBL_EMPLOYEE_KTP_NPWP_JOIN_DATE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMPLOYEE_PASSPORT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMPLOYEE_PASSPORT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[EMPLOYEE_NAME] [varchar](512) NULL,
	[DEPARTMENT_NAME] [varchar](512) NULL,
	[PASSPORT_NO] [varchar](50) NULL,
	[PASSPORT_EXPIRED] [datetime] NULL,
 CONSTRAINT [PK_TBL_EMPLOYEE_PASSPORT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EMPLOYEE_START_RS]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EMPLOYEE_START_RS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[EMPLOYEE_NAME] [varchar](512) NULL,
	[DEPARTMENT_NAME] [varchar](512) NULL,
	[STATUS] [varchar](50) NULL,
	[START_DATE] [datetime] NULL,
	[LOCATION] [varchar](512) NULL,
	[START_RS_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBL_EMPLOYEE_START_RS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FAM_REL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FAM_REL](
	[REL_ID] [char](2) NOT NULL,
	[REL_DESC] [varchar](20) NULL,
 CONSTRAINT [PK_TBL_FAM_REL] PRIMARY KEY CLUSTERED 
(
	[REL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FAMILY]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FAMILY](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[REL_ID] [char](2) NOT NULL,
	[SNAME] [varchar](50) NULL,
	[GENDER] [char](1) NULL,
	[BIRTHDATE] [datetime] NULL,
	[BIRTH_CERTIFICATE_ATTACHMENT] [varchar](512) NULL,
	[OCCUPATION] [varchar](20) NULL,
	[REMARKS] [varchar](20) NULL,
	[UPDATED_BY] [varchar](5) NULL,
	[UPDATED_DATE] [datetime] NULL,
	[LIFE_STATUS_ID] [varchar](50) NULL,
	[LIFE_STATUS_NAME] [varchar](512) NULL,
	[MEDICAL_ID] [int] NULL,
	[MEDICAL_COVERED_ID] [varchar](50) NULL,
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_FAMILY_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_FAMILY] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[TR_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FAMILY_DOCUMENT_ATTACHMENT]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FAMILY_DOCUMENT_ATTACHMENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NOT NULL,
	[FAMILY_DOCUMENT_TYPE_ID] [varchar](255) NOT NULL,
	[FAMILY_DOCUMENT_ATTACHMENT] [varchar](512) NOT NULL,
	[FAMILY_DOCUMENT_REMARK] [varchar](1024) NULL,
	[UPDATED_DATE] [datetime] NOT NULL CONSTRAINT [DF_TBL_FAMILY_DOCUMENT_ATTACHMENT_UPDATED_DATE]  DEFAULT (getdate()),
	[UPDATED_BY] [varchar](10) NOT NULL,
	[IS_DELETED] [bit] NOT NULL CONSTRAINT [DF_TBL_FAMILY_DOCUMENT_ATTACHMENT_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_FAMILY_DOCUMENT_ATTACHMENT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FAMILY_TEMP_EXCEL]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FAMILY_TEMP_EXCEL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NO_PESERTA] [varchar](20) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[MEDICAL_ID] [int] NULL,
	[PATIENT_NAME] [varchar](512) NULL,
	[SEX] [varchar](1) NULL,
	[BIRTH_DATE] [datetime] NULL,
	[GRADE] [varchar](10) NULL,
	[SECT_ID] [varchar](50) NULL,
	[SECT_NAME] [varchar](512) NULL,
	[MEDICAL_PLAN] [varchar](10) NULL,
	[STATUS] [varchar](50) NULL,
	[BANK_NAME] [varchar](512) NULL,
	[ACCOUNT_NO] [varchar](50) NULL,
	[ACCOUNT_NAME] [varchar](512) NULL,
 CONSTRAINT [PK_TBL_FAMILY_TEMP_EXCEL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FASKES]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FASKES](
	[CODE] [varchar](50) NOT NULL,
	[NAME] [varchar](255) NULL,
	[ADDRESS] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FWBS]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FWBS](
	[CATEGORY_ID] [varchar](6) NULL,
	[CATEGORY_NAME] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_GENERAL_TYPE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_GENERAL_TYPE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TYPE] [varchar](255) NOT NULL,
	[TYPE_ID] [varchar](255) NOT NULL,
	[TYPE_NAME] [varchar](512) NOT NULL,
	[SEQUENCE] [int] NOT NULL CONSTRAINT [DF_TBL_GENERAL_TYPE_SEQUENCE]  DEFAULT ((0)),
	[CREATION_DATE] [datetime] NOT NULL CONSTRAINT [DF_TBL_GENERAL_TYPE_CREATION_DATE]  DEFAULT (getdate()),
	[CREATED_BY] [varchar](10) NOT NULL,
	[IS_DELETED] [bit] NOT NULL CONSTRAINT [DF_TBL_GENERAL_TYPE_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_GENERAL_TYPE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_TBL_GENERAL_TYPE] UNIQUE NONCLUSTERED 
(
	[TYPE] ASC,
	[TYPE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_GRADE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_GRADE](
	[GRADE_ID] [char](2) NOT NULL,
	[GRADE_NAME] [varchar](20) NULL,
	[REMARK] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_GRADE] PRIMARY KEY CLUSTERED 
(
	[GRADE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HOL_TR]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HOL_TR](
	[HOL_DATE] [smalldatetime] NOT NULL,
	[HOL_TYPE_ID] [char](2) NULL,
	[HOL_NAME] [varchar](50) NULL,
	[LOC_ID] [varchar](4) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HOL_TYPE]    Script Date: 6/10/2016 5:47:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HOL_TYPE](
	[HOL_TYPE_ID] [char](2) NOT NULL,
	[HOL_NAME] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_JOB_EX]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_JOB_EX](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[COMPANY] [varchar](512) NULL,
	[COMPANY_DESC] [varchar](512) NULL,
	[REFFERENSI] [varchar](50) NULL,
	[POS] [varchar](512) NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[LASTSALARY] [decimal](18, 0) NULL,
	[JOBDESC] [varchar](512) NULL,
	[RLEAVING] [varchar](512) NULL,
	[REMARKS] [varchar](512) NULL,
	[CURRENCY] [varchar](10) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_JOB_EX_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NOT NULL CONSTRAINT [DF_TBL_JOB_EX_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_JOB_EX] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[TR_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LEAVE_ALLOCATION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LEAVE_ALLOCATION](
	[Emp_id] [char](4) NOT NULL,
	[Yeartr] [int] NOT NULL,
	[LastYearRemain] [int] NULL,
	[AL] [int] NULL,
	[AD] [int] NULL,
	[HL] [int] NULL,
	[CreatedBy] [char](4) NULL,
	[CreatedOn] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[ModifiedBy] [char](4) NULL,
	[ExpiredAL] [datetime] NULL,
	[ExpiredAD] [datetime] NULL,
	[ExpiredHL] [datetime] NULL,
	[ExtStatus] [int] NULL,
	[EXP_AL] [int] NULL CONSTRAINT [DF_TBL_LEAVE_ALLOCATION_EXP_AL]  DEFAULT ((0)),
	[EXP_AD] [int] NULL CONSTRAINT [DF_TBL_LEAVE_ALLOCATION_EXP_AD]  DEFAULT ((0)),
	[EXP_HL] [int] NULL,
 CONSTRAINT [PK_TBL_LEAVE_ALLOCATION] PRIMARY KEY CLUSTERED 
(
	[Emp_id] ASC,
	[Yeartr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LEAVE_REQUEST]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LEAVE_REQUEST](
	[LEAVE_REQUEST] [varchar](20) NOT NULL,
	[EMP_ID] [char](4) NULL,
	[CLASS_TYPE] [char](2) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[TOTAL_DAY] [int] NULL,
	[STATUS] [int] NOT NULL,
	[REMARKS] [varchar](50) NULL,
	[APP_DEPT_BY] [char](4) NULL,
	[APP_DIV_BY] [char](4) NULL,
	[DEPT_MGR] [char](4) NULL,
	[DEPT_DATE] [datetime] NULL,
	[DIV_MGR] [char](10) NULL,
	[DIV_DATE] [datetime] NULL,
	[REQUEST_BY] [char](10) NULL,
	[REQUESTED_ON] [datetime] NULL,
	[UPDATE_BY] [char](10) NULL,
	[UPDATED_ON] [datetime] NULL,
	[PROCESS] [int] NULL,
	[ADV_YEAR] [int] NULL,
	[PROCESS_DATE] [datetime] NULL,
 CONSTRAINT [PK_TBL_LEAVE_REQUEST] PRIMARY KEY CLUSTERED 
(
	[LEAVE_REQUEST] ASC,
	[STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LEAVE_STATUS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LEAVE_STATUS](
	[EMP_ID] [varchar](4) NOT NULL,
	[MONTHTR] [int] NOT NULL,
	[YEARTR] [int] NOT NULL,
	[EMPLOYEE_NAME] [char](50) NULL,
	[SECT_ID] [char](5) NULL,
	[DEPT_ID] [char](4) NULL,
	[SECT_NAME] [char](100) NULL,
	[DATE1] [varchar](10) NULL,
	[DATE2] [varchar](10) NULL,
	[DATE3] [varchar](10) NULL,
	[DATE4] [varchar](10) NULL,
	[DATE5] [varchar](10) NULL,
	[DATE6] [varchar](10) NULL,
	[DATE7] [varchar](10) NULL,
	[DATE8] [varchar](10) NULL,
	[DATE9] [varchar](10) NULL,
	[DATE10] [varchar](10) NULL,
	[DATE11] [varchar](10) NULL,
	[DATE12] [varchar](10) NULL,
	[DATE13] [varchar](10) NULL,
	[DATE14] [varchar](10) NULL,
	[DATE15] [varchar](10) NULL,
	[DATE16] [varchar](10) NULL,
	[DATE17] [varchar](10) NULL,
	[DATE18] [varchar](10) NULL,
	[DATE19] [varchar](10) NULL,
	[DATE20] [varchar](10) NULL,
	[DATE21] [varchar](10) NULL,
	[DATE22] [varchar](10) NULL,
	[DATE23] [varchar](10) NULL,
	[DATE24] [varchar](10) NULL,
	[DATE25] [varchar](10) NULL,
	[DATE26] [varchar](10) NULL,
	[DATE27] [varchar](10) NULL,
	[DATE28] [varchar](10) NULL,
	[DATE29] [varchar](10) NULL,
	[DATE30] [varchar](10) NULL,
	[DATE31] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LOAN]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LOAN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EMPLOYEE_ID] [varchar](10) NULL,
	[BANK_ID] [varchar](50) NULL,
	[BANK_NAME] [varchar](512) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[AMOUNT] [decimal](18, 3) NULL,
	[DURATION] [int] NULL,
	[INSTALLMENT] [int] NULL,
	[PAYMENT] [decimal](18, 3) NULL,
	[PAID_OFF_DATE] [datetime] NULL,
	[AS_OF_DATE] [datetime] NULL,
	[PAID_OFF_ATTACHMENT] [varchar](512) NULL,
	[ACTIVE] [varchar](50) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_LOAN_UPDATED_DATE]  DEFAULT (getdate()),
	[UPDATED_BY] [varchar](10) NULL,
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_LOAN_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_LOAN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LOCATION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LOCATION](
	[LOC_ID] [char](2) NOT NULL,
	[LOC_NAME] [varchar](50) NULL,
	[COUNTRY] [char](2) NULL,
	[AREA] [int] NULL,
	[PROC_ATT] [int] NULL,
 CONSTRAINT [PK_TBL_LOCATION] PRIMARY KEY CLUSTERED 
(
	[LOC_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LOG_EMAIL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LOG_EMAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailDate] [datetime] NULL,
	[EmailFrom] [varchar](150) NULL,
	[EmailTo] [varchar](150) NULL,
	[Subject] [varchar](200) NULL,
	[Description] [text] NULL,
	[Process] [int] NULL,
	[ProcessDate] [datetime] NULL,
	[MsgType] [char](3) NULL,
 CONSTRAINT [PK_TBL_LOG_EMAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LOGPINTU]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LOGPINTU](
	[EMPNO] [char](10) NOT NULL,
	[TGL] [smalldatetime] NOT NULL,
	[JAM] [int] NOT NULL,
	[MENIT] [int] NOT NULL,
	[SERIAL] [int] NOT NULL,
	[MESIN] [char](10) NOT NULL,
	[FLAG] [char](1) NULL,
	[CONVERTED] [smalldatetime] NULL,
	[NOTE] [char](30) NULL,
	[DESCRIPTN] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MAR_STAT]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MAR_STAT](
	[MAR_STAT_ID] [char](2) NOT NULL,
	[DESCRIPTION] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_MAR_STAT] PRIMARY KEY CLUSTERED 
(
	[MAR_STAT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MASTER_BALANCE_SHEET]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MASTER_BALANCE_SHEET](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [varchar](2) NOT NULL,
	[PROJECT_NAME] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TBL_MASTER_BALANCE_SHEET] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MASTER_BALANCE_SHEET_DETAIL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MASTER_BALANCE_SHEET_DETAIL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_MASTER] [int] NOT NULL,
	[CODE] [varchar](2) NOT NULL,
	[DETAIL_NAME] [varchar](500) NOT NULL,
 CONSTRAINT [PK_TBL_MASTER_BALANCE_SHEET_DETAIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MASTER_BALANCE_SHEET_JOBCODE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MASTER_BALANCE_SHEET_JOBCODE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_DETAIL] [int] NOT NULL,
	[JOB_CODE] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TBL_MASTER_BALANCE_SHEET_JOBCODE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MASTER_DROPDOWN]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MASTER_DROPDOWN](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Dropdown_value] [varchar](50) NULL,
	[Dropdown_text] [varchar](50) NULL,
	[Application] [varchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TBL_MASTER_DROPDOWN] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MEDICAL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MEDICAL](
	[EMP_ID] [char](4) NOT NULL,
	[TR_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[TYPE_ID] [varchar](50) NULL,
	[TYPE_NAME] [varchar](512) NULL,
	[DETAIL_ID] [varchar](50) NULL,
	[DETAIL_NAME] [varchar](512) NULL,
	[MEDICAL_DATE] [datetime] NULL,
	[REFFERENCE] [varchar](512) NULL,
	[ATTACHMENT] [varchar](512) NULL,
	[REMARKS] [varchar](512) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_MEDICAL_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_MEDICAL_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_MEDICAL] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[TR_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_OL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_OL](
	[hol_date] [datetime] NULL,
	[hol_name] [char](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ORG_CHART]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ORG_CHART](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHART_GROUP] [varchar](50) NULL,
	[CHART_GROUP_DETAIL] [varchar](50) NULL,
	[CHART_GROUP_DETAIL_DESC] [varchar](512) NULL,
	[POSITION_NAME] [varchar](512) NULL,
	[EMP_ID] [varchar](10) NULL,
	[EMP_NAME] [varchar](512) NULL,
	[PICTURE_NAME] [varchar](512) NULL,
	[SEQUENCE] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PERIOD]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PERIOD](
	[PERIOD_ID] [int] NULL,
	[PERIOD_NAME] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PLANT]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PLANT](
	[ID_PLANT] [varchar](2) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TBL_PLANT] PRIMARY KEY CLUSTERED 
(
	[ID_PLANT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_POSITION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_POSITION](
	[POSITION_ID] [char](3) NOT NULL,
	[POSITION_NAME] [varchar](50) NULL,
	[STATUS] [char](10) NULL,
	[STRUCTURAL] [int] NULL,
 CONSTRAINT [PK_TBL_POSITION] PRIMARY KEY CLUSTERED 
(
	[POSITION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PPA_DURATION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PPA_DURATION](
	[DURATION_ID] [int] IDENTITY(1,1) NOT NULL,
	[EMP_ID] [varchar](4) NOT NULL,
	[JOBCODE_ID] [varchar](20) NOT NULL,
	[CATEGORY_ID] [varchar](50) NULL,
	[FWBS] [varchar](10) NULL,
	[PPA_POS_ID] [varchar](5) NOT NULL,
	[PPA_LEVEL_ID] [varchar](5) NOT NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[FLAG] [char](1) NULL,
	[PROCESS] [char](1) NULL,
	[PARENT_ID] [int] NULL,
	[PREV_JOBCODE] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PROJECT]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PROJECT](
	[JOBCODE_ID] [varchar](20) NOT NULL,
	[PROJECT_NAME] [varchar](255) NULL,
	[CLIENT_NAME] [varchar](50) NULL,
	[CLIENT_JOBCODE] [varchar](20) NOT NULL,
	[SCOPE] [varchar](500) NULL,
	[LOC_ID] [char](2) NULL,
	[CONTRACT_PRICE_US] [money] NULL,
	[EXCHANGE_RATE_US] [char](10) NULL,
	[CONTRACT_PRICE_YEN] [money] NULL,
	[EXCHANGE_RATE_YEN] [char](10) NULL,
	[CONTRACT_PRICE] [money] NULL,
	[EXCHANGE_RATE] [numeric](18, 0) NULL,
	[CONTRACT_TYPE] [char](2) NULL,
	[COMMENCE] [varchar](20) NULL,
	[DELIVERY] [varchar](20) NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[KEYP_EMP_ID] [char](4) NULL,
	[CHECK_EMP_ID] [char](4) NULL,
	[MEMBS_EMP_ID] [char](4) NULL,
	[REMARK] [varchar](255) NULL,
	[REFF] [varchar](30) NULL,
	[CO_TOTAL] [float] NULL,
	[MH_TOTAL] [float] NULL,
	[PC_TOTAL] [char](10) NULL,
	[PROFLOSS] [char](10) NULL,
	[QUALITYREC] [char](10) NULL,
	[STATUS] [char](1) NULL,
	[WO_DATE] [datetime] NULL,
	[WO_FILE] [varchar](30) NULL,
	[PREPAREDBY] [char](4) NULL,
	[CHECKEDBY] [char](4) NULL,
	[APPROVEDBY] [char](4) NULL,
	[PROJECTISSUED] [datetime] NULL,
	[CREATEDBY] [char](4) NULL,
	[MODIFYBY] [char](4) NULL,
	[RELEASED] [char](1) NULL CONSTRAINT [DF_TBL_PROJECT_RELEASED]  DEFAULT ((0)),
	[HISTORY] [char](1) NULL CONSTRAINT [DF_TBL_PROJECT_HISTORY]  DEFAULT ((0)),
	[TYPE] [varchar](2) NULL,
	[CURRENCY] [varchar](3) NULL,
	[COR] [varchar](30) NULL,
	[COR_DATE] [datetime] NULL,
	[GENERAL] [bit] NULL CONSTRAINT [DF_TBL_PROJECT_GENERAL]  DEFAULT ((0)),
	[MHRATE] [float] NULL,
	[ADD_PRICE] [money] NULL,
	[LOCATION] [varchar](100) NULL,
	[PRICE_PAID] [money] NULL,
	[REVISED_DATE] [datetime] NULL,
	[VERSION] [int] NOT NULL,
	[LOCK] [varchar](4) NULL,
	[REVISION] [int] NOT NULL CONSTRAINT [DF_TBL_PROJECT_REVISION]  DEFAULT ((0)),
	[RFNA_NUMBER] [varchar](50) NULL,
	[RFNA_DOC] [varchar](50) NULL,
	[APPROVED_BY] [varchar](4) NULL,
	[APPROVED_DATE] [datetime] NULL,
	[APPROVAL_COMMENT] [varchar](200) NULL,
	[GEC_JOBCODE] [varchar](20) NULL,
	[GENERIC_NAME] [varchar](100) NULL,
	[APP_TYPE] [int] NULL CONSTRAINT [DF_TBL_PROJECT_APP_TYPE]  DEFAULT ((0)),
	[OWN_JOB] [int] NULL CONSTRAINT [DF_TBL_PROJECT_OWN_JOB]  DEFAULT ((0)),
	[PPA_CRITERIA] [varchar](3) NULL DEFAULT ('N/A'),
	[PPA_SCOPE] [varchar](3) NULL DEFAULT ('N/A'),
 CONSTRAINT [PK_TBL_PROJECT] PRIMARY KEY CLUSTERED 
(
	[JOBCODE_ID] ASC,
	[CLIENT_JOBCODE] ASC,
	[VERSION] ASC,
	[REVISION] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PROJECT_FWBS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PROJECT_FWBS](
	[CLIENT_JOBCODE] [varchar](20) NOT NULL,
	[CATEGORY_ID] [char](10) NOT NULL,
	[FWBS] [varchar](10) NULL,
	[DESCRIPTION] [char](50) NULL,
	[SECT_ID] [char](4) NOT NULL,
	[JOBCODE_ID] [varchar](50) NOT NULL,
	[MHPLAN] [int] NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[KEYPERSON] [varchar](4) NULL,
	[MHACTUAL] [int] NULL,
	[STARTDATE_ACTUAL] [datetime] NULL,
	[ENDDATE_ACTUAL] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PROJECT_GENERAL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PROJECT_GENERAL](
	[JOBCODE_ID] [varchar](20) NOT NULL,
	[DEPT] [char](4) NOT NULL,
	[CLIENT_JOBCODE] [varchar](20) NOT NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[CATEGORY_ID] [varchar](20) NOT NULL,
	[FWBS] [varchar](10) NULL,
	[STATUS] [int] NOT NULL,
	[EMP_ID] [varchar](500) NULL,
	[TRAINING] [int] NULL CONSTRAINT [DF__TBL_PROJE__TRAIN__45B29F2B]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_PROJECT_GENERAL] PRIMARY KEY CLUSTERED 
(
	[JOBCODE_ID] ASC,
	[DEPT] ASC,
	[CLIENT_JOBCODE] ASC,
	[CATEGORY_ID] ASC,
	[STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PROJECT_PREV]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PROJECT_PREV](
	[Emp_Id] [char](4) NOT NULL,
	[TR_Number] [int] IDENTITY(1,1) NOT NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[ProjectType] [varchar](50) NULL,
	[ProjectName] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[ProjectOwner] [varchar](50) NULL,
	[ProjectValue] [varchar](50) NULL,
	[CompanyName] [varchar](50) NULL,
	[BL_ID] [varchar](50) NULL,
	[Pos_ID] [varchar](50) NULL,
	[UPDATED_BY] [varchar](10) NULL,
	[UPDATED_DATE] [datetime] NULL CONSTRAINT [DF_TBL_PROJECT_PREV_UPDATED_DATE]  DEFAULT (getdate()),
	[IS_DELETED] [bit] NULL CONSTRAINT [DF_TBL_PROJECT_PREV_IS_DELETED]  DEFAULT ((0)),
 CONSTRAINT [PK_TBL_PROJECT_PREV] PRIMARY KEY CLUSTERED 
(
	[Emp_Id] ASC,
	[TR_Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RELIGION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RELIGION](
	[RELIGION_ID] [char](1) NOT NULL,
	[RELIGION_NAME] [varchar](20) NULL,
 CONSTRAINT [PK_TBL_RELIGION] PRIMARY KEY CLUSTERED 
(
	[RELIGION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SAVING_CARD]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SAVING_CARD](
	[SAVING_CARD_ID] [varchar](10) NULL,
	[SAVING_DATE_FROM] [smalldatetime] NOT NULL,
	[SAVING_DATE_TO] [smalldatetime] NULL,
	[TR_NUMBER] [int] NOT NULL,
	[EMP_ID] [char](4) NULL,
	[STATUS] [int] NULL,
	[CREATED_BY] [varchar](4) NULL,
	[CREATED_DATE] [smalldatetime] NULL,
	[UPDATED_BY] [varchar](4) NULL,
	[UPDATED_DATE] [smalldatetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SECTION]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SECTION](
	[SECT_ID] [char](4) NOT NULL,
	[SECT_NAME] [varchar](150) NULL,
	[DEPT_ID] [char](4) NULL,
	[MGR_EMP_ID] [char](4) NULL,
	[GEC_DEPT] [varchar](10) NULL,
	[CATEGORY] [varchar](50) NULL,
	[PHONE_NO] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_SECTION] PRIMARY KEY CLUSTERED 
(
	[SECT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TEMP_COLUMN_UPDATE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TEMP_COLUMN_UPDATE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[COLUMN_UPDATE] [varbinary](1024) NULL,
	[COLUMN_UPDATE_LENGTH] [int] NULL,
	[DATA] [varchar](512) NULL,
 CONSTRAINT [PK_TBL_TEMP_COLUMN_UPDATE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TIMESHEET]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TIMESHEET](
	[EMP_ID] [char](4) NOT NULL,
	[CLIENT_JOBCODE] [varchar](20) NOT NULL,
	[JOBCODE_ID] [varchar](20) NOT NULL,
	[PROJECT_SECT_ID] [char](4) NOT NULL,
	[CATEGORY_ID] [varchar](20) NOT NULL,
	[FWBS] [varchar](10) NULL,
	[TR_DATE] [datetime] NOT NULL,
	[TIME_IN] [datetime] NULL,
	[TIME_OUT] [datetime] NULL,
	[APPROVE] [bit] NULL,
	[APPROVE1] [bit] NULL,
	[APPROVE2] [bit] NULL,
	[APPROVE3] [bit] NULL,
	[REMARKS] [varchar](50) NULL,
	[TIMESPENT] [int] NULL,
	[LOC_ID] [char](2) NULL,
	[MODIFY_BY] [char](4) NULL,
	[LAST_MODIFIED] [datetime] NULL,
	[REMARKS_JOBCODE] [varchar](500) NULL,
	[DATE_LOCK] [datetime] NULL,
	[KNUMBER] [varchar](6) NULL,
 CONSTRAINT [PK_TBL_TIMESHEET] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[CLIENT_JOBCODE] ASC,
	[JOBCODE_ID] ASC,
	[PROJECT_SECT_ID] ASC,
	[CATEGORY_ID] ASC,
	[TR_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TIMESHEET_DE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TIMESHEET_DE](
	[EMP_ID] [char](4) NOT NULL,
	[CLIENT_JOBCODE] [varchar](20) NOT NULL,
	[JOBCODE_ID] [varchar](20) NOT NULL,
	[PROJECT_SECT_ID] [char](4) NOT NULL,
	[CATEGORY_ID] [varchar](20) NOT NULL,
	[FWBS] [varchar](10) NULL,
	[TR_DATE] [datetime] NOT NULL,
	[TIME_IN] [datetime] NULL,
	[TIME_OUT] [datetime] NULL,
	[APPROVE] [bit] NULL,
	[APPROVE1] [bit] NULL,
	[APPROVE2] [bit] NULL,
	[APPROVE3] [bit] NULL,
	[REMARKS] [varchar](50) NULL,
	[TIMESPENT] [int] NULL,
	[LOC_ID] [char](2) NULL,
	[MODIFY_BY] [char](4) NULL,
	[LAST_MODIFIED] [datetime] NULL,
	[REMARKS_JOBCODE] [varchar](500) NULL,
	[DATE_LOCK] [datetime] NULL,
	[KNUMBER] [varchar](6) NULL,
	[FWBS6] [varchar](10) NULL,
	[Plant] [varchar](2) NULL,
	[Unit] [int] NULL,
 CONSTRAINT [PK_TBL_TIMESHEET_DE] PRIMARY KEY CLUSTERED 
(
	[EMP_ID] ASC,
	[CLIENT_JOBCODE] ASC,
	[JOBCODE_ID] ASC,
	[PROJECT_SECT_ID] ASC,
	[CATEGORY_ID] ASC,
	[TR_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_UNIT]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_UNIT](
	[ID_UNIT] [int] IDENTITY(1,1) NOT NULL,
	[ID_PLANT] [varchar](2) NOT NULL,
	[NAME] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TBL_UNIT] PRIMARY KEY CLUSTERED 
(
	[ID_UNIT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_USER_MS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_USER_MS](
	[USER_ID] [char](4) NOT NULL,
	[PASSWORD] [binary](50) NULL,
	[USER_FGS_ID] [char](4) NULL,
	[MODUL_ID] [char](2) NOT NULL,
	[AUTHORITY] [int] NULL,
	[GROUP_ID] [char](4) NULL,
	[CREATED_BY] [varchar](4) NULL,
	[CREATED_DATE] [smalldatetime] NULL,
	[UPDATED_BY] [varchar](4) NULL,
	[LAST_UPDATE] [smalldatetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_WORK_HOUR]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_WORK_HOUR](
	[WORK_HOUR_ID] [char](2) NOT NULL,
	[START_TIME] [smalldatetime] NULL,
	[END_TIME] [smalldatetime] NULL,
	[BSTART_TIME] [smalldatetime] NULL,
	[BEND_TIME] [smalldatetime] NULL,
	[TOT_HOUR] [int] NULL,
	[EXT_START_TIME] [smalldatetime] NULL,
	[LOC_ID] [varchar](2) NOT NULL,
	[JOBCODE_ID] [varchar](20) NULL,
	[WORKTYPE] [int] NULL,
	[REMARKS] [char](50) NULL,
	[START_FASTING] [smalldatetime] NULL,
	[END_FASTING] [smalldatetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[V_EMPLOYEE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_EMPLOYEE]
AS
SELECT     a.EMP_ID, a.START_DATE, a.EXP_DATE, a.EXP_DATE1, a.END_DATE, dbo.FUNCT_NAME(a.EMP_ID) + ' [' + ISNULL(a.EMP_ID, '') + ']' AS EMPLOYEE_NAME, ISNULL(a.GRADE, '') 
                      AS GRADE, ISNULL(a.STATUS, '') AS STATUS, ISNULL(b.POSITION_NAME, '') AS POSITION_NAME, ISNULL(c.SECT_ID, '') AS SECT_ID, 
                      ISNULL(c.SECT_NAME, '') AS SECT_NAME, ISNULL(c.DEPT_ID, '') AS DEPT_ID, ISNULL(d.DEPT_NAME, '') AS DEPT_NAME, a.EMP_TYPE_ID, 
                      ISNULL(a.SEX, '') AS SEX, ISNULL(a.LOC_ID, '') AS LOC_ID, dbo.TBL_LOCATION.LOC_NAME, ISNULL(a.SALARY_GRADE, '') AS SAL_GRADE, 
                      ISNULL(a.EXT, '') AS EXTENSION,A.POSITION_ID
FROM         dbo.TBL_EMP a LEFT OUTER JOIN
                      dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID INNER JOIN
                      dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID INNER JOIN
                      dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID LEFT OUTER JOIN
                      dbo.TBL_LOCATION ON a.LOC_ID = dbo.TBL_LOCATION.LOC_ID
WHERE     (a.STATUS = '1')



GO
/****** Object:  View [dbo].[V_APPRAISAL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_APPRAISAL]
AS
SELECT A.EMPNO,
	A.EMPNAME,
	[MONTH],
	OVERTIME,
	LL,
	AL,
	OL,
	HL,
	SI,
	SL,
	EL,
	EF,
	CL,
	AB,
	ISNULL(AD,0) AS AD,
	ABS(CLATE) AS CLATE,
	ABS(EFINISH) AS EFINISH,
	ABS(ILLACCESS) AS ILLACCESS,
	UL,
	MONTHTR,
	CAST(YEARTR AS VARCHAR) AS YEARTR,
	B.EMP_ID,
	B.DEPT_NAME,
	B.DEPT_ID,
	B.SECT_ID,
	B.SECT_NAME,
	B.SAL_GRADE,
	ISNULL(A.TOTALWD,0) AS TOTALWD,
	ISNULL(A.TOTALACTUALWD,0) AS TOTALACTUALWD,
	SDATE = CAST(CAST(MONTHTR AS VARCHAR) + '/1/' + CAST(YEARTR AS VARCHAR) AS DATETIME),
  CASE
    WHEN B.EXP_DATE1 IS NULL OR B.EXP_DATE1 = '01-JAN-1900' THEN B.START_DATE
    ELSE B.EXP_DATE+1
  END AS START_DATE, 
  CASE
    WHEN B.EXP_DATE1 IS NULL OR B.EXP_DATE1 = '01-JAN-1900' THEN B.EXP_DATE
    ELSE B.EXP_DATE1
  END AS EXP_DATE, 
  B.END_DATE, B.EMP_TYPE_ID
	FROM 
	TBL_APPRAISAL A INNER JOIN V_EMPLOYEE B
ON SUBSTRING(A.EMPNO,2,4) = B.EMP_ID

GO
/****** Object:  View [dbo].[V_DE_MHSPENTPERDATE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM V_MHSPENTPERDATE WHERE EMP_ID = '0858' order by  DATE_TR desc
CREATE VIEW [dbo].[V_DE_MHSPENTPERDATE]
AS
SELECT     DISTINCT 
		A.EMP_ID, 
		A.TR_DATE AS DATE_TR, 
		B.PROJECT_NAME,  
		TOTAL_SPENT = CASE
			WHEN A.JOBCODE_ID = 'N-0000-00-000' THEN 480
			ELSE TIMESPENT
		END,	
		A.JOBCODE_ID, 
		A.CLIENT_JOBCODE,
		A.PROJECT_SECT_ID,
		A.CATEGORY_ID,
		ISNULL(A.TIME_IN,'') AS TIME_IN,
		ISNULL(A.TIME_OUT,'') AS TIME_OUT,
		A.APPROVE3,
		A.LOC_ID,
		' ' AS GENERAL,
		ISNULL(REMARKS_JOBCODE,'') AS REMARKS_JOBCODE,
		FWBS6,
		A.PLANT,
		A.UNIT
FROM      PFNATT.dbo.TBL_TIMESHEET_DE A 
INNER JOIN MICADEBUG.dbo.TBL_PROJECT B ON A.JOBCODE_ID = B.JOBCODE_ID AND A.CLIENT_JOBCODE = B.CLIENT_JOBCODE
UNION ALL
SELECT     DISTINCT 
		A.EMP_ID, 
		A.TR_DATE AS DATE_TR, 
		B.DESCRIPTION AS PROJECT_NAME,  
		A.TIMESPENT AS TOTAL_SPENT, 
		A.JOBCODE_ID, 
		A.CLIENT_JOBCODE,
		A.PROJECT_SECT_ID,
		A.CATEGORY_ID,
		ISNULL(A.TIME_IN,'') AS TIME_IN,
		ISNULL(A.TIME_OUT,'') AS TIME_OUT,
		A.APPROVE3,
		A.LOC_ID,
		'G' AS GENERAL,
		ISNULL(REMARKS_JOBCODE,'') AS REMARKS_JOBCODE,
		FWBS6,
		A.PLANT,
		A.UNIT
FROM       PFNATT.dbo.TBL_TIMESHEET_DE A 
INNER JOIN MICADEBUG.dbo.TBL_PROJECT_GENERAL B ON A.JOBCODE_ID = B.JOBCODE_ID AND A.CLIENT_JOBCODE = B.CLIENT_JOBCODE AND A.PROJECT_SECT_ID = B.DEPT


GO
/****** Object:  View [dbo].[V_MHSPENTPERDATE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM V_MHSPENTPERDATE WHERE EMP_ID = '0858' order by  DATE_TR desc
CREATE VIEW [dbo].[V_MHSPENTPERDATE]
AS
SELECT     DISTINCT 
		A.EMP_ID, 
		A.TR_DATE AS DATE_TR, 
		B.PROJECT_NAME,  
		TOTAL_SPENT = CASE
			WHEN A.JOBCODE_ID = 'N-0000-00-000' THEN 480
			ELSE TIMESPENT
		END,	
		A.JOBCODE_ID, 
		A.CLIENT_JOBCODE,
		A.PROJECT_SECT_ID,
		A.CATEGORY_ID,
		ISNULL(A.TIME_IN,'') AS TIME_IN,
		ISNULL(A.TIME_OUT,'') AS TIME_OUT,
		A.APPROVE3,
		A.LOC_ID,
		' ' AS GENERAL,
		ISNULL(REMARKS_JOBCODE,'') AS REMARKS_JOBCODE
FROM       TBL_TIMESHEET A 
INNER JOIN TBL_PROJECT B ON A.JOBCODE_ID = B.JOBCODE_ID AND A.CLIENT_JOBCODE = B.CLIENT_JOBCODE
UNION ALL
SELECT     DISTINCT 
		A.EMP_ID, 
		A.TR_DATE AS DATE_TR, 
		B.DESCRIPTION AS PROJECT_NAME,  
		A.TIMESPENT AS TOTAL_SPENT, 
		A.JOBCODE_ID, 
		A.CLIENT_JOBCODE,
		A.PROJECT_SECT_ID,
		A.CATEGORY_ID,
		ISNULL(A.TIME_IN,'') AS TIME_IN,
		ISNULL(A.TIME_OUT,'') AS TIME_OUT,
		A.APPROVE3,
		A.LOC_ID,
		'G' AS GENERAL,
		ISNULL(REMARKS_JOBCODE,'') AS REMARKS_JOBCODE
FROM       TBL_TIMESHEET A 
INNER JOIN TBL_PROJECT_GENERAL B ON A.JOBCODE_ID = B.JOBCODE_ID AND A.CLIENT_JOBCODE = B.CLIENT_JOBCODE AND A.PROJECT_SECT_ID = B.DEPT
































GO
/****** Object:  View [dbo].[V_OT_EMP_APPROVER]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_OT_EMP_APPROVER]
AS
SELECT [EMP_ID]
      ,[JOBCODE_ID]
      ,[Project_Name]
      ,[CLIENT_JOBCODE]
      ,[SECT_ID]
      ,[CATEGORY_ID]
      ,[P_YEAR]
      ,[FIRSTRATER]
      ,[SECONDRATER]
  FROM MICADEBUG.[dbo].[V1_OT_EMP_APPROVER]



GO
/****** Object:  View [dbo].[V_OVERTIMEREQUEST]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_OVERTIMEREQUEST]
AS
SELECT  DISTINCT MICADEBUG.dbo.TBL_OVERTIMEREQ.REQUEST_ID, 
	--'[' + RIGHT(LTRIM(RTRIM(CATEGORY_ID)),3) + '] ' + 
	LTRIM(RTRIM(TBL_PROJECT.PROJECT_NAME)) AS PROJECT_NAME,
	'[' +  LTRIM(RTRIM(MICADEBUG.dbo.TBL_PROJECT.JOBCODE_ID)) + '] ' + LTRIM(RTRIM(TBL_PROJECT.PROJECT_NAME)) AS PROJECT,
	MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID, 
	ISNULL(MICADEBUG.dbo.TBL_OVERTIMEREQ.REMARKS, '') AS REMARKS, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_DATE, 
	ISNULL(MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_TIME,'') AS OT_TIME, 
	MICADEBUG.dbo.TBL_STATUS.DESCRIPTION AS STATUSDESC, 
        MICADEBUG.dbo.TBL_OVERTIMEREQ.STATUS, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID + '-' + ISNULL(MICADEBUG.dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(MICADEBUG.dbo.TBL_EMP.LASTNAME, '') AS EMPLOYEE_NAME, 
	ISNULL(MICADEBUG.dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(MICADEBUG.dbo.TBL_EMP.LASTNAME, '') AS EMPLOYEE, 
	MICADEBUG.dbo.TBL_CARD_TR.CARD_IN, MICADEBUG.dbo.TBL_CARD_TR.CARD_OUT, 
        LTRIM(RTRIM(MICADEBUG.dbo.TBL_PROJECT.JOBCODE_ID)) AS JOBCODE_ID,
	LTRIM(RTRIM(TBL_PROJECT.CLIENT_JOBCODE)) AS CLIENT_JOBCODE,
	MICADEBUG.dbo.TBL_CARD_TR.REG, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.SECT_ID, 
	MICADEBUG.dbo.TBL_SECTION.DEPT_ID,
	ISNULL(APPROVALSECT,'') AS APPROVALSECT, 
	ISNULL(TBL_OVERTIMEREQ.CATEGORY_ID,'') AS CATEGORY_ID, 	
	ISNULL(APPROVALDEPT,'') AS APPROVALDEPT,
	ISNULL(TBL_SECTION.SECT_NAME ,'') AS SECT_NAME,
	ISNULL(TBL_EMP.POSITION_ID,'') AS POS_ID,
	DEPT_NAME, 
	ISNULL(REQUESTED_DATE,'') AS REQUESTED_DATE,
	ISNULL(B.FIRSTNAME, '') + ' ' + ISNULL(B.LASTNAME, '') AS APP_MAN,
	ISNULL(C.FIRSTNAME, '') + ' ' + ISNULL(C.LASTNAME, '') AS APP_FIRST,
	APPROVEDDEPT_DATE,
	APPROVEDSECT_DATE
FROM		MICADEBUG.dbo.TBL_OVERTIMEREQ 
INNER JOIN	MICADEBUG.DBO.TBL_PROJECT ON LTRIM(RTRIM(MICADEBUG.DBO.TBL_PROJECT.JOBCODE_ID)) = LTRIM(RTRIM(MICADEBUG.DBO.TBL_OVERTIMEREQ.JOBCODE_ID)) AND LTRIM(RTRIM(MICADEBUG.dbo.TBL_PROJECT.CLIENT_JOBCODE)) = LTRIM(RTRIM(MICADEBUG.dbo.TBL_OVERTIMEREQ.CLIENT_JOBCODE)) 
INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_SEC ON RTRIM(MICADEBUG.dbo.TBL_PROJECT_SEC.JOBCODE_ID) = RTRIM(MICADEBUG.dbo.TBL_PROJECT.JOBCODE_ID) AND MICADEBUG.dbo.TBL_PROJECT_SEC.CLIENT_JOBCODE = MICADEBUG.dbo.TBL_PROJECT.CLIENT_JOBCODE 
INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_FWBS ON RTRIM(MICADEBUG.dbo.TBL_PROJECT_SEC.JOBCODE_ID) = RTRIM(MICADEBUG.dbo.TBL_PROJECT_FWBS.JOBCODE_ID) AND MICADEBUG.dbo.TBL_PROJECT_SEC.CLIENT_JOBCODE = MICADEBUG.dbo.TBL_PROJECT_FWBS.CLIENT_JOBCODE 
		AND MICADEBUG.dbo.TBL_PROJECT_FWBS.SECT_ID = MICADEBUG.dbo.TBL_PROJECT_SEC.SECT_ID AND MICADEBUG.dbo.TBL_OVERTIMEREQ.SECT_ID = MICADEBUG.dbo.TBL_PROJECT_FWBS.SECT_ID 
INNER JOIN	MICADEBUG.dbo.TBL_EMP ON MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID = MICADEBUG.dbo.TBL_EMP.EMP_ID 
INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON MICADEBUG.dbo.TBL_EMP.SECT_ID = MICADEBUG.dbo.TBL_SECTION.SECT_ID 
INNER JOIN	MICADEBUG.dbo.TBL_DEPT ON MICADEBUG.dbo.TBL_SECTION.DEPT_ID = MICADEBUG.dbo.TBL_DEPT.DEPT_ID 
LEFT OUTER JOIN		MICADEBUG.dbo.TBL_STATUS ON MICADEBUG.dbo.TBL_OVERTIMEREQ.STATUS = MICADEBUG.dbo.TBL_STATUS.STATUS 
LEFT OUTER JOIN		MICADEBUG.dbo.TBL_CARD_TR ON MICADEBUG.dbo.TBL_EMP.EMP_ID = MICADEBUG.dbo.TBL_CARD_TR.EMP_ID AND MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_DATE = MICADEBUG.dbo.TBL_CARD_TR.TR_DATE
LEFT JOIN MICADEBUG.dbo.TBL_EMP B ON B.EMP_ID = MICADEBUG.dbo.TBL_OVERTIMEREQ.APPROVALDEPT
LEFT JOIN MICADEBUG.dbo.TBL_EMP C ON C.EMP_ID = TBL_OVERTIMEREQ.APPROVALSECT

UNION ALL
SELECT  DISTINCT MICADEBUG.dbo.TBL_OVERTIMEREQ.REQUEST_ID, 
	LTRIM(RTRIM(TBL_PROJECT_GENERAL.DESCRIPTION)) AS PROJECT_NAME,
	'[' +  LTRIM(RTRIM(MICADEBUG.dbo.TBL_PROJECT_GENERAL.JOBCODE_ID)) + '] ' + LTRIM(RTRIM(TBL_PROJECT_GENERAL.DESCRIPTION)) AS PROJECT,
	MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID, 
	ISNULL(MICADEBUG.dbo.TBL_OVERTIMEREQ.REMARKS, '') AS REMARKS, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_DATE, 
	ISNULL(MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_TIME,'') AS OT_TIME, 
	MICADEBUG.dbo.TBL_STATUS.DESCRIPTION AS STATUSDESC, 
        MICADEBUG.dbo.TBL_OVERTIMEREQ.STATUS, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID + '-' + ISNULL(dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(dbo.TBL_EMP.LASTNAME, '') AS EMPLOYEE_NAME, 
	ISNULL(dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(dbo.TBL_EMP.LASTNAME, '') AS EMPLOYEE, 
	MICADEBUG.dbo.TBL_CARD_TR.CARD_IN, MICADEBUG.dbo.TBL_CARD_TR.CARD_OUT, 
        LTRIM(RTRIM(MICADEBUG.dbo.TBL_PROJECT_GENERAL.JOBCODE_ID)) AS JOBCODE_ID,
	LTRIM(RTRIM(TBL_PROJECT_GENERAL.CLIENT_JOBCODE)) AS CLIENT_JOBCODE,
	MICADEBUG.dbo.TBL_CARD_TR.REG, 
	MICADEBUG.dbo.TBL_OVERTIMEREQ.SECT_ID, 
	MICADEBUG.dbo.TBL_SECTION.DEPT_ID,
	ISNULL(APPROVALSECT,'') AS APPROVALSECT, 
	ISNULL(TBL_OVERTIMEREQ.CATEGORY_ID,'') AS CATEGORY_ID, 	
	ISNULL(APPROVALDEPT,'') AS APPROVALDEPT,
	ISNULL(TBL_SECTION.SECT_NAME ,'') AS SECT_NAME,
	ISNULL(TBL_EMP.POSITION_ID,'') AS POS_ID,
	DEPT_NAME, 
	ISNULL(REQUESTED_DATE,'') AS REQUESTED_DATE,
	ISNULL(B.FIRSTNAME, '') + ' ' + ISNULL(B.LASTNAME, '') AS APP_MAN,
	ISNULL(C.FIRSTNAME, '') + ' ' + ISNULL(C.LASTNAME, '') AS APP_FIRST,
	APPROVEDDEPT_DATE,
	APPROVEDSECT_DATE
FROM    MICADEBUG.dbo.TBL_OVERTIMEREQ 
INNER JOIN MICADEBUG.dbo.TBL_PROJECT_GENERAL ON MICADEBUG.dbo.TBL_PROJECT_GENERAL.JOBCODE_ID = MICADEBUG.dbo.TBL_OVERTIMEREQ.JOBCODE_ID AND TBL_PROJECT_GENERAL.CLIENT_JOBCODE = TBL_OVERTIMEREQ.CLIENT_JOBCODE 
			AND MICADEBUG.dbo.TBL_OVERTIMEREQ.CATEGORY_ID=MICADEBUG.dbo.TBL_PROJECT_GENERAL.CATEGORY_ID 
INNER JOIN dbo.TBL_EMP ON MICADEBUG.dbo.TBL_OVERTIMEREQ.EMP_ID = dbo.TBL_EMP.EMP_ID
INNER JOIN MICADEBUG.dbo.TBL_SECTION ON dbo.TBL_EMP.SECT_ID = MICADEBUG.dbo.TBL_SECTION.SECT_ID 
INNER JOIN MICADEBUG.dbo.TBL_DEPT ON MICADEBUG.dbo.TBL_SECTION.DEPT_ID = MICADEBUG.dbo.TBL_DEPT.DEPT_ID 
LEFT OUTER JOIN MICADEBUG.dbo.TBL_STATUS ON MICADEBUG.dbo.TBL_OVERTIMEREQ.STATUS = MICADEBUG.dbo.TBL_STATUS.STATUS 
LEFT OUTER JOIN MICADEBUG.dbo.TBL_CARD_TR ON dbo.TBL_EMP.EMP_ID = MICADEBUG.dbo.TBL_CARD_TR.EMP_ID AND MICADEBUG.dbo.TBL_OVERTIMEREQ.OT_DATE = MICADEBUG.dbo.TBL_CARD_TR.TR_DATE
LEFT JOIN TBL_EMP B ON B.EMP_ID = TBL_OVERTIMEREQ.APPROVALDEPT
LEFT JOIN TBL_EMP C ON C.EMP_ID = TBL_OVERTIMEREQ.APPROVALSECT

GO
/****** Object:  View [dbo].[V_PROJECTCATEGORY]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_PROJECTCATEGORY]
AS
SELECT	DISTINCT ISNULL(TBL_PROJECT_SEC.CLIENT_JOBCODE,'') AS CLIENT_JOBCODE, 
	TBL_PROJECT_SEC.JOBCODE_ID, 
	TBL_PROJECT_SEC.SECT_ID, 
	ISNULL(TBL_PROJECT_FWBS.STARTDATE,'') AS STARTDATE, 
        ISNULL(TBL_PROJECT_FWBS.ENDDATE,'') AS ENDDATE, 
	ISNULL(TBL_PROJECT_FWBS.MHPLAN,0) AS MHPLAN, 
	TBL_PROJECT_SEC.STARTDATE_ACTUAL, 
        TBL_PROJECT_SEC.ENDDATE_ACTUAL, 
	TBL_PROJECT_SEC.MHACTUAL, 
	TBL_SECTION.SECT_NAME, 
	TBL_PROJECT_FWBS.KEYPERSON + ' ' + LTRIM(RTRIM(ISNULL(TBL_EMP.FIRSTNAME,'') + ' ' +  ISNULL(TBL_EMP.LASTNAME,''))) AS EMPLOYEE_NAME, 
	isnull(TBL_PROJECT_FWBS.KEYPERSON,'') AS EMP_ID,
	ISNULL(TBL_PROJECT_FWBS.CATEGORY_ID,'') AS CATEGORY_ID,
	SUBSTRING(LTRIM(RTRIM(ISNULL(TBL_CATEGORY.CATEGORY_ID,''))),5,4) + '-' + ISNULL(CATEGORY_NAME ,'')AS CATEGORY_NAME
FROM    MICADEBUG.dbo.TBL_PROJECT_SEC LEFT JOIN
        TBL_PROJECT_FWBS ON TBL_PROJECT_FWBS.CLIENT_JOBCODE = TBL_PROJECT_SEC.CLIENT_JOBCODE AND 
	TBL_PROJECT_FWBS.JOBCODE_ID = TBL_PROJECT_SEC.JOBCODE_ID AND TBL_PROJECT_SEC.SECT_ID = TBL_PROJECT_FWBS.SECT_ID 
	LEFT JOIN  TBL_SECTION ON TBL_PROJECT_SEC.SECT_ID = TBL_SECTION.SECT_ID 
	LEFT JOIN TBL_CATEGORY ON TBL_PROJECT_FWBS.CATEGORY_ID = TBL_CATEGORY.CATEGORY_ID 
	LEFT JOIN TBL_EMP ON TBL_PROJECT_FWBS.KEYPERSON = TBL_EMP.EMP_ID








GO
/****** Object:  View [dbo].[V1_DEPTSECT]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V1_DEPTSECT]
AS
SELECT     dbo.TBL_DEPT.DEPT_ID, dbo.TBL_DEPT.DEPT_NAME, dbo.TBL_SECTION.SECT_ID, dbo.TBL_SECTION.SECT_NAME
FROM         dbo.TBL_DEPT INNER JOIN
                      dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID


GO
/****** Object:  View [dbo].[vw_compareActiveDirectory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vw_compareActiveDirectory]
AS

select	b.EMP_ID as EMP_ID_MICA
		,a.Description as EMP_ID_AD
		,b.FIRSTNAME as [FIRSTNAME]
		,b.LASTNAME as [LASTNAME]
		,b.FULL_NAME as NAME_MICA
		,a.DisplayName as NAME_AD
		,a.Department as DEPARTMENT_AD
		,c.SECT_NAME as DEPARTMENT_MICA
		,a.JobTitle as POSITION_NAME
		,a.Email
		,'Full Name' as Remarks
from TBL_ACTIVE_DIRECTORY a
inner join TBL_EMP b on a.Description = b.EMP_ID
inner join PFNATT.dbo.TBL_SECTION c on b.SECT_ID = c.SECT_ID
where a.DisplayName != b.FULL_NAME
AND b.STATUS = 1
union
select	b.EMP_ID as EMP_ID_MICA
		,a.Description as EMP_ID_AD
		,b.FIRSTNAME as [FIRSTNAME]
		,b.LASTNAME as [LASTNAME]
		,b.FULL_NAME as NAME_MICA
		,a.DisplayName as NAME_AD
		,a.Department as DEPARTMENT_AD
		,c.SECT_NAME as DEPARTMENT_MICA
		,a.JobTitle as POSITION_NAME
		,a.Email
		,'Employee Id' as Remarks
from TBL_ACTIVE_DIRECTORY a
inner join TBL_EMP b on a.DisplayName = b.FULL_NAME
inner join PFNATT.dbo.TBL_SECTION c on b.SECT_ID = c.SECT_ID
where a.Description != b.EMP_ID
AND b.STATUS = 1
union
select	b.EMP_ID as EMP_ID_MICA
		,a.Description as EMP_ID_AD
		,b.FIRSTNAME as [FIRSTNAME]
		,b.LASTNAME as [LASTNAME]
		,b.FULL_NAME as NAME_MICA
		,a.DisplayName as NAME_AD
		,a.Department as DEPARTMENT_AD
		,c.SECT_NAME as DEPARTMENT_MICA
		,a.JobTitle as POSITION_NAME
		,a.Email
		,'Department' as Remarks
from PFNATT.dbo.TBL_ACTIVE_DIRECTORY a
inner join PFNATT.dbo.TBL_EMP b on a.DisplayName = b.FULL_NAME and a.Description = b.EMP_ID
inner join PFNATT.dbo.TBL_SECTION c on b.SECT_ID = c.SECT_ID
where CHARINDEX(a.Department, c.SECT_NAME) = 0
AND b.STATUS = 1




GO
/****** Object:  View [dbo].[vwActivitiesList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from vwActivitiesList
CREATE VIEW [dbo].[vwActivitiesList]
AS

SELECT	a.ACT_ID AS ID, a.ACT_DESC AS NAME
FROM	TBL_ACTIVITIES a


GO
/****** Object:  View [dbo].[vwBusinessLineList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from vwBusinessLineList
CREATE VIEW [dbo].[vwBusinessLineList]
AS

SELECT	a.BL_id AS ID, a.BusinessLine AS NAME
FROM	TBL_BUSINESSLINE a


GO
/****** Object:  View [dbo].[vwCountryList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from vwCountryList
CREATE VIEW [dbo].[vwCountryList]
AS

SELECT	a.COUNTRY_ID AS ID, a.COUNTRY_NAME AS NAME
FROM	TBL_COUNTRY a


GO
/****** Object:  View [dbo].[vwDepartmentList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from vwDepartmentList
CREATE VIEW [dbo].[vwDepartmentList]
AS

SELECT	a.SECT_ID AS DEPARTMENT_ID, a.SECT_NAME AS DEPARTMENT_NAME
FROM	PFNATT.dbo.TBL_SECTION a
GO
/****** Object:  View [dbo].[vwDivisionList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from vwDivisionList
CREATE VIEW [dbo].[vwDivisionList]
AS

SELECT	a.DEPT_ID AS DIVISION_ID, a.DEPT_NAME AS DIVISION_NAME
FROM	PFNATT.dbo.TBL_DEPT a
GO
/****** Object:  View [dbo].[vwEducationList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from vwEducationList
CREATE VIEW [dbo].[vwEducationList]
AS

SELECT	a.EDU_ID AS ID, a.EDU_DESC AS NAME
FROM	TBL_EDU_TYPE a


GO
/****** Object:  View [dbo].[vwEmployeeKKSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwEmployeeKTPSearcher
CREATE VIEW [dbo].[vwEmployeeKKSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		c.SECT_NAME AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, d.DEPT_NAME AS DIVISION_NAME, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME, KK_NUMBER
FROM	PFNATT.dbo.TBL_EMP a
		INNER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID



GO
/****** Object:  View [dbo].[vwEmployeeKTPSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--select * from vwEmployeeKTPSearcher
CREATE VIEW [dbo].[vwEmployeeKTPSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		c.SECT_NAME AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, d.DEPT_NAME AS DIVISION_NAME, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME, IDNUMBER, IDNUMBER_EXPIRE
FROM	PFNATT.dbo.TBL_EMP a
		INNER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID


GO
/****** Object:  View [dbo].[vwEmployeeList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwEmployeeList]
AS
SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME, STATUS,
case
  when STATUS = 0 then 'RESIGN'
  when status = 1 then 'ACTIVE'
end as EMP_STS,
substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME
FROM	PFNATT.dbo.TBL_EMP a
LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID

GO
/****** Object:  View [dbo].[vwEmployeeList2]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vwEmployeeList2]
AS
SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME, STATUS,
case
  when STATUS = 0 then 'RESIGN'
  when status = 1 then 'ACTIVE'
end as EMP_STS,
a.SECT_ID,
substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME
FROM	PFNATT.dbo.TBL_EMP a
LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID



GO
/****** Object:  View [dbo].[vwEmployeeLoanSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwEmployeeLoanSearcher]
AS

SELECT	a.EMPLOYEE_ID, b.FULL_NAME, a.BANK_NAME, a.START_DATE, a.END_DATE, a.AMOUNT, a.DURATION, a.INSTALLMENT,
		a.PAYMENT, a.PAID_OFF_DATE, a.AS_OF_DATE, a.PAID_OFF_ATTACHMENT,
		b.POSITION_ID, c.POSITION_NAME AS POSITION_NAME, b.SECT_ID AS DEPARTMENT_ID, 
		substring(d.SECT_NAME,CHARINDEX('-',d.SECT_NAME)+2,LEN(d.SECT_NAME)) AS DEPARTMENT_NAME, 
		d.DEPT_ID AS DIVISION_ID, substring(e.DEPT_NAME,CHARINDEX('-',e.DEPT_NAME)+2,LEN(e.DEPT_NAME)) AS DIVISION_NAME, 
		b.STATUS AS STATUS_ID, CASE b.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
FROM	TBL_LOAN a
		INNER JOIN TBL_EMP b ON a.EMPLOYEE_ID = b.EMP_ID
		INNER JOIN PFNATT.dbo.TBL_POSITION c ON b.POSITION_ID = c.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION d ON b.SECT_ID = d.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT e ON d.DEPT_ID = e.DEPT_ID
		LEFT OUTER JOIN TBL_GENERAL_TYPE f ON a.BANK_ID = f.TYPE_ID AND f.TYPE = 'BankType'
WHERE	a.IS_DELETED = 0


GO
/****** Object:  View [dbo].[vwEmployeePassportSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwEmployeePassportSearcher
CREATE VIEW [dbo].[vwEmployeePassportSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		c.SECT_NAME AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, d.DEPT_NAME AS DIVISION_NAME, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME, PASSPORT, PASSPORTV
FROM	PFNATT.dbo.TBL_EMP a
		INNER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID



GO
/****** Object:  View [dbo].[vwEmployeeReportAgeSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






--select * from vwEmployeeReportAgeSearcher
CREATE VIEW [dbo].[vwEmployeeReportAgeSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, d.DEPT_NAME AS DIVISION_NAME, a.JOIN_DATE, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
		(SELECT TOP 1 COMPANY FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC) AS LAST_COMPANY,
		a.SALARY_GRADE
FROM	PFNATT.dbo.TBL_EMP a
		INNER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID





GO
/****** Object:  View [dbo].[vwEmployeeReportStatusSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwEmployeeReportStatusSearcher
CREATE VIEW [dbo].[vwEmployeeReportStatusSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		c.SECT_NAME AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, d.DEPT_NAME AS DIVISION_NAME, a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
		a.SALARY_GRADE
FROM	PFNATT.dbo.TBL_EMP a
		INNER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
		LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'



GO
/****** Object:  View [dbo].[vwEmployeeSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--select * from vwEmployeeSearcher
CREATE VIEW [dbo].[vwEmployeeSearcher]
AS

SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, 
		substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS  DIVISION_NAME, a.STATUS AS STATUS_ID,
		CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME, a.SALARY_GRADE,
		f.GROUP_NAME
FROM	PFNATT.dbo.TBL_EMP a
		LEFT JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
		LEFT JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
		LEFT JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
		left join PFNATT.dbo.TBL_EMP_GROUP_MEMBER e on a.EMP_ID = e.EMP_ID and e.YEARTR = YEAR(GETDATE()) and e.MONTHTR = MONTH(GETDATE())
		left join PFNATT.dbo.TBL_EMP_GROUP f on e.GROUP_ID = f.GROUP_ID






GO
/****** Object:  View [dbo].[vwEmployeeTrainingSearcher]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwEmployeeTrainingSearcher
CREATE VIEW [dbo].[vwEmployeeTrainingSearcher]
AS

SELECT	a.EMPLOYEE_ID, b.FULL_NAME, a.SUBJECT, a.OBJECTIVE, a.PROVIDER, a.START_DATE, a.END_DATE, a.CURRENCY, a.AMOUNT, a.PLACE,
		b.POSITION_ID, c.POSITION_NAME AS POSITION_NAME, b.SECT_ID AS DEPARTMENT_ID, 
		d.SECT_NAME AS DEPARTMENT_NAME, d.DEPT_ID AS DIVISION_ID, e.DEPT_NAME AS DIVISION_NAME, b.STATUS AS STATUS_ID,
		CASE b.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
FROM	TBL_EMP_TRAINING a
		INNER JOIN TBL_EMP b ON a.EMPLOYEE_ID = b.EMP_ID
		INNER JOIN PFNATT.dbo.TBL_POSITION c ON b.POSITION_ID = c.POSITION_ID
		INNER JOIN PFNATT.dbo.TBL_SECTION d ON b.SECT_ID = d.SECT_ID
		INNER JOIN PFNATT.dbo.TBL_DEPT e ON d.DEPT_ID = e.DEPT_ID
WHERE	IS_DELETED = 0



GO
/****** Object:  View [dbo].[vwEmployeeTypeList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from vwEmployeeTypeList
CREATE VIEW [dbo].[vwEmployeeTypeList]
AS

SELECT	a.EMP_TYPE_ID AS EMPLOYEE_TYPE_ID, a.TYPE_NAME AS EMPLOYEE_TYPE_NAME
FROM	TBL_EMP_TYPE a

GO
/****** Object:  View [dbo].[vwFamilyRelationList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from vwFamilyRelationList
CREATE VIEW [dbo].[vwFamilyRelationList]
AS

SELECT	a.REL_ID AS ID, a.REL_DESC AS NAME
FROM	TBL_FAM_REL a



GO
/****** Object:  View [dbo].[vwFaskesList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwFaskesList]
AS
SELECT     CODE, NAME, ADDRESS
FROM         dbo.TBL_FASKES

GO
/****** Object:  View [dbo].[vwGradeClassificationList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwGradeClassificationList
CREATE VIEW [dbo].[vwGradeClassificationList]
AS

SELECT	a.GRADECLASS_ID AS GRADE_CLASSIFICATION_ID, RTRIM(LTRIM(a.GRADE_POS)) + ' - ' + a.GRADE_CLASSIFICATION AS GRADE_CLASSIFICATION_NAME
FROM	TBL_EMP_GRADE_CLASS a




GO
/****** Object:  View [dbo].[vwGradeList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from vwGradeList
CREATE VIEW [dbo].[vwGradeList]
AS

SELECT	a.GRADE_ID AS GRADE_ID, a.GRADE_ID + ' - ' + a.GRADE_NAME AS GRADE_NAME
FROM	TBL_GRADE a


GO
/****** Object:  View [dbo].[vwGroupList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * from vwGradeClassificationList
CREATE VIEW [dbo].[vwGroupList]
AS

SELECT	LTRIM(RTRIM(a.GROUP_ID)) AS GROUP_ID, a.GROUP_NAME AS GROUP_NAME
FROM	TBL_EMP_GROUP a




GO
/****** Object:  View [dbo].[vwLocationList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from vwLocationList
CREATE VIEW [dbo].[vwLocationList]
AS

SELECT	a.LOC_ID AS LOCATION_ID, a.LOC_NAME AS LOCATION_NAME
FROM	TBL_LOCATION a

GO
/****** Object:  View [dbo].[vwMaritalStatusList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from vwMaritalStatusList
CREATE VIEW [dbo].[vwMaritalStatusList]
AS

SELECT	a.MAR_STAT_ID AS ID, a.DESCRIPTION AS NAME
FROM	TBL_MAR_STAT a


GO
/****** Object:  View [dbo].[vwOvertimeHeader]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwOvertimeHeader]
AS
SELECT	dbo.TBL_EMP.EMP_ID
		, isnull(dbo.TBL_EMP.EMP_TYPE_ID, ' ') as EMP_TYPE
		, isnull(dbo.TBL_EMP_TYPE.TYPE_NAME, ' ') as TYPE_NAME
		, ISNULL(dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(dbo.TBL_EMP.LASTNAME, '') AS Name
		, isnull(dbo.TBL_EMP.GRADE, ' ') as GRADE
		, isnull(dbo.TBL_GRADE.GRADE_NAME, ' ') as GRADE_NAME
		, isnull(dbo.TBL_EMP.POSITION_ID, ' ') as POSITION_ID
		, isnull(dbo.TBL_POSITION.POSITION_NAME, ' ') as POSITION_NAME
		, isnull(dbo.TBL_GRADE.GRADE_ID, ' ') + '/' + isnull(dbo.TBL_EMP.EMP_TYPE_ID, ' ') AS EMP_STATUS
		, isnull(dbo.TBL_EMP.SECT_ID,' ') as SECT_ID
		, isnull(dbo.TBL_SECTION.SECT_NAME, ' ') + ' / ' + ISNULL(dbo.TBL_SECTION.GEC_DEPT, '') AS SECT_NAME
		, isnull(dbo.TBL_SECTION.DEPT_ID, ' ') as DEPT_ID
		, isnull(dbo.TBL_DEPT.DEPT_NAME, ' ') as DEPT_NAME
FROM	dbo.TBL_POSITION 
RIGHT OUTER JOIN dbo.TBL_EMP ON dbo.TBL_POSITION.POSITION_ID = dbo.TBL_EMP.POSITION_ID 
LEFT OUTER JOIN dbo.TBL_GRADE ON dbo.TBL_EMP.GRADE = dbo.TBL_GRADE.GRADE_ID 
LEFT OUTER JOIN dbo.TBL_LOCATION ON dbo.TBL_EMP.LOC_ID = dbo.TBL_LOCATION.LOC_ID 
LEFT OUTER JOIN dbo.TBL_DEPT 
INNER JOIN dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID ON dbo.TBL_EMP.SECT_ID = dbo.TBL_SECTION.SECT_ID 
LEFT OUTER JOIN dbo.TBL_EMP_TYPE ON dbo.TBL_EMP.EMP_TYPE_ID = dbo.TBL_EMP_TYPE.EMP_TYPE_ID


GO
/****** Object:  View [dbo].[vwPositionList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from vwPositionList
CREATE VIEW [dbo].[vwPositionList]
AS

SELECT	a.POSITION_ID, a.POSITION_NAME
FROM	PFNATT.dbo.TBL_POSITION a where STATUS = '1'

GO
/****** Object:  View [dbo].[vwReligionList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from vwReligionList
CREATE VIEW [dbo].[vwReligionList]
AS

SELECT	a.RELIGION_ID AS ID, a.RELIGION_NAME AS NAME
FROM	TBL_RELIGION a



GO
/****** Object:  View [dbo].[vwTbl_Overtime]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vwTbl_Overtime]
AS
SELECT [REQUEST_ID]
      ,[EMP_ID]
      ,[JOBCODE_ID]
      ,[OT_DATE]
      ,[OT_TIME]
      ,[OT_REVISED]
      ,[REMARKS]
      ,[STATUS]
      ,[APPROVALSECT]
      ,[APPROVALDEPT]
      ,[PROCESS]
      ,[CLIENT_JOBCODE]
      ,[SECT_ID]
      ,[LOC_ID]
      ,[APPROVED_OTTIME]
      ,[REQUESTED_DATE]
      ,[REQUESTED_BY]
      ,[APPROVEDSECT_DATE]
      ,[APPROVEDDEPT_DATE]
      ,[CATEGORY_ID]
      ,[FWBS]
      ,[FIRSTRATER]
      ,[SECONDRATER]
      ,[APP_TYPE]
  FROM [MICADEBUG].[dbo].[TBL_OVERTIMEREQ]



GO
ALTER TABLE [dbo].[TBL_TIMESHEET]  WITH CHECK ADD  CONSTRAINT [FK_dbo_TBL_TIMESHEET_1] FOREIGN KEY([EMP_ID])
REFERENCES [dbo].[TBL_EMP] ([EMP_ID])
GO
ALTER TABLE [dbo].[TBL_TIMESHEET] CHECK CONSTRAINT [FK_dbo_TBL_TIMESHEET_1]
GO
ALTER TABLE [dbo].[TBL_UNIT]  WITH CHECK ADD  CONSTRAINT [FK_TBL_UNIT_TBL_PLANT] FOREIGN KEY([ID_PLANT])
REFERENCES [dbo].[TBL_PLANT] ([ID_PLANT])
GO
ALTER TABLE [dbo].[TBL_UNIT] CHECK CONSTRAINT [FK_TBL_UNIT_TBL_PLANT]
GO
/****** Object:  StoredProcedure [dbo].[GetAuthority]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAuthority]
	@EmpId varchar(10)
AS
BEGIN
	 SELECT USER_ID , MODUL_ID , AUTHORITY , GROUP_ID
     FROM TBL_USER_MS
     where dbo.TBL_USER_MS.USER_ID = @EmpId
END

GO
/****** Object:  StoredProcedure [dbo].[GetDiffLeaveDate]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDiffLeaveDate]
@StartDate datetime, @EndDate datetime, @EmpId varchar(10) 
AS
BEGIN
	Select dbo.Funct_LeaveDiffDays(@StartDate,@EndDate,@EmpId)
END

GO
/****** Object:  StoredProcedure [dbo].[GetOrgChartMember]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetOrgChartMember] 
	@Group varchar(50),@Group_Detail varchar(50)
AS
BEGIN
	SELECT  POSITION_NAME, EMP_ID, EMP_NAME, EMP_ID + ' ' + EMP_NAME AS NAME, PICTURE_NAME
	FROM TBL_ORG_CHART
	WHERE CHART_GROUP = @Group AND CHART_GROUP_DETAIL = @Group_Detail
	ORDER BY SEQUENCE
END

GO
/****** Object:  StoredProcedure [dbo].[GetSameBirthDate]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSameBirthDate]
AS
BEGIN
	SELECT a.EMP_ID, a.BIRTHDATE
	FROM TBL_FAMILY a inner join TBL_FAMILY_TEMP_EXCEL b ON a.EMP_ID = b.PATIENT_ID and a.BIRTHDATE = b.BIRTH_DATE
	GROUP BY a.EMP_ID, a.BIRTHDATE HAVING COUNT(a.BIRTHDATE) > 1
END
GO
/****** Object:  StoredProcedure [dbo].[Stp_Alert_Employee_EndOfContractHRIS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_Alert_Employee_EndOfContractHRIS]
WITH EXEC AS CALLER
AS
-- EOC : END OF CONTRACT DATE

DECLARE @RATER_EMP_ID AS CHAR(4)
DECLARE @APP_RATER AS INT
DECLARE @STRSQL AS VARCHAR(8000)
DECLARE @APPROVER AS CHAR(4)
DECLARE @MESSAGE AS VARCHAR(8000)

-- SUB MESSAGE
DECLARE @EMP_ID AS VARCHAR(6)
DECLARE @EOC_ID AS VARCHAR(6)
DECLARE @EMPNAME AS VARCHAR(200)
DECLARE @DEPTNAME AS VARCHAR(100)
DECLARE @EMP_TYPE_ID AS VARCHAR(4)
DECLARE @EOC_DATE AS VARCHAR(20)
DECLARE @NOTIFY2MONTH AS DATETIME
DECLARE @NOTIFY1MONTH AS DATETIME

--DECLARE @ROLE_APV AS VARCHAR(20)

-- TAMBAHAN
DECLARE @ONBEHALF_OF AS VARCHAR(4)
DECLARE @ONBEHALF_ID AS VARCHAR(4)

-- EMAIL PURPOSE
DECLARE @EMAILTo AS VARCHAR(200)
DECLARE @SUBMESSAGE AS VARCHAR(max)
DECLARE @SUBJECT AS VARCHAR(200)
DECLARE @DESCRIPTION AS VARCHAR(max)
DECLARE @GENDER AS CHAR(1)
DECLARE @TIME AS INT
DECLARE @GREETINGS AS VARCHAR(200)

DECLARE @POSITION VARCHAR(512)
DECLARE @FIRSTMONTH DATETIME
DECLARE @SECONDMONTH DATETIME
DECLARE @NUMBER VARCHAR(10)

DECLARE CC Cursor For 
 SELECT 'EOC'
OPEN CC
FETCH NEXT FROM CC INTO 
@EOC_ID
WHILE @@FETCH_STATUS = 0 
BEGIN

SET @FIRSTMONTH = DATEADD(MONTH,3,GETDATE())
SET @SECONDMONTH = DATEADD(MONTH,4,GETDATE())

	--GET APPROVER
			DECLARE QQ Cursor For 
					SELECT EMP_ID FROM TBL_DELEGATION_ALERT WHERE DEPT='EOCHRIS'
PRINT 'LOOP QQ'
			OPEN QQ
			FETCH NEXT FROM QQ INTO
					@APPROVER
			WHILE @@FETCH_STATUS = 0 
			BEGIN
	
						-- LOOP TO GET EMAIL
						PRINT 'GET EMAIL ADDRESS'

						SET @EMAILTo = (SELECT EMAIL FROM TBL_EMAIL WHERE EMP_ID = @APPROVER)
							--- TAMBAHAN NULL EMAIL
	          IF  RTRIM(LTRIM(@EMAILTo)) ='' 
	          BEGIN
		          SET @EMAILTo ='mica.system@jgc-indonesia.com'
	          END
	
	          IF ISNULL(@EMAILTo,'') = '' SET @EMAILTo = 'mica.system@Jgc-indonesia.com'
						PRINT 'GET EMAIL ADDRESS'
						PRINT @EMAILTO

						SET @SUBJECT = '[HRIS Alert] Employee End of Contract in ' + CONVERT(CHAR(4), @FIRSTMONTH, 100) + CONVERT(CHAR(4), @FIRSTMONTH, 120)  + ' and '  + CONVERT(CHAR(4), @SECONDMONTH, 100) + CONVERT(CHAR(4), @SECONDMONTH, 120)

						SET @GENDER = (SELECT SEX FROM TBL_EMP WHERE EMP_ID = @APPROVER)
						SET @TIME = (SELECT DATEPART(hh, GETDATE()) )

						IF @TIME BETWEEN 0 AND 11 SET @GREETINGS = 'Good Morning '
						IF @TIME BETWEEN 11 AND 15 SET @GREETINGS = 'Good Day '
						IF @TIME BETWEEN 15 AND 23 SET @GREETINGS = 'Good Afternoon '
						
						IF @GENDER = 'M' SET @GREETINGS = @GREETINGS + 'Mr. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',<br><br>' 
						IF @GENDER = 'F' SET @GREETINGS = @GREETINGS + 'Ms. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',<br><br>' 
						
						SET @MESSAGE = 'Please kindly note for employee end of contract date within next 3 and 4 months as follow : <br><br>'

						SET @MESSAGE = @MESSAGE + '<table border="1" style="border-collapse:collapse; border:1px solid #000000">
						<thead >
						<tr >
						<th style="padding:5px;border:1px solid black">NO</th>
						<th style="padding:5px;border:1px solid black">ID</th>
						<th style="padding:5px;border:1px solid black">NAME</th>
						<th style="padding:5px;border:1px solid black">POSITION</th>
						<th style="padding:5px;border:1px solid black">DEPARTMENT</th>
						<th style="padding:5px;border:1px solid black">TYPE</th>
						<th style="padding:5px;border:1px solid black">END DATE CONTRACT</th>
						</tr>
						</thead>'

						 --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						SELECT @APPROVER, @EMAILTo, @SUBJECT, @MESSAGE
						DECLARE DD CURSOR FOR 
							-- KONDISI SUBMESSAGE
											
							SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, b.POSITION_NAME AS POSITION_NAME, 
									c.SECT_NAME AS DEPARTMENT_NAME, 
									a.EMP_TYPE_ID, convert(VARCHAR(20), a.END_DATE_CONTRACT, 106) END_DATE_CONTRACT	
							FROM	PFNATT.dbo.TBL_EMP a
									LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
									LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
									LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
									LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
									LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
							WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
									and a.STATUS = 1 and ((Month(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Month(GETDATE()) and YEAR(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Year(Getdate())) OR (Month(DATEADD(month, -4, a.END_DATE_CONTRACT)) = Month(GETDATE()) and YEAR(DATEADD(month, -4, a.END_DATE_CONTRACT)) = YEAR(GETDATE())))
							ORDER BY a.END_DATE_CONTRACT, a.EMP_ID 
						SET @SUBMESSAGE = ''
						OPEN DD
						FETCH NEXT FROM DD INTO 
							@NUMBER,@EMP_ID, @EMPNAME, @POSITION, @DEPTNAME, @EMP_TYPE_ID,@EOC_DATE

						WHILE @@FETCH_STATUS = 0 
						BEGIN
						
					SET @SUBMESSAGE = @SUBMESSAGE +  '<tr>
					    <td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@NUMBER+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EMP_ID+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@EMPNAME+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@POSITION+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@DEPTNAME+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EMP_TYPE_ID+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EOC_DATE+'</td>
					</tr>'

					-----##############################################
							FETCH NEXT FROM DD INTO 
								@NUMBER,@EMP_ID, @EMPNAME, @POSITION, @DEPTNAME, @EMP_TYPE_ID,@EOC_DATE

						END
						CLOSE DD
						DEALLOCATE DD
 						--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

					
						IF @Submessage <> '' 
						BEGIN
							SET @SUBMESSAGE = @SUBMESSAGE + '</table>'

							SET @Description = @GREETINGS + @Message +'<br>' + @Submessage + '<br><br>'
							Set @Description = @Description + 'You could download the excel version of employee list above <a href="http://jindsrv0202/PersonalData/report/DownloadHRISAlertEOC" >here</a> <br><br>' 
							Set @Description = @Description + 'Please ignore this if you have noted this HRIS Alert' + '<br><br>' 
							Set @description = @Description + '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'+ '<br><br>' 
							Set @description = @Description + 'This is a HRIS Alert System. '+ '<br><br>'  
							
              --PRINT 'INPUT KOK'
							--//Select Getdate(), 'akhmad.baihaqy@jgc-indonesia.com', @EmailTo, @Subject, @Description, 0, 1
							INSERT INTO WORKFLOW.dbo.TBL_EMAIL_SENDER
							(EMAIL_FROM, EMAIL_TO, EMAIL_SUBJECT, EMAIL_BODY)
							VALUES
							('Hris.System@jgc-indonesia.com', @EmailTo, @Subject, @Description)
						END


							-----------------------------------
							----- LANJUTAN LOOP QQ HERE -------
							-----------------------------------
													
							FETCH NEXT FROM QQ INTO @APPROVER
							END --END QQ
							CLOSE QQ
							DEALLOCATE QQ

	FETCH NEXT FROM CC INTO @EOC_ID
END 
CLOSE CC
DEALLOCATE CC

GO
/****** Object:  StoredProcedure [dbo].[Stp_Alert_Employee_EndOfContractHRISRemainder]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_Alert_Employee_EndOfContractHRISRemainder]
WITH EXEC AS CALLER
AS
-- EOC : END OF CONTRACT DATE

DECLARE @RATER_EMP_ID AS CHAR(4)
DECLARE @APP_RATER AS INT
DECLARE @STRSQL AS VARCHAR(8000)
DECLARE @APPROVER AS CHAR(4)
DECLARE @MESSAGE AS VARCHAR(8000)

-- SUB MESSAGE
DECLARE @EMP_ID AS VARCHAR(6)
DECLARE @EOC_ID AS VARCHAR(6)
DECLARE @EMPNAME AS VARCHAR(200)
DECLARE @DEPTNAME AS VARCHAR(100)
DECLARE @EMP_TYPE_ID AS VARCHAR(4)
DECLARE @EOC_DATE AS VARCHAR(20)
DECLARE @NOTIFY2MONTH AS DATETIME
DECLARE @NOTIFY1MONTH AS DATETIME

--DECLARE @ROLE_APV AS VARCHAR(20)

-- TAMBAHAN
DECLARE @ONBEHALF_OF AS VARCHAR(4)
DECLARE @ONBEHALF_ID AS VARCHAR(4)

-- EMAIL PURPOSE
DECLARE @EMAILTo AS VARCHAR(200)
DECLARE @SUBMESSAGE AS VARCHAR(max)
DECLARE @SUBJECT AS VARCHAR(200)
DECLARE @DESCRIPTION AS VARCHAR(max)
DECLARE @GENDER AS CHAR(1)
DECLARE @TIME AS INT
DECLARE @GREETINGS AS VARCHAR(200)

DECLARE @POSITION VARCHAR(512)
DECLARE @FIRSTMONTH DATETIME
DECLARE @SECONDMONTH DATETIME
DECLARE @NUMBER VARCHAR(10)

DECLARE CC Cursor For 
 SELECT 'EOC'
OPEN CC
FETCH NEXT FROM CC INTO 
@EOC_ID
WHILE @@FETCH_STATUS = 0 
BEGIN

SET @FIRSTMONTH = DATEADD(MONTH,1,GETDATE())
SET @SECONDMONTH = DATEADD(MONTH,2,GETDATE())

	--GET APPROVER
			DECLARE QQ Cursor For 
					SELECT EMP_ID FROM TBL_DELEGATION_ALERT WHERE DEPT='EOCHRIS'
PRINT 'LOOP QQ'
			OPEN QQ
			FETCH NEXT FROM QQ INTO
					@APPROVER
			WHILE @@FETCH_STATUS = 0 
			BEGIN
	
						-- LOOP TO GET EMAIL
						PRINT 'GET EMAIL ADDRESS'

						SET @EMAILTo = (SELECT EMAIL FROM TBL_EMAIL WHERE EMP_ID = @APPROVER)
							--- TAMBAHAN NULL EMAIL
	          IF  RTRIM(LTRIM(@EMAILTo)) ='' 
	          BEGIN
		          SET @EMAILTo ='mica.system@jgc-indonesia.com'
	          END
	
	          IF ISNULL(@EMAILTo,'') = '' SET @EMAILTo = 'mica.system@Jgc-indonesia.com'
						PRINT 'GET EMAIL ADDRESS'
						PRINT @EMAILTO

						SET @SUBJECT = '[REMAINDER - HRIS Alert] Employee End of Contract in Next Month (' + CONVERT(CHAR(4), @FIRSTMONTH, 100) + CONVERT(CHAR(4), @FIRSTMONTH, 120)  + ') and '  + CONVERT(CHAR(4), @SECONDMONTH, 100) + CONVERT(CHAR(4), @SECONDMONTH, 120)

						SET @GENDER = (SELECT SEX FROM TBL_EMP WHERE EMP_ID = @APPROVER)
						SET @TIME = (SELECT DATEPART(hh, GETDATE()) )

						IF @TIME BETWEEN 0 AND 11 SET @GREETINGS = 'Good Morning '
						IF @TIME BETWEEN 11 AND 15 SET @GREETINGS = 'Good Day '
						IF @TIME BETWEEN 15 AND 23 SET @GREETINGS = 'Good Afternoon '
						
						IF @GENDER = 'M' SET @GREETINGS = @GREETINGS + 'Mr. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',<br><br>' 
						IF @GENDER = 'F' SET @GREETINGS = @GREETINGS + 'Ms. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',<br><br>' 
						
						SET @MESSAGE = 'Please kindly note for employee end of contract date within next two months as follows : <br><br>'

						SET @MESSAGE = @MESSAGE + '<table border="1" style="border-collapse:collapse; border:1px solid #000000">
						<thead >
						<tr >
						<th style="padding:5px;border:1px solid black">NO</th>
						<th style="padding:5px;border:1px solid black">ID</th>
						<th style="padding:5px;border:1px solid black">NAME</th>
						<th style="padding:5px;border:1px solid black">POSITION</th>
						<th style="padding:5px;border:1px solid black">DEPARTMENT</th>
						<th style="padding:5px;border:1px solid black">TYPE</th>
						<th style="padding:5px;border:1px solid black">END DATE CONTRACT</th>
						</tr>
						</thead>'

						 --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						SELECT @APPROVER, @EMAILTo, @SUBJECT, @MESSAGE
						DECLARE DD CURSOR FOR 
							-- KONDISI SUBMESSAGE
											
							SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, b.POSITION_NAME AS POSITION_NAME, 
									c.SECT_NAME AS DEPARTMENT_NAME, 
									a.EMP_TYPE_ID, convert(VARCHAR(20), a.END_DATE_CONTRACT, 106) END_DATE_CONTRACT	
							FROM	PFNATT.dbo.TBL_EMP a
									LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
									LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
									LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
									LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
									LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
							WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
									and a.STATUS = 1 and ((Month(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Month(GETDATE()) and YEAR(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Year(Getdate())) OR (Month(DATEADD(month, -2, a.END_DATE_CONTRACT)) = Month(GETDATE()) and YEAR(DATEADD(month, -2, a.END_DATE_CONTRACT)) = YEAR(GETDATE())))
							ORDER BY a.END_DATE_CONTRACT, a.EMP_ID 
						SET @SUBMESSAGE = ''
						OPEN DD
						FETCH NEXT FROM DD INTO 
							@NUMBER,@EMP_ID, @EMPNAME, @POSITION, @DEPTNAME, @EMP_TYPE_ID,@EOC_DATE

						WHILE @@FETCH_STATUS = 0 
						BEGIN
						
					SET @SUBMESSAGE = @SUBMESSAGE +  '<tr>
					    <td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@NUMBER+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EMP_ID+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@EMPNAME+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@POSITION+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black">'+@DEPTNAME+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EMP_TYPE_ID+'</td>
						<td style="padding-left:5px;padding-right:5px;border:1px solid black;text-align:center">'+@EOC_DATE+'</td>
					</tr>'

					-----##############################################
							FETCH NEXT FROM DD INTO 
								@NUMBER,@EMP_ID, @EMPNAME, @POSITION, @DEPTNAME, @EMP_TYPE_ID,@EOC_DATE

						END
						CLOSE DD
						DEALLOCATE DD
 						--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

					
						IF @Submessage <> '' 
						BEGIN
							SET @SUBMESSAGE = @SUBMESSAGE + '</table>'

							SET @Description = @GREETINGS + @Message +'<br>' + @Submessage + '<br><br>'
							Set @Description = @Description + 'You could download the excel version of employee list above <a href="http://jindsrv0202/PersonalData/report/DownloadHRISAlertEOCRem" >here</a> <br><br>' 
							Set @Description = @Description + 'Please ignore this if you have noted this HRIS Alert' + '<br><br>' 
							Set @description = @Description + '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'+ '<br><br>' 
							Set @description = @Description + 'This is a HRIS Alert System. '+ '<br><br>'  
							
              --PRINT 'INPUT KOK'
							--//Select Getdate(), 'akhmad.baihaqy@jgc-indonesia.com', @EmailTo, @Subject, @Description, 0, 1
							INSERT INTO WORKFLOW.dbo.TBL_EMAIL_SENDER
							(EMAIL_FROM, EMAIL_TO, EMAIL_SUBJECT, EMAIL_BODY)
							VALUES
							('Hris.System@jgc-indonesia.com', @EmailTo, @Subject, @Description)
						END


							-----------------------------------
							----- LANJUTAN LOOP QQ HERE -------
							-----------------------------------
													
							FETCH NEXT FROM QQ INTO @APPROVER
							END --END QQ
							CLOSE QQ
							DEALLOCATE QQ

	FETCH NEXT FROM CC INTO @EOC_ID
END 
CLOSE CC
DEALLOCATE CC

GO
/****** Object:  StoredProcedure [dbo].[Stp_AlertJINDUserResignation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT TOP 10 START_DATE,* FROM TBL_EMP ORDER BY START_DATE DESC


-- exec Stp_AlertJINDUserResignation '0419'
-- SELECT * FROM TBL_LOG_EMAIL
-- [STP_UPDEMPRESIGN]
CREATE PROCEDURE [dbo].[Stp_AlertJINDUserResignation]
@EMP_ID varchar(6)
WITH EXEC AS CALLER
AS
DECLARE @RATER_EMP_ID AS CHAR(4)
DECLARE @APP_RATER AS INT
DECLARE @STRSQL AS VARCHAR(8000)
DECLARE @APPROVER AS CHAR(4)
DECLARE @MESSAGE AS VARCHAR(8000)

-- SUB MESSAGE
DECLARE @EMP_NAME AS VARCHAR(200)
DECLARE @EMP_NAME2 AS VARCHAR(200)

--PAR APPROVER
DECLARE @ASSIGN_ID AS INT
DECLARE @APP_STATUS AS INT
DECLARE @DEPT_ID AS VARCHAR(6)
DECLARE @DEPT_NAME AS VARCHAR(100)
DECLARE @DIV_ID AS VARCHAR(4)
DECLARE @AUTH AS INT
DECLARE @ROLE_APV AS VARCHAR(20)

-- EMAIL PURPOSE
DECLARE @EMAILTo AS VARCHAR(200)
DECLARE @SUBMESSAGE AS VARCHAR(8000)
DECLARE @SUBJECT AS VARCHAR(200)
DECLARE @DESCRIPTION AS VARCHAR(8000)
DECLARE @GENDER AS CHAR(1)
DECLARE @TIME AS INT
DECLARE @GREETINGS AS VARCHAR(200)



DECLARE CC Cursor For 

-- AMBIL DATA YANG BLM DI REQUEST
SELECT  EMP_ID, dbo.Funct_Name(EMP_ID)AS EMP_NAME ,dbo.Funct_Name2(EMP_ID) AS EMP_NAME2,SECT_ID FROM TBL_EMP WHERE EMP_ID=@EMP_ID

OPEN CC
FETCH NEXT FROM CC INTO 
@EMP_ID,@EMP_NAME,@EMP_NAME2,@DEPT_ID
WHILE @@FETCH_STATUS = 0 
BEGIN
	-- ID APPROVER
	SET @APPROVER =@DEPT_ID
	SET @ROLE_APV ='IT Member'

	SET @EMAILTo = (SELECT EMAIL FROM TBL_EMAIL_ITD WHERE ACTION = 'JINDRESIGN')
		--- TAMBAHAN NULL EMAIL
	IF  RTRIM(LTRIM(@EMAILTo)) ='' 
	BEGIN
		SET @EMAILTo ='mica.system@jgc-indonesia.com'
	END
	
	IF ISNULL(@EMAILTo,'') = '' SET @EMAILTo = 'mica.system@Jgc-indonesia.com'

	SET @SUBJECT = '[JIND Resign User] Removal of PC/Lotus/User Login - ' +  @EMP_NAME2 + ' - MICA Automatic Reminder '
	--SET @SUBJECT = '[JIND Resign User-- TESTING ONLY] Removal of PC/Lotus/User Login - ' +  @EMP_NAME2+ ' - MICA TESTING '

	SET @GENDER = (SELECT SEX FROM TBL_EMP WHERE EMP_ID = @APPROVER)
	SET @TIME = (SELECT DATEPART(hh, GETDATE()) )

	IF @TIME BETWEEN 0 AND 11 SET @GREETINGS = 'Good Morning '
	IF @TIME BETWEEN 11 AND 15 SET @GREETINGS = 'Good Day '
	IF @TIME BETWEEN 15 AND 23 SET @GREETINGS = 'Good Afternoon '

	SET @DEPT_NAME = (SELECT SECT_NAME FROM TBL_SECTION WHERE SECT_ID = @DEPT_ID )

	--IF @GENDER = 'M' SET @GREETINGS = @GREETINGS + 'Mr. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',' + char(13) + Char(10)+ char(13) + Char(10)
	--IF @GENDER = 'F' SET @GREETINGS = @GREETINGS + 'Ms. ' + (SELECT dbo.FUnct_Name(@APPROVER)) + ',' + char(13) + Char(10)+ char(13) + Char(10)
	
	SET @MESSAGE =  char(13) + Char(10)+'Please kindly execute this notification for removal of JIND System (PC/Lotus/User Login) : ' + char(13) + Char(10)
	SET @SUBMESSAGE = ''
	SET @SUBMESSAGE = @SUBMESSAGE + 'Emp ID : ' + @EMP_ID + char(13) + Char(10)
	SET @SUBMESSAGE = @SUBMESSAGE + 'Employee Name : ' + @EMP_NAME +  char(13) + Char(10)
	SET @SUBMESSAGE = @SUBMESSAGE + 'Department / Division : ' + @DEPT_NAME +  char(13) + Char(10)+ char(13) + Char(10)
	SET @SUBMESSAGE = @SUBMESSAGE + '>>>>>>>>> REMOVAL For PC / Lotus Notes Email / User Login <<<<<<<<<< ' + char(13) + Char(10)+ char(13) + Char(10)
	SET @SUBMESSAGE = @SUBMESSAGE + 'Please note that user already resigned/pension/demob '	+ char(13) + Char(10) + char(13) + Char(10)
	SET @SUBMESSAGE = @SUBMESSAGE + 'Thanks for your attention '
 

	 --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SELECT @APPROVER, @EMAILTo, @SUBJECT, @MESSAGE
	
	
	
 	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LOGIC DETAIL APPROVAL REQUEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	IF @Submessage <> '' 
	BEGIN
		SET @Description = @GREETINGS + @Message + Char(13) + Char(10) + @Submessage + Char(13) + Char(10) + Char(13) + Char(10) 
		Set @Description = @Description + 'Please ignore this alert if the request already DONE.' + Char(13) + Char(10) + Char(13) + Char(10) 
		Set @description = @Description + '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'+ Char(13) + Char(10) 
		Set @description = @Description + 'This is a MICA Alert System. '+ Char(13) + Char(10) 
		Set @description = @Description + 'Please mailto mica.system@Jgc-indonesia.com if any suggestion of this notification. '
		
		
		INSERT INTO [TBL_LOG_EMAIL]
		(EmailDate, EmailFrom, EmailTo, Subject, Description, Process, MsgType)
		VALUES
		(Getdate(), 'mica.system@jgc-indonesia.com', @EmailTo, @Subject, @Description, 0, 1)
		-- TO DEBUG / TRIAL
		SELECT Getdate(), 'mica.system@jgc-indonesia.com', @EmailTo, @Subject, @Description--, "0", "1"
-- 		SELECT * FROM TBL_LOG_EMAIL
	END
	FETCH NEXT FROM CC INTO @EMP_ID,@EMP_NAME,@EMP_NAME2,@DEPT_ID
END 
CLOSE CC
DEALLOCATE CC

GO
/****** Object:  StoredProcedure [dbo].[stp_ApprovalLeaveReq]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_ApprovalLeaveReq]
@APPROVED_BY varchar(4), @AUTHORITY varchar(2), @STATUS char(3), @RATER int, @LSTREQ_ID varchar(5000)
WITH EXEC AS CALLER
AS
-- Additional check 4 HRD Manager, although as admin .. keep approval as first rater
IF @AUTHORITY = 5 AND @APPROVED_BY = '2126' SET @RATER = 1
IF @AUTHORITY = 5 AND @APPROVED_BY = '2278' SET @RATER = 1

IF @AUTHORITY = 5 OR @AUTHORITY = 10 SET @AUTHORITY = 2
IF @RATER = 5 OR @RATER = 10 SET @RATER = 2

PRINT @APPROVED_BY + ', ' + convert(char(1), @rater)

DECLARE @EMP_ID AS CHAR(4)
DECLARE @DEPT_ID AS VARCHAR(6)
DECLARE @START_DATE AS DATETIME
DECLARE @END_DATE AS DATETIME
DECLARE @CLASS_TYPE AS CHAR(2)
DECLARE @1ST_RATER AS VARCHAR(6)
DECLARE @2ND_RATER AS VARCHAR(6)

DECLARE @SQL AS VARCHAR(8000)
DECLARE @REGID varCHAR(20)
IF @LSTREQ_ID <> '' Select ''
BEGIN
WHILE LTRIM(RTRIM(@LSTREQ_ID)) <> ''
	BEGIN
		IF CHARINDEX(',',@LSTREQ_ID) > 0
		BEGIN
			SET @REGID =  LEFT(@LSTREQ_ID,CHARINDEX(',',@LSTREQ_ID)-1)	
			SET @LSTREQ_ID = SUBSTRING(@LSTREQ_ID,CHARINDEX(',',@LSTREQ_ID)+1,LEN(@LSTREQ_ID)-CHARINDEX(',',@LSTREQ_ID))

			--print '''' + replace('2345,3456',',',''',''') + ''''
		END
		ELSE
		BEGIN
			SET @REGID = @LSTREQ_ID
			SET @LSTREQ_ID = ''
		END

		SET @EMP_ID = RIGHT(RTRIM(LTRIM(@REGID)),4)
    SET @DEPT_ID = dbo.Funct_GetEmpDeptCode(@EMP_ID)
    IF @APPROVED_BY = '2278' AND @DEPT_ID <> 'N120' SET @RATER = 2
    
		SET @START_DATE = (SELECT ISNULL(START_DATE,'1/1/1900') AS START_DATE FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @REGID AND STATUS < 4)
		SET @END_DATE = (SELECT ISNULL(END_DATE,'1/1/1900') AS END_DATE FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @REGID AND STATUS < 4)
		SET @CLASS_TYPE = (SELECT CLASS_TYPE FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @REGID AND STATUS < 4)
    SELECT @1ST_RATER = APP_DEPT_BY, @2ND_RATER = APP_DIV_BY FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @REGID AND STATUS < 4

		IF @STATUS = 'APP'
		BEGIN
      IF @1ST_RATER = @2ND_RATER
        UPDATE TBL_LEAVE_REQUEST
				SET DEPT_DATE = GETDATE(), DEPT_MGR = @APPROVED_BY, 
        DIV_DATE = GETDATE(), DIV_MGR = @APPROVED_BY,
				STATUS = 3 
				WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID))
    
			IF @RATER = 1
				UPDATE TBL_LEAVE_REQUEST
				SET DEPT_DATE = GETDATE(), 
				DEPT_MGR = @APPROVED_BY, 
				STATUS = 2 
				WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID)) AND STATUS = '1'
	
	
			IF @RATER = 2 --OR @APPROVED_BY = '9030'
			BEGIN
				--IF @RATER 1 Still Empty
				IF EXISTS (SELECT ISNULL(DEPT_MGR,'1/1/1900' ) AS DEPT_DATE FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID)) AND STATUS = '1')
					UPDATE TBL_LEAVE_REQUEST 
					SET DEPT_DATE = GETDATE(), 
					DEPT_MGR = @APPROVED_BY, 
					STATUS = 2 
					WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID)) AND STATUS = '1'

				UPDATE TBL_LEAVE_REQUEST 
				SET DIV_DATE = GETDATE(), 
				DIV_MGR = @APPROVED_BY, 
				STATUS = 3 
				WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID)) AND STATUS = '2'


	
				--DECLARE @CLASS_TYPE AS CHAR(2)
				--SET @CLASS_TYPE = (Select Class_type from tbl_Leave_request where Reques_id = @Request_id)		

				--Close aja since now 4 Nov 2008
				--IF @CLASS_TYPE IN ('AL') 
				--	Exec Stp_TimeAttendanceConversion_Leave @EMP_ID, @START_DATE, @END_DATE, @CLASS_TYPE 

			END
		END
		IF @STATUS = 'REJ' 
		BEGIN
			IF @RATER = 1
			--UPDATE TBL_LEAVE_REQUEST SET REMARKS = 'Rejected By Sect/Dept Manager' , Status = 4,
			--WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID))
				UPDATE TBL_LEAVE_REQUEST
				SET REMARKS = 'Rejected By Sect/Dept Manager' ,
				DEPT_DATE = GETDATE(), 
				DEPT_MGR = @APPROVED_BY, 
				STATUS = 4
				WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID))  		
--				AND STATUS < 100
				AND STATUS <= 1
			IF @RATER = 2 --OR  @APPROVED_BY = '9030'
			--UPDATE TBL_LEAVE_REQUEST SET REMARKS = 'Rejected By Sect/Dept Manager' , Status = 4,
			--WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID))
				UPDATE TBL_LEAVE_REQUEST
				SET REMARKS = 'Rejected By Sect/Dept Manager' ,
				DIV_DATE = GETDATE(), 
				DIV_MGR = @APPROVED_BY, 
				STATUS = 4
				WHERE LEAVE_REQUEST = RTRIM(LTRIM(@REGID)) 
--				AND STATUS < 100
				AND STATUS <= 2
		END
		IF @STATUS = 'PTA' --Process To Attendance 
		BEGIN
			Exec Stp_TimeAttendanceConversion_Leave @EMP_ID, @START_DATE, @END_DATE, @CLASS_TYPE 
		END

			
	END


END

GO
/****** Object:  StoredProcedure [dbo].[stp_ApprovalOvertimeReq_WithLog]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_ApprovalOvertimeReq_WithLog]
(@APPROVED_BY varchar(4), @AUTHORITY varchar(2), @STATUS char(3), @RATER int, @LSTREQ_ID varchar(8000), @APPROVAL_REMARK varchar(100))
WITH 
EXECUTE AS CALLER
AS
DECLARE @SQL AS VARCHAR(8000)
DECLARE @APPROVAL_DATE AS DATETIME
DECLARE @REGID varCHAR(20)


IF @LSTREQ_ID <> '' Select ''
BEGIN
WHILE LTRIM(RTRIM(@LSTREQ_ID)) <> ''
	BEGIN
		IF CHARINDEX(',',@LSTREQ_ID) > 0
		BEGIN
			SET @REGID =  LEFT(@LSTREQ_ID,CHARINDEX(',',@LSTREQ_ID)-1)	
			SET @LSTREQ_ID = SUBSTRING(@LSTREQ_ID,CHARINDEX(',',@LSTREQ_ID)+1,LEN(@LSTREQ_ID)-CHARINDEX(',',@LSTREQ_ID))
			--print '''' + replace('2345,3456',',',''',''') + ''''
		END
		ELSE
		BEGIN
			SET @REGID = @LSTREQ_ID
			SET @LSTREQ_ID = ''
		END

		SET @APPROVAL_DATE = GETDATE()

		IF @STATUS = 'APP'
		BEGIN
			IF @RATER = 1
				IF EXISTS (SELECT ISNULL(APPROVEDSECT_DATE,'1/1/1900' ) AS APPROVEDSECT_DATE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '1' AND FIRSTRATER=SECONDRATER)
					 UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
				   SET APPROVEDSECT_DATE = @APPROVAL_DATE, APPROVALSECT = @APPROVED_BY, 
				   APPROVEDDEPT_DATE = @APPROVAL_DATE, APPROVALDEPT = @APPROVED_BY, STATUS = 3
				   WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '1'
				ELSE
					 UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
					 SET APPROVEDSECT_DATE = @APPROVAL_DATE, 
					 APPROVALSECT = @APPROVED_BY, 
					 STATUS = 2 
					 WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '1'
		
			IF @RATER = 2
			BEGIN
				--IF @RATER 1 Still Empty
				IF EXISTS (SELECT ISNULL(APPROVEDSECT_DATE,'1/1/1900' ) AS APPROVEDSECT_DATE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '1')
					UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
					SET APPROVEDSECT_DATE = @APPROVAL_DATE, 
					APPROVALSECT = @APPROVED_BY, 
					STATUS = 2 
					WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '1'

				UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
				SET APPROVEDDEPT_DATE = @APPROVAL_DATE, 
				APPROVALDEPT = @APPROVED_BY, 
				STATUS = 3 
				WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) AND STATUS = '2'
			END
		END
		ELSE IF @STATUS = 'REJ' 
		BEGIN

			IF @RATER = 1
				UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
				SET APPROVEDSECT_DATE = @APPROVAL_DATE, 
					APPROVALSECT = @APPROVED_BY, 
					STATUS = 4,
					REMARKS = 'Rejected By Sect/Dept Manager'
				WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) 
			

			IF @RATER = 2
				UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ 
				SET APPROVEDDEPT_DATE = @APPROVAL_DATE, 
					APPROVALDEPT = @APPROVED_BY, 
					STATUS = 4,
					REMARKS = 'Rejected By Sect/Dept Manager'
				WHERE REQUEST_ID = RTRIM(LTRIM(@REGID)) 
			
			

		END
		
		-- INSERT TO TBL_OVERTIMEREQ_APP_LOG
		BEGIN
			exec MICADEBUG.dbo.stp_InsertOTApproval_Log @REGID, @RATER, @APPROVED_BY, @APPROVAL_DATE, @APPROVAL_REMARK
		END

	END


END

GO
/****** Object:  StoredProcedure [dbo].[STP_COPYEMPDATEREHIRE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[STP_COPYEMPDATEREHIRE]
@Emp_id char(4), @New_Emp_Id char(4), @StartDate datetime, @EndDate datetime, @MODIFY_BY char(4)
WITH EXEC AS CALLER
AS
IF RTRIM(LTRIM(@New_Emp_Id)) <> ''
BEGIN
	 INSERT INTO TBL_EMP (
     EMP_ID    ,EMP_TYPE_ID    ,FIRSTNAME    ,LASTNAME    ,GRADE    ,SECT_ID    ,SECT_EFFECTIVE_DATE
    ,POSITION_ID    ,POSITION_EFFECTIVE_DATE    ,FRONTTITLE    ,LASTTITTLE    ,BIRTHDATE    ,BIRTHLOC
    ,BIRTH_CERTIFICATE    ,SEX    ,MAR_STAT_ID    ,RELIGION_ID    ,ADDRESS_TYPE_ID    ,ADDRESS    ,CITY
    ,REGION    ,POSTALCODE    ,COUNTRY    ,EMAIL    ,NATIONALITY    ,LOC_ID    ,PHONE    ,TEST1    ,TEST2
    ,JOIN_DATE    ,PERMANENT_START_DATE    ,START_DATE_CONTRACT    ,END_DATE_CONTRACT    ,START_DATE
    ,EXP_DATE    ,EXP_DATE1    ,END_DATE    ,STATUS    ,ANUAL_LEAVE    ,BANKNAME    ,BRANCH    ,ACC_NUMBER
    ,ACC_NAME    ,BANK_ATTACHMENT    ,MOVECOMPANY    ,PENSION    ,DIED    ,OTHERS    ,MOVETO    ,MOVEADDRESS
    ,CIPNAME    ,CIPTELP    ,CIPEMAIL    ,REMARKS    ,NEWEMP_ID    ,SALARY_GRADE
    ,SALARY_GRADE_EFFECTIVE_DATE    ,WIN_LOGIN    ,LOCAL_EMP_ID    ,NEW_EMP_TYPE    ,HP    ,HP1
    ,MIDDLENAME    ,PADDRESS_TYPE_ID    ,PADDRESS    ,PCITY    ,PPOSTALCODE    ,PREGION    ,PCOUNTRY
    ,IDNUMBER    ,IDNUMBER_EXPIRE    ,IDNUMBER_ATTACHMENT    ,KK_NUMBER    ,KITAS_NUMBER    ,KITAS_EXPIRE
    ,KITAS_POSITION    ,KITAS_ATTACHMENT    ,NPWP_NUMBER    ,NPWP_ADDRESS    ,NPWP_ATTACHMENT    ,JAMSOSTEK_NO
    ,JAMSOSTEK_START_DATE    ,JAMSOSTEK_STATUS_ID    ,JAMSOSTEK_ATTACHMENT    ,PASSPORT    ,PASSPORTV
    ,PASSPORT_ATTACHMENT    ,EMERGENCY_PHONE    ,EMERGENCY_PHONE_NAME    ,EMERGENCY_PHONE_RELATION_ID
    ,BLOOD_TYPE    ,TAX_STATUS_ID    ,PREFER_SEND_LETTER_ID    ,BPJS_KESEHATAN_NO    ,BPJS_KESEHATAN_STATUS_ID
    ,BPJS_KESEHATAN_START_DATE    ,BPJS_KESEHATAN_ATTACHMENT    ,BPJS_KESEHATAN_FASKES_CODE    ,EXT
    ,MODIFY_BY    ,RESIGNED_BY    ,MODIFIED    ,FULL_NAME    ,REF_ID    ,CREATION_DATE    ,CREATION_BY
  ) 
  SELECT @NEW_EMP_ID AS EMP_ID,	'' AS EMP_TYPE_ID,	[FIRSTNAME], 	[LASTNAME], 	[GRADE], 	[SECT_ID],  [SECT_EFFECTIVE_DATE], [POSITION_ID], [POSITION_EFFECTIVE_DATE],
	[FRONTTITLE], 	[LASTTITTLE], 	[BIRTHDATE], 	[BIRTHLOC], [BIRTH_CERTIFICATE],	[SEX], 	[MAR_STAT_ID], 	[RELIGION_ID], 	[ADDRESS_TYPE_ID], [ADDRESS], 
	[CITY], 	[REGION], 	[POSTALCODE], 	[COUNTRY], 	[EMAIL], 	[NATIONALITY], 	[LOC_ID], 	[PHONE], null AS TEST1, [TEST2],	[JOIN_DATE], [PERMANENT_START_DATE], @StartDate, @EndDate,
	ISNULL(@StartDate,'1/1/1900') AS STARTDATE,	ISNULL(@EndDate,'1/1/1900') AS EXP_DATE,	'1/1/1900' AS EXP_DATE1,	'1/1/1900' AS END_DATE,
	1 AS STATUS,	[ANUAL_LEAVE], 	[BANKNAME], 	[BRANCH], 	[ACC_NUMBER], 	[ACC_NAME], [BANK_ATTACHMENT],	0 AS MOVECOMPANY,	0 AS PENSION,
	0 AS DIED,	' ' AS OTHERS,	' ' AS MOVETO,	' ' AS MOVEADDRESS,	' ' AS CIPNAME,	' ' AS CIPTELP,	' ' AS CIPEMAIL,
	' ' AS REMARKS,	' ' AS NEWEMP_ID,	' ' AS SALARY_GRADE, [SALARY_GRADE_EFFECTIVE_DATE],	' ' AS WIN_LOGIN,	' ' AS LOCAL_EMP_ID,	' ' AS NEW_EMP_TYPE,
	[HP], [HP1],	[MIDDLENAME], [PADDRESS_TYPE_ID],	[PADDRESS],	[PCITY], [PPOSTALCODE], [PREGION], [PCOUNTRY],	[IDNUMBER],	[IDNUMBER_EXPIRE], [IDNUMBER_ATTACHMENT],
	[KK_NUMBER], [KITAS_NUMBER], [KITAS_EXPIRE], [KITAS_POSITION], [KITAS_ATTACHMENT], [NPWP_NUMBER],	[NPWP_ADDRESS],	[NPWP_ATTACHMENT], [JAMSOSTEK_NO],
	[JAMSOSTEK_START_DATE],	[JAMSOSTEK_STATUS_ID], [JAMSOSTEK_ATTACHMENT],	[PASSPORT],	[PASSPORTV], [PASSPORT_ATTACHMENT],	[EMERGENCY_PHONE], [EMERGENCY_PHONE_NAME], [EMERGENCY_PHONE_RELATION_ID], 
	[BLOOD_TYPE], [TAX_STATUS_ID], [PREFER_SEND_LETTER_ID], [BPJS_KESEHATAN_NO], [BPJS_KESEHATAN_STATUS_ID], [BPJS_KESEHATAN_START_DATE], [BPJS_KESEHATAN_ATTACHMENT], [BPJS_KESEHATAN_FASKES_CODE],
  [EXT],	@MODIFY_BY,	'' AS RESIGNED_BY,	GETDATE(),	'' AS FULL_NAME,	[REF_ID], GETDATE(), @MODIFY_BY
  FROM [PFNATT].[dbo].[TBL_EMP]	WHERE EMP_ID = @EMP_ID

	--copy family update by julian 5 Agustus 2014
	INSERT INTO TBL_FAMILY(EMP_ID, REL_ID, SNAME, GENDER, BIRTHDATE, BIRTH_CERTIFICATE_ATTACHMENT, OCCUPATION, REMARKS, UPDATED_BY, UPDATED_DATE, LIFE_STATUS_ID, LIFE_STATUS_NAME, MEDICAL_ID, MEDICAL_COVERED_ID, IS_DELETED)
	SELECT RTRIM(LTRIM(@New_Emp_Id)), REL_ID, SNAME, GENDER, BIRTHDATE, BIRTH_CERTIFICATE_ATTACHMENT, OCCUPATION, REMARKS, UPDATED_BY, UPDATED_DATE, LIFE_STATUS_ID, LIFE_STATUS_NAME, MEDICAL_ID, MEDICAL_COVERED_ID, IS_DELETED
	FROM TBL_FAMILY
	WHERE EMP_ID = @EMP_ID
	
	-- Copy Education
	INSERT INTO TBL_EDUCATION(EMP_ID, S_YEAR, GRAD_YEAR, INSTITUTE, MAJORING, LOCATION, EDU_ID, GRADE, REMARKS, TITLE_DEGREE, UPDATED_BY, ATTACHMENT, UPDATED_DATE, IS_DELETED)
	SELECT RTRIM(LTRIM(@New_Emp_Id)), S_YEAR, GRAD_YEAR, INSTITUTE, MAJORING, LOCATION, EDU_ID, GRADE, REMARKS, TITLE_DEGREE, UPDATED_BY, ATTACHMENT, UPDATED_DATE, IS_DELETED
	FROM TBL_EDUCATION WHERE EMP_ID = @Emp_id AND IS_DELETED = 0

	--update by julian 14 October 2014 join date must restart if old employee permanent
	IF EXISTS(SELECT 1 FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND EMP_TYPE_ID = 'RS')
	BEGIN
		UPDATE TBL_EMP
		SET JOIN_DATE = null
		WHERE EMP_ID = @NEW_EMP_ID
	END
  
  -- copy PPA, additional task when employee change their emp_id -- 11 dec 2014
  DECLARE @JOBCODE_ID AS VARCHAR(20)
  DECLARE @RESIGN_DATE AS DATETIME
  DECLARE @NEW_STARTDATE AS DATETIME
  
  IF EXISTS (SELECT '' FROM TBL_PPA_DURATION
             WHERE EMP_ID=@EMP_ID AND FLAG=1 AND END_DATE > GETDATE())
  BEGIN
      SELECT @RESIGN_DATE = END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID
      SELECT @NEW_STARTDATE = START_DATE FROM TBL_EMP WHERE EMP_ID = @New_Emp_Id
  
      DECLARE CC CURSOR FOR 
         SELECT JOBCODE_ID FROM TBL_PPA_DURATION WHERE EMP_ID = @EMP_ID AND FLAG = 1 AND END_DATE > GETDATE()
	    OPEN CC
	    FETCH NEXT FROM CC INTO @JOBCODE_ID

	    WHILE @@FETCH_STATUS = 0 
	    BEGIN
          INSERT INTO TBL_PPA_DURATION (EMP_ID  ,JOBCODE_ID ,CATEGORY_ID  ,FWBS  ,PPA_POS_ID  ,PPA_LEVEL_ID
                                        ,START_DATE  ,END_DATE  ,FLAG  ,PROCESS  ,PREV_JOBCODE) 
          SELECT @New_Emp_Id,JOBCODE_ID,CATEGORY_ID  ,FWBS  ,PPA_POS_ID  ,PPA_LEVEL_ID  ,@NEW_STARTDATE
          ,END_DATE  ,FLAG  ,PROCESS  ,PREV_JOBCODE
          FROM TBL_PPA_DURATION
          WHERE EMP_ID = @EMP_ID AND JOBCODE_ID = @JOBCODE_ID AND FLAG = 1 AND END_DATE > GETDATE()
          
          UPDATE TBL_PPA_DURATION
          SET FLAG = 0, END_DATE = @RESIGN_DATE
          WHERE EMP_ID = @EMP_ID AND JOBCODE_ID = @JOBCODE_ID AND FLAG = 1 AND END_DATE > GETDATE()
          
          UPDATE TBL_PPA_DURATION
          SET PARENT_ID = DURATION_ID
          WHERE EMP_ID = @New_Emp_Id AND JOBCODE_ID = @JOBCODE_ID AND FLAG = 1 AND END_DATE > GETDATE()
          
          FETCH NEXT FROM CC INTO @JOBCODE_ID
      END
      CLOSE CC
	    DEALLOCATE CC
  END
  -- end of copy PPA

	-- UPDATE ASSIGNMENT
	-- Request per tanggal March 30, 2007 Rizalino M. Iljas
	DECLARE @C_EMP_ID AS CHAR(4)
	DECLARE @C_ASSIGN_ID AS INT

	DECLARE CC CURSOR FOR SELECT ASSIGN_ID FROM TBL_EMP_ASSIGNMENT WHERE EMP_ID = @EMP_ID AND STATUS = 1
	OPEN CC
	FETCH NEXT FROM CC INTO @C_ASSIGN_ID

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		UPDATE TBL_EMP_ASSIGNMENT SET EMP_ID = @NEW_EMP_ID WHERE ASSIGN_ID = @C_ASSIGN_ID
		--SELECT @EMP_ID, @NEW_EMP_ID, @C_ASSIGN_ID 
		FETCH NEXT FROM CC INTO @C_ASSIGN_ID
	END
	CLOSE CC
	DEALLOCATE CC

END


GO
/****** Object:  StoredProcedure [dbo].[stp_deleteOvertimeReq]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_deleteOvertimeReq]
(@REQUEST_ID varchar(20), @REQUESTER varchar(4), @MSG char(100) OUTPUT)
WITH 
EXECUTE AS CALLER
AS
SET @MSG = ''
DECLARE @EMP_ID VARCHAR(4)
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @STATUS VARCHAR(4)
DECLARE @REMARKS VARCHAR(100)
DECLARE @PROCESS VARCHAR(4)
DECLARE @SECTID VARCHAR(4)
DECLARE @OT_DATE VARCHAR(12)
DECLARE @OT_TIME VARCHAR(12)
--DECLARE @REQUESTER VARCHAR(4)

-- Edit by Indra, 25-Mar-2013
-- HRD minta agar, OT yg status nya sdh Approved / Rejected, Admin masih bisa delete .. dikarenakan sesuatu hal
-- Employee tidak bisa delete OT yg status nya sdh Approved / Rejected
--IF EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND STATUS <> '3')
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID)
BEGIN
	--SELECT @EMP_ID = EMP_ID FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @JOBID = JOBCODE_ID FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @CLIENID = CLIENT_JOBCODE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @REMARKS = REMARKS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @PROCESS = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @SECTID = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @OT_DATE = CONVERT(VARCHAR(12),OT_DATE,106) FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @OT_TIME =  CONVERT(VARCHAR(12),OT_TIME,106) + ' ' + CONVERT(VARCHAR(12),OT_TIME,108) FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	
	--Edited by Jeremia
	SELECT	@EMP_ID = EMP_ID,
			@JOBID = JOBCODE_ID,
			@CLIENID = CLIENT_JOBCODE,
			@STATUS = STATUS,
			@REMARKS = REMARKS,
			@PROCESS = PROCESS,
			@SECTID = PROCESS,
			@OT_DATE = CONVERT(VARCHAR(12),OT_DATE,106),
			@OT_TIME = CONVERT(VARCHAR(12),OT_TIME,106) + ' ' + CONVERT(VARCHAR(12),OT_TIME,108)
	FROM MICADEBUG.dbo.TBL_OVERTIMEREQ 
	WHERE REQUEST_ID = @REQUEST_ID

	DELETE MICADEBUG.dbo.TBL_OVERTIMEREQ_APP_LOG WHERE REQUEST_ID = @REQUEST_ID
	DELETE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	
	EXEC MICADEBUG.dbo.STP_INSERTHISTORICALDATA 'DELETE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OT_TIME, ''
	SET @MSG = 'Request overtime Deleted !'
END
--ELSE
--	SET @MSG = 'Request overtime Already approved ! Request can not be deleted !'
-- End of Edit by Indra, 25-Mar-2013

GO
/****** Object:  StoredProcedure [dbo].[stp_deleteOvertimeReqNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[stp_deleteOvertimeReqNew]
(@REQUEST_ID varchar(20), @REQUESTER varchar(4))
--WITH 
--EXECUTE AS CALLER
AS
DECLARE @EMP_ID VARCHAR(4)
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @STATUS VARCHAR(4)
DECLARE @REMARKS VARCHAR(100)
DECLARE @PROCESS VARCHAR(4)
DECLARE @SECTID VARCHAR(4)
DECLARE @OT_DATE VARCHAR(12)
DECLARE @OT_TIME VARCHAR(12)
DECLARE @MSG char(100)

SET @MSG = ''
--DECLARE @REQUESTER VARCHAR(4)

-- Edit by Indra, 25-Mar-2013
-- HRD minta agar, OT yg status nya sdh Approved / Rejected, Admin masih bisa delete .. dikarenakan sesuatu hal
-- Employee tidak bisa delete OT yg status nya sdh Approved / Rejected
--IF EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND STATUS <> '3')
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID)
BEGIN
	--SELECT @EMP_ID = EMP_ID FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @JOBID = JOBCODE_ID FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @CLIENID = CLIENT_JOBCODE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @REMARKS = REMARKS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @PROCESS = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @SECTID = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @OT_DATE = CONVERT(VARCHAR(12),OT_DATE,106) FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	--SELECT @OT_TIME =  CONVERT(VARCHAR(12),OT_TIME,106) + ' ' + CONVERT(VARCHAR(12),OT_TIME,108) FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	
	--Edited by Jeremia
	SELECT	@EMP_ID = EMP_ID,
			@JOBID = JOBCODE_ID,
			@CLIENID = CLIENT_JOBCODE,
			@STATUS = STATUS,
			@REMARKS = REMARKS,
			@PROCESS = PROCESS,
			@SECTID = PROCESS,
			@OT_DATE = CONVERT(VARCHAR(12),OT_DATE,106),
			@OT_TIME = CONVERT(VARCHAR(12),OT_TIME,106) + ' ' + CONVERT(VARCHAR(12),OT_TIME,108)
	FROM MICADEBUG.dbo.TBL_OVERTIMEREQ 
	WHERE REQUEST_ID = @REQUEST_ID

	DELETE MICADEBUG.dbo.TBL_OVERTIMEREQ_APP_LOG WHERE REQUEST_ID = @REQUEST_ID
	DELETE FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	
	EXEC MICADEBUG.dbo.STP_INSERTHISTORICALDATA 'DELETE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OT_TIME, ''
	SET @MSG = 'Request overtime Deleted !'
END
--ELSE
--	SET @MSG = 'Request overtime Already approved ! Request can not be deleted !'
-- End of Edit by Indra, 25-Mar-2013

SELECT @MSG as Message


GO
/****** Object:  StoredProcedure [dbo].[Stp_DelLeaveRequest]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_DelLeaveRequest](@LEAVE_REQUEST varchar(20), @UPDATE_BY char(4))
AS
--DELETE FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST
DECLARE @MSG CHAR(100) 
DECLARE @CANCEL_ID AS INT
SET @MSG = ''

SET @CANCEL_ID = (SELECT MAX(STATUS) FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST)
SELECT @CANCEL_ID
IF ISNULL(@CANCEL_ID,0) < 100 SET @CANCEL_ID = 100
ELSE SET @CANCEL_ID = @CANCEL_ID + 1

--update by Ary ==> Update di lakukan untuk Leave yang belum di proses oleh HRD
IF NOT EXISTS(SELECT * FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND PROCESS = 1)
	OR EXISTS (SELECT * FROM TBL_USER_MS WHERE GROUP_ID = 'G000' AND AUTHORITY = 5 AND USER_ID = @UPDATE_BY)
BEGIN
  --IF EXISTS (SELECT * FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS <> '3' and status < 100) 
	   --BEGIN
			 UPDATE TBL_LEAVE_REQUEST
		 	 SET STATUS = @CANCEL_ID,
			 UPDATE_BY = @UPDATE_BY,
			 UPDATED_ON = getdate()
			 WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS < 100
			 
			 SET @MSG = 'Request Leave deleted !'
		 --END
	--ELSE
	  -- SET @MSG = 'Request leave Already approved ! Request can not be deleted !'
END
else
  SET @MSG = 'Request leave Already approved ! Request can not be deleted !'



GO
/****** Object:  StoredProcedure [dbo].[STP_EMPREHIRE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[STP_EMPREHIRE]

(
@EMP_ID AS CHAR(4),
@NEWEMP_ID AS VARCHAR(4),
@EMP_TYPE_ID AS CHAR(3),
@UPDATED_BY AS CHAR(4),
@START_DATE datetime,
@END_DATE datetime
)
as
	--Masukkan ke Historical data

	--EXEC STP_INSERTHISTORICALDATA 'UPDATE','EMPLOYEE',@RESIGNED_BY, @EMP_ID, '', '', '', '', @STATUS, '', '', '', '', ''

	declare @isActive varchar(5)

	select @isActive = STATUS from TBL_EMP WHERE EMP_ID = @EMP_ID

	IF @isActive = '1' 
	BEGIN
		UPDATE TBL_EMP
		SET 
		NEWEMP_ID = @NEWEMP_ID,
		NEW_EMP_TYPE = @EMP_TYPE_ID,
		TEST1 = '1', -- This is flag for FAD before set this employee InActive
		MODIFY_BY = @UPDATED_BY,
		MODIFIED = GETDATE()
		WHERE EMP_ID = @EMP_ID
	END
	ELSE IF @isActive = '0'
	BEGIN
		UPDATE TBL_EMP
		SET 
		NEWEMP_ID = @NEWEMP_ID,
		NEW_EMP_TYPE = @EMP_TYPE_ID,
		--TEST1 = '1', -- This is flag for FAD before set this employee InActive
		MODIFY_BY = @UPDATED_BY,
		MODIFIED = GETDATE()
		WHERE EMP_ID = @EMP_ID
	END

	-- RUN ALERT SYSTEM MICA
	--EXEC Stp_AlertJINDUserResignation @EMP_ID
	
	--COPY DATA
	EXEC STP_COPYEMPDATEREHIRE @EMP_ID, @NEWEMP_ID, @START_DATE, @END_DATE, @UPDATED_BY
	
	--SET NEW EMP_TYPE_ID
	UPDATE TBL_EMP
	SET 
	EMP_TYPE_ID = @EMP_TYPE_ID
	WHERE EMP_ID = @NEWEMP_ID


GO
/****** Object:  StoredProcedure [dbo].[stp_getTimeCardNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procEDURE [dbo].[stp_getTimeCardNew]
(
@TR_DATE DATETIME,
@EMP_ID VARCHAR(4),
@REQUESTER VARCHAR(4)
)
-- EXEC sp_getTimeCard '5/7/2006','0858','','','',''
AS
DECLARE @CARD_IN VARCHAR(5)
DECLARE @CARD_OUT VARCHAR(5)
DECLARE @JOBCODE VARCHAR(100)
DECLARE @MSG VARCHAR(100)
DECLARE @CARDIN INTEGER
DECLARE @CARDOUT INTEGER
DECLARE @LIMIT DATETIME
DECLARE @POS_ID VARCHAR(3)
SET DATEFIRST 1
SELECT @CARDIN = COUNT(*) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_IN IS NOT NULL
SELECT @CARDOUT = COUNT(*) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_OUT IS NOT NULL
SELECT @POS_ID = POSITION_ID FROM TBL_EMP WHERE EMP_ID =  @EMP_ID

IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = DATEADD(DAY,1,@TR_DATE) AND LOC_ID = '00')
	SET @LIMIT = DATEADD(DAY,1,@TR_DATE)
ELSE
BEGIN
	IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = DATEADD(DAY,2,@TR_DATE) AND LOC_ID = '00')
		SET @LIMIT = DATEADD(DAY,2,@TR_DATE)
	ELSE
		SET @LIMIT = DATEADD(DAY,3,@TR_DATE)
END

-- IF DATEPART(WEEKDAY,@TR_DATE) > 5 OR EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID IN (SELECT LOC_ID FROM TBL_LOCATION WHERE COUNTRY IN (SELECT COUNTRY FROM TBL_LOCATION WHERE LOC_ID = '00')))
--  	SET @LIMIT = DATEADD(DAY,-1,@TR_DATE)


IF DATEPART(WEEKDAY,@TR_DATE) = 5  
BEGIN
	IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = DATEADD(DAY,3,@TR_DATE) AND LOC_ID = '00')
	 	SET @LIMIT = DATEADD(DAY,3,@TR_DATE)
	ELSE
 		SET @LIMIT = DATEADD(DAY,4,@TR_DATE)
END
IF DATEPART(WEEKDAY,@TR_DATE) = 6
BEGIN
	IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = DATEADD(DAY,2,@TR_DATE) AND LOC_ID = '00')
	 	SET @LIMIT = DATEADD(DAY,2,@TR_DATE)
	ELSE
 		SET @LIMIT = DATEADD(DAY,3,@TR_DATE)
END
-- 	SET @LIMIT = DATEADD(DAY,2,@TR_DATE)

--* CLOSE BY ARY
-- IF @POS_ID IN ('OSB','OSD','OSS','OST')
-- BEGIN
-- 	SET @LIMIT = DATEADD(DAY,4,@TR_DATE)
-- END
--* END CLOSE

-- 
--  IF DAY(@TR_DATE) BETWEEN 1 AND 10 SET @LIMIT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/16/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
--  IF DAY(@TR_DATE) BETWEEN 11 AND 20 SET @LIMIT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/26/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
--  IF DAY(@TR_DATE) BETWEEN 21 AND 31 SET @LIMIT = CAST(CAST(MONTH(@TR_DATE)+1 AS VARCHAR) + '/6/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)

DECLARE @TodayStartTime DATETIME 
DECLARE @TodayEndTime DATETIME 
SET @TodayStartTime = convert(datetime, replace(CONVERT(VARCHAR(20), GETDATE(),111),'/','-') + ' 10:00:00') 
SET @TodayEndTime = convert(datetime, replace(CONVERT(VARCHAR(20), GETDATE(),111),'/','-') + ' 15:00:00')

IF ((@LIMIT < GETDATE() AND CONVERT(VARCHAR(12),@LIMIT,101) <> CONVERT(VARCHAR(12),GETDATE(),101)) 
OR ( CONVERT(VARCHAR(12),@LIMIT,101) = CONVERT(VARCHAR(12),GETDATE(),101) AND CONVERT(VARCHAR(5),GETDATE(),108) > '10:00')) --and  @POS_ID NOT IN ('OSB','OSD','OSS','OST')
AND NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_USER_MS WHERE USER_ID = @REQUESTER AND GROUP_ID = 'G000')
-- AND
--IF @LIMIT < GETDATE() --AND  @POS_ID NOT IN ('OSB','OSD','OSS','OST')
BEGIN
	print 'masuk'
	SET @MSG = 'Overtime Request Expired !! '
	SET @CARD_IN = ''
	SET @CARD_OUT = ''
	SELECT @JOBCODE = RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LEFT(LTRIM(RTRIM(CATEGORY_ID)),4) FROM TBL_PROJECT_GENERAL WHERE DEPT IN (SELECT DEPT_ID FROM V_EMPLOYEE WHERE EMP_ID = @EMP_ID)
END
ELSE IF (GETDATE() > @TodayEndTime
		 AND NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_USER_MS WHERE USER_ID = @REQUESTER AND GROUP_ID = 'G000')
		 )
	BEGIN
		print 'masuk ke 2'
		SET @MSG = 'Overtime cannot be proceed !! please input overtime for today before 03.00 pm'
		SET @CARD_IN = ''
		SET @CARD_OUT = ''
		SELECT @JOBCODE = RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LEFT(LTRIM(RTRIM(CATEGORY_ID)),4) FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE DEPT IN (SELECT DEPT_ID FROM MICADEBUG.dbo.V_EMPLOYEE WHERE EMP_ID = @EMP_ID)
	END
ELSE
BEGIN
	IF (@CARDIN = 0 AND @CARDOUT = 0) 
	BEGIN
		SET @MSG = 'Attendance Record For Date : [' + convert(varchar(12),@TR_DATE,106) + '] not retrieved yet ! Input Overtime Hours ONLY'
		SET @CARD_IN = ''
		SET @CARD_OUT = ''
		SELECT @JOBCODE = RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LEFT(LTRIM(RTRIM(CATEGORY_ID)),4) FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE DEPT IN (SELECT DEPT_ID FROM MICADEBUG.dbo.V_EMPLOYEE WHERE EMP_ID = @EMP_ID)
	END
	ELSE 	
	BEGIN
		IF @CARDIN = 0 OR @CARDOUT = 0
		BEGIN
			IF @CARDIN >= 1
				SELECT @CARD_IN = CONVERT(VARCHAR(5),CARD_IN,108) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_IN IS NOT NULL
			ELSE
				SET @CARD_IN = ''
		
			IF @CARDOUT >= 1
				SELECT @CARD_OUT = CONVERT(VARCHAR(5),CARD_OUT,108) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_OUT IS NOT NULL
			ELSE
				SET @CARD_OUT = ''
		END
		ELSE
		BEGIN
			SELECT @CARD_IN = CONVERT(VARCHAR(5),CARD_IN,108) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_IN IS NOT NULL
			SELECT @CARD_OUT = CONVERT(VARCHAR(5),CARD_OUT,108) FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND CARD_OUT IS NOT NULL
		END
	END
	SELECT @JOBCODE = RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LEFT(LTRIM(RTRIM(CATEGORY_ID)),4) FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE DEPT IN (SELECT DEPT_ID FROM MICADEBUG.dbo.V_EMPLOYEE WHERE EMP_ID = @EMP_ID)
	SET @MSG = ISNULL(@MSG,'')
	SET @CARD_IN = ISNULL(@CARD_IN,'')
	SET @CARD_OUT = ISNULL(@CARD_OUT,'')
END
SELECT @CARD_IN as CardIn ,@CARD_OUT as CardOut, @JOBCODE as JobCode, @MSG as Msg
GO
/****** Object:  StoredProcedure [dbo].[stp_InsertHistoricalData]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_InsertHistoricalData]
(
@TYPE VARCHAR(10),
@MODULE VARCHAR(50),
@USER_ID VARCHAR(4),
@F1 VARCHAR(100),
@F2 VARCHAR(100),
@F3 VARCHAR(100),
@F4 VARCHAR(100),
@F5 VARCHAR(100),
@F6 VARCHAR(100),
@F7 VARCHAR(100),
@F8 VARCHAR(100),
@F9 VARCHAR(100),
@F10 VARCHAR(100),
@COMPUTER_NAME VARCHAR(100)
)
AS

DECLARE @F1_OLD VARCHAR(100)
DECLARE @F2_OLD VARCHAR(100)
DECLARE @F3_OLD VARCHAR(100)
DECLARE @F4_OLD VARCHAR(100)
DECLARE @F5_OLD VARCHAR(100)
DECLARE @F6_OLD VARCHAR(100)
DECLARE @F7_OLD VARCHAR(100)
DECLARE @F8_OLD VARCHAR(100)
DECLARE @F9_OLD VARCHAR(100)
DECLARE @F10_OLD VARCHAR(100)


IF @TYPE = 'UPDATE'
BEGIN
	IF @MODULE = 'EMPLOYEE'
	BEGIN
		SET @F1_OLD = @F1 -- EMP_ID
		SELECT @F2_OLD = GRADE FROM TBL_EMP WHERE EMP_ID = @F1 -- GRADE
		SELECT @F3_OLD = POSITION_ID FROM TBL_EMP WHERE EMP_ID = @F1 -- POSITION 
		SELECT @F4_OLD = LOC_ID FROM TBL_EMP WHERE EMP_ID = @F1 -- LOCATION
		SELECT @F5_OLD = SALARY_GRADE FROM TBL_EMP WHERE EMP_ID = @F1 -- SALARY GRADE
		SELECT @F6_OLD = STATUS FROM TBL_EMP WHERE EMP_ID = @F1 -- STATUS
		SELECT @F7_OLD = SECT_ID FROM TBL_EMP WHERE EMP_ID = @F1 -- SECTION
		SELECT @F8_OLD = EMP_TYPE_ID FROM TBL_EMP WHERE EMP_ID = @F1 -- EMPLOYEE TYPE
		SELECT @F9_OLD =  CONVERT(VARCHAR(12),START_DATE,106)  FROM TBL_EMP WHERE EMP_ID = @F1 -- START DATE
		SELECT @F10_OLD = CONVERT(VARCHAR(12),END_DATE,106)  FROM TBL_EMP WHERE EMP_ID = @F1 -- END DATE
	END

 	IF @MODULE = 'OVERTIME'
 	BEGIN
		SET @F1_OLD = @F1 -- EMP_ID 	
		SET @F2_OLD = @F2 -- REQUEST ID
		SELECT @F3_OLD = JOBCODE_ID FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- JOBCODE
		SELECT @F4_OLD = CLIENT_JOBCODE FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 -- CLIENT JOBCODE
		SELECT @F5_OLD = STATUS FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- STATUS
		SELECT @F6_OLD = REMARKS FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- REMARKS
		SELECT @F7_OLD = PROCESS FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- PROCESS
		SELECT @F8_OLD = SECT_ID FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- SECT ID
		SELECT @F9_OLD = CONVERT(VARCHAR(12),OT_DATE,106)  FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- OT DATE
		SELECT @F10_OLD = CONVERT(VARCHAR(12),OT_TIME,106) + ' ' + CONVERT(VARCHAR(12),OT_TIME,108) FROM TBL_OVERTIMEREQ WHERE EMP_ID = @F1 AND REQUEST_ID = @F2 -- OT TIME
 	END

 	IF @MODULE = 'TIMESHEET'
 	BEGIN
		SET @F1_OLD = @F1 -- EMP_ID 	
		SET @F2_OLD = @F2 -- TR_DATE
		SET @F3_OLD = @F3 -- JOBCODE_ID
		SET @F4_OLD = @F4 -- CATEGORY_ID
		SELECT @F5_OLD = CLIENT_JOBCODE FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2 AND  JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
		SELECT @F6_OLD = PROJECT_SECT_ID FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2 AND  JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
		SELECT @F7_OLD = TIME_IN FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2 AND  JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
		SELECT @F8_OLD = TIME_OUT FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2 AND  JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
		SELECT @F9_OLD = LOC_ID FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2 AND  JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
		SELECT @F10_OLD = TIMESPENT FROM TBL_TIMESHEET WHERE EMP_ID = @F1 AND TR_DATE = @F2  AND JOBCODE_ID = @F3 AND CATEGORY_ID = @F4-- CLIENT JOBCODE		
	END

	IF @MODULE = 'ATTENDANCE'
 	BEGIN
		SET @F1_OLD = @F1 -- EMP_ID 	
		SET @F2_OLD = @F2 -- TR DATE
		SELECT @F3_OLD = CONVERT(VARCHAR(12),CARD_IN,106) + ' ' + CONVERT(VARCHAR(12),CARD_IN,108) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- CARD IN
		SELECT @F4_OLD = CONVERT(VARCHAR(12),CARD_OUT,106) + ' ' + CONVERT(VARCHAR(12),CARD_OUT,108) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- CARD OUT
		SELECT @F5_OLD = ATT_TYPE_ID FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- ATT_TYPE_ID
		SELECT @F6_OLD = ISNULL(CL,0) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- CL
		SELECT @F7_OLD = ISNULL(EF,0) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- EF
		SELECT @F8_OLD = ISNULL(PERMISSION,0) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- O3
		SELECT @F9_OLD = LOC_ID  FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- OT DATE
		SELECT @F10_OLD = ISNULL(HOL,0)+ISNULL(OT,0) FROM TBL_CARD_TR WHERE EMP_ID = @F1 AND TR_DATE = @F2 -- OT TIME
 	END

	IF @MODULE = 'PLANNING'
	BEGIN
		print 'Planning'
	END

	IF @MODULE = 'PROJECT'
	BEGIN
		print 'Project'
	END

END

INSERT INTO TBL_HISTORY
(
	TYPE,
	MODULE,
	MODIFY_BY,
	MODIFY_DATE,
	F1_OLD,
	F1_NEW,
	F2_OLD,
	F2_NEW,
	F3_OLD,
	F3_NEW,
	F4_OLD,
	F4_NEW,
	F5_OLD,
	F5_NEW,
	F6_OLD,
	F6_NEW,
	F7_OLD,
	F7_NEW,
	F8_OLD,
	F8_NEW,
	F9_OLD,
	F9_NEW,
	F10_OLD,
	F10_NEW,
	COMPUTER_NAME
)
VALUES
(
	@TYPE,
	@MODULE,
	@USER_ID,
	GETDATE(),
	@F1_OLD,
	@F1,
	@F2_OLD,
	@F2,
	@F3_OLD,
	@F3,
	@F4_OLD,
	@F4,
	@F5_OLD,
	@F5,
	@F6_OLD,
	@F6,
	@F7_OLD,
	@F7,
	@F8_OLD,
	@F8,
	@F9_OLD,
	@F9,
	@F10_OLD,
	@F10,
	@COMPUTER_NAME
)








GO
/****** Object:  StoredProcedure [dbo].[stp_insertOvertimeReq]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--CREATE PROCEDURE stp_insertOvertimeReq
--(
--@REQUEST_ID VARCHAR(20),
--@EMP_ID VARCHAR(20),
--@JOBCODE_ID VARCHAR(100),
--@OT_DATE DATETIME,
--@OT_TIME VARCHAR(5),
--@REMARKS VARCHAR(100),
--@STATUS CHAR(1),
--@REQUEST_BY VARCHAR(20),
--@MSG CHAR(100) OUTPUT
--)
--AS
--DECLARE @OT AS DATETIME
--DECLARE @CARDIN AS DATETIME
--DECLARE @JOBID VARCHAR(20)
--DECLARE @CLIENID VARCHAR(20)
--DECLARE @SECTID VARCHAR(10)
--DECLARE @LOC_ID VARCHAR(2)

--SET @MSG = ''
--IF CHARINDEX('|',@JOBCODE_ID) > 0
--BEGIN
--	SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--	SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--	IF CHARINDEX('|',@JOBCODE_ID) > 0	
--	BEGIN
--		SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--		SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--	END
--	ELSE
--	BEGIN
--		SET @CLIENID = @JOBCODE_ID
--		SET @SECTID = ''
--	END
--END
--IF @OT_TIME = '24:00' OR @OT_TIME = '00:00'
--BEGIN
--	SET @OT_TIME = '00:00'
--	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--END
--ELSE
--	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--SELECT @CARDIN = CARD_IN FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
--IF CONVERT(VARCHAR(5),@OT,108) <= CONVERT(VARCHAR(5),@CARDIN,108) AND CONVERT(VARCHAR(10),@OT,101) = CONVERT(VARCHAR(10),@CARDIN,101) SET @OT = DATEADD(DAY,1,@OT)

--IF NOT EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
--BEGIN
--	INSERT INTO TBL_OVERTIMEREQ	
--	(	REQUEST_ID,
--		EMP_ID,
--		JOBCODE_ID,
--		CLIENT_JOBCODE,
--		SECT_ID,
--		OT_DATE,
--		OT_TIME,
--		REMARKS,
--		STATUS,
--		REQUESTED_BY
--	)
--	VALUES 
--	(	@REQUEST_ID,
--		@EMP_ID,
--		ISNULL(@JOBID,''),
--		ISNULL(@CLIENID,''),
--		ISNULL(@SECTID,''),
--		@OT_DATE,
--		ISNULL(@OT,''),
--		ISNULL(@REMARKS,''),
--		ISNULL(@STATUS,'1'),
--		ISNULL(@REQUEST_BY,'')
--	)
--	SELECT @LOC_ID = LOC_ID FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE
--	UPDATE TBL_OVERTIMEREQ SET LOC_ID = @LOC_ID WHERE REQUEST_ID = @REQUEST_ID
--	DECLARE @CURRDATE AS VARCHAR(20)
--	SET @CURRDATE = GETDATE()
--	EXEC STP_INSERTHISTORICALDATA 'INSERT','OVERTIME',@REQUEST_BY, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, '0', @SECTID, @OT_DATE, @OT, ''
---- 		@TYPE 
---- 		@MODULE 
---- 		@USER_ID 
---- 		@F1 EMPLOYEE ID
---- 		@F2 REQUEST ID
---- 		@F3 JOBCODE
---- 		@F4 CLIENT JOBCODE
---- 		@F5 STATUS
---- 		@F6 REMARKS
---- 		@F7 PROCESS
---- 		@F8 SECTION
---- 		@F9 OT DATE
---- 		@F10 OT TIME
---- 		@COMPUTER_NAME 
--END
--ELSE
--	SET @MSG = 'Request Already Exists !!'
--GO


--<JID001> Additional OT Request Info

--stp_insertOvertimeReq 'OT-121109-1315','1315','0-0571-00-0000|G-2004-00-0000|E230|E230136','12/11/2009','21:30','','1','0858',''
CREATE PROCEDURE [dbo].[stp_insertOvertimeReq]
@REQUEST_ID varchar(20), @EMP_ID varchar(20), @JOBCODE_ID varchar(100), @OT_DATE datetime, @OT_TIME varchar(5), @REMARKS varchar(100), @STATUS char(1), @REQUEST_BY varchar(20), @MSG char(100) OUTPUT
WITH EXEC AS CALLER
AS
DECLARE @OT AS DATETIME
DECLARE @CARDIN AS DATETIME
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @SECTID VARCHAR(20)
DECLARE @LOC_ID VARCHAR(2)
DECLARE @CATID VARCHAR(20)
DECLARE @FWBS VARCHAR(10)

SET @MSG = ''
IF CHARINDEX('|',@JOBCODE_ID) > 0
BEGIN
	SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
	SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
	IF CHARINDEX('|',@JOBCODE_ID) > 0	
	BEGIN
		--SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		--SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
		--<JID001>
		SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		SET @SECTID = LEFT(SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID)),4)
		SET @CATID = RIGHT(@JOBCODE_ID,CHARINDEX('|',REVERSE(@JOBCODE_ID))-1)
		SET @FWBS = RIGHT(LTRIM(RTRIM(@CATID)),LEN(LTRIM(RTRIM(@CATID)))-LEN(@SECTID))
		--<\JID001>
	END
	ELSE
	BEGIN
		SET @CLIENID = @JOBCODE_ID
		SET @SECTID = ''
	END
END
IF @OT_TIME = '24:00' OR @OT_TIME = '00:00'
BEGIN
	SET @OT_TIME = '00:00'
	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
END
ELSE
	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME

SELECT @CARDIN = CARD_IN FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID

IF (CONVERT(VARCHAR(5),@OT,108) <= CONVERT(VARCHAR(5),@CARDIN,108) 
AND CONVERT(VARCHAR(10),@OT,101) = CONVERT(VARCHAR(10),@CARDIN,101) )
OR (@CARDIN IS NULL AND @OT_TIME = '00:00')
	SET @OT = DATEADD(DAY,1,@OT)

IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
BEGIN
	INSERT INTO MICADEBUG.dbo.TBL_OVERTIMEREQ	
	(	REQUEST_ID,
		EMP_ID,
		JOBCODE_ID,
		CLIENT_JOBCODE,
		SECT_ID,
		OT_DATE,
		OT_TIME,
		REMARKS,
		STATUS,
		REQUESTED_BY,
		CATEGORY_ID,
		FWBS,
		FIRSTRATER,
		SECONDRATER,
		APP_TYPE
	)
	VALUES 
	(	@REQUEST_ID,
		@EMP_ID,
		ISNULL(@JOBID,''),
		ISNULL(@CLIENID,''),
		ISNULL(@SECTID,''),
		@OT_DATE,
		ISNULL(@OT,''),
		ISNULL(@REMARKS,''),
		ISNULL(@STATUS,'1'),
		ISNULL(@REQUEST_BY,''),
		ISNULL(@CATID,''),
		@FWBS,
		MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1),
		MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2),
		--dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''))
    MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,''))
		
	)
	SELECT @LOC_ID = LOC_ID FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE
	UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ SET LOC_ID = @LOC_ID WHERE REQUEST_ID = @REQUEST_ID
	DECLARE @CURRDATE AS VARCHAR(20)
	SET @CURRDATE = GETDATE()
	--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'INSERT','OVERTIME',@REQUEST_BY, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, '0', @SECTID, @OT_DATE, @OT, ''
-- 		@TYPE 
-- 		@MODULE 
-- 		@USER_ID 
-- 		@F1 EMPLOYEE ID
-- 		@F2 REQUEST ID
-- 		@F3 JOBCODE
-- 		@F4 CLIENT JOBCODE
-- 		@F5 STATUS
-- 		@F6 REMARKS
-- 		@F7 PROCESS
-- 		@F8 SECTION
-- 		@F9 OT DATE
-- 		@F10 OT TIME
-- 		@COMPUTER_NAME 
END
ELSE
	SET @MSG = 'Request Already Exists !!'

GO
/****** Object:  StoredProcedure [dbo].[stp_insertOvertimeReqNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--CREATE PROCEDURE stp_insertOvertimeReq
--(
--@REQUEST_ID VARCHAR(20),
--@EMP_ID VARCHAR(20),
--@JOBCODE_ID VARCHAR(100),
--@OT_DATE DATETIME,
--@OT_TIME VARCHAR(5),
--@REMARKS VARCHAR(100),
--@STATUS CHAR(1),
--@REQUEST_BY VARCHAR(20),
--@MSG CHAR(100) OUTPUT
--)
--AS
--DECLARE @OT AS DATETIME
--DECLARE @CARDIN AS DATETIME
--DECLARE @JOBID VARCHAR(20)
--DECLARE @CLIENID VARCHAR(20)
--DECLARE @SECTID VARCHAR(10)
--DECLARE @LOC_ID VARCHAR(2)

--SET @MSG = ''
--IF CHARINDEX('|',@JOBCODE_ID) > 0
--BEGIN
--	SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--	SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--	IF CHARINDEX('|',@JOBCODE_ID) > 0	
--	BEGIN
--		SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--		SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--	END
--	ELSE
--	BEGIN
--		SET @CLIENID = @JOBCODE_ID
--		SET @SECTID = ''
--	END
--END
--IF @OT_TIME = '24:00' OR @OT_TIME = '00:00'
--BEGIN
--	SET @OT_TIME = '00:00'
--	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--END
--ELSE
--	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--SELECT @CARDIN = CARD_IN FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
--IF CONVERT(VARCHAR(5),@OT,108) <= CONVERT(VARCHAR(5),@CARDIN,108) AND CONVERT(VARCHAR(10),@OT,101) = CONVERT(VARCHAR(10),@CARDIN,101) SET @OT = DATEADD(DAY,1,@OT)

--IF NOT EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
--BEGIN
--	INSERT INTO TBL_OVERTIMEREQ	
--	(	REQUEST_ID,
--		EMP_ID,
--		JOBCODE_ID,
--		CLIENT_JOBCODE,
--		SECT_ID,
--		OT_DATE,
--		OT_TIME,
--		REMARKS,
--		STATUS,
--		REQUESTED_BY
--	)
--	VALUES 
--	(	@REQUEST_ID,
--		@EMP_ID,
--		ISNULL(@JOBID,''),
--		ISNULL(@CLIENID,''),
--		ISNULL(@SECTID,''),
--		@OT_DATE,
--		ISNULL(@OT,''),
--		ISNULL(@REMARKS,''),
--		ISNULL(@STATUS,'1'),
--		ISNULL(@REQUEST_BY,'')
--	)
--	SELECT @LOC_ID = LOC_ID FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE
--	UPDATE TBL_OVERTIMEREQ SET LOC_ID = @LOC_ID WHERE REQUEST_ID = @REQUEST_ID
--	DECLARE @CURRDATE AS VARCHAR(20)
--	SET @CURRDATE = GETDATE()
--	EXEC STP_INSERTHISTORICALDATA 'INSERT','OVERTIME',@REQUEST_BY, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, '0', @SECTID, @OT_DATE, @OT, ''
---- 		@TYPE 
---- 		@MODULE 
---- 		@USER_ID 
---- 		@F1 EMPLOYEE ID
---- 		@F2 REQUEST ID
---- 		@F3 JOBCODE
---- 		@F4 CLIENT JOBCODE
---- 		@F5 STATUS
---- 		@F6 REMARKS
---- 		@F7 PROCESS
---- 		@F8 SECTION
---- 		@F9 OT DATE
---- 		@F10 OT TIME
---- 		@COMPUTER_NAME 
--END
--ELSE
--	SET @MSG = 'Request Already Exists !!'
--GO


--<JID001> Additional OT Request Info

--stp_insertOvertimeReq 'OT-121109-1315','1315','0-0571-00-0000|G-2004-00-0000|E230|E230136','12/11/2009','21:30','','1','0858',''
CREATE PROCEDURE [dbo].[stp_insertOvertimeReqNew]
(@REQUEST_ID varchar(20), 
@EMP_ID varchar(20), 
@JOBCODE_ID varchar(100), 
@OT_DATE datetime, 
@OT_TIME varchar(5), 
@REMARKS varchar(100), 
@STATUS char(1), 
@REQUEST_BY varchar(20))
--WITH EXEC AS CALLER
AS
DECLARE @OT AS DATETIME
DECLARE @CARDIN AS DATETIME
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @SECTID VARCHAR(20)
DECLARE @LOC_ID VARCHAR(2)
DECLARE @CATID VARCHAR(20)
DECLARE @FWBS VARCHAR(10)
DECLARE @MSG char(100)

SET @MSG = ''
IF CHARINDEX('|',@JOBCODE_ID) > 0
BEGIN
	SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
	SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
	IF CHARINDEX('|',@JOBCODE_ID) > 0	
	BEGIN
		--SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		--SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
		--<JID001>
		SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		SET @SECTID = LEFT(SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID)),4)
		SET @CATID = RIGHT(@JOBCODE_ID,CHARINDEX('|',REVERSE(@JOBCODE_ID))-1)
		SET @FWBS = RIGHT(LTRIM(RTRIM(@CATID)),LEN(LTRIM(RTRIM(@CATID)))-LEN(@SECTID))
		--<\JID001>
	END
	ELSE
	BEGIN
		SET @CLIENID = @JOBCODE_ID
		SET @SECTID = ''
	END
END
IF @OT_TIME = '24:00' OR @OT_TIME = '00:00'
BEGIN
	SET @OT_TIME = '00:00'
	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
END
ELSE
	SET @OT = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME

SELECT @CARDIN = CARD_IN FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID

IF (CONVERT(VARCHAR(5),@OT,108) <= CONVERT(VARCHAR(5),@CARDIN,108) 
AND CONVERT(VARCHAR(10),@OT,101) = CONVERT(VARCHAR(10),@CARDIN,101) )
OR (@CARDIN IS NULL AND @OT_TIME = '00:00')
	SET @OT = DATEADD(DAY,1,@OT)

IF NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
BEGIN
	INSERT INTO MICADEBUG.dbo.TBL_OVERTIMEREQ	
	(	REQUEST_ID,
		EMP_ID,
		JOBCODE_ID,
		CLIENT_JOBCODE,
		SECT_ID,
		OT_DATE,
		OT_TIME,
		REMARKS,
		STATUS,
		REQUESTED_BY,
		CATEGORY_ID,
		FWBS,
		FIRSTRATER,
		SECONDRATER,
		APP_TYPE
	)
	VALUES 
	(	@REQUEST_ID,
		@EMP_ID,
		ISNULL(@JOBID,''),
		ISNULL(@CLIENID,''),
		ISNULL(@SECTID,''),
		@OT_DATE,
		ISNULL(@OT,''),
		ISNULL(@REMARKS,''),
		ISNULL(@STATUS,'1'),
		ISNULL(@REQUEST_BY,''),
		ISNULL(@CATID,''),
		@FWBS,
		MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1),
		MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2),
		--dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''))
    MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,''))
		
	)
	SELECT @LOC_ID = LOC_ID FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE
	UPDATE MICADEBUG.dbo.TBL_OVERTIMEREQ SET LOC_ID = @LOC_ID WHERE REQUEST_ID = @REQUEST_ID
	DECLARE @CURRDATE AS VARCHAR(20)
	SET @CURRDATE = GETDATE()
	--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'INSERT','OVERTIME',@REQUEST_BY, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, '0', @SECTID, @OT_DATE, @OT, ''
-- 		@TYPE 
-- 		@MODULE 
-- 		@USER_ID 
-- 		@F1 EMPLOYEE ID
-- 		@F2 REQUEST ID
-- 		@F3 JOBCODE
-- 		@F4 CLIENT JOBCODE
-- 		@F5 STATUS
-- 		@F6 REMARKS
-- 		@F7 PROCESS
-- 		@F8 SECTION
-- 		@F9 OT DATE
-- 		@F10 OT TIME
-- 		@COMPUTER_NAME 
END
ELSE
BEGIN
	SET @MSG = 'Request Already Exists !!'
END

SELECT @MSG AS Message


GO
/****** Object:  StoredProcedure [dbo].[Stp_InsLeaveRequest]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Select dbo.Funct_LeaveQuota ('0447',2010,'AL','7/28/2010')  
--Stp_InsLeaveRequest '0447','AD','7/28/2010','7/28/2010',1, 1,'','0447',0,''
CREATE PROCEDURE [dbo].[Stp_InsLeaveRequest]
@EMP_ID char(4), @CLASS_TYPE char(2), @START_DATE datetime, @END_DATE datetime, @TOTAL_DAY int, @STATUS int, @REMARKS varchar(100), @REQUEST_BY char(4), @ADV_STATUS int, @MSG varchar(255) OUTPUT
WITH EXEC AS CALLER
AS
--DECLARE @MSG Varchar(100) 

--)

--SET @EMP_ID = '0842'
--SET @CLASS_TYPE = 'AL'
--SET @START_DATE = '2008-6-5'
--SET @END_DATE = '2008-6-5'
--SET @TOTAL_DAY = 1
--SET @STATUS = 1
--SET @REMARKS = ''
--APP_DEPT_BY [char] (4) ,
--@APP_DIV_BY [char] (4) ,
--@DEPT_MGR [char] (4) ,
--@DEPT_DATE [datetime] ,
--@DIV_MGR [char] (10) ,
--@DIV_DATE [datetime] ,
--SET @REQUEST_BY = '0895'

DECLARE @DATE_COUNTER AS DATETIME

DECLARE @LEAVE_REQUEST [varchar] (20)
DECLARE @APP_DEPT_BY [char] (4) 
DECLARE @APP_DIV_BY [char] (4) 
DECLARE @SMONTH AS CHAR(2)
DECLARE @SYEAR AS CHAR(2)
DECLARE @SDATE AS CHAR(2)
DECLARE @TYEAR AS CHAR(4)
DECLARE @IS_OK AS INT
DECLARE @AUTH AS INT
DECLARE @EMP_AUTH AS INT
DECLARE @DEPT_ID AS CHAR(4)
DECLARE @LOC_ID AS CHAR(2)
DECLARE @StartDay_Is_holiday as INT
DECLARE @I AS INt
DECLARE @MONTH AS INT
DECLARE @YEAR AS INT
DECLARE @EMpStart_date AS DATETIME
DECLARE @CS_ServiceLength AS INT
DECLARE @EXTSTATUS AS INT
DECLARE @ADV_YEAR AS INT
--DECLARE @ADV_STATUS AS INt
DECLARE @IS_INSERT_OK as INT
DECLARE @SEND_TO_ATT AS INT
DECLARE @END_OL_DATE AS DATETIME
DECLARE @QUOTA_AD AS INT
DECLARE @QUOTA_AL AS INT
--DECLARE @LastYearRemain as int
--Declare @Exp_AL as int
--Declare @ExpiredAL as datetime
Declare @Quota_ExpAL as int
DECLARE @UpComingOL as int
DECLARE @UpComingOL_NotTaken as int
DECLARE @UpComingOL_taken as int

IF @ADV_STATUS = 1 SET @ADV_YEAR = YEAR(GETDATE()) 
ELSE SET @ADV_YEAR = ''

SET @IS_INSERT_OK = 0
SET @SEND_TO_ATT = 0
SET @IS_OK = 0
SET @AUTH = (SELECT AUTHORITY FROM TBL_USER_MS WHERE USER_ID = @REQUEST_BY)
SET @EMP_AUTH = (SELECT AUTHORITY FROM TBL_USER_MS WHERE USER_ID = @EMP_ID)

--CEK STARTDATE APAKAH SEBUAH HOLIDAY?
SET @LOC_ID = (SELECT LOC_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)

SET @I = (DAY(@START_DATE))
SET @MONTH = (MONTH(@START_DATE))
SET @YEAR = (YEAR(@START_DATE))

SET @STARTDAY_IS_HOLIDAY = (SELECT DBO.FUNCT_ISHOLIDAY1(@i, @month, @Year, @Loc_Id))

SET @TYEAR = CAST(YEAR(@START_DATE) AS CHAR(4))
SET @SYEAR = RIGHT(@TYEAR, 2)
SET @SMONTH = CAST(MONTH(@START_DATE) as CHAR(2))
IF LEN(RTRIM(LTRIM(@SMONTH))) = 1 SET @SMONTH = '0' + @SMONTH

SET @SDATE = CAST(DAY(@START_DATE) as CHAR(2))
IF LEN(RTRIM(LTRIM(@SDATE))) = 1 SET @SDATE = '0' + @SDATE

SET @LEAVE_REQUEST = 'LA-' + @SMONTH + @SDATE + @SYEAR + '-' + @EMP_ID 

--IF NOT EXISTS (SELECT * FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS < 4) edit by Indra, 04/JAN/2013
IF NOT EXISTS (SELECT * FROM TBL_LEAVE_REQUEST WHERE EMP_ID=@EMP_ID AND ((@START_DATE BETWEEN START_DATE AND END_DATE) OR (@END_DATE BETWEEN START_DATE AND END_DATE)) AND STATUS < 4) 
	AND YEAR(@START_DATE) = YEAR(@END_DATE) --AND @STARTDAY_IS_HOLIDAY = 0
BEGIN
	SET @APP_DEPT_BY = (Select dbo.Funct_FindDeptManager_ByEmp(@EMP_ID) as APP_DEPT_BY)
	SET @APP_DIV_BY = (Select dbo.Funct_FindDivManager_ByEmp(@EMP_ID) as APP_DIV_BY)
	SET @DEPT_ID = (Select Sect_Id from tbl_emp where emp_id = @Emp_ID)

	--HARDCODE KHUSUS PAK BANDUNG AGUST 2009 --
	IF @EMP_ID = '1852' SET @APP_DEPT_BY = '9030' -- EMPLOYEE_ID CHANGE FROM 0658 TO 1852
	IF @EMP_ID = '1852' SET @APP_DIV_BY = '9030'

--	set @LastYearRemain = (select isnull(LastYearRemain,0)-isnull(AL,0) as LastYearRemain from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
--	set @ExpiredAL = (select isnull(ExpiredAL,'1/1/1900') as ExpiredAL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
--	set @Exp_AL = (select isnull(Exp_AL,0) as Exp_AL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
	
--	PRINT @LastYearRemain
--	PRINT @Exp_AL
--	PRINT @ExpiredAL
	
--	if (@CLASS_TYPE='AL' or @CLASS_TYPE='OL') and (@TOTAL_DAY >= @Exp_AL) and (@Exp_AL > 0)
--	   begin
--		   declare @SelDay AS INT
--		   if (@START_DATE <= @ExpiredAL) 
--			    begin
--				    if (@END_DATE <= @ExpiredAL)
--					      set  @SelDay = dbo.Funct_LeaveDiffDays(@START_DATE,@END_DATE,@Emp_ID)
--					  else
--				        set  @SelDay = dbo.Funct_LeaveDiffDays(@START_DATE,@ExpiredAL,@Emp_ID)
								
						--update tbl_leave_allocation set exp_al = exp_al - @SelDay where emp_id=@Emp_Id and yeartr=@TYEAR
--			 		end
--		 end
	
	set @Quota_ExpAL = (Select IsNull(dbo.Funct_LeaveQuota_ExpAL (@Emp_Id,@start_date,@end_date,@total_day,@CLASS_TYPE),0) AS Quota_ExpAL)

	DECLARE @QUOTA AS INT
	SET @QUOTA = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),@CLASS_TYPE,@End_Date) AS Quota) 
  SET @QUOTA = ISNULL(@QUOTA,0) + ISNULL(@Quota_ExpAL,0)
  
  PRINT 'QUOTA => ' + CONVERT(VARCHAR,@QUOTA)
  PRINT 'TTL DAY => ' + CONVERT(VARCHAR,@TOTAL_DAY)
  
	SELECT @Emp_Id,Year(@Start_date),'AL',@End_Date, @QUOTA
	PRINT @Quota
	PRINT @TOTAL_DAY
	PRINT @CLASS_TYPE
	
	DECLARE @IS_OL_DATE AS INT
	--IF @CLASS_TYPE = 'OL' AND EXISTS (SELECT * FROM TBL_OL WHERE HOL_DATE = @START_DATE) SET @IS_OL_DATE = 1
  IF EXISTS (SELECT * FROM TBL_OL WHERE HOL_DATE = @START_DATE) SET @IS_OL_DATE = 1 -- edit 11 dec 2014
  
--ADDED BY ARY
	IF @CLASS_TYPE = 'AD'
	BEGIN
		SET @QUOTA_AL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AL',@End_Date) AS Quota) + IsNull(@Quota_ExpAL,0)
    -- Edit by Indra, 21 Jul 2014
    -- ketika AL seseorang sudah habis (dan masih menyisakan OL yang sudah di LOCK / UpComing OL), si employee sudah bisa me request AD nya ( bagi yang sudah berhak dapat AD)
		/*SELECT @END_OL_DATE = MAX(END_DATE) FROM TBL_LEAVE_REQUEST WHERE EMP_ID = @EMP_ID AND CLASS_TYPE = 'OL' AND YEAR(END_DATE) = YEAR(@END_DATE) and END_DATE < @End_Date
		IF @END_OL_DATE IS NULL SET @END_OL_DATE = GETDATE()
		Set @UpComingOL = (select count(*) From TBL_OL Where Hol_Date >= @END_OL_DATE and Year(Hol_Date) = year(@END_OL_DATE))
    SET @QUOTA_AL = @QUOTA_AL - @UpComingOL*/
    -- End Of Edit by Indra, 21 Jul 2014
		SET @QUOTA_AD = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AD',@End_Date) AS Quota)
		print convert(varchar,@QUOTA_AL) + ', ' + convert(varchar,@QUOTA_AD)
		IF @QUOTA_AL > 0 
		BEGIN
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Use remaining AL : ' +  cast(@QUOTA_AL as char(2)) + ' day(s) !'
			SET @IS_INSERT_OK = 0
			SET @SEND_TO_ATT = 0
		END
		ELSE
		BEGIN
			IF ISNULL(@QUOTA_AD,0) > 0 
			BEGIN
				IF @TOTAL_DAY <= @QUOTA_AD AND @QUOTA_AL <= 0
				BEGIN
					SET @IS_INSERT_OK = 1
		
					SET @MSG =  @CLASS_TYPE + ' REQUEST FOR ' + CAST (@START_DATE AS CHAR(12)) + ' TO ' + CAST(@END_DATE AS CHAR(12)) + ' FOR '  + CAST(@TOTAL_DAY AS CHAR(2)) + ' DAY(S) IS SAVED !'
			
					SET @SEND_TO_ATT = 1	
				END
				ELSE
				BEGIN
					SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Not Enought Quota AD ! '
					SET @IS_INSERT_OK = 0
					SET @SEND_TO_ATT = 0
				END
			END
			ELSE
			BEGIN
				SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. No Quota Left for AD ! '
				SET @IS_INSERT_OK = 0
				SET @SEND_TO_ATT = 0
			END
		END
		
	END
--ADDED BY ARY

  IF @CLASS_TYPE = 'AL' 
	BEGIN
    /* GAK DIPAKE
		SELECT @END_OL_DATE = MAX(END_DATE) FROM TBL_LEAVE_REQUEST WHERE EMP_ID = @EMP_ID AND CLASS_TYPE = 'OL' AND YEAR(END_DATE) = YEAR(@END_DATE) and END_DATE < @END_DATE
		IF @END_OL_DATE IS NULL SET @END_OL_DATE = GETDATE()
		Set @UpComingOL = (select count(*) From TBL_OL Where Hol_Date >= @END_OL_DATE and Year(Hol_Date) = year(@END_OL_DATE))
    set @UpComingOL_NotTaken = (select ISNULL(count(*),0) from tbl_card_tr a where emp_id=@Emp_Id and tr_date >= @END_OL_DATE and year(tr_date)=@YearTr AND CARD_IN IS NOT NULL and exists (select '' from tbl_ol b where a.TR_DATE=b.hol_date))
    Set @UpComingOL = @UpComingOL - @UpComingOL_NotTaken

    -- additional check, for upcoming OL taken, 11 dec 2014
    Set @UpComingOL_Taken = (select  ISNULL(count(*),0) from dbo.tbl_OL a where year(hol_date)=@yeartr AND hol_date > getdate()
                                and exists (select '' from TBL_LEAVE_REQUEST b
                                where b.emp_id=@Emp_Id and a.hol_date between b.start_date and b.end_date
                                and b.class_type = 'OL' and b.status < 100))
                                
    Set @UpComingOL = @UpComingOL - @UpComingOL_NotTaken
    */

		PRINT 'AL'
		--Set @EmpStart_date = (Select start_date from tbl_emp where emp_id = @emp_id)
    declare @RefId int
    select @EmpStart_date=START_DATE, @RefId=REF_ID from tbl_emp where emp_id= @emp_id
    if exists (select '' from tbl_emp where REF_ID=@RefId and EMP_ID <> @emp_id)
    begin
      select top 1 @EmpStart_date = 
      case
        when isnull(JOIN_DATE,'1/1/1900') = '1/1/1900' then START_DATE
        when isnull(JOIN_DATE,'1/1/1900') <> '1/1/1900' then JOIN_DATE
      end
      from tbl_emp where REF_ID = @RefId and EMP_ID <> @emp_id
      order by START_DATE desc
    end
		Set @CS_ServiceLength = (Select datediff(month, @EmpStart_date, getdate()))
		Set @ExtStatus = (Select ExtStatus from tbl_Leave_Allocation where emp_id = @emp_id And YearTr = @Year)
    
    -- start re-coding by Indra, 18 Jul 2014
    IF @CS_ServiceLength >= 12
    BEGIN
       
       IF @TOTAL_DAY <= @Quota
       BEGIN
          PRINT 'AL OK'
			    SET @IS_INSERT_OK = 1
          SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'
					SET @SEND_TO_ATT = 1	
       END
       ELSE
       BEGIN
          PRINT 'AL NOT OK'
				  IF @Quota <= 0 
				    begin 
              Set @MSG = 'Your quota is reached. You are no longer able to take annual leave. Please Contact HRD' 
				      SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					  end
				  IF (@QUOTA > 0) AND (@TOTAL_DAY > @Quota)
				    begin 
            IF @AUTH = 5 -- only for special case, Admin still can input AL for emp who will be resign (COMPLETION on the Remarks)
              begin
                if upper(@REMARKS) = 'COMPLETION'
                BEGIN
                  PRINT 'AL OK'
			            SET @IS_INSERT_OK = 1
                  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'
					        SET @SEND_TO_ATT = 1	
                END
              end
            else
              begin
					      Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				        SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
              end
					  end
       END
    END
    ELSE
    BEGIN
       IF @AUTH = 5 -- only for special case, Admin still can input AL for emp whose @CS_ServiceLength < 12
			 BEGIN
				  PRINT 'AL OK'
				  SET @IS_INSERT_OK = 1
				  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
				  IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
				  SET @SEND_TO_ATT = 1			
			 END 
       ELSE
       BEGIN
          PRINT 'AL NOT OK'
          SET @MSG = 'You have no right to insert AL. Please contact HRD'
       END
    END
    -- end of re-coding by Indra, 18 Jul 2014
    
    /* -- replace by above code, 18 Jul 2014
		IF @TOTAL_DAY <= @Quota AND @CS_ServiceLength >= 12--OR @AUTH = 5
		BEGIN
			PRINT 'AL OK'
			SET @IS_INSERT_OK = 1

--			SET @MSG = 'Data is Saved !'
			
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'
			

			SET @SEND_TO_ATT = 1			
		END
		
		--Start New Case when Employee ID Has change
		IF @TOTAL_DAY <= @Quota AND @CS_ServiceLength <= 12 AND @EXTSTATUS = 1 --OR @AUTH = 5
		BEGIN
			PRINT 'AL OK'
			SET @IS_INSERT_OK = 1

--			SET @MSG = 'Data is Saved !'
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'

			SET @SEND_TO_ATT = 1			
		END

	--End New Case when Employee ID Has change

		ELSE 
			IF @AUTH = 5 
			BEGIN
				PRINT 'AL OK'
				SET @IS_INSERT_OK = 1
--				SET @MSG = 'Data is Saved !'
				SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
				IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
				SET @SEND_TO_ATT = 1			
			END
			ELSE
			BEGIN

				PRINT 'AL NOT OK'
				IF @Quota = 0 
				   begin 
					   Set @MSG = 'Your quota is reached. You are no longer able to take anual leave. Please Contact HRD' 
				     SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					 end
				IF (@QUOTA > 0) AND (@TOTAL_DAY > @Quota)
				    begin 
					   Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				     SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
					 end
			END
      */
	END
  
  ELSE IF (@CLASS_TYPE = 'AL' AND EXISTS (SELECT * FROM TBL_OL WHERE HOL_DATE = '10/14/2013'))
  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved. Please contact HRD !'
  
	ELSE IF (@CLASS_TYPE = 'OL' ) 
	BEGIN
    -- start re-coding by Indra, 18 Jul 2014  
    Set @EmpStart_date = (Select start_date from tbl_emp where emp_id = @emp_id)
		Set @CS_ServiceLength = (Select datediff(month, @EmpStart_date, getdate()))
		Set @ExtStatus = (Select ExtStatus from tbl_Leave_Allocation where emp_id = @emp_id And YearTr = @Year)
    
    IF @IS_OL_DATE = 1
    BEGIN
      IF @CS_ServiceLength < 12
      BEGIN
        PRINT 'OL OK, but ADVANCE LEAVE'
			  SET @IS_INSERT_OK = 1
  			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			SET @SEND_TO_ATT = 1
        SET @ADV_YEAR = YEAR(GETDATE())
      END
      ELSE -- @CS_ServiceLength >= 12
      BEGIN
        DECLARE @REMAIN_AL_OL AS INT
        SET @REMAIN_AL_OL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'OL',@End_Date) AS Quota) + @Quota_ExpAL
        IF (@TOTAL_DAY <= @REMAIN_AL_OL) 
        BEGIN
          PRINT 'OL OK'
			    SET @IS_INSERT_OK = 1
  			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			  SET @SEND_TO_ATT = 1
        END
        ELSE
        BEGIN
          PRINT 'OL NOT OK'
				  IF @REMAIN_AL_OL <= 0 
				  begin 
					   Set @MSG = 'Your quota is reached. You are no longer able to take obligatory leave. Please Contact HRD' 
				     SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
				  IF (@REMAIN_AL_OL > 0) AND (@TOTAL_DAY > @REMAIN_AL_OL)
				  begin 
					   Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				     SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
        END
      END
    END
    ELSE
    BEGIN
      SET @MSG = 'This date is not Obligatory Leave or you have no right to insert OL. Please contact HRD'
    END
    -- end of re-coding by Indra, 18 Jul 2014
    
    /* replace by above code, Indra - 18 Jul 2014
--		SELECT @CLASS_TYPE, @TOTAL_DAY, @Quota, @CS_ServiceLength
		IF @IS_OL_DATE = 1  --AND @CS_ServiceLength >= 12
		BEGIN
			PRINT 'NO AL BUT OK'
			SET @IS_INSERT_OK = 1

--			SET @MSG = 'Data is Saved !'
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 

			SET @SEND_TO_ATT = 1 --SET ADVANCE	
			IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
		END
		ELSE SET @MSG = 'This date is not Obligatory Leave or you have no right to insert OL. Please contact HRD'
    
    */
	END	
  ELSE IF (@CLASS_TYPE = 'HL')
  BEGIN
   
    IF EXISTS (SELECT '' FROM TBL_LEAVE_ALLOCATION
               WHERE Emp_id=@EMP_ID AND Yeartr=YEAR(@START_DATE) AND ISNULL(HL,0) > 0)
    BEGIN
      declare @ExpiredHL datetime
      declare @Exp_HL int
      declare @Auth_HL int
      declare @QuotaHL as int
      
      set @ExpiredHL = (select isnull(ExpiredHL,'1/1/1900') as ExpiredHL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
	    set @Exp_HL = (select isnull(Exp_HL,0) as Exp_HL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=year(@Start_Date))
      SET @QuotaHL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'HL',@End_Date) AS Quota)
      set @Auth_HL = 0
      if (@CLASS_TYPE='HL') and (isnull(@ExpiredHL,'1/1/1900') <> '1/1/1900')
	    begin
	      if (@END_DATE > @ExpiredHL)
        begin
				  set @Exp_HL = dbo.Funct_LeaveDiffDays(DATEADD(day, 1, @ExpiredHL), @END_DATE, @Emp_ID)
          set @Auth_HL = dbo.Funct_LeaveDiffDays(@Start_date, @ExpiredHL, @Emp_ID)
        end
        else
        begin
          set  @Exp_HL = 0
          set  @Auth_HL = dbo.Funct_LeaveDiffDays(@Start_date, @END_DATE, @Emp_ID)
        end
	    end
      
      set @QuotaHL = @QuotaHL - @Exp_HL
    
      IF @TOTAL_DAY <= @QuotaHL
      begin
        if @TOTAL_DAY <> @Auth_HL
        begin
          PRINT 'HL NOT OK'
          Set @MSG = 'Your quota is reached or its already expired. You are no longer able to take home leave. Please Contact HRD' 
			    SET @MSG = @MSG + 'You just able to request home leave for ' + cast(@Auth_HL as Char(2))+'  day(s) only.'
        end
        else
        begin
          PRINT 'HL OK'
			    SET @IS_INSERT_OK = 1
          SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'
			    SET @SEND_TO_ATT = 1
        end
        
      end
      else
      begin
        PRINT 'HL NOT OK'
        IF @QuotaHL <= 0 
			  begin 
          Set @MSG = 'Your quota is reached or its already expired. You are no longer able to take home leave. Please Contact HRD' 
			    SET @MSG = @MSG + 'You have ' + cast(@QuotaHL as Char(2))+'  day(s) left .'
			  end
			  IF (@QuotaHL > 0) AND (@TOTAL_DAY > @QuotaHL)
			  begin 
          Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
			    SET @MSG = @MSG + 'You just have ' + cast(@QuotaHL as Char(2))+'  day(s) left .'
        end
      end
    END
    ELSE
    BEGIN
      Set @MSG = 'Sorry, you are not authorized to take home leave. Please Contact HRD'
    END
        
  END
	ELSE IF  @CLASS_TYPE <> 'OL' AND @CLASS_TYPE <> 'AL' AND @CLASS_TYPE <> 'AD' AND @CLASS_TYPE <> 'HL'
	BEGIN
      /*IF @IS_OL_DATE = 1
        IF @AUTH = 5
        begin
          PRINT 'NO AL BUT OK'
			    SET @IS_INSERT_OK = 1
        end
        else
        begin
          SET @MSG = 'This date is Obligatory Leave or you have no right to insert ' + @CLASS_TYPE + '. Please contact HRD'
        end
      ELSE
      BEGIN*/
        PRINT 'NO AL BUT OK'
			  SET @IS_INSERT_OK = 1

--			SET @MSG = 'Data is Saved ! ' 
			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
			  IF @CLASS_TYPE = 'SI' 
				BEGIN 
					SET @MSG = @MSG +  'Please attach your name and ID in top right corner of doctor certificate, find your manager for approval and Submit your evidences to HRD'
				END
--			silahkan mencantumkan Nama dan Nomor Karyawan pada sudut kanan atas surat dokter anda dan diberikan ke Dept. manager untuk mendapatkan approval"..'
			  IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + ' Please submit your evident to HRD.' 
--      END
			
	END	
	--)

END
ELSE SET @MSG = 'Please crosscheck StartDate & EndDate of your leave request, You already have request for this date!'
PRINT 'MSG = ' + @MSG

IF @IS_INSERT_OK = 1
BEGIN
		INSERT INTO TBL_LEAVE_REQUEST(LEAVE_REQUEST, EMP_ID, CLASS_TYPE, START_DATE, END_DATE, TOTAL_DAY, STATUS, REMARKS, APP_DEPT_BY, APP_DIV_BY, --DEPT_MGR, DEPT_DATE, DIV_MGR, DIV_DATE,
			REQUEST_BY ,
			REQUESTED_ON
			,ADV_YEAR
			)
			VALUES
			--SELECT
			(
			@LEAVE_REQUEST, 
			@EMP_ID,
			@CLASS_TYPE, 
			ISNULL(@START_DATE,'1/1/1900'), 
			ISNULL(@END_DATE,'1/1/1900'),
			@TOTAL_DAY,
			@STATUS,
			@REMARKS,
			@APP_DEPT_BY,
			@APP_DIV_BY,
			--@DEPT_MGR,
			--@DEPT_DATE,
			--@DIV_MGR,
			--@DIV_DATE,
			@REQUEST_BY ,
			GETDATE()
			,@ADV_YEAR
			)
      
      -- Additional if Request_By = 1st Rater, Automatically 1st Rater Approve " 17 Oct 2013 "
      if @EMP_ID = @APP_DEPT_BY
      begin
        --PRINT 'AUTOMATICALLY 1ST RATER APPROVE'
        EXEC stp_ApprovalLeaveReq @APP_DEPT_BY, @EMP_AUTH, 'APP', 1, @LEAVE_REQUEST
      end


END

IF @SEND_TO_ATT = 1
BEGIN
SET @DATE_COUNTER = @START_DATE	

	WHILE @DATE_COUNTER <= @END_DATE
	BEGIN
    --exec stp_timeAttendanceConversionABS @DATE_COUNTER
		exec stp_timeAttendanceConversionEmpABS @DATE_COUNTER, @EMP_ID
		PRINT @DATE_COUNTER
		SET @DATE_COUNTER = DATEADD(Day, 1, @DATE_COUNTER)
	END
END

IF YEAR(@START_DATE) <> YEAR(@END_DATE)
BEGIN
	SET @MSG = 'Please Request separately for Leave Request in different year !'
END

GO
/****** Object:  StoredProcedure [dbo].[Stp_LeaveList2]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Stp_LeaveList2 '0388',2010

CREATE PROCEDURE [dbo].[Stp_LeaveList2]
@emp_id char(4), @yeartr int
WITH EXEC AS CALLER
AS
DECLARE @LastYearRemain as int 
DECLARE @AL as int 
DECLARE @AD as int 
DECLARE @HL as int 
DECLARE @ALx as int 
DECLARE @OLx as int 
DECLARE @ULx as int 
DECLARE @SIx as int 
DECLARE @SLx as int 
DECLARE @ABx as int 
DECLARE @HLx as int 
DECLARE @ADx as int 
DECLARE @TotalAlloc as int 
DECLARE @TotalUsage as int 
DECLARE @Remain as int
declare @TotalOL as int
declare @GrandTot as int 
DECLARE @AL_EXP AS INT
DECLARE @ExpiredAL as datetime
DECLARE @HL_EXP AS INT
DECLARE @ExpiredHL as datetime

declare @UpcomingOL_alreadyTaken as int

--@LastYearRemain= (SELECT    isnull(AL,0) as AL  FROM TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr-1) - (select dbo.Funct_SumLeave(@Emp_id,@Yeartr-1,'ALx'))

--if ((SELECT    isnull(AL,0) as AL  FROM TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr-1) - (select dbo.Funct_SumLeave(@Emp_id,@Yeartr-1,'ALx')))>0
--Ary Edited Begin
-- if Not Exists (select isnull(AL,0) as AL FROM TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr-1)
-- begin
-- set @LastYearRemain=0
-- end
-- else
--Ary Edited end

--set @LastYearRemain=   (SELECT    isnull(AL,0) as AL  FROM TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr - 1)   - (select isnull(dbo.Funct_SumLeave(@Emp_id,@Yeartr - 1,'ALx'),0))
set @LastYearRemain=   (SELECT    isnull(LastYearRemain,0) as LastYearRemain  FROM TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr ) 

--Set @AL = (SELECT      isnull(AL,0) as AL FROM         TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr - 1)
Set @AL = (SELECT      isnull(AL,0) as AL FROM         TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr  )

Set @AD = (SELECT     isnull(AD,0) as AD FROM         TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr)

Set @HL = (SELECT      isnull(HL,0) as HL FROM         TBL_LEAVE_ALLOCATION where emp_id= @emp_id and yeartr=@yeartr)

Set @TotalAlloc=@LastYearRemain+@AL

--+@AD+@HL

Set @ALX = (select dbo.Funct_SumLeave3(@Emp_id,@Yeartr,'ALx',GETDATE()))
Set @OLX = (select dbo.Funct_SumOL(@Emp_id,@yeartr,'OLx') )
Set @ULX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'ULx') )
Set @SIX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'SIx') )
Set @SLX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'SLX') )
Set @ABX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'ABX') )
Set @HLX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'HLX') )
Set @ADX = (select dbo.Funct_SumLeave(@Emp_id,@yeartr,'ADX') )
SET @AL_EXP = (SELECT  Isnull(EXP_AL,0) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)
--SET @ExpiredAL = (SELECT  Isnull(ExpiredAL,'1/1/1900') FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)
SET @ExpiredAL = (SELECT  Isnull(ExpiredAL,convert(datetime,'31-dec-'+convert(varchar,@YearTR))) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)
SET @HL_EXP = (SELECT  Isnull(EXP_HL,0) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)
SET @ExpiredHL = (SELECT  Isnull(ExpiredHL,convert(datetime,'31-dec-'+convert(varchar,@YearTR))) FROM TBL_LEAVE_ALLOCATION Where EMp_ID = @Emp_Id and YearTr = @YearTR)

Set @TotalUsage=@ALX

--Set @TotalOL = (select count (*) from dbo.tbl_OL where year(hol_date)=@yeartr) - @OLX
Set @TotalOL = (select count (*) from dbo.tbl_OL where year(hol_date)=@yeartr AND hol_date > getdate()) 

-- additional check, whether upcoming OL already taken or not, edit by Indra 11 Dec 2014
/*Set @TotalOL = (select  ISNULL(count(*),0) from dbo.tbl_OL a where year(hol_date)=@yeartr AND hol_date > getdate()
                and not exists (select '' from TBL_LEAVE_REQUEST b
                                where b.emp_id=@Emp_Id and a.hol_date between b.start_date and b.end_date
                                and b.class_type in ('AL','OL') and b.status < 100))*/ 
                                
Set @UpcomingOL_alreadyTaken = (select  ISNULL(count(*),0) from dbo.tbl_OL a where year(hol_date)=@yeartr AND hol_date > getdate()
                                and exists (select '' from TBL_LEAVE_REQUEST b
                                where b.emp_id=@Emp_Id and a.hol_date between b.start_date and b.end_date
                                and b.class_type in ('AL','OL') and b.status = 3)) -- only approved request counted !
-- end of additional check

--set @Remain=@TotalAlloc-@TotalUsage
set @Remain=@LastYearRemain-(@TotalUsage + @AL_EXP)
set @GrandTot= @remain-(@TotalOL-@UpcomingOL_alreadyTaken)

--Leave Expired
Declare @IsExpired as int
--IF EXISTS (SELECT * FROM TBL_LEAVE_ALLOCATION WHERE  Month(getdate()) = Month(DateAdd(Month, -1, Isnull(ExpiredAL,'1/1/1900'))) and emp_Id = @Emp_id )
IF EXISTS (SELECT * FROM TBL_LEAVE_ALLOCATION WHERE  Month(getdate()) = Month(Isnull(ExpiredAL,convert(datetime,'31-dec-'+convert(varchar,@YearTR)))) and emp_Id = @Emp_id and year(getdate())=year(Isnull(ExpiredAL,convert(datetime,'31-dec-'+convert(varchar,@YearTR)))))
	Set @IsExpired = 1
Else Set @IsExpired = 0

SET @LASTYEARREMAIN = ISNULL(@LASTYEARREMAIN,0) - ISNULL(@AL,0)

declare @RemainAD as int
declare @RemainHL as int

set @RemainAD = 0
if ISNULL(@AD,0) > 0 set @RemainAD = ISNULL(@AD,0) - ISNULL(@ADX,0)

set @RemainHL = 0
if ISNULL(@HL,0) > 0 set @RemainHL = ISNULL(@HL,0) - ISNULL(@HLX,0)

SELECT 
ISNULL(@LastYearRemain,0) AS LastYearRemain,
ISNULL(@AL,0) AS AL,
ISNULL(@AD,0) AS AD,
ISNULL(@HL,0) AS HL,
ISNULL(@TotalAlloc,0) AS TotalAlloc,
ISNULL(@ALX,0) AS ALX,
ISNULL(@OLX,0) AS OLX,
ISNULL(@ULX,0) AS ULX,
ISNULL(@SIX,0) AS SIX,
ISNULL(@SLX,0) AS SLX,
ISNULL(@ABX,0) AS ABX,
ISNULL(@HLX,0) AS HLX,
ISNULL(@ADX,0) AS ADX,
ISNULL(@TotalUsage,0) AS TotalUsage,
ISNULL(@Remain,0) AS Remain,
ISNULL(@TotalOL,0) AS TotalOL,
ISNULL(@UpcomingOL_alreadyTaken,0) AS UpcomingOL_alreadyTaken,
ISNULL(@GrandTot,0) AS GrandTot,
ISNULL(@IsExpired,0) AS IsExpired,
@RemainAD as RemainAD, @RemainHL as RemainHL,
ISNULL(@AL_EXP,0) AS AL_EXPIRED,
CONVERT(nvarchar(30),ISNULL(@ExpiredAL,'1/1/1900'),106) AS ExpiredAL,
ISNULL(@HL_EXP,0) AS HL_EXPIRED,
CONVERT(nvarchar(30),ISNULL(@ExpiredHL,'1/1/1900'),106) AS ExpiredHL
GO
/****** Object:  StoredProcedure [dbo].[Stp_LeaveOL]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Stp_LeaveOL]
(
@Year as int
)
AS


--Select  left(hol_date,12) as hol_date, hol_name
Select  convert(varchar, hol_date,106) as hol_date_odr, hol_name, hol_date, status =
	case
	When Hol_date >= getdate() then 1
	When Hol_date <  getdate() then 0
	end
FROM         TBL_OL
WHERE YEAR(hol_date) = @Year
order by hol_date asc

GO
/****** Object:  StoredProcedure [dbo].[Stp_LeaveRequestList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- select * from TBL_LEAVE_REQUEST order by requested_on desc
-- Stp_LeaveRequestList '0858',11,2010,'1,2,3','ALL','ALL','ALL',0
--SP_HELP TBL_LEAVE_REQUEST
CREATE PROCEDURE [dbo].[Stp_LeaveRequestList]
@EMP_ID char(4), @MONTH int, @YEAR int, @Status char(100), @DEPT_ID char(4), @DIV_ID char(4), @SEARCH_EMP_ID char(4), @PROCESS int
WITH EXEC AS CALLER
AS
Declare @Auth as Int
Set @Auth = (Select Authority from tbl_user_Ms where user_Id = @Emp_Id)
--select 
print @auth

Declare @STRSQL AS VARCHAR(8000) 
Declare @STRSQL1 AS VARCHAR(8000) 
Declare @STRSQL2 AS VARCHAR(8000) 
Declare @STRSQL3 AS VARCHAR(8000) 
set @strsql = ''

IF @AUTH = 1 
BEGIN
	SET @STRSQL1 =
	'SELECT     A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,''' + CAST(@Year AS CHAR(4)) + ''',''AL'',A.End_Date) AS Quota,
     ISNULL(APP_DEPT_BY,'''') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'''') AS APP_DIV_BY, ISNULL(DEPT_MGR,'''') AS DEPT, ISNULL(DIV_MGR,'''') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE
	FROM         dbo.TBL_LEAVE_REQUEST A INNER JOIN
                      dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
                      dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ' OR MONTH(A.END_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ')
	AND
	(YEAR(A.START_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ' OR YEAR(A.END_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ')
	AND 
	APP_DEPT_BY = ''' + @EMP_ID  + ''' AND A.STATUS IN (' + @STATUS + ')'
	IF @DIV_ID <> 'ALL' SET @STRSQL1 = @STRSQL1 + ' AND V1_DEPTSECT.DEPT_ID =  ''' + @DIV_ID + ''''
	IF @DEPT_ID <> 'ALL' SET @STRSQL1 = @STRSQL1 + ' AND V1_DEPTSECT.SECT_ID =  ''' + @DEPT_ID + ''''
	IF @SEARCH_EMP_ID <> 'ALL' SET @STRSQL1 = @STRSQL1 + ' AND A.EMP_ID =  ''' + @SEARCH_EMP_ID + ''''
	IF @PROCESS <> 9 SET @STRSQL = @STRSQL + ' AND ISNULL(A.PROCESS,0) =  ' + CAST (@PROCESS AS CHAR(2))
END
IF @AUTH = 2 
BEGIN
  SET @STRSQL2 =
	'SELECT     A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,''' + CAST(@Year AS CHAR(4)) + ''',''AL'',A.End_Date) AS Quota,
                                   ISNULL(APP_DEPT_BY,'''') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'''') AS APP_DIV_BY, ISNULL(DEPT_MGR,'''') AS DEPT, ISNULL(DIV_MGR,'''') AS DIV, ISNULL(PROCESS,0) AS PROCESS,  REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE
	FROM         dbo.TBL_LEAVE_REQUEST A INNER JOIN
                      dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
                      dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ' OR MONTH(A.END_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ')
	AND
	(YEAR(A.START_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ' OR YEAR(A.END_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ')
	AND 
	APP_DIV_BY = ''' + @EMP_ID  + ''' AND A.STATUS IN (' + @STATUS + ')'	

	IF @DIV_ID <> 'ALL' SET @STRSQL2 = @STRSQL2 + ' AND V1_DEPTSECT.DEPT_ID =  ''' + @DIV_ID + ''''
	IF @DEPT_ID <> 'ALL' SET @STRSQL2 = @STRSQL2 + ' AND V1_DEPTSECT.SECT_ID =  ''' + @DEPT_ID + ''''
	IF @SEARCH_EMP_ID <> 'ALL' SET @STRSQL2 = @STRSQL2 + ' AND A.EMP_ID =  ''' + @SEARCH_EMP_ID + ''''
	IF @PROCESS <> 9 SET @STRSQL = @STRSQL + ' AND ISNULL(A.PROCESS,0) =  ' + CAST (@PROCESS AS CHAR(2))

END
IF @AUTH = 5 
BEGIN
	SET @STRSQL =
	'SELECT     A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,''' + CAST(@Year AS CHAR(4)) + ''',''AL'',A.End_Date) AS Quota,
                                   ISNULL(APP_DEPT_BY,'''') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'''') AS APP_DIV_BY, ISNULL(DEPT_MGR,'''') AS DEPT, ISNULL(DIV_MGR,'''') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE
	FROM         dbo.TBL_LEAVE_REQUEST A INNER JOIN
                      dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
                      dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ' OR MONTH(A.END_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ')
	AND
	(YEAR(A.START_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ' OR YEAR(A.END_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ')
	AND 
	A.STATUS IN (' + @STATUS + ')'
	IF @DIV_ID <> 'ALL' SET @STRSQL = @STRSQL + ' AND V1_DEPTSECT.DEPT_ID =  ''' + @DIV_ID + ''''
	IF @DEPT_ID <> 'ALL' SET @STRSQL = @STRSQL + ' AND V1_DEPTSECT.SECT_ID =  ''' + @DEPT_ID + ''''
	IF @SEARCH_EMP_ID <> 'ALL' SET @STRSQL = @STRSQL + ' AND A.EMP_ID =  ''' + @SEARCH_EMP_ID + ''''
	IF @PROCESS <> 9 SET @STRSQL = @STRSQL + ' AND ISNULL(A.PROCESS,0) =  ' + CAST (@PROCESS AS CHAR(2))
END

if exists (select '' from tbl_emp where EMP_ID=@Emp_Id and EMP_ID in ('9022','9030','9054','9055','9059'))
BEGIN
	SET @STRSQL3 = 'SELECT     A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,''' + CAST(@Year AS CHAR(4)) + ''',''AL'',A.End_Date) AS Quota,
                                   ISNULL(APP_DEPT_BY,'''') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'''') AS APP_DIV_BY, ISNULL(DEPT_MGR,'''') AS DEPT, ISNULL(DIV_MGR,'''') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE
				FROM         TBL_LEAVE_REQUEST A
				WHERE     (APP_DIV_BY = ''' + @EMP_ID  + ''') 	AND
	(MONTH(A.START_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ' OR MONTH(A.END_DATE) = '+ CAST(@MONTH AS CHAR(2)) + ')
	AND
	(YEAR(A.START_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ' OR YEAR(A.END_DATE) = '+ CAST(@YEAR AS CHAR(4)) + ')
	AND 
	A.STATUS IN (' + @STATUS + ')'
	IF @SEARCH_EMP_ID <> 'ALL' SET @STRSQL3 = @STRSQL3 + ' AND A.EMP_ID =  ''' + @SEARCH_EMP_ID + ''''
	IF @PROCESS <> 9 SET @STRSQL3 = @STRSQL3 + ' AND ISNULL(A.PROCESS,0) =  ' + CAST (@PROCESS AS CHAR(2))
END

--SELECT @STRSQL
IF @AUTH = 5 SET @STRSQL = @STRSQL
IF @AUTH = 1 SET @STRSQL = @STRSQL1
IF @AUTH = 2 SET @STRSQL = @STRSQL2
if exists (select '' from tbl_emp where EMP_ID=@Emp_Id and EMP_ID in ('9022','9030','9054','9055','9059')) SET @STRSQL = @STRSQL3

EXEC(@STRSQL)
--	dbo.Funct_LeaveQuota (Emp_Id,@Year,''AL'',GetDate()) AS Quota,

GO
/****** Object:  StoredProcedure [dbo].[Stp_LeaveRequestList2]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- select * from TBL_LEAVE_REQUEST order by requested_on desc
-- Stp_LeaveRequestList '0858',11,2010,'1,2,3','ALL','ALL','ALL',0
--SP_HELP TBL_LEAVE_REQUEST
CREATE PROCEDURE [dbo].[Stp_LeaveRequestList2]
@EMP_ID char(4), @MONTH int, @YEAR int
AS
Declare @Auth as Int
Set @Auth = (Select Authority from tbl_user_Ms where user_Id = @Emp_Id)
--select 
print @auth

IF @AUTH = 1 
BEGIN
	SELECT A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,@YEAR,'AL',A.End_Date) AS Quota, 
	ISNULL(APP_DEPT_BY,'') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'') AS APP_DIV_BY, ISNULL(DEPT_MGR,'') AS DEPT, 
	ISNULL(DIV_MGR,'') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE,
	V1_DEPTSECT.DEPT_ID, V1_DEPTSECT.SECT_ID
	FROM 
	dbo.TBL_LEAVE_REQUEST A INNER JOIN
    dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
    dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = @MONTH OR MONTH(A.END_DATE) = @MONTH)
	AND
	(YEAR(A.START_DATE) = @YEAR OR YEAR(A.END_DATE) = @YEAR)
	AND 
	APP_DEPT_BY = @EMP_ID 
	AND A.STATUS < 100
END
IF @AUTH = 2 
BEGIN
	SELECT A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,@YEAR,'AL',A.End_Date) AS Quota, 
	ISNULL(APP_DEPT_BY,'') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'') AS APP_DIV_BY, ISNULL(DEPT_MGR,'') AS DEPT, 
	ISNULL(DIV_MGR,'') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE,
	V1_DEPTSECT.DEPT_ID, V1_DEPTSECT.SECT_ID
	FROM 
	dbo.TBL_LEAVE_REQUEST A INNER JOIN
    dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
    dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = @MONTH OR MONTH(A.END_DATE) = @MONTH)
	AND
	(YEAR(A.START_DATE) = @YEAR OR YEAR(A.END_DATE) = @YEAR)
	AND 
	APP_DIV_BY = @EMP_ID
	AND A.STATUS < 100
END
IF @AUTH = 5 
BEGIN
	SELECT A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,@YEAR,'AL',A.End_Date) AS Quota, 
	ISNULL(APP_DEPT_BY,'') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'') AS APP_DIV_BY, ISNULL(DEPT_MGR,'') AS DEPT, 
	ISNULL(DIV_MGR,'') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE,
	V1_DEPTSECT.DEPT_ID, V1_DEPTSECT.SECT_ID
	FROM 
	dbo.TBL_LEAVE_REQUEST A INNER JOIN
    dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
    dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = @MONTH OR MONTH(A.END_DATE) = @MONTH)
	AND
	(YEAR(A.START_DATE) = @YEAR OR YEAR(A.END_DATE) = @YEAR)
	AND A.STATUS < 100
END

if exists (select '' from tbl_emp where EMP_ID=@Emp_Id and EMP_ID in ('9022','9030','9054','9055','9059'))
BEGIN
	SELECT A.LEAVE_REQUEST, A.EMP_ID, dbo.FUNCT_NAME2(A.EMP_ID) AS EMP_NAME, A.CLASS_TYPE, A.START_DATE, A.END_DATE, A.TOTAL_DAY, A.STATUS, A.REMARKS, 
	dbo.Funct_LeaveQuota (A.Emp_Id,@YEAR,'AL',A.End_Date) AS Quota, 
	ISNULL(APP_DEPT_BY,'') AS APP_DEPT_BY, ISNULL(APP_DIV_BY,'') AS APP_DIV_BY, ISNULL(DEPT_MGR,'') AS DEPT, 
	ISNULL(DIV_MGR,'') AS DIV, ISNULL(PROCESS,0) AS PROCESS, REQUESTED_ON, REQUEST_BY, A.DEPT_DATE, A.DIV_DATE,
	V1_DEPTSECT.DEPT_ID, V1_DEPTSECT.SECT_ID
	FROM 
	dbo.TBL_LEAVE_REQUEST A INNER JOIN
    dbo.TBL_EMP ON A.EMP_ID = dbo.TBL_EMP.EMP_ID INNER JOIN
    dbo.V1_DEPTSECT ON dbo.TBL_EMP.SECT_ID = dbo.V1_DEPTSECT.SECT_ID
	WHERE
	(MONTH(A.START_DATE) = @MONTH OR MONTH(A.END_DATE) = @MONTH)
	AND
	(YEAR(A.START_DATE) = @YEAR OR YEAR(A.END_DATE) = @YEAR)
	AND A.STATUS < 100
END


--	dbo.Funct_LeaveQuota (Emp_Id,@Year,''AL'',GetDate()) AS Quota,

GO
/****** Object:  StoredProcedure [dbo].[Stp_LeaveStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



CREATE  PROCEDURE [dbo].[Stp_LeaveStatus] 
(
@Emp_Id as char(4),
@Year as int
)
AS


Select Emp_Id as Emp_Id, MonthTR, 
Datename(Month, cast(LTRIM(RTRIM(Cast(MonthTR as char(2)))) + '/1/' + LTRIM(RTRIM(Cast(@year as char(4)))) as datetime)) + ' ' + Cast (@Year as char (4)) AS PERIOD,
dbo.Funct_IsAl(DATE1) AS DATE1, 
dbo.Funct_IsAl(DATE2) AS DATE2,
dbo.Funct_IsAl(DATE3) AS DATE3,
dbo.Funct_IsAl(DATE4) AS DATE4,
dbo.Funct_IsAl(DATE5) AS DATE5, 
dbo.Funct_IsAl(DATE6) AS DATE6, 
dbo.Funct_IsAl(DATE7) AS DATE7, 
dbo.Funct_IsAl(DATE8) AS DATE8, 
dbo.Funct_IsAl(DATE9) AS DATE9, 
dbo.Funct_IsAl(DATE10) AS DATE10, 
dbo.Funct_IsAl(DATE11) AS DATE11, 
dbo.Funct_IsAl(DATE12) AS DATE12, 
dbo.Funct_IsAl(DATE13) AS DATE13, 
dbo.Funct_IsAl(DATE14) AS DATE14, 
dbo.Funct_IsAl(DATE15) AS DATE15, 
dbo.Funct_IsAl(DATE16) AS DATE16, 
dbo.Funct_IsAl(DATE17) AS DATE17, 
dbo.Funct_IsAl(DATE18) AS DATE18, 
dbo.Funct_IsAl(DATE19) AS DATE19, 
dbo.Funct_IsAl(DATE20) AS DATE20, 
dbo.Funct_IsAl(DATE21) AS DATE21, 
dbo.Funct_IsAl(DATE22) AS DATE22, 
dbo.Funct_IsAl(DATE23) AS DATE23, 
dbo.Funct_IsAl(DATE24) AS DATE24, 
dbo.Funct_IsAl(DATE25) AS DATE25, 
dbo.Funct_IsAl(DATE26) AS DATE26, 
dbo.Funct_IsAl(DATE27) AS DATE27, 
dbo.Funct_IsAl(DATE28) AS DATE28, 
dbo.Funct_IsAl(DATE29) AS DATE29, 
dbo.Funct_IsAl(DATE30) AS DATE30, 
dbo.Funct_IsAl(DATE31)  AS DATE31
FROM         TBL_LEAVE_STATUS
WHERE YEARTR = @Year
AND EMP_ID = @Emp_id
order by YEARTR, MONTHTR


GO
/****** Object:  StoredProcedure [dbo].[stp_ListEmployeeOvertimeRequest]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--stp_ListEmployeeOvertimeRequest 2,2009,'ALL','0858','ALL','REQUEST_ID','ALL'
--SELECT * FROM TBL_OVERTIMEREQ WHERE EMP_ID = '0858' ORDER BY OT_DATE DESC
CREATE PROCEDURE [dbo].[stp_ListEmployeeOvertimeRequest]
(@MONTH int, @YEAR int, @PERIOD char(10), @EMP_ID char(4), @JOBCODE_ID char(100), @ORDERTEXT char(100), @STATUS char(3))
----WITH 
----EXECUTE AS CALLER
AS

DECLARE @TempTable TABLE
(
RequestId varchar(50),
EmpId char(4),
OtDate DATETIME,
OtTime DATETIME,
StatusDesc varchar(50),
[Status] varchar(1),
Card_in DATETIME,
Card_out DATETIME,
Remarks varchar(100),
Reg varchar(50),
ProjectName varchar(50),
Project varchar(50),
JobCodeId varchar(50),
Pos_Id varchar(10)
)

DECLARE @SQLSTRING AS VARCHAR(5000)
DECLARE @JOB AS VARCHAR(20)
DECLARE @CLIENT AS VARCHAR(20)

IF LTRIM(RTRIM(@JOBCODE_ID)) <> 'ALL'
BEGIN
	IF  CHARINDEX('|',@JOBCODE_ID) > 0
	BEGIN
		SET @JOB = LEFT(LTRIM(RTRIM(@JOBCODE_ID)),CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))-1)
		--SET @CLIENT = SUBSTRING(LTRIM(RTRIM(@JOBCODE_ID)),CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))+1,LEN(LTRIM(RTRIM(@JOBCODE_ID)))-CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID))))
		SET @CLIENT = LEFT(SUBSTRING(LTRIM(RTRIM(@JOBCODE_ID)),CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))+1,LEN(LTRIM(RTRIM(@JOBCODE_ID)))-CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))),LEN(SUBSTRING(LTRIM(RTRIM(@JOBCODE_ID)),CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))+1,LEN(LTRIM(RTRIM(@JOBCODE_ID)))-CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))))-CHARINDEX('|',SUBSTRING(LTRIM(RTRIM(@JOBCODE_ID)),CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))+1,LEN(LTRIM(RTRIM(@JOBCODE_ID)))-CHARINDEX('|',LTRIM(RTRIM(@JOBCODE_ID)))))+1)
	END
END

IF LTRIM(RTRIM(@ORDERTEXT)) = '' SET @ORDERTEXT = 'REQUEST_ID'
SET DATEFIRST  1 
SET @SQLSTRING = 'SELECT DISTINCT REQUEST_ID, EMP_ID, OT_DATE, isnull(OT_TIME,'''') AS OT_TIME, STATUSDESC,STATUS,ISNULL(CARD_IN,'''') AS CARD_IN, ISNULL(CARD_OUT,'''') AS CARD_OUT, REMARKS, ISNULL(REG,0) AS REG, PROJECT_NAME, PROJECT, ISNULL(LTRIM(RTRIM(JOBCODE_ID)),'''')+''|''+ISNULL(LTRIM(RTRIM(CLIENT_JOBCODE)),'''')+''|''+ISNULL(LTRIM(RTRIM(SECT_ID)),'''')+''|''+ISNULL(LTRIM(RTRIM(CATEGORY_ID)),'''') AS JOBCODE_ID, POS_ID   '
SET @SQLSTRING = @SQLSTRING + ' FROM V_OVERTIMEREQUEST '
SET @SQLSTRING = @SQLSTRING + ' WHERE (EMP_ID = ''' + @EMP_ID + ''' AND MONTH(OT_DATE) = ' + CAST(@MONTH AS VARCHAR) + ' AND YEAR(OT_DATE) = ' + CAST(@YEAR AS VARCHAR) 
IF LTRIM(RTRIM(@JOBCODE_ID)) <> 'ALL' SET @SQLSTRING = @SQLSTRING + ' AND JOBCODE_ID = ''' + LTRIM(RTRIM(@JOB)) + ''' AND CLIENT_JOBCODE = ''' + LTRIM(RTRIM(@CLIENT)) + ''''
IF LTRIM(RTRIM(@PERIOD)) = '1' SET @SQLSTRING = @SQLSTRING + ' AND DAY(OT_DATE) BETWEEN 1 AND 10 '
IF LTRIM(RTRIM(@PERIOD)) = '2' SET @SQLSTRING = @SQLSTRING + ' AND DAY(OT_DATE) BETWEEN 11 AND 20 '
IF LTRIM(RTRIM(@PERIOD)) = '3' SET @SQLSTRING = @SQLSTRING + ' AND DAY(OT_DATE) BETWEEN 21 AND 31 '
IF LTRIM(RTRIM(@STATUS)) <> 'ALL' SET @SQLSTRING = @SQLSTRING + ' AND STATUS = ''' + @STATUS + ''''
SET @SQLSTRING = @SQLSTRING + ') ORDER BY  ' + @ORDERTEXT

print (@SQLSTRING)
insert into @TempTable
EXEC (@SQLSTRING)
select ROW_NUMBER() over(order by OtDate asc) as No, * from @TempTable


-- sp_ListEmployeeOvertimeRequest 5,2006,'ALL','0858','ALL','','ALL'


GO
/****** Object:  StoredProcedure [dbo].[Stp_ListOfDept_RealNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_ListOfDept_RealNew]
@EMP_ID char(4), @AUTHORITY int
WITH EXEC AS CALLER
AS
--updated by Ary

DECLARE @GROUP AS CHAR(4)
DECLARE @PM AS VARCHAR(4)
DECLARE @KEYP AS VARCHAR(4)
SET @KEYP = ''
SET @PM = ''
SELECT @PM = ISNULL(KEYP_EMP_ID,'') FROM MICADEBUG.dbo.TBL_PROJECT WHERE STATUS IN ('0','1') AND KEYP_EMP_ID = @EMP_ID
SELECT @KEYP = ISNULL(KEYPERSON,'') FROM MICADEBUG.dbo.TBL_PROJECT_FWBS WHERE JOBCODE_ID IN (SELECT JOBCODE_ID FROM TBL_PROJECT WHERE STATUS IN ('0','1','2')) AND KEYPERSON = @EMP_ID

--perubahan by Jemmy, Auhtoritynya di set mengambil langsung dari database, karena migrasi tidak akan pake session
SELECT @AUTHORITY = AUTHORITY FROM MICADEBUG.dbo.TBL_USER_MS WHERE USER_ID = @EMP_ID

SET @GROUP = (SELECT GROUP_ID FROM MICADEBUG.dbo.TBL_USER_MS WHERE [USER_ID] = @EMP_ID )

  -- ACTING MANAGER
  IF EXISTS (SELECT '' FROM MICADEBUG.dbo.TBL_ACT_MANAGER WHERE EMP_ID=@EMP_ID AND GETDATE() BETWEEN START_DATE AND END_DATE)
  BEGIN
    SELECT DISTINCT * FROM
    (SELECT     TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
		FROM		MICADEBUG.DBO.TBL_DEPT 
		INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
 		WHERE     (TBL_EMP.EMP_ID = @EMP_ID)
    UNION ALL
    SELECT     TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
		FROM		MICADEBUG.DBO.TBL_DEPT 
		INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID AND TBL_SECTION.SECT_ID IN (
              SELECT DISTINCT SECT_ID FROM MICADEBUG.dbo.TBL_ACT_MANAGER
              WHERE EMP_ID=@EMP_ID AND GETDATE() BETWEEN START_DATE AND END_DATE
              )
 		WHERE     (TBL_EMP.EMP_ID = @EMP_ID)) X
  END
  ELSE
  BEGIN
  
  IF @AUTHORITY = 0 -- STANDARD USER
	(SELECT     TBL_SECTION.SECT_ID, TBL_SECTION.SECT_NAME, TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
	FROM		MICADEBUG.DBO.TBL_DEPT 
	INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
	INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
	WHERE     (TBL_EMP.EMP_ID = @EMP_ID)
	)
	--Update by Wied
	--UNION
	--SELECT DISTINCT 
              --        TBL_PROJECT_FWBS.SECT_ID, dbo.Funct_SectName(TBL_PROJECT_FWBS.SECT_ID) AS SECT_NAME, TBL_DEPT.DEPT_ID, 
              --        TBL_DEPT.DEPT_NAME
	--FROM         TBL_PROJECT INNER JOIN
              --        TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE INNER JOIN
              --        TBL_DEPT ON TBL_PROJECT_FWBS.SECT_ID = TBL_DEPT.DEPT_ID
	--WHERE     (TBL_PROJECT.KEYP_EMP_ID = @EMP_ID)

	
IF @AUTHORITY = 1 AND @EMP_ID <> '0244' -- SECTION_USER

	(SELECT     TBL_SECTION.SECT_ID, TBL_SECTION.SECT_NAME, TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
	 FROM		MICADEBUG.DBO.TBL_DEPT 
	 INNER JOIN MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
	 INNER JOIN MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
	 WHERE     (TBL_EMP.EMP_ID = @EMP_ID)
	 )

IF @AUTHORITY = 2 -- DEPT_USER
--	IF @GROUP = 'G011'  OR @EMP_ID = '0766'
--	(SELECT DEPT_ID, DEPT_NAME FROM TBL_DEPT)
--	ELSE

	(
		SELECT     TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
		FROM        MICADEBUG.DBO.TBL_DEPT 
		INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
 		WHERE     (TBL_EMP.EMP_ID = @EMP_ID)
	)
  
	-- Per tgl 12/11/2012 pak indra gunawan jadi manager procurement, so pak suryadi can't approve procurement again
	--if @EMP_ID = '0266'
	--begin
	--	(
	--	SELECT     DBO.TBL_SECTION.DEPT_ID, DBO.TBL_DEPT.DEPT_NAME
	--	FROM         DBO.TBL_DEPT INNER JOIN
	--	DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
  --           	DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
 	--	WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
	--	UNION
	--	SELECT     DBO.TBL_SECTION.DEPT_ID, DBO.TBL_DEPT.DEPT_NAME
	--	FROM         DBO.TBL_DEPT INNER JOIN
	--	DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID AND DBO.TBL_SECTION.SECT_ID='E160'
	--	)
	--end
	--else
	--begin
	--	(
	--	SELECT     DBO.TBL_SECTION.DEPT_ID, DBO.TBL_DEPT.DEPT_NAME
	--	FROM         DBO.TBL_DEPT INNER JOIN
	--	DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
 	--          	DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
 	--	WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
	--	)
	--end

IF @AUTHORITY = 3 -- PROJECT_USER
	(SELECT     TBL_PROJECT_DIST.DEPT_ID, TBL_DEPT.DEPT_NAME
		FROM        MICADEBUG.DBO.TBL_DEPT 
		INNER JOIN	MICADEBUG.DBO.TBL_PROJECT_DIST ON TBL_DEPT.DEPT_ID = TBL_PROJECT_DIST.DEPT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID 
		INNER JOIN	MICADEBUG.DBO.TBL_PROJECT ON TBL_PROJECT_DIST.JOBCODE_ID = TBL_PROJECT.JOBCODE_ID
		WHERE		(TBL_EMP.EMP_ID = @EMP_ID)
	)

IF @AUTHORITY = 4 -- HRB_USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)

IF @AUTHORITY = 5 OR @EMP_ID = '0244'-- HRB_SECTION_USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)

IF @AUTHORITY = 6  -- PAS_USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)

IF @AUTHORITY = 7 -- PAS_SECTION_USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)
	
IF @AUTHORITY = 8 OR @AUTHORITY = 10 -- MANAGEMENT USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)

IF @AUTHORITY = 9 -- MH CONTROL USER
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)


IF @AUTHORITY = 10 -- MANAGEMENT
	(SELECT DEPT_ID, DEPT_NAME FROM MICADEBUG.dbo.TBL_DEPT)

IF @AUTHORITY = 11 -- FAD
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 12 -- SECRETARY
	(SELECT     TBL_SECTION.SECT_ID, TBL_SECTION.SECT_NAME, TBL_SECTION.DEPT_ID, TBL_DEPT.DEPT_NAME
	 FROM		MICADEBUG.DBO.TBL_DEPT 
	 INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
	 INNER JOIN MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
	 WHERE     (TBL_EMP.EMP_ID = @EMP_ID)
	)

-- IF @AUTHORITY = 10 -- DEPUTY USER
-- 	(SELECT     DBO.TBL_SECTION.DEPT_ID, DBO.TBL_DEPT.DEPT_NAME
-- 	FROM         DBO.TBL_DEPT INNER JOIN
-- 	DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
--              DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
--  	WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
-- 	)
  
  END

GO
/****** Object:  StoredProcedure [dbo].[Stp_ListOfSectionNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Stp_ListOfSectionNew]
@EMP_ID char(4), @AUTHORITY int
WITH EXEC AS CALLER
AS
DECLARE @GROUP AS CHAR(4)

--Perubahan Jemmy, Authoritynya di ambil dari bawah sini
SELECT @GROUP = GROUP_ID, @AUTHORITY = AUTHORITY 
FROM MICADEBUG.dbo.TBL_USER_MS WHERE [USER_ID]  = @EMP_ID

-- ACTING MANAGER
  IF EXISTS (SELECT '' FROM MICADEBUG.dbo.TBL_ACT_MANAGER WHERE EMP_ID=@EMP_ID AND GETDATE() BETWEEN START_DATE AND END_DATE)
  BEGIN
    select distinct * from
    (SELECT  TBL_EMP.SECT_ID, TBL_SECTION.SECT_NAME
    FROM		MICADEBUG.dbo.TBL_EMP 
    INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_EMP.SECT_ID = TBL_SECTION.SECT_ID WHERE EMP_ID = @EMP_ID
    union all
    select sect_id, sect_name 
    FROM MICADEBUG.dbo.TBL_SECTION
    where SECT_ID in (select distinct SECT_ID 
						from MICADEBUG.dbo.TBL_ACT_MANAGER 
						where EMP_ID=@EMP_ID and getdate() between start_date and end_date)) x
  end
  ELSE
  BEGIN
  
  IF @AUTHORITY = 0 -- STANDARD USER
	(SELECT     TBL_EMP.SECT_ID, TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.dbo.TBL_EMP 
	INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_EMP.SECT_ID = TBL_SECTION.SECT_ID WHERE EMP_ID = @EMP_ID)
	
IF @AUTHORITY = 1 AND @EMP_ID <> '0244' -- SECTION_USER
	(SELECT     TBL_EMP.SECT_ID, TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.dbo.TBL_EMP 
	INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_EMP.SECT_ID = TBL_SECTION.SECT_ID WHERE EMP_ID = @EMP_ID)
	
	
IF @AUTHORITY = 2 -- DEPT_USER
BEGIN
	IF @GROUP = 'G011'
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)
	ELSE
	(SELECT DISTINCT(TBL_SECTION.SECT_ID), TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.DBO.TBL_DEPT 
	INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
	INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID			
	WHERE TBL_DEPT.DEPT_ID = 
		(SELECT     TBL_SECTION.DEPT_ID
			FROM		MICADEBUG.DBO.TBL_DEPT 
			INNER JOIN	MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
			INNER JOIN	MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID
			WHERE		(TBL_EMP.EMP_ID = @EMP_ID)
		)
	)

	--per tgl 12/11/2012 pak indra become procurement manager, so pak suryadi can't approve 4 procurement again
	--IF @EMP_ID='0266'
	--BEGIN
	--	(SELECT     DISTINCT(DBO.TBL_SECTION.SECT_ID), DBO.TBL_SECTION.SECT_NAME
	--FROM         DBO.TBL_DEPT INNER JOIN
  --           	           DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
  --                      DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID			
	--WHERE TBL_DEPT.DEPT_ID = 
	--	(SELECT     DBO.TBL_SECTION.DEPT_ID
	--              FROM DBO.TBL_DEPT INNER JOIN
  --      	              DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
  --                     	 DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
	--	WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
	--	)
	--)
	--	UNION
	--	SELECT SECT_ID, SECT_NAME FROM TBL_SECTION WHERE SECT_ID = 'E160'
	--END
	--ELSE
	--BEGIN
	--	IF @GROUP = 'G011'
	--(SELECT SECT_ID, SECT_NAME FROM TBL_SECTION)
	--ELSE
	--(SELECT     DISTINCT(DBO.TBL_SECTION.SECT_ID), DBO.TBL_SECTION.SECT_NAME
	--FROM         DBO.TBL_DEPT INNER JOIN
  --           	           DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
  --                      DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID			
	--WHERE TBL_DEPT.DEPT_ID = 
	--	(SELECT     DBO.TBL_SECTION.DEPT_ID
	--              FROM DBO.TBL_DEPT INNER JOIN
  --      	              DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
  --                     	 DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
	--	WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
	--	)
	--)
	--END
END


IF @AUTHORITY = 3 -- PROJECT_USER
	(SELECT     TBL_PROJECT_DIST.SECT_ID, TBL_SECTION.SECT_NAME
	 FROM			MICADEBUG.DBO.TBL_DEPT 
	 INNER JOIN		MICADEBUG.DBO.TBL_PROJECT_DIST ON TBL_DEPT.DEPT_ID = TBL_PROJECT_DIST.DEPT_ID 
	 INNER JOIN		MICADEBUG.DBO.TBL_SECTION ON TBL_DEPT.DEPT_ID = TBL_SECTION.DEPT_ID 
	 INNER JOIN		MICADEBUG.DBO.TBL_EMP ON TBL_SECTION.SECT_ID = TBL_EMP.SECT_ID 
	 INNER JOIN		MICADEBUG.DBO.TBL_PROJECT ON TBL_PROJECT_DIST.JOBCODE_ID = TBL_PROJECT.JOBCODE_ID
	 WHERE			(TBL_EMP.EMP_ID = @EMP_ID)
	)


IF @AUTHORITY = 4 -- HRB_USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 5 OR @EMP_ID = '0244' -- HRB_SECTION_USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 6 -- PAS_USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 7 -- PAS_SECTION_USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)
	
IF @AUTHORITY = 8 OR @AUTHORITY = 10-- MANAGEMENT USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 9 -- MH CONTROL USER
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION WHERE SECT_ID NOT IN ('E100'))

IF @AUTHORITY = 10 -- MANAGEMENT
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)

IF @AUTHORITY = 10 -- Top Management
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)


IF @AUTHORITY = 11 -- FAD
	(SELECT SECT_ID, SECT_NAME FROM MICADEBUG.dbo.TBL_SECTION)


IF @AUTHORITY = 12 -- SECRETARY
	(SELECT     TBL_EMP.SECT_ID, TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.dbo.TBL_EMP 
	INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_EMP.SECT_ID = TBL_SECTION.SECT_ID WHERE EMP_ID = @EMP_ID)


-- IF @AUTHORITY = 10 -- DEPUTY MANAGER
-- 	(SELECT     DISTINCT(DBO.TBL_SECTION.SECT_ID), DBO.TBL_SECTION.SECT_NAME
-- 	FROM         DBO.TBL_DEPT INNER JOIN
--              	           DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
--                         DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID			
-- 	WHERE TBL_DEPT.DEPT_ID = 
-- 		(SELECT     DBO.TBL_SECTION.DEPT_ID
-- 	              FROM DBO.TBL_DEPT INNER JOIN
--         	              DBO.TBL_SECTION ON DBO.TBL_DEPT.DEPT_ID = DBO.TBL_SECTION.DEPT_ID INNER JOIN
--                        	 DBO.TBL_EMP ON DBO.TBL_SECTION.SECT_ID = DBO.TBL_EMP.SECT_ID
-- 		WHERE     (DBO.TBL_EMP.EMP_ID = @EMP_ID)
-- 		)
-- 	)
--
  
  END


GO
/****** Object:  StoredProcedure [dbo].[Stp_OT_Emp_FirstRater]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[Stp_OT_Emp_FirstRater] 
(@EMP_ID AS CHAR(4))
AS
(SELECT     Jobcode_id, MICADEBUG.dbo.Funct_ProjectName(Jobcode_id) AS project_Name, Client_Jobcode, Sect_Id, 
		RIGHT(RTRIM(LTRIM(CATEGORY_ID)),Len(RTRIM(LTRIM(CATEGORY_ID)))-4) AS CATEGORY_ID,
		FirstRater, SecondRater
FROM         MICADEBUG.dbo.TBL_OVERTIMEREQ_APP Where FirstRater = @EMP_ID)

GO
/****** Object:  StoredProcedure [dbo].[Stp_OT_Emp_SecondRater]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[Stp_OT_Emp_SecondRater]
(@EMP_ID AS CHAR(4))
AS
SELECT     Jobcode_id, MICADEBUG.dbo.Funct_ProjectName(Jobcode_id) AS project_Name, Client_Jobcode, Sect_Id, 
	RIGHT(RTRIM(LTRIM(CATEGORY_ID)),Len(RTRIM(LTRIM(CATEGORY_ID)))-4) AS CATEGORY_ID,
	FirstRater, SecondRater
FROM         MICADEBUG.dbo.TBL_OVERTIMEREQ_APP
 Where SecondRater = @EMP_ID

GO
/****** Object:  StoredProcedure [dbo].[stp_OvertimeCheckAuthority]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procEDURE [dbo].[stp_OvertimeCheckAuthority]
(
@EMP_ID VARCHAR(4)
)
AS
SELECT	a.[USER_ID]
		,a.MODUL_ID
		,a.AUTHORITY
		,a.GROUP_ID 
FROM MICADEBUG.dbo.TBL_USER_MS a WHERE a.[USER_ID] = @EMP_ID

GO
/****** Object:  StoredProcedure [dbo].[Stp_OvertimeDeptList_AsProjectNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[Stp_OvertimeDeptList_AsProjectNew]
(
@Emp_id as char(4),
@Approvalrater as int, 	-- 0:All ; 1:SPV ; 2:PM
@App_Type as int,	-- 0:Structural ; 1:Project ; 9:All;
@Jobcode_id as Varchar(20)
)
AS

--Declare @Emp_id as char(4)
--Declare @Approvalrater as int 	-- 0:All ; 1:SPV ; 2:PM
--Declare @App_Type as int	-- 0:Structural ; 1:Project ; 9:All;
--Declare @Status as Char(100)	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
--Declare @Month as int		
--Declare @Year as int
--Declare @Period as int		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
--Declare @Jobcode_id as Varchar(20)
--Declare @Sect_Id as Char(4)

--Set @Emp_Id = '0658'
--Set @ApprovalRater = 2
--Set @App_Type = 9
--Set @Status = ' 0, 1, 2, 3'
--Set @Month = 3
--Set @Year = 2008
--Set @Period = 0
--Set @Jobcode_Id = 'ALL'
--Set @Sect_Id = 'ALL'

DECLARE @TempTable TABLE
(
DEPT_ID varchar(50),
DEPT_NAME varchar(50)
)

Declare @Strsql1 as varchar(1000)
Declare @Strsql2 as varchar(1000)
Declare @Strsql as varchar(1000)
Declare @StrFilter1 as varchar(1000)
Declare @StrFilter2 as varchar(1000)
Declare @StrFilter as varchar(1000)


SET @STRFILTER = '  '
IF @JOBCODE_ID <> 'ALL' AND @JOBCODE_ID <> '' SET @STRFILTER = @STRFILTER + ' AND TBL_PROJECT.JOBCODE_ID = ''' + @JOBCODE_ID + ''''
IF @APP_TYPE <> 9 SET @STRFILTER = @STRFILTER + ' AND ISNULL(TBL_PROJECT.APP_TYPE,0) = ' + Cast(@APP_TYPE as char(1))


IF @ApprovalRater = 1 or @ApprovalRater = 0
Begin
	Set @Strsql1 = 
	'SELECT DISTINCT 
	 TBL_DEPT.DEPT_ID, TBL_DEPT.DEPT_NAME
	FROM         MICADEBUG.dbo.TBL_PROJECT INNER JOIN
                      MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE INNER JOIN
                      MICADEBUG.dbo.TBL_SECTION ON TBL_PROJECT_FWBS.SECT_ID = TBL_SECTION.SECT_ID INNER JOIN
                      MICADEBUG.dbo.TBL_DEPT ON TBL_SECTION.DEPT_ID = TBL_DEPT.DEPT_ID
	WHERE KEYPERSON = ''' + @EMP_ID + ''''

	Set @Strsql1 = @strsql1 + ' ' + @StrFilter

End


IF @ApprovalRater = 2 or @ApprovalRater = 0
Begin
	Set @Strsql2 = 
	'SELECT DISTINCT 
	 TBL_DEPT.DEPT_ID, TBL_DEPT.DEPT_NAME
	FROM         MICADEBUG.dbo.TBL_PROJECT INNER JOIN
                      MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE INNER JOIN
                      MICADEBUG.dbo.TBL_SECTION ON TBL_PROJECT_FWBS.SECT_ID = TBL_SECTION.SECT_ID INNER JOIN
                      MICADEBUG.dbo.TBL_DEPT ON TBL_SECTION.DEPT_ID = TBL_DEPT.DEPT_ID
	WHERE KEYP_EMP_ID = ''' + @EMP_ID + ''''

	Set @Strsql2 = @strsql2 + @StrFilter
End

IF @ApprovalRater = 0 Set @Strsql = @Strsql1 + ' UNION ' + @Strsql2
If @ApprovalRater = 1 Set @StrSql = @Strsql1 
If @ApprovalRater = 2 Set @StrSql = @Strsql2 

--Select @Strsql
insert into @TempTable
Exec (@strSql)
select * from @TempTable


GO
/****** Object:  StoredProcedure [dbo].[Stp_OvertimeEmpList_AsProjectNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[Stp_OvertimeEmpList_AsProjectNew]
(
@Emp_id as char(4),
@Approvalrater as int, 	-- 0:All ; 1:SPV ; 2:PM
@App_Type as int,	-- 0:Structural ; 1:Project ; 9:All;
@Jobcode_id as Varchar(20),
@DEPT_ID AS CHAR(4),
@SECT_ID AS CHAR(4)
)
AS

--Declare @Emp_id as char(4)
--Declare @Approvalrater as int 	-- 0:All ; 1:SPV ; 2:PM
--Declare @App_Type as int	-- 0:Structural ; 1:Project ; 9:All;
--Declare @Status as Char(100)	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
--Declare @Month as int		
--Declare @Year as int
--Declare @Period as int		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
--Declare @Jobcode_id as Varchar(20)
--Declare @Sect_Id as Char(4)

--Set @Emp_Id = '0658'
--Set @ApprovalRater = 2
--Set @App_Type = 9
--Set @Status = ' 0, 1, 2, 3'
--Set @Month = 3
--Set @Year = 2008
--Set @Period = 0
--Set @Jobcode_Id = 'ALL'
--Set @Sect_Id = 'ALL'
DECLARE @TempTable TABLE
(
EMP_ID		VARCHAR(5),
EMP_NAME	VARCHAR(50),
FULL_NAME	VARCHAR(50)
)

Declare @Strsql1 as varchar(1000)
Declare @Strsql2 as varchar(1000)
Declare @Strsql as varchar(1000)
Declare @StrFilter1 as varchar(1000)
Declare @StrFilter2 as varchar(1000)
Declare @StrFilter as varchar(1000)


SET @STRFILTER = '  '
IF @JOBCODE_ID <> 'ALL' AND @JOBCODE_ID <> '' SET @STRFILTER = @STRFILTER + ' AND TBL_PROJECT.JOBCODE_ID = ''' + @JOBCODE_ID + ''''
IF @APP_TYPE <> 9 SET @STRFILTER = @STRFILTER + ' AND ISNULL(TBL_PROJECT.APP_TYPE,0) = ' + Cast(@APP_TYPE as char(1))
IF @DEPT_ID <> '' AND RTRIM(LTRIM(@DEPT_ID)) <> 'ALL'   SET @STRFILTER = @STRFILTER + ' AND MICADEBUG.dbo.Funct_WhatDiv(EMP_ID) = ''' + @DEPT_ID + ''''
IF @SECT_ID <> '' AND RTRIM(LTRIM(@SECT_ID)) <> 'ALL'   SET @STRFILTER = @STRFILTER + ' AND MICADEBUG.dbo.Funct_WhatSect(EMP_ID) = ''' + @SECT_ID + ''''


IF @ApprovalRater = 1 or @ApprovalRater = 0
Begin
	Set @Strsql1 = 
	'SELECT DISTINCT 
	TBL_MH_PLAN.EMP_ID, MICADEBUG.dbo.FUNCT_NAME(TBL_MH_PLAN.EMP_ID) AS EMP_NAME, ''['' + TBL_MH_PLAN.EMP_ID   + ''] '' + '' '' +  MICADEBUG.dbo.FUNCT_NAME(TBL_MH_PLAN.EMP_ID) AS FULL_NAME
	FROM        MICADEBUG.dbo.TBL_PROJECT 
	INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE 
	INNER JOIN	MICADEBUG.dbo.TBL_MH_PLAN ON TBL_PROJECT_FWBS.CLIENT_JOBCODE = TBL_MH_PLAN.CLIENT_JOBCODE AND 
                      TBL_PROJECT_FWBS.CATEGORY_ID = TBL_MH_PLAN.CATEGORY_ID AND 
                      TBL_PROJECT_FWBS.SECT_ID = TBL_MH_PLAN.SECT_ID AND 
                      TBL_PROJECT_FWBS.JOBCODE_ID = TBL_MH_PLAN.JOBCODE_ID	
	WHERE  KEYPERSON = ''' + @EMP_ID + ''''

	Set @Strsql1 = @strsql1 + ' ' + @StrFilter

End


IF @ApprovalRater = 2 or @ApprovalRater = 0
Begin
	Set @Strsql2 = 
	'SELECT DISTINCT 
	TBL_MH_PLAN.EMP_ID, MICADEBUG.dbo.FUNCT_NAME(TBL_MH_PLAN.EMP_ID) AS EMP_NAME, ''['' + TBL_MH_PLAN.EMP_ID   + ''] '' + '' '' +  MICADEBUG.dbo.FUNCT_NAME(TBL_MH_PLAN.EMP_ID) AS FULL_NAME
	FROM        MICADEBUG.dbo.TBL_PROJECT 
	INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE 
	INNER JOIN	MICADEBUG.dbo.TBL_MH_PLAN ON TBL_PROJECT_FWBS.CLIENT_JOBCODE = TBL_MH_PLAN.CLIENT_JOBCODE AND 
                      TBL_PROJECT_FWBS.CATEGORY_ID = TBL_MH_PLAN.CATEGORY_ID AND 
                      TBL_PROJECT_FWBS.SECT_ID = TBL_MH_PLAN.SECT_ID AND 
                      TBL_PROJECT_FWBS.JOBCODE_ID = TBL_MH_PLAN.JOBCODE_ID	
	WHERE KEYP_EMP_ID = ''' + @EMP_ID + ''''

	Set @Strsql2 = @strsql2 + @StrFilter
End

IF @ApprovalRater = 0 Set @Strsql = @Strsql1 + ' UNION ' + @Strsql2
If @ApprovalRater = 1 Set @StrSql = @Strsql1 
If @ApprovalRater = 2 Set @StrSql = @Strsql2 

--Select @Strsql
insert into @TempTable
Exec (@strSql)
select * from @TempTable

GO
/****** Object:  StoredProcedure [dbo].[Stp_OvertimeProjectList_AsProjectNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_OvertimeProjectList_AsProjectNew]
(
@Emp_id as char(4),
@Approvalrater as int, 	-- 0:All ; 1:SPV ; 2:PM
@App_Type as int,	-- 0:Structural ; 1:Project ; 9:All;
@Jobcode_id as Varchar(20)
)
AS

--Declare @Emp_id as char(4)
--Declare @Approvalrater as int 	-- 0:All ; 1:SPV ; 2:PM
--Declare @App_Type as int	-- 0:Structural ; 1:Project ; 9:All;
--Declare @Status as Char(100)	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
--Declare @Month as int		
--Declare @Year as int
--Declare @Period as int		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
--Declare @Jobcode_id as Varchar(20)
--Declare @Sect_Id as Char(4)

--Set @Emp_Id = '0658'
--Set @ApprovalRater = 2
--Set @App_Type = 9
--Set @Status = ' 0, 1, 2, 3'
--Set @Month = 3
--Set @Year = 2008
--Set @Period = 0
--Set @Jobcode_Id = 'ALL'
--Set @Sect_Id = 'ALL'

DECLARE @TempTable TABLE
(
JOBCODE_ID varchar(50),
PROJECT_NAME varchar(200),
APP_TYPE varchar(50),
JOBCODE varchar(50)
)

Declare @Strsql1 as varchar(1000)
Declare @Strsql2 as varchar(1000)
Declare @Strsql as varchar(1000)
Declare @StrFilter1 as varchar(1000)
Declare @StrFilter2 as varchar(1000)
Declare @StrFilter as varchar(1000)




SET @STRFILTER = '  '
IF @JOBCODE_ID <> 'ALL' AND @JOBCODE_ID <> '' SET @STRFILTER = @STRFILTER + ' AND TBL_PROJECT.JOBCODE_ID = ''' + @JOBCODE_ID + ''
IF @APP_TYPE <> 9 SET @STRFILTER = @STRFILTER + ' AND ISNULL(TBL_PROJECT.APP_TYPE,0) = ' + Cast(@APP_TYPE as char(1))


IF @ApprovalRater = 1 or @ApprovalRater = 0
Begin
	Set @Strsql1 = 
	'SELECT     DISTINCT TBL_PROJECT.JOBCODE_ID, TBL_PROJECT.PROJECT_NAME, TBL_PROJECT.APP_TYPE, TBL_PROJECT.JOBCODE_ID AS JOBCODE
	FROM         MICADEBUG.dbo.TBL_PROJECT INNER JOIN
                      MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE AND 
                      TBL_PROJECT.JOBCODE_ID = TBL_PROJECT_FWBS.JOBCODE_ID
	WHERE KEYPERSON = ''' + @EMP_ID + ''''

	Set @Strsql1 = @strsql1 + ' ' + @StrFilter

End


IF @ApprovalRater = 2 or @ApprovalRater = 0
Begin
	Set @Strsql2 = 
	'SELECT     DISTINCT TBL_PROJECT.JOBCODE_ID, TBL_PROJECT.PROJECT_NAME, TBL_PROJECT.APP_TYPE, TBL_PROJECT.JOBCODE_ID AS JOBCODE
FROM         MICADEBUG.dbo.TBL_PROJECT INNER JOIN
                      MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE AND 
                      TBL_PROJECT.JOBCODE_ID = TBL_PROJECT_FWBS.JOBCODE_ID
	WHERE KEYP_EMP_ID = ''' + @EMP_ID + ''''

	Set @Strsql2 = @strsql2 + @StrFilter
End

IF @ApprovalRater = 0 Set @Strsql = @Strsql1 + ' UNION ' + @Strsql2
If @ApprovalRater = 1 Set @StrSql = @Strsql1 
If @ApprovalRater = 2 Set @StrSql = @Strsql2 

--Select @Strsql
print @strSql
insert into @TempTable
Exec (@strSql)
select * from @TempTable



GO
/****** Object:  StoredProcedure [dbo].[Stp_OvertimeRequestListNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Created By Wied, March 13, 2008

--Stp_OvertimeRequestList '0266',2,1,'0',11,2011,0,'','','ALL','ALL','ALL'
CREATE PROCEDURE [dbo].[Stp_OvertimeRequestListNew] 
(
@Emp_id as char(4),
@Approvalrater as int, 	-- 0:All ; 1:SPV ; 2:PM
@App_Type as int,	-- 0:Structural ; 1:Project ; 9:All;
@Status as Char(100),	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
@Month as int,		
@Year as int,
@Period as int,		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
@Date as Datetime,
@Search_Emp_Id as char(4),
@Jobcode_Id as Varchar(20),
@Sect_Id as Char(4)
,@Dept_Id as Char(4)
)
AS


--Declare @Emp_id as char(4)
--Declare @Approvalrater as int 	-- 0:All ; 1:SPV ; 2:PM
--Declare @App_Type as int	-- 0:Structural ; 1:Project ; 9:All;
--Declare @Status as Char(100)	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
--Declare @Month as int		
--Declare @Year as int
--Declare @Period as int		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
-- Declare @Date as Datetime
-- Declare @Search_Emp_Id as char(4)
-- Declare @Jobcode_Id as Varchar(20)
-- Declare @Sect_Id as Char(4)
-- Declare @Dept_Id as Char(4)

--Set @Emp_Id = '0815'
--Set @ApprovalRater = 1
--Set @App_Type = 1
--Set @Status = ' 0, 1, 2'
--Set @Month = 2
--Set @Year = 2008
--Set @Period = 0

DECLARE @TempTable TABLE
(
REQUEST_ID varchar(20),
EMP_ID varchar(4),
EMPLOYEE_NAME varchar(50),
JOBCODE_ID varchar(50),
OT_DATE datetime,
OT_TIME datetime, 
REMARKS varchar(300),
APPROVALSECT varchar(4),
APPROVALDEPT varchar(4),
REQUESTED_DATE datetime,
STATUS varchar(1),
PROJECT_NAME varchar(50),
CARD_IN datetime,
CARD_OUT datetime,
FIRSTRATER varchar(50),
SECONDRATER varchar(50)
)

Declare @Strsql1 as varchar(MAX)
Declare @Strsql2 as varchar(max)
Declare @Strsql as varchar(max)
Declare @StrFilter1 as varchar(1000)
Declare @StrFilter2 as varchar(1000)
Declare @StrFilter as varchar(1000)

Set @Strfilter = ' '
Set @Strfilter1 = ' '
Set @Strfilter2 = ' '
IF ISNULL(@Status, '') <> '' Set @StrFilter = @StrFilter + ' And Status  in (' + @Status + ')'
Set @StrFilter = @StrFilter + ' And Month(OT_DATE) = ' + Cast(@Month as char(2))
Set @StrFilter = @StrFilter + ' And Year(OT_DATE) = ' + cast(@Year as char(4))
If @Period = 1 Set @StrFilter = @StrFilter + ' And Day(OT_DATE) Between 1 and 10 '
If @Period = 2 Set @StrFilter = @StrFilter + ' And Day(OT_DATE) Between 11 and 20 '
If @Period = 3 Set @StrFilter = @StrFilter + ' And Day(OT_DATE) Between 21 and 31 '
If @App_Type <> 9 Set @StrFilter = @StrFilter + ' And App_Type = ' + Cast(@App_type as char(1))
IF @Search_Emp_Id <> '' Set @StrFilter = @StrFilter + ' And Emp_Id = ''' + @Search_Emp_Id + ''''
--If @Date <> '' Set @StrFilter = @StrFilter + ' And OT_DATE = ' + Cast(@Date as Varchar(20))
If @Date <> '' Set @StrFilter = @StrFilter + ' And Day(OT_DATE) = ' + Cast(day(@DATE) as char(2))
IF @Jobcode_Id <> 'ALL' AND @Jobcode_Id <> '' SET @StrFilter = @StrFilter + ' AND V1_OVERTIMEREQ_1.JOBCODE_ID = ''' + @Jobcode_Id + ''''
IF @Sect_Id <> 'ALL' AND  @Sect_Id <> '' SET @StrFilter = @StrFilter + ' AND V1_OVERTIMEREQ_1.SECT_ID = ''' + @Sect_Id + ''''
IF @Dept_Id <> 'ALL' AND  @Dept_Id <> '' SET @StrFilter = @StrFilter + ' AND V1_OVERTIMEREQ_1.DEPT_ID = ''' + @Dept_Id + ''''

IF @ApprovalRater = 1  or @ApprovalRater = 0
Begin
	print 1
	SET @STRSQL1 = 'SELECT      REQUEST_ID, EMP_ID, MICADEBUG.dbo.FUNCT_NAME(EMP_ID) AS EMPLOYEE_NAME, JOBCODE_ID, OT_DATE, OT_TIME, REMARKS, APPROVALSECT, APPROVALDEPT, 
                      REQUESTED_DATE, STATUS, MICADEBUG.dbo.funct_projectname(JOBCODE_ID) AS PROJECT_NAME, CARD_IN, CARD_OUT, ISNULL(FIRSTRATER,'''') AS FIRSTRATER, ISNULL(SECONDRATER,'''') AS SECONDRATER  FROM         MICADEBUG.dbo.V1_OVERTIMEREQ_1
		WHERE ISNULL(FIRSTRATER,'''') = ''' + @Emp_Id + ''''
	Set @Strsql1 = @StrSql1 + ' ' + @StrFilter
End
IF @ApprovalRater = 2 or @ApprovalRater = 0
Begin
	print 2
	SET @STRSQL2 = 'SELECT      REQUEST_ID, EMP_ID, MICADEBUG.dbo.FUNCT_NAME(EMP_ID) AS EMPLOYEE_NAME, JOBCODE_ID, OT_DATE, OT_TIME, REMARKS, APPROVALSECT, APPROVALDEPT, 
                      REQUESTED_DATE, STATUS, MICADEBUG.dbo.funct_projectname(JOBCODE_ID) AS PROJECT_NAME, CARD_IN, CARD_OUT, FIRSTRATER, SECONDRATER  FROM         MICADEBUG.dbo.V1_OVERTIMEREQ_1
		WHERE ISNULL(SECONDRATER,'''') = ''' + @Emp_Id + ''''
	Set @Strsql2 = @StrSql2 + ' ' + @StrFilter
End
If @ApprovalRater = 0 Set @StrSql = @Strsql1 + ' UNION '+ @Strsql2
If @ApprovalRater = 1 Set @StrSql = @Strsql1 
If @ApprovalRater = 2 Set @StrSql = @Strsql2 

--Select @Strsql
insert into @TempTable
Exec (@strSql)
select ROW_NUMBER() over(order by REQUEST_ID asc) as No, * from @TempTable
print @strSql



GO
/****** Object:  StoredProcedure [dbo].[Stp_OvertimeSectList_AsProjectNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[Stp_OvertimeSectList_AsProjectNew]
(
@Emp_id as char(4),
@Approvalrater as int, 	-- 0:All ; 1:SPV ; 2:PM
@App_Type as int,	-- 0:Structural ; 1:Project ; 9:All;
@Jobcode_id as Varchar(20),
@DEPT_ID AS CHAR(4)
)
AS

--Declare @Emp_id as char(4)
--Declare @Approvalrater as int 	-- 0:All ; 1:SPV ; 2:PM
--Declare @App_Type as int	-- 0:Structural ; 1:Project ; 9:All;
--Declare @Status as Char(100)	-- 0:All ; 1:New ; 2:Awaiting ; 3:Approved
--Declare @Month as int		
--Declare @Year as int
--Declare @Period as int		-- 0:All ; 1:Period 1 ; 2:Period 2 ; 3:Period 3
--Declare @Jobcode_id as Varchar(20)
--Declare @Sect_Id as Char(4)

--Set @Emp_Id = '0658'
--Set @ApprovalRater = 2
--Set @App_Type = 9
--Set @Status = ' 0, 1, 2, 3'
--Set @Month = 3
--Set @Year = 2008
--Set @Period = 0
--Set @Jobcode_Id = 'ALL'
--Set @Sect_Id = 'ALL'

DECLARE @TempTable TABLE
(
SECT_ID		varchar(10),
SECT_NAME	varchar(50)
)

Declare @Strsql1 as varchar(1000)
Declare @Strsql2 as varchar(1000)
Declare @Strsql as varchar(1000)
Declare @StrFilter1 as varchar(1000)
Declare @StrFilter2 as varchar(1000)
Declare @StrFilter as varchar(1000)


SET @STRFILTER = '  '
IF @JOBCODE_ID <> 'ALL' AND @JOBCODE_ID <> '' SET @STRFILTER = @STRFILTER + ' AND TBL_PROJECT.JOBCODE_ID = ''' + @JOBCODE_ID + ''''
IF @APP_TYPE <> 9 SET  @STRFILTER = @STRFILTER + ' AND ISNULL(TBL_PROJECT.APP_TYPE,0) = ' + Cast(@APP_TYPE as char(1))
IF @DEPT_ID <> '' AND RTRIM(LTRIM(@DEPT_ID)) <> 'ALL'   SET @STRFILTER = @STRFILTER + ' AND ISNULL(TBL_DEPT.DEPT_ID,'''') = ''' + @DEPT_ID + ''''


IF @ApprovalRater = 1 or @ApprovalRater = 0
Begin
	Set @Strsql1 = 
	'SELECT DISTINCT 
                      TBL_SECTION.SECT_ID, TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.dbo.TBL_PROJECT 
	INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE 
	INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_PROJECT_FWBS.SECT_ID = TBL_SECTION.SECT_ID 
	INNER JOIN	MICADEBUG.dbo.TBL_DEPT ON TBL_SECTION.DEPT_ID = TBL_DEPT.DEPT_ID
	WHERE KEYPERSON = ''' + @EMP_ID + ''''

	Set @Strsql1 = @strsql1 + ' ' + @StrFilter

End


IF @ApprovalRater = 2 or @ApprovalRater = 0
Begin
	Set @Strsql2 = 
	'SELECT DISTINCT 
                      TBL_SECTION.SECT_ID, TBL_SECTION.SECT_NAME
	FROM        MICADEBUG.dbo.TBL_PROJECT 
	INNER JOIN	MICADEBUG.dbo.TBL_PROJECT_FWBS ON TBL_PROJECT.CLIENT_JOBCODE = TBL_PROJECT_FWBS.CLIENT_JOBCODE 
	INNER JOIN	MICADEBUG.dbo.TBL_SECTION ON TBL_PROJECT_FWBS.SECT_ID = TBL_SECTION.SECT_ID 
	INNER JOIN	MICADEBUG.dbo.TBL_DEPT ON TBL_SECTION.DEPT_ID = TBL_DEPT.DEPT_ID
	WHERE KEYP_EMP_ID = ''' + @EMP_ID + ''''

	Set @Strsql2 = @strsql2 + @StrFilter
End

IF @ApprovalRater = 0 Set @Strsql = @Strsql1 + ' UNION ' + @Strsql2
If @ApprovalRater = 1 Set @StrSql = @Strsql1 
If @ApprovalRater = 2 Set @StrSql = @Strsql2 

--Select @Strsql
insert into @TempTable
Exec (@strSql)
select * from @TempTable

GO
/****** Object:  StoredProcedure [dbo].[Stp_Sel_Delegation_App]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_Sel_Delegation_App]
(
@Emp_Id as char(4),
@Type as char(2)
)
AS
BEGIN
	SELECT [EMP_ID], [REP_OF], '[' + [REP_OF] + '] ' + dbo.Funct_name(REP_OF) AS REP_NAME, [STARTDATE], [ENDDATE]
	FROM [PFNATT].[dbo].[TBL_DELEGATION_APP]
	WHERE EMP_ID = @EMP_ID and TYPE = @TYPE
	AND STARTDATE <= GETDATE() AND ENDDATE >= GETDATE()
	UNION
	SELECT @EMP_ID, @EMP_ID, '[' + @EMP_ID + '] ' + dbo.Funct_name(@EMP_ID) AS REP_NAME, '2007-1-1' AS STARTDATE, '2010-12-31' AS ENDDATE


END

GO
/****** Object:  StoredProcedure [dbo].[Stp_SelectLeaveRequest]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_SelectLeaveRequest]
(@MONTH int, @YEAR int, @EMP_ID char(4), @STATUS char(10))
WITH 
EXECUTE AS CALLER
AS
DECLARE @STRSQL as Varchar(8000)
DECLARE @STRFILTER as Varchar(8000)
DECLARE @LSTRFILTER as int

SET @STRSQL =
'SELECT     LEAVE_REQUEST, EMP_ID, CLASS_TYPE, START_DATE, END_DATE, TOTAL_DAY, STATUS, REMARKS, dbo.FUNCT_NAME2(APP_DEPT_BY) AS APP_DEPT_BY, 
dbo.FUNCT_NAME2(APP_DIV_BY) AS APP_DIV_BY, Isnull(ADV_YEAR,'''') as ADV_Year FROM         TBL_LEAVE_REQUEST'

SET @STRFILTER = ' '
IF @MONTH <> 0 SET @STRFILTER = @STRFILTER +  ' AND MONTH(START_DATE) = ' + CAST(@MONTH AS CHAR(2))
SET @STRFILTER = @STRFILTER +  ' AND YEAR(START_DATE) = ' + CAST(@YEAR AS CHAR(4))
IF @EMP_ID <> 'ALL' SET @STRFILTER = @STRFILTER +  ' AND EMP_ID = ''' + @EMP_ID + ''''
IF @STATUS <> 'ALL' SET @STRFILTER = @STRFILTER +  ' AND STATUS IN (' + @STATUS + ')'
SET @STRFILTER = @STRFILTER +  ' AND STATUS < 100 ORDER BY STATUS ASC, EMP_ID ASC, END_DATE DESC'

SET @LSTRFILTER = (LEN(RTRIM(LTRIM(@STRFILTER))))
IF @STRFILTER <> '' 
BEGIN
	SET @STRFILTER = ( RIGHT(@STRFILTER, @LSTRFILTER - 4))
	SET @STRSQL = @STRSQL + ' WHERE ' + @STRFILTER 

END

EXEC (@STRSQL)

GO
/****** Object:  StoredProcedure [dbo].[Stp_SelectLeaveRequest2]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Stp_SelectLeaveRequest2]
(@YEAR int)

AS

SELECT   LEAVE_REQUEST, EMP_ID, CLASS_TYPE, START_DATE, END_DATE, TOTAL_DAY, STATUS, REMARKS, 
dbo.FUNCT_NAME2(APP_DEPT_BY) AS APP_DEPT_BY, 
dbo.FUNCT_NAME2(APP_DIV_BY) AS APP_DIV_BY, 
Isnull(ADV_YEAR,'') as ADV_Year 
FROM   TBL_LEAVE_REQUEST
WHERE YEAR(START_DATE) = @YEAR AND STATUS < 100 ORDER BY STATUS ASC, EMP_ID ASC, END_DATE DESC


GO
/****** Object:  StoredProcedure [dbo].[Stp_SelectLeaveRequestById]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Stp_SelectLeaveRequestById]
@LeaveId varchar(20)

AS

SELECT   LEAVE_REQUEST, EMP_ID, CLASS_TYPE, START_DATE, END_DATE, TOTAL_DAY, STATUS, REMARKS, 
dbo.FUNCT_NAME2(APP_DEPT_BY) AS APP_DEPT_BY, 
dbo.FUNCT_NAME2(APP_DIV_BY) AS APP_DIV_BY, 
Isnull(ADV_YEAR,'') as ADV_Year 
FROM   TBL_LEAVE_REQUEST
where TBL_LEAVE_REQUEST.LEAVE_REQUEST = @LeaveId AND STATUS < 100


GO
/****** Object:  StoredProcedure [dbo].[STP_SETEMPINACTIVE]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[STP_SETEMPINACTIVE]
	@EMP_ID VARCHAR(10),
	@END_DATE datetime,
	@UPDATED_BY AS VARCHAR(10)
AS
BEGIN

	-- INSERT INTO HISTORICAL DATA (!UNCOMMENT IN PRODUCTION)
	EXEC STP_INSERTHISTORICALDATA 'UPDATE','EMPLOYEE',@UPDATED_BY, @EMP_ID, '', '', '', '', '0', '', '', '', '',''


	UPDATE TBL_EMP 
	SET 
	END_DATE = @END_DATE,
	STATUS = 0,
	RESIGNED_BY = @UPDATED_BY,
	MODIFIED = GETDATE(),
	TEST1 = null
	WHERE EMP_ID = @EMP_ID
	
	--DELETE FROM MICA AUTHENTICATION TABEL (!UNCOMMENT IN PRODUCTION)
	DELETE FROM TBL_USER_MS WHERE USER_ID = @EMP_ID

	-- RUN ALERT SYSTEM MICA (!UNCOMMENT IN PRODUCTION)
	EXEC Stp_AlertJINDUserResignation @EMP_ID
END


GO
/****** Object:  StoredProcedure [dbo].[Stp_TimeAttendanceConversion_Leave]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM TBL_LEAVE_REQUEST WHERE EMP_ID = '0514'
CREATE PROCEDURE [dbo].[Stp_TimeAttendanceConversion_Leave]
@EMP_ID char(4), @START_DATE datetime, @END_DATE datetime, @CLASS_TYPE char(2)
WITH EXEC AS CALLER
AS
DECLARE @TR_DATE AS DATETIME
DECLARE @i as int
DECLARE @month as int
DECLARE @Year as int
DECLARE @Loc_Id as char(2)
DECLARE @IsHoliday as int
DECLARE @POS_ID AS CHAR(3)
DECLARE @INSERT_OK AS INT

SET @INSERT_OK = 0

SET @TR_DATE = @START_DATE

DECLARE @AB INTEGER
DECLARE @AL INTEGER
DECLARE @OL INTEGER
DECLARE @HL INTEGER
DECLARE @SL INTEGER
DECLARE @EL INTEGER
DECLARE @UL INTEGER
DECLARE @SI INTEGER
DECLARE @BT INTEGER
DECLARE @SA INTEGER
DECLARE @AD INTEGER

WHILE @TR_DATE <= @END_DATE
BEGIN
	IF NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE)
	BEGIN
		SET @I = DAY(@TR_DATE)
		SET @MONTH = MONTH(@TR_DATE)
		SET @YEAR = YEAR(@TR_DATE)
		SET @LOC_ID = (SELECT ISNULL(LOC_ID,'00') AS LOC_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
		SET @POS_ID = (SELECT ISNULL(POSITION_ID,'') AS POS_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID)
		IF (@POS_ID NOT IN ('OSS', 'OSH'))
		BEGIN --1
			SET @IsHoliday = (SELECT dbo.FUNCT_ISHOLIDAY1(@I, @Month, @Year, @Loc_Id))
			IF @IsHoliday = 0 SET @INSERT_OK = 1
		END --1
		ELSE SET @INSERT_OK = 1 -- @POS_ID = 'OSS'

		SELECT @I, @MONTH, @YEAR, @LOC_ID, @POS_ID, @INSERT_OK
		
		IF @INSERT_OK = 1
			--SELECT 'INSERT', @TR_DATE
      -- KHUSUS TGL 31, WORK HALF DAY
      IF @TR_DATE = '31-DEC-2013'
      BEGIN
        INSERT INTO TBL_CARD_TR 
 			  (	CARD_ID,TR_DATE,CARD_IN,CARD_OUT,CARD_STATUS,CARDR_IN,CARDR_OUT,CARDR_STATUS,
 	       			CL,EF,IA,REG,OT,HOL,TOTAL,EMP_ID,EXTTRANS,LOC_ID,AL,OL,HL,SL,EL,
    		  		UL,SI,AB,CLSTAT,EFSTAT,BT,
 	     			ATT_TYPE_ID,
   				TOTALDEDUCT, CREATED_BY, TYPE_ATT, WORKTYPE
	     	  		)
		     	  	VALUES
   			  	(	@EMP_ID,@TR_DATE,NULL,NULL,0,NULL,NULL,'',0,0,
   	    				0,0,0,0,0,ISNULL(@EMP_ID,''),0,
     					'00',0,0,0,0,0,0,0,1,0,0,0,
     					@CLASS_TYPE,210, 'LA','SYSTEM','1'
     				)
      END
      ELSE
      BEGIN
 			  INSERT INTO TBL_CARD_TR 
 			  (	CARD_ID,TR_DATE,CARD_IN,CARD_OUT,CARD_STATUS,CARDR_IN,CARDR_OUT,CARDR_STATUS,
 	       			CL,EF,IA,REG,OT,HOL,TOTAL,EMP_ID,EXTTRANS,LOC_ID,AL,OL,HL,SL,EL,
    		  		UL,SI,AB,CLSTAT,EFSTAT,BT,
 	     			ATT_TYPE_ID,
   				TOTALDEDUCT, CREATED_BY, TYPE_ATT, WORKTYPE
	     	  		)
		     	  	VALUES
   			  	(	@EMP_ID,@TR_DATE,NULL,NULL,0,NULL,NULL,'',0,0,
   	    				0,0,0,0,0,ISNULL(@EMP_ID,''),0,
     					'00',0,0,0,0,0,0,0,1,0,0,0,
     					@CLASS_TYPE,480, 'LA','SYSTEM','1'
     				)
      END
	END

	ELSE
	BEGIN 
	--IF EXISTS (SELECT EMP_ID FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE)
		--SELECT 'UPDATE', @TR_DATE
		SET @AB = 0
		SET @AL = 0
		SET @OL = 0
		SET @HL = 0
		SET @SL = 0
		SET @EL = 0
		SET @UL = 0
		SET @SI = 0
		SET @BT = 0
		SET @SA = 0
		SET @AD = 0
		
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'AB'  SET @AB = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'AL'  SET @AL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'OL'  SET @OL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'HL'  SET @HL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'SL'  SET @SL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'EL'  SET @EL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'UL'  SET @UL = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'SI'  SET @SI = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'BT'  SET @BT = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'SA'  SET @SA = 1
	IF LTRIM(RTRIM(@CLASS_TYPE)) = 'AD'  SET @AD = 1


		UPDATE TBL_CARD_TR 
		SET ATT_TYPE_ID = @CLASS_TYPE,
			CREATED_BY = 'LA',
			--Tambahan 
			LASTUPDATE = Getdate(),
			-- Ary --
			AL = ISNULL(@AL,0),
			OL = ISNULL(@OL,0),
			HL = ISNULL(@HL,0),
			SL = ISNULL(@SL,0),
			EL = ISNULL(@EL,0),
			UL = ISNULL(@UL,0),
			SI = ISNULL(@SI,0),
			AB = ISNULL(@AB,0),
			BT = ISNULL(@BT,0),
			SA = ISNULL(@SA,0),
			AD = ISNULL(@AD,0)
			
			WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE
	END
	SET @TR_DATE = (SELECT DATEADD(DAY, 1, @TR_DATE))
	SET @INSERT_OK = 0

END

GO
/****** Object:  StoredProcedure [dbo].[stp_timeAttendanceConversionABS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from tbl_card_tr where created_by = 'ABS_STAT'

--SELECT * FROM TBL_CARD_TR WHERE EMP_ID = '1138' ORDER BY TR_DATE DESC
-- DELETE FROM tbl_card_tr WHERE EMP_ID = '9022'

-- select * from tbl_emp where FIRSTNAME LIKE 'HUSIN%' = '0145'
-- select * from tbl_card_tr where emp_id = '0145' order by tr_date desc
-- stp_timeAttendanceConversionABS '6/4/2007'
CREATE PROCEDURE [dbo].[stp_timeAttendanceConversionABS]
@TR_DATE datetime
WITH EXEC AS CALLER
AS
DECLARE @EMP_ID VARCHAR(4)
DECLARE @TGLTR DATETIME
DECLARE @CURRENTDATE DATETIME
DECLARE @CARD_ID VARCHAR(4)
DECLARE @FIRSTNAME VARCHAR(50)
DECLARE @MONTH INT
DECLARE @YEAR INT
DECLARE @ASSIGN_LOCID VARCHAR(4)
--sp_timeAttendanceConversionABS '6/1/2006'
SET @CURRENTDATE = CAST(MONTH(GETDATE()) AS VARCHAR) + '/' + CAST(DAY(GETDATE()) AS VARCHAR) + '/' + CAST(YEAR(GETDATE()) AS VARCHAR)
IF MONTH(@CURRENTDATE) = 1
BEGIN
	SET @MONTH = 12
	SET @YEAR = YEAR(@CURRENTDATE) - 1
END
ELSE
BEGIN
	SET @MONTH = MONTH(@CURRENTDATE) - 1
	SET @YEAR = YEAR(@CURRENTDATE)
END
IF YEAR(@TR_DATE) = 1900 
	SET @TGLTR = @CURRENTDATE
ELSE
	SET @TGLTR = @TR_DATE

SET DATEFIRST  1

SET NOCOUNT ON
PRINT @TGLTR
print @CURRENTDATE
DECLARE CC_CURSOR CURSOR FOR 
	--SELECT DISTINCT EMP_ID,FIRSTNAME FROM TBL_EMP WHERE ((LOC_ID = '00' AND
	SELECT DISTINCT EMP_ID,FIRSTNAME FROM TBL_EMP WHERE ((LOC_ID in (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND
		EMP_ID NOT IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL = @TGLTR)
		AND EMP_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL <= @TGLTR)
		AND EMP_ID IN (SELECT EMP_ID FROM TBL_EMP WHERE START_DATE <= @TGLTR 
			AND (YEAR(isnull(END_DATE,'1/1/1900')) = 1900 OR @TGLTR < END_DATE))
		AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_CARD_TR WHERE TR_DATE = @TGLTR)
		AND STATUS = '1')
  	OR (STATUS = '1' AND EMP_ID IN 
  		(SELECT EMP_ID FROM TBL_SAVING_CARD WHERE @TGLTR BETWEEN SAVING_DATE_FROM AND SAVING_DATE_TO
  			AND SAVING_CARD_ID NOT IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL = @TGLTR)
  			AND SAVING_CARD_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL <= @TGLTR)
			AND EMP_ID IN (SELECT EMP_ID FROM TBL_EMP WHERE START_DATE <= @TGLTR 
			AND (YEAR(isnull(END_DATE,'1/1/1900')) = 1900 OR @TGLTR < END_DATE) AND STATUS = '1')
  			AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_CARD_TR WHERE TR_DATE = @TGLTR))))
	AND @TGLTR <= GETDATE()
	AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_EMP WHERE POSITION_ID = 'OSS'  AND STATUS = '1')
	AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_EMP WHERE SECT_ID IN ('N920','N910') AND STATUS = '1')

--	OR (LOC_ID <> '00' AND EMP_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE MONTH(TGL) = MONTH(@TGLTR)))
--select * from tbl_emp where emp_id  = '0216'
 OPEN CC_CURSOR
 FETCH NEXT FROM CC_CURSOR INTO @EMP_ID,@FIRSTNAME
 if EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
 print DATEPART(WEEKDAY,@TGLTR)  
 WHILE @@FETCH_STATUS = 0
 BEGIN
 	IF DATEPART(WEEKDAY,@TGLTR) <= 5 AND @TGLTR <= @CURRENTDATE
 	BEGIN
 		print '1 ' + @emp_id
 		SELECT @CARD_ID = SAVING_CARD_ID FROM TBL_SAVING_CARD WHERE EMP_ID = @EMP_ID AND @TGLTR BETWEEN SAVING_DATE_FROM AND SAVING_DATE_TO
 				AND STATUS = 0
  		IF ISNULL(@CARD_ID,'') = '' SET @CARD_ID = @EMP_ID
 
  		IF (EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND YEAR(TR_DATE) = YEAR(@TGLTR) AND MONTH(TR_DATE) = MONTH(@TGLTR)) AND
			NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND DATEPART(WEEKDAY,@TGLTR) <= 5)
			--AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND STATUS = '1')
			--AND (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24'))
			--OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND YEAR(END_DATE) = 1900)))
			AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND STATUS = '1')
			AND (@TGLTR < (SELECT isnull(END_DATE,'1/1/1900') FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
			OR EXISTS (SELECT isnull(END_DATE,'1/1/1900') FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND YEAR(isnull(END_DATE,'1/1/1900')) = 1900)))
		   OR 
			(NOT EXISTS (SELECT * FROM TBL_CARD_TR  WHERE EMP_ID = @EMP_ID AND YEAR(TR_DATE) = YEAR(@TGLTR) AND MONTH(TR_DATE) = MONTH(@TGLTR))
			 --AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND STATUS = '1')
			--AND  (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24'))
			--OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND YEAR(END_DATE) = 1900)))	
			AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND STATUS = '1')
			AND  (@TGLTR < (SELECT isnull(END_DATE,'1/1/1900') FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
			OR EXISTS (SELECT isnull(END_DATE,'1/1/1900') FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND YEAR(isnull(END_DATE,'1/1/1900')) = 1900)))

			-- OR @TGLTR > (SELECT MAX(TR_DATE) FROM TBL_CARD_TR WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID)))
  		BEGIN
 			print '1-x ' + @emp_id
			SELECT @ASSIGN_LOCID = ISNULL(LOC_ID,'') FROM TBL_EMP_ASSIGNMENT X1 WHERE X1.EMP_ID=@EMP_ID AND @TGLTR BETWEEN X1.START_MOB AND X1.END_MOB
			PRINT 'ASS_LOC_ID = ' + @ASSIGN_LOCID
			/*IF LTRIM(RTRIM(@ASSIGN_LOCID)) <> '00'
			BEGIN
			  IF NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR) AND DATEPART(WEEKDAY,@TGLTR) <= 5 
				AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TGLTR AND LOC_ID = @ASSIGN_LOCID)
				AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
				AND NOT EXISTS 
					--(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN ('00','24'))
					(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
  			BEGIN
				  print CONVERT(VARCHAR,@TGLTR) + ' - ' + @ASSIGN_LOCID
  				PRINT @EMP_ID + ' - ' + @FIRSTNAME 
   		 		INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					@ASSIGN_LOCID,
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					480, 'ABS','SYSTEM', GETDATE()
     			  	)
   			END
			END
			ELSE
			BEGIN*/
  			IF NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR) AND DATEPART(WEEKDAY,@TGLTR) <= 5 
				AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TGLTR AND LOC_ID = '00')
				AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
				AND NOT EXISTS 
					--(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN ('00','24'))
					(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
  			BEGIN
				  print @TGLTR
  				PRINT @EMP_ID + ' - ' + @FIRSTNAME 
          
          IF @TGLTR = '31-DEC-2015'
          BEGIN
          INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					'00',
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					240, 'ABS','SYSTEM', GETDATE()
     			  	)
          END
          ELSE
          BEGIN
   		 		INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					'00',
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					480, 'ABS','SYSTEM', GETDATE()
     			  	)
          END
   			END
			--END
  		END
 
 	END
 	FETCH NEXT FROM CC_CURSOR INTO @EMP_ID,@FIRSTNAME
 END
 CLOSE CC_CURSOR
 DEALLOCATE CC_CURSOR
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [dbo].[stp_timeAttendanceConversionEmpABS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from tbl_card_tr where created_by = 'ABS_STAT'

--SELECT * FROM TBL_CARD_TR WHERE EMP_ID = '1138' ORDER BY TR_DATE DESC
-- DELETE FROM tbl_card_tr WHERE EMP_ID = '9022'

-- select * from tbl_emp where FIRSTNAME LIKE 'HUSIN%' = '0145'
-- select * from tbl_card_tr where emp_id = '0145' order by tr_date desc
-- stp_timeAttendanceConversionABS '6/4/2007'
CREATE PROCEDURE [dbo].[stp_timeAttendanceConversionEmpABS]
@TR_DATE datetime, @EMP_ID char(6)
WITH EXEC AS CALLER
AS
--DECLARE @EMP_ID VARCHAR(4)
DECLARE @TGLTR DATETIME
DECLARE @CURRENTDATE DATETIME
DECLARE @CARD_ID VARCHAR(4)
DECLARE @FIRSTNAME VARCHAR(50)
DECLARE @MONTH INT
DECLARE @YEAR INT
DECLARE @ASSIGN_LOCID VARCHAR(4)
--sp_timeAttendanceConversionABS '6/1/2006'
SET @CURRENTDATE = CAST(MONTH(GETDATE()) AS VARCHAR) + '/' + CAST(DAY(GETDATE()) AS VARCHAR) + '/' + CAST(YEAR(GETDATE()) AS VARCHAR)
IF MONTH(@CURRENTDATE) = 1
BEGIN
	SET @MONTH = 12
	SET @YEAR = YEAR(@CURRENTDATE) - 1
END
ELSE
BEGIN
	SET @MONTH = MONTH(@CURRENTDATE) - 1
	SET @YEAR = YEAR(@CURRENTDATE)
END
IF YEAR(@TR_DATE) = 1900 
	SET @TGLTR = @CURRENTDATE
ELSE
	SET @TGLTR = @TR_DATE

SET DATEFIRST  1

SET NOCOUNT ON
PRINT @TGLTR
print @CURRENTDATE
DECLARE CC_CURSOR CURSOR FOR 
	--SELECT DISTINCT EMP_ID,FIRSTNAME FROM TBL_EMP WHERE ((LOC_ID = '00' AND
	SELECT DISTINCT EMP_ID,FIRSTNAME FROM TBL_EMP WHERE ((LOC_ID in (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND
		EMP_ID NOT IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL = @TGLTR)
		AND EMP_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL <= @TGLTR)
		AND EMP_ID IN (SELECT EMP_ID FROM TBL_EMP WHERE START_DATE <= @TGLTR 
			AND (YEAR(END_DATE) = 1900 OR @TGLTR < END_DATE))
		AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_CARD_TR WHERE TR_DATE = @TGLTR)
		AND STATUS = '1')
  	OR (STATUS = '1' AND EMP_ID IN 
  		(SELECT EMP_ID FROM TBL_SAVING_CARD WHERE @TGLTR BETWEEN SAVING_DATE_FROM AND SAVING_DATE_TO
  			AND SAVING_CARD_ID NOT IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL = @TGLTR)
  			AND SAVING_CARD_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE TGL <= @TGLTR)
			AND EMP_ID IN (SELECT EMP_ID FROM TBL_EMP WHERE START_DATE <= @TGLTR 
			AND (YEAR(END_DATE) = 1900 OR @TGLTR < END_DATE) AND STATUS = '1')
  			AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_CARD_TR WHERE TR_DATE = @TGLTR))))
	AND @TGLTR <= GETDATE()
	AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_EMP WHERE POSITION_ID = 'OSS'  AND STATUS = '1')
	AND EMP_ID NOT IN (SELECT EMP_ID FROM TBL_EMP WHERE SECT_ID IN ('N920','N910') AND STATUS = '1')
  AND EMP_ID = @EMP_ID

--	OR (LOC_ID <> '00' AND EMP_ID IN (SELECT EMPNO FROM TBL_LOGPINTU WHERE MONTH(TGL) = MONTH(@TGLTR)))
--select * from tbl_emp where emp_id  = '0216'
 OPEN CC_CURSOR
 FETCH NEXT FROM CC_CURSOR INTO @EMP_ID,@FIRSTNAME
 if EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
 print DATEPART(WEEKDAY,@TGLTR)  
 WHILE @@FETCH_STATUS = 0
 BEGIN
 	IF DATEPART(WEEKDAY,@TGLTR) <= 5 AND @TGLTR <= @CURRENTDATE
 	BEGIN
 		print '1 ' + @emp_id
 		SELECT @CARD_ID = SAVING_CARD_ID FROM TBL_SAVING_CARD WHERE EMP_ID = @EMP_ID AND @TGLTR BETWEEN SAVING_DATE_FROM AND SAVING_DATE_TO
 				AND STATUS = 0
  		IF ISNULL(@CARD_ID,'') = '' SET @CARD_ID = @EMP_ID
 
  		IF (EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND YEAR(TR_DATE) = YEAR(@TGLTR) AND MONTH(TR_DATE) = MONTH(@TGLTR)) AND
			NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND DATEPART(WEEKDAY,@TGLTR) <= 5)
			--AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND STATUS = '1')
			--AND (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24'))
			--OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND YEAR(END_DATE) = 1900)))
			AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND STATUS = '1')
			AND (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
			OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND YEAR(END_DATE) = 1900)))
		   OR 
			(NOT EXISTS (SELECT * FROM TBL_CARD_TR  WHERE EMP_ID = @EMP_ID AND YEAR(TR_DATE) = YEAR(@TGLTR) AND MONTH(TR_DATE) = MONTH(@TGLTR))
			 --AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND STATUS = '1')
			--AND  (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24'))
			--OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN ('00','24') AND YEAR(END_DATE) = 1900)))	
			AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND STATUS = '1')
			AND  (@TGLTR < (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
			OR EXISTS (SELECT END_DATE FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND LOC_ID IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1) AND YEAR(END_DATE) = 1900)))

			-- OR @TGLTR > (SELECT MAX(TR_DATE) FROM TBL_CARD_TR WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID)))
  		BEGIN
 			print '1 ' + @emp_id
			SELECT @ASSIGN_LOCID = ISNULL(LOC_ID,'') FROM TBL_EMP_ASSIGNMENT X1 WHERE X1.EMP_ID=@EMP_ID AND @TGLTR BETWEEN X1.START_MOB AND X1.END_MOB
			PRINT 'ASS_LOC_ID = ' + @ASSIGN_LOCID
			/*IF LTRIM(RTRIM(@ASSIGN_LOCID)) <> '00'
			BEGIN
			  IF NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR) AND DATEPART(WEEKDAY,@TGLTR) <= 5 
				AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TGLTR AND LOC_ID = @ASSIGN_LOCID)
				AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
				AND NOT EXISTS 
					--(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN ('00','24'))
					(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
  			BEGIN
				  print CONVERT(VARCHAR,@TGLTR) + ' - ' + @ASSIGN_LOCID
  				PRINT @EMP_ID + ' - ' + @FIRSTNAME 
   		 		INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					@ASSIGN_LOCID,
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					480, 'ABS','SYSTEM', GETDATE()
     			  	)
   			END
			END
			ELSE
			BEGIN*/
  			IF NOT EXISTS (SELECT * FROM TBL_CARD_TR WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR) AND DATEPART(WEEKDAY,@TGLTR) <= 5 
				AND NOT EXISTS (SELECT * FROM TBL_HOL_TR WHERE HOL_DATE = @TGLTR AND LOC_ID = '00')
				AND EXISTS (SELECT * FROM TBL_EMP WHERE EMP_ID = @EMP_ID AND START_DATE <= @TGLTR)
				AND NOT EXISTS 
					--(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN ('00','24'))
					(SELECT * FROM TBL_TIMESHEET WHERE EMP_ID = @EMP_ID AND TR_DATE = @TGLTR AND LOC_ID NOT IN (SELECT loc_id FROM TBL_LOCATION WHERE PROC_ATT=1))
  			BEGIN
				  print @TGLTR
  				PRINT @EMP_ID + ' - ' + @FIRSTNAME 
          
          IF @TGLTR = '31-DEC-2013'
          BEGIN
          INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					'00',
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					210, 'ABS','SYSTEM', GETDATE()
     			  	)
          END
          ELSE
          BEGIN
   		 		INSERT INTO TBL_CARD_TR 
     				(	CARD_ID,
       					TR_DATE,
   	    				CARD_IN,
     			  		CARD_OUT,
       					CARD_STATUS,
       					CARDR_IN,
   	  	  			CARDR_OUT,
     		  			CARDR_STATUS,
       					CL,
     	  				EF,
       					IA,
   	  				REG,
     					OT,
     					HOL,
       					TOTAL,
     					EMP_ID,
   	  				EXTTRANS,
     					LOC_ID,
     					AL,OL,HL,SL,EL,
   	  				UL,SI,AB,CLSTAT,EFSTAT,BT,
     					ATT_TYPE_ID,
  					TOTALDEDUCT, CREATED_BY, TYPE_ATT, CREATED_DATE
     	  			)
     	  			VALUES
   	  		  	(	@EMP_ID,
       					@TGLTR,
       					NULL,
     		  			NULL,
       					0,
   	    				NULL,
     		  			NULL,
     	  				'',
       					0,
     	  				0,
   	    				0,	
     					0,
     					0,
     					0,
   	    				0,
     					ISNULL(@EMP_ID,''),
     					0,
     					'00',
     					0,0,0,0,0,
   	  				0,0,1,0,0,0,
     					'AB',
  					480, 'ABS','SYSTEM', GETDATE()
     			  	)
          END
   			END
			--END
  		END
 
 	END
 	FETCH NEXT FROM CC_CURSOR INTO @EMP_ID,@FIRSTNAME
 END
 CLOSE CC_CURSOR
 DEALLOCATE CC_CURSOR
SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [dbo].[stp_updateOvertimeReq]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--CREATE PROCEDURE stp_updateOvertimeReq
--(
--@REQUEST_ID VARCHAR(20),
--@EMP_ID VARCHAR(4),
--@REQUESTER VARCHAR(4),
--@JOBCODE_ID VARCHAR(100),
--@OT_DATE VARCHAR(20),
--@OT_TIME VARCHAR(5),
--@REMARKS VARCHAR(100),
--@STATUS CHAR(1),
--@MSG CHAR(100) OUTPUT
--)
--AS

--DECLARE @STATUSNOW CHAR(1)
--DECLARE @DBOT_TIME DATETIME
--DECLARE @OTTIME DATETIME
--DECLARE @CARDIN DATETIME
--DECLARE @PROCESS BIT
--DECLARE @JOBID VARCHAR(20)
--DECLARE @CLIENID VARCHAR(20)
--DECLARE @SECTID VARCHAR(10)

--SET @MSG = ''
--SET @OTTIME = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--SELECT @CARDIN = CARD_IN FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
--IF CONVERT(VARCHAR(5),@OTTIME,108) <= CONVERT(VARCHAR(5),@CARDIN,108)  AND CONVERT(VARCHAR(101),@OTTIME,101) = CONVERT(VARCHAR(101),@CARDIN,101)
--	SET @OTTIME = DATEADD(DAY,1,@OTTIME)

--IF EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
--BEGIN
--	SELECT @STATUS = STATUS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--	SELECT @DBOT_TIME = OT_TIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--	SELECT @PROCESS = PROCESS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID

--	IF CHARINDEX('|',@JOBCODE_ID) > 0
--	BEGIN
--		SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--		SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--		IF CHARINDEX('|',@JOBCODE_ID) > 0	
--		BEGIN
--			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--			SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--		END
--		ELSE
--		BEGIN
--			SET @CLIENID = @JOBCODE_ID
--			SET @SECTID = ''
--		END
--	END

--	IF CONVERT(VARCHAR(5),@OTTIME,108)  <> CONVERT(VARCHAR(5),@DBOT_TIME,108) AND 
--		NOT EXISTS (SELECT * FROM TBL_USER_MS WHERE GROUP_ID = 'G000' AND USER_ID = @REQUESTER)
--		--	OR @EMP_ID = @REQUESTER)
--	BEGIN
--		SELECT @STATUS = STATUS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--		IF @STATUS = '1'  
--		BEGIN
--			IF CHARINDEX('REVISED',@REMARKS) > 0
--			BEGIN
--				SELECT @DBOT_TIME = APPROVED_OTTIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
--				BEGIN
--					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1)
--					SET @STATUS = '3'
--					SET @PROCESS = 1
--				END
--				ELSE
--				BEGIN
--					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1) + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--					SET @STATUS = '1'
--					SET @PROCESS = 0
--				END
--			END
--			ELSE
--			BEGIN

--				SELECT @DBOT_TIME = APPROVED_OTTIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
--				BEGIN
--					SET @REMARKS = @REMARKS
--					SET @STATUS = '3'
--					SET @PROCESS = 1
--				END
--				ELSE
--				BEGIN
--					SET @REMARKS = ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--					SET @STATUS = '1'
--					SET @PROCESS = 0
--				END
--			END
 
--		END
--		ELSE
--		BEGIN
--			SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--			SET @STATUS = '1'
--			SET @PROCESS = 0
--		END
		
--		EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
---- 		@TYPE 
---- 		@MODULE 
---- 		@USER_ID 
---- 		@F1 EMPLOYEE ID
---- 		@F2 REQUEST ID
---- 		@F3 JOBCODE
---- 		@F4 CLIENT JOBCODE
---- 		@F5 STATUS
---- 		@F6 REMARKS
---- 		@F7 PROCESS
---- 		@F8 SECTION
---- 		@F9 OT DATE
---- 		@F10 OT TIME
---- 		@COMPUTER_NAME 


--		UPDATE  TBL_OVERTIMEREQ	SET
--			JOBCODE_ID = ISNULL(@JOBID,''),
--			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
--			SECT_ID = ISNULL(@SECTID,''),
--			OT_DATE = @OT_DATE,
--			OT_TIME = @OTTIME,
--			REMARKS = @REMARKS,
--			APPROVALSECT = '',
--			APPROVALDEPT = '',
--			PROCESS = @PROCESS,

--			STATUS = @STATUS
--		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
--	END
--	ELSE
--	BEGIN
--		EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
--		UPDATE  TBL_OVERTIMEREQ	SET
--			JOBCODE_ID = ISNULL(@JOBID,''),
--			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
--			SECT_ID = ISNULL(@SECTID,''),
--			OT_DATE = @OT_DATE,
--			OT_TIME = @OTTIME,
--			REMARKS = @REMARKS,
--			STATUS = @STATUS,
--			PROCESS = @PROCESS
--		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
--	END
--	SET @MSG = 'Request overtime Updated !'
--END
--ELSE
--	SET @MSG = 'Request overtime NOT Exists !'






























































CREATE PROCEDURE [dbo].[stp_updateOvertimeReq]
@REQUEST_ID varchar(20), @EMP_ID varchar(4), @REQUESTER varchar(4), @JOBCODE_ID varchar(100), @OT_DATE varchar(20), @OT_TIME varchar(5), @REMARKS varchar(100), @STATUS char(1), @MSG char(100) OUTPUT
WITH EXEC AS CALLER
AS
DECLARE @STATUSNOW CHAR(1)
DECLARE @DBOT_TIME DATETIME
DECLARE @OTTIME DATETIME
DECLARE @CARDIN DATETIME
DECLARE @PROCESS BIT
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @SECTID VARCHAR(20)
DECLARE @CATID VARCHAR(20)
DECLARE @FWBS VARCHAR(10)

SET @MSG = ''
SET @OTTIME = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
SELECT @CARDIN = CARD_IN FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
IF CONVERT(VARCHAR(5),@OTTIME,108) <= CONVERT(VARCHAR(5),@CARDIN,108)  AND CONVERT(VARCHAR(101),@OTTIME,101) = CONVERT(VARCHAR(101),@CARDIN,101)
	SET @OTTIME = DATEADD(DAY,1,@OTTIME)

IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
BEGIN
	SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	SELECT @DBOT_TIME = OT_TIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	SELECT @PROCESS = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID

	IF CHARINDEX('|',@JOBCODE_ID) > 0
	BEGIN
		SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
		IF CHARINDEX('|',@JOBCODE_ID) > 0	
		BEGIN

--			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--			SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
			SET @SECTID = LEFT(SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID)),4)
			SET @CATID = RIGHT(@JOBCODE_ID,CHARINDEX('|',REVERSE(@JOBCODE_ID))-1)
			SET @FWBS = RIGHT(LTRIM(RTRIM(@CATID)),LEN(LTRIM(RTRIM(@CATID)))-LEN(@SECTID))

		END
		ELSE
		BEGIN
			SET @CLIENID = @JOBCODE_ID
			SET @SECTID = ''
		END
	END

	IF CONVERT(VARCHAR(5),@OTTIME,108)  <> CONVERT(VARCHAR(5),@DBOT_TIME,108) AND 
		NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_USER_MS WHERE GROUP_ID = 'G000' AND USER_ID = @REQUESTER)
		--	OR @EMP_ID = @REQUESTER)
	BEGIN
		SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
		IF @STATUS = '1'  
		BEGIN
			IF CHARINDEX('REVISED',@REMARKS) > 0
			BEGIN
				SELECT @DBOT_TIME = APPROVED_OTTIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
				BEGIN
					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1)
					SET @STATUS = '3'
					SET @PROCESS = 1
				END
				ELSE
				BEGIN
					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1) + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
					SET @STATUS = '1'
					SET @PROCESS = 0
				END
			END
			ELSE
			BEGIN

				SELECT @DBOT_TIME = APPROVED_OTTIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
				BEGIN
					SET @REMARKS = @REMARKS
					SET @STATUS = '3'
					SET @PROCESS = 1
				END
				ELSE
				BEGIN
					SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
					SET @STATUS = '1'
					SET @PROCESS = 0
				END
			END
 
		END
		ELSE
		BEGIN
			SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
			SET @STATUS = '1'
			SET @PROCESS = 0
		END
		
		--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
-- 		@TYPE 
-- 		@MODULE 
-- 		@USER_ID 
-- 		@F1 EMPLOYEE ID
-- 		@F2 REQUEST ID
-- 		@F3 JOBCODE
-- 		@F4 CLIENT JOBCODE
-- 		@F5 STATUS
-- 		@F6 REMARKS
-- 		@F7 PROCESS
-- 		@F8 SECTION
-- 		@F9 OT DATE
-- 		@F10 OT TIME
-- 		@COMPUTER_NAME 


		UPDATE  MICADEBUG.dbo.TBL_OVERTIMEREQ	SET
			JOBCODE_ID = ISNULL(@JOBID,''),
			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
			SECT_ID = ISNULL(@SECTID,''),
			CATEGORY_ID = ISNULL(@CATID,''),
			FWBS = @FWBS,
			OT_DATE = @OT_DATE,
			OT_TIME = @OTTIME,
			REMARKS = @REMARKS,
			APPROVALSECT = '',
			APPROVALDEPT = '',
			PROCESS = @PROCESS,
			APP_TYPE = (MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,''))),
			FIRSTRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1)),
			SECONDRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2)),

			STATUS = @STATUS
		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
	END
	ELSE
	BEGIN
		--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
		UPDATE  MICADEBUG.dbo.TBL_OVERTIMEREQ	SET
			JOBCODE_ID = ISNULL(@JOBID,''),
			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
			SECT_ID = ISNULL(@SECTID,''),
			CATEGORY_ID = ISNULL(@CATID,''),
			FWBS = @FWBS,
			OT_DATE = @OT_DATE,
			OT_TIME = @OTTIME,
			REMARKS = @REMARKS,
			STATUS = @STATUS,
			PROCESS = @PROCESS,
			FIRSTRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1)),
			SECONDRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2)),
			APP_TYPE = (MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,'')))
		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
	END
	SET @MSG = 'Request overtime Updated !'
END
ELSE
	SET @MSG = 'Request overtime NOT Exists !'

GO
/****** Object:  StoredProcedure [dbo].[stp_updateOvertimeReqBySPV]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_updateOvertimeReqBySPV]
(
@REQUEST_ID VARCHAR(20),
@OT_DATE DATETIME,
@OT_TIME VARCHAR(5)
)
AS
DECLARE @REMARKS VARCHAR(100)
DECLARE @OTTIME DATETIME

SELECT @REMARKS = REMARKS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
SELECT @OTTIME = OT_TIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
IF CONVERT(VARCHAR(5),ISNULL(@OTTIME,''),108) <> @OT_TIME
	SET @REMARKS = LTRIM(LTRIM(ISNULL(@REMARKS,''))) + ' [Updated By SECT/DEPT Manager From ' + CONVERT(VARCHAR(5),ISNULL(@OTTIME,''),108) + ']'
ELSE
	SET @REMARKS = @REMARKS

SET @OTTIME = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + LTRIM(RTRIM(@OT_TIME))

IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
BEGIN
	UPDATE  MICADEBUG.dbo.TBL_OVERTIMEREQ	SET
			OT_TIME = @OTTIME,
			REMARKS = @REMARKS
	WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
END
--exec sp_updateOvertimeReqBySPV 'OT-010206-0858','02-jan-06','22:00' 

GO
/****** Object:  StoredProcedure [dbo].[stp_updateOvertimeReqNew]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--CREATE PROCEDURE stp_updateOvertimeReq
--(
--@REQUEST_ID VARCHAR(20),
--@EMP_ID VARCHAR(4),
--@REQUESTER VARCHAR(4),
--@JOBCODE_ID VARCHAR(100),
--@OT_DATE VARCHAR(20),
--@OT_TIME VARCHAR(5),
--@REMARKS VARCHAR(100),
--@STATUS CHAR(1),
--@MSG CHAR(100) OUTPUT
--)
--AS

--DECLARE @STATUSNOW CHAR(1)
--DECLARE @DBOT_TIME DATETIME
--DECLARE @OTTIME DATETIME
--DECLARE @CARDIN DATETIME
--DECLARE @PROCESS BIT
--DECLARE @JOBID VARCHAR(20)
--DECLARE @CLIENID VARCHAR(20)
--DECLARE @SECTID VARCHAR(10)

--SET @MSG = ''
--SET @OTTIME = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
--SELECT @CARDIN = CARD_IN FROM TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
--IF CONVERT(VARCHAR(5),@OTTIME,108) <= CONVERT(VARCHAR(5),@CARDIN,108)  AND CONVERT(VARCHAR(101),@OTTIME,101) = CONVERT(VARCHAR(101),@CARDIN,101)
--	SET @OTTIME = DATEADD(DAY,1,@OTTIME)

--IF EXISTS (SELECT * FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
--BEGIN
--	SELECT @STATUS = STATUS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--	SELECT @DBOT_TIME = OT_TIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--	SELECT @PROCESS = PROCESS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID

--	IF CHARINDEX('|',@JOBCODE_ID) > 0
--	BEGIN
--		SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--		SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--		IF CHARINDEX('|',@JOBCODE_ID) > 0	
--		BEGIN
--			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--			SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
--		END
--		ELSE
--		BEGIN
--			SET @CLIENID = @JOBCODE_ID
--			SET @SECTID = ''
--		END
--	END

--	IF CONVERT(VARCHAR(5),@OTTIME,108)  <> CONVERT(VARCHAR(5),@DBOT_TIME,108) AND 
--		NOT EXISTS (SELECT * FROM TBL_USER_MS WHERE GROUP_ID = 'G000' AND USER_ID = @REQUESTER)
--		--	OR @EMP_ID = @REQUESTER)
--	BEGIN
--		SELECT @STATUS = STATUS FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--		IF @STATUS = '1'  
--		BEGIN
--			IF CHARINDEX('REVISED',@REMARKS) > 0
--			BEGIN
--				SELECT @DBOT_TIME = APPROVED_OTTIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
--				BEGIN
--					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1)
--					SET @STATUS = '3'
--					SET @PROCESS = 1
--				END
--				ELSE
--				BEGIN
--					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1) + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--					SET @STATUS = '1'
--					SET @PROCESS = 0
--				END
--			END
--			ELSE
--			BEGIN

--				SELECT @DBOT_TIME = APPROVED_OTTIME FROM TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
--				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
--				BEGIN
--					SET @REMARKS = @REMARKS
--					SET @STATUS = '3'
--					SET @PROCESS = 1
--				END
--				ELSE
--				BEGIN
--					SET @REMARKS = ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--					SET @STATUS = '1'
--					SET @PROCESS = 0
--				END
--			END
 
--		END
--		ELSE
--		BEGIN
--			SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
--			SET @STATUS = '1'
--			SET @PROCESS = 0
--		END
		
--		EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
---- 		@TYPE 
---- 		@MODULE 
---- 		@USER_ID 
---- 		@F1 EMPLOYEE ID
---- 		@F2 REQUEST ID
---- 		@F3 JOBCODE
---- 		@F4 CLIENT JOBCODE
---- 		@F5 STATUS
---- 		@F6 REMARKS
---- 		@F7 PROCESS
---- 		@F8 SECTION
---- 		@F9 OT DATE
---- 		@F10 OT TIME
---- 		@COMPUTER_NAME 


--		UPDATE  TBL_OVERTIMEREQ	SET
--			JOBCODE_ID = ISNULL(@JOBID,''),
--			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
--			SECT_ID = ISNULL(@SECTID,''),
--			OT_DATE = @OT_DATE,
--			OT_TIME = @OTTIME,
--			REMARKS = @REMARKS,
--			APPROVALSECT = '',
--			APPROVALDEPT = '',
--			PROCESS = @PROCESS,

--			STATUS = @STATUS
--		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
--	END
--	ELSE
--	BEGIN
--		EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
--		UPDATE  TBL_OVERTIMEREQ	SET
--			JOBCODE_ID = ISNULL(@JOBID,''),
--			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
--			SECT_ID = ISNULL(@SECTID,''),
--			OT_DATE = @OT_DATE,
--			OT_TIME = @OTTIME,
--			REMARKS = @REMARKS,
--			STATUS = @STATUS,
--			PROCESS = @PROCESS
--		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
--	END
--	SET @MSG = 'Request overtime Updated !'
--END
--ELSE
--	SET @MSG = 'Request overtime NOT Exists !'






























































CREATE PROCEDURE [dbo].[stp_updateOvertimeReqNew]
(@REQUEST_ID varchar(20), 
@EMP_ID varchar(4), 
@REQUESTER varchar(4), 
@JOBCODE_ID varchar(100), 
@OT_DATE varchar(20), 
@OT_TIME varchar(5), 
@REMARKS varchar(100), 
@STATUS char(1))
--WITH EXEC AS CALLER
AS
DECLARE @STATUSNOW CHAR(1)
DECLARE @DBOT_TIME DATETIME
DECLARE @OTTIME DATETIME
DECLARE @CARDIN DATETIME
DECLARE @PROCESS BIT
DECLARE @JOBID VARCHAR(20)
DECLARE @CLIENID VARCHAR(20)
DECLARE @SECTID VARCHAR(20)
DECLARE @CATID VARCHAR(20)
DECLARE @FWBS VARCHAR(10)
DECLARE @MSG char(100)

SET @MSG = ''
SET @OTTIME = CAST(MONTH(@OT_DATE) AS VARCHAR) + '/' + CAST(DAY(@OT_DATE) AS VARCHAR) + '/' + CAST(YEAR(@OT_DATE) AS VARCHAR) + ' ' + @OT_TIME
SELECT @CARDIN = CARD_IN FROM MICADEBUG.dbo.TBL_CARD_TR WHERE TR_DATE = @OT_DATE AND EMP_ID = @EMP_ID
IF CONVERT(VARCHAR(5),@OTTIME,108) <= CONVERT(VARCHAR(5),@CARDIN,108)  AND CONVERT(VARCHAR(101),@OTTIME,101) = CONVERT(VARCHAR(101),@CARDIN,101)
	SET @OTTIME = DATEADD(DAY,1,@OTTIME)

IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE)
BEGIN
	SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	SELECT @DBOT_TIME = OT_TIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
	SELECT @PROCESS = PROCESS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID

	IF CHARINDEX('|',@JOBCODE_ID) > 0
	BEGIN
		SET @JOBID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
		SET @JOBCODE_ID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
		IF CHARINDEX('|',@JOBCODE_ID) > 0	
		BEGIN

--			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
--			SET @SECTID = SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID))
			SET @CLIENID = LEFT(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)-1)
			SET @SECTID = LEFT(SUBSTRING(@JOBCODE_ID,CHARINDEX('|',@JOBCODE_ID)+1,LEN(@JOBCODE_ID)-CHARINDEX('|',@JOBCODE_ID)),4)
			SET @CATID = RIGHT(@JOBCODE_ID,CHARINDEX('|',REVERSE(@JOBCODE_ID))-1)
			SET @FWBS = RIGHT(LTRIM(RTRIM(@CATID)),LEN(LTRIM(RTRIM(@CATID)))-LEN(@SECTID))

		END
		ELSE
		BEGIN
			SET @CLIENID = @JOBCODE_ID
			SET @SECTID = ''
		END
	END

	IF CONVERT(VARCHAR(5),@OTTIME,108)  <> CONVERT(VARCHAR(5),@DBOT_TIME,108) AND 
		NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_USER_MS WHERE GROUP_ID = 'G000' AND USER_ID = @REQUESTER)
		--	OR @EMP_ID = @REQUESTER)
	BEGIN
		SELECT @STATUS = STATUS FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
		IF @STATUS = '1'  
		BEGIN
			IF CHARINDEX('REVISED',@REMARKS) > 0
			BEGIN
				SELECT @DBOT_TIME = APPROVED_OTTIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
				BEGIN
					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1)
					SET @STATUS = '3'
					SET @PROCESS = 1
				END
				ELSE
				BEGIN
					SET @REMARKS = LEFT(@REMARKS,CHARINDEX('{',@REMARKS)-1) + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
					SET @STATUS = '1'
					SET @PROCESS = 0
				END
			END
			ELSE
			BEGIN

				SELECT @DBOT_TIME = APPROVED_OTTIME FROM MICADEBUG.dbo.TBL_OVERTIMEREQ WHERE REQUEST_ID = @REQUEST_ID
				IF @OT_TIME = CONVERT(VARCHAR(5),@DBOT_TIME,108)
				BEGIN
					SET @REMARKS = @REMARKS
					SET @STATUS = '3'
					SET @PROCESS = 1
				END
				ELSE
				BEGIN
					SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
					SET @STATUS = '1'
					SET @PROCESS = 0
				END
			END
 
		END
		ELSE
		BEGIN
			SET @REMARKS = ISNULL(@REMARKS,'') + ' {REVISED - FROM [' + CONVERT(VARCHAR(5),@DBOT_TIME,108) + ']}'
			SET @STATUS = '1'
			SET @PROCESS = 0
		END
		
		--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
-- 		@TYPE 
-- 		@MODULE 
-- 		@USER_ID 
-- 		@F1 EMPLOYEE ID
-- 		@F2 REQUEST ID
-- 		@F3 JOBCODE
-- 		@F4 CLIENT JOBCODE
-- 		@F5 STATUS
-- 		@F6 REMARKS
-- 		@F7 PROCESS
-- 		@F8 SECTION
-- 		@F9 OT DATE
-- 		@F10 OT TIME
-- 		@COMPUTER_NAME 


		UPDATE  MICADEBUG.dbo.TBL_OVERTIMEREQ	SET
			JOBCODE_ID = ISNULL(@JOBID,''),
			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
			SECT_ID = ISNULL(@SECTID,''),
			CATEGORY_ID = ISNULL(@CATID,''),
			FWBS = @FWBS,
			OT_DATE = @OT_DATE,
			OT_TIME = @OTTIME,
			REMARKS = @REMARKS,
			APPROVALSECT = '',
			APPROVALDEPT = '',
			PROCESS = @PROCESS,
			APP_TYPE = (MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,''))),
			FIRSTRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1)),
			SECONDRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2)),

			STATUS = @STATUS
		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
	END
	ELSE
	BEGIN
		--HQJUN2011 EXEC STP_INSERTHISTORICALDATA 'UPDATE','OVERTIME',@REQUESTER, @EMP_ID, @REQUEST_ID, @JOBID, @CLIENID, @STATUS, @REMARKS, @PROCESS, @SECTID, @OT_DATE, @OTTIME, ''
		UPDATE  MICADEBUG.dbo.TBL_OVERTIMEREQ	SET
			JOBCODE_ID = ISNULL(@JOBID,''),
			CLIENT_JOBCODE = ISNULL(@CLIENID,''),
			SECT_ID = ISNULL(@SECTID,''),
			CATEGORY_ID = ISNULL(@CATID,''),
			FWBS = @FWBS,
			OT_DATE = @OT_DATE,
			OT_TIME = @OTTIME,
			REMARKS = @REMARKS,
			STATUS = @STATUS,
			PROCESS = @PROCESS,
			FIRSTRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 1)),
			SECONDRATER = (MICADEBUG.dbo.Funct_WhoMustApproved(@EMP_ID, ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@SECTID,''),ISNULL(@CATID,''), 2)),
			APP_TYPE = (MICADEBUG.dbo.Funct_ProjectAppType(ISNULL(@JOBID,''), ISNULL(@CLIENID,''), ISNULL(@CATID,'')))
		WHERE REQUEST_ID = @REQUEST_ID AND OT_DATE = @OT_DATE
	END
	SET @MSG = 'Request overtime Updated !'
END
ELSE
BEGIN
	SET @MSG = 'Request overtime NOT Exists !'
END

SELECT @MSG as Message

GO
/****** Object:  StoredProcedure [dbo].[Stp_UpdLeaveRequest]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Stp_UpdLeaveRequest]
@TYPE char(4), @LEAVE_REQUEST varchar(20), @CLASS_TYPE char(2), @START_DATE datetime, @END_DATE datetime, @TOTAL_DAY int, @STATUS int, @REMARKS varchar(100), @MGR char(4), @USER_ID char(4), @ADV_STATUS int, @MSG varchar(255) OUTPUT
WITH EXEC AS CALLER
AS
-- SET @TYPE = 'DEPT'
-- SET @LEAVE_REQUEST = 'LA-073109-0280'
-- SET @CLASS_TYPE = 'AL'
-- SET @START_DATE = '7/31/2009'
-- SET @END_DATE = '7/31/2009'
-- SET @TOTAL_DAY = 1
-- SET @STATUS = 1
-- SET @REMARKS = 'ANUAL LEAVE AB'
-- SET @MGR = ''
-- SET @USER_ID = '0302'
-- SET @MSG

DECLARE @EMP_ID AS CHAR(4)
DECLARE @QUOTA AS INT
DECLARE @QUOTA_AL AS INT
DECLARE @QUOTA_AD AS INT
DECLARE @DATE_COUNTER AS DATETIME
DECLARE @AUTH AS INT
DECLARE @CS_ServiceLength INT
DECLARE @EMPSTART_DATE AS DATETIME
DECLARE @ExtStatus AS INT
DECLARE @ADV_YEAR AS INT
DECLARE @IS_UPDATE_OK AS INT
DECLARE @SEND_TO_ATT AS INT
--DECLARE @LastYearRemain as int
--Declare @Exp_AL as int
--Declare @ExpiredAL as datetime
Declare @Quota_ExpAL as INT
DECLARE @REMAIN_AL_OL AS INT

IF @ADV_STATUS = 1 SET @ADV_YEAR = YEAR(GETDATE()) 
ELSE SET @ADV_YEAR = ''

--	set @LastYearRemain = (select isnull(LastYearRemain,0)-isnull(AL,0) as LastYearRemain from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
--	set @ExpiredAL = (select isnull(ExpiredAL,'1/1/1900') as ExpiredAL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
--	set @Exp_AL = (select isnull(Exp_AL,0) as Exp_AL from tbl_leave_allocation where emp_id=@Emp_Id and yeartr=@TYEAR)
	
--	PRINT @LastYearRemain
--	PRINT @Exp_AL
--	PRINT @ExpiredAL
	
--	if (@CLASS_TYPE='AL' or @CLASS_TYPE='OL') and (@TOTAL_DAY >= @Exp_AL) and (@Exp_AL > 0)
--	   begin
--		   declare @SelDay AS INT
--		   if (@START_DATE <= @ExpiredAL) 
--			    begin
--				    if (@END_DATE <= @ExpiredAL)
--					      set  @SelDay = dbo.Funct_LeaveDiffDays(@START_DATE,@END_DATE,@Emp_ID)
--					  else
--				        set  @SelDay = dbo.Funct_LeaveDiffDays(@START_DATE,@ExpiredAL,@Emp_ID)
								
						--update tbl_leave_allocation set exp_al = exp_al - @SelDay where emp_id=@Emp_Id and yeartr=@TYEAR
--			 		end
--		 end
	
set @Quota_ExpAL = (Select dbo.Funct_LeaveQuota_ExpAL (@Emp_Id,@start_date,@end_date,@total_day,@CLASS_TYPE) AS Quota_ExpAL)

SET @EMP_ID = RIGHT(RTRIM(LTRIM(@LEAVE_REQUEST)),4)
--SET @QUOTA = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),@CLASS_TYPE,GetDate()) AS Quota) + IsNull(@Quota_ExpAL,0)
--SET @QUOTA_AL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AL',GetDate()) AS Quota) + IsNull(@Quota_ExpAL,0)
--SET @QUOTA_AD = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AD',GetDate()) AS Quota)
SET @QUOTA = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),@CLASS_TYPE,@End_Date) AS Quota) 
SET @QUOTA = @QUOTA + IsNull(@Quota_ExpAL,0)
SET @QUOTA_AL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AL',@End_Date) AS Quota)
SET @QUOTA_AL = @QUOTA_AL + IsNull(@Quota_ExpAL,0)
SET @QUOTA_AD = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AD',@End_Date) AS Quota)
SET @AUTH = (SELECT AUTHORITY FROM TBL_USER_MS WHERE USER_ID = @USER_ID)

DECLARE @NEW_LEAVE_REQUEST AS VARCHAR(20)
DECLARE @SMONTH AS CHAR(2)
DECLARE @SYEAR AS CHAR(2)
DECLARE @SDATE AS CHAR(2)
DECLARE @TYEAR AS CHAR(4)

SET @TYEAR = CAST(YEAR(@START_DATE) AS CHAR(4))
SET @SYEAR = RIGHT(@TYEAR, 2)
SET @SMONTH = CAST(MONTH(@START_DATE) as CHAR(2))
IF LEN(RTRIM(LTRIM(@SMONTH))) = 1 SET @SMONTH = '0' + @SMONTH

SET @SDATE = CAST(DAY(@START_DATE) as CHAR(2))
IF LEN(RTRIM(LTRIM(@SDATE))) = 1 SET @SDATE = '0' + @SDATE

SET @NEW_LEAVE_REQUEST = 'LA-' + @SMONTH + @SDATE + @SYEAR + '-' + @EMP_ID

DECLARE @CANCEL_ID AS INT

IF @LEAVE_REQUEST <> @NEW_LEAVE_REQUEST
BEGIN 
	SET @CANCEL_ID = (SELECT MAX(STATUS) FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST)
	SELECT @CANCEL_ID
	IF ISNULL(@CANCEL_ID,0) < 100 SET @CANCEL_ID = 100
	ELSE SET @CANCEL_ID = @CANCEL_ID + 1
			
	UPDATE TBL_LEAVE_REQUEST
	SET STATUS = @CANCEL_ID,
	UPDATE_BY = @USER_ID,
	UPDATED_ON = getdate()
	WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS < 100

  SET @QUOTA = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),@CLASS_TYPE,@End_Date) AS Quota) 
  SET @QUOTA = @QUOTA + IsNull(@Quota_ExpAL,0)
  SET @QUOTA_AL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AL',@End_Date) AS Quota)
  SET @QUOTA_AL = @QUOTA_AL + IsNull(@Quota_ExpAL,0)
  SET @QUOTA_AD = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'AD',@End_Date) AS Quota)
END

DECLARE @APP_DEPT_BY [char] (4) 
DECLARE @APP_DIV_BY [char] (4)

	SET @APP_DEPT_BY = (Select dbo.Funct_FindDeptManager_ByEmp(@EMP_ID) as APP_DEPT_BY)
	SET @APP_DIV_BY = (Select dbo.Funct_FindDivManager_ByEmp(@EMP_ID) as APP_DIV_BY)

	--HARDCODE KHUSUS PAK BANDUNG AGUST 2009 --
	IF @EMP_ID = '1852' SET @APP_DEPT_BY = '9030' -- EMPLOYEE_ID CHANGE FROM 0658 TO 1852
	IF @EMP_ID = '1852' SET @APP_DIV_BY = '9030'

DECLARE @IS_OL_DATE AS INT
IF @CLASS_TYPE = 'OL' AND EXISTS (SELECT * FROM TBL_OL WHERE HOL_DATE = @START_DATE) SET @IS_OL_DATE = 1

DECLARE @IS_OK AS INT
DECLARE @UpComingOL as int

Set @EmpStart_date = (Select start_date from tbl_emp where emp_id = @emp_id)
Set @CS_ServiceLength = (Select datediff(month, @EmpStart_date, getdate()))
Set @ExtStatus = (Select isnull(ExtStatus,0) as ExtStatus from tbl_Leave_Allocation where emp_id = @emp_id And YearTr = Year(@Start_date))

IF @TYPE = 'EMP'
BEGIN

	IF @CLASS_TYPE = 'AL' 
	BEGIN
		Set @UpComingOL = (select count(*) From TBL_OL Where Hol_Date >= Getdate() and Year(Hol_Date) = year(@Start_Date))
		--set @QUOTA_AL=@QUOTA_AL-@UpComingOL

		IF  @TOTAL_DAY <= @QUOTA_AL AND @CS_ServiceLength >= 12 SET @IS_OK = 1
		ELSE
			IF @AUTH = 5 SET @IS_OK = 1
			ELSE SET @MSG = 'You are no longer able to request annual leave. Please contact HRD!'
	END
	ELSE IF @CLASS_TYPE = 'AD' 
	BEGIN
		Set @UpComingOL = (select count(*) From TBL_OL Where Hol_Date >= Getdate() and Year(Hol_Date) = year(@Start_Date))
		
		IF @QUOTA_AL > 0 
		BEGIN
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Use remaining AL : ' +  cast(@QUOTA_AL as char(2)) + ' day(s) !'
			SET @IS_OK = 0
		END
		ELSE
		BEGIN
			IF ISNULL(@QUOTA_AD,0) > 0 
			BEGIN
				IF @TOTAL_DAY <= @QUOTA_AD AND @QUOTA_AL <= 0 AND @CS_ServiceLength >= 12
				BEGIN
					SET @IS_OK = 1
					--set @QUOTA_AD=@QUOTA_AD-@UpComingOL
					SET @MSG =  @CLASS_TYPE + ' REQUEST FOR ' + CAST (@START_DATE AS CHAR(12)) + ' TO ' + CAST(@END_DATE AS CHAR(12)) + ' FOR '  + CAST(@TOTAL_DAY AS CHAR(2)) + ' DAY(S) IS SAVED !'
				END
			END
			ELSE
			BEGIN
			  IF @AUTH = 5 
			    BEGIN
			      SET @IS_OK = 1
			      --set @QUOTA_AD=@QUOTA_AD-@UpComingOL
			    END
			  ELSE 
			    BEGIN
				    SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. No Quota Left for AD ! '
				    SET @IS_OK = 0
				  END
			END
		END
	END
	ELSE IF @CLASS_TYPE = 'OL'
   BEGIN
		IF @IS_OL_DATE = 1 --AND @TOTAL_DAY <= @QUOTA --AND @CS_ServiceLength >= 12 
		BEGIN
			--SET @IS_OK = 1
			--IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
      
      IF @CS_ServiceLength < 12
      BEGIN
        PRINT 'OL OK, but ADVANCE LEAVE'
			  SET @IS_OK = 1
  			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			SET @SEND_TO_ATT = 1
        SET @ADV_YEAR = YEAR(GETDATE())
      END
      ELSE -- @CS_ServiceLength >= 12
      BEGIN
        
        SET @REMAIN_AL_OL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'OL',@End_Date) AS Quota) 
        set @REMAIN_AL_OL = isnull(@REMAIN_AL_OL,0) + isnull(@Quota_ExpAL,0)
        PRINT CONVERT(VARCHAR(5),@REMAIN_AL_OL)
        IF @REMAIN_AL_OL <= 0 
				begin 
          PRINT 'OL NOT OK'
				  Set @MSG = 'Your quota is reached. You are no longer able to take obligatory leave. Please Contact HRD' 
				  SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .' 
          --PRINT @MSG
				End
        ELSE
        BEGIN
          IF (@TOTAL_DAY <= @REMAIN_AL_OL) 
          BEGIN
            PRINT 'OL OK'
			      SET @IS_OK = 1
  			    SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			    SET @SEND_TO_ATT = 1
          END
          ELSE
          BEGIN
            PRINT 'OL NOT OK'
            IF (@REMAIN_AL_OL > 0) AND (@TOTAL_DAY > @REMAIN_AL_OL)
				    begin 
					    Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				      SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
              --PRINT @MSG
					  end
          END
        END
        PRINT 'MSG = ' + @MSG
      END
		END
		ELSE SET @MSG = 'This is not Obligatory Leave Day or Your Quota is reached! Please try your input again.'

   END
  ELSE IF (@CLASS_TYPE = 'HL')
  BEGIN
    declare @Quota_ExpHL as int
    declare @QuotaHL as int
    
    set @Quota_ExpHL = (Select dbo.Funct_LeaveQuota_ExpHL (@Emp_Id,@start_date,@end_date,@total_day,@CLASS_TYPE) AS Quota_ExpAL)
    set @QuotaHL = @Quota_ExpHL
    
    IF @TOTAL_DAY <= @QuotaHL
    begin
      PRINT 'HL OK'
			SET @IS_OK = 1
      SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !'
    end
    else
    begin
      PRINT 'HL NOT OK'
      IF @QuotaHL <= 0 
			begin 
        Set @MSG = 'Your quota is reached or its already expired. You are no longer able to take home leave. Please Contact HRD' 
			  SET @MSG = @MSG + 'You have ' + cast(@QuotaHL as Char(2))+'  day(s) left .'
			end
			IF (@QuotaHL > 0) AND (@TOTAL_DAY > @QuotaHL)
			begin 
        Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
			  SET @MSG = @MSG + 'You just have ' + cast(@QuotaHL as Char(2))+'  day(s) left .'
      end
    end
  END
	ELSE SET @IS_OK = 1
	  
	IF @IS_OK = 1 
	BEGIN
		SET @IS_UPDATE_OK = 1
		--SET @MSG = 'Data Is Updated. '
		--IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
		--IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
		--SET @SEND_TO_ATT = 1
	END
			
END	

IF @TYPE = 'DEPT'
BEGIN
	IF @CLASS_TYPE = 'AL' 
	BEGIN
		IF @EXTSTATUS = 0 
			IF  @TOTAL_DAY <= @QUOTA_AL AND @CS_ServiceLength >= 12 SET @IS_OK = 1 
			ELSE SET @MSG = 'Emp is not able to request annual leave. Please contact HRD!'
		ELSE 
			IF  @TOTAL_DAY <= @QUOTA_AL SET @IS_OK = 1 
			ELSE SET @MSG = 'Emp is not able to request annual leave. Please contact HRD!'
	END
	ELSE IF @CLASS_TYPE = 'AD' 
	BEGIN
    IF @QUOTA_AL > 0 
		BEGIN
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Use remaining AL : ' +  cast(@QUOTA_AL as char(2)) + ' day(s) !'
			SET @IS_OK = 0
		END
    ELSE
    BEGIN
      IF @EXTSTATUS = 0 
			  IF  @TOTAL_DAY <= @QUOTA_AD AND @CS_ServiceLength >= 12 SET @IS_OK = 1 
			  ELSE SET @MSG = 'Emp is not able to request additional leave. Please contact HRD!'
		  ELSE 
			  IF  @TOTAL_DAY <= @QUOTA_AD SET @IS_OK = 1 
			  ELSE SET @MSG = 'Emp is not able to request additional leave. Please contact HRD!'
    END
	END
	ELSE IF @CLASS_TYPE = 'OL'
--		IF @IS_OL_DATE = 1 AND @CS_ServiceLength >= 12 AND @EXTSTATUS = 1 SET @IS_OK = 1 
--		ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'

		IF @EXTSTATUS = 0 
			IF  @IS_OL_DATE = 1 --AND @CS_ServiceLength >= 12 
			BEGIN
				--SET @IS_OK = 1
			--IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
      
      IF @CS_ServiceLength < 12
      BEGIN
        PRINT 'OL OK, but ADVANCE LEAVE'
			  SET @IS_OK = 1
  			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			SET @SEND_TO_ATT = 1
        SET @ADV_YEAR = YEAR(GETDATE())
      END
      ELSE -- @CS_ServiceLength >= 12
      BEGIN

        SET @REMAIN_AL_OL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'OL',@End_Date) AS Quota) + @Quota_ExpAL
        IF (@TOTAL_DAY <= @REMAIN_AL_OL) 
        BEGIN
          PRINT 'OL OK'
			    SET @IS_OK = 1
  			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			  SET @SEND_TO_ATT = 1
        END
        ELSE
        BEGIN
          PRINT 'OL NOT OK'
				  IF @REMAIN_AL_OL <= 0 
				  begin 
					   Set @MSG = 'Your quota is reached. You are no longer able to take obligatory leave. Please Contact HRD' 
				     SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
				  IF (@REMAIN_AL_OL > 0) AND (@TOTAL_DAY > @REMAIN_AL_OL)
				  begin 
					   Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				     SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
        END
      END
			END
			ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'
		ELSE 
			IF  @IS_OL_DATE = 1 SET @IS_OK = 1 
			ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'


	ELSE SET @IS_OK = 1
	
	IF @IS_OK = 1
	BEGIN
--		SELECT 'MANTEBB'
		SET @IS_UPDATE_OK = 1
 		--SET @MSG = 'Data Is Updated. '
		--IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
		--IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
		--SET @SEND_TO_ATT = 1

	END
--	SELECT @MSG
--	SELECT @TOTAL_DAY, @QUOTA, @CS_ServiceLength, @EXTSTATUS

	
END

IF @TYPE = 'DIV'

BEGIN
	IF @CLASS_TYPE = 'AL' 
	BEGIN
		IF @EXTSTATUS = 0 
			IF  @TOTAL_DAY <= @QUOTA_AL AND @CS_ServiceLength >= 12 SET @IS_OK = 1 
			ELSE SET @MSG = 'Emp is not able to request annual leave. Please contact HRD!'
		ELSE 
			IF  @TOTAL_DAY <= @QUOTA_AL SET @IS_OK = 1 
			ELSE SET @MSG = 'Emp is not able to request annual leave. Please contact HRD!'

	END
	ELSE IF @CLASS_TYPE = 'AD' 
	BEGIN
    IF @QUOTA_AL > 0 
		BEGIN
			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Use remaining AL : ' +  cast(@QUOTA_AL as char(2)) + ' day(s) !'
			SET @IS_OK = 0
		END
    ELSE
    BEGIN
      IF @EXTSTATUS = 0 
			  IF  @TOTAL_DAY <= @QUOTA_AD AND @CS_ServiceLength >= 12 SET @IS_OK = 1 
			  ELSE SET @MSG = 'Emp is not able to request additional leave. Please contact HRD!'
		  ELSE 
			  IF  @TOTAL_DAY <= @QUOTA_AD SET @IS_OK = 1 
			  ELSE SET @MSG = 'Emp is not able to request additional leave. Please contact HRD!'
      END
		
	END
	ELSE IF @CLASS_TYPE = 'OL'
--		IF @IS_OL_DATE = 1 AND @CS_ServiceLength >= 12 AND @EXTSTATUS = 1 SET @IS_OK = 1 
--		ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'

--kalo dia OL, tak perlu melihat service length
--		IF @EXTSTATUS = 0 
--			IF  @IS_OL_DATE = 1 AND @CS_ServiceLength >= 12 SET @IS_OK = 1 
--			ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'
--		ELSE 
--			IF  @IS_OL_DATE = 1 SET @IS_OK = 1 
--			ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'
		IF  @IS_OL_DATE = 1 
		BEGIN
			--SET @IS_OK = 1
			--IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
      
      IF @CS_ServiceLength < 12
      BEGIN
        PRINT 'OL OK, but ADVANCE LEAVE'
			  SET @IS_OK = 1
  			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			SET @SEND_TO_ATT = 1
        SET @ADV_YEAR = YEAR(GETDATE())
      END
      ELSE -- @CS_ServiceLength >= 12
      BEGIN

        SET @REMAIN_AL_OL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'OL',@End_Date) AS Quota) + @Quota_ExpAL
        IF (@TOTAL_DAY <= @REMAIN_AL_OL) 
        BEGIN
          PRINT 'OL OK'
			    SET @IS_OK = 1
  			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			  SET @SEND_TO_ATT = 1
        END
        ELSE
        BEGIN
          PRINT 'OL NOT OK'
				  IF @REMAIN_AL_OL <= 0 
				  begin 
					   Set @MSG = 'Your quota is reached. You are no longer able to take obligatory leave. Please Contact HRD' 
				     SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
				  IF (@REMAIN_AL_OL > 0) AND (@TOTAL_DAY > @REMAIN_AL_OL)
				  begin 
					   Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				     SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
        END
      END
			
		END

	ELSE SET @IS_OK = 1
	
	IF @IS_OK = 1
	BEGIN
		SET @IS_UPDATE_OK = 1

		--SET @MSG = 'Data Is Updated. '
		--IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
		--IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
		--SET @SEND_TO_ATT = 1

	END

	
END
	
IF @TYPE = 'HRD'
BEGIN
	IF @CLASS_TYPE = 'AL' 
		BEGIN
			IF  @TOTAL_DAY <= @QUOTA_AL 
			BEGIN
				SET @IS_OK = 1	
			END
			IF (@TOTAL_DAY > @QUOTA_AL) 
			BEGIN
				SET @ADV_YEAR = YEAR(GETDATE()) 
				SET @IS_OK = 1		
			END
	--		ELSE SET @MSG = 'The Quota is reached. Emp is no longer able to request anual leave. Please contact HRD!'
		END
    ELSE IF @CLASS_TYPE = 'AD' 
		BEGIN
      IF @QUOTA_AL > 0 
		  BEGIN
			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is can not be saved. Use remaining AL : ' +  cast(@QUOTA_AL as char(2)) + ' day(s) !'
			  SET @IS_OK = 0
		  END
      ELSE
      BEGIN
        IF  @TOTAL_DAY <= @QUOTA_AD 
			  BEGIN
				  SET @IS_OK = 1	
			  END
			  IF (@TOTAL_DAY > @QUOTA_AD) 
			  BEGIN
				  SET @ADV_YEAR = YEAR(GETDATE()) 
				  SET @IS_OK = 1		
			  END
      END
			
	--		ELSE SET @MSG = 'The Quota is reached. Emp is no longer able to request anual leave. Please contact HRD!'
		END
		ELSE IF @CLASS_TYPE = 'OL'
			IF @IS_OL_DATE = 1 
			BEGIN
				--SET @IS_OK = 1
			--IF (@TOTAL_DAY > @Quota) SET @ADV_YEAR = YEAR(GETDATE()) 
      
      IF @CS_ServiceLength < 12
      BEGIN
        PRINT 'OL OK, but ADVANCE LEAVE'
			  SET @IS_OK = 1
  			SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			SET @SEND_TO_ATT = 1
        SET @ADV_YEAR = YEAR(GETDATE())
      END
      ELSE -- @CS_ServiceLength >= 12
      BEGIN

        SET @REMAIN_AL_OL = (Select dbo.Funct_LeaveQuota (@Emp_Id,Year(@Start_date),'OL',@End_Date) AS Quota) + @Quota_ExpAL
        IF (@TOTAL_DAY <= @REMAIN_AL_OL) 
        BEGIN
          PRINT 'OL OK'
			    SET @IS_OK = 1
  			  SET @MSG =  @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) is Saved !' 
  			  SET @SEND_TO_ATT = 1
        END
        ELSE
        BEGIN
          PRINT 'OL NOT OK'
				  IF @REMAIN_AL_OL = 0 
				  begin 
					   Set @MSG = 'Your quota is reached. You are no longer able to take obligatory leave. Please Contact HRD' 
				     SET @MSG = @MSG + 'You have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
				  IF (@REMAIN_AL_OL > 0) AND (@TOTAL_DAY > @REMAIN_AL_OL)
				  begin 
					   Set @MSG = @Class_Type + ' request for ' + cast (@start_date as char(12)) + ' to ' + cast(@end_date as char(12)) + ' for '  + cast(@Total_day as char(2)) + ' day(s) can''t be Saved !'
				     SET @MSG = @MSG + 'You just have ' + cast(@quota as Char(2))+'  day(s) left .'
					end
        END
      END 
			END
			ELSE SET @MSG = 'This is not Obligatory Leave Day! Please Try your input again.'
		ELSE SET @IS_OK = 1
		
		IF @IS_OK = 1
		BEGIN
			SET @IS_UPDATE_OK = 1
			--SET @MSG = 'Data Is Updated. '
			--IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
			--IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
			--SET @SEND_TO_ATT = 1

		END
	END


IF @IS_UPDATE_OK = 1
BEGIN
			-- EDIT BY INDRA, 04/JAN/2013
			IF @LEAVE_REQUEST = @NEW_LEAVE_REQUEST
			BEGIN
					UPDATE TBL_LEAVE_REQUEST
					SET CLASS_TYPE=@CLASS_TYPE,
					START_DATE=@START_DATE,
					END_DATE=@END_DATE,
					TOTAL_DAY=@TOTAL_DAY,
					STATUS=@STATUS, 
					REMARKS=@REMARKS,
					Update_By = @USER_ID,
					Updated_On = Getdate(),
					DEPT_MGR='',
					DEPT_DATE='1/1/1900',
					DIV_MGR='',
					DIV_DATE='1/1/1900'
					, ADV_YEAR = @ADV_YEAR
					WHERE [LEAVE_REQUEST]=@LEAVE_REQUEST
					AND STATUS < 100
					
					SET @MSG = 'Data Is Updated. '
					IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
					IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
					SET @SEND_TO_ATT = 1
			END
			ELSE
			BEGIN
					IF NOT EXISTS (SELECT * FROM TBL_LEAVE_REQUEST WHERE EMP_ID=@EMP_ID AND LEAVE_REQUEST <> @LEAVE_REQUEST AND ((@START_DATE BETWEEN START_DATE AND END_DATE) OR (@END_DATE BETWEEN START_DATE AND END_DATE)) AND STATUS < 4) 
				 		AND YEAR(@START_DATE) = YEAR(@END_DATE)
					BEGIN
						INSERT INTO TBL_LEAVE_REQUEST
						(LEAVE_REQUEST, EMP_ID, CLASS_TYPE, START_DATE, END_DATE, TOTAL_DAY, STATUS, REMARKS, 
						APP_DEPT_BY, APP_DIV_BY, REQUEST_BY , REQUESTED_ON	,ADV_YEAR)
						VALUES
						(
						@NEW_LEAVE_REQUEST, 
						@EMP_ID,
						@CLASS_TYPE, 
						ISNULL(@START_DATE,'1/1/1900'), 
						ISNULL(@END_DATE,'1/1/1900'),
						@TOTAL_DAY,
						@STATUS,
						@REMARKS,
						@APP_DEPT_BY,
						@APP_DIV_BY,
						@USER_ID ,
						GETDATE()
						,@ADV_YEAR
						)
				
						SET @CANCEL_ID = (SELECT MAX(STATUS) FROM TBL_LEAVE_REQUEST WHERE LEAVE_REQUEST = @LEAVE_REQUEST)
						SELECT @CANCEL_ID
						IF ISNULL(@CANCEL_ID,0) < 100 SET @CANCEL_ID = 100
						ELSE SET @CANCEL_ID = @CANCEL_ID + 1
			
						UPDATE TBL_LEAVE_REQUEST
		 	 			SET STATUS = @CANCEL_ID,
			 			UPDATE_BY = @USER_ID,
			 			UPDATED_ON = getdate()
			 			WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS < 100
				
						SET @MSG = 'Data Is Updated. '
						IF @CLASS_TYPE = 'SI' SET @MSG = @MSG + 'Your doctor certificate need to be submitted to HRB.'
						IF @CLASS_TYPE = 'SL' SET @MSG = @MSG + 'Your proper evident need to be submitted to HRB.'
						SET @SEND_TO_ATT = 1
					END
					ELSE
					BEGIN
						SET @MSG = 'Please crosscheck StartDate & EndDate of your leave request, You already have request for this date!'
					END
			END
		  				
END
ELSE
BEGIN
  IF (@LEAVE_REQUEST <> @NEW_LEAVE_REQUEST)
     AND EXISTS (SELECT '' FROM TBL_LEAVE_REQUEST
                 WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS = @CANCEL_ID)
  BEGIN
    UPDATE TBL_LEAVE_REQUEST
	  SET STATUS = 1,
	  UPDATE_BY = @USER_ID,
	  UPDATED_ON = getdate()
	  WHERE LEAVE_REQUEST = @LEAVE_REQUEST AND STATUS = @CANCEL_ID
  END
END


IF @SEND_TO_ATT = 1
BEGIN
SET @DATE_COUNTER = @START_DATE	

	WHILE @DATE_COUNTER <= @END_DATE
	BEGIN
		exec stp_timeAttendanceConversionABS @DATE_COUNTER
		PRINT @DATE_COUNTER
		SET @DATE_COUNTER = DATEADD(Day, 1, @DATE_COUNTER)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[TimesheetInsertTemplate_Rem]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TimesheetInsertTemplate_Rem]
(
@EMP_ID varchar(4), 
@PERIOD varchar(3), 
@MONTH int, 
@YEAR int, 
@JOBCODE varchar(100), 
@REG int, 
@OT int, 
@LOC_ID varchar(10), 
@CAT_ID varchar(10), 
@USER_ID varchar(4), 
@DAYSINMONTH int, 
@REMARKS varchar(500),
@FWBS6 varchar(10))
AS
--Created By Ary
--EXEC sp_InsertTimesheetTemplate '0834','ALL','3','2006','0-0182-00-0006|0-8900-10-1100|E230',480,120,'23',31

DECLARE @MESSAGE varchar(100)
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @FWBS VARCHAR(10)
DECLARE @TIME_START DATETIME
DECLARE @TIME_END DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TR_DATE DATETIME
DECLARE @TIMESPENT AS INT
SET DATEFIRST 1
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CLIENT = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF LTRIM(RTRIM(@CAT_ID)) <> ''
BEGIN
	SET @SECT_ID = LEFT(@CAT_ID,4)
	SET @CAT_ID = ISNULL(@CAT_ID,'')
	SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))
END

-- SELECT @CAT_ID = CATEGORY_ID FROM TBL_PROJECT_FWBS WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID
-- AND EXISTS (SELECT * FROM TBL_MH_PLAN WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID)

SET @MESSAGE = ''
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
BEGIN
	SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
END
ELSE
BEGIN
	IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
	BEGIN
		SET @MESSAGE = 'Please input remarks for this jobcode !'
	END
	ELSE
	BEGIN
		IF @PERIOD = '1'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/10/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '2'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/11/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/20/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '3'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/21/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = 'ALL'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END

		SET @TR_DATE = @TIME_START
		WHILE @TR_DATE <= @TIME_END
		BEGIN
			
			SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'IN')
			SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'OU')
			SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
			SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
			IF DATEPART(WEEKDAY,@TR_DATE) > 5 AND @TIMEIN IS NULL AND @TIMEOUT IS NULL
			BEGIN
				SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'IN')
				SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'OU')
				SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @TIMEIN = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEIN,108) AS DATETIME)
				SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEOUT,108) AS DATETIME)
				SET @REST_START = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_START,108) AS DATETIME)
				SET @REST_END = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_END,108) AS DATETIME)
			END

			IF @TIMEIN IS NOT NULL OR @TIMEOUT IS NOT NULL
			BEGIN
		--		IF EXISTS (SELECT * FROM TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND ATT_TYPE_ID NOT IN ('AB','AL','UL','HL','OL','SL','SI'))
		--		BEGIN

				IF NOT EXISTS(SELECT * FROM MICADEBUG.dbo.TBL_TIMESHEET WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID) AND
					NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID = @LOC_ID)
					AND (@TIMEIN IS NOT NULL AND @TIMEOUT IS NOT NULL)
				AND DATEPART(WEEKDAY,@TR_DATE) <= 7
				BEGIN
					--IF (DATEDIFF(MINUTE,@TIMEIN,@TIMEOUT) - DATEDIFF(MINUTE,@REST_START,@REST_END)) <> @REG
					--	SET @TIMEOUT = DATEADD(MINUTE,@REG+DATEDIFF(MINUTE,@REST_START,@REST_END),@TIMEIN)
					IF @OT > 0 SET @TIMEOUT = DATEADD(MINUTE,@OT,@TIMEOUT)
					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)
					IF @LOC_ID = '23'
					BEGIN
						IF CONVERT(VARCHAR(5),DATEADD(MINUTE,30,@TIMEOUT),108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),DATEADD(MINUTE,60,@TIMEOUT),108) > '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,60,@TIMEOUT)
				
					END

					INSERT INTO MICADEBUG.dbo.TBL_TIMESHEET 
					(
						EMP_ID,
						CLIENT_JOBCODE,
						JOBCODE_ID,
						PROJECT_SECT_ID,
						CATEGORY_ID,
						FWBS,
						TR_DATE,
						TIME_IN,
						TIME_OUT,
						APPROVE,
						APPROVE1,
						APPROVE2,
						APPROVE3,
						REMARKS,
						TIMESPENT,
						LOC_ID,
 						MODIFY_BY,
 						LAST_MODIFIED,
						REMARKS_JOBCODE,
						FWBS6
					)
					VALUES
					(
						@EMP_ID,
						@CLIENT,
						@JOB_ID,
						@SECT_ID,
						@CAT_ID,
						@FWBS,
						@TR_DATE,
						@TIMEIN ,
						@TIMEOUT ,
						0 ,
						0 ,
						0 ,
						0 ,
						'TEMPLATE',
						@TIMESPENT,
						@LOC_ID,
						@USER_ID,
						GETDATE(),
						ISNULL(@REMARKS,''),
						@FWBS6
					)

		-- 			IF EXISTS (SELECT * FROM TBL_MH_DAY_SPENT WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID)
		-- 			BEGIN
		-- 				DELETE FROM TBL_MH_DAY_SPENT 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID
		-- 			END


		-- 			SET @TIMESPENT = (SELECT ISNULL(TIMESPENT,0) FROM TBL_TIMESHEET 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID )
		-- 	
		-- 			INSERT INTO TBL_MH_DAY_SPENT
		-- 			VALUES
		-- 			(
		-- 				@EMP_ID,
		-- 				@JOB_ID,
		-- 				@CLIENT,
		-- 				@SECT_ID,
		-- 				@CAT_ID,
		-- 				@TR_DATE,
		-- 				'',
		-- 				@TIMESPENT
		-- 			)
				END
			END
			SET @TR_DATE = DATEADD(DAY,1,@TR_DATE)
		END
	END
END

select @MESSAGE as [Message]


GO
/****** Object:  StoredProcedure [dbo].[UpdateBankAccount]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateBankAccount]
AS
BEGIN
	UPDATE a
	SET a.BANKNAME = 'BCA', a.ACC_NUMBER = b.ACCOUNT_NO, a.ACC_NAME = b.ACCOUNT_NAME
	FROM PFNATT.dbo.TBL_EMP a inner join PFNATT.dbo.TBL_FAMILY_TEMP_EXCEL b on a.EMP_ID = b.PATIENT_ID
	WHERE b.MEDICAL_ID = 0 AND b.BANK_NAME like '%BCA%'
	UPDATE a
	SET a.BANKNAME = 'BANK MANDIRI', a.ACC_NUMBER = b.ACCOUNT_NO, a.ACC_NAME = b.ACCOUNT_NAME
	FROM PFNATT.dbo.TBL_EMP a inner join PFNATT.dbo.TBL_FAMILY_TEMP_EXCEL b on a.EMP_ID = b.PATIENT_ID
	WHERE b.MEDICAL_ID = 0 AND b.BANK_NAME like '%MANDIRI%'
	UPDATE a
	SET a.BANKNAME = 'BANK MANDIRI', a.ACC_NUMBER = b.ACCOUNT_NO, a.ACC_NAME = b.ACCOUNT_NAME
	FROM PFNATT.dbo.TBL_EMP a inner join PFNATT.dbo.TBL_FAMILY_TEMP_EXCEL b on a.EMP_ID = b.PATIENT_ID
	WHERE b.MEDICAL_ID = 0 AND b.BANK_NAME like '%BNI%'
	UPDATE a
	SET a.BANKNAME = 'BANK MANDIRI', a.ACC_NUMBER = b.ACCOUNT_NO, a.ACC_NAME = b.ACCOUNT_NAME
	FROM PFNATT.dbo.TBL_EMP a inner join PFNATT.dbo.TBL_FAMILY_TEMP_EXCEL b on a.EMP_ID = b.PATIENT_ID
	WHERE b.MEDICAL_ID = 0 AND b.BANK_NAME like '%NIAGA%'
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateBirthDateWife]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateBirthDateWife]
AS
BEGIN
	UPDATE b
	SET b.BIRTHDATE = a.BIRTH_DATE
	FROM TBL_FAMILY_TEMP_EXCEL a inner join TBL_FAMILY b ON a.PATIENT_ID = b.EMP_ID and CASE a.MEDICAL_ID WHEN 1 THEN '03' ELSE '' END = b.REL_ID
	WHERE ID not in
	(
		SELECT ID
		FROM TBL_FAMILY_TEMP_EXCEL
		WHERE MEDICAL_ID = 0
		UNION
		SELECT ID
		FROM TBL_FAMILY a inner join TBL_FAMILY_TEMP_EXCEL b ON a.EMP_ID = b.PATIENT_ID and a.BIRTHDATE = b.BIRTH_DATE
	)
	UPDATE b
	SET b.BIRTHDATE = a.BIRTH_DATE
	FROM TBL_FAMILY_TEMP_EXCEL a inner join TBL_FAMILY b ON a.PATIENT_ID = b.EMP_ID and a.PATIENT_NAME = b.SNAME
	WHERE ID not in
	(
		SELECT ID
		FROM TBL_FAMILY_TEMP_EXCEL
		WHERE MEDICAL_ID = 0
		UNION
		SELECT ID
		FROM TBL_FAMILY a inner join TBL_FAMILY_TEMP_EXCEL b ON a.EMP_ID = b.PATIENT_ID and a.BIRTHDATE = b.BIRTH_DATE
	)
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateFullName]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateFullName]
AS
BEGIN
	DECLARE @EmployeeId Varchar(10)
	DECLARE @FullName Varchar(512)


	DECLARE db_cursor CURSOR 
	FOR
		SELECT EMP_ID, dbo.GetEmployeeName(EMP_ID)
		FROM TBL_EMP
		
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @EmployeeId, @FullName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		   UPDATE TBL_EMP
		   SET FULL_NAME = @FullName
		   WHERE EMP_ID = @EmployeeId

		   FETCH NEXT FROM db_cursor INTO @EmployeeId, @FullName
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateIsMedicalAllocated]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateIsMedicalAllocated]
AS
BEGIN
	DECLARE @EmployeeId Varchar(10)
	DECLARE @BirthDate DateTime


	DECLARE db_cursor CURSOR 
	FOR
		SELECT EMP_ID, BIRTH_DATE
		FROM TBL_FAMILY_TEMP
		
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @EmployeeId, @BirthDate

	WHILE @@FETCH_STATUS = 0
	BEGIN
		   UPDATE TBL_FAMILY
		   SET IS_MEDICAL_ALLOCATED = 1
		   WHERE EMP_ID = @EmployeeId AND BIRTHDATE = @BirthDate

		   FETCH NEXT FROM db_cursor INTO @EmployeeId, @BirthDate
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateMedicalId]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec UpdateMedicalId
CREATE PROCEDURE [dbo].[UpdateMedicalId]
AS
BEGIN
	DECLARE @EmployeeId Varchar(10)
	DECLARE @FamilyName Varchar(512)
	DECLARE @RelationId Varchar(10)
	DECLARE @MedicalId Int
	DECLARE @BirthDate DateTime
	
	UPDATE a
	SET a.MEDICAL_ID = b.MEDICAL_ID
	FROM TBL_FAMILY a inner join TBL_FAMILY_TEMP_EXCEL b ON a.EMP_ID = b.PATIENT_ID and a.BIRTHDATE = b.BIRTH_DATE

	DECLARE db_cursor CURSOR 
	FOR
		SELECT EMP_ID, BIRTHDATE, SNAME, REL_ID
		FROM TBL_FAMILY
		WHERE REL_ID in ('03', '04') AND EMP_ID <> '' AND ISNULL(MEDICAL_ID, 0) = 0
		ORDER BY BIRTHDATE
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @EmployeeId, @BirthDate, @FamilyName, @RelationId

	WHILE @@FETCH_STATUS = 0
	BEGIN
			IF @RelationId = '03'
			BEGIN
			   UPDATE TBL_FAMILY
			   SET MEDICAL_ID = 1
			   WHERE EMP_ID = @EmployeeId AND BIRTHDATE = @BirthDate AND SNAME = @FamilyName
			END
			ELSE IF @RelationId = '04'
			BEGIN
				SELECT @MedicalId = ISNULL(MAX(MEDICAL_ID), 1)
				FROM TBL_FAMILY
				WHERE EMP_ID = @EmployeeId AND REL_ID = @RelationId
				UPDATE TBL_FAMILY
				SET MEDICAL_ID = @MedicalId + 1
				WHERE EMP_ID = @EmployeeId AND BIRTHDATE = @BirthDate AND SNAME = @FamilyName
			END
		   FETCH NEXT FROM db_cursor INTO @EmployeeId, @BirthDate, @FamilyName, @RelationId
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateRefId]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec UpdateRefId
CREATE PROCEDURE [dbo].[UpdateRefId]
AS
BEGIN
	DECLARE @BirthDate DATETIME
	DECLARE @FirstName Varchar(512)
	DECLARE @MaxRefId INT

	DECLARE db_cursor CURSOR 
	FOR
		select ltrim(rtrim(a.FIRSTNAME)), a.BIRTHDATE
		from TBL_EMP a
		where ISNULL(REF_ID, 0) = 0
		--select ltrim(rtrim(a.FIRSTNAME)), a.BIRTHDATE
		--from TBL_EMP a
		--where a.BIRTHDATE <> '1900-01-01' and ltrim(rtrim(a.FIRSTNAME)) like '% %' AND ISNULL(REF_ID, 0) = 0
		--group by ltrim(rtrim(a.FIRSTNAME)), a.BIRTHDATE having COUNT(*)>1
		--order by ltrim(rtrim(a.FIRSTNAME))
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @FirstName, @BirthDate

	WHILE @@FETCH_STATUS = 0
	BEGIN
			SELECT @MaxRefId = ISNULL(MAX(REF_ID), 0) + 1 FROM TBL_EMP
			
			UPDATE TBL_EMP
			SET REF_ID = @MaxRefId
			WHERE FIRSTNAME = @FirstName AND BIRTHDATE = @BirthDate
			
			FETCH NEXT FROM db_cursor INTO @FirstName, @BirthDate
	END

	CLOSE db_cursor   
	DEALLOCATE db_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CheckEmployeeStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec usp_InsertMedical '2076'
CREATE PROCEDURE [dbo].[usp_CheckEmployeeStatus]
(
@EMP_ID VARCHAR(4)
)
AS
BEGIN
	SELECT	USER_ID, 
		MICADEBUG.dbo.FUnct_name(User_Id) As EMPLOYEE_NAME, 
		CAST(PASSWORD AS varchar) AS PASSWORD,
		ISNULL(GROUP_ID,'') AS GROUP_ID, 
		ISNULL(SECT_NAME,'') AS SECT_NAME, 
		ISNULL(AUTHORITY,0) AS AUTHORITY 
	FROM MICADEBUG.dbo.TBL_EMP 
	INNER JOIN MICADEBUG.dbo.TBL_USER_MS ON TBL_EMP.EMP_ID = TBL_USER_MS.USER_ID
	LEFT JOIN MICADEBUG.dbo.TBL_SECTION ON TBL_EMP.SECT_ID = TBL_SECTION.SECT_ID
	WHERE TBL_EMP.STATUS = '1' and USER_ID = @EMP_ID
END

GO
/****** Object:  StoredProcedure [dbo].[usp_checkOvertimeIsAdmin]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec GetPlafondDental '', '2076'
CREATE PROCEDURE [dbo].[usp_checkOvertimeIsAdmin]
(
	@EmpId VARCHAR(50)
)
AS
BEGIN
	select COUNT(1) isAdmin from MICADEBUG.dbo.V_USERGROUP where USER_ID = @EmpId and GROUP_ID = 'G000'
END


GO
/****** Object:  StoredProcedure [dbo].[usp_deleteActiveDirectory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_InsertMedical '2076'
CREATE PROCEDURE [dbo].[usp_deleteActiveDirectory]
AS
BEGIN
	DELETE FROM PFNATT.dbo.TBL_ACTIVE_DIRECTORY
END

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteConfidential]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteConfidential '2076'
CREATE PROCEDURE [dbo].[usp_DeleteConfidential]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_CONFIDENTIAL
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteCurrentCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteCurrentCourse '2076'
CREATE PROCEDURE [dbo].[usp_DeleteCurrentCourse]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_COURSE_INT
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEducation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteEducation '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEducation]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EDUCATION
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEmployeeHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteEmployeeHistory '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEmployeeHistory]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_HISTORY
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEmployeeHistoryCareerPath]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteEmployeeHistoryCareerPath '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEmployeeHistoryCareerPath]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	DECLARE @EmployeeIdTemp TABLE(EMPLOYEE_ID VARCHAR(10))
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	INSERT	INTO @EmployeeIdTemp(EMPLOYEE_ID)
	SELECT	EMP_ID
	FROM	TBL_EMP
	WHERE	REF_ID = @RefId
	
	UPDATE	TBL_EMP_HISTORY_CAREER_PATH
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID IN (SELECT EMPLOYEE_ID FROM @EmployeeIdTemp)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEmployeeHistoryRecruitment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_DeleteEmployeeHistoryStatus '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEmployeeHistoryRecruitment]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_HISTORY_RECRUITMENT
	SET ISDELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEmployeeHistoryStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteEmployeeHistoryStatus '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEmployeeHistoryStatus]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	DECLARE @EmployeeIdTemp TABLE(EMPLOYEE_ID VARCHAR(10))
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	INSERT	INTO @EmployeeIdTemp(EMPLOYEE_ID)
	SELECT	EMP_ID
	FROM	TBL_EMP
	WHERE	REF_ID = @RefId
	
	UPDATE	TBL_EMP_HISTORY_STATUS
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID IN (SELECT EMPLOYEE_ID FROM @EmployeeIdTemp)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEnglishAssessment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteEnglishAssessment '2076'
CREATE PROCEDURE [dbo].[usp_DeleteEnglishAssessment]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_ENGLISH_ASSESSMENT
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteFamily]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteFamily '2076'
CREATE PROCEDURE [dbo].[usp_DeleteFamily]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_FAMILY
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteFamilyDocumentAttachment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteFamilyDocumentAttachment '2076'
CREATE PROCEDURE [dbo].[usp_DeleteFamilyDocumentAttachment]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_FAMILY_DOCUMENT_ATTACHMENT
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteLanguageSkill]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteLanguageSkill '2076'
CREATE PROCEDURE [dbo].[usp_DeleteLanguageSkill]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_LANGUAGE
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteLoan]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteLoan '2076'
CREATE PROCEDURE [dbo].[usp_DeleteLoan]
(
	@EmployeeId VARCHAR(10), @AsOfDate datetime
)
AS
BEGIN
	UPDATE	TBL_LOAN
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId and AS_OF_DATE = @AsOfDate
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteLoanAsOf]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DeleteLoanAsOf]
(
	@AsOfDate Datetime
)
AS
BEGIN
	Delete from TBL_LOAN where AS_OF_DATE = @AsOfDate
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteMedical]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteMedical '2076'
CREATE PROCEDURE [dbo].[usp_DeleteMedical]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_MEDICAL
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePreviousCompany]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeletePreviousCompany '2076'
CREATE PROCEDURE [dbo].[usp_DeletePreviousCompany]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_JOB_EX
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePreviousCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeletePreviousCourse '2076'
CREATE PROCEDURE [dbo].[usp_DeletePreviousCourse]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_COURSE
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePreviousProject]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeletePreviousProject '2076'
CREATE PROCEDURE [dbo].[usp_DeletePreviousProject]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_PROJECT_PREV
	SET		IS_DELETED = 1
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteTOEIC]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteTOEIC '2076'
CREATE PROCEDURE [dbo].[usp_DeleteTOEIC]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_TOEIC
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteTraining]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_DeleteTraining '2076'
CREATE PROCEDURE [dbo].[usp_DeleteTraining]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP_TRAINING
	SET		IS_DELETED = 1
	WHERE	EMPLOYEE_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetConfidential]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetConfidential '0716'
CREATE PROCEDURE [dbo].[usp_GetConfidential]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ACTIVITY_DATE) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.TYPE_ID, a.TYPE_NAME, a.DETAIL_TYPE_ID, a.DETAIL_TYPE_NAME, a.ACTIVITY_DATE, a.CONFIDENTIAL_ATTACHMENT, a.REMARK
	FROM	TBL_EMP_CONFIDENTIAL a
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0
	ORDER BY a.ACTIVITY_DATE
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetConfidentialForEmpHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetConfidential '0716'
CREATE PROCEDURE [dbo].[usp_GetConfidentialForEmpHistory]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ACTIVITY_DATE) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.TYPE_ID, a.TYPE_NAME, a.DETAIL_TYPE_ID, a.DETAIL_TYPE_NAME, a.ACTIVITY_DATE, a.CONFIDENTIAL_ATTACHMENT, a.REMARK
	FROM	TBL_EMP_CONFIDENTIAL a
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0 and TYPE_ID IN ('WRL','VBW')
	ORDER BY a.ACTIVITY_DATE
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCurrentCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetCurrentCourse '0534'
CREATE PROCEDURE [dbo].[usp_GetCurrentCourse]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.C_FROM DESC) AS NO, a.TR_Number AS ID, a.EMP_ID, a.SUBJECT, a.PROVIDER, a.C_FROM, a.C_TO, a.PLACEOFTRAINING, a.TRAINER, a.PURPOSE, a.GRADE,
			a.CURRENCY, a.AMOUNT, a.EXPENSES, a.PAIDOUT, a.REMARKS
	FROM	TBL_COURSE_INT a
	WHERE	a.EMP_ID = @EmployeeId AND IS_DELETED = 0
	ORDER BY a.C_FROM DESC
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCurrentProject]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetCurrentProject '2076'
CREATE PROCEDURE [dbo].[usp_GetCurrentProject]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ID ASC) AS NO, a.ID, a.EMP_ID, a.EMPLOYEE_NAME, a.DEPT_ID, a.DEPT_NAME, a.SECT_ID, a.SECT_NAME, a.START_PJ, a.END_PJ, a.POSITION_NAME, a.JOBCODE_ID, 
			a.PROJECT_NAME,	a.PPA_CRITERIA, a.CRITERIA_NAME, a.PPA_SCOPE, a.PROJECT_VALUE, a.TOTALHOURALL, a.TOTALMINUTEALL, a.TOTALTSALLBYJOBCODE, 
			a.MHSPENT, a.FLAG, a.CREATED_ON, a.CREATED_BY
	FROM	TBL_EMP_WORKDONE_RS a
	WHERE	a.EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDepartmentList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetDepartmentList '2076'
CREATE PROCEDURE [dbo].[usp_GetDepartmentList]
(
	@DivisionId VARCHAR(10)
)
AS
BEGIN
	SELECT	a.SECT_ID AS DEPARTMENT_ID, a.SECT_NAME AS DEPARTMENT_NAME
	FROM	TBL_SECTION a
	WHERE	a.DEPT_ID = @DivisionId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDeptMgrCM]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDeptMgrCM]
AS
BEGIN
	select SUBSTRING(dbo.GetDeptManagerName('E300'),2,4) CBDMGRID, dbo.GetDeptManagerName('E300') CBDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N220'),2,4) QHSEMGRID, dbo.GetDeptManagerName('N220') QHSEMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E400'),2,4) TGMMGRID, dbo.GetDeptManagerName('E400') TGMMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E200'),2,4) DEDMGRID, dbo.GetDeptManagerName('E200') DEDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E100'),2,4) PODMGRID, dbo.GetDeptManagerName('E100') PODMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N100'),2,4) CODMGRID, dbo.GetDeptManagerName('E100') CODMGRNAME
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetDeptMgrCOD]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDeptMgrCOD]
AS
BEGIN
	select SUBSTRING(dbo.GetDeptManagerName('N100'),2,4) DIVMGRID, dbo.GetDeptManagerName('N100') DIVMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N110'),2,4) FADMGRID, dbo.GetDeptManagerName('N110') FADMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N120'),2,4) HRDMGRID, dbo.GetDeptManagerName('N120') HRDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N140'),2,4) PADMGRID, dbo.GetDeptManagerName('N140') PADMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('N130'),2,4) GADMGRID, dbo.GetDeptManagerName('N130') GADMGRNAME
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetDeptMgrDED]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDeptMgrDED]
AS
BEGIN
	select SUBSTRING(dbo.GetDeptManagerName('E200'),2,4) DIVMGRID, dbo.GetDeptManagerName('E200') DIVMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E210'),2,4) PROCESSMGRID, dbo.GetDeptManagerName('E210') PROCESSMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E220'),2,4) CIVILMGRID, dbo.GetDeptManagerName('E220') CIVILMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E230'),2,4) PIPINGMGRID, dbo.GetDeptManagerName('E230') PIPINGMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E240'),2,4) INSTRUMENTMGRID, dbo.GetDeptManagerName('E240') INSTRUMENTMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E250'),2,4) ELECTRICALMGRID, dbo.GetDeptManagerName('E250') ELECTRICALMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E260'),2,4) MECHANICALMGRID, dbo.GetDeptManagerName('E260') MECHANICALMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E270'),2,4) ROTARYMGRID, dbo.GetDeptManagerName('E270') ROTARYMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E280'),2,4) ITMGRID, dbo.GetDeptManagerName('E280') ITMGRNAME
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetDeptMgrPOD]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDeptMgrPOD]
AS
BEGIN
	select SUBSTRING(dbo.GetDeptManagerName('E100'),2,4) DIVMGRID, dbo.GetDeptManagerName('E100') DIVMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E110'),2,4) PMDMGRID, dbo.GetDeptManagerName('E110') PMDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E150'),2,4) PPCDMGRID, dbo.GetDeptManagerName('E150') PPCDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E160'),2,4) PROCMGRID, dbo.GetDeptManagerName('E160') PROCMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E170'),2,4) CMDMGRID, dbo.GetDeptManagerName('E170') CMDMGRNAME,
	SUBSTRING(dbo.GetDeptManagerName('E180'),2,4) QCMGRID, dbo.GetDeptManagerName('E180') DCMGRNAME
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEducation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_GetEducation]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.EDU_ID DESC) AS NO, a.TR_NUMBER AS ID, a.EMP_ID, a.EDU_ID, b.EDU_DESC, a.S_YEAR AS FROM_YEAR, a.GRAD_YEAR AS TO_YEAR, a.INSTITUTE, a.MAJORING, a.LOCATION,
			a.GRADE, a.TITLE_DEGREE, a.REMARKS, a.ATTACHMENT
	FROM	TBL_EDUCATION a
			INNER JOIN TBL_EDU_TYPE b ON a.EDU_ID = b.EDU_ID
	WHERE	a.EMP_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeeHistory '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeHistory]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.NUMB ASC) AS NO, a.NUMB AS ID, a.EMP_ID, a.ACT AS ACTIVITY_ID, b.ACT_DESC AS ACTIVITY_NAME, a.TR_DATE AS START_DATE, a.TR_DATE1 AS END_DATE,
			a.LOC_ID, c.LOC_NAME, a.SECT_ID, d.SECT_NAME, a.POSITION_ID, e.POSITION_NAME, a.GRADE_ID, f.GRADE_NAME, 
			a.EMP_TYPE_ID, g.TYPE_NAME AS EMP_TYPE_NAME, a.SALARY_GRADE AS SALARY_GRADE_ID, h.TYPE_NAME AS SALARY_GRADE_NAME, a.DETAILS
	FROM	TBL_EMP_HISTORY a
			LEFT OUTER JOIN TBL_ACTIVITIES b ON a.ACT = b.ACT_ID
			LEFT OUTER JOIN TBL_LOCATION c ON a.LOC_ID = c.LOC_ID
			LEFT OUTER JOIN TBL_SECTION d ON a.SECT_ID = d.SECT_ID
			LEFT OUTER JOIN TBL_POSITION e ON a.POSITION_ID = e.POSITION_ID
			LEFT OUTER JOIN TBL_GRADE f ON a.GRADE_ID = f.GRADE_ID
			LEFT OUTER JOIN TBL_EMP_TYPE g ON a.EMP_TYPE_ID = g.EMP_TYPE_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE h ON a.SALARY_GRADE = h.TYPE_ID AND h.TYPE = 'SalaryGradeType'
	WHERE	a.EMP_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeHistoryCareerPath]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeeHistoryCareerPath '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeHistoryCareerPath]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	DECLARE @EmployeeIdTemp TABLE(EMPLOYEE_ID VARCHAR(10))
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	INSERT	INTO @EmployeeIdTemp(EMPLOYEE_ID)
	SELECT	EMP_ID
	FROM	TBL_EMP
	WHERE	REF_ID = @RefId
	
	SELECT	ROW_NUMBER() OVER(ORDER BY a.START_DATE ASC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.ACTIVITY_ID AS ACTIVITY_ID, c.TYPE_NAME AS ACTIVITY_NAME,
			a.COLUMN_VALUE, ISNULL(a.ATTACHMENT, '') AS ATTACHMENT, a.START_DATE, CASE a.END_DATE WHEN '12/31/2999' THEN '01/01/1900' ELSE a.END_DATE END AS END_DATE, 
			a.UPDATED_BY AS UPDATE_BY_ID, ISNULL(b.FIRSTNAME+'.'+b.LASTNAME, a.UPDATED_BY) AS UPDATE_BY_NAME, a.UPDATED_DATE, a.TABLE_NAME, a.COLUMN_NAME
	FROM	TBL_EMP_HISTORY_CAREER_PATH a
			LEFT OUTER JOIN TBL_EMP b ON a.UPDATED_BY = b.EMP_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE c ON a.ACTIVITY_ID = c.TYPE_ID AND c.TYPE = 'ActivityHistoryCareerPathType'
			
	WHERE	a.EMPLOYEE_ID IN (SELECT EMPLOYEE_ID FROM @EmployeeIdTemp) AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeHistoryRecruitment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEmployeeHistoryRecruitment]
	@EmployeeId VARCHAR(10)
AS
BEGIN
	SELECT ROW_NUMBER() OVER(ORDER BY a.APPLICATION_ID ASC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.APPLICATION_ID
	FROM	dbo.TBL_EMP_HISTORY_RECRUITMENT a
	WHERE a.EMPLOYEE_ID = @EmployeeId AND a.ISDELETED = 0
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeHistoryStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeeHistoryStatus '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeHistoryStatus]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	DECLARE @EmployeeIdTemp TABLE(EMPLOYEE_ID VARCHAR(10))
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	INSERT	INTO @EmployeeIdTemp(EMPLOYEE_ID)
	SELECT	EMP_ID
	FROM	TBL_EMP
	WHERE	REF_ID = @RefId
	
	SELECT	ROW_NUMBER() OVER(ORDER BY a.START_DATE ASC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.ACTIVITY_ID AS ACTIVITY_ID, c.TYPE_NAME AS ACTIVITY_NAME,
			a.COLUMN_VALUE_ID, a.COLUMN_VALUE_NAME, ISNULL(a.COLUMN_VALUE_DESCRIPTION, '') AS COLUMN_VALUE_DESCRIPTION, ISNULL(a.ATTACHMENT, '') AS ATTACHMENT, 
			a.START_DATE, CASE a.END_DATE WHEN '12/31/2999' THEN '01/01/1900' ELSE a.END_DATE END AS END_DATE, a.UPDATED_BY AS UPDATE_BY_ID, 
			ISNULL(b.FIRSTNAME+'.'+b.LASTNAME, a.UPDATED_BY) AS UPDATE_BY_NAME, a.UPDATED_DATE, a.TABLE_NAME, a.COLUMN_NAME
	FROM	TBL_EMP_HISTORY_STATUS a
			LEFT OUTER JOIN TBL_EMP b ON a.UPDATED_BY = b.EMP_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE c ON a.ACTIVITY_ID = c.TYPE_ID AND c.TYPE = 'ActivityHistoryStatusType'
			
	WHERE	a.EMPLOYEE_ID IN (SELECT EMPLOYEE_ID FROM @EmployeeIdTemp) AND a.IS_DELETED = 0
END



GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeId]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeeId '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeId]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	
	SELECT	@RefId = REF_ID
	FROM	TBL_EMP a
	WHERE	a.EMP_ID = @EmployeeId
	
	SELECT	EMP_ID
	FROM	TBL_EMP
	WHERE	REF_ID = @RefId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeIdHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeeIdHistory '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeIdHistory]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @RefId INT
	SELECT @RefId = REF_ID FROM TBL_EMP WHERE EMP_ID = @EmployeeId
	
	SELECT ROW_NUMBER() OVER(ORDER BY START_DATE ASC) AS NO, EMP_ID, FULL_NAME, START_DATE, CASE WHEN EMP_TYPE_ID = 'RS' THEN END_DATE WHEN ISNULL(EXP_DATE1, '01/01/1900') = '01/01/1900' THEN EXP_DATE ELSE EXP_DATE1 END AS END_DATE
	FROM TBL_EMP
	WHERE REF_ID = @RefId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeInformation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPersonal '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeInformation]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, UPPER(a.FULL_NAME) AS EMPLOYEE_NAME, a.POSITION_ID, UPPER(b.POSITION_NAME) AS POSITION_NAME, 
			(SELECT TOP 1 GROUP_ID FROM TBL_EMP_GROUP_MEMBER WHERE EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC) AS GROUP_ID,
			UPPER((SELECT TOP 1 b.GROUP_NAME FROM TBL_EMP_GROUP_MEMBER a INNER JOIN TBL_EMP_GROUP b ON a.GROUP_ID = b.GROUP_ID WHERE a.EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC)) AS GROUP_NAME,
			a.SECT_ID AS DEPARTMENT_ID, UPPER(c.SECT_NAME) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, UPPER(d.DEPT_NAME) AS DIVISION_NAME
	FROM	TBL_EMP a
			LEFT OUTER JOIN TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
			LEFT OUTER JOIN TBL_SECTION c ON a.SECT_ID = c.SECT_ID
			LEFT OUTER JOIN TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
	where	a.EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeList '2076', '', ''
CREATE PROCEDURE [dbo].[usp_GetEmployeeList]
(
	@EmployeeId VARCHAR(10),
	@FilterType VARCHAR(50),
	@IsResign VARCHAR(1)
)
AS
BEGIN
	DECLARE @FilterValue VARCHAR(512)
	IF @FilterType = 'Position'
	BEGIN
		SELECT	@FilterValue = POSITION_ID FROM TBL_EMP WHERE EMP_ID = @EmployeeId
		SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME
		FROM	PFNATT.dbo.TBL_EMP a
		WHERE	STATUS = @IsResign AND POSITION_ID = @FilterValue
	END
	ELSE IF @FilterType = 'Group'
	BEGIN
		SELECT	@FilterValue = GROUP_ID FROM TBL_EMP_GROUP_MEMBER WHERE EMP_ID = @EmployeeId
		SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				INNER JOIN TBL_EMP_GROUP_MEMBER b ON a.EMP_ID = b.EMP_ID
		WHERE	a.STATUS = @IsResign AND b.GROUP_ID = @FilterValue
	END
	ELSE IF @FilterType = 'Department'
	BEGIN
		SELECT	@FilterValue = SECT_ID FROM TBL_EMP WHERE EMP_ID = @EmployeeId
		SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME
		FROM	PFNATT.dbo.TBL_EMP a
		WHERE	STATUS = @IsResign AND SECT_ID = @FilterValue
	END
	ELSE IF @FilterType = 'Division'
	BEGIN
		SELECT	@FilterValue = b.DEPT_ID FROM TBL_EMP a INNER JOIN TBL_SECTION b ON a.SECT_ID = b.SECT_ID WHERE a.EMP_ID = @EmployeeId
		SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				INNER JOIN PFNATT.dbo.TBL_SECTION b ON a.SECT_ID = b.SECT_ID
		WHERE	a.STATUS = @IsResign AND b.DEPT_ID = @FilterValue
	END
	ELSE
	BEGIN
		SELECT	a.Emp_Id AS EMPLOYEE_ID, ISNULL(a.FULL_NAME, '') As EMPLOYEE_NAME
		FROM	PFNATT.dbo.TBL_EMP a
		WHERE	STATUS = @IsResign
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeRecords]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeRecords '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeRecords]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @USAGEL INT, @AL_EXP INT, @ADX as INT
	SET @AL_EXP = (SELECT  Isnull(EXP_AL,0) FROM PFNATT.dbo.TBL_LEAVE_ALLOCATION Where EMp_ID = @EmployeeId and YearTr = YEAR(GETDATE()))
	SET @USAGEL = (select PFNATT.dbo.Funct_SumLeave3(@EmployeeId,YEAR(GETDATE()),'ALx',GETDATE()))
	SET @ADX = (select PFNATT.dbo.Funct_SumLeave(@EmployeeId,YEAR(GETDATE()),'ADX'))
	
	SELECT	TOP 1 a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, UPPER(a.FULL_NAME) AS EMPLOYEE_NAME, a.FIRSTNAME, a.MIDDLENAME, a.LASTNAME, a.EMP_TYPE_ID AS EMPLOYEE_TYPE_ID, 
			e.TYPE_NAME AS EMPLOYEE_TYPE_NAME, a.GRADE AS GRADE_ID, f.GRADE_NAME AS GRADE_NAME, a.JOIN_DATE, a.START_DATE, a.END_DATE, a.EXP_DATE, a.EXP_DATE1, a.PERMANENT_START_DATE, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT,
			(SELECT TOP 1 RTRIM(LTRIM(GROUP_ID)) FROM TBL_EMP_GROUP_MEMBER WHERE EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC) AS GROUP_ID,
			UPPER((SELECT TOP 1 b.GROUP_NAME FROM TBL_EMP_GROUP_MEMBER a INNER JOIN TBL_EMP_GROUP b ON a.GROUP_ID = b.GROUP_ID WHERE a.EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC)) AS GROUP_NAME,
			(SELECT TOP 1 GRADECLASS_ID FROM TBL_EMP_GRADE_CLASS_MEMBER WHERE EMP_ID = @EmployeeId) AS GRADE_CLASS_ID,
			UPPER((SELECT TOP 1 b.GRADE_CLASSIFICATION FROM TBL_EMP_GRADE_CLASS_MEMBER a INNER JOIN TBL_EMP_GRADE_CLASS b ON a.GRADECLASS_ID = b.GRADECLASS_ID WHERE a.EMP_ID = @EmployeeId)) AS GRADE_CLASS_NAME,
			a.POSITION_ID, UPPER(b.POSITION_NAME) AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, UPPER(c.SECT_NAME) AS DEPARTMENT_NAME, 
			c.DEPT_ID AS DIVISION_ID, UPPER(d.DEPT_NAME) AS DIVISION_NAME, RTRIM(LTRIM(a.SALARY_GRADE)) AS SALARY_GRADE_ID, k.TYPE_NAME AS SALARY_GRADE_NAME, a.LOC_ID, ISNULL(g.LOC_NAME,'-') LOC_NAME, ISNULL(a.BANKNAME,'-') BANKNAME, ISNULL(a.BRANCH,'-') BRANCH, ISNULL(a.ACC_NUMBER,'-') ACC_NUMBER, ISNULL(a.ACC_NAME,'-') ACC_NAME, a.BANK_ATTACHMENT,
			ISNULL(a.JAMSOSTEK_NO,'-') JAMSOSTEK_NO, a.JAMSOSTEK_START_DATE, a.JAMSOSTEK_STATUS_ID, ISNULL(i.TYPE_NAME,'-') AS JAMSOSTEK_STATUS_NAME, a.JAMSOSTEK_ATTACHMENT, 
			ISNULL(a.BPJS_KESEHATAN_NO,'-') BPJS_KESEHATAN_NO, a.BPJS_KESEHATAN_STATUS_ID, ISNULL(o.TYPE_NAME,'-') AS BPJS_KESEHATAN_STATUS_NAME,
			ISNULL(a.BPJS_KESEHATAN_FASKES_CODE,'-') BPJS_KESEHATAN_FASKES_CODE, ISNULL(p.NAME,'-') AS BPJS_KESEHATAN_FASKES_NAME,
			a.BPJS_KESEHATAN_START_DATE, a.BPJS_KESEHATAN_ATTACHMENT, 
			a.TAX_STATUS_ID, ISNULL(j.TYPE_NAME,'-') AS TAX_STATUS_NAME,
			ISNULL(l.AL,0) AS ANNUAL_LEAVE,ISNULL(l.LastYearRemain,0) - (@USAGEL + @AL_EXP) AS TOTAL_ANNUAL_LEAVE, (ISNULL(l.AD,0) - @ADX) AS ADDITIONAL_LEAVE, 
			ISNULL(dbo.GetPreviousExperience(a.EMP_ID),'-') AS PREVIOUS_EXPERIENCE, ISNULL(dbo.GetCurrentExperience(a.EMP_ID),'-') AS CURRENT_EXPERIENCE, ISNULL(dbo.GetTotalExperience(a.EMP_ID),'-') AS TOTAL_EXPERIENCE,
			m.LOC_ID AS ASSIGNMENT_LOCATION_ID, ISNULL(n.LOC_NAME,'-') AS ASSIGNMENT_LOCATION_NAME, ISNULL(m.POSNAME,'-') AS ASSIGNMENT_POSITION, ISNULL(m.RFNE,'-') AS RFA_NO, m.RFA_DATE AS RFA_DATE, m.START_MOB, m.END_MOB,
			ISNULL((SELECT TOP 1 ISNULL(COMPANY,'-') COMPANY FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC),'-') AS LAST_COMPANY,
			ISNULL((SELECT TOP 1 ISNULL(POS,'-') POS FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC),'-') AS LAST_POSITION, ISNULL(dbo.GetStatusLeave(a.EMP_ID),'-') AS STATUS_LEAVE, '-' AS SCHEDULE_HOME_LEAVE,
			(SELECT TOP 1 ID FROM Resignation.dbo.RESIGN_MAIN WHERE EMPLOYEE_ID = a.EMP_ID AND STATUS IN (0, 1)) AS RESIGN_ID,
			(SELECT TOP 1 ID FROM CareerResume.dbo.CAREER_RESUME_MAIN WHERE EMPLOYEE_ID = a.EMP_ID AND STATUS IN (1) ORDER BY COMPLETED_DATE DESC) AS CV_ID,
			(SELECT TOP 1 ASSIGNMENT_ID FROM Assignment.dbo.PRE_ASSIGNMENT WHERE EMP_ID = a.EMP_ID AND ISDELETED IN (0) ORDER BY ASSIGNMENT_START DESC) AS PREASSIGNMENTID,
			q.Email AS EMAIL, dbo.Funct_GetEmpPensionDate(a.EMP_ID) AS PENSION_DATE
	FROM	TBL_EMP a
			LEFT OUTER JOIN TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
			LEFT OUTER JOIN TBL_SECTION c ON a.SECT_ID = c.SECT_ID
			LEFT OUTER JOIN TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
			LEFT OUTER JOIN TBL_GRADE f ON a.GRADE = f.GRADE_ID
			LEFT OUTER JOIN TBL_LOCATION g ON a.LOC_ID = g.LOC_ID
			LEFT OUTER JOIN TBL_EMP_GRADE_CLASS_MEMBER h ON a.EMP_ID = h.EMP_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE i ON a.JAMSOSTEK_STATUS_ID = i.TYPE_ID AND i.TYPE = 'JamsostekType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE j ON a.TAX_STATUS_ID = j.TYPE_ID AND j.TYPE = 'TaxStatusType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE k ON a.SALARY_GRADE = k.TYPE_ID AND k.TYPE = 'SalaryGradeType'
			LEFT OUTER JOIN TBL_LEAVE_ALLOCATION l ON a.EMP_ID = l.Emp_id AND l.Yeartr = YEAR(GETDATE())
			LEFT OUTER JOIN TBL_EMP_ASSIGNMENT m ON a.EMP_ID = m.Emp_id AND m.STATUS = 0
			LEFT OUTER JOIN TBL_LOCATION n ON m.LOC_ID = n.LOC_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.BPJS_KESEHATAN_STATUS_ID = o.TYPE_ID AND o.TYPE = 'JamsostekType'
			LEFT OUTER JOIN TBL_FASKES p ON a.BPJS_KESEHATAN_FASKES_CODE = p.CODE
			LEFT OUTER JOIN TBL_EMAIL q ON a.EMP_ID = q.Emp_Id
	where	a.EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeRecruitmentDetailRecord]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEmployeeRecruitmentDetailRecord] 
	@EmployeeId VARCHAR(10)
AS
BEGIN
	Declare @AppId varchar(50)
	
	select top 1 @AppId = APPLICATION_ID
	from dbo.TBL_EMP_HISTORY_RECRUITMENT where EMPLOYEE_ID = @EmployeeId and ISDELETED = 0
	
	SELECT CODE, ENTRY_DATE, PAPER_DATE, PAPER_TIME, 
	INTERVIEW_DATE,  INTERVIEW_TIME, INTERVIEW_PIC, 
	SALARY_DATE, SALARY_PIC, TARGET_DATE,
	HIRED, REMARKS, SECT_ID, RESULT, STATUS, INPUT_SEQ, CATEGORY_STAT, DUE_DATE,  
	ALARM, DESCRIPTION 
	FROM Recruitment.dbo.V1_APPLICANT_DETAIL  WHERE CODE = @AppId
	ORDER BY ENTRY_DATE DESC, INPUT_SEQ DESC
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeRecruitmentRecord]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEmployeeRecruitmentRecord]
	@EmployeeId VARCHAR(10)
AS
BEGIN
	Declare @AppId varchar(50)
	
	select top 1 @AppId = APPLICATION_ID
	from dbo.TBL_EMP_HISTORY_RECRUITMENT where EMPLOYEE_ID = @EmployeeId and ISDELETED = 0
	
	SELECT a.CODE, a.APPLICANT_NAME, a.SECT_ID, b.ABBR_SECT, a.TITLE, c.position_name, a.DATE_OF_BIRTH, a.EDUCATION, a.COMP_CODE, a.CURRENT_COMPANY, 
	a.CURRENT_STATUS, a.EXPECTED_SALARY, a.ENTRY_DATE, a.CONTRACT_TYPE, a.HIRED_DATE, a.STATUS, a.START_DATE, 
	a.PROJECT_NAME, a.REMARKS, a.PHONE, a.MOBILE, a.APP_EMAIL, a.EDUCATION_DESC, 
	a.SCAN_STATUS, a.TECHNICAL_TEST, a.APTITUDE_TEST, a.TOEIC_TEST, a.ENGLISH_TEST, a.UPLOAD_STATUS 
	FROM Recruitment.dbo.TBL_APPLICANT a 
	left join Recruitment.dbo.TBL_SECTION b on a.SECT_ID = b.SECT_ID
	left join Recruitment.dbo.TBL_POSITION c on a.TITLE = c.Pos_id
	WHERE CODE = @AppId
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeStatus]
AS
BEGIN
	SELECT	DISTINCT a.EMP_TYPE_ID
	FROM	TBL_EMP a
	WHERE	a.STATUS = 1
	ORDER	BY a.EMP_TYPE_ID
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeStatusDepartment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeStatusDepartment]
AS
BEGIN
	--SELECT	DISTINCT a.EMP_TYPE_ID
	--FROM	TBL_EMP a
	--WHERE	a.STATUS = 1
	--ORDER	BY a.EMP_TYPE_ID

	SELECT DISTINCT Z.EMP_TYPE_ID, ORDINAL
	FROM
	(SELECT A.EMP_TYPE_ID, 
		CASE A.EMP_TYPE_ID
			WHEN 'RS' THEN 1 
			WHEN 'CS' THEN 2
			WHEN 'TS' THEN 3
			WHEN 'MGT' THEN 4
			WHEN 'SS' THEN 5
			WHEN 'MS' THEN 6
			ELSE 7
		END ORDINAL
	FROM TBL_EMP A WHERE A.STATUS = 1
	) Z ORDER BY Z.ORDINAL

END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeeStatusTotal]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_GetEmployeeStatusTotal]
AS
BEGIN
	--SELECT	a.SECT_ID, b.SECT_NAME, a.EMP_TYPE_ID, COUNT(*) AS TOTAL
	--FROM	TBL_EMP a
	--		INNER JOIN TBL_SECTION b ON a.SECT_ID = b.SECT_ID
	--WHERE	a.STATUS = 1
	--GROUP	BY a.SECT_ID, b.SECT_NAME, a.EMP_TYPE_ID
	--ORDER	BY a.SECT_ID, a.EMP_TYPE_ID
	
	SELECT * FROM
	(
	SELECT c.DEPT_ID, c.DEPT_NAME, a.SECT_ID, b.SECT_NAME, a.EMP_TYPE_ID, COUNT(*) AS TOTAL
	FROM	TBL_EMP a
			INNER JOIN TBL_SECTION b ON a.SECT_ID = b.SECT_ID
			INNER JOIN TBL_DEPT c on b.DEPT_ID = c.DEPT_ID
	WHERE	a.STATUS = 1
	GROUP	BY c.DEPT_ID, c.DEPT_NAME, a.SECT_ID, b.SECT_NAME, a.EMP_TYPE_ID
	--ORDER	BY a.SECT_ID, a.EMP_TYPE_ID
	UNION
	SELECT 'GRAND TOTAL', 'GRAND TOTAL', 'XXXX', 'GRAND TOTAL', a.EMP_TYPE_ID, COUNT(*) AS TOTAL
	FROM	TBL_EMP a
			INNER JOIN TBL_SECTION b ON a.SECT_ID = b.SECT_ID
			INNER JOIN TBL_DEPT c on b.DEPT_ID = c.DEPT_ID
	WHERE	a.STATUS = 1
	GROUP	BY a.EMP_TYPE_ID
	) A
	ORDER BY A.SECT_ID, A.EMP_TYPE_ID
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEnglishAssessment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEnglishAssessment '2076'
CREATE PROCEDURE [dbo].[usp_GetEnglishAssessment]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ENGLISH_ASSESSMENT_DATE DESC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.ENGLISH_ASSESSMENT_TYPE_ID, a.ENGLISH_ASSESSMENT_TYPE_NAME, 
			a.ENGLISH_ASSESSMENT_SCORE, a.ENGLISH_ASSESSMENT_DATE, a.ENGLISH_ASSESSMENT_ATTACHMENT
	FROM	TBL_EMP_ENGLISH_ASSESSMENT a
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetFamily]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetFamily '0716'
CREATE PROCEDURE [dbo].[usp_GetFamily]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY b.SEQUENCE, a.BIRTHDATE) AS NO, a.TR_NUMBER AS ID, a.EMP_ID, a.REL_ID, b.TYPE_NAME AS REL_DESC, ISNULL(a.MEDICAL_ID, 0) AS MEDICAL_ID, CASE ISNULL(a.MEDICAL_ID, 0) WHEN 0 THEN '' ELSE a.EMP_ID + '-' + CAST(a.MEDICAL_ID AS VARCHAR(10)) END AS MEDICAL_NAME, a.SNAME, 
			a.GENDER AS SEX_ID, c.TYPE_NAME AS SEX_NAME,
			a.BIRTHDATE, a.BIRTH_CERTIFICATE_ATTACHMENT, a.LIFE_STATUS_ID, a.LIFE_STATUS_NAME, a.OCCUPATION, a.REMARKS, a.MEDICAL_COVERED_ID, d.TYPE_NAME AS MEDICAL_COVERED_NAME
	FROM	TBL_FAMILY a
			LEFT OUTER JOIN TBL_GENERAL_TYPE b ON a.REL_ID = b.TYPE_ID AND b.TYPE = 'FamilyType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE c ON a.GENDER = c.TYPE_ID AND c.TYPE = 'SexType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE d ON a.MEDICAL_COVERED_ID = d.TYPE_ID AND d.TYPE = 'CoveredMedicalType'
	WHERE	a.EMP_ID = @EmployeeId AND a.IS_DELETED = 0
	ORDER BY b.SEQUENCE, a.BIRTHDATE
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetFamilyDocumentAttachment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec GetFamilyDocumentAttachment '2076'
CREATE PROCEDURE [dbo].[usp_GetFamilyDocumentAttachment]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ID ASC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.FAMILY_DOCUMENT_TYPE_ID, b.TYPE_NAME AS FAMILY_DOCUMENT_TYPE_NAME, a.FAMILY_DOCUMENT_ATTACHMENT, a.FAMILY_DOCUMENT_REMARK
	FROM	TBL_FAMILY_DOCUMENT_ATTACHMENT a
			LEFT OUTER JOIN TBL_GENERAL_TYPE b ON a.FAMILY_DOCUMENT_TYPE_ID = b.TYPE_ID AND b.TYPE = 'FamilyDocumentType'
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_getFwbs6]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_getFwbs6]
@EMP_ID varchar(4)
AS

select a.Dropdown_text, a.Dropdown_value 
from PFNATT.dbo.TBL_MASTER_DROPDOWN a 
where a.Application = 'fwbs6' and a.IsActive = 'True'


GO
/****** Object:  StoredProcedure [dbo].[usp_GetGeneralHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetGeneralHistory '2076', ''
CREATE PROCEDURE [dbo].[usp_GetGeneralHistory]
(
	@KeyId VARCHAR(20),
	@TableName VARCHAR(512),
	@ColumnName VARCHAR(512)
)
AS
BEGIN
	SELECT ROW_NUMBER() OVER(ORDER BY a.CREATION_DATE ASC) AS NO, a.CREATION_DATE, a.CREATED_BY, b.FULL_NAME, ISNULL(a.COLUMN_VALUE, '') AS COLUMN_VALUE
	FROM TBL_CHANGE_HISTORY a
			INNER JOIN TBL_EMP b ON a.CREATED_BY = b.EMP_ID
	WHERE KEY_ID = @KeyId AND TABLE_NAME = @TableName AND COLUMN_NAME = @ColumnName
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGeneralType]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetGeneralType 'SalaryGradeType'
CREATE PROCEDURE [dbo].[usp_GetGeneralType]
(
	@Type VARCHAR(255)
)
AS
BEGIN
	SELECT	a.TYPE_ID, a.TYPE_NAME
	FROM	TBL_GENERAL_TYPE a
	WHERE	a.TYPE = @Type AND IS_DELETED = 0
	ORDER BY a.SEQUENCE, a.ID
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGradeClassificationList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetDepartmentList '2076'
CREATE PROCEDURE [dbo].[usp_GetGradeClassificationList]
(
	@DepartmentId VARCHAR(10), @SalaryGrade VARCHAR(10)
)
AS
BEGIN
	SELECT	a.GRADECLASS_ID AS GRADE_CLASSIFICATION_ID, RTRIM(LTRIM(a.GRADE_POS)) + ' - ' + a.GRADE_CLASSIFICATION AS GRADE_CLASSIFICATION_NAME
	FROM	TBL_EMP_GRADE_CLASS a
	WHERE	a.DEPT_ID = @DepartmentId AND a.GRADE_POS = @SalaryGrade
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGroupList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetDepartmentList '2076'
CREATE PROCEDURE [dbo].[usp_GetGroupList]
(
	@DepartmentId VARCHAR(10)
)
AS
BEGIN
	SELECT	LTRIM(RTRIM(a.GROUP_ID)) AS GROUP_ID, a.GROUP_NAME AS GROUP_NAME
	FROM	TBL_EMP_GROUP a
	WHERE	a.SECT_ID = @DepartmentId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLanguageSkill]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetLanguageSkill '2076'
CREATE PROCEDURE [dbo].[usp_GetLanguageSkill]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.LANG_ID ASC) AS NO, a.LANG_ID AS ID, a.EMP_ID, a.LANG_ID, a.LANGUAGE_ID, CASE ISNULL(e.TYPE_NAME, '') WHEN '' THEN a.LANGUAGE_ ELSE e.TYPE_NAME END AS LANGUAGE_NAME, a.CLASSIFICATION, 
			a.SKILL_READ AS LANGUAGE_READ_ID, b.TYPE_NAME AS LANGUAGE_READ_NAME, a.SKILL_WRITE AS LANGUAGE_WRITE_ID, c.TYPE_NAME AS LANGUAGE_WRITE_NAME, 
			a.SKILL_SPEAK AS LANGUAGE_SPEAK_ID, d.TYPE_NAME AS LANGUAGE_SPEAK_NAME
	FROM	TBL_EMP_LANGUAGE a
			LEFT OUTER JOIN TBL_GENERAL_TYPE b ON a.SKILL_READ = b.TYPE_ID AND b.TYPE = 'LanguageType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE c ON a.SKILL_WRITE = c.TYPE_ID AND c.TYPE = 'LanguageType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE d ON a.SKILL_SPEAK = d.TYPE_ID AND d.TYPE = 'LanguageType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.LANGUAGE_ID = e.TYPE_ID AND e.TYPE = 'LanguageNameType'
	WHERE	a.EMP_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLoan]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetLoan '2076'
CREATE PROCEDURE [dbo].[usp_GetLoan]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	--SELECT  ROW_NUMBER() OVER(ORDER BY a.START_DATE DESC) AS NO, 
	select ROW_NUMBER() OVER(ORDER BY a.START_DATE desc) AS NO,*
	from
	(
	select a.ID AS ID, a.EMPLOYEE_ID,  
	a.BANK_NAME,a.START_DATE,a.END_DATE, a.AMOUNT, a.DURATION, a.INSTALLMENT, a.PAYMENT, 
	a.PAID_OFF_DATE, a.AS_OF_DATE, a.PAID_OFF_ATTACHMENT, a.ACTIVE, a.UPDATED_BY, a.UPDATED_DATE,
	b.FULL_NAME, b.POSITION_ID, c.POSITION_NAME AS POSITION_NAME, b.SECT_ID AS DEPARTMENT_ID, 
		substring(d.SECT_NAME,CHARINDEX('-',d.SECT_NAME)+2,LEN(d.SECT_NAME)) AS DEPARTMENT_NAME, 
		d.DEPT_ID AS DIVISION_ID, substring(e.DEPT_NAME,CHARINDEX('-',e.DEPT_NAME)+2,LEN(e.DEPT_NAME)) AS DIVISION_NAME
	FROM	TBL_LOAN a
			INNER JOIN TBL_EMP b ON a.EMPLOYEE_ID = b.EMP_ID
			INNER JOIN PFNATT.dbo.TBL_POSITION c ON b.POSITION_ID = c.POSITION_ID
			INNER JOIN PFNATT.dbo.TBL_SECTION d ON b.SECT_ID = d.SECT_ID
			INNER JOIN PFNATT.dbo.TBL_DEPT e ON d.DEPT_ID = e.DEPT_ID
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0 and (a.PAID_OFF_DATE is not null or a.PAID_OFF_DATE != '1900/01/01')
	
	Union 
	
	select top 1 a.ID AS ID, a.EMPLOYEE_ID,  
	a.BANK_NAME,a.START_DATE,a.END_DATE, a.AMOUNT, a.DURATION, a.INSTALLMENT, a.PAYMENT, 
	a.PAID_OFF_DATE, a.AS_OF_DATE, a.PAID_OFF_ATTACHMENT, a.ACTIVE, a.UPDATED_BY, a.UPDATED_DATE,
	b.FULL_NAME, b.POSITION_ID, c.POSITION_NAME AS POSITION_NAME, b.SECT_ID AS DEPARTMENT_ID, 
		substring(d.SECT_NAME,CHARINDEX('-',d.SECT_NAME)+2,LEN(d.SECT_NAME)) AS DEPARTMENT_NAME, 
		d.DEPT_ID AS DIVISION_ID, substring(e.DEPT_NAME,CHARINDEX('-',e.DEPT_NAME)+2,LEN(e.DEPT_NAME)) AS DIVISION_NAME
	FROM	TBL_LOAN a
			INNER JOIN TBL_EMP b ON a.EMPLOYEE_ID = b.EMP_ID
			INNER JOIN PFNATT.dbo.TBL_POSITION c ON b.POSITION_ID = c.POSITION_ID
			INNER JOIN PFNATT.dbo.TBL_SECTION d ON b.SECT_ID = d.SECT_ID
			INNER JOIN PFNATT.dbo.TBL_DEPT e ON d.DEPT_ID = e.DEPT_ID
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0 and (a.PAID_OFF_DATE is null or a.PAID_OFF_DATE = '1900/01/01')
	order by a.AS_OF_DATE desc
	) a
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedical]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetMedical '2076'
CREATE PROCEDURE [dbo].[usp_GetMedical]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.TR_NUMBER ASC) AS NO, a.TR_NUMBER AS ID, a.EMP_ID, 
	a.TYPE_ID, a.TYPE_NAME, a.DETAIL_ID, a.DETAIL_NAME, a.MEDICAL_DATE, a.REFFERENCE, a.ATTACHMENT, a.REMARKS
	FROM	TBL_MEDICAL a
	WHERE	a.EMP_ID = @EmployeeId AND IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetNewEmployeeId]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetNewEmployeeId '2076'
CREATE PROCEDURE [dbo].[usp_GetNewEmployeeId]
(
	@EmployeeType VARCHAR(50)
)
AS
BEGIN
	IF @EmployeeType = 'MGT'
	BEGIN
		SELECT MAX(a.EMP_ID) + 1 AS NEW_EMPLOYEE_ID
		FROM TBL_EMP a
		WHERE SUBSTRING(a.EMP_ID, 1, 1) = '9'
	END
	ELSE IF @EmployeeType = 'MS'
	BEGIN
		SELECT MAX(a.EMP_ID) + 1 AS NEW_EMPLOYEE_ID
		FROM TBL_EMP a
		WHERE SUBSTRING(a.EMP_ID, 1, 1) = '6'
	END
	ELSE
	BEGIN
		SELECT MAX(a.EMP_ID) + 1 AS NEW_EMPLOYEE_ID
		FROM TBL_EMP a
		WHERE SUBSTRING(a.EMP_ID, 1, 1) not in ('9', '8', '7', '6', '5', '4')
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_getOvertimeProject]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec GetPlafondDental '', '2076'
CREATE PROCEDURE [dbo].[usp_getOvertimeProject]
(
	@EmpId VARCHAR(50),
	@LastYear VARCHAR(20),
	@LastMonth int
)
AS
BEGIN
	
	SELECT 	DISTINCT RTRIM(LTRIM(X1.JOBCODE_ID))+'|'+RTRIM(LTRIM(X1.CLIENT_JOBCODE))+'|'+RTRIM(LTRIM(X1.SECT_ID))+'|'+RTRIM(LTRIM(X1.CATEGORY_ID)) AS JOBCODE_ID, 
			'['+RTRIM(LTRIM(X1.JOBCODE_ID))+'] ['+LTRIM(RTRIM(X1.Category_Id)) +'] '+LTRIM(RTRIM(X1.PROJECT_NAME)) AS PROJECT_NAME, 
			ISNULL(X1.SUPERVISOR,'') AS SUPERVISOR
			, ISNULL(X1.PM,'') AS PM
	FROM 	MICADEBUG.DBO.V_MHPLAN_MONTH X1
			, MICADEBUG.DBO.TBL_PROJECT X2 
	WHERE X1.JOBCODE_ID = X2.JOBCODE_ID 
		AND X1.STATUSPLAN = 1 
		AND X1.P_YEAR = @LastYear
		AND X1.EMP_ID = @EmpId 
		AND X1.JOBCODE_ID NOT LIKE 'N%'
		AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@LastMonth as char) + '/01/' + cast(@LastYear as char) as datetime))), DATEADD(m,1,cast(cast(@LastMonth as char) + '/01/' + cast(@LastYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X2.ENDDATE)), DATEADD(m,1,X2.ENDDATE))
		AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@LastMonth as char) + '/01/' + cast(@LastYear as char) as datetime))), DATEADD(m,1,cast(cast(@LastMonth as char) + '/01/' + cast(@LastYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X1.END_DATE)), DATEADD(m,1,X1.END_DATE))
	UNION ALL
	SELECT 	RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LTRIM(RTRIM(DEPT)) +'|'+RTRIM(LTRIM(CATEGORY_ID)) AS JOBCODE_ID,
			'['+RTRIM(LTRIM(JOBCODE_ID))+'] ['+ LTRIM(RTRIM(Category_Id)) +'] '+LTRIM(RTRIM(DESCRIPTION)) AS PROJECT_NAME,
			MICADEBUG.DBO.FUNCT_WHOISDM(@EmpId) AS SUPERVISOR
			, MICADEBUG.DBO.FUNCT_WHOISDivM(@EmpId) AS PM
	FROM 	MICADEBUG.dbo.TBL_PROJECT_GENERAL 
	WHERE TRAINING=0 
	AND DEPT IN (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @EmpId)  
	AND JOBCODE_ID NOT LIKE 'N%'
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetPersonal]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPersonal '2076'
CREATE PROCEDURE [dbo].[usp_GetPersonal]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, UPPER(a.FULL_NAME) AS EMPLOYEE_NAME, a.FIRSTNAME, CASE ISNULL(a.MIDDLENAME, '-') WHEN '' THEN '-' ELSE a.MIDDLENAME END AS MIDDLENAME, ISNULL(a.LASTNAME, '-') AS LASTNAME, a.EMP_TYPE_ID AS EMPLOYEE_TYPE_ID, 
			e.TYPE_NAME AS EMPLOYEE_TYPE_NAME, a.GRADE AS GRADE_ID, f.GRADE_NAME AS GRADE_NAME, a.BIRTHDATE, a.BIRTHLOC, a.BIRTH_CERTIFICATE, a.SEX, 
			CASE a.SEX WHEN 'M' THEN 'MALE' WHEN 'F' THEN 'FEMALE' ELSE '-' END AS SEX_NAME, a.MAR_STAT_ID, h.DESCRIPTION AS MAR_STAT_NAME, a.RELIGION_ID, ISNULL(g.RELIGION_NAME,'-') as RELIGION_NAME,
			(SELECT TOP 1 GROUP_ID FROM TBL_EMP_GROUP_MEMBER WHERE EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC) AS GROUP_ID,
			UPPER((SELECT TOP 1 b.GROUP_NAME FROM TBL_EMP_GROUP_MEMBER a INNER JOIN TBL_EMP_GROUP b ON a.GROUP_ID = b.GROUP_ID WHERE a.EMP_ID = @EmployeeId ORDER BY YEARTR DESC, MONTHTR DESC)) AS GROUP_NAME,
			a.POSITION_ID, UPPER(b.POSITION_NAME) AS POSITION_NAME, a.SECT_ID AS DEPARTMENT_ID, UPPER(c.SECT_NAME) AS DEPARTMENT_NAME, 
			c.DEPT_ID AS DIVISION_ID, UPPER(d.DEPT_NAME) AS DIVISION_NAME, a.SALARY_GRADE, a.FRONTTITLE AS FIRST_TITLE_ID, ISNULL(m.TYPE_NAME,'-') AS FIRST_TITLE_NAME, ISNULL(a.LASTTITTLE,'-') AS LASTTITTLE, a.ADDRESS_TYPE_ID AS PRESENT_ADDRESS_TYPE_ID, 
			ISNULL(i.TYPE_NAME,'-') AS PRESENT_ADDRESS_TYPE_NAME, ISNULL(a.ADDRESS,'-') AS PRESENT_ADDRESS, dbo.GetAge(a.EMP_ID) AS AGE,
			ISNULL(a.CITY,'-') AS PRESENT_CITY, ISNULL(a.REGION,'-') AS PRESENT_REGION, ISNULL(a.COUNTRY,'-') AS PRESENT_COUNTRY_ID, ISNULL(o.TYPE_NAME ,ISNULL(a.COUNTRY,'-')) AS PRESENT_COUNTRY_NAME, 
			ISNULL(a.POSTALCODE,'-') AS PRESENT_POSTAL_CODE, 
			a.PADDRESS_TYPE_ID AS PERMANENT_ADDRESS_TYPE_ID, ISNULL(j.TYPE_NAME,'-') AS PERMANENT_ADDRESS_TYPE_NAME, ISNULL(a.PADDRESS,'-') AS PERMANENT_ADDRESS,
			ISNULL(a.PCITY,'-') AS PERMANENT_CITY, ISNULL(a.PREGION,'-') AS PERMANENT_REGION, ISNULL(a.PCOUNTRY,'-') AS PERMANENT_COUNTRY_ID, ISNULL(p.TYPE_NAME, ISNULL(a.PCOUNTRY,'-')) AS PERMANENT_COUNTRY_NAME, 
			ISNULL(a.PPOSTALCODE,'-') AS PERMANENT_POSTAL_CODE,
			ISNULL(a.EMAIL,'-') AS EMAIL, ISNULL(a.PHONE,'-') AS TELEPHONE, ISNULL(a.HP,'-') AS MOBILE_PHONE, ISNULL(a.HP1,'-') AS MOBILE_PHONE1, ISNULL(a.EMERGENCY_PHONE,'-') AS EMERGENCY_PHONE, ISNULL(a.EMERGENCY_PHONE_NAME,'-') AS EMERGENCY_PHONE_NAME, a.EMERGENCY_PHONE_RELATION_ID, 
			ISNULL(k.REL_DESC,'-') AS EMEREGENCY_PHONE_RELATION_NAME, a.NATIONALITY AS NATIONALITY_ID, ISNULL(n.TYPE_NAME, ISNULL(a.NATIONALITY,'-')) AS NATIONALITY_NAME, 
			ISNULL(a.BLOOD_TYPE,'-') AS BLOOD_TYPE, ISNULL(a.IDNUMBER,'-') AS KTP_NUMBER, a.IDNUMBER_EXPIRE AS KTP_EXPIRE, ISNULL(a.KK_NUMBER,'-') AS KK_NUMBER, a.IDNUMBER_ATTACHMENT AS KTP_ATTACHMENT, ISNULL(KITAS_NUMBER,'') AS KITAS_NUMBER, KITAS_EXPIRE AS KITAS_EXPIRE, ISNULL(KITAS_POSITION,'-') AS KITAS_POSITION, KITAS_ATTACHMENT,
			ISNULL(a.PASSPORT,'-') AS PASSPORT, a.PASSPORTV AS PASSPORT_EXPIRE, a.PASSPORT_ATTACHMENT AS PASSPORT_ATTACHMENT, ISNULL(a.NPWP_NUMBER,'-') AS NPWP_NUMBER, ISNULL(a.NPWP_ADDRESS,'-') AS NPWP_ADDRESS, a.NPWP_ATTACHMENT, a.PREFER_SEND_LETTER_ID, l.TYPE_NAME AS PREFER_SEND_LETTER_NAME,
			a.STATUS AS STATUS_ID, ISNULL(q.TYPE_NAME, a.STATUS) AS STATUS_NAME,
			r.Email AS EMAIL_CORPORATE
	FROM	TBL_EMP a
			LEFT OUTER JOIN TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
			LEFT OUTER JOIN TBL_SECTION c ON a.SECT_ID = c.SECT_ID
			LEFT OUTER JOIN TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
			LEFT OUTER JOIN TBL_EMP_TYPE e ON a.EMP_TYPE_ID = e.EMP_TYPE_ID
			LEFT OUTER JOIN TBL_GRADE f ON a.GRADE = f.GRADE_ID
			LEFT OUTER JOIN TBL_RELIGION g ON a.RELIGION_ID = g.RELIGION_ID
			LEFT OUTER JOIN TBL_MAR_STAT h ON a.MAR_STAT_ID = h.MAR_STAT_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE i ON a.ADDRESS_TYPE_ID = i.TYPE_ID AND i.TYPE = 'AddressType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE j ON a.PADDRESS_TYPE_ID = j.TYPE_ID AND j.TYPE = 'AddressType'
			LEFT OUTER JOIN TBL_FAM_REL k ON a.EMERGENCY_PHONE_RELATION_ID = k.REL_ID
			LEFT OUTER JOIN TBL_GENERAL_TYPE l ON a.PREFER_SEND_LETTER_ID = l.TYPE_ID AND l.TYPE = 'SendLetterType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE m ON a.FRONTTITLE = m.TYPE_ID AND m.TYPE = 'FirstTitleType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE n ON a.NATIONALITY = n.TYPE_ID AND n.TYPE = 'CountryType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.COUNTRY = o.TYPE_ID AND o.TYPE = 'CountryType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE p ON a.PCOUNTRY = p.TYPE_ID AND p.TYPE = 'CountryType'
			LEFT OUTER JOIN TBL_GENERAL_TYPE q ON a.STATUS = q.TYPE_ID AND q.TYPE = 'EmpStatusType'
			LEFT OUTER JOIN TBL_EMAIL r ON a.EMP_ID = r.Emp_Id
	where	a.EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPositionList]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetPositionList]
AS
BEGIN
	SELECT POSITION_ID, POSITION_NAME,STATUS, B.TYPE_NAME STATUS_NAME,STRUCTURAL,C.TYPE_NAME STRUCTURAL_NAME  
	FROM TBL_POSITION A
	LEFT JOIN TBL_GENERAL_TYPE B ON A.STATUS = B.TYPE_ID AND B.TYPE = 'IsActive'
	LEFT JOIN TBL_GENERAL_TYPE C ON A.STRUCTURAL = C.TYPE_ID AND C.TYPE = 'IsStructural'
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetPreviousCompany]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPreviousCompany '2076'
CREATE PROCEDURE [dbo].[usp_GetPreviousCompany]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.ENDDATE DESC) AS NO, a.TR_NUMBER AS ID, a.EMP_ID, a.COMPANY, a.COMPANY_DESC, a.REFFERENSI, a.POS, a.STARTDATE, a.ENDDATE, a.CURRENCY, a.LASTSALARY,
			a.JOBDESC, a.RLEAVING, a.REMARKS
	FROM	TBL_JOB_EX a
	WHERE	a.EMP_ID = @EmployeeId AND IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPreviousCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPreviousCourse '2076'
CREATE PROCEDURE [dbo].[usp_GetPreviousCourse]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.TR_Number ASC) AS NO, a.TR_Number AS ID, a.EMP_ID, a.SUBJECT, a.PROVIDER, a.C_FROM, a.C_TO, a.PLACEOFTRAINING, a.TRAINER, a.PURPOSE, a.GRADE,
			a.CURRENCY, a.AMOUNT, a.REMARKS
	FROM	TBL_COURSE a
	WHERE	a.EMP_ID = @EmployeeId AND IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPreviousProject]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPreviousProject '2076'
CREATE PROCEDURE [dbo].[usp_GetPreviousProject]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.TR_Number ASC) AS NO, a.TR_Number AS ID, a.EMP_ID, a.Startdate, a.Enddate, a.ProjectType, a.ProjectName, a.Location, a.ProjectOwner, a.ProjectValue,
			a.CompanyName, a.BL_ID AS COMPANY_BUSINESS_LINE, Pos_ID AS POSITION
	FROM	TBL_PROJECT_PREV a
	WHERE	a.EMP_ID = @EmployeeId AND IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPreviousProjectCV]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetPreviousProjectCV '2076'
CREATE PROCEDURE [dbo].[usp_GetPreviousProjectCV]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	DECLARE @MainId INT

	SELECT TOP 1 @MainId = ID FROM CareerResume.dbo.CAREER_RESUME_MAIN WHERE EMPLOYEE_ID = @EmployeeId AND STATUS = 1 ORDER BY COMPLETED_DATE DESC
	SELECT	0 AS ID, ROW_NUMBER() OVER(ORDER BY a.START_DATE DESC) AS NO, 0 AS MAIN_ID, 
			a.COMPANY_NAME, a.PROJECT_NAME, a.START_DATE, a.END_DATE, a.PROJECT_LOCATION, a.PROJECT_OWNER, a.PROJECT_POSITION, a.PROJECT_RESPONSIBILITY
	FROM	CareerResume.dbo.CAREER_RESUME_PREVIOUS_EXPERIENCE a
	WHERE	MAIN_ID = @MainId AND a.IS_DELETED = 0
	ORDER	BY a.START_DATE DESC
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportBPJS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeePersonalData '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportBPJS]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) As BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE, 
				ISNULL(a.BPJS_KESEHATAN_NO,'-') BPJS_KESEHATAN_NO, a.BPJS_KESEHATAN_STATUS_ID, ISNULL(o.TYPE_NAME,'-') AS BPJS_KESEHATAN_STATUS_NAME,
				ISNULL(a.BPJS_KESEHATAN_FASKES_CODE,'-') BPJS_KESEHATAN_FASKES_CODE, ISNULL(p.NAME,'-') AS BPJS_KESEHATAN_FASKES_NAME,
				a.BPJS_KESEHATAN_START_DATE, a.BPJS_KESEHATAN_ATTACHMENT
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.BPJS_KESEHATAN_STATUS_ID = o.TYPE_ID AND o.TYPE = 'JamsostekType'
				LEFT OUTER JOIN TBL_FASKES p ON a.BPJS_KESEHATAN_FASKES_CODE = p.CODE
		where (TEST1 is null or TEST1 <> '1') and a.STATUS = 1 and BPJS_KESEHATAN_NO is not null and BPJS_KESEHATAN_NO != '' and BPJS_KESEHATAN_NO != '-'
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) as BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE,
				ISNULL(a.BPJS_KESEHATAN_NO,'-') BPJS_KESEHATAN_NO, a.BPJS_KESEHATAN_STATUS_ID, ISNULL(o.TYPE_NAME,'-') AS BPJS_KESEHATAN_STATUS_NAME,
				ISNULL(a.BPJS_KESEHATAN_FASKES_CODE,'-') BPJS_KESEHATAN_FASKES_CODE, ISNULL(p.NAME,'-') AS BPJS_KESEHATAN_FASKES_NAME,
				a.BPJS_KESEHATAN_START_DATE, a.BPJS_KESEHATAN_ATTACHMENT
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.BPJS_KESEHATAN_STATUS_ID = o.TYPE_ID AND o.TYPE = 'JamsostekType'
				LEFT OUTER JOIN TBL_FASKES p ON a.BPJS_KESEHATAN_FASKES_CODE = p.CODE
		where (TEST1 is null or TEST1 <> '1') and a.STATUS = 1 and BPJS_KESEHATAN_NO is not null and BPJS_KESEHATAN_NO != '' and BPJS_KESEHATAN_NO != '-'
	END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeContractStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeDepartment '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeContractStatus]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID = 'CS'
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID = 'CS'
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeContractStatusAlert]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeDepartment '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeContractStatusAlert]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
			and a.STATUS = 1 and ((Month(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Year(@EffectiveDate)) OR (Month(DATEADD(month, -4, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -4, a.END_DATE_CONTRACT)) = YEAR(@EffectiveDate)))
		 ORDER BY a.END_DATE_CONTRACT, a.EMP_ID
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
			and a.STATUS = 1 and ((Month(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -3, a.END_DATE_CONTRACT)) = Year(@EffectiveDate)) OR (Month(DATEADD(month, -4, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -4, a.END_DATE_CONTRACT)) = YEAR(@EffectiveDate)))
		ORDER BY a.END_DATE_CONTRACT, a.EMP_ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeContractStatusAlertRemainder]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeDepartment '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeContractStatusAlertRemainder]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
			and a.STATUS = 1 and ((Month(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Year(@EffectiveDate)) OR (Month(DATEADD(month, -2, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -2, a.END_DATE_CONTRACT)) = YEAR(@EffectiveDate)))
		 ORDER BY a.END_DATE_CONTRACT, a.EMP_ID
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	ROW_NUMBER() over (order by a.end_date_contract) as NUMBER, a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,a.EXP_DATE,a.EXP_DATE1,a.START_DATE,a.BIRTHDATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		WHERE	a.EMP_TYPE_ID IN ('CS','RSP')
			and a.STATUS = 1 and ((Month(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -1, a.END_DATE_CONTRACT)) = Year(@EffectiveDate)) OR (Month(DATEADD(month, -2, a.END_DATE_CONTRACT)) = Month(@EffectiveDate) and YEAR(DATEADD(month, -2, a.END_DATE_CONTRACT)) = YEAR(@EffectiveDate)))
		ORDER BY a.END_DATE_CONTRACT, a.EMP_ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeDepartment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeeDepartment '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeDepartment]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, dbo.ProperCase(e.TYPE_NAME) AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
				CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		ORDER BY c.DEPT_ID,A.SECT_ID,g.GROUP_ID
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, dbo.ProperCase(e.TYPE_NAME) AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
				CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
		ORDER BY c.DEPT_ID,A.SECT_ID,g.GROUP_ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeExperience]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetReportEmployeeExperience '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeExperience]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				(SELECT TOP 1 COMPANY FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC) AS LAST_COMPANY,
				dbo.GetPreviousExperienceInYear(a.EMP_ID) AS PREVIOUS_EXPERIENCE,
				dbo.GetCurrentExperienceInYear(a.EMP_ID) AS CURRENT_EXPERIENCE,
				dbo.GetTotalExperienceInYear(a.EMP_ID) AS TOTAL_EXPERIENCE,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
				CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				(SELECT TOP 1 COMPANY FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC) AS LAST_COMPANY,
				dbo.GetPreviousExperienceInYear(a.EMP_ID) AS PREVIOUS_EXPERIENCE,
				dbo.GetCurrentExperienceInYear(a.EMP_ID) AS CURRENT_EXPERIENCE,
				dbo.GetTotalExperienceInYear(a.EMP_ID) AS TOTAL_EXPERIENCE,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
				CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeePersonalData]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEmployeePersonalData '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeePersonalData]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) As BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) as BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportEmployeeResign]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetReportEmployeeResign '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportEmployeeResign]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	--IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	--BEGIN
	--	WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
	--	AS
	--	(
	--		SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
	--		FROM	TBL_EMP_GROUP_MEMBER
	--		WHERE	MONTHTR = @Month and YEARTR = @Year
	--	)
	--	SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
	--			a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
	--			a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
	--			k.TYPE_NAME AS RESIGN_TYPE, j.NEXT_COMPANY,j.REMARK,
	--			a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
	--			CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
	--	FROM	PFNATT.dbo.TBL_EMP a
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
	--			LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
	--			LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
	--			LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
	--			INNER JOIN Resignation.dbo.RESIGN_MAIN i ON a.EMP_ID = i.EMPLOYEE_ID
	--			INNER JOIN Resignation.dbo.RESIGN_DETAIL j ON i.ID = j.MAIN_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_GENERAL_TYPE k ON i.RESIGN_TYPE = k.TYPE_ID AND k.TYPE = 'ResignType'
	--END
	--ELSE
	--BEGIN
	--	WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
	--	AS
	--	(
	--		SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
	--		FROM	TBL_EMP_GROUP_MEMBER
	--		WHERE	MONTHTR = @Month and YEARTR = @Year
	--	)
	--	SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, g.GROUP_ID, h.GROUP_NAME,
	--			a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
	--			a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
	--			k.TYPE_NAME AS RESIGN_TYPE, j.NEXT_COMPANY,j.REMARK,
	--			a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
	--			CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME
	--	FROM	PFNATT.dbo.TBL_EMP a
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
	--			LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
	--			LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
	--			LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
	--			INNER JOIN Resignation.dbo.RESIGN_MAIN i ON a.EMP_ID = i.EMPLOYEE_ID
	--			INNER JOIN Resignation.dbo.RESIGN_DETAIL j ON i.ID = j.MAIN_ID
	--			LEFT OUTER JOIN PFNATT.dbo.TBL_GENERAL_TYPE k ON i.RESIGN_TYPE = k.TYPE_ID AND k.TYPE = 'ResignType'
	--END

		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, dbo.GetLastGroupId(a.EMP_ID) GROUP_ID,dbo.GetLastGroupName(a.EMP_ID) GROUP_NAME,
				a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.GetAgeInt(a.EMP_ID) AS AGE, a.LOC_ID, f.LOC_NAME,
				k.TYPE_NAME AS RESIGN_TYPE, j.NEXT_COMPANY,j.REMARK,j.EFFECTIVE_DATE,
				a.EMP_TYPE_ID, dbo.ProperCase(e.TYPE_NAME) AS EMP_TYPE_NAME, a.JOIN_DATE, a.STATUS AS STATUS_ID,
				CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				(SELECT TOP 1 COMPANY FROM TBL_JOB_EX WHERE EMP_ID = a.EMP_ID AND IS_DELETED = 0 ORDER BY ENDDATE DESC) AS LAST_COMPANY
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				INNER JOIN Resignation.dbo.RESIGN_MAIN i ON a.EMP_ID = i.EMPLOYEE_ID
				INNER JOIN Resignation.dbo.RESIGN_DETAIL j ON i.ID = j.MAIN_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_GENERAL_TYPE k ON i.RESIGN_TYPE = k.TYPE_ID AND k.TYPE = 'ResignType'

END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetReportNonBPJS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEmployeePersonalData '01/08/2015'
CREATE PROCEDURE [dbo].[usp_GetReportNonBPJS]
(
	@EffectiveDate DateTime
)
AS
BEGIN
	DECLARE @Month INT
	DECLARE @Year INT
	DECLARE @Count INT
	SET @Month = MONTH(@EffectiveDate)
	SET @Year = YEAR(@EffectiveDate)
	SET @Count = 0

	WHILE NOT EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE MONTHTR = @Month AND YEARTR = @Year) AND @Count < 10
	BEGIN
		IF @Month = 1
		BEGIN
			SET @Month = 12
			SET @Year = @Year - 1
		END
		ELSE
		BEGIN
			SET @Month = @Month - 1
		END
		SET @Count = @Count + 1
	END
	
	IF CONVERT(VARCHAR(10), @EffectiveDate, 10) = CONVERT(VARCHAR(10), GETDATE(), 10)
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) As BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE, 
				ISNULL(a.BPJS_KESEHATAN_NO,'-') BPJS_KESEHATAN_NO, a.BPJS_KESEHATAN_STATUS_ID, ISNULL(o.TYPE_NAME,'-') AS BPJS_KESEHATAN_STATUS_NAME,
				ISNULL(a.BPJS_KESEHATAN_FASKES_CODE,'-') BPJS_KESEHATAN_FASKES_CODE, ISNULL(p.NAME,'-') AS BPJS_KESEHATAN_FASKES_NAME,
				a.BPJS_KESEHATAN_START_DATE, a.BPJS_KESEHATAN_ATTACHMENT
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.BPJS_KESEHATAN_STATUS_ID = o.TYPE_ID AND o.TYPE = 'JamsostekType'
				LEFT OUTER JOIN TBL_FASKES p ON a.BPJS_KESEHATAN_FASKES_CODE = p.CODE
		where a.STATUS = 1 and (BPJS_KESEHATAN_NO is null or BPJS_KESEHATAN_NO = '' or BPJS_KESEHATAN_NO = '-' ) and (TEST1 is null or TEST1 <> '1')
	END
	ELSE
	BEGIN
		WITH TBL_EMPLOYEE_GROUP(EMP_ID, GROUP_ID)
		AS
		(
			SELECT	LTRIM(RTRIM(EMP_ID)), GROUP_ID 
			FROM	TBL_EMP_GROUP_MEMBER
			WHERE	MONTHTR = @Month and YEARTR = @Year
		)
		SELECT	a.REF_ID, a.EMP_ID AS EMPLOYEE_ID, a.FULL_NAME AS EMPLOYEE_NAME, a.POSITION_ID, b.POSITION_NAME AS POSITION_NAME, 
				g.GROUP_ID, h.GROUP_NAME, a.SECT_ID AS DEPARTMENT_ID, substring(c.SECT_NAME,CHARINDEX('-',c.SECT_NAME)+2,LEN(c.SECT_NAME)) AS DEPARTMENT_NAME, c.DEPT_ID AS DIVISION_ID, substring(d.DEPT_NAME,CHARINDEX('-',d.DEPT_NAME)+2,LEN(d.DEPT_NAME)) AS DIVISION_NAME, 
				a.SALARY_GRADE, dbo.ProperCase(a.BIRTHLOC) as BIRTHLOC, a.BIRTHDATE, dbo.GetAgeInt(a.EMP_ID) AS AGE, 
				a.SEX, CASE a.SEX WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE '' END AS SEX_NAME, 
				a.MAR_STAT_ID, dbo.ProperCase(i.DESCRIPTION) AS MAR_STAT_NAME, a.TAX_STATUS_ID, a.NPWP_NUMBER, a.ADDRESS, a.HP, a.LOC_ID, f.LOC_NAME,
				a.EMP_TYPE_ID, e.TYPE_NAME AS EMP_TYPE_NAME, a.START_DATE_CONTRACT, a.END_DATE_CONTRACT, a.JOIN_DATE,
				a.STATUS AS STATUS_ID, CASE a.STATUS WHEN '1' THEN 'ACTIVE' WHEN '0' THEN 'RESIGN' ELSE '' END AS STATUS_NAME,
				a.GRADE,
				ISNULL(a.BPJS_KESEHATAN_NO,'-') BPJS_KESEHATAN_NO, a.BPJS_KESEHATAN_STATUS_ID, ISNULL(o.TYPE_NAME,'-') AS BPJS_KESEHATAN_STATUS_NAME,
				ISNULL(a.BPJS_KESEHATAN_FASKES_CODE,'-') BPJS_KESEHATAN_FASKES_CODE, ISNULL(p.NAME,'-') AS BPJS_KESEHATAN_FASKES_NAME,
				a.BPJS_KESEHATAN_START_DATE, a.BPJS_KESEHATAN_ATTACHMENT
		FROM	PFNATT.dbo.TBL_EMP a
				LEFT OUTER JOIN PFNATT.dbo.TBL_POSITION b ON a.POSITION_ID = b.POSITION_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_SECTION c ON a.SECT_ID = c.SECT_ID
				LEFT OUTER JOIN PFNATT.dbo.TBL_DEPT d ON c.DEPT_ID = d.DEPT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE e ON a.EMP_TYPE_ID = e.TYPE_ID AND e.TYPE = 'EmployeeType'
				LEFT OUTER JOIN PFNATT.dbo.TBL_LOCATION f ON a.LOC_ID = f.LOC_ID
				LEFT OUTER JOIN TBL_EMPLOYEE_GROUP g ON a.EMP_ID = g.EMP_ID
				LEFT OUTER JOIN TBL_EMP_GROUP h ON g.GROUP_ID = h.GROUP_ID
				LEFT OUTER JOIN TBL_MAR_STAT i ON a.MAR_STAT_ID = i.MAR_STAT_ID
				LEFT OUTER JOIN TBL_GENERAL_TYPE o ON a.BPJS_KESEHATAN_STATUS_ID = o.TYPE_ID AND o.TYPE = 'JamsostekType'
				LEFT OUTER JOIN TBL_FASKES p ON a.BPJS_KESEHATAN_FASKES_CODE = p.CODE
		where a.STATUS = 1 and (BPJS_KESEHATAN_NO is null or BPJS_KESEHATAN_NO = '' or BPJS_KESEHATAN_NO = '-' ) and (TEST1 is null or TEST1 <> '1')
	END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GetTOEIC]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_GetTOEIC]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.TOEIC_DATE DESC) AS NO, a.ID AS ID, a.EMPLOYEE_ID, a.TOEIC_SCORE, a.TOEIC_DATE, a.TOEIC_ATTACHMENT
	FROM	TBL_TOEIC a
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND a.IS_DELETED = 0
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTraining]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetTraining '2076'
CREATE PROCEDURE [dbo].[usp_GetTraining]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT	ROW_NUMBER() OVER(ORDER BY a.START_DATE DESC) AS NO, a.ID, a.EMPLOYEE_ID, a.SUBJECT, a.OBJECTIVE, a.PROVIDER, a.START_DATE, a.END_DATE, a.CURRENCY, a.AMOUNT, a.PLACE
	FROM	TBL_EMP_TRAINING a
	WHERE	a.EMPLOYEE_ID = @EmployeeId AND IS_DELETED = 0
	ORDER BY a.START_DATE DESC
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertActiveDirectory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_InsertMedical '2076'
CREATE PROCEDURE [dbo].[usp_InsertActiveDirectory]
(
	@FirstName varchar(100), 
	@LastName VARCHAR(100), 
	@DisplayName VARCHAR(200), 
	@Email VARCHAR(100), 
	@Description varchar(50), 
	@JobTitle VARCHAR(100), 
	@Department VARCHAR(100),
	@Path VARCHAR(500)
)
AS
BEGIN
	INSERT INTO [PFNATT].[dbo].[TBL_ACTIVE_DIRECTORY]
           ([FirstName]
           ,[LastName]
           ,[DisplayName]
           ,[Email]
           ,[Description]
           ,[JobTitle]
           ,[Department]
           ,[Path])
     VALUES
           (@FirstName
           ,@LastName
           ,@DisplayName
           ,@Email
           ,@Description
           ,@JobTitle
           ,@Department
           ,@Path)
END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertConfidential]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertConfidential '2076'
CREATE PROCEDURE [dbo].[usp_InsertConfidential]
(
	@Id INT, @EmployeeId VARCHAR(10), @TypeId VARCHAR(50), @TypeName VARCHAR(512), @DetailTypeId VARCHAR(50), @DetailTypeName VARCHAR(512), @ActivityDate DATETIME,@ConfidentialAttachment varchar(512),	@Remark VARCHAR(1024), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_CONFIDENTIAL(EMPLOYEE_ID, TYPE_ID, TYPE_NAME, DETAIL_TYPE_ID, DETAIL_TYPE_NAME, ACTIVITY_DATE,CONFIDENTIAL_ATTACHMENT, REMARK, UPDATED_BY)
		VALUES(@EmployeeId, @TypeId, @TypeName, @DetailTypeId, @DetailTypeName, @ActivityDate,@ConfidentialAttachment, @Remark, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_CONFIDENTIAL
		SET		TYPE_ID = @TypeId, TYPE_NAME = @TypeName, DETAIL_TYPE_ID = @DetailTypeId, DETAIL_TYPE_NAME = @DetailTypeName, ACTIVITY_DATE = @ActivityDate,CONFIDENTIAL_ATTACHMENT = @ConfidentialAttachment ,REMARK = @Remark, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCurrentCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertCurrentCourse '2076'
CREATE PROCEDURE [dbo].[usp_InsertCurrentCourse]
(
	@Id INT, @EmployeeId VARCHAR(10), @Subject VARCHAR(512), @Trainer VARCHAR(512), @Grade VARCHAR(512), @Provider VARCHAR(512), 
	@StartDate DateTime, @EndDate DateTime, @Remark VARCHAR(512), @Purpose VARCHAR(1024), @Amount INT, @Currency VARCHAR(10), @PlaceOfTraining VARCHAR(255),
	@Expense INT, @PaidOut INT, @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_COURSE_INT(EMP_ID, SUBJECT, TRAINER, GRADE, PROVIDER, C_FROM, C_TO, REMARKS, PURPOSE, AMOUNT, CURRENCY, PLACEOFTRAINING, EXPENSES, PAIDOUT, UPDATED_BY)
		VALUES(@EmployeeId, @Subject, @Trainer, @Grade, @Provider, @StartDate, @EndDate, @Remark, @Purpose, @Amount, @Currency, @PlaceOfTraining, @Expense, @PaidOut, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_COURSE_INT
		SET		SUBJECT = @Subject, TRAINER = @Trainer, GRADE = @Grade, PROVIDER = @Provider, C_FROM = @StartDate, C_TO = @EndDate, REMARKS = @Remark,
				PURPOSE = @Purpose, AMOUNT = @Amount, CURRENCY = @Currency, PLACEOFTRAINING = @PlaceOfTraining, EXPENSES = @Expense, PAIDOUT = @PaidOut,
				UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEducation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertEducation '2076'
CREATE PROCEDURE [dbo].[usp_InsertEducation]
(
	@Id INT, @EmployeeId VARCHAR(10), @StartYear DateTime, @EndYear DateTime, @Institute VARCHAR(512), @Majoring VARCHAR(512), @Location VARCHAR(512), 
	@EducationId VARCHAR(2), @Grade VARCHAR(4), @Remark VARCHAR(255), @TitleDegree VARCHAR(255), @UpdatedBy VARCHAR(10), @Attachment VARCHAR(512)
)
AS
BEGIN
	DECLARE @TitleDegreeTemp VARCHAR(512)
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EDUCATION(EMP_ID, S_YEAR, GRAD_YEAR, INSTITUTE, MAJORING, LOCATION, EDU_ID, GRADE, REMARKS, TITLE_DEGREE, UPDATED_BY, ATTACHMENT)
		VALUES(@EmployeeId, @StartYear, @EndYear, @Institute, @Majoring, @Location, @EducationId, @Grade, @Remark, @TitleDegree, @UpdatedBy, @Attachment)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EDUCATION
		SET		S_YEAR = @StartYear, GRAD_YEAR = @EndYear, INSTITUTE = @Institute, MAJORING = @Majoring, LOCATION = @Location, EDU_ID = @EducationId, GRADE = @Grade,
				REMARKS = @Remark, TITLE_DEGREE = @TitleDegree, ATTACHMENT = @Attachment, UPDATED_BY = @UpdatedBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
	
	SELECT	TOP 1 @TitleDegreeTemp = TITLE_DEGREE 
	FROM	TBL_EDUCATION
	WHERE	EMP_ID = @EmployeeId and IS_DELETED = 0
	ORDER	BY EDU_ID DESC
	
	UPDATE	TBL_EMP
	SET		LASTTITTLE = @TitleDegreeTemp
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEmployeeHistory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_InsertEmployeeHistory '2076'
CREATE PROCEDURE [dbo].[usp_InsertEmployeeHistory]
(
	@Id INT, @EmployeeId VARCHAR(10), @StartDate DateTime, @EndDate DateTime, @ActivityId VARCHAR(3), @LocationId VARCHAR(2), @SectionId VARCHAR(4), 
	@PositionId VARCHAR(3), @GradeId VARCHAR(2), @EmployeeTypeId VARCHAR(3), @SalaryGrade VARCHAR(10), @Detail VARCHAR(1024), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_HISTORY(EMP_ID, TR_DATE, TR_DATE1, ACT, LOC_ID, SECT_ID, POSITION_ID, GRADE_ID, EMP_TYPE_ID, SALARY_GRADE, DETAILS, UPDATED_BY)
		VALUES(@EmployeeId, @StartDate, @EndDate, @ActivityId, @LocationId, @SectionId, @PositionId, @GradeId, @EmployeeTypeId, @SalaryGrade, @Detail, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_HISTORY
		SET		TR_DATE = @StartDate, TR_DATE1 = @EndDate, ACT = @ActivityId, LOC_ID = @LocationId, SECT_ID = @SectionId, POSITION_ID = @PositionId, GRADE_ID = @GradeId,
				EMP_TYPE_ID = @EmployeeTypeId, SALARY_GRADE = @SalaryGrade, DETAILS = @Detail, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	NUMB = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEmployeeHistoryCareerpath]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_InsertEmployeeHistoryCareerpath '2076'
CREATE PROCEDURE [dbo].[usp_InsertEmployeeHistoryCareerpath]
(
	@Id INT, @EmployeeId VARCHAR(10), @ActivityId VARCHAR(10), @StartDate DateTime, @EndDate DateTime, @ColumnValue VARCHAR(512), @Attachment VARCHAR(512), @UpdateBy VARCHAR(10), @UpdatedDate DATETIME
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		IF @ActivityId = 'PMP'
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'POSITION_ID', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
		ELSE IF @ActivityId = 'PMG'
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'SALARY_GRADE', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
		ELSE IF @ActivityId = 'TNE'
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'SECT_ID', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
		ELSE IF @ActivityId = 'DIS'
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'DICIPLINE_STATUS', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
		ELSE IF @ActivityId = 'SLU'
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'SALARY_UPDATE', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
		ELSE
		BEGIN
			INSERT INTO TBL_EMP_HISTORY_CAREER_PATH(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
			VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'COLUMN_NAME', @ColumnValue, @Attachment, @UpdateBy, @UpdatedDate)
		END
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_HISTORY_CAREER_PATH
		SET		ACTIVITY_ID = @ActivityId, START_DATE = @StartDate, END_DATE = @EndDate, COLUMN_VALUE = @ColumnValue,
				ATTACHMENT = @Attachment, UPDATED_BY = @UpdateBy, UPDATED_DATE = @UpdatedDate, IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEmployeeHistoryRecruitment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_InsertEmployeeHistoryRecruitment]
(
	@Id INT, @EmployeeId VARCHAR(10), @ApplicationId VARCHAR(50)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_HISTORY_RECRUITMENT(EMPLOYEE_ID, APPLICATION_ID, ISDELETED)
		VALUES(@EmployeeId, @ApplicationId, 0)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_HISTORY_RECRUITMENT
		SET		APPLICATION_ID = @ApplicationId, ISDELETED = 0
		WHERE	ID = @Id
	END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEmployeeHistoryStatus]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_InsertEmployeeHistoryStatus '2076'
CREATE PROCEDURE [dbo].[usp_InsertEmployeeHistoryStatus]
(
	@Id INT, @EmployeeId VARCHAR(10), @ActivityId VARCHAR(10), @StartDate DateTime, @EndDate DateTime, @ColumnValueId VARCHAR(255), @ColumnValueName VARCHAR(512), @ColumnValueDescription VARCHAR(512), @Attachment VARCHAR(512), @UpdateBy VARCHAR(10), @UpdatedDate DATETIME
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_HISTORY_STATUS(EMPLOYEE_ID, ACTIVITY_ID, START_DATE, END_DATE, TABLE_NAME, COLUMN_NAME, COLUMN_VALUE_ID, COLUMN_VALUE_NAME, COLUMN_VALUE_DESCRIPTION, ATTACHMENT, UPDATED_BY, UPDATED_DATE)
		VALUES(@EmployeeId, @ActivityId, @StartDate, @EndDate, 'TBL_EMP', 'EMP_TYPE_ID', @ColumnValueId, @ColumnValueName, @ColumnValueDescription, @Attachment, @UpdateBy, @UpdatedDate)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_HISTORY_STATUS
		SET		ACTIVITY_ID = @ActivityId, START_DATE = @StartDate, END_DATE = @EndDate, COLUMN_VALUE_ID = @ColumnValueId, COLUMN_VALUE_NAME = @ColumnValueName, 
				COLUMN_VALUE_DESCRIPTION = @ColumnValueDescription, ATTACHMENT = @Attachment, UPDATED_BY = @UpdateBy, UPDATED_DATE = @UpdatedDate, IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEnglishAssessment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertEnglishAssessment '2076'
CREATE PROCEDURE [dbo].[usp_InsertEnglishAssessment]
(
	@Id INT, @EmployeeId VARCHAR(10), @EnglishAssessmentTypeId VARCHAR(50), @EnglishAssessmentTypeName VARCHAR(512), @EnglishAssessmentScore INT, @EnglishAssessmentDate DateTime, @EnglishAssessmentAttachment VARCHAR(512), @UpdatedBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_ENGLISH_ASSESSMENT(EMPLOYEE_ID, ENGLISH_ASSESSMENT_TYPE_ID, ENGLISH_ASSESSMENT_TYPE_NAME, ENGLISH_ASSESSMENT_SCORE, ENGLISH_ASSESSMENT_DATE, ENGLISH_ASSESSMENT_ATTACHMENT, UPDATED_BY)
		VALUES(@EmployeeId, @EnglishAssessmentTypeId, @EnglishAssessmentTypeName, @EnglishAssessmentScore, @EnglishAssessmentDate, @EnglishAssessmentAttachment, @UpdatedBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_ENGLISH_ASSESSMENT
		SET		ENGLISH_ASSESSMENT_TYPE_ID = @EnglishAssessmentTypeId, ENGLISH_ASSESSMENT_TYPE_NAME = @EnglishAssessmentTypeName, 
				ENGLISH_ASSESSMENT_SCORE = @EnglishAssessmentScore, ENGLISH_ASSESSMENT_DATE = @EnglishAssessmentDate, 
				ENGLISH_ASSESSMENT_ATTACHMENT = @EnglishAssessmentAttachment, UPDATED_BY = @UpdatedBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertFamily]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertFamily '2076'
CREATE PROCEDURE [dbo].[usp_InsertFamily]
(
	@Id INT, @EmployeeId VARCHAR(10), @RelationId VARCHAR(2), @Name VARCHAR(50), @Gender VARCHAR(1), @BirthDate DateTime, @BirthCertificateAttachment VARCHAR(512), @Occupation VARCHAR(20), 
	@Remark VARCHAR(20), @LifeStatusId VARCHAR(50), @LifeStatusName VARCHAR(512), @UpdateBy VARCHAR(10), @MedicalId INT, @MedicalCoveredId VARCHAR(50)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_FAMILY(EMP_ID, REL_ID, SNAME, GENDER, BIRTHDATE, BIRTH_CERTIFICATE_ATTACHMENT, OCCUPATION, REMARKS, LIFE_STATUS_ID, LIFE_STATUS_NAME, UPDATED_BY, UPDATED_DATE, MEDICAL_ID, MEDICAL_COVERED_ID)
		VALUES(@EmployeeId, @RelationId, @Name, @Gender, @BirthDate, @BirthCertificateAttachment, @Occupation, @Remark, @LifeStatusId, @LifeStatusName, @UpdateBy, GETDATE(), @MedicalId, @MedicalCoveredId)
	END
	ELSE
	BEGIN
		UPDATE	TBL_FAMILY
		SET		REL_ID = @RelationId, SNAME = @Name, GENDER = @Gender, BIRTHDATE = @BirthDate, BIRTH_CERTIFICATE_ATTACHMENT = @BirthCertificateAttachment, OCCUPATION = @Occupation, REMARKS = @Remark, 
				LIFE_STATUS_ID = @LifeStatusId, LIFE_STATUS_NAME = @LifeStatusName, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), MEDICAL_ID = @MedicalId, MEDICAL_COVERED_ID = @MedicalCoveredId, IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertFamilyDocumentAttachment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertFamilyDocumentAttachment '2076'
CREATE PROCEDURE [dbo].[usp_InsertFamilyDocumentAttachment]
(
	@Id INT, @EmployeeId VARCHAR(10), @FamilyDocumentTypeId VARCHAR(255), @FamilyDocumentAttachment VARCHAR(512), @FamilyDocumentRemark VARCHAR(1024), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_FAMILY_DOCUMENT_ATTACHMENT(EMPLOYEE_ID, FAMILY_DOCUMENT_TYPE_ID, FAMILY_DOCUMENT_ATTACHMENT, FAMILY_DOCUMENT_REMARK, UPDATED_BY, UPDATED_DATE)
		VALUES(@EmployeeId, @FamilyDocumentTypeId, @FamilyDocumentAttachment, @FamilyDocumentRemark, @UpdateBy, GETDATE())
	END
	ELSE
	BEGIN
		UPDATE	TBL_FAMILY_DOCUMENT_ATTACHMENT
		SET		FAMILY_DOCUMENT_TYPE_ID = @FamilyDocumentTypeId, FAMILY_DOCUMENT_ATTACHMENT = @FamilyDocumentAttachment, FAMILY_DOCUMENT_REMARK = @FamilyDocumentRemark,
				UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertGradeClassification]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertGradeClassification '2076'
CREATE PROCEDURE [dbo].[usp_InsertGradeClassification]
(
	@EmployeeId VARCHAR(10), @GradeClassification VARCHAR(10), @SalaryGrade VARCHAR(10), @DepartmentId VARCHAR(4)
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM TBL_EMP_GRADE_CLASS_MEMBER WHERE EMP_ID = @EmployeeId)
	BEGIN
		UPDATE TBL_EMP_GRADE_CLASS_MEMBER
		SET GRADECLASS_ID = @GradeClassification, GRADE_POS = @SalaryGrade, DEPT_ID = @DepartmentId
		WHERE EMP_ID = @EmployeeId
	END
	ELSE
	BEGIN
		INSERT INTO TBL_EMP_GRADE_CLASS_MEMBER(EMP_ID, GRADECLASS_ID, GRADE_POS, DEPT_ID)
		VALUES(@EmployeeId, @GradeClassification, @SalaryGrade, @DepartmentId)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertGroup]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertGroup '2076'
CREATE PROCEDURE [dbo].[usp_InsertGroup]
(
	@EmployeeId VARCHAR(10), @GroupId VARCHAR(10)
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM TBL_EMP_GROUP_MEMBER WHERE EMP_ID = @EmployeeId AND MONTHTR = MONTH(GETDATE()) AND YEARTR = YEAR(GETDATE()))
	BEGIN
		UPDATE TBL_EMP_GROUP_MEMBER
		SET GROUP_ID = @GroupId
		WHERE EMP_ID = @EmployeeId AND MONTHTR = MONTH(GETDATE()) AND YEARTR = YEAR(GETDATE())
	END
	ELSE
	BEGIN
		INSERT INTO TBL_EMP_GROUP_MEMBER(EMP_ID, GROUP_ID, MONTHTR, YEARTR, STATUS)
		VALUES(@EmployeeId, @GroupId, MONTH(GETDATE()), YEAR(GETDATE()), 1)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertLanguageSkill]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertLanguageSkill '2076'
CREATE PROCEDURE [dbo].[usp_InsertLanguageSkill]
(
	@Id INT, @EmployeeId VARCHAR(10), @LanguageId VARCHAR(50), @Language VARCHAR(50), @Classification VARCHAR(50), @SkillRead VARCHAR(50), @SkillWrite VARCHAR(50), @SkillSpeak VARCHAR(50), @UpdatedBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_LANGUAGE(EMP_ID, LANGUAGE_ID, LANGUAGE_, CLASSIFICATION, SKILL_READ, SKILL_WRITE, SKILL_SPEAK, UPDATED_BY)
		VALUES(@EmployeeId, @LanguageId, @Language, @Classification, @SkillRead, @SkillWrite, @SkillSpeak, @UpdatedBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_LANGUAGE
		SET		LANGUAGE_ID = @LanguageId, LANGUAGE_ = @Language, CLASSIFICATION = @Classification, SKILL_READ = @SkillRead, SKILL_WRITE = @SkillWrite, SKILL_SPEAK = @SkillSpeak, UPDATED_BY = @UpdatedBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	LANG_ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertLoan]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertLoan '2076'
CREATE PROCEDURE [dbo].[usp_InsertLoan]
(
	@Id INT, @EmployeeId VARCHAR(10), @BankName VARCHAR(512), @StartDate DateTime, @EndDate DateTime, 
	@Amount decimal(18,2), @Duration INT, @Installment int, @Payment decimal(18,2), 
	@PaidOffDate DateTime, @AsOfDate Datetime, @PaidOffAttachment VARCHAR(512), @UpdatedBy Varchar(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_LOAN(EMPLOYEE_ID, BANK_NAME, START_DATE, END_DATE,AMOUNT, DURATION, INSTALLMENT, PAYMENT, PAID_OFF_DATE, AS_OF_DATE, PAID_OFF_ATTACHMENT, UPDATED_BY)
		VALUES(@EmployeeId, @BankName, @StartDate, @EndDate,@Amount, @Duration,@Installment, @Payment, @PaidOffDate,@AsOfDate, @PaidOffAttachment, @UpdatedBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_LOAN
		SET		BANK_NAME = @BankName, START_DATE = @StartDate, END_DATE = @EndDate, AMOUNT = @Amount, DURATION = @Duration, INSTALLMENT = @Installment, PAYMENT = @Payment, 
				PAID_OFF_DATE = @PaidOffDate, AS_OF_DATE = @AsOfDate, PAID_OFF_ATTACHMENT = @PaidOffAttachment, UPDATED_BY = @UpdatedBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertLoanUpload]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertLoan '2076'
CREATE PROCEDURE [dbo].[usp_InsertLoanUpload]
(
	@EmployeeId VARCHAR(10), @BankName VARCHAR(512), @StartDate DateTime, @EndDate DateTime, 
	@Amount decimal(18,2), @Duration INT, @Installment int, @Payment decimal(18,2), 
	@PaidOffDate DateTime, @AsOfDate Datetime, @PaidOffAttachment VARCHAR(512), @UpdatedBy Varchar(10)
)
AS
BEGIN
	INSERT INTO TBL_LOAN(EMPLOYEE_ID, BANK_NAME, START_DATE, END_DATE,AMOUNT, DURATION, INSTALLMENT, PAYMENT, PAID_OFF_DATE, AS_OF_DATE, PAID_OFF_ATTACHMENT, UPDATED_BY)
	VALUES(@EmployeeId, @BankName, @StartDate, @EndDate,@Amount, @Duration,@Installment, @Payment, @PaidOffDate,@AsOfDate, @PaidOffAttachment, @UpdatedBy)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertMedical]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertMedical '2076'
CREATE PROCEDURE [dbo].[usp_InsertMedical]
(
	@Id INT, @EmployeeId VARCHAR(10), 
	@Type_Id VARCHAR(50), @Type_Name varchar(512),
	@Detail_Id VARCHAR(50), @Detail_Name varchar (512),
	@MedicalDate DateTime, @Reference VARCHAR(512), 
	@Attachment varchar(512), @Remarks VARCHAR(1024), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_MEDICAL(EMP_ID, TYPE_ID, TYPE_NAME, DETAIL_ID, DETAIL_NAME, 
		MEDICAL_DATE, REFFERENCE, ATTACHMENT, REMARKS, UPDATED_BY, UPDATED_DATE)
		VALUES(@EmployeeId, @Type_Id, @Type_Name, @Detail_Id, @Detail_Name, @MedicalDate, @Reference, @Attachment, @Remarks, @UpdateBy, GETDATE())
	END
	ELSE
	BEGIN
		UPDATE	TBL_MEDICAL
		SET		TYPE_ID = @Type_Id, 
				TYPE_NAME = @Type_Name, 
				DETAIL_ID= @Detail_Id, 
				DETAIL_NAME = @Detail_Name, 
				MEDICAL_DATE = @MedicalDate, 
				REFFERENCE = @Reference, 
				ATTACHMENT = @Attachment,
				REMARKS = @Remarks,
				UPDATED_BY = @UpdateBy,
				UPDATED_DATE = GETDATE(),
				IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNewEmployee]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertNewEmployee '2076'
CREATE PROCEDURE [dbo].[usp_InsertNewEmployee]
(
	@EmployeeId VARCHAR(10), @EmployeeType VARCHAR(50), @CreatedBy VARCHAR(10)
)
AS
BEGIN
	DECLARE @MaxRefId INT
	IF EXISTS(SELECT 1 FROM TBL_EMP WHERE EMP_ID = @EmployeeId)
	BEGIN
		SELECT 0 AS EMPLOYEE_STATUS, 'Employee ID Already Exists' AS EMPLOYEE_MESSAGE
	END
	ELSE
	BEGIN
		SELECT @MaxRefId = MAX(a.REF_ID) + 1 FROM TBL_EMP a
		INSERT INTO TBL_EMP(EMP_ID, EMP_TYPE_ID, REF_ID, CREATION_BY)
		VALUES(@EmployeeId, @EmployeeType, @MaxRefId, @CreatedBy)
		SELECT  1 AS EMPLOYEE_STATUS, 'Insert Employee ID Success' AS EMPLOYEE_MESSAGE
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPreviousCompany]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertPreviousCompany '2076'
CREATE PROCEDURE [dbo].[usp_InsertPreviousCompany]
(
	@Id INT, @EmployeeId VARCHAR(10), @Company VARCHAR(50), @Description VARCHAR(50), @Preference VARCHAR(50), @Position VARCHAR(50), @StartDate DateTime, 
	@EndDate DateTime, @LastSalary Decimal(18, 0), @JobDescription VARCHAR(50), @ReasonLeaving VARCHAR(50), @Remark VARCHAR(50), @Currency VARCHAR(10), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_JOB_EX(EMP_ID, COMPANY, COMPANY_DESC, REFFERENSI, POS, STARTDATE, ENDDATE, LASTSALARY, JOBDESC, RLEAVING, REMARKS, CURRENCY, UPDATED_BY)
		VALUES(@EmployeeId, @Company, @Description, @Preference, @Position, @StartDate, @EndDate, @LastSalary, @JobDescription, @ReasonLeaving, @Remark, @Currency, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_JOB_EX
		SET		COMPANY = @Company, COMPANY_DESC = @Description, REFFERENSI = @Preference, POS = @Position, STARTDATE = @StartDate, ENDDATE = @EndDate, LASTSALARY = @LastSalary,
				JOBDESC = @JobDescription, RLEAVING = @ReasonLeaving, REMARKS = @Remark, CURRENCY = @Currency, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPreviousCourse]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertPreviousCourse '2076'
CREATE PROCEDURE [dbo].[usp_InsertPreviousCourse]
(
	@Id INT, @EmployeeId VARCHAR(10), @Subject VARCHAR(255), @Trainer VARCHAR(255), @Grade VARCHAR(10), @Provider VARCHAR(255), @StartDate DateTime, 
	@EndDate DateTime, @Remark VARCHAR(255), @Purpose VARCHAR(1024), @Amount INT, @Currency VARCHAR(10), @PlaceOfTraining VARCHAR(255), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_COURSE(EMP_ID, SUBJECT, TRAINER, GRADE, PROVIDER, C_FROM, C_TO, REMARKS, PURPOSE, AMOUNT, CURRENCY, PLACEOFTRAINING, UPDATED_BY)
		VALUES(@EmployeeId, @Subject, @Trainer, @Grade, @Provider, @StartDate, @EndDate, @Remark, @Purpose, @Amount, @Currency, @PlaceOfTraining, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_COURSE
		SET		SUBJECT = @Subject, TRAINER = @Trainer, GRADE = @Grade, PROVIDER = @Provider, C_FROM = @StartDate, C_TO = @EndDate, REMARKS = @Remark,
				PURPOSE = @Purpose, AMOUNT = @Amount, CURRENCY = @Currency, PLACEOFTRAINING = @PlaceOfTraining, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPreviousProject]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertPreviousProject '2076'
CREATE PROCEDURE [dbo].[usp_InsertPreviousProject]
(
	@Id INT, @EmployeeId VARCHAR(10), @StartDate DateTime, @EndDate DateTime, @ProjectType VARCHAR(50), @ProjectName VARCHAR(50), @Location VARCHAR(50), 
	@ProjectOwner VARCHAR(50), @ProjectValue VARCHAR(50), @CompanyName VARCHAR(50), @BusinessLine VARCHAR(50), @Position VARCHAR(50), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_PROJECT_PREV(EMP_ID, Startdate, Enddate, ProjectType, ProjectName, Location, ProjectOwner, ProjectValue, CompanyName, BL_ID, Pos_ID, UPDATED_BY)
		VALUES(@EmployeeId, @StartDate, @EndDate, @ProjectType, @ProjectName, @Location, @ProjectOwner, @ProjectValue, @CompanyName, @BusinessLine, @Position, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_PROJECT_PREV
		SET		Startdate = @StartDate, Enddate = @EndDate, ProjectType = @ProjectType, ProjectName = @ProjectName, Location = @Location, ProjectOwner = @ProjectOwner, ProjectValue = @ProjectValue,
				CompanyName = @CompanyName, BL_ID = @BusinessLine, Pos_ID = @Position, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	TR_NUMBER = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertTOEIC]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertTOEIC '2076'
CREATE PROCEDURE [dbo].[usp_InsertTOEIC]
(
	@Id INT, @EmployeeId VARCHAR(10), @TOEICScore INT, @TOEICDate DateTime, @TOEICAttachment VARCHAR(512), @UpdatedBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_TOEIC(EMPLOYEE_ID, TOEIC_SCORE, TOEIC_DATE, TOEIC_ATTACHMENT, UPDATED_BY)
		VALUES(@EmployeeId, @TOEICScore, @TOEICDate, @TOEICAttachment, @UpdatedBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_TOEIC
		SET		TOEIC_SCORE = @TOEICScore, TOEIC_DATE = @TOEICDate, TOEIC_ATTACHMENT = @TOEICAttachment, UPDATED_BY = @UpdatedBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertTraining]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_InsertTraining '2076'
CREATE PROCEDURE [dbo].[usp_InsertTraining]
(
	@Id INT, @EmployeeId VARCHAR(10), @Subject VARCHAR(1024), @Objective VARCHAR(8000), @Provider VARCHAR(512),
	@StartDate DateTime, @EndDate DateTime, @Currency VARCHAR(10), @Amount DECIMAL(18, 2), @Place VARCHAR(512), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	IF @Id = 0
	BEGIN
		INSERT INTO TBL_EMP_TRAINING(EMPLOYEE_ID, SUBJECT, OBJECTIVE, PROVIDER, START_DATE, END_DATE, CURRENCY, AMOUNT, PLACE, UPDATED_BY)
		VALUES(@EmployeeId, @Subject, @Objective, @Provider, @StartDate, @EndDate, @Currency, @Amount, @Place, @UpdateBy)
	END
	ELSE
	BEGIN
		UPDATE	TBL_EMP_TRAINING
		SET		SUBJECT = @Subject, OBJECTIVE = @Objective, PROVIDER = @Provider, START_DATE = @StartDate, END_DATE = @EndDate, 
				CURRENCY = @Currency, AMOUNT = @Amount, PLACE = @Place, UPDATED_BY = @UpdateBy, UPDATED_DATE = GETDATE(), IS_DELETED = 0
		WHERE	ID = @Id
	END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_IsAdminOvertime]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_GetEducation '2076'
CREATE PROCEDURE [dbo].[usp_IsAdminOvertime]
(
	@EmployeeId VARCHAR(10)
)
AS
BEGIN
	SELECT COUNT(1) from MICADEBUG.dbo.TBL_USER_MS where GROUP_ID = 'G000' and [USER_ID] = @EmployeeId
END

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetAdd]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from tbl_timesheet where emp_id = '0623' order by tr_date desc
CREATE PROCEDURE [dbo].[usp_TimesheetAdd]
(
@TR_DATE datetime, 
@TIME_IN varchar(5), 
@TIME_OUT varchar(5), 
@EMP_ID varchar(4), 
@USER_ID varchar(4), 
@JOBCODE varchar(100), 
@REMARKS varchar(500),
@FWBS6 varchar(10),
@Plant varchar(2),
@Unit Int
)
AS
--Created By Ary, modif by Jeremy
--EXEC stp_InsManHour '12/9/2009','08:00','11:00','9032','0858','0-0544-00-0000|G-2XXX-00-0544|N920|E100136',''
DECLARE @MESSAGE varchar(100)
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @CAT_ID VARCHAR(10)
DECLARE @FWBS VARCHAR(10)
DECLARE @LOC_ID VARCHAR(2)
DECLARE @TIMEINLIMIT DATETIME
DECLARE @TIMEINSTART DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TIMESPENT INTEGER
DECLARE @STARTDATE DATETIME
DECLARE @ENDDATE DATETIME
DECLARE @POSDATE DATETIME
DECLARE @DAYSINMONTH INTEGER
DECLARE @KNUMBER VARCHAR(6)-- HQ APR2013

SET @MESSAGE = ''

IF  EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_CLOSE_PERIOD_TIMESHEET 
			WHERE MONTHTR = MONTH(@TR_DATE) 
			AND YEARTR = YEAR(@TR_DATE) 
			AND CONVERT(VARCHAR(12),CLOSING_DATE,101) <= CONVERT(VARCHAR(12),GETDATE(),101)) 
			AND @USER_ID <> '1375'
BEGIN
 	SET @MESSAGE = 'Period Timesheet already close !'
END
ELSE
BEGIN
	
	IF CHARINDEX('|',@JOBCODE) > 0
	BEGIN
		SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
		SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
	END
	
	IF CHARINDEX('|',@JOBCODE) > 0
	BEGIN
		SET @CLIENT = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
		SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
	END
	
	IF CHARINDEX('|',@JOBCODE) > 0
	BEGIN
		SET @SECT_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
		SET @CAT_ID = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
		SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))
	END
	
	IF RTRIM(LTRIM(@CLIENT)) = '' AND EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL 
												WHERE JOBCODE_ID = @JOB_ID AND CATEGORY_ID = @CAT_ID)
		SELECT @CLIENT = CLIENT_JOBCODE FROM  TBL_PROJECT_GENERAL WHERE JOBCODE_ID = @JOB_ID AND CATEGORY_ID = @CAT_ID


	SET @TIMEIN = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_IN AS DATETIME)
	SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_OUT AS DATETIME)
	
	SET @CAT_ID = ISNULL(@CAT_ID,'')
	
	IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL 
				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
	BEGIN
		SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
	END
	ELSE
	BEGIN
		IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
		BEGIN
			SET @MESSAGE = 'Please input remarks for this jobcode !'
		END
		ELSE
		BEGIN
			IF @JOB_ID = 'N-0000-00-000'
			BEGIN
				exec MICADEBUG.dbo.stp_instimesheet_leave @EMP_ID, @TR_DATE, @USER_ID
			END
			ELSE
			BEGIN
				SELECT @LOC_ID = LOC_ID FROM TBL_EMP WHERE EMP_ID = @EMP_ID
				SET @TIMEINLIMIT = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'IN')
				SET @TIMEINSTART = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'EX')
				SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
				SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
				--LEAVE AUTOMATICALLY INSERTED - ADDED BY ARY - 16 NOV 2010
				IF DAY(@TR_DATE) BETWEEN 1 AND 10
				BEGIN
					SET @STARTDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/1/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
					SET @ENDDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/10/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
					--DECLARE CC CURSOR FOR SELECT EMP_ID, TR_DATE FROM TBL_CARD_TR 
					--	WHERE TR_DATE BETWEEN @STARTDATE AND @ENDDATE AND ATT_TYPE_ID IN ('AL','UL','AB','SI','SL','AD','HL','OL')
				END
				IF DAY(@TR_DATE) BETWEEN 11 AND 20
				BEGIN
					SET @STARTDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/11/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
					SET @ENDDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/20/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
				END
				IF DAY(@TR_DATE) BETWEEN 21 AND 31
				BEGIN
					SET @STARTDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/1/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
					SET @DAYSINMONTH = DAY(DATEADD(DAY,-1,DATEADD(MONTH,1,@STARTDATE)))
					SET @STARTDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/21/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
					SET @ENDDATE = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) AS DATETIME)
				END
				SET @POSDATE = @STARTDATE
				WHILE @POSDATE <= @ENDDATE
				BEGIN	
					IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_CARD_TR 
								WHERE TR_DATE = @POSDATE 
								AND EMP_ID = @EMP_ID 
								AND ISNULL(ATT_TYPE_ID,'') IN ('AL','UL','HL','OL','SL','SI','AD','AB'))
						AND CONVERT(VARCHAR(12),@POSDATE,101) <> CONVERT(VARCHAR(12),GETDATE(),101)
							exec MICADEBUG.dbo.stp_instimesheet_leave @EMP_ID, @POSDATE, @USER_ID
					SET @POSDATE = DATEADD(DAY,1,@POSDATE)
				END

				IF @TIMEIN < @TIMEINSTART SET @TIME_IN = @TIMEINSTART
				IF @TIMEIN > @TIMEINLIMIT AND EXISTS (SELECT * 
														FROM MICADEBUG.dbo.TBL_CARD_TR 
														WHERE EMP_ID = @EMP_ID 
														AND TR_DATE = @TR_DATE 
														AND CL = 0 
														AND ATT_TYPE_ID = 'EL') 
					SET @TIMEIN = @TIMEINLIMIT
					
				IF ABS(DATEDIFF(MINUTE,@TIMEIN,@TIMEOUT)) > 0
				BEGIN
					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
					AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

					IF LTRIM(RTRIM(@LOC_ID)) = '23'  
					BEGIN	
						IF CONVERT(VARCHAR(5),DATEADD(MINUTE,30,@TIMEOUT),108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,-30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),DATEADD(MINUTE,60,@TIMEOUT),108) > '20:00' OR (@TIMEOUT > @TIMEIN AND CONVERT(VARCHAR(12),@TIMEOUT,101) > CONVERT(VARCHAR(12),@TIMEIN,101))	
							SET @TIMEOUT = DATEADD(MINUTE,-60,@TIMEOUT)
					END
					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)
					print @TIMESPENT
					SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_OUT AS DATETIME)
					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
						AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)
			      SET @KNUMBER = (SELECT MICADEBUG.dbo.Funct_GetIDEASKNumber(@EMP_ID)) -- HQ APR 2013
            
		 				INSERT INTO PFNATT.dbo.TBL_TIMESHEET_DE
						(	EMP_ID,
							CLIENT_JOBCODE,
							JOBCODE_ID,
							PROJECT_SECT_ID,
							CATEGORY_ID,
							FWBS,
							TR_DATE,
							TIME_IN,
							TIME_OUT,
							APPROVE,
							APPROVE1,
							APPROVE2,
							APPROVE3,
							REMARKS,
							TIMESPENT,
							LOC_ID,
	 						MODIFY_BY,
	 						LAST_MODIFIED,
							REMARKS_JOBCODE,
							KNUMBER,
							FWBS6,
							Plant,
							Unit
						)
						VALUES
						(
							@EMP_ID,
							@CLIENT,
							@JOB_ID,
							@SECT_ID,
							@CAT_ID,
							@FWBS,
							@TR_DATE,
							@TIMEIN ,
							@TIMEOUT ,
							0 ,
							0 ,
							0 ,
							0 ,
							'',
							@TIMESPENT,
							@LOC_ID,
							@USER_ID,
							GETDATE(),
							ISNULL(@REMARKS,''),
							@KNUMBER,
							@FWBS6,
							@Plant,
							@Unit
						)
					END
				END
			END
	END
	END
select @MESSAGE as Message
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetCalculate]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetCalculate]
(
@MONTH INT,
@YEAR INT,
@PERIOD CHAR(10),
@EMP_ID CHAR(4)
)
AS
DECLARE @CHECK INTEGER
DECLARE @ATT INTEGER
DECLARE @TIMESHEET INTEGER
DECLARE @COUNT AS INTEGER
DECLARE @APPROVE AS INTEGER

IF LTRIM(RTRIM(@PERIOD)) = 'ALL' OR LTRIM(RTRIM(@PERIOD)) = '0' 
BEGIN
	SELECT @ATT = SUM((REG+OT+HOL)-(EF+CL+PERMISSION)) FROM MICADEBUG.dbo.V_TIMEATTENDANCE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = RTRIM(LTRIM(@EMP_ID))
	SELECT @TIMESHEET = SUM(TIMESPENT) FROM PFNATT.dbo.TBL_TIMESHEET_DE  
		WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND JOBCODE_ID NOT LIKE 'N-%'
	SELECT @COUNT = COUNT(*) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID 
	SELECT @APPROVE = SUM(CAST(APPROVE3 AS INTEGER)) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID 
END

IF LTRIM(RTRIM(@PERIOD)) = '1' 
BEGIN
	SELECT @ATT = SUM((REG+OT+HOL)-(EF+CL+PERMISSION)) FROM MICADEBUG.dbo.V_TIMEATTENDANCE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = RTRIM(LTRIM(@EMP_ID)) AND DAY(TR_DATE) BETWEEN 1 AND 10
-- 	SELECT @TIMESHEET = SUM(MH_DAY) FROM TBL_MH_DAY_SPENT A INNER JOIN TBL_TIMESHEET B ON
-- 		A.JOBCODE_ID = B.JOBCODE_ID AND A.CLIENT_JOBCODE = B.CLIENT_JOBCODE AND A.PROJECT_SECT_ID = B.PROJECT_SECT_ID AND A.EMP_ID = B.EMP_ID AND A.DATE_TR = B.TR_DATE
-- 		AND A.CATEGORY_ID = B.CATEGORY_ID
-- 		WHERE MONTH(DATE_TR) = @MONTH AND YEAR(DATE_TR) = @YEAR AND A.EMP_ID = @EMP_ID AND DAY(DATE_TR) BETWEEN 1 AND 10
	SELECT @TIMESHEET = SUM(TIMESPENT) FROM PFNATT.dbo.TBL_TIMESHEET_DE  
		WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 1 AND 10 AND JOBCODE_ID NOT LIKE 'N-%'
	SELECT @COUNT = COUNT(*) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 1 AND 10
	SELECT @APPROVE = SUM(CAST(APPROVE3 AS INTEGER)) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 1 AND 10
END

IF LTRIM(RTRIM(@PERIOD)) = '2' 
BEGIN
	SELECT @ATT = SUM((REG+OT+HOL)-(EF+CL+PERMISSION)) FROM MICADEBUG.dbo.V_TIMEATTENDANCE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = RTRIM(LTRIM(@EMP_ID)) AND DAY(TR_DATE) BETWEEN 11 AND 20 
	SELECT @TIMESHEET = SUM(TIMESPENT) FROM PFNATT.dbo.TBL_TIMESHEET_DE  
		WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 11 AND 20 AND JOBCODE_ID NOT LIKE 'N-%'
	SELECT @COUNT = COUNT(*) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 11 AND 20
	SELECT @APPROVE = SUM(CAST(APPROVE3 AS INTEGER)) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 11 AND 20
END

IF LTRIM(RTRIM(@PERIOD)) = '3' 
BEGIN
	SELECT @ATT = SUM((REG+OT+HOL)-(EF+CL+PERMISSION)) FROM MICADEBUG.dbo.V_TIMEATTENDANCE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = RTRIM(LTRIM(@EMP_ID)) AND DAY(TR_DATE) BETWEEN 21 AND 31
		SELECT @TIMESHEET = SUM(TIMESPENT) FROM PFNATT.dbo.TBL_TIMESHEET_DE
		WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 21 AND 31 AND JOBCODE_ID NOT LIKE 'N-%'
	SELECT @COUNT = COUNT(*) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 21 AND 31
	SELECT @APPROVE = SUM(CAST(APPROVE3 AS INTEGER)) FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE MONTH(TR_DATE) = @MONTH AND YEAR(TR_DATE) = @YEAR AND EMP_ID = @EMP_ID  AND DAY(TR_DATE) BETWEEN 21	AND 31

END
SET @COUNT = ISNULL(@COUNT,0)
SET @APPROVE = ISNULL(@APPROVE,0)
IF @COUNT = @APPROVE AND @COUNT > 0 
	SET @CHECK = 1
ELSE
	SET @CHECK = 0
SET @ATT = ISNULL(@ATT,0)
SET @TIMESHEET = ISNULL(@TIMESHEET,0)

select	@CHECK as [CHECK], 
		@ATT as ATT, 
		@TIMESHEET as TIMESHEET

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetCategory]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[usp_TimesheetCategory]
(
@dlJob varchar(500),
@Emp_Id varchar(4)
)
AS

SELECT DISTINCT CATEGORY_ID, 
		SUBSTRING(CATEGORY_ID,5,4) + '|' + LTRIM(RTRIM(SUBSTRING(SECT_NAME,CHARINDEX('-',SECT_NAME)+1,6))) + '|' + LTRIM(RTRIM(CATEGORY_NAME)) AS CATEGORY_NAME 
FROM MICADEBUG.dbo.V1_LIST_TIMESHEET_JOBCODE 
WHERE LTRIM(RTRIM(JOBCODE_ID))+'|'+LTRIM(RTRIM(CLIENT_JOBCODE)) = @dlJob
AND EMP_ID = @Emp_Id
UNION ALL
SELECT CATEGORY_ID, 
		SUBSTRING(CATEGORY_ID,5,4) + '|' + DESCRIPTION AS CATEGORY_NAME 
FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL
WHERE LTRIM(RTRIM(JOBCODE_ID))+'|'+LTRIM(RTRIM(CLIENT_JOBCODE)) = @dlJob
AND DEPT IN (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @Emp_Id)
UNION ALL 
SELECT '' AS CATEGORY_ID, '-- Not Specified --' AS CATEGORY_NAME 
ORDER BY CATEGORY_ID ASC
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetDelete]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_TimesheetDelete]
(
@TR_DATE DATETIME,
@EMP_ID VARCHAR(4),
@JOBCODE VARCHAR(100)
)
AS
--Created By Ary

DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @CAT_ID VARCHAR(10)
DECLARE @LOC_ID VARCHAR(2)
DECLARE @CLIENT_OLD VARCHAR(20)
DECLARE @JOB_ID_OLD VARCHAR(20)
DECLARE @SECT_ID_OLD VARCHAR(4)
DECLARE @TIMEINLIMIT DATETIME
DECLARE @TIMEINSTART DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @MESSAGE VARCHAR(20)

SET @MESSAGE = ''

IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @CLIENT = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @SECT_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CAT_ID = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END

IF EXISTS (SELECT * FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID)
BEGIN
	DELETE FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID
	--DELETE FROM MICADEBUG.dbo.TBL_MH_DAY_SPENT WHERE DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID AND JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID
	SET @MESSAGE = 'Request Timesheet Deleted !'
END

select @MESSAGE as Message
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetDetail]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetDetail]
(
@MONTH INT,
@YEAR INT,
@PERIOD CHAR(10),
@EMP_ID CHAR(4),
@TYPE CHAR(3)
)
AS

DECLARE @SQLSTRING AS VARCHAR(5000)

DECLARE @TempTable TABLE
(
EMP_ID varchar(4),
TOTAL integer,
TR_DATE datetime,
TIME_IN datetime,
TIME_OUT datetime,
TOTAL_SPENT integer,
JOBCODE_ID varchar(500),
PROJECT_NAME varchar(500),
CLIENT_JOBCODE varchar(500),
DAYORDER integer,
VISIBLE varchar(20),
GANJIL integer,
CAT_ID varchar(100),
UNBALANCE_HOUR integer,
TOTALMHDAY integer,
APPROVE3 integer,
REMARKS_JOBCODE varchar(500),
FWBS6 varchar(10),
Plant varchar(2),
Unit int
)

SET DATEFIRST  1 

SET @SQLSTRING = 'SELECT DISTINCT B.EMP_ID, ISNULL((REG+OT+HOL-(CL+EF+PERMISSION)),0) AS TOTAL ,  B.DATE_TR AS TR_DATE, B.TIME_IN, B.TIME_OUT, ISNULL(B.TOTAL_SPENT,0) AS TOTAL_SPENT,'
SET @SQLSTRING = @SQLSTRING + ' RTRIM(LTRIM(JOBCODE_ID))+''|''+RTRIM(LTRIM(CLIENT_JOBCODE))+''|''+RTRIM(LTRIM(PROJECT_SECT_ID))+''|''+RTRIM(LTRIM(CATEGORY_ID)) AS JOBCODE_ID, ISNULL(LTRIM(RTRIM(B.PROJECT_NAME)),'''') AS PROJECT_NAME,ISNULL(B.CLIENT_JOBCODE,'''') CLIENT_JOBCODE, '
SET @SQLSTRING = @SQLSTRING + ' MICADEBUG.dbo.FUNCT_CHECKHOLIDAY(B.DATE_TR,A.LOC_ID) AS DAYORDER, ''TRUE'' AS VISIBLE, 1 AS GANJIL, SUBSTRING(CATEGORY_ID,5,5) AS CAT_ID, '
SET @SQLSTRING = @SQLSTRING + ' ISNULL((REG+OT+HOL-(CL+EF+PERMISSION)),0)-MICADEBUG.dbo.FUNCT_TOTALMHSPENT(B.DATE_TR,B.EMP_ID) AS UNBALANCE_HOUR, MICADEBUG.dbo.FUNCT_TOTALMHSPENT(B.DATE_TR,B.EMP_ID) AS TOTALMHDAY, CAST(ISNULL(APPROVE3,0) AS INTEGER) AS APPROVE3, REMARKS_JOBCODE, FWBS6, PLANT, UNIT '
SET @SQLSTRING = @SQLSTRING + ' FROM MICADEBUG.dbo.V_TIMEATTENDANCE A RIGHT JOIN PFNATT.dbo.V_DE_MHSPENTPERDATE B ON A.EMP_ID = B.EMP_ID AND A.TR_DATE = B.DATE_TR '
SET @SQLSTRING = @SQLSTRING + ' WHERE (B.EMP_ID = ''' + @EMP_ID + ''' AND MONTH(B.DATE_TR) = ' + CAST(@MONTH AS VARCHAR) + ' AND YEAR(B.DATE_TR) = ' + CAST(@YEAR AS VARCHAR) 
IF LTRIM(RTRIM(@PERIOD)) = '1' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 1 AND 10 '
IF LTRIM(RTRIM(@PERIOD)) = '2' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 11 AND 20 '
IF LTRIM(RTRIM(@PERIOD)) = '3' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 21 AND 31 '
SET @SQLSTRING = @SQLSTRING + ' AND ISNULL(B.JOBCODE_ID,'''') <> '''') ORDER BY B.DATE_TR, TIME_IN'
print @SQLSTRING
INSERT INTO @TempTable
EXEC (@SQLSTRING)
SELECT 
ROW_NUMBER() over(order by TR_DATE asc) as No, *
FROM @TempTable


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetFWBS6]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[usp_TimesheetFWBS6]
(
@dlJob varchar(500),
@EMP_ID varchar(4)
)
AS

IF CHARINDEX('|',@dlJob) > 0
BEGIN
	set @dlJob = LEFT(@dlJob,CHARINDEX('|',@dlJob)-1)
END

--select distinct a.JOBCODE_ID, c.CATEGORY_ID, c.CATEGORY_NAME, c.FWBS, c.FWBS as FWBS_TEXT
--from  MICADEBUG.dbo.TBL_MH_PLAN a
--inner join MICADEBUG.dbo.TBL_CATEGORY c on c.CATEGORY_ID = a.CATEGORY_ID
--INNER JOIN TBL_SECTION d ON a.SECT_ID = d.SECT_ID
--where a.JOBCODE_ID = @dlJob
--and a.EMP_ID = @EMP_ID
--union all
--select distinct a.JOBCODE_ID, c.CATEGORY_ID, c.CATEGORY_NAME, c.FWBS, c.FWBS as FWBS_TEXT
--from MICADEBUG.dbo.TBL_PROJECT_GENERAL a
--inner join MICADEBUG.dbo.TBL_CATEGORY c on a.DEPT = c.SECT_ID
--where a.TRAINING = 0
--and a.JOBCODE_ID = @dlJob
SELECT '0' AS JOBCODE_ID, '0' as CATEGORY_ID, '0' as CATEGORY_NAME, CATEGORY_ID as FWBS, CATEGORY_ID+'-'+ CATEGORY_NAME AS FWBS_TEXT
from PFNATT.dbo.TBL_FWBS
union all
SELECT '' AS JOBCODE_ID, '' as CATEGORY_ID, '' as CATEGORY_NAME, '' as FWBS, '-- Not Specified --' AS FWBS_TEXT
ORDER BY JOBCODE_ID ASC
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGenerateTemplate_Rem]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--stp_InsertTimesheetTemplate_Rem

CREATE PROCEDURE [dbo].[usp_TimesheetGenerateTemplate_Rem]
(
@EMP_ID varchar(4), 
@USER_ID varchar(4), 
@PERIOD varchar(3), 
@MONTH int, 
@YEAR int, 
@JOBCODE varchar(100), 
@LOC_ID varchar(10), 
@CAT_ID varchar(10), 
@DAYSINMONTH int, 
@REMARKS varchar(500), 
@FWBS6 varchar(10),
@PLANT varchar(2),
@UNIT int
)
AS
--Created By Ary
--EXEC sp_GenerateTimesheetTemplate '0883','1','3','2006','0-0486-05-0000|7-1836-10-0000','00','E210120',31
DECLARE @MESSAGE varchar(100)
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @FWBS VARCHAR(10)
DECLARE @TIME_START DATETIME
DECLARE @TIME_END DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TR_DATE DATETIME
DECLARE @TIMESPENT AS INT
DECLARE @IA INTEGER
DECLARE @KNUMBER VARCHAR(6)-- HQ APR2013

IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CLIENT = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
SET @SECT_ID = LEFT(@CAT_ID,4)

-- SELECT @CAT_ID = CATEGORY_ID FROM TBL_PROJECT_FWBS WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID
-- AND EXISTS (SELECT * FROM TBL_MH_PLAN WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID)
SET @MESSAGE = ''

 -- HQ APRIL 2013
 SET @KNUMBER = 'NONE'
 SET @KNUMBER = (SELECT MICADEBUG.dbo.Funct_GetIDEASKNumber(@EMP_ID)) 
 -- HQ APR 2013
 
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
BEGIN
	SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
END
ELSE
BEGIN
	IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
	BEGIN
		SET @MESSAGE = 'Please input remarks for this jobcode !'
	END
	ELSE
	BEGIN

		SET @CAT_ID = ISNULL(@CAT_ID,'')
		SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))

		IF @PERIOD = '1'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/10/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '2'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/11/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/20/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '3'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/21/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = 'ALL'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		
		print @PERIOD
		
		SET @TR_DATE = @TIME_START
		WHILE @TR_DATE <= @TIME_END
		BEGIN
			print @TR_DATE
			
			IF EXISTS (SELECT * 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE)
			BEGIN
				
				IF EXISTS (SELECT * 
								FROM MICADEBUG.dbo.TBL_CARD_TR 
								WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE AND LOC_ID = '00')
				BEGIN
					SELECT @TIMEIN = CARDR_IN,
							@TIMEOUT = CARDR_OUT 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID
				END
				ELSE
				BEGIN
					SELECT @TIMEIN = CARD_IN 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID
						
					SELECT @TIMEOUT = CARD_OUT 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID
				END
				SELECT @LOC_ID = LOC_ID 
					FROM MICADEBUG.dbo.TBL_CARD_TR 
					WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID
				
				SELECT @TIMESPENT = (REG+ISNULL(OT,0)+ISNULL(HOL,0))-(ISNULL(PERMISSION,0)+ISNULL(CL,0)+ISNULL(EF,0)) 
					FROM MICADEBUG.dbo.TBL_CARD_TR 
					WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE
					
				IF @TIMEIN IS NULL
				BEGIN
					SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'IN')
					SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
					SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
					SET @TIMEOUT = DATEADD(MINUTE,@TIMESPENT+(DATEDIFF(MINUTE,@REST_START,@REST_END)),@TIMEIN)
				END
			END
			SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
			SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
			IF EXISTS (SELECT * 
							FROM MICADEBUG.dbo.TBL_CARD_TR 
							WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID 
							AND ISNULL(ATT_TYPE_ID,'') NOT IN ('AB','AL','UL','HL','OL','SL','SI'))
			BEGIN
 				IF NOT EXISTS(SELECT * 
 								FROM PFNATT.dbo.TBL_TIMESHEET_DE 
 								WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID) --AND
 				BEGIN
 					print @TR_DATE
 					print @EMP_ID
 					print 'Masuk'
 					
 					SELECT @LOC_ID = LOC_ID 
 						FROM MICADEBUG.dbo.TBL_CARD_TR 
 						WHERE EMP_ID = @EMP_ID AND TR_DATE = @TR_DATE
 						
					SELECT @IA = IA 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID
					--IF @IA > 0 SET @TIMEOUT = DATEADD(MINUTE,ABS(@IA)*-1,@TIMEOUT)

 					INSERT INTO PFNATT.dbo.TBL_TIMESHEET_DE 
					(
						EMP_ID,
						CLIENT_JOBCODE,
						JOBCODE_ID,
						PROJECT_SECT_ID,
						CATEGORY_ID,
						FWBS,
						TR_DATE,
						TIME_IN,
						TIME_OUT,
						APPROVE,
						APPROVE1,
						APPROVE2,
						APPROVE3,
						REMARKS,
						TIMESPENT,
						LOC_ID,
 						MODIFY_BY,
 						LAST_MODIFIED,
						REMARKS_JOBCODE,
						KNUMBER,
						FWBS6,
						PLANT,
						UNIT
					)
					VALUES
 					(
 						@EMP_ID,
 						@CLIENT,
 						@JOB_ID,
 						@SECT_ID,
 						@CAT_ID,
						@FWBS,
 						@TR_DATE,
 						@TIMEIN ,
 						@TIMEOUT ,
 						0 ,
 						0 ,
 						0 ,
 						0 ,
 						'GENERATE ATT',
 						@TIMESPENT,
 						@LOC_ID,
						@USER_ID,
						GETDATE(),
						ISNULL(@REMARKS,''),
						@KNUMBER,
						@FWBS6,
						@PLANT,
						@UNIT
 					)
		 
		--  			IF EXISTS (SELECT * FROM TBL_MH_DAY_SPENT WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID)
		--  			BEGIN
		--  				DELETE FROM TBL_MH_DAY_SPENT 
		--  				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID
		--  			END
		 
		 
		--  			SET @TIMESPENT = (SELECT ISNULL(TIMESPENT,0) FROM TBL_TIMESHEET 
		--  				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID )
			
		--  			INSERT INTO TBL_MH_DAY_SPENT
		--  			VALUES
		--  			(
		--  				@EMP_ID,
		--  				@JOB_ID,
		--  				@CLIENT,
		--  				@SECT_ID,
		--  				@CAT_ID,
		--  				@TR_DATE,
		--  				'',
		--  				@TIMESPENT
		--  			)
 				END
			END
			IF EXISTS (SELECT * 
						FROM MICADEBUG.dbo.TBL_CARD_TR 
						WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID 
						AND ISNULL(ATT_TYPE_ID,'') IN ('AL','UL','HL','OL','SL','SI','AD','AB'))
						AND CONVERT(VARCHAR(12),@TR_DATE,101) <> CONVERT(VARCHAR(12),GETDATE(),101)
			BEGIN
				print '1'
				exec MICADEBUG.dbo.stp_instimesheet_leave @EMP_ID, @TR_DATE, @USER_ID
			END
				
			
			SET @TR_DATE = DATEADD(DAY,1,@TR_DATE)
		END
	END
END


SELECT @MESSAGE AS [Message]
--print CONVERT(VARCHAR(12),GETDATE(),101)

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetBalanceSheetJobCode]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_TimesheetGetBalanceSheetJobCode]
(
@IDDetail int,
@DivId varchar(10),
@DeptId varchar(10)
)
AS
 
DECLARE @MESSAGE varchar(100)
SET @MESSAGE = ''

select distinct a.JOB_CODE
				,b.PROJECT_NAME
				,0 As TotalMhPrevYear
				,0 As Jan
				,0 As Feb
				,0 As Mar
				,0 As Apr
				,0 As May
				,0 As Jun
				,0 As Jul
				,0 As Aug
				,0 As Sep
				,0 As Oct
				,0 As Nov
				,0 As Dec
from TBL_MASTER_BALANCE_SHEET_JOBCODE a
left outer join MICADEBUG.dbo.TBL_PROJECT b on a.JOB_CODE = b.JOBCODE_ID
where a.ID_DETAIL = @IDDetail



GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetCardTime]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetGetCardTime]
(
@TR_DATE DATETIME,
@EMP_ID VARCHAR(4)
)
-- EXEC sp_getTimeCard '1/28/2006','0858','','','',''
AS
DECLARE @CARDR_IN VARCHAR(5)
DECLARE @CARDR_OUT VARCHAR(5)
DECLARE @CARDIN INTEGER
DECLARE @CARDOUT INTEGER

SELECT @CARDIN = COUNT(*) 
	FROM MICADEBUG.dbo.TBL_CARD_TR 
	WHERE TR_DATE = @TR_DATE 
	AND EMP_ID = @EMP_ID 
	AND CARD_IN IS NOT NULL

SELECT @CARDOUT = COUNT(*) 
	FROM MICADEBUG.dbo.TBL_CARD_TR 
	WHERE TR_DATE = @TR_DATE 
	AND EMP_ID = @EMP_ID 
	AND CARD_OUT IS NOT NULL

IF (@CARDIN = 0 AND @CARDOUT = 0) 
BEGIN
--	SET @MSG = 'No Attendance Record For Date : [' + convert(varchar(12),@TR_DATE,106) + ']'
	SET @CARDR_IN = ''
	SET @CARDR_OUT = ''
END
ELSE 	
BEGIN
	IF @CARDIN = 0 OR @CARDOUT = 0
	BEGIN
		IF @CARDIN >= 1
			SELECT @CARDR_IN = CONVERT(VARCHAR(5),CARDR_IN,108) 
				FROM MICADEBUG.dbo.TBL_CARD_TR 
				WHERE TR_DATE = @TR_DATE 
				AND EMP_ID = @EMP_ID 
				AND CARD_IN IS NOT NULL
		ELSE
			SET @CARDR_IN = ''
		
		IF @CARDOUT >= 1
			SELECT @CARDR_OUT = CONVERT(VARCHAR(5),CARDR_OUT,108) 
				FROM MICADEBUG.dbo.TBL_CARD_TR 
				WHERE TR_DATE = @TR_DATE 
				AND EMP_ID = @EMP_ID 
				AND CARD_OUT IS NOT NULL
		ELSE
			SET @CARDR_OUT = ''
	END
	ELSE
	BEGIN
		SELECT @CARDR_IN = CONVERT(VARCHAR(5),CARDR_IN,108) 
			FROM MICADEBUG.dbo.TBL_CARD_TR 
			WHERE TR_DATE = @TR_DATE 
			AND EMP_ID = @EMP_ID 
			AND CARD_IN IS NOT NULL
		SELECT @CARDR_OUT = CONVERT(VARCHAR(5),CARDR_OUT,108) 
			FROM MICADEBUG.dbo.TBL_CARD_TR 
			WHERE TR_DATE = @TR_DATE 
			AND EMP_ID = @EMP_ID 
			AND CARD_OUT IS NOT NULL
	END
END
--SET @MSG = ISNULL(@MSG,'')
SET @CARDR_IN = ISNULL(@CARDR_IN,'')
SET @CARDR_OUT = ISNULL(@CARDR_OUT,'')

select @CARDR_IN as CARD_IN, @CARDR_OUT as CARD_OUT


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetDepartment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[usp_TimesheetGetDepartment]
(
@DepartmentId varchar(4)
)
AS

DECLARE @TempTable TABLE
(
SECT_ID varchar(4),
SECT_NAME char(100)
)

DECLARE @SQLSTRING AS VARCHAR(5000)
set @SQLSTRING = 'SELECT ''ALL'' AS SECT_ID,''ALL'' AS SECT_NAME UNION ALL SELECT SECT_ID, SECT_NAME FROM TBL_SECTION WHERE 1=1'
if LTRIM(RTRIM(@DepartmentId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' AND DEPT_ID = '''+@DepartmentId+''''
set @SQLSTRING = @SQLSTRING + ' ORDER BY SECT_ID'

print @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select * from @TempTable

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetDetailBalanceSheet]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetGetDetailBalanceSheet]
(
@IDMaster int
)
AS
 
DECLARE @MESSAGE varchar(100)
SET @MESSAGE = ''

select a.* 
from TBL_MASTER_BALANCE_SHEET_DETAIL a
where a.ID_MASTER = @IDMaster


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetDivision]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[usp_TimesheetGetDivision]
AS

SELECT 'ALL' AS DEPT_ID,'ALL' AS DEPT_NAME 
UNION ALL 
SELECT 'NON-DE' AS DEPT_ID , 'NON-DE' AS DEPT_NAME 
UNION ALL 
SELECT DEPT_ID, DEPT_NAME 
FROM MICADEBUG.dbo.TBL_DEPT ORDER BY DEPT_ID


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetEmployee]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE  PROCEDURE [dbo].[usp_TimesheetGetEmployee]
(
@Emp_Id varchar(4),
@Year integer,
@Div varchar(20),
@Dept varchar(20)
)
AS

DECLARE @TempTable TABLE
(
EMP_ID varchar(4),
EMPLOYEE_NAME char(50)
)

DECLARE @SQLSTRING AS VARCHAR(5000)
set @SQLSTRING = 'SELECT ''ALL'' AS EMP_ID,''ALL'' AS EMPLOYEE_NAME UNION ALL SELECT DISTINCT EMP_ID, EMPLOYEE_NAME '
set @SQLSTRING = @SQLSTRING + 'FROM MICADEBUG.dbo.V_EMPLOYEE WHERE EMP_ID IN (SELECT EMP_ID FROM MICADEBUG.dbo.TBL_MH_PLAN '
set @SQLSTRING = @SQLSTRING + 'WHERE P_YEAR = '+cast(@Year as varchar)+' AND JOBCODE_ID IN (SELECT JOBCODE_ID  FROM MICADEBUG.dbo.TBL_EMP_MODULE_JOBCODE)) '
if LTRIM(RTRIM(@Div)) = ''
	set @SQLSTRING = @SQLSTRING + ' SECT_ID = '+@Div
if LTRIM(RTRIM(@Dept)) = ''
	set @SQLSTRING = @SQLSTRING + ' DEPT_ID = '+@Dept
	
print @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select * from @TempTable


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetFWBS]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetGetFWBS]
(
@dlJobcode varchar(500)
)
AS

DECLARE @TempTable TABLE
(
CATEGORY_ID varchar(20),
CATEGORY_NAME char(1000)
)

DECLARE @SQLSTRING AS VARCHAR(5000)

set @SQLSTRING = 'SELECT ''ALL'' AS CATEGORY_ID,''ALL'' AS CATEGORY_NAME UNION ALL SELECT DISTINCT substring(CATEGORY_ID,5,6) AS CATEGORY_ID, '
set @SQLSTRING = @SQLSTRING + 'RTRIM(SUBSTRING(CATEGORY_ID,5,4)) + ''-'' + CATEGORY_NAME AS CATEGORY_NAME FROM MICADEBUG.dbo.TBL_CATEGORY '
set @SQLSTRING = @SQLSTRING + 'WHERE CATEGORY_ID IN (SELECT CATEGORY_ID FROM MICADEBUG.dbo.TBL_PROJECT_FWBS '
if LTRIM(RTRIM(@dlJobcode)) != 'ALL'			
	set @SQLSTRING = @SQLSTRING+ ' WHERE JOBCODE_ID = '''+@dlJobcode+''''
set @SQLSTRING = @SQLSTRING + ')'

print @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select * from @TempTable
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetGroup]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetGetGroup]
(
@Sect_Id varchar(500)
)
AS

if LTRIM(RTRIM(@Sect_Id)) != 'ALL'
begin
	SELECT 'ALL' AS GROUP_ID, 'ALL' AS GROUP_NAME 
	UNION ALL 
	SELECT GROUP_ID, GROUP_NAME 
	FROM MICADEBUG.dbo.TBL_EMP_GROUP
	WHERE SECT_ID = @Sect_Id
end
else
begin
	SELECT 'ALL' AS GROUP_ID, 'ALL' AS GROUP_NAME 
	UNION ALL 
	SELECT GROUP_ID, GROUP_NAME 
	FROM MICADEBUG.dbo.TBL_EMP_GROUP
end
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetHeader]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_TimesheetGetHeader]
@EMP_ID varchar(4)
AS
SELECT	dbo.TBL_EMP.EMP_ID
		, isnull(dbo.TBL_EMP.EMP_TYPE_ID, ' ') as EMP_TYPE
		, isnull(dbo.TBL_EMP_TYPE.TYPE_NAME, ' ') as TYPE_NAME
		, ISNULL(dbo.TBL_EMP.FIRSTNAME, '') + ' ' + ISNULL(dbo.TBL_EMP.LASTNAME, '') AS Name
		, isnull(dbo.TBL_EMP.GRADE, ' ') as GRADE
		, isnull(dbo.TBL_GRADE.GRADE_NAME, ' ') as GRADE_NAME
		, isnull(dbo.TBL_EMP.POSITION_ID, ' ') as POSITION_ID
		, isnull(dbo.TBL_POSITION.POSITION_NAME, ' ') as POSITION_NAME
		, isnull(dbo.TBL_GRADE.GRADE_ID, ' ') + '/' + isnull(dbo.TBL_EMP.EMP_TYPE_ID, ' ') AS EMP_STATUS
		, isnull(dbo.TBL_EMP.SECT_ID,' ') as SECT_ID
		, isnull(dbo.TBL_SECTION.SECT_NAME, ' ') + ' / ' + ISNULL(dbo.TBL_SECTION.GEC_DEPT, '') AS SECT_NAME
		, isnull(dbo.TBL_SECTION.DEPT_ID, ' ') as DEPT_ID
		, isnull(dbo.TBL_DEPT.DEPT_NAME, ' ') as DEPT_NAME
FROM	dbo.TBL_POSITION 
RIGHT OUTER JOIN dbo.TBL_EMP ON dbo.TBL_POSITION.POSITION_ID = dbo.TBL_EMP.POSITION_ID 
LEFT OUTER JOIN dbo.TBL_GRADE ON dbo.TBL_EMP.GRADE = dbo.TBL_GRADE.GRADE_ID 
LEFT OUTER JOIN dbo.TBL_LOCATION ON dbo.TBL_EMP.LOC_ID = dbo.TBL_LOCATION.LOC_ID 
LEFT OUTER JOIN dbo.TBL_DEPT 
INNER JOIN dbo.TBL_SECTION ON dbo.TBL_DEPT.DEPT_ID = dbo.TBL_SECTION.DEPT_ID ON dbo.TBL_EMP.SECT_ID = dbo.TBL_SECTION.SECT_ID 
LEFT OUTER JOIN dbo.TBL_EMP_TYPE ON dbo.TBL_EMP.EMP_TYPE_ID = dbo.TBL_EMP_TYPE.EMP_TYPE_ID
WHERE dbo.TBL_EMP.EMP_ID = @EMP_ID

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetHeaderBalanceSheet]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetGetHeaderBalanceSheet]
(
@Department varchar(4)
)
AS
 
DECLARE @MESSAGE varchar(100)
SET @MESSAGE = ''

select a.* 
from TBL_MASTER_BALANCE_SHEET a


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetJobcode]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetGetJobcode]
(
@Emp_id varchar(4),
@Dept varchar(4),
@Div varchar(4)
)
AS

DECLARE @TempTable TABLE
(
JOBCODE_ID varchar(20),
PROJECT_NAME char(1000)
)

DECLARE @SQLSTRING AS VARCHAR(5000)
set @SQLSTRING = 'SELECT DISTINCT JOBCODE_ID, JOBCODE_ID + ''|'' + DESCRIPTION AS PROJECT_NAME FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL UNION ALL '
set @SQLSTRING = @SQLSTRING + 'SELECT DISTINCT JOBCODE_ID, JOBCODE_ID + ''|'' + PROJECT_NAME AS PROJECT_NAME FROM MICADEBUG.dbo.TBL_PROJECT '
set @SQLSTRING = @SQLSTRING + 'WHERE JOBCODE_ID IN (SELECT JOBCODE_ID FROM MICADEBUG.dbo.TBL_EMP_MODULE_JOBCODE ) '
if LTRIM(RTRIM(@Dept)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + 'AND JOBCODE_ID IN (SELECT JOBCODE_ID FROM MICADEBUG.dbo.TBL_PROJECT_SEC WHERE SECT_ID IN (SELECT SECT_ID FROM TBL_SECTION WHERE DEPT_ID = '''+@Dept+'''))'
if LTRIM(RTRIM(@Div)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + 'AND JOBCODE_ID IN (SELECT JOBCODE_ID FROM MICADEBUG.dbo.TBL_PROJECT_SEC WHERE SECT_ID = '''+ @Div +''') '
set @SQLSTRING = @SQLSTRING + ' AND STATUS IN (0,1,2) ORDER BY JOBCODE_ID'

print @SQLSTRING
insert into @TempTable
SELECT 'ALL' AS JOBCODE_ID,'ALL' AS PROJECT_NAME

insert into @TempTable
EXEC (@SQLSTRING)
select * from @TempTable

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetPlant]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[usp_TimesheetGetPlant]
AS
select a.ID_PLANT, a.NAME
from PFNATT.dbo.TBL_PLANT a
union all
SELECT '' AS ID_PLANT, '-- Not Specified --' AS NAME
ORDER BY ID_PLANT ASC

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetGetUnit]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[usp_TimesheetGetUnit]
(
@PlantID varchar(2)
)
AS
select a.ID_UNIT, a.NAME
from PFNATT.dbo.TBL_UNIT a
where a.ID_PLANT = @PlantID
union all
SELECT '' AS ID_UNIT, '-- Not Specified --' AS NAME
ORDER BY ID_UNIT ASC

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetInsertTemplate_Rem]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetInsertTemplate_Rem]
(
@EMP_ID varchar(4), 
@PERIOD varchar(3), 
@MONTH int, 
@YEAR int, 
@JOBCODE varchar(100), 
@REG int, 
@OT int, 
@LOC_ID varchar(10), 
@CAT_ID varchar(10), 
@USER_ID varchar(4), 
@DAYSINMONTH int, 
@REMARKS varchar(500),
@FWBS6 varchar(10),
@Plant varchar(2),
@Unit int)
AS
--Created By Ary
--EXEC sp_InsertTimesheetTemplate '0834','ALL','3','2006','0-0182-00-0006|0-8900-10-1100|E230',480,120,'23',31

DECLARE @MESSAGE varchar(100)
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @FWBS VARCHAR(10)
DECLARE @TIME_START DATETIME
DECLARE @TIME_END DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TR_DATE DATETIME
DECLARE @TIMESPENT AS INT
SET DATEFIRST 1
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CLIENT = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF LTRIM(RTRIM(@CAT_ID)) <> ''
BEGIN
	SET @SECT_ID = LEFT(@CAT_ID,4)
	SET @CAT_ID = ISNULL(@CAT_ID,'')
	SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))
END

-- SELECT @CAT_ID = CATEGORY_ID FROM TBL_PROJECT_FWBS WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID
-- AND EXISTS (SELECT * FROM TBL_MH_PLAN WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID)

SET @MESSAGE = ''
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
BEGIN
	SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
END
ELSE
BEGIN
	IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
	BEGIN
		SET @MESSAGE = 'Please input remarks for this jobcode !'
	END
	ELSE
	BEGIN
		IF @PERIOD = '1'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/10/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '2'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/11/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/20/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '3'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/21/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = 'ALL'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END

		SET @TR_DATE = @TIME_START
		WHILE @TR_DATE <= @TIME_END
		BEGIN
			
			SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'IN')
			SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'OU')
			SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
			SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
			IF DATEPART(WEEKDAY,@TR_DATE) > 5 AND @TIMEIN IS NULL AND @TIMEOUT IS NULL
			BEGIN
				SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'IN')
				SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'OU')
				SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @TIMEIN = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEIN,108) AS DATETIME)
				SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEOUT,108) AS DATETIME)
				SET @REST_START = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_START,108) AS DATETIME)
				SET @REST_END = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_END,108) AS DATETIME)
			END

			IF @TIMEIN IS NOT NULL OR @TIMEOUT IS NOT NULL
			BEGIN
		--		IF EXISTS (SELECT * FROM TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND ATT_TYPE_ID NOT IN ('AB','AL','UL','HL','OL','SL','SI'))
		--		BEGIN

				IF NOT EXISTS(SELECT * FROM PFNATT.dbo.TBL_TIMESHEET_DE WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID) AND
					NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID = @LOC_ID)
					AND (@TIMEIN IS NOT NULL AND @TIMEOUT IS NOT NULL)
				AND DATEPART(WEEKDAY,@TR_DATE) <= 7
				BEGIN
					--IF (DATEDIFF(MINUTE,@TIMEIN,@TIMEOUT) - DATEDIFF(MINUTE,@REST_START,@REST_END)) <> @REG
					--	SET @TIMEOUT = DATEADD(MINUTE,@REG+DATEDIFF(MINUTE,@REST_START,@REST_END),@TIMEIN)
					IF @OT > 0 SET @TIMEOUT = DATEADD(MINUTE,@OT,@TIMEOUT)
					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)
					IF @LOC_ID = '23'
					BEGIN
						IF CONVERT(VARCHAR(5),DATEADD(MINUTE,30,@TIMEOUT),108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),DATEADD(MINUTE,60,@TIMEOUT),108) > '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,60,@TIMEOUT)
				
					END

					INSERT INTO PFNATT.dbo.TBL_TIMESHEET_DE 
					(
						EMP_ID,
						CLIENT_JOBCODE,
						JOBCODE_ID,
						PROJECT_SECT_ID,
						CATEGORY_ID,
						FWBS,
						TR_DATE,
						TIME_IN,
						TIME_OUT,
						APPROVE,
						APPROVE1,
						APPROVE2,
						APPROVE3,
						REMARKS,
						TIMESPENT,
						LOC_ID,
 						MODIFY_BY,
 						LAST_MODIFIED,
						REMARKS_JOBCODE,
						FWBS6,
						PLANT,
						UNIT
					)
					VALUES
					(
						@EMP_ID,
						@CLIENT,
						@JOB_ID,
						@SECT_ID,
						@CAT_ID,
						@FWBS,
						@TR_DATE,
						@TIMEIN ,
						@TIMEOUT ,
						0 ,
						0 ,
						0 ,
						0 ,
						'TEMPLATE',
						@TIMESPENT,
						@LOC_ID,
						@USER_ID,
						GETDATE(),
						ISNULL(@REMARKS,''),
						@FWBS6,
						@Plant,
						@Unit
					)

		-- 			IF EXISTS (SELECT * FROM TBL_MH_DAY_SPENT WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID)
		-- 			BEGIN
		-- 				DELETE FROM TBL_MH_DAY_SPENT 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID
		-- 			END


		-- 			SET @TIMESPENT = (SELECT ISNULL(TIMESPENT,0) FROM TBL_TIMESHEET 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID )
		-- 	
		-- 			INSERT INTO TBL_MH_DAY_SPENT
		-- 			VALUES
		-- 			(
		-- 				@EMP_ID,
		-- 				@JOB_ID,
		-- 				@CLIENT,
		-- 				@SECT_ID,
		-- 				@CAT_ID,
		-- 				@TR_DATE,
		-- 				'',
		-- 				@TIMESPENT
		-- 			)
				END
			END
			SET @TR_DATE = DATEADD(DAY,1,@TR_DATE)
		END
	END
END

select @MESSAGE as [Message]


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetJobcode]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetJobcode]
(
@LstYear integer,
@Month integer,
@Emp_Id varchar(4)
)
AS

select distinct RTRIM(LTRIM(X1.JOBCODE_ID))+'|'+RTRIM(LTRIM(X1.CLIENT_JOBCODE))+'|'+RTRIM(LTRIM(X1.SECT_ID))+'|'+LTRIM(RTRIM(X1.CATEGORY_ID)) AS JOBCODE_ID,
'['+RTRIM(LTRIM(X1.JOBCODE_ID))+'] - ['+ LTRIM(SUBSTRING(X1.CATEGORY_ID,5,4)) + '] ' + LTRIM(RTRIM(X1.PROJECT_NAME)) AS PROJECT_NAME
FROM MICADEBUG.dbo.V1_LIST_TIMESHEET_JOBCODE X1, MICADEBUG.dbo.TBL_PROJECT X2 
WHERE X1.JOBCODE_ID=X2.JOBCODE_ID 
AND X1.YEARTR = @LstYear 
AND X1.MONTHTR IN (0,@Month) 
AND X1.EMP_ID = @Emp_Id   
AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))), DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X2.ENDDATE)), DATEADD(m,1,X2.ENDDATE))
AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))), DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X1.ENDDATE)), DATEADD(m,1,X1.ENDDATE))
UNION ALL
SELECT RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE))+'|'+LEFT(LTRIM(RTRIM(CATEGORY_ID)),4)+'|'+LTRIM(RTRIM(CATEGORY_ID)) AS JOBCODE_ID,
'['+RTRIM(LTRIM(JOBCODE_ID))+'] - ['+ LTRIM(SUBSTRING(CATEGORY_ID,5,4)) + '] ' + LTRIM(RTRIM(DESCRIPTION)) AS PROJECT_NAME
FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL 
WHERE TRAINING = 0 
AND DEPT IN (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @Emp_Id) 
AND (ISNULL(EMP_ID,'') = '' OR (ISNULL(EMP_ID,'') <> '' 
AND CHARINDEX(@Emp_Id,EMP_ID) > 0 ))
UNION ALL 
SELECT '' AS JOBCODE_ID, '-- Not Specified --' AS PROJECT_NAME 
ORDER BY JOBCODE_ID ASC
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetJobcode_Rem]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[usp_TimesheetJobcode_Rem]
(
@LstYear integer,
@Month integer,
@Emp_Id varchar(4)
)
AS

SELECT DISTINCT RTRIM(LTRIM(X1.JOBCODE_ID))+'|'+RTRIM(LTRIM(X1.CLIENT_JOBCODE))AS JOBCODE_ID,
'['+RTRIM(LTRIM(X1.JOBCODE_ID))+'] '+LTRIM(RTRIM(X1.PROJECT_NAME)) AS PROJECT_NAME 
FROM MICADEBUG.dbo.V1_LIST_TIMESHEET_JOBCODE X1, TBL_PROJECT X2 
WHERE X1.JOBCODE_ID=X2.JOBCODE_ID AND X1.YEARTR = @LstYear AND X1.MONTHTR IN (0,@Month) AND X1.EMP_ID = @Emp_Id
AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))), DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X2.ENDDATE)), DATEADD(m,1,X2.ENDDATE))
AND DATEADD(dd, -DAY(DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))), DATEADD(m,1,cast(cast(@Month as char) + '/01/' + cast(@LstYear as char) as datetime))) <= DATEADD(dd, -DAY(DATEADD(m,1,X1.ENDDATE)), DATEADD(m,1,X1.ENDDATE))
UNION ALL 
SELECT RTRIM(LTRIM(JOBCODE_ID))+'|'+RTRIM(LTRIM(CLIENT_JOBCODE)) AS JOBCODE_ID,
'['+RTRIM(LTRIM(JOBCODE_ID))+'] '+LTRIM(RTRIM(DESCRIPTION)) AS PROJECT_NAME
FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL 
WHERE TRAINING = 0 AND DEPT IN (SELECT SECT_ID FROM TBL_EMP WHERE EMP_ID = @Emp_Id)
UNION ALL 
SELECT '' AS JOBCODE_ID, '-- Not Specified --' AS PROJECT_NAME
ORDER BY JOBCODE_ID ASC

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetLocation]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[usp_TimesheetLocation]
AS
SELECT LOC_ID, LOC_NAME FROM MICADEBUG.dbo.TBL_LOCATION ORDER BY LOC_ID

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetManHour]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[usp_TimesheetManHour]
(
@MONTH INT,
@YEAR INT,
@PERIOD CHAR(10),
@EMP_ID CHAR(4),
@TYPE CHAR(3)
)
AS

DECLARE @SQLSTRING AS VARCHAR(5000)

DECLARE @TempTable TABLE
(
EMP_ID varchar(4),
CARD_IN DATETIME,
CARD_OUT DATETIME,
OVERTIME DATETIME,
LOCATION VARCHAR(1000),
TOTAL INTEGER,
CL INTEGER,
EF INTEGER,
IA INTEGER,
TR_DATE DATETIME,
ATT_TYPE_ID VARCHAR(2),
DAYORDER INTEGER,
PERMISSION INTEGER
)

SET DATEFIRST  1 

SET @SQLSTRING = 'SELECT DISTINCT A.EMP_ID, A.CARD_IN AS CARD_IN, A.CARD_OUT AS CARD_OUT, ISNULL(A.OVERTIME,'''') AS OVERTIME, A.LOCATION, (REG+OT+HOL-(CL+EF+PERMISSION)) AS TOTAL ,CL,EF,PERMISSION AS IA, A.TR_DATE, ATT_TYPE_ID, MICADEBUG.dbo.FUNCT_CHECKHOLIDAY(TR_DATE,A.LOC_ID) AS DAYORDER, PERMISSION '
SET @SQLSTRING = @SQLSTRING + ' FROM MICADEBUG.dbo.V_TIMEATTENDANCE A '
SET @SQLSTRING = @SQLSTRING + ' WHERE (A.EMP_ID = ''' + @EMP_ID + ''' AND MONTH(A.TR_DATE) = ' + CAST(@MONTH AS VARCHAR) + ' AND YEAR(A.TR_DATE) = ' + CAST(@YEAR AS VARCHAR) 
IF LTRIM(RTRIM(@PERIOD)) = '1' SET @SQLSTRING = @SQLSTRING + ' AND DAY(A.TR_DATE) BETWEEN 1 AND 10 '
IF LTRIM(RTRIM(@PERIOD)) = '2' SET @SQLSTRING = @SQLSTRING + ' AND DAY(A.TR_DATE) BETWEEN 11 AND 20 '
IF LTRIM(RTRIM(@PERIOD)) = '3' SET @SQLSTRING = @SQLSTRING + ' AND DAY(A.TR_DATE) BETWEEN 21 AND 31 '
SET @SQLSTRING = @SQLSTRING + ' )'

INSERT INTO @TempTable
EXEC (@SQLSTRING)
SELECT 
ROW_NUMBER() over(order by TR_DATE asc) as No, *
FROM @TempTable

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetMHProjectSpend]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetMHProjectSpend]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
JOBCODE_ID varchar(30)
,PROJECT_NAME varchar(1000)
,CATEGORY_NAME varchar(100)
,FWBS varchar(10)
,CLIENT_NAME varchar(100)
,MH_TOTAL integer
,REVISION integer
,BeforeYear integer
,jan integer
,feb integer
,mar integer
,apr integer
,may integer
,jun integer
,jul integer
,aug integer
,sep integer
,oct integer
,nov integer
,dec integer
)

DECLARE @SQLSTRING AS VARCHAR(max)
DECLARE @BeforeYear as integer
set @BeforeYear = cast(@Year as integer)-1;
print @BeforeYear
set @SQLSTRING = '
select	a.JOBCODE_ID
		,a.PROJECT_NAME
		,d.CATEGORY_NAME
		,d.FWBS
		,b.CLIENT_NAME
		,isnull(a.MH_TOTAL,0) as MH_TOTAL
		,max(a.REVISION) as Revision
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+cast(@BeforeYear as varchar(4))+' and JOBCODE_ID = a.JOBCODE_ID) BeforeYear
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 1 and JOBCODE_ID = a.JOBCODE_ID) jan
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 2 and JOBCODE_ID = a.JOBCODE_ID) feb
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 3 and JOBCODE_ID = a.JOBCODE_ID) mar
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 4 and JOBCODE_ID = a.JOBCODE_ID) apr
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 5 and JOBCODE_ID = a.JOBCODE_ID) may
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 6 and JOBCODE_ID = a.JOBCODE_ID) jun
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 7 and JOBCODE_ID = a.JOBCODE_ID) jul
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 8 and JOBCODE_ID = a.JOBCODE_ID) aug
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 9 and JOBCODE_ID = a.JOBCODE_ID) sep
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 10 and JOBCODE_ID = a.JOBCODE_ID) oct
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 11 and JOBCODE_ID = a.JOBCODE_ID) nov
		,(select isnull(SUM(TIMESPENT),0) from MICADEBUG.dbo.TBL_TIMESHEET
		  where YEAR(TR_DATE) = '+@Year+' and MONTH(TR_DATE) = 12 and JOBCODE_ID = a.JOBCODE_ID) dec
from MICADEBUG.dbo.TBL_PROJECT a
inner join MICADEBUG.dbo.TBL_CLIENT b on a.CLIENT_NAME = b.CLIENT_ID
left outer join MICADEBUG.dbo.TBL_TIMESHEET c on a.JOBCODE_ID = c.JOBCODE_ID
left outer join MICADEBUG.dbo.TBL_CATEGORY d on d.CATEGORY_ID = c.CATEGORY_ID and d.FWBS = c.FWBS6 and d.SECT_ID = c.PROJECT_SECT_ID
left outer join MICADEBUG.dbo.TBL_SECTION e on e.SECT_ID = c.PROJECT_SECT_ID
where 1=1 '
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and c.PROJECT_SECT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and c.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
--if LTRIM(RTRIM(@Month)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and Month(c.TR_DATE) = ''' + @Month + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEAR(c.TR_DATE) = ' + @Year
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and c.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and e.DEPT_ID = ''' + @DeptId + ''''

set @SQLSTRING = @SQLSTRING + ' group by a.JOBCODE_ID ,d.CATEGORY_NAME ,d.FWBS ,b.CLIENT_NAME ,a.PROJECT_NAME ,a.MH_TOTAL '
PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select *
from @TempTable a
order by JOBCODE_ID
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetPinTemplate]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_TimesheetPinTemplate]
(
@EMP_ID varchar(4), 
@PERIOD varchar(3), 
@MONTH int, 
@YEAR int, 
@JOBCODE varchar(100), 
@REG int, 
@OT int, 
@LOC_ID varchar(10), 
@CAT_ID varchar(10), 
@USER_ID varchar(4), 
@DAYSINMONTH int, 
@REMARKS varchar(500),
@FWBS6 varchar(10)
)
AS
--Created By Ary
--EXEC sp_InsertTimesheetTemplate '0834','ALL','3','2006','0-0182-00-0006|0-8900-10-1100|E230',480,120,'23',31
DECLARE @MESSAGE varchar(100)
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @FWBS VARCHAR(10)
DECLARE @TIME_START DATETIME
DECLARE @TIME_END DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TR_DATE DATETIME
DECLARE @TIMESPENT AS INT
SET DATEFIRST 1
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CLIENT = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF LTRIM(RTRIM(@CAT_ID)) <> ''
BEGIN
	SET @SECT_ID = LEFT(@CAT_ID,4)
	SET @CAT_ID = ISNULL(@CAT_ID,'')
	SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))
END

-- SELECT @CAT_ID = CATEGORY_ID FROM TBL_PROJECT_FWBS WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID
-- AND EXISTS (SELECT * FROM TBL_MH_PLAN WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND SECT_ID = @SECT_ID)

SET @MESSAGE = ''
IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
BEGIN
	SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
END
ELSE
BEGIN
	IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
	BEGIN
		SET @MESSAGE = 'Please input remarks for this jobcode !'
	END
	ELSE
	BEGIN
		IF @PERIOD = '1'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/10/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '2'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/11/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/20/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = '3'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/21/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END
		IF @PERIOD = 'ALL'
		BEGIN
			SET @TIME_START = CAST(CAST(@MONTH AS VARCHAR) + '/1/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
			SET @TIME_END = CAST(CAST(@MONTH AS VARCHAR) + '/' + CAST(@DAYSINMONTH AS VARCHAR) + '/' + CAST(@YEAR AS VARCHAR) AS DATETIME)
		END

		SET @TR_DATE = @TIME_START
		WHILE @TR_DATE <= @TIME_END
		BEGIN
			
			SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'IN')
			SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(@TR_DATE,@LOC_ID,'OU')
			SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
			SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
			IF DATEPART(WEEKDAY,@TR_DATE) > 5 AND @TIMEIN IS NULL AND @TIMEOUT IS NULL
			BEGIN
				SET @TIMEIN = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'IN')
				SET @TIMEOUT = MICADEBUG.dbo.FUNCT_GETWORKTIME(DATEADD(DAY,-3,@TR_DATE),@LOC_ID,'OU')
				SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',DATEADD(DAY,-3,@TR_DATE),@LOC_ID)
				SET @TIMEIN = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEIN,108) AS DATETIME)
				SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@TIMEOUT,108) AS DATETIME)
				SET @REST_START = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_START,108) AS DATETIME)
				SET @REST_END = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + CONVERT(VARCHAR(5),@REST_END,108) AS DATETIME)
			END

			IF @TIMEIN IS NOT NULL OR @TIMEOUT IS NOT NULL
			BEGIN
		--		IF EXISTS (SELECT * FROM TBL_CARD_TR WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND ATT_TYPE_ID NOT IN ('AB','AL','UL','HL','OL','SL','SI'))
		--		BEGIN

				IF NOT EXISTS(SELECT * FROM MICADEBUG.dbo.TBL_TIMESHEET WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID) AND
					NOT EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_HOL_TR WHERE HOL_DATE = @TR_DATE AND LOC_ID = @LOC_ID)
					AND (@TIMEIN IS NOT NULL AND @TIMEOUT IS NOT NULL)
				AND DATEPART(WEEKDAY,@TR_DATE) <= 7
				BEGIN
					--IF (DATEDIFF(MINUTE,@TIMEIN,@TIMEOUT) - DATEDIFF(MINUTE,@REST_START,@REST_END)) <> @REG
					--	SET @TIMEOUT = DATEADD(MINUTE,@REG+DATEDIFF(MINUTE,@REST_START,@REST_END),@TIMEIN)
					IF @OT > 0 SET @TIMEOUT = DATEADD(MINUTE,@OT,@TIMEOUT)
					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)
					IF @LOC_ID = '23'
					BEGIN
						IF CONVERT(VARCHAR(5),DATEADD(MINUTE,30,@TIMEOUT),108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),DATEADD(MINUTE,60,@TIMEOUT),108) > '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,60,@TIMEOUT)
				
					END

					INSERT INTO MICADEBUG.dbo.TBL_TIMESHEET 
					(
						EMP_ID,
						CLIENT_JOBCODE,
						JOBCODE_ID,
						PROJECT_SECT_ID,
						CATEGORY_ID,
						FWBS,
						TR_DATE,
						TIME_IN,
						TIME_OUT,
						APPROVE,
						APPROVE1,
						APPROVE2,
						APPROVE3,
						REMARKS,
						TIMESPENT,
						LOC_ID,
 						MODIFY_BY,
 						LAST_MODIFIED,
						REMARKS_JOBCODE,
						FWBS6
					)
					VALUES
					(
						@EMP_ID,
						@CLIENT,
						@JOB_ID,
						@SECT_ID,
						@CAT_ID,
						@FWBS,
						@TR_DATE,
						@TIMEIN ,
						@TIMEOUT ,
						0 ,
						0 ,
						0 ,
						0 ,
						'TEMPLATE',
						@TIMESPENT,
						@LOC_ID,
						@USER_ID,
						GETDATE(),
						ISNULL(@REMARKS,''),
						@FWBS6
					)

		-- 			IF EXISTS (SELECT * FROM TBL_MH_DAY_SPENT WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID)
		-- 			BEGIN
		-- 				DELETE FROM TBL_MH_DAY_SPENT 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID
		-- 			END


		-- 			SET @TIMESPENT = (SELECT ISNULL(TIMESPENT,0) FROM TBL_TIMESHEET 
		-- 				WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND CLIENT_JOBCODE = LTRIM(RTRIM(@CLIENT)) AND PROJECT_SECT_ID = @SECT_ID AND TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID )
		-- 	
		-- 			INSERT INTO TBL_MH_DAY_SPENT
		-- 			VALUES
		-- 			(
		-- 				@EMP_ID,
		-- 				@JOB_ID,
		-- 				@CLIENT,
		-- 				@SECT_ID,
		-- 				@CAT_ID,
		-- 				@TR_DATE,
		-- 				'',
		-- 				@TIMESPENT
		-- 			)
				END
			END
			SET @TR_DATE = DATEADD(DAY,1,@TR_DATE)
		END
	END
END
SELECT @MESSAGE as [Message]

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportDEDSheet]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetReportDEDSheet]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
Location_Name varchar(1000)
,JobCode_Id varchar(50)
,Project_Name varchar(1000)
,TotalMhBefore integer
,jan integer
,feb integer
,mar integer
,apr integer
,may integer
,jun integer
,jul integer
,aug integer
,sep integer
,oct integer
,nov integer
,dec integer
)
DECLARE @BeforeYear as VARCHAR(4)
set @BeforeYear = CAST(CAST(@Year as integer) - 1 as varchar)
DECLARE @SQLSTRING AS VARCHAR(MAX)
set @SQLSTRING = '
USE MICADEBUG
select c.LOC_NAME
		,a.JOBCODE_ID
		,b.PROJECT_NAME
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and YEAR(x1.TR_DATE) = '+@BeforeYear+') as TotalMhBeforeYear
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 1
					and YEAR(x1.TR_DATE) = '+@Year+') as Jan
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 2
					and YEAR(x1.TR_DATE) = '+@Year+') as Feb
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 3
					and YEAR(x1.TR_DATE) = '+@Year+') as Mar
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 4
					and YEAR(x1.TR_DATE) = '+@Year+') as Apr
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 5
					and YEAR(x1.TR_DATE) = '+@Year+') as May
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 6
					and YEAR(x1.TR_DATE) = '+@Year+') as Jun
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 7
					and YEAR(x1.TR_DATE) = '+@Year+') as Jul
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 8
					and YEAR(x1.TR_DATE) = '+@Year+') as Aug
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 9
					and YEAR(x1.TR_DATE) = '+@Year+') as Sep
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 10
					and YEAR(x1.TR_DATE) = '+@Year+') as Oct
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 11
					and YEAR(x1.TR_DATE) = '+@Year+') as Nov
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 12
					and YEAR(x1.TR_DATE) = '+@Year+') as Dec
from TBL_TIMESHEET a
inner join 
(select a.JOBCODE_ID, a.PROJECT_NAME, max(a.REVISION) REVISION, a.LOC_ID
	from TBL_PROJECT a
	group by a.JOBCODE_ID, a.PROJECT_NAME, a.LOC_ID)b on a.JOBCODE_ID = b.JOBCODE_ID
inner join TBL_LOCATION c on c.LOC_ID = b.LOC_ID
inner join MICADEBUG.dbo.TBL_SECTION d on d.SECT_ID = a.PROJECT_SECT_ID
'
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.DEPT_ID = ''' + @DivId + ''''
	
--if LTRIM(RTRIM(@Fwbs)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and f.FWBS = ''' + @Fwbs + ''''
	
--if LTRIM(RTRIM(@jobcode)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
--if LTRIM(RTRIM(@Month)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and Month(e.TR_DATE) = ''' + @Month + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEAR(a.TR_DATE) = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.PROJECT_SECT_ID = ''' + @DeptId + ''''

set @SQLSTRING = @SQLSTRING + ' group by c.LOC_NAME ,a.JOBCODE_ID ,b.PROJECT_NAME '

set @SQLSTRING = @SQLSTRING+' union
select '' '' as LOC_NAME
		,a.JOBCODE_ID
		,b.DESCRIPTION as PROJECT_NAME
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and YEAR(x1.TR_DATE) = '+@BeforeYear+') as TotalMhBeforeYear
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 1
					and YEAR(x1.TR_DATE) = '+@Year+') as Jan
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 2
					and YEAR(x1.TR_DATE) = '+@Year+') as Feb
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 3
					and YEAR(x1.TR_DATE) = '+@Year+') as Mar
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 4
					and YEAR(x1.TR_DATE) = '+@Year+') as Apr
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 5
					and YEAR(x1.TR_DATE) = '+@Year+') as May
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 6
					and YEAR(x1.TR_DATE) = '+@Year+') as Jun
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 7
					and YEAR(x1.TR_DATE) = '+@Year+') as Jul
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 8
					and YEAR(x1.TR_DATE) = '+@Year+') as Aug
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 9
					and YEAR(x1.TR_DATE) = '+@Year+') as Sep
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 10
					and YEAR(x1.TR_DATE) = '+@Year+') as Oct
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 11
					and YEAR(x1.TR_DATE) = '+@Year+') as Nov
		,(select ISNULL(SUM(x1.TIMESPENT),0) from TBL_TIMESHEET x1 
			where x1.JOBCODE_ID = a.JOBCODE_ID 
					and MONTH(x1.TR_DATE) = 12
					and YEAR(x1.TR_DATE) = '+@Year+') as Dec
from TBL_TIMESHEET a
inner join MICADEBUG.dbo.TBL_PROJECT_GENERAL b on a.JOBCODE_ID = b.JOBCODE_ID
inner join MICADEBUG.dbo.TBL_SECTION d on d.SECT_ID = a.PROJECT_SECT_ID
'

if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.DEPT_ID = ''' + @DivId + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEAR(a.TR_DATE) = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.PROJECT_SECT_ID = ''' + @DeptId + ''''

set @SQLSTRING = @SQLSTRING + ' group by a.JOBCODE_ID, b.DESCRIPTION '

PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select *
from @TempTable


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportDepartmentExperience]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetReportDepartmentExperience]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @int int
declare @monthYear varchar(6)
set @int = CAST(@Month as int)
print @Month
print @Year
set @monthYear = @Year + RIGHT('0' + cast(@int%60 as varchar(2)), 2)
print @monthYear

DECLARE @TempTable TABLE
(
FWBS varchar(10)
,CATEGORY_NAME varchar (1000)
,SECT_NAME varchar(1000)
,EMP_ID varchar(4)
,EMPLOYEE_NAME varchar(5000)
,MHSPENT decimal(16,2)
,MONTH varchar(6)
)

DECLARE @SQLSTRING AS VARCHAR(max)
set @SQLSTRING = '
select a.FWBS
	  ,c.CATEGORY_NAME
	  ,b.SECT_NAME
	  ,a.EMP_ID
	  ,a.EMPLOYEE_NAME
	  ,a.MHSPENT 
	  ,a.MONTH
from MICADEBUG.dbo.TBL_GEC_N a
inner join MICADEBUG.dbo.TBL_SECTION b on a.GEC_DEPT = b.GEC_DEPT and a.SECT_ID = b.SECT_ID
inner join (select distinct x.CATEGORY_ID, x.CATEGORY_NAME from MICADEBUG.dbo.TBL_FWBS x) c on c.CATEGORY_ID = a.FWBS
where 1=1 AND MONTH = ' + @monthYear
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and b.DEPT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.SECT_ID = ''' + @DeptId + ''''


PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select FWBS 
		,CATEGORY_NAME
		,SECT_NAME
		,EMP_ID
		,EMPLOYEE_NAME
		,SUM(MHSPENT) MHSPENT
		,MONTH
from @TempTable
group by FWBS 
		,CATEGORY_NAME
		,SECT_NAME
		,EMP_ID
		,EMPLOYEE_NAME
		,MONTH
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportGroupProcessUnit]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[usp_TimesheetReportGroupProcessUnit]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
PLANT_ID INT
,PLANT_NAME varchar (500)
,UNIT_ID INT
,UNIT_NAME varchar(500)
,GROUP_NAME varchar(500)
,EMP_ID varchar(4)
,EMPLOYEE_NAME varchar(5000)
,MHSPENT int
)

DECLARE @SQLSTRING AS VARCHAR(max)
set @SQLSTRING = '
select a.Plant
		,e.NAME as PlantName
		,a.Unit
		,f.NAME as UnitName
		,c.GROUP_NAME
		,a.EMP_ID
		,d.FIRSTNAME + '' '' + ISNULL(d.LASTNAME,'''') as FullName
		,a.TIMESPENT
from MICADEBUG.dbo.TBL_TIMESHEET a
inner join MICADEBUG.dbo.TBL_EMP_GROUP_MEMBER b on a.EMP_ID = b.EMP_ID and b.MONTHTR = '+@Month+' and b.YEARTR = '+@Year+'
inner join MICADEBUG.dbo.TBL_EMP_GROUP c on b.GROUP_ID = c.GROUP_ID
inner join MICADEBUG.dbo.TBL_EMP d on d.EMP_ID = a.EMP_ID
inner join PFNATT.dbo.tbl_plant e on a.Plant = e.ID_PLANT
inner join PFNATT.dbo.TBL_UNIT f on a.Unit = f.ID_UNIT
inner join MICADEBUG.dbo.TBL_SECTION g on g.SECT_ID = d.SECT_ID
where 1=1 '
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and g.DEPT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.SECT_ID = ''' + @DeptId + ''''


PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select PLANT_ID
		,PLANT_NAME
		,UNIT_ID
		,UNIT_NAME
		,GROUP_NAME
		,EMP_ID
		,EMPLOYEE_NAME
		,SUM(MHSPENT) as MHSPENT
from @TempTable
group by PLANT_ID
		,PLANT_NAME
		,UNIT_ID
		,UNIT_NAME
		,GROUP_NAME
		,EMP_ID
		,EMPLOYEE_NAME

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportHistorical]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetReportHistorical]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
EMP_ID varchar(4)
,FULL_NAME varchar(100)
,POSITION_NAME varchar(100)
,GRADE varchar(10)
,EMP_TYPE_ID varchar(10)
,SECT_NAME varchar(100)
,DEPT_NAME VARCHAR(100)
,TOTAL_SPENT INTEGER
,CATEGORY_NAME VARCHAR(100)
,FWBS VARCHAR(10)
)

DECLARE @SQLSTRING AS VARCHAR(max)
set @SQLSTRING = '
SELECT	a.EMP_ID
		,a.FIRSTNAME+'' ''+isnull(a.LASTNAME,'''') as FullName 
		,c.POSITION_NAME
		,a.GRADE
		,a.EMP_TYPE_ID
		,d.SECT_NAME
		,e.DEPT_NAME
		,SUM(b.TIMESPENT) as TotalSpent
		,f.CATEGORY_NAME
		,f.FWBS
FROM MICADEBUG.dbo.TBL_EMP a
inner join MICADEBUG.dbo.TBL_TIMESHEET b on a.EMP_ID = b.EMP_ID
inner join MICADEBUG.dbo.TBL_POSITION c on a.POSITION_ID = c.POSITION_ID
inner join MICADEBUG.dbo.TBL_SECTION d on d.SECT_ID = a.SECT_ID
inner join MICADEBUG.dbo.TBL_DEPT e on e.DEPT_ID=d.DEPT_ID
inner join MICADEBUG.dbo.TBL_CATEGORY f on f.CATEGORY_ID = b.CATEGORY_ID and f.FWBS = b.FWBS and f.SECT_ID = b.PROJECT_SECT_ID
where 1=1 '
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and e.DEPT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and f.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and b.JOBCODE_ID = ''' + @jobcode + ''''
	
--if LTRIM(RTRIM(@Month)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and Month(b.TR_DATE) = ''' + @Month + ''''

--if LTRIM(RTRIM(@Year)) != 'ALL'
--	set @SQLSTRING = @SQLSTRING + ' and YEAR(b.TR_DATE) = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and b.PROJECT_SECT_ID = ''' + @DeptId + ''''

set @SQLSTRING = @SQLSTRING + ' group by a.EMP_ID,a.FIRSTNAME,a.LASTNAME,c.POSITION_NAME,a.GRADE,a.EMP_TYPE_ID,d.SECT_NAME,e.DEPT_NAME,f.CATEGORY_NAME,f.FWBS order by EMP_ID '
PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select *
from @TempTable


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportMHSummary]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetReportMHSummary]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
CATEGORY_ID varchar(50)
,CATEGORY_NAME varchar(100)
,EMP_ID varchar(4)
,FullName varchar(100)
,GRADE varchar(5)
,TIME_IN datetime
,TIME_OUT datetime
,MinuteHour varchar(100)
,InMinute integer
,SECT_NAME varchar(100)
,SECT_ID varchar(50)
,DEPT_ID varchar(10)
,JOBCODE_ID varchar(50)
,TR_Month integer
,TR_YEAR integer
,TR_DATE datetime
,FWBS varchar(10)
,POSITION_NAME VARCHAR(100)
,CLientName varchar(100)
,ProjectName varchar(500)
,Descriptions varchar(5000)
)
PRINT @DivId
DECLARE @SQLSTRING AS VARCHAR(max)
set @SQLSTRING = '
select distinct	b.CATEGORY_ID
		,b.CATEGORY_NAME
		,c.EMP_ID
		,c.FIRSTNAME + '' '' + isnull(c.LASTNAME,'''') as FullName
		,c.GRADE
		,a.TIME_IN
		,a.TIME_OUT
		,substring(convert(varchar(12),(a.TIME_OUT-a.TIME_IN),108), 1,5) as MinuteHour
		,DATEDIFF(SS,a.TIME_IN,a.TIME_OUT)/60 as InMinute
		,d.SECT_NAME
		,d.SECT_ID
		,d.DEPT_ID
		,a.JOBCODE_ID
		,Month(a.TR_DATE) as TR_Month
		,YEAR(a.TR_DATE) as TR_YEAR
		,a.TR_DATE
		,b.FWBS
		,e.POSITION_NAME
		,g.CLIENT_NAME
		,f.PROJECT_NAME
		,f.SCOPE as Description
from MICADEBUG.dbo.TBL_TIMESHEET a
inner join MICADEBUG.dbo.TBL_CATEGORY b on a.CATEGORY_ID = b.CATEGORY_ID
inner join MICADEBUG.dbo.TBL_EMP c on c.EMP_ID = a.EMP_ID
inner join MICADEBUG.dbo.TBL_SECTION d on d.SECT_ID = c.SECT_ID
INNER JOIN MICADEBUG.dbo.TBL_POSITION e on e.POSITION_ID = c.POSITION_ID
INNER JOIN 
(
SELECT x.JOBCODE_ID, max(x.REVISION) REVISION, x.PROJECT_NAME, x.CLIENT_NAME, x.SCOPE
FROM MICADEBUG.dbo.TBL_PROJECT x
group by x.JOBCODE_ID, x.PROJECT_NAME, x.CLIENT_NAME, x.SCOPE
) f on f.JOBCODE_ID = a.JOBCODE_ID
INNER JOIN MICADEBUG.dbo.TBL_CLIENT g on g.CLIENT_ID = f.CLIENT_NAME
where 1=1 '
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.DEPT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and b.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
if LTRIM(RTRIM(@Month)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and Month(a.TR_DATE) = ''' + @Month + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEAR(a.TR_DATE) = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and c.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.SECT_ID = ''' + @DeptId + ''''
/*
set @SQLSTRING = @SQLSTRING + 'union
select distinct	b.CATEGORY_ID
		,b.CATEGORY_NAME
		,c.EMP_ID
		,c.FIRSTNAME + '' '' + ISNULL(c.LASTNAME,'''') as FullName
		,c.GRADE
		,a.TIME_IN
		,a.TIME_OUT
		,substring(convert(varchar(12),(a.TIME_OUT-a.TIME_IN),108), 1,5) as MinuteHour
		,DATEDIFF(SS,a.TIME_IN,a.TIME_OUT)/60 as InMinute
		,d.SECT_NAME
		,d.SECT_ID
		,d.DEPT_ID
		,a.JOBCODE_ID
		,Month(a.TR_DATE) as TR_Month
		,YEAR(a.TR_DATE) as TR_YEAR
		,a.TR_DATE
		,b.FWBS
		,e.POSITION_NAME
		,'' '' as CLIENT_NAME
		,f.DESCRIPTION as PROJECT_NAME
		,f.DESCRIPTION as Description
from MICADEBUG.dbo.TBL_TIMESHEET a
inner join MICADEBUG.dbo.TBL_CATEGORY b on a.CATEGORY_ID = b.CATEGORY_ID
inner join MICADEBUG.dbo.TBL_EMP c on c.EMP_ID = a.EMP_ID
inner join MICADEBUG.dbo.TBL_SECTION d on d.SECT_ID = c.SECT_ID
INNER JOIN MICADEBUG.dbo.TBL_POSITION e on e.POSITION_ID = c.POSITION_ID
INNER JOIN MICADEBUG.dbo.TBL_PROJECT_GENERAL f on f.JOBCODE_ID = a.JOBCODE_ID
where 1=1  '
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.SECT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and b.FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and a.JOBCODE_ID = ''' + @jobcode + ''''
	
if LTRIM(RTRIM(@Month)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and Month(a.TR_DATE) = ''' + @Month + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEAR(a.TR_DATE) = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and c.EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and d.DEPT_ID = ''' + @DeptId + ''''
*/

print @SQLSTRING

insert into @TempTable
EXEC (@SQLSTRING)
select CATEGORY_ID
		,CATEGORY_NAME
		,EMP_ID
		,FullName
		,GRADE
		,SUM(InMinute) as InMinute
		,SECT_NAME
		,SECT_ID
		,DEPT_ID
		,JOBCODE_ID
		,TR_Month
		,TR_YEAR
		,FWBS
		,POSITION_NAME
		,CLientName
		,ProjectName
		,Descriptions 
from @TempTable
group by CATEGORY_ID
		,CATEGORY_NAME
		,EMP_ID
		,FullName
		,GRADE
		,SECT_NAME
		,SECT_ID
		,DEPT_ID
		,JOBCODE_ID
		,TR_Month
		,TR_YEAR
		,FWBS
		,POSITION_NAME
		,CLientName
		,ProjectName
		,Descriptions 
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportOverall]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[usp_TimesheetReportOverall]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
JOBCODE_ID varchar(30)
,PROJECT_NAME varchar(100)
,CLIENT_NAME varchar(100)
,TimeCount integer
,FWBS varchar(10)
,CATEGORY_NAME varchar(100)
)

DECLARE @SQLSTRING AS VARCHAR(max)
set @SQLSTRING = '
SELECT [JOBCODE_ID]
	  ,[PROJECT_NAME]
	  ,[CLIENT_NAME]
	  , [TOTALTS1] + [TOTALTS2] + [TOTALTS3] as TimeCount
	  ,[FWBS]
	  ,[CATEGORY_NAME]
FROM [MICADEBUG].[dbo].[TBL_ASSIGNMENT_SCHEDULE]
where 1=1'
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and DEPT_ID = ''' + @DivId + ''''
	
if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and FWBS = ''' + @Fwbs + ''''
	
if LTRIM(RTRIM(@jobcode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and JOBCODE_ID = ''' + @jobcode + ''''
	
if LTRIM(RTRIM(@Month)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and MONTHTR = ''' + @Month + ''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and YEARTR = ''' + @Year + ''''
	
if LTRIM(RTRIM(@EmpId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and EMP_ID = ''' + @EmpId + ''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and SECT_ID = ''' + @DeptId + ''''


PRINT @SQLSTRING
insert into @TempTable
EXEC (@SQLSTRING)
select JOBCODE_ID
		,PROJECT_NAME
		,CLIENT_NAME
		,SUM(TimeCount) TimeCount
		,FWBS
		,CATEGORY_NAME
from @TempTable
group by JOBCODE_ID
		,PROJECT_NAME
		,CLIENT_NAME
		,FWBS
		,CATEGORY_NAME
GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetReportYearlyAssignment]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[usp_TimesheetReportYearlyAssignment]
(
@DivId varchar(4)
,@Fwbs varchar(10)
,@jobcode varchar(20)
,@Month varchar(2)
,@Year varchar(4)
,@EmpId varchar(4)
,@Group varchar(10)
,@DeptId varchar(4)
,@Period varchar(4)
)
AS

DECLARE @TempTable TABLE
(
	[EMP_ID] varchar(4),
	[NAME] varchar(151),
	[JOBCODE_ID] varchar(20),
	[CATEGORY_ID] varchar(20),
	[FWBS] varchar(10) ,
	[P_JAN] int ,
	[P_FEB] int ,
	[P_MAR] int ,
	[P_APR] int ,
	[P_MAY] int ,
	[P_JUN] int ,
	[P_JUL] int ,
	[P_AUG] int ,
	[P_SEP] int ,
	[P_OCT] int ,
	[P_NOV] int ,
	[P_DEC] int ,
	[A_JAN] int ,
	[A_FEB] int ,
	[A_MAR] int ,
	[A_APR] int ,
	[A_MAY] int ,
	[A_JUN] int ,
	[A_JUL] int ,
	[A_AUG] int ,
	[A_SEP] int ,
	[A_OCT] int ,
	[A_NOV] int ,
	[A_DEC] int ,
	[PROJECT_NAME] varchar(200),
	[YEARTR] int ,
	[SECT_ID] varchar(4) ,
	[DEPT_ID] varchar(4) ,
	[SECT_NAME] varchar(100)
)

DECLARE @SQLSTRING AS VARCHAR(5000)
set @SQLSTRING = 'SELECT [EMP_ID]
      ,[NAME]
      ,[JOBCODE_ID]
      ,[CATEGORY_ID]
      ,[FWBS]
      ,[P_JAN]
      ,[P_FEB]
      ,[P_MAR]
      ,[P_APR]
      ,[P_MAY]
      ,[P_JUN]
      ,[P_JUL]
      ,[P_AUG]
      ,[P_SEP]
      ,[P_OCT]
      ,[P_NOV]
      ,[P_DEC]
      ,[A_JAN]
      ,[A_FEB]
      ,[A_MAR]
      ,[A_APR]
      ,[A_MAY]
      ,[A_JUN]
      ,[A_JUL]
      ,[A_AUG]
      ,[A_SEP]
      ,[A_OCT]
      ,[A_NOV]
      ,[A_DEC]
      ,[PROJECT_NAME]
      ,[YEARTR]
      ,[SECT_ID]
      ,[DEPT_ID]
      ,[SECT_NAME]
  FROM [MICADEBUG].[dbo].[V_PLAN_ACTUAL] where 1=1'
  
if LTRIM(RTRIM(@DivId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and [DEPT_ID] = ''' + @DivId+''''

if LTRIM(RTRIM(@Fwbs)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and [FWBS] = ''' + @Fwbs+''''

if LTRIM(RTRIM(@jobCode)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and [JOBCODE_ID] = ''' + @jobCode+''''

if LTRIM(RTRIM(@Year)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and [YEARTR] = '''+@Year+''''
	
if LTRIM(RTRIM(@DeptId)) != 'ALL'
	set @SQLSTRING = @SQLSTRING + ' and [SECT_ID] = '''+@DeptId+''''
	
	

print @SQLSTRING

insert into @TempTable
EXEC (@SQLSTRING)
select * from @TempTable

GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetUpdate]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec stp_UpdManHour '6/7/2009','05:30','18:30','1299','0858','0-0352-07-0001|0-3400-20-0000|E230|E230136',780,'0-0352-07-0001|0-3400-20-0000|E230|E230136'
CREATE PROCEDURE [dbo].[usp_TimesheetUpdate]
(
@TR_DATE datetime, 
@TIME_IN varchar(5), 
@TIME_OUT varchar(5), 
@EMP_ID varchar(4), 
@USER_ID varchar(4), 
@JOBCODE varchar(100), 
@REMARKS varchar(500), 
@SPENT int, 
@JOBCODE_OLD varchar(100),
@FWBS6 VARCHAR(10),
@Plant varchar(2),
@Unit int
)
AS
--Created By Ary, update by Jeremy
 
DECLARE @MESSAGE varchar(100)
SET @MESSAGE = ''
DECLARE @TIMEIN DATETIME
DECLARE @TIMEOUT DATETIME
DECLARE @TIMEIN_TS DATETIME
DECLARE @TIMEOUT_TS DATETIME
DECLARE @CLIENT VARCHAR(20)
DECLARE @JOB_ID VARCHAR(20)
DECLARE @SECT_ID VARCHAR(4)
DECLARE @CAT_ID VARCHAR(10)
DECLARE @FWBS VARCHAR(10)
DECLARE @LOC_ID VARCHAR(2)
DECLARE @CLIENT_OLD VARCHAR(20)
DECLARE @JOB_ID_OLD VARCHAR(20)
DECLARE @SECT_ID_OLD VARCHAR(4)
DECLARE @TIMEINLIMIT DATETIME
DECLARE @TIMEINSTART DATETIME
DECLARE @REST_START DATETIME
DECLARE @REST_END DATETIME
DECLARE @TIMESPENT INTEGER
DECLARE @KNUMBER VARCHAR(6)-- HQ APR2013

SET @JOBCODE_OLD = LTRIM(RTRIM(@JOBCODE_OLD))
SET @JOBCODE = LTRIM(RTRIM(@JOBCODE))
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @JOB_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @CLIENT = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @JOBCODE = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
END
IF CHARINDEX('|',@JOBCODE) > 0
BEGIN
	SET @SECT_ID = LEFT(@JOBCODE,CHARINDEX('|',@JOBCODE)-1)
	SET @CAT_ID = SUBSTRING(@JOBCODE,CHARINDEX('|',@JOBCODE)+1,LEN(@JOBCODE)-CHARINDEX('|',@JOBCODE))
	SET @FWBS = RIGHT(LTRIM(RTRIM(@CAT_ID)),LEN(LTRIM(RTRIM(@CAT_ID)))-LEN(@SECT_ID))
END

IF CHARINDEX('|',@JOBCODE_OLD) > 0
BEGIN
	SET @JOB_ID_OLD = LEFT(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)-1)
	SET @JOBCODE_OLD = SUBSTRING(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)+1,LEN(@JOBCODE_OLD)-CHARINDEX('|',@JOBCODE_OLD))
END

IF CHARINDEX('|',@JOBCODE_OLD) > 0
BEGIN
	SET @CLIENT_OLD = LEFT(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)-1)
	SET @JOBCODE_OLD = SUBSTRING(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)+1,LEN(@JOBCODE_OLD)-CHARINDEX('|',@JOBCODE_OLD))
END

IF CHARINDEX('|',@JOBCODE_OLD) > 0
BEGIN
	SET @SECT_ID_OLD = LEFT(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)-1)
	--SET @CAT_ID = SUBSTRING(@JOBCODE_OLD,CHARINDEX('|',@JOBCODE_OLD)+1,LEN(@JOBCODE_OLD)-CHARINDEX('|',@JOBCODE_OLD))
END


SET @TIMEIN = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_IN AS DATETIME)
SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_OUT AS DATETIME)
--IF @TIME_OUT = '00:00' OR @TIME_OUT = '00.00' SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

PRINT @JOB_ID_OLD
PRINT @CLIENT_OLD
PRINT @SECT_ID_OLD

SET @CAT_ID = ISNULL(@CAT_ID,'')
SET @JOB_ID = LTRIM(RTRIM(@JOB_ID))
SET @JOB_ID_OLD = LTRIM(RTRIM(@JOB_ID_OLD))
SET @CLIENT = LTRIM(RTRIM(@CLIENT))
SET @CLIENT_OLD = LTRIM(RTRIM(@CLIENT_OLD))

SET @KNUMBER = (SELECT MICADEBUG.dbo.Funct_GetIDEASKNumber(@EMP_ID)) -- HQ APR 2013

IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_PROJECT_GENERAL 
			WHERE JOBCODE_ID = LTRIM(RTRIM(@JOB_ID)) AND TRAINING = 1)
BEGIN
	SET @MESSAGE = 'This Jobcode ' + LTRIM(RTRIM(@JOB_ID)) + ' no longer in use, please contact Mr. Nurmansyah (HRD-Training) !'
END
ELSE
BEGIN
	IF LTRIM(RTRIM(@JOB_ID)) = '7-1000-05-0004' AND LTRIM(RTRIM(@REMARKS)) = ''
	BEGIN
		SET @MESSAGE = 'Please input remarks for this jobcode !'
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_TIMESHEET 
					WHERE TR_DATE = @TR_DATE 
					AND EMP_ID = @EMP_ID 
					AND JOBCODE_ID = @JOB_ID_OLD 
					AND CLIENT_JOBCODE = @CLIENT_OLD 
					AND PROJECT_SECT_ID = @SECT_ID_OLD)
		BEGIN
 			PRINT 'ADA'
			SELECT @LOC_ID = ISNULL(LOC_ID,'00') FROM MICADEBUG.dbo.TBL_TIMESHEET 
				WHERE EMP_ID = @EMP_ID 
				AND TR_DATE = @TR_DATE
				AND JOBCODE_ID =@JOB_ID_OLD 
				AND CLIENT_JOBCODE = @CLIENT_OLD 
				AND PROJECT_SECT_ID = @SECT_ID_OLD
			SET @REST_START = MICADEBUG.dbo.FUNCT_GETRESTTIME('IN',@TR_DATE,@LOC_ID)
			SET @REST_END = MICADEBUG.dbo.FUNCT_GETRESTTIME('OU',@TR_DATE,@LOC_ID)
			PRINT @LOC_ID
			IF @JOB_ID <> @JOB_ID_OLD
			BEGIN
				PRINT 'MASUK'
 				DELETE FROM MICADEBUG.dbo.TBL_TIMESHEET  WHERE TR_DATE = @TR_DATE AND EMP_ID = @EMP_ID AND JOBCODE_ID = @JOB_ID_OLD AND CLIENT_JOBCODE = @CLIENT_OLD AND PROJECT_SECT_ID = @SECT_ID_OLD
 				DELETE FROM MICADEBUG.dbo.TBL_MH_DAY_SPENT WHERE DATE_TR = @TR_DATE AND EMP_ID = @EMP_ID AND JOBCODE_ID = @JOB_ID_OLD AND CLIENT_JOBCODE = @CLIENT_OLD AND PROJECT_SECT_ID = @SECT_ID_OLD
				
 				IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_TIMESHEET 
 							WHERE TR_DATE = @TR_DATE 
 							AND EMP_ID = @EMP_ID 
							AND JOBCODE_ID = @JOB_ID 
							AND CLIENT_JOBCODE = @CLIENT 
							AND PROJECT_SECT_ID = @SECT_ID) 
 				BEGIN
					SELECT @TIMEIN_TS = MIN(TIME_IN) 
						FROM MICADEBUG.dbo.TBL_TIMESHEET 
						WHERE TR_DATE = @TR_DATE 
						AND EMP_ID = @EMP_ID 
						AND JOBCODE_ID = @JOB_ID 
						AND CLIENT_JOBCODE = @CLIENT 
						AND PROJECT_SECT_ID = @SECT_ID
					SELECT @TIMEOUT_TS = MIN(TIME_OUT) 
						FROM MICADEBUG.dbo.TBL_TIMESHEET 
						WHERE TR_DATE = @TR_DATE 
						AND EMP_ID = @EMP_ID 
						AND JOBCODE_ID = @JOB_ID 
						AND CLIENT_JOBCODE = @CLIENT 
						AND PROJECT_SECT_ID = @SECT_ID
					IF @TIMEIN > @TIMEIN_TS SET @TIMEIN = @TIMEIN_TS
					IF @TIMEOUT_TS < @TIME_OUT SET @TIMEOUT = @TIMEOUT_TS

					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
						AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

					IF LTRIM(RTRIM(@LOC_ID)) = '23'  
					BEGIN	
						IF CONVERT(VARCHAR(5),@TIMEOUT,108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,-30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),@TIMEOUT,108) > '20:00' OR (@TIMEOUT > @TIMEIN AND CONVERT(VARCHAR(12),@TIMEOUT,101) > CONVERT(VARCHAR(12),@TIMEIN,101))
							SET @TIMEOUT = DATEADD(MINUTE,-60,@TIMEOUT)
					END

					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)

					SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_OUT AS DATETIME)
					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
						AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)
          

					UPDATE PFNATT.dbo.TBL_TIMESHEET_DE SET
						TIME_IN = @TIMEIN,
						TIME_OUT = @TIMEOUT,
						TIMESPENT = @TIMESPENT,
						KNUMBER = @KNUMBER, -- HQ
						MODIFY_BY = @USER_ID,
						LAST_MODIFIED = GETDATE(),
						FWBS6 = @FWBS6,
						Plant = @Plant,
						Unit = @Unit
					WHERE TR_DATE = @TR_DATE 
					AND EMP_ID = @EMP_ID 
					AND JOBCODE_ID = @JOB_ID 
					AND CLIENT_JOBCODE = @CLIENT 
					AND PROJECT_SECT_ID = @SECT_ID

				END	
				ELSE

				BEGIN
					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
						AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

					IF LTRIM(RTRIM(@LOC_ID)) = '23'  
					BEGIN	
						IF CONVERT(VARCHAR(5),@TIMEOUT,108) = '20:00'	
							SET @TIMEOUT = DATEADD(MINUTE,-30,@TIMEOUT)
						ELSE IF CONVERT(VARCHAR(5),@TIMEOUT,108) > '20:00' OR (@TIMEOUT > @TIMEIN AND CONVERT(VARCHAR(12),@TIMEOUT,101) > CONVERT(VARCHAR(12),@TIMEIN,101))
							SET @TIMEOUT = DATEADD(MINUTE,-60,@TIMEOUT)
					END
					
					SET @TIMESPENT =  ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)

					SET @TIMEOUT = CAST(CAST(MONTH(@TR_DATE) AS VARCHAR) + '/' + CAST(DAY(@TR_DATE) AS VARCHAR) + '/' + CAST(YEAR(@TR_DATE) AS VARCHAR) + ' ' + @TIME_OUT AS DATETIME)
					IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
						AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
						SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

		 			
					IF LTRIM(RTRIM(@JOB_ID)) LIKE 'N-%'
						EXEC MICADEBUG.dbo.STP_INSTIMESHEET_LEAVE @EMP_ID, @TR_DATE, @USER_ID
					ELSE
					BEGIN

			 			INSERT INTO MICADEBUG.dbo.TBL_TIMESHEET 
						(
							EMP_ID,
							CLIENT_JOBCODE,
							JOBCODE_ID,
							PROJECT_SECT_ID,
							CATEGORY_ID,
							FWBS,
							TR_DATE,
							TIME_IN,
							TIME_OUT,
							APPROVE,
							APPROVE1,
							APPROVE2,
							APPROVE3,
							REMARKS,
							TIMESPENT,
							LOC_ID,
 							MODIFY_BY,
 							LAST_MODIFIED,
							REMARKS_JOBCODE,
							KNUMBER,
							FWBS6,
							Plant,
							Unit
						)
						VALUES
	 					(
 						@EMP_ID,
 						@CLIENT,
 						@JOB_ID,
 						@SECT_ID,
 						@CAT_ID,
						@FWBS,
 						@TR_DATE,
 						@TIMEIN ,
 						@TIMEOUT ,
 						0 ,
 						0 ,
 						0 ,
 						0 ,
 						'',
 						@TIMESPENT,
 						@LOC_ID,
						@USER_ID,
						GETDATE(),
						ISNULL(@REMARKS,''),
						@KNUMBER,
						@FWBS6,
						@Plant,
						@Unit
						)
		 			 
					END
				END
			END
			ELSE
			BEGIN
				IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
					AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
					SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

				IF LTRIM(RTRIM(@LOC_ID)) = '23'  
				BEGIN	
					IF CONVERT(VARCHAR(5),@TIMEOUT,108) = '20:00'	
						SET @TIMEOUT = DATEADD(MINUTE,-30,@TIMEOUT)
					ELSE IF CONVERT(VARCHAR(5),@TIMEOUT,108) > '20:00' OR (@TIMEOUT > @TIMEIN AND CONVERT(VARCHAR(12),@TIMEOUT,101) > CONVERT(VARCHAR(12),@TIMEIN,101))
						SET @TIMEOUT = DATEADD(MINUTE,-60,@TIMEOUT)
				END
				

  				IF CAST(@SPENT AS INTEGER) <> 0
  				BEGIN	
					IF EXISTS (SELECT * FROM MICADEBUG.dbo.TBL_CARD_TR 
								WHERE EMP_ID = @EMP_ID 
								AND TR_DATE = @TR_DATE 
								AND @TIMEIN BETWEEN CARDR_IN AND CARDR_OUT 
								AND @TIMEOUT BETWEEN CARDR_IN AND CARDR_OUT)
						SET @SPENT = @SPENT
					ELSE
					begin
	  					SET @SPENT = ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0)
					end
					
  				END
		 		

	 			SET @SPENT = ISNULL(MICADEBUG.dbo.FUNCT_GETTOTALHOUR(@TIMEIN,@TIMEOUT,@REST_START,@REST_END),0) 

				IF  CONVERT(VARCHAR(5),@TIMEOUT,108) < CONVERT(VARCHAR(5),@TIMEIN,108) 
					AND CONVERT(VARCHAR(12),@TIMEOUT,101) = CONVERT(VARCHAR(12),@TIMEIN,101)
					SET @TIMEOUT = DATEADD(DAY,1,@TIMEOUT)

 				UPDATE PFNATT.dbo.TBL_TIMESHEET_DE SET
	 					TIMESPENT = @SPENT,
	 					TIME_IN = @TIMEIN,
 						TIME_OUT = @TIMEOUT,
						JOBCODE_ID = @JOB_ID,
						CLIENT_JOBCODE = @CLIENT,
						PROJECT_SECT_ID = @SECT_ID,
						CATEGORY_ID = @CAT_ID,
						FWBS = @FWBS,
						MODIFY_BY = @USER_ID,
						LAST_MODIFIED = GETDATE(),
						REMARKS_JOBCODE = ISNULL(@REMARKS,''),
						KNUMBER = @KNUMBER, -- HQ
						FWBS6 = @FWBS6,
						Plant = @Plant,
						Unit = @Unit
	 			WHERE TR_DATE = @TR_DATE 
	 			AND EMP_ID = @EMP_ID 
	 			AND JOBCODE_ID = @JOB_ID_OLD 
	 			AND CLIENT_JOBCODE = @CLIENT_OLD 
	 			AND PROJECT_SECT_ID = @SECT_ID_OLD

			END

		END
	END
END

SELECT @MESSAGE AS Message


GO
/****** Object:  StoredProcedure [dbo].[usp_TimesheetViewInsert]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec stp_ListEmployeeManHour 5,2009,'ALL','1332','TSH-T'
CREATE  PROCEDURE [dbo].[usp_TimesheetViewInsert]
(
@MONTH INT,
@YEAR INT,
@PERIOD CHAR(10),
@EMP_ID CHAR(4),
@TYPE CHAR(3)
)
AS

DECLARE @TempTable TABLE
(
EMP_ID varchar(4),
TOTAL INTEGER,
TR_DATE DATETIME,
TIME_IN DATETIME,
TIME_OUT DATETIME,
TOTAL_SPENT INTEGER,
JOBCODE_ID VARCHAR(500),
PROJECT_NAME VARCHAR(500),
CLIENT_JOBCODE VARCHAR(500),
DAYORDER INTEGER,
VISIBLE VARCHAR(20),
GANJIL INTEGER,
CAT_ID VARCHAR(20),
UNBALANCE_HOUR INTEGER,
TOTALMHDAY INTEGER,
APPROVE3 INTEGER,
REMAKRS_JOBCODE VARCHAR(500)
)

DECLARE @SQLSTRING AS VARCHAR(5000)

SET DATEFIRST  1 
SET @SQLSTRING = 'SELECT DISTINCT B.EMP_ID, ISNULL((REG+OT+HOL-(CL+EF+PERMISSION)),0) AS TOTAL ,  B.DATE_TR AS TR_DATE, B.TIME_IN, B.TIME_OUT, ISNULL(B.TOTAL_SPENT,0) AS TOTAL_SPENT,'
SET @SQLSTRING = @SQLSTRING + ' RTRIM(LTRIM(JOBCODE_ID))+''|''+RTRIM(LTRIM(CLIENT_JOBCODE))+''|''+RTRIM(LTRIM(PROJECT_SECT_ID))+''|''+RTRIM(LTRIM(CATEGORY_ID)) AS JOBCODE_ID, ISNULL(LTRIM(RTRIM(B.PROJECT_NAME)),'''') AS PROJECT_NAME,ISNULL(B.CLIENT_JOBCODE,'''') CLIENT_JOBCODE, '
SET @SQLSTRING = @SQLSTRING + ' MICADEBUG.dbo.FUNCT_CHECKHOLIDAY(B.DATE_TR,A.LOC_ID) AS DAYORDER, ''TRUE'' AS VISIBLE, 1 AS GANJIL, SUBSTRING(CATEGORY_ID,5,5) AS CAT_ID, '
SET @SQLSTRING = @SQLSTRING + ' ISNULL((REG+OT+HOL-(CL+EF+PERMISSION)),0)-MICADEBUG.dbo.FUNCT_TOTALMHSPENT(B.DATE_TR,B.EMP_ID) AS UNBALANCE_HOUR, MICADEBUG.dbo.FUNCT_TOTALMHSPENT(B.DATE_TR,B.EMP_ID) AS TOTALMHDAY, CAST(ISNULL(APPROVE3,0) AS INTEGER) AS APPROVE3, REMARKS_JOBCODE '
SET @SQLSTRING = @SQLSTRING + ' FROM MICADEBUG.dbo.V_TIMEATTENDANCE A RIGHT JOIN MICADEBUG.dbo.V_MHSPENTPERDATE B ON A.EMP_ID = B.EMP_ID AND A.TR_DATE = B.DATE_TR '
SET @SQLSTRING = @SQLSTRING + ' WHERE (B.EMP_ID = ''' + @EMP_ID + ''' AND MONTH(B.DATE_TR) = ' + CAST(@MONTH AS VARCHAR) + ' AND YEAR(B.DATE_TR) = ' + CAST(@YEAR AS VARCHAR) 
IF LTRIM(RTRIM(@PERIOD)) = '1' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 1 AND 10 '
IF LTRIM(RTRIM(@PERIOD)) = '2' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 11 AND 20 '
IF LTRIM(RTRIM(@PERIOD)) = '3' SET @SQLSTRING = @SQLSTRING + ' AND DAY(B.DATE_TR) BETWEEN 21 AND 31 '
SET @SQLSTRING = @SQLSTRING + ' AND ISNULL(B.JOBCODE_ID,'''') <> '''') ORDER BY B.DATE_TR, TIME_IN'

print @SQLSTRING
INSERT INTO @TempTable
EXEC (@SQLSTRING)
SELECT * FROM @TempTable

--EXEC stp_ListEmployeeManHour 5,2007,'ALL','0858','TSH'


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateKK]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_UpdateKK '2076'
CREATE PROCEDURE [dbo].[usp_UpdateKK]
(
	@EmployeeId VARCHAR(10), @KKNo VARCHAR(50), @UpdateBy VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP
	SET		KK_NUMBER = @KKNo, MODIFY_BY = @UpdateBy
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateKTP]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_UpdateKTP '2076'
CREATE PROCEDURE [dbo].[usp_UpdateKTP]
(
	@EmployeeId VARCHAR(10), @KTPNo VARCHAR(20), @KTPValidity DATETIME, @UpdateBy VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP
	SET		IDNUMBER = @KTPNo, IDNUMBER_EXPIRE = @KTPValidity, MODIFY_BY = @UpdateBy
	WHERE	EMP_ID = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePassport]    Script Date: 6/10/2016 5:47:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_UpdatePassport '2076'
CREATE PROCEDURE [dbo].[usp_UpdatePassport]
(
	@EmployeeId VARCHAR(10), @PassportNo VARCHAR(20), @PassportValidity DATETIME, @UpdateBy VARCHAR(10)
)
AS
BEGIN
	UPDATE	TBL_EMP
	SET		PASSPORT = @PassportNo, PASSPORTV = @PassportValidity, MODIFY_BY = @UpdateBy
	WHERE	EMP_ID = @EmployeeId
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OTA, ALA, LAA, TSI,' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBL_LOG_EMAIL', @level2type=N'COLUMN',@level2name=N'MsgType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBL_FASKES"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 203
               Right = 347
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwFaskesList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwFaskesList'
GO
USE [master]
GO
ALTER DATABASE [PFNATT] SET  READ_WRITE 
GO
