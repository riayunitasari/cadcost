﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CADcost.Entities
{
    public class Connection
    {
        public static string PFNATTConn
        { get { return ConfigurationManager.ConnectionStrings["PFNATTConnectionString"].ConnectionString; } }

        public static string SOFIConn
        { get { return ConfigurationManager.ConnectionStrings["PertaBEConnectionString"].ConnectionString; } }

        public static string CADcostConn
        { get { return ConfigurationManager.ConnectionStrings["CADcostConnectionString"].ConnectionString; } }
    }
}