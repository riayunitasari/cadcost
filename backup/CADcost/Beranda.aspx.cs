﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Entities;
using CADcost.Helper;

namespace CADcost
{
    public partial class Beranda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            const string curItem = "list_Item_cnst";
            const string curUser = "current_Log_In_User";
            const string curState = "current_Req_State";
            const string curReq = "current_Requisition";
            const string GANoState = "is_GA_REQ_NO_Generated";
            const string quotID = "quotation_ID_Of_curReq";
            const string dlFile = "downloaded_File";
            const string flName = "downloaded_FileName";
            Employee logInUser = new Employee();

            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                lblUsername.Text += logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                lblDate.Text = DateTime.Now.Day.ToString();
                lblMonth.Text =" "+DateTime.Now.ToString("MMMM")+" ";
            }
        }
    }
}