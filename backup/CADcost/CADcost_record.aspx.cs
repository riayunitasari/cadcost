﻿using CADcost.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;

namespace CADcost
{
    public partial class CADcost_record : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        DateTime timeStart = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                int id_cadreq = Helper.CADcostHelper.getLastCADreqNO();
                PFNATTHelper.generateDDLProjectName(DropDownListProject);
                DropDownListProject_SelectedIndexChanged(sender, e);
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                    DropDownListProject_SelectedIndexChanged(sender, e);
                }
                if (logInUser.ISADMIN)
                {
                    PanelApproveENIT.Visible = true;
                }
                else
                {
                    PanelApproveENIT.Visible = false;
                }

            }
            else
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                    DropDownListProject_SelectedIndexChanged(sender, e);
                }
                if (logInUser.ISADMIN)
                {
                    PanelApproveENIT.Visible = true;
                }
                else
                {
                    PanelApproveENIT.Visible = false;
                }
            }
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = null;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewApproveENIT.DataSource = CADcostHelper.getCADreq(DropDownListProject.SelectedValue.ToString());
            GridViewApproveENIT.DataBind();
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser);
            GridViewLicenseRequest.DataBind();
            GridViewActionHistory.DataSource = CADcostHelper.getActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID);
            GridViewActionHistory.DataBind();
        }

        protected void GridViewApproveENIT_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("view"))
            {
                string[] CommandArgument = e.CommandArgument.ToString().Split(',');
                ScriptManager.RegisterStartupScript(this, GetType(), "alertApprove", "opendialog('dialogApprove');", true);
                PanelApprove.Visible = true;
                ButtonApproveOK.CommandArgument = CommandArgument[0].ToString();
                GridViewApprove.DataSource = CADcostHelper.getItem(Convert.ToInt32(CommandArgument[0]));
                GridViewApprove.DataBind();
                LabelCommentApprove.Text = CADcostHelper.getLastActivityComment(Convert.ToInt32(CommandArgument[0]));
            }
            
        }

        protected void GridViewLicenseRequest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string[] CommandArgument = e.CommandArgument.ToString().Split(',');
            if (e.CommandName.Equals("view"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertView", "opendialog('dialogView');", true);
                PanelView.Visible = true;
                GridViewView.DataSource = CADcostHelper.getItem(Convert.ToInt32(CommandArgument[0]));
                GridViewView.DataBind();
                LabelCommentView.Text = CommandArgument[1];
            }
            else if (e.CommandName.Equals("editrequest"))
            {
                
            }
            else if (e.CommandName.Equals("cancelrequest"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertCancel", "opendialog('dialogCancel');", true);
                PanelCancel.Visible = true;
                GridViewCancel.DataSource = CADcostHelper.getItem(Convert.ToInt32(CommandArgument[0]));
                GridViewCancel.DataBind();
                LabelCommentCancel.Text = CommandArgument[1];
                ButtonCancelOK.CommandArgument = CommandArgument[0];
            }
        }

        protected void GridViewActionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("view"))
            {
                string[] CommandArgument = e.CommandArgument.ToString().Split(',');
                ScriptManager.RegisterStartupScript(this, GetType(), "alertViewAction", "opendialog('dialogView');", true);
                PanelView.Visible = true;
                GridViewView.DataSource = CADcostHelper.getItem(Convert.ToInt32(CommandArgument[0]));
                GridViewView.DataBind();
                LabelCommentView.Text = CommandArgument[1];
                ButtonApproveOK.CommandArgument = CommandArgument[0];
            }
        }

        protected void ButtonApproveOK_Click(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "closedialog('dialogApprove');", true);
            Button btn =(Button)sender;
            CADcostHelper.approveRequest(Convert.ToInt32(btn.CommandArgument), logInUser.EMP_ID);


            ACTION_HISTORY act = new ACTION_HISTORY();
            act.ID_ACTION = 6; //approve request
            act.ID_ACTIVITY = 4; //approved by enit
            act.ID_CADREQ = Convert.ToInt32(btn.CommandArgument);
            act.JOBCODE = DropDownListProject.SelectedValue;
            act.TIME_START = timeStart;
            act.TIME_END = DateTime.Now;
            act.COMMENT = TextBoxCommentApprove.Text;
            if (logInUser.ISPARTICIPANT)
            {
                act.PARTICIPANT = logInUser.EMP_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
            }
            else
            {
                act.PARTICIPANT = logInUser.PARTICIPANT_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
                act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
            }

            CADcostHelper.insertActionHistory(act);

            Response.Redirect(Request.RawUrl);

        }

        protected void ButtonApproveCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonViewCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonCancelOK_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonCancelCancel_Click(object sender, EventArgs e)
        {

        }

        protected void GridViewLicenseRequest_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void GridViewLicenseRequest_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void GridViewApproveENIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewApproveENIT.PageIndex = e.NewPageIndex;
            GridViewApproveENIT.DataSource = CADcostHelper.getCADreq(DropDownListProject.SelectedValue.ToString());
            GridViewApproveENIT.DataBind();
        }

        protected void GridViewLicenseRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.PageIndex = e.NewPageIndex;
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser);
            GridViewLicenseRequest.DataBind();
        }

        protected void GridViewActionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewActionHistory.PageIndex = e.NewPageIndex;
            GridViewActionHistory.DataSource = CADcostHelper.getActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID);
            GridViewActionHistory.DataBind();
        }

        

    }
}