﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_server.aspx.cs" Inherits="CADcost.Software_server" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="http://localhost:52689/code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        function closedialog(dialogname) {
            $("#" + dialogname).dialog("close");
        }

        function opendialog(dialogname) {
            $("#" + dialogname).dialog({
                title: "Summary",
                modal: true,
                resizable: false,
                width: 'auto',
                create: function (event, ui) { }
            });
            $("#" + dialogname).parent().appendTo($("#form1"));
        }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="80%"></asp:DropDownList><br />
    Month: <asp:DropDownList ID="DropDownListMonth" runat="server" OnSelectedIndexChanged="DropDownListMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> Year: <asp:DropDownList ID="DropDownListYear" runat="server" OnSelectedIndexChanged="DropDownListYear_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><br />
    <h2>License Server List</h2>
    <asp:Button ID="ButtonAddLicenseServer" runat="server" Text="Add License Server" OnClick="ButtonAddLicenseServer_Click"/>
    <asp:GridView ID="GridViewSoftwareServer" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewSoftwareServer_RowEditing" OnRowDeleting="GridViewSoftwareServer_RowDeleting" OnRowUpdating="GridViewSoftwareServer_RowUpdating" OnRowCancelingEdit="GridViewSoftwareServer_RowCancelingEdit" OnRowDataBound="GridViewSoftwareServer_RowDataBound">
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Software">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftwareServer" runat="server"  Value='<%# Eval("ID_SOFTWARE_SERVER") %>'/>
                    <asp:HiddenField ID="HiddenFieldIDSoftware" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="License Status">
                <ItemTemplate>
                    <asp:Label ID="LabelLicenseStatus" runat="server" Text='<%# Eval("LICENSE_STATUS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="LabelDepartment" runat="server" Text='<%# Eval("DEPT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quantity">
                <ItemTemplate>
                    <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IP Server">
                <ItemTemplate>
                    <asp:Label ID="LabelIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    
                    <asp:DropDownList ID="DropDownListServer" runat="server"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateCost" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelCost" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditCost" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteCost" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#666666" />

    </asp:GridView>

    <h2>Server List</h2>
    <asp:GridView ID="GridViewServerList" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true" OnRowEditing="GridViewServerList_RowEditing" OnRowDeleting="GridViewServerList_RowDeleting" OnRowUpdating="GridViewServerList_RowUpdating" OnRowCancelingEdit="GridViewServerList_RowCancelingEdit">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddServer" runat="server" Text="Add"  OnClick="ButtonAddServer_Click"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Server Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServer" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:Label ID="LabelServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServerEdit" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:TextBox ID="TextBoxServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxServerNameAdd" runat="server"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IP Server">
                <ItemTemplate>
                    <asp:Label ID="LabelIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxIPServerAdd" runat="server"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateServer" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelServer" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditServer" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteServer" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#666666" />
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>

    </asp:GridView>
    <div id="dialogAddLicenseServer">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="PanelAddLicenseServer" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table2" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Add License Server</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Software :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="DropDownListSoftware" runat="server"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Department :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="DropDownListDepartment" runat="server"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Server :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="DropDownListServer" runat="server"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Add" ID="ButtonAddLicenseServerOK" OnClick="ButtonAddLicenseServerOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonAddLicenseServerCancel" OnClick="ButtonAddLicenseServerCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>
</asp:Content>
