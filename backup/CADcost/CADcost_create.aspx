﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_create.aspx.cs" Inherits="CADcost.CADcost_create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        function closedialog(dialogname) {
            $("#" + dialogname).dialog("close");
        }

        function opendialog(dialogname) {
            $("#" + dialogname).dialog({
                title: "Summary",
                modal: true,
                resizable: false,
                width: 'auto',
                create: function (event, ui) { }
            });
            $("#" + dialogname).parent().appendTo($("#form1"));
        }

  </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <asp:Table ID="TableCreateCADcost" runat="server" Width="100%" GridLines="Horizontal" ForeColor="Black" CellPadding="10">
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">CAD Cost Request NO : </asp:TableCell>
            <asp:TableCell><asp:Label ID="LabelCADcostNO" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Department : </asp:TableCell>
            <asp:TableCell><asp:Label ID="LabelDepartment" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Project : </asp:TableCell>
            <asp:TableCell><asp:DropDownList ID="DropDownListProject" runat="server" VerticalAlign="Middle" Width="100%"></asp:DropDownList>
                <asp:Label ID="LabelProject" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">
                Item :
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center">
                 <asp:GridView ID="GridViewItem" runat="server" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666" HorizontalAlign="Center" Font-Size="Small" ShowFooter="True" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnDataBound="GridViewItem_DataBound"  Width="90%" OnRowCommand="GridViewItem_RowCommand" OnRowEditing="GridViewItem_RowEditing" OnRowUpdating="GridViewItem_RowUpdating" OnRowDeleting="GridViewItem_RowDeleting" OnRowDataBound="GridViewItem_RowDataBound" OnRowCancelingEdit="GridViewItem_RowCancelingEdit" RowStyle-HorizontalAlign="Center">


        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>

        
        <Columns>
            <asp:TemplateField HeaderText="No.">
                <FooterTemplate>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                </FooterTemplate>
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Software Name">
                <ItemTemplate>

                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>' Width="80%"></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameEdit" runat="server" Width="80%"></asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameAdd" runat="server" Width="80%"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <ItemTemplate>

                        <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>' Width="80%"></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBoxQuantityEdit" runat="server" Text='<%# Eval("QUANTITY") %>' Width="80%"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="TextBoxQuantityAdd" runat="server" Width="80%"></asp:TextBox>
                    </FooterTemplate>

                </asp:TemplateField>
            <asp:TemplateField HeaderText="Remark">
                <ItemTemplate>
                    <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxRemarkEdit" runat="server" Text='<%# Eval("REMARK") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxRemarkAdd" runat="server" Text='<%# Eval("REMARK") %>'></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdate" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>

                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
            <FooterStyle BackColor="#666666" />

    <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>

                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
   
    <asp:Table runat="server" Width="100%" GridLines="Horizontal" ForeColor="Black" CellPadding="10">
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">
                Description:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBoxDescription" runat="server"></asp:TextBox>
                <asp:Label ID="LabelDescription" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" class="button-green button-radius-50" OnClick="ButtonSubmit_Click"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSaveAsDraft" runat="server" Text="SaveAsDraft" class="button-white button-radius-50" OnClick="ButtonSaveAsDraft_Click"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" class="button-red button-radius-50" OnClick="ButtonCancel_Click"/>
                </span>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="dialogSummary">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanelSummary" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table1" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Summary</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewItemSummary" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment: <asp:TextBox ID="TextBoxComment" runat="server"></asp:TextBox></h5>
                        </asp:TableCell>
                    </asp:TableRow>
            
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                                <asp:Button runat="server" class="button-green button-radius-50" Text="OK" ID="ButtonConfirmOK" OnClick="ButtonConfirmOK_Click"/>
                                <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonConfirmCancel" OnClick="ButtonConfirmCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>
    
</asp:Content>
