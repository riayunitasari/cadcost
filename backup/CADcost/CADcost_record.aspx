﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_record.aspx.cs" Inherits="CADcost.CADcost_record" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        function closedialog(dialogname) {
            $("#" + dialogname).dialog("close");
        }

        function opendialog(dialogname) {
            $("#" + dialogname).dialog({
                title: "Summary",
                modal: true,
                resizable: false,
                width: 'auto',
                create: function (event, ui) { }
            });
            $("#" + dialogname).parent().appendTo($("#form1"));
        }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="80%"></asp:DropDownList>
    <asp:Panel ID="PanelApproveENIT" runat="server">
    <h2>To Be Approved - ENIT</h2>
    <asp:GridView ID="GridViewApproveENIT" runat="server" AutoGenerateColumns="False" OnRowCommand="GridViewApproveENIT_RowCommand" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewApproveENIT_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True">
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>

                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requester">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request" >
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                
                    <asp:LinkButton ID="LinkButtonApproveENITView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>'>View</asp:LinkButton>
                
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />

<RowStyle HorizontalAlign="Center"></RowStyle>
    </asp:GridView>
    </asp:Panel>
    <h2>License Request History</h2>
    <asp:GridView ID="GridViewLicenseRequest" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" OnRowCommand="GridViewLicenseRequest_RowCommand" OnRowEditing="GridViewLicenseRequest_RowEditing" OnRowCancelingEdit="GridViewLicenseRequest_RowCancelingEdit" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewLicenseRequest_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True">
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonRequestHistoryView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ")+ "," + Eval("COMMENT") %>'>View</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryEdit" runat="server" CommandName="editrequest" CommandArgument='<%# Eval("ID_CADREQ")+ "," + Eval("COMMENT")%>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryCancel" runat="server" CommandName="cancelrequest" CommandArgument='<%# Eval("ID_CADREQ")+ "," + Eval("COMMENT")%>'>Cancel</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" HorizontalAlign="Right" />
        <RowStyle HorizontalAlign="Center" />
    </asp:GridView>
    
    <h2>Action History</h2>
    <asp:GridView ID="GridViewActionHistory" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" OnRowCommand="GridViewActionHistory_RowCommand" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewActionHistory_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True">
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request By">
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("CADREQ_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("DEPT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Time">
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("NAME_ACTION") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action Time">
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("TIME_END") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="View CAD Request">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonActionHistoryView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ")+ "," + Eval("COMMENT") %>'>View</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle HorizontalAlign="Right" />

<RowStyle HorizontalAlign="Center"></RowStyle>
    </asp:GridView>

    <div id="dialogApprove">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="PanelApprove" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table1" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewApprove" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentApprove" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment for Approve : </h5>
                            <asp:TextBox ID="TextBoxCommentApprove" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Approve?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Approve" ID="ButtonApproveOK" CommandArgument="" OnClick="ButtonApproveOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonApproveCancel" OnClick="ButtonApproveCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>

    <div id="dialogView">
        <asp:UpdatePanel runat="server" ID="PanelView" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table2" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewView" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                        
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentView" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center"><asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonViewCancel" OnClick="ButtonViewCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>

    <div id="dialogCancel">
        <asp:UpdatePanel runat="server" ID="PanelCancel" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table3" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewCancel" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentCancel" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment for Cancel Request : </h5>
                            <asp:TextBox ID="TextBoxCommentCancel" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Cancel Request?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Cancel Request" ID="ButtonCancelOK" OnClick="ButtonCancelOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonCancelCancel" OnClick="ButtonCancelCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>
</asp:Content>
