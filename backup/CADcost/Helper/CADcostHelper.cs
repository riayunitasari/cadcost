﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CADcost.Entities;
using System.Web.UI.WebControls;
using System.Globalization;

namespace CADcost.Helper
{
    [Serializable]
    public class CADcostItem
    {
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public int QUANTITY { get; set; }
        public decimal PRICE { get; set; }
        public string STATUS { get; set; }
        public string IP_SERVER { get; set; }
        public string REMARK { get; set; }

        public CADcostItem() { }
        public CADcostItem(int id, string name, int qtty, string remark)
        {
            ID_SOFTWARE = id;
            NAME_SOFTWARE = name;
            QUANTITY = qtty;
            REMARK = remark;
        }
        public CADcostItem(int id, decimal price)
        {
            ID_SOFTWARE = id;
            PRICE = price;
        }

    }

    public class ApproveENIT
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string FULL_NAME { get; set; }
        public string SECT_ID { get; set; }
        public string DATE_REQUEST { get; set; }
        public string TIME_REQUEST { get; set; }
        public string NAME_ACTIVITY { get; set; }
        public string DESCRIPTION { get; set; }
    }

    public class LicenseHistory
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string DATE_REQUEST { get; set; }
        public string TIME_REQUEST { get; set; }
        public string NAME_ACTIVITY { get; set; }
        public string DESCRIPTION { get; set; }
        public string COMMENT { get; set; }
    }

    public class ActionHistoryDetail
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string CADREQ_BY { get; set; }
        public string DEPT { get; set; }
        public string TIME_REQUEST { get; set; }
        public string NAME_ACTION { get; set; }
        public string TIME_END { get; set; }
        public string DESCRIPTION { get; set; }
        public string COMMENT { get; set; }
    }

    public class EmployeeData
    {
        public string SECT_ID { get; set; }
        public string SECT_NAME { get; set; }
        public string EMP_ID { get; set; }
        public string EMP_NAME { get; set; }
    }

    public class SoftwareVendor
    {
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public string DATE_RENEWAL { get; set; }
        public int ID_VENDOR { get; set; }
        public string NAME_VENDOR { get; set; }
    }

    public class SoftwareCost
    {
        public int ID_COST { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public decimal CAD_COST { get; set; }
        public decimal REQ_COST { get; set; }
    }

    public class SoftwareServer
    {
        public int ID_SOFTWARE_SERVER { get; set; }
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public string LICENSE_STATUS { get; set; }
        public string DEPT { get; set; }
        public int QUANTITY { get; set; }
        public int ID_SERVER { get; set; }
        public string IP_SERVER { get; set; }
    }

    public class CADcostHelper
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";

        public static List<CADcostItem> getItem(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<CADcostItem> result = new List<CADcostItem>();
            Func<string, int, decimal, string, string, string, CADcostItem> makeCADcostItem = (p1, p2, p3, p4, p5, p6) => new CADcostItem {NAME_SOFTWARE = p1, QUANTITY = p2, PRICE = p3, STATUS = p4, IP_SERVER = p5, REMARK = p6 };
                 
            result = (from price in data.SOFTWARE_COSTs group price by price.ID_SOFTWARE into pricegroup 
                    join item in data.CADDETAILs on pricegroup.Key equals item.ID_SOFTWARE
                    join software in data.SOFTWAREs on item.ID_SOFTWARE equals software.ID_SOFTWARE
                    join softserver in data.SOFTWARE_SERVERs on software.ID_SOFTWARE equals softserver.ID_SOFTWARE
                    join server in data.SERVERs on softserver.ID_SERVER equals server.ID_SERVER
                    where item.ID_CADREQ == id_cadreq && softserver.LAST_UPDATED.Month == DateTime.Now.Month && softserver.LAST_UPDATED.Year == DateTime.Now.Year
                    select makeCADcostItem(software.NAME_SOFTWARE, item.QUANTITY, Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_COST), (Convert.ToBoolean(softserver.LICENSE_STATUS) == true ? "Owned" : "Additional"), server.IP_SERVER.ToString(), item.REMARK)).ToList();

            return result;
        }

        public static List<ApproveENIT> getCADreq(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<ApproveENIT> result = new List<ApproveENIT>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, string, string, ApproveENIT> makeApproveENIT = (p1, p2, p3, p4, p5, p6, p7, p8) => new ApproveENIT {ID_CADREQ=p1, CADREQ_NO = p2, FULL_NAME=p3, SECT_ID=p4, DATE_REQUEST=p5, TIME_REQUEST=p6, NAME_ACTIVITY=p7, DESCRIPTION=p8};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID=p1, SECT_NAME=p2, EMP_ID=p3, EMP_NAME=p4 };

            tempEmployee =(from employee in data2.TBL_EMPs
                           join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                           let sect = section.SECT_NAME.Split('-') 
                           select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();

            result = (from employee in tempEmployee
                      join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.CADREQ_BY
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(2)
                      orderby cadreq.TIME_REQUEST descending
                      select makeApproveENIT(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.DESCRIPTION)).ToList();
            
            return result;
        }

        public static string getLastActivityComment(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            string comment="";
            comment = (from activity in data.ACTION_HISTORies
                       group activity by activity.ID_CADREQ into timeact
                       where timeact.Key.Equals(id_cadreq)
                       select timeact.OrderByDescending(x => x.TIME_END).First().COMMENT).ToString();
            return comment;
        }

        public static List<LicenseHistory> getLicenseHistory(string jobcode_id, Employee emp_data)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<LicenseHistory> result = new List<LicenseHistory>();
            Func<int, string, string, string, string, string, string, LicenseHistory> makeLicenseHistory = (p1, p2, p3, p4, p5, p6, p7) => new LicenseHistory { ID_CADREQ=p1, CADREQ_NO = p2, DATE_REQUEST = p3, TIME_REQUEST = p4, NAME_ACTIVITY = p5, DESCRIPTION=p6, COMMENT=p7};
            result = (from act in data.ACTION_HISTORies group act by act.ID_CADREQ into timeact
                      join cadreq in data.CADREQs on timeact.Key equals cadreq.ID_CADREQ
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.JOBCODE.Equals(jobcode_id) && (cadreq.CADREQ_BY.Equals(emp_data.EMP_ID) || cadreq.CADREQ_BY.Equals(emp_data.PARTICIPANT_ID))
                      orderby cadreq.TIME_REQUEST descending
                      select makeLicenseHistory(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.DESCRIPTION, timeact.OrderByDescending(x => x.TIME_END).First().COMMENT)).ToList();
            return result;
        }

        public static List<ActionHistoryDetail> getActionHistory(string jobcode_id, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<ActionHistoryDetail> acthistory = new List<ActionHistoryDetail>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();
            Func<int, string, string, string, string, string, string, string, string, ActionHistoryDetail> makeActionHistoryDetail = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new ActionHistoryDetail { ID_CADREQ=p1, CADREQ_NO = p2, CADREQ_BY = p3, DEPT = p4, TIME_REQUEST = p5, NAME_ACTION = p6, TIME_END = p7, DESCRIPTION=p8, COMMENT=p9};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };
            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            acthistory = (from employee in tempEmployee
                          join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.CADREQ_BY
                          join act in data.ACTION_HISTORies on cadreq.ID_CADREQ equals act.ID_CADREQ 
                          join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                          where (act.PARTICIPANT.ToString().Equals(emp_id) || act.COMPLETED_BY.ToString().Equals(emp_id)) && act.JOBCODE.Equals(jobcode_id)
                          orderby act.TIME_END descending
                          select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT)).ToList();

            return acthistory;
        }

        public static void approveRequest(int id_cadreq, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = (from req in data.CADREQs
                            where req.ID_CADREQ.Equals(id_cadreq)
                            select req).FirstOrDefault();

            cadreq.ID_ACTIVITY = 4;
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.LASTACTIVITY_BY = emp_id;
            data.SubmitChanges();
        }

        public static void insertActionHistory(ACTION_HISTORY act)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.ACTION_HISTORies.InsertOnSubmit(act);
            data.SubmitChanges();
        }

        public static int getLastCADreqNO()
        {
            CADcostDataContext data = new CADcostDataContext();
            try
            {
                return (int)data.CADREQs.OrderByDescending(x => x.ID_CADREQ).Select(x => x.ID_CADREQ).First();
            }
            catch
            {
                return 1;
            }
        }

        public static string generateCADreqNO(string sectcode)
        {
            CADcostDataContext data = new CADcostDataContext();
            return getLastCADreqNO().ToString() + '/' + sectcode + '/' + DateTime.Now.Month.ToString() + '/' + DateTime.Now.Year.ToString();
        }

        public static decimal generateTotalItemAmount(List<CADcostItem> items, string jobcode)
        {
            
            List<CADcostItem> result = new List<CADcostItem>();
            CADcostDataContext data = new CADcostDataContext();
            Func<int, decimal, CADcostItem> makeCADcostItem = (p1, p2) => new CADcostItem {ID_SOFTWARE = p1, PRICE = p2};
            result = (from itemcounted in items
                      join cost in data.SOFTWARE_COSTs on itemcounted.ID_SOFTWARE equals cost.ID_SOFTWARE
                      group cost by new { cost.ID_SOFTWARE, cost.JOBCODE_ID } into pricegroup
                      where pricegroup.Key.JOBCODE_ID.Equals(jobcode)
                      select makeCADcostItem(Convert.ToInt32(pricegroup.Key.ID_SOFTWARE), Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_COST))).ToList();
            
            List<CADcostItem> result2 = new List<CADcostItem>();
            result2 = (from result1 in result
                       join item in items on result1.ID_SOFTWARE equals item.ID_SOFTWARE
                       select makeCADcostItem(result1.ID_SOFTWARE, item.QUANTITY * result1.PRICE)).ToList();
            decimal total = 0;
            foreach (CADcostItem item in result2)
            {
                total += item.PRICE;
            }

            return total;
        }

        public static void generateDDLSoftwareName(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SOFTWARE> software = (from sfwr in data.SOFTWAREs select sfwr).ToList();
            foreach (SOFTWARE sw in software)
            {
                ddl.Items.Add(new ListItem(sw.NAME_SOFTWARE.ToString(), sw.ID_SOFTWARE.ToString()));
            }
        }

        public static void generateDDLVendorName(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<VENDOR> vendor = (from vndr in data.VENDORs select vndr).ToList();
            foreach (VENDOR vndr in vendor)
            {
                ddl.Items.Add(new ListItem(vndr.NAME_VENDOR.ToString(), vndr.ID_VENDOR.ToString()));
            }
        }

        public static void generateDDLServer(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SERVER> server = (from srvr in data.SERVERs select srvr).ToList();
            foreach (SERVER srvr in server)
            {
                ddl.Items.Add(new ListItem(srvr.IP_SERVER.ToString(), srvr.ID_SERVER.ToString()));
            }
        }

        public static List<SoftwareVendor> getAllSoftwareVendor()
        {
            CADcostDataContext data = new CADcostDataContext();
            List <SoftwareVendor> softwareVendor = new List<SoftwareVendor>();
            Func<int, string, string, int, string, SoftwareVendor> makeSoftwareVendor = (p1, p2, p3, p4, p5) => new SoftwareVendor {ID_SOFTWARE=p1, NAME_SOFTWARE=p2, DATE_RENEWAL=p3, ID_VENDOR=p4, NAME_VENDOR=p5};
            softwareVendor = (from software in data.SOFTWAREs
                              join sfwrVendor in data.SOFTWARE_VENDORs on software.ID_SOFTWARE equals sfwrVendor.ID_SOFTWARE
                              join vendor in data.VENDORs on sfwrVendor.ID_VENDOR equals vendor.ID_VENDOR
                              select makeSoftwareVendor(software.ID_SOFTWARE, software.NAME_SOFTWARE, software.DATE_RENEWAL.ToString(), vendor.ID_VENDOR, vendor.NAME_VENDOR)).ToList();
            return softwareVendor;
        }

        public static List <SoftwareCost> getAllSoftwareCost(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            List <SoftwareCost> softwareCost = new List<SoftwareCost>();
            Func<int, string, decimal, decimal, SoftwareCost> makeSoftwareCost = (p1, p2, p3, p4) => new SoftwareCost { ID_COST = p1, NAME_SOFTWARE = p2, CAD_COST = p3, REQ_COST = p4 };
            softwareCost = (from software in data.SOFTWAREs
                              join sfwrCost in data.SOFTWARE_COSTs on software.ID_SOFTWARE equals sfwrCost.ID_SOFTWARE
                              where sfwrCost.JOBCODE_ID.Equals(jobcode_id)
                            select makeSoftwareCost(sfwrCost.ID_COST, software.NAME_SOFTWARE,Convert.ToDecimal(sfwrCost.CAD_COST),Convert.ToDecimal(sfwrCost.REQ_COST))).ToList();
            return softwareCost;
        }

        public static List <SoftwareServer> getAllSoftwareServer(string jobcode_id, int month, int year)
        {
            CADcostDataContext data = new CADcostDataContext();
            List <SoftwareServer> softwareServer = new List<SoftwareServer>();
            Func<int, int, string, string, string, int, int, string, SoftwareServer> makeSoftwareServer = (p1, p2, p3, p4, p5, p6, p7, p8) => new SoftwareServer { ID_SOFTWARE_SERVER=p1, ID_SOFTWARE=p2, NAME_SOFTWARE=p3, LICENSE_STATUS=p4, DEPT=p5, QUANTITY=p6, ID_SERVER=p7, IP_SERVER=p8 };
            softwareServer = (from software in data.SOFTWAREs
                            join sfwrServer in data.SOFTWARE_SERVERs on software.ID_SOFTWARE equals sfwrServer.ID_SOFTWARE
                            join server in data.SERVERs on sfwrServer.ID_SERVER equals server.ID_SERVER
                            where sfwrServer.LAST_UPDATED.Month.Equals(month) && sfwrServer.LAST_UPDATED.Year.Equals(year) && sfwrServer.JOBCODE_ID.Equals(jobcode_id)
                            select makeSoftwareServer(Convert.ToInt32(sfwrServer.ID_SOFTWARE_SERVER), Convert.ToInt32(sfwrServer.ID_SOFTWARE), software.NAME_SOFTWARE, (Convert.ToBoolean(sfwrServer.LICENSE_STATUS) == true ? "Owned" : "Additional"), sfwrServer.DEPT, sfwrServer.QUANTITY, server.ID_SERVER, server.IP_SERVER)).ToList();
            return softwareServer;
        }

        public static List<VENDOR> getAllVendor()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<VENDOR> vendor = new List<VENDOR>();
            vendor = (from vndr in data.VENDORs select vndr).ToList();
            return vendor;
        }

        public static List<SERVER> getAllServer()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SERVER> server = new List<SERVER>();
            server = (from srvr in data.SERVERs select srvr).ToList();
            return server;
        }

        public static void SubmitCADreq(string dept, CADREQ req, string jobcode_id, List<CADcostItem> items)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.CADREQs.InsertOnSubmit(req);
            data.SubmitChanges();
            InsertCADItem(dept, req.ID_CADREQ, jobcode_id, items);
        }

        public static void InsertCADItem(string dept, int id_cadreq, string jobcode_id, List<CADcostItem> items)
        {
            //Func<int, int, bool, int, DateTime, string, 
            CADcostDataContext data = new CADcostDataContext();
            CADDETAIL cd = new CADDETAIL();
            foreach (CADcostItem cci in items)
            {
                cd = new CADDETAIL();
                cd.ID_CADREQ = id_cadreq;
                cd.ID_SOFTWARE = cci.ID_SOFTWARE;
                cd.QUANTITY = cci.QUANTITY;
                data.CADDETAILs.InsertOnSubmit(cd);
                data.SubmitChanges();

                SOFTWARE_SERVER ss = new SOFTWARE_SERVER();
                ss = (from softsrv in data.SOFTWARE_SERVERs
                     where softsrv.DEPT.Contains(dept) && softsrv.ID_SOFTWARE.Equals(cci.ID_SOFTWARE) && softsrv.JOBCODE_ID.Equals(jobcode_id)
                     select softsrv).FirstOrDefault() as SOFTWARE_SERVER;

                ss.QUANTITY = cci.QUANTITY;
                ss.LAST_UPDATED = DateTime.Now;
                data.SubmitChanges();
            }
        }

        public static int getLastIDSoftware()
        {
            CADcostDataContext data = new CADcostDataContext();
            return (int) data.SOFTWAREs.OrderByDescending(x => x.ID_SOFTWARE).Select(x => x.ID_SOFTWARE).FirstOrDefault();
        }

        public static void insertSoftware(SOFTWARE software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SOFTWAREs.InsertOnSubmit(software);
            data.SubmitChanges();
            SOFTWARE_VENDOR sfwrVndr = new SOFTWARE_VENDOR();
            sfwrVndr.ID_SOFTWARE = getLastIDSoftware();
            sfwrVndr.ID_VENDOR = id_vendor;
            data.SOFTWARE_VENDORs.InsertOnSubmit(sfwrVndr);
            data.SubmitChanges();
        }

        public static void updateSoftware(SOFTWARE software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE sfwr = new SOFTWARE();
            sfwr = (from sfwr2 in data.SOFTWAREs
                   where sfwr2.ID_SOFTWARE.Equals(software.ID_SOFTWARE)
                   select sfwr2).FirstOrDefault();
            sfwr.NAME_SOFTWARE = software.NAME_SOFTWARE;
            sfwr.DATE_RENEWAL = software.DATE_RENEWAL;
            data.SubmitChanges();
            SOFTWARE_VENDOR sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor = (from sfwrVendor2 in data.SOFTWARE_VENDORs
                          where sfwrVendor2.ID_SOFTWARE.Equals(software.ID_SOFTWARE)
                          select sfwrVendor2).FirstOrDefault();
            data.SOFTWARE_VENDORs.DeleteOnSubmit(sfwrVendor);
            data.SubmitChanges();
            sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor.ID_SOFTWARE = software.ID_SOFTWARE;
            sfwrVendor.ID_VENDOR = id_vendor;
            data.SOFTWARE_VENDORs.InsertOnSubmit(sfwrVendor);
            data.SubmitChanges();
        }

        public static void deleteSoftware(int id_software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_VENDOR sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor = (from softwareVendor in data.SOFTWARE_VENDORs
                          where softwareVendor.ID_SOFTWARE.Equals(id_software)
                          select softwareVendor).FirstOrDefault();
            data.SOFTWARE_VENDORs.DeleteOnSubmit(sfwrVendor);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE_VENDOR" + ", RESEED)");
            SOFTWARE sfwr = new SOFTWARE();
            sfwr = (from software in data.SOFTWAREs
                    where software.ID_SOFTWARE.Equals(id_software)
                    select software).FirstOrDefault();
            data.SOFTWAREs.DeleteOnSubmit(sfwr);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE" + ", RESEED)");
        }

        public static void insertVendor(VENDOR vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.VENDORs.InsertOnSubmit(vendor);
            data.SubmitChanges();
        }

        public static void updateVendor(VENDOR vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            VENDOR vndr = new VENDOR();
            vndr = (from vndr2 in data.VENDORs
                    where vndr2.ID_VENDOR.Equals(vendor.ID_VENDOR)
                    select vndr2).FirstOrDefault();
            vndr.NAME_VENDOR = vendor.NAME_VENDOR;
            vndr.CONTACT = vendor.CONTACT;
            vndr.EMAIL = vendor.EMAIL;
            vndr.WEBSITE = vendor.WEBSITE;
            vndr.VENDOR_DESCRIPTION = vendor.VENDOR_DESCRIPTION;
            data.SubmitChanges();
        }

        public static void deleteVendor(int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            VENDOR vendor = new VENDOR();
            vendor = (from vendor2 in data.VENDORs
                          where vendor2.ID_VENDOR.Equals(id_vendor)
                          select vendor2).FirstOrDefault();
            data.VENDORs.DeleteOnSubmit(vendor);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "VENDOR" + ", RESEED)");
        }

        public static void insertServer(SERVER server)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SERVERs.InsertOnSubmit(server);
            data.SubmitChanges();
        }

        public static void updateServer(SERVER server)
        {
            CADcostDataContext data = new CADcostDataContext();
            SERVER srvr = new SERVER();
            srvr = (from srvr2 in data.SERVERs
                    where srvr2.ID_SERVER.Equals(server.ID_SERVER)
                    select srvr2).FirstOrDefault();
            srvr.SERVER_NAME = server.SERVER_NAME;
            srvr.IP_SERVER = server.IP_SERVER;
            data.SubmitChanges();
        }

        public static void deleteServer(int id_server)
        {
            CADcostDataContext data = new CADcostDataContext();
            SERVER server = new SERVER();
            server = (from server2 in data.SERVERs
                      where server2.ID_SERVER.Equals(id_server)
                      select server2).FirstOrDefault();
            data.SERVERs.DeleteOnSubmit(server);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT ("+"SERVER"+", RESEED)");
        }

        public static void insertSoftwareServer(SOFTWARE_SERVER sfwrServer)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SOFTWARE_SERVERs.InsertOnSubmit(sfwrServer);
            data.SubmitChanges();
        }

        public static void updateSoftwareServer(SOFTWARE_SERVER sfwrServer, string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_SERVER sfwrServer2 = new SOFTWARE_SERVER();
            sfwrServer2 = (from softwareServer in data.SOFTWARE_SERVERs
                           where softwareServer.ID_SOFTWARE_SERVER.Equals(sfwrServer.ID_SOFTWARE_SERVER)
                           select softwareServer).FirstOrDefault();
            data.SOFTWARE_SERVERs.DeleteOnSubmit(sfwrServer2);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE_SERVER" + ", RESEED)");
            sfwrServer2 = new SOFTWARE_SERVER();
            sfwrServer2.ID_SOFTWARE_SERVER = null;
            sfwrServer2.ID_SERVER = sfwrServer.ID_SERVER;
            sfwrServer2.ID_SOFTWARE = sfwrServer.ID_SOFTWARE;
            sfwrServer2.DEPT = sfwrServer.DEPT;
            sfwrServer2.QUANTITY = sfwrServer.QUANTITY;
            sfwrServer2.JOBCODE_ID = jobcode_id;
            sfwrServer2.LAST_UPDATED = sfwrServer.LAST_UPDATED;
            data.SOFTWARE_SERVERs.InsertOnSubmit(sfwrServer2);
            data.SubmitChanges();
        }

        public static void deleteSoftwareServer(int id_software_server)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_SERVER sfwrServer = new SOFTWARE_SERVER();
            sfwrServer = (from softwareServer in data.SOFTWARE_SERVERs
                          where softwareServer.ID_SOFTWARE_SERVER.Equals(id_software_server)
                          select softwareServer).FirstOrDefault();
            data.SOFTWARE_SERVERs.DeleteOnSubmit(sfwrServer);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE_SERVER" + ", RESEED)");
        }

        public static void insertImpersonate(string winCredential, string impersonate)
        {
            CADcostDataContext data = new CADcostDataContext();
            ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
            if (a != null)
            {
                a.IMPERSONATE = impersonate;
                data.SubmitChanges();
            }
        }

        public static void releaseImpersonate(string winCredential)
        {
            CADcostDataContext data = new CADcostDataContext();
            ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
            if (a != null)
            {
                a.IMPERSONATE = null;
                data.SubmitChanges();
            }
        }

        public static bool isImpersonate(string winCredential, out string impersonate, out bool isAdmin)
        {
            CADcostDataContext data = new CADcostDataContext(Connection.CADcostConn);
            isAdmin = CADcostHelper.isAdmin(winCredential);
            impersonate = "";
            if (data.ADMINs.Where(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower())).Any())
            {
                ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
                
                if (a.IMPERSONATE != null)
                {
                    impersonate = a.IMPERSONATE;
                    isAdmin = data.ADMINs.Any(x => x.EMP_ID == a.IMPERSONATE);
                    return true;
                }

            }

            return false;
        }

        public static bool isAdmin(string winCredential)
        {
            CADcostDataContext data = new CADcostDataContext();

            return data.ADMINs.Any(x => x.WindowsCredential.ToLower().Contains(winCredential.ToLower()));
        }
    }
}