﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class CAD : System.Web.UI.MasterPage
    {
        Employee logInUser = new Employee();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonImpersonate_Click(object sender, EventArgs e)
        {
            CADcostHelper.insertImpersonate(Request.LogonUserIdentity.Name, TextBoxImpersonate.Text);
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            LabelLogin.Text = logInUser.FULL_NAME;
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonRelease_Click(object sender, EventArgs e)
        {
            CADcostHelper.releaseImpersonate(Request.LogonUserIdentity.Name);
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            LabelLogin.Text = logInUser.FULL_NAME;
            Response.Redirect(Request.RawUrl);
        }
    }
}