﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class Software_server : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                PFNATTHelper.generateDDLProjectName(DropDownListProject);
                DropDownListMonth.Items.Add(new ListItem("January", "1"));
                DropDownListMonth.Items.Add(new ListItem("February", "2"));
                DropDownListMonth.Items.Add(new ListItem("March", "3"));
                DropDownListMonth.Items.Add(new ListItem("April", "4"));
                DropDownListMonth.Items.Add(new ListItem("May", "5"));
                DropDownListMonth.Items.Add(new ListItem("June", "6"));
                DropDownListMonth.Items.Add(new ListItem("July", "7"));
                DropDownListMonth.Items.Add(new ListItem("August", "8"));
                DropDownListMonth.Items.Add(new ListItem("September", "9"));
                DropDownListMonth.Items.Add(new ListItem("October", "10"));
                DropDownListMonth.Items.Add(new ListItem("November", "11"));
                DropDownListMonth.Items.Add(new ListItem("December", "12"));
                DropDownListYear.Items.Add(new ListItem("2015", "2015"));
                DropDownListYear.Items.Add(new ListItem("2016", "2016"));
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                }
                if (Session["month"] != null)
                {
                    DropDownListMonth.SelectedValue = Session["month"].ToString();
                }
                if (Session["year"] != null)
                {
                    DropDownListYear.SelectedValue = Session["year"].ToString();
                }

                GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
                GridViewSoftwareServer.DataBind();
                GridViewServerList.DataSource = CADcostHelper.getAllServer();
                GridViewServerList.DataBind();
                CADcostHelper.generateDDLSoftwareName(DropDownListSoftware);
                PFNATTHelper.generateDDLDepartment(DropDownListDepartment);
                CADcostHelper.generateDDLServer(DropDownListServer);
            }
            else
            {
                /*
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                    Session["jobcode"] = null;
                }
                if (Session["month"] != null)
                {
                    DropDownListMonth.SelectedValue = Session["month"].ToString();
                    Session["month"] = null;
                }
                if (Session["year"] != null)
                {
                    DropDownListYear.SelectedValue = Session["year"].ToString();
                    Session["year"] = null;
                }
                GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
                GridViewSoftwareServer.DataBind();
                GridViewServerList.DataSource = CADcostHelper.getAllServer();
                GridViewServerList.DataBind();
                 * */
            }
        }

        protected void ButtonAddLicenseServer_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertAddLicenseServer", "opendialog('dialogAddLicenseServer');", true);
            PanelAddLicenseServer.Visible = true;
        }

        protected void ButtonAddLicenseServerOK_Click(object sender, EventArgs e)
        {
            SOFTWARE_SERVER sfwrServer = new SOFTWARE_SERVER();
            sfwrServer.ID_SOFTWARE = Convert.ToInt32(DropDownListSoftware.SelectedValue.ToString());
            sfwrServer.DEPT = DropDownListDepartment.SelectedValue.ToString();
            sfwrServer.ID_SERVER = Convert.ToInt32(DropDownListServer.SelectedValue.ToString());
            sfwrServer.JOBCODE_ID = DropDownListProject.SelectedValue.ToString();
            sfwrServer.LAST_UPDATED = DateTime.Now;
            sfwrServer.LICENSE_STATUS = false;
            sfwrServer.QUANTITY = 0;
            CADcostHelper.insertSoftwareServer(sfwrServer);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseAddLicenseServer", "closedialog('dialogAddLicenseServer');", true);
            PanelAddLicenseServer.Visible = false;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonAddLicenseServerCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseAddLicenseServer", "closedialog('dialogAddLicenseServer');", true);
            PanelAddLicenseServer.Visible = false;
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["jobcode"] = DropDownListProject.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void DropDownListMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["month"] = DropDownListMonth.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void DropDownListYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Session["year"] = DropDownListYear.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void ButtonAddServer_Click(object sender, EventArgs e)
        {
            GridViewRow row = GridViewServerList.FooterRow;
            TextBox tbServerName = row.FindControl("TextBoxServerNameAdd") as TextBox;
            TextBox tbIPServer = row.FindControl("TextBoxIPServerAdd") as TextBox;
            SERVER server = new SERVER();
            server.SERVER_NAME = tbServerName.Text;
            server.IP_SERVER = tbIPServer.Text;
            CADcostHelper.insertServer(server);
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewServerList.EditIndex = index;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewServerList.Rows[e.RowIndex];
            HiddenField hfIDServer = row.FindControl("HiddenFieldIDServer") as HiddenField;
            CADcostHelper.deleteServer(Convert.ToInt32(hfIDServer.Value));
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewServerList.Rows[e.RowIndex];
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDServerEdit") as HiddenField;
            TextBox tbServerName = row.FindControl("TextBoxServerName") as TextBox;
            TextBox tbIPServer = row.FindControl("TextBoxIPServer") as TextBox;
            SERVER srvr = new SERVER();
            srvr.ID_SERVER = Convert.ToInt32(hfIDVendor.Value);
            srvr.SERVER_NAME = tbServerName.Text;
            srvr.IP_SERVER = tbIPServer.Text;
            CADcostHelper.updateServer(srvr);
            GridViewServerList.EditIndex = -1;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewServerList.EditIndex = -1;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewSoftwareServer_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewSoftwareServer.EditIndex = index;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewSoftwareServer.Rows[e.RowIndex];
            HiddenField hfIDSoftwareServer = row.FindControl("HiddenFieldIDSoftwareServer") as HiddenField;
            CADcostHelper.deleteSoftwareServer(Convert.ToInt32(hfIDSoftwareServer.Value));
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewSoftwareServer.Rows[e.RowIndex];
            HiddenField hfIDSoftwareServer = row.FindControl("HiddenFieldIDSoftwareServer") as HiddenField;
            HiddenField hfIDSoftware = row.FindControl("HiddenFieldIDSoftware") as HiddenField;
            Label lblLicenseStatus = row.FindControl("LabelLicenseStatus") as Label;
            Label lblDEPT = row.FindControl("LabelDepartment") as Label;
            Label lblQuantity = row.FindControl("LabelQuantity") as Label;
            DropDownList ddlServer = row.FindControl("DropDownListServer") as DropDownList;
            SOFTWARE_SERVER srvr = new SOFTWARE_SERVER();
            srvr.ID_SOFTWARE_SERVER = Convert.ToInt32(hfIDSoftwareServer.Value);
            srvr.ID_SOFTWARE = Convert.ToInt32(hfIDSoftware.Value);
            if (lblLicenseStatus.Text.Equals("Owned"))
                srvr.LICENSE_STATUS = true;
            else srvr.LICENSE_STATUS = false;
            srvr.DEPT = lblDEPT.Text;
            srvr.QUANTITY = Convert.ToInt32(lblQuantity.Text);
            srvr.ID_SERVER = Convert.ToInt32(ddlServer.SelectedValue);
            srvr.LAST_UPDATED = DateTime.Now;
            CADcostHelper.updateSoftwareServer(srvr, DropDownListProject.SelectedValue.ToString());
            GridViewSoftwareServer.EditIndex = -1;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewSoftwareServer.EditIndex = -1;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServer(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                SoftwareServer itemedit = e.Row.DataItem as SoftwareServer;
                DropDownList ddl = e.Row.FindControl("DropDownListServer") as DropDownList;
                CADcostHelper.generateDDLServer(ddl);
                ddl.SelectedValue = itemedit.ID_SERVER.ToString();
            }
        }
    }
}