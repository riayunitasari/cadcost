﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Beranda.aspx.cs" Inherits="CADcost.Beranda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/stylesheet">
        #contents {
        color: black;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="overlow post-holder-sm extra">
    <div class="date"> 
        <span class="day"><asp:Label ID="lblDate" runat="server"></asp:Label></span>
        <span class="month"><asp:Label ID="lblMonth" runat="server"></asp:Label></span>
    </div>
        <div id="contents">
        <h5><asp:Label ID="lblUsername" runat="server" Text="Welcome "></asp:Label></h5>
        </div>
    </div>

</asp:Content>
