﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class Software_cost : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                PFNATTHelper.generateDDLProjectName(DropDownListProject);
                GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(DropDownListProject.SelectedValue.ToString());
                GridViewSoftwareCost.DataBind();
            }
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(DropDownListProject.SelectedValue.ToString());
            GridViewSoftwareCost.DataBind();
        }

        protected void ButtonAddCost_Click(object sender, EventArgs e)
        {

        }
    }
}