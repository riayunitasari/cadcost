﻿using CADcost.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;
using System.Data;

namespace CADcost
{
    public partial class CADcost_create : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        DateTime timeStart = DateTime.Now;
        
        /*
        private List<Dictionary<string,object>> CurrentCADDETAIL
        {
            get
            {
                return ((SViewState<List<Dictionary<string, object>>>)ViewState["curItem"]).ViewState;
            }
            set
            {
                ViewState["curItem"] = new SViewState<List<Dictionary<string, object>>>(value);
            }
        }
        */

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                int id_cadreq = Helper.CADcostHelper.getLastCADreqNO()+1;
                string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).ToString();
                LabelCADcostNO.Text = Helper.CADcostHelper.generateCADreqNO(sectcode);
                LabelDepartment.Text = logInUser.SECT_NAME;
                Helper.PFNATTHelper.generateDDLProjectName(DropDownListProject);
                
                onViewState(id_cadreq);
                if (Request["act"] != null)
                {
                    string act = Request["act"].ToString();
                    if (act.Equals("edit"))
                    {
                        DropDownListProject.Visible = false;
                    }
                }

                
            }
        }

        private void onViewState(int id_cadreq)
        {
            loadGridViewItem(id_cadreq);
        }

        private void loadGridViewItem(int id_cadreq)
        {
            /*
            //List<Dictionary<string, object>> items = CurrentCADDETAIL;
            List<CADcostItem> items = Helper.CADcostHelper.getItem(id_cadreq, logInUser.SECT_ID);
            
            if (items.Count > 0)
            {
                GridViewItem.DataSource = items;
                GridViewItem.DataBind();
            }
            else
            {
                
            }
            */
            DataTable dt = new DataTable();
            dt.Columns.Add("No.", typeof(int));
            dt.Columns.Add("NAME_SOFTWARE", typeof(string));
            dt.Columns.Add("QUANTITY", typeof(string));
            dt.Columns.Add("REMARK", typeof(string));
            DataRow dtRow = dt.NewRow();
            dt.Rows.Add(dtRow);

            GridViewItem.DataSource = dt;
            GridViewItem.DataBind();

            GridViewItem.Rows[0].Visible = false;

            /*
            GridViewRow row = GridViewItem.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareNameAdd") as DropDownList;
            Helper.CADcostHelper.generateDDLSoftwareName(ddlSoftware);*/
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            GridViewRow row = GridViewItem.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareNameAdd") as DropDownList;
            TextBox tbQuantity = row.FindControl("TextBoxQuantityAdd") as TextBox;
            TextBox tbRemark = row.FindControl("TextBoxRemarkAdd") as TextBox;
            CADcostItem item = new CADcostItem(Convert.ToInt32(ddlSoftware.SelectedValue), ddlSoftware.SelectedItem.Text, Convert.ToInt32(tbQuantity.Text), tbRemark.Text);
            List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
            if (items == null) items = new List<CADcostItem>();
            items.Add(item);
            ViewState[curItem] = items;
            GridViewItem.DataSource = items;
            GridViewItem.DataBind();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertSummary", "opendialog('dialogSummary');", true);
            UpdatePanelSummary.Visible = true;
            GridViewItemSummary.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItemSummary.DataBind();
        }

        protected void ButtonSaveAsDraft_Click(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            CADREQ req = new CADREQ();
            req.ID_CADREQ = CADcostHelper.getLastCADreqNO();
            req.CADREQ_BY = logInUser.EMP_ID;
            req.ID_ACTIVITY = 1;
            string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).ToString();
            req.CADREQ_NO = CADcostHelper.generateCADreqNO(sectcode);
            req.FOR_MONTH = DateTime.Now.AddMonths(1);
            req.DESCRIPTION = TextBoxDescription.Text;
            req.JOBCODE = DropDownListProject.SelectedValue;
            req.LASTACTIVITY_BY = logInUser.EMP_ID;
            req.PIC = logInUser.EMP_ID;
            req.TIME_LASTACTIVITY = DateTime.Now;
            req.TIME_REQUEST = DateTime.Now;
            List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
            req.TOTAL_AMOUNT = CADcostHelper.generateTotalItemAmount(items, DropDownListProject.SelectedValue);
            CADcostHelper.SubmitCADreq(logInUser.SECT_ID, req, DropDownListProject.SelectedValue.ToString(), items);
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonConfirmOK_Click(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            CADREQ req = new CADREQ();
            req.CADREQ_BY = logInUser.EMP_ID;
            req.ID_ACTIVITY = 2;
            string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).Trim().ToString();
            req.CADREQ_NO = CADcostHelper.generateCADreqNO(sectcode);
            req.FOR_MONTH = DateTime.Now.AddMonths(1);
            req.DESCRIPTION = TextBoxDescription.Text;
            req.JOBCODE = DropDownListProject.SelectedValue;
            req.LASTACTIVITY_BY = logInUser.EMP_ID;
            req.PIC = logInUser.EMP_ID;
            req.TIME_LASTACTIVITY = DateTime.Now;
            req.TIME_REQUEST = DateTime.Now;
            List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
            req.TOTAL_AMOUNT = CADcostHelper.generateTotalItemAmount(items, DropDownListProject.SelectedValue);
            CADcostHelper.SubmitCADreq(logInUser.SECT_ID, req, DropDownListProject.SelectedValue.ToString(), items);


            ACTION_HISTORY act = new ACTION_HISTORY();
            act.ID_ACTION = 1; //submit request
            act.ID_ACTIVITY = 2; //fix request
            act.ID_CADREQ = CADcostHelper.getLastCADreqNO();
            act.JOBCODE = DropDownListProject.SelectedValue;
            act.TIME_START = timeStart;
            act.TIME_END = DateTime.Now;
            act.COMMENT = TextBoxComment.Text;
            if (logInUser.ISPARTICIPANT)
            {
                act.PARTICIPANT = logInUser.EMP_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;

            }
            else
            {
                act.PARTICIPANT = logInUser.PARTICIPANT_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
                act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
            }

            CADcostHelper.insertActionHistory(act);

            Session["jobcode"] = DropDownListProject.SelectedValue;
            var url = "~/CADcost_record.aspx";
            Response.Redirect(url);
            
        }

        protected void ButtonConfirmCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSummary", "closedialog('dialogSummary');", true);
        }

        protected void GridViewItem_DataBound(object sender, EventArgs e)
        {
            GridViewRow row = GridViewItem.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareNameAdd") as DropDownList;
            Helper.CADcostHelper.generateDDLSoftwareName(ddlSoftware);
        }

        protected void GridViewItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewItem.EditIndex = index;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void GridViewItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            List<CADcostItem> items =(List<CADcostItem>) ViewState[curItem];
            GridViewRow row = GridViewItem.Rows[e.RowIndex];
            DropDownList ddl = row.FindControl("DropDownListSoftwareNameEdit") as DropDownList;
            TextBox tbQtty = row.FindControl("TextBoxQuantityEdit") as TextBox;
            TextBox tbRemark = row.FindControl("TextBoxRemarkEdit") as TextBox;
            items[e.RowIndex].ID_SOFTWARE =Convert.ToInt32(ddl.SelectedValue);
            items[e.RowIndex].NAME_SOFTWARE = ddl.SelectedItem.ToString();
            items[e.RowIndex].QUANTITY = Convert.ToInt32(tbQtty.Text);
            items[e.RowIndex].REMARK = tbRemark.Text;
            ViewState[curItem] = items;
            GridViewItem.EditIndex = -1;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void GridViewItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void GridViewItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            ((List <CADcostItem>) ViewState[curItem]).RemoveAt(e.RowIndex);
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void GridViewItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                CADcostItem itemedit = e.Row.DataItem as CADcostItem;
                DropDownList ddl = e.Row.FindControl("DropDownListSoftwareNameEdit") as DropDownList;
                CADcostHelper.generateDDLSoftwareName(ddl);
                ddl.SelectedValue = itemedit.ID_SOFTWARE.ToString();
            }
            

        }

        protected void GridViewItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewItem.EditIndex = -1;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        
    }
}