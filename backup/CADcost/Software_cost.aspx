﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_cost.aspx.cs" Inherits="CADcost.Software_cost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        function closedialog(dialogname) {
            $("#" + dialogname).dialog("close");
        }

        function opendialog(dialogname) {
            $("#" + dialogname).dialog({
                title: "Summary",
                modal: true,
                resizable: false,
                width: 'auto',
                create: function (event, ui) { }
            });
            $("#" + dialogname).parent().appendTo($("#form1"));
        }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="80%"></asp:DropDownList>
    <h2>Software Cost</h2>
    <asp:GridView ID="GridViewSoftwareCost" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddCost" runat="server" Text="Add" OnClick="ButtonAddCost_Click"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Software">
                <ItemTemplate>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="DropDownListSoftware" runat="server"></asp:DropDownList>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelCADCost" runat="server" Text='<%# Eval("CAD_COST") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDCost" runat="server"  Value='<%# Eval("ID_COST") %>'/>
                    <asp:TextBox ID="TextBoxCADCost" runat="server" Text='<%# Eval("CAD_COST") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxCADCostAdd" runat="server"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="REQ Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelREQCost" runat="server" Text='<%# Eval("REQ_COST") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxREQCost" runat="server" Text='<%# Eval("REQ_COST") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxREQCostAdd" runat="server"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateCost" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelCost" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditCost" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteCost" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#666666" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="#666666" />
    </asp:GridView>
</asp:Content>
