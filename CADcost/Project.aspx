﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Project.aspx.cs" Inherits="CADcost.Project" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
    <legend>List of Active Projects</legend>
        <button type="button" id="ButtonEdit" class="btn btn-warning" data-toggle="modal" data-target="#dialogEdit" style="display:none">...</button>
    <asp:GridView ID="GridViewProject" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowCommand="GridViewProject_RowCommand" OnRowEditing="GridViewProject_RowEditing" OnRowUpdating="GridViewProject_RowUpdating" OnRowDeleting="GridViewProject_RowDeleting" OnRowCancelingEdit="GridViewProject_RowCancelingEdit" HorizontalAlign="Center" Width="80%" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Jobcode">
                <ItemTemplate>
                    <asp:Label ID="LabelJobcode" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Project Name">
                <ItemTemplate>
                    <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Reminder">
                <ItemTemplate>
                    <asp:Label ID="LabelDateReminder" runat="server" Text='<%# Eval("DATE_REMINDER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxDateReminder" runat="server" Text='<%# Eval("DATE_REMINDER") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditProject" runat="server" CommandName="Edit" CommandArgument='<%# Eval("JOBCODE_ID") %>' Text="Edit"/>
                    <asp:Button CssClass="btn-danger" ID="LinkButtonUpdateProject" runat="server" CommandName="View" CommandArgument='<%# Eval("JOBCODE_ID") %>' Text="EditRater"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteProject" runat="server" CommandName="Delete" CommandArgument='<%# Eval("JOBCODE_ID") %>' Text="Delete"/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateProject" runat="server" CommandName="Update" CommandArgument='<%# Eval("JOBCODE_ID") %>' Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelProject" runat="server" CommandName="Cancel" CommandArgument='<%# Eval("JOBCODE_ID") %>' Text="Cancel"/>
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <RowStyle HorizontalAlign="Center"></RowStyle>
    </asp:GridView>

    
    <legend>Add Project</legend>
        <asp:Table runat="server" Width="100%">
            <asp:TableRow>
                <asp:TableCell>
                    Project:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:UpdatePanel ID="updatePanelDropdown" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                    <asp:DropDownList ID="DropDownListProject" runat="server" Width="100%" CssClass="form-control"></asp:DropDownList>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Date Renewal:
                </asp:TableCell>
                <asp:TableCell ColumnSpan="2">
                    <asp:TextBox ID="TextBoxDateReminder" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                    <asp:Button ID="ButtonAddProject" runat="server" Text="Add Project Reminder" CssClass="btn-primary" OnClick="ButtonAddProject_Click"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        
            <asp:Table ID="Table1" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="5" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
              </div>
          </div>
        </div>
        
    </div>

    <div class="modal fade" id="dialogEdit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="editClose">&times;</button>
          <h4 class="modal-title">
              <asp:Label ID="LabelEditRater" runat="server" Text=""></asp:Label></h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanelRater" runat="server" UpdateMode="Conditional">
                          <ContentTemplate>
              <asp:Table runat="server" HorizontalAlign="Center" Width="100%">
                  <asp:TableRow>
                      <asp:TableCell HorizontalAlign="Center"><h3>Rater</h3></asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow><asp:TableCell HorizontalAlign="Center">
                        
                            <asp:GridView ID="GridViewRater" runat="server" CssClass="table table-striped table-bordered table-hover" OnRowEditing="GridViewRater_RowEditing" OnRowUpdating="GridViewRater_RowUpdating" OnRowCancelingEdit="GridViewRater_RowCancelingEdit" OnRowDeleting="GridViewRater_RowDeleting" OnDataBound="GridViewRater_DataBound" OnPageIndexChanging="GridViewRater_PageIndexChanging" AutoGenerateColumns="false" ShowFooter="true" ShowHeaderWhenEmpty="true" OnRowDataBound="GridViewRater_RowDataBound" Width="100%" HorizontalAlign="Center" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Right">
                                
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="ButtonAddRater" runat="server" Text="Add" CssClass="btn-primary" OnClick="ButtonAddRater_Click"/>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenFieldIDRater" runat="server" Value='<%# Eval("ID_RATER")%>'/>
                                            <asp:HiddenField ID="HiddenFieldSectID" runat="server" Value='<%# Eval("SECT_ID")%>'/>
                                            <asp:Label ID="LabelDepartment" runat="server" Text='<%# Eval("Department") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="DropDownListDepartmentAdd" runat="server" OnSelectedIndexChanged="DropDownListDepartmentAdd_SelectedIndexChanged" CssClass="form-control input-sm" AutoPostBack="true" Width="100%"></asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Rater">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelFirstRater" runat="server" Text='<%# Eval("FirstRater") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListFirstRaterEdit" runat="server" CssClass="form-control input-sm" Width="100%"></asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="DropDownListFirstRaterAdd" runat="server" CssClass="form-control input-sm" Width="100%"></asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Second Rater">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSecondRater" runat="server" Text='<%# Eval("SecondRater") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListSecondRaterEdit" runat="server" CssClass="form-control input-sm" Width="100%"></asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="DropDownListSecondRaterAdd" runat="server" CssClass="form-control input-sm" Width="100%"></asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button CssClass="btn-primary" ID="LinkButtonRaterEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("ID_RATER")%>' Text="Edit"/>
                                            <asp:Button CssClass="btn-warning" ID="LinkButtonRaterDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("ID_RATER")%>' Text="Delete"/>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Button CssClass="btn-primary" ID="LinkButtonRaterUpdate" runat="server" CommandName="Update" CommandArgument='<%# Eval("ID_RATER")%>' Text="Update"/>
                                            <asp:Button CssClass="btn-warning" ID="LinkButtonRaterCancel" runat="server" CommandName="Cancel" CommandArgument='<%# Eval("ID_RATER")%>' Text="Cancel"/>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        
                      </asp:TableCell>
                              </asp:TableRow>
                  <asp:TableRow>
                      <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                          <asp:Button ID="ButtonReset" runat="server" Text="Delete All Rater" CssClass="btn-danger btn" OnClick="ButtonReset_Click"/>
                          <asp:Button ID="ButtonBack" runat="server" Text="Back" CssClass="btn-danger btn" OnClick="ButtonBack_Click"/>
                      </asp:TableCell>
                  </asp:TableRow>
              </asp:Table>
              </ContentTemplate>
                      </asp:UpdatePanel>
          </div>
    </div>
    </div>
</div>

    
</asp:Content>
