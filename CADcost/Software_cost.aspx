﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_cost.aspx.cs" Inherits="CADcost.Software_cost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <div class="well ">
        
    <legend>Last Update Software Cost</legend>
        <asp:Table ID="Table1" runat="server" Width="100%">
            <asp:TableRow>
                <asp:TableCell Width="10%"><h5>Currency:</h5></asp:TableCell>
                <asp:TableCell><asp:DropDownList ID="DropDownListCurrency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListCurrency_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList></asp:TableCell>
            </asp:TableRow>
        
            </asp:Table>
    <asp:GridView ID="GridViewSoftwareCost" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true" OnDataBound="GridViewSoftwareCost_DataBound" OnRowEditing="GridViewSoftwareCost_RowEditing" OnRowDeleting="GridViewSoftwareCost_RowDeleting" OnRowUpdating="GridViewSoftwareCost_RowUpdating" OnRowCancelingEdit="GridViewSoftwareCost_RowCancelingEdit" OnRowDataBound="GridViewSoftwareCost_RowDataBound" HorizontalAlign="Center" Width="80%" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddCost" runat="server" Text="Add" OnClick="ButtonAddCost_Click" CssClass="btn btn-primary"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Software">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDCost" runat="server"  Value='<%# Eval("ID_COST") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="DropDownListSoftwareAdd" runat="server" CssClass="form-control"></asp:DropDownList>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("TYPE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxType" runat="server" Text='<%# Eval("TYPE") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxTypeAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelCADCost" runat="server" Text='<%# Eval("CAD_COST", "{0:N2}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDCostEdit" runat="server"  Value='<%# Eval("ID_COST") %>'/>
                    <asp:TextBox ID="TextBoxCADCost" runat="server" Text='<%# Eval("CAD_COST", "{0:N2}") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxCADCostAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateCost" runat="server" CommandName="Update" Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelCost" runat="server" CommandName="Cancel" Text="Cancel"/>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditCost" runat="server" CommandName="Edit" Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteCost" runat="server" CommandName="Delete" Text="Delete"/>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="DropDownListCurrencyAdd" runat="server" CssClass="form-control"></asp:DropDownList>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="LightGray" />
    </asp:GridView>
    <legend>Software Cost History</legend>
    <asp:DropDownList runat="server" id="DropDownListSoftware" OnSelectedIndexChanged="DropDownListSoftware_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
    <asp:GridView ID="GridViewSoftwareCostHistory" runat="server" OnRowDataBound="GridViewSoftwareCostHistory_RowDataBound" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Yet" HorizontalAlign="Center" Width="50%" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type">
                <ItemTemplate>
                    <asp:Label ID="LabelType" runat="server" Text='<%# Eval("TYPE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Year Update">
                <ItemTemplate>
                    <asp:Label ID="LabelDateUpdate" runat="server" Text='<%# Eval("CAD_STARTDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelCADCost" runat="server" Text='<%# Eval("CAD_COST", "{0:0.00}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="LightGray" />
    </asp:GridView>
    </div>
</asp:Content>
