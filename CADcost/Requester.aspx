﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Requester.aspx.cs" Inherits="CADcost.Requester" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
        <legend>Requester</legend>
        <asp:GridView ID="GridViewRequester" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowDeleting="GridViewRequester_RowDeleting" HorizontalAlign="Center" Width="80%" CssClass="table table-striped table-bordered table-hover">
            <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="LabelDepartment" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee ID">
                <ItemTemplate>
                    <asp:Label ID="LabelEmployeeID" runat="server" Text='<%# Eval("EMP_ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name">
                <ItemTemplate>
                    <asp:Label ID="LabelEmployeeName" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("EMP_ID") %>' Text="Delete"/>
                </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <legend>Add Employee</legend>
        <asp:Table runat="server" Width="80%">
            <asp:TableRow>
                <asp:TableCell Width="20%">
                    Department:
                </asp:TableCell>
                <asp:TableCell Width="80%">
                    <asp:DropDownList ID="DropDownListDepartment" runat="server" OnSelectedIndexChanged="DropDownListDepartment_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control input-sm"></asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Employee:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelEmployee" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListEmployee" runat="server" Width="100%"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                    <asp:Button ID="ButtonAddEmployee" runat="server" Text="Add Employee" OnClick="ButtonAddEmployee_Click"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
