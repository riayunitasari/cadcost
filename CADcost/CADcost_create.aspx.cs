﻿using CADcost.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;
using System.Data;
using System.Globalization;

namespace CADcost
{
    public partial class CADcost_create : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        DateTime timeStart;
        int act;
        int id_cadreq;
        /*
        private List<Dictionary<string,object>> CurrentCADDETAIL
        {
            get
            {
                return ((SViewState<List<Dictionary<string, object>>>)ViewState["curItem"]).ViewState;
            }
            set
            {
                ViewState["curItem"] = new SViewState<List<Dictionary<string, object>>>(value);
            }
        }
        */

        protected void Page_Load(object sender, EventArgs e)
        {

            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            timeStart = DateTime.Now;
            int projectCount = 0;
            if (!IsPostBack)
            {
                DropDownListMonth.Items.Add(new ListItem("January", "1"));
                DropDownListMonth.Items.Add(new ListItem("February", "2"));
                DropDownListMonth.Items.Add(new ListItem("March", "3"));
                DropDownListMonth.Items.Add(new ListItem("April", "4"));
                DropDownListMonth.Items.Add(new ListItem("May", "5"));
                DropDownListMonth.Items.Add(new ListItem("June", "6"));
                DropDownListMonth.Items.Add(new ListItem("July", "7"));
                DropDownListMonth.Items.Add(new ListItem("August", "8"));
                DropDownListMonth.Items.Add(new ListItem("September", "9"));
                DropDownListMonth.Items.Add(new ListItem("October", "10"));
                DropDownListMonth.Items.Add(new ListItem("November", "11"));
                DropDownListMonth.Items.Add(new ListItem("December", "12"));

                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));

                DropDownListMonth.SelectedValue = ((DateTime.Now.Month+1)%12).ToString();
                if ((DateTime.Now.Month + 1)%12==1)
                    DropDownListYear.SelectedValue = (DateTime.Now.Year+1).ToString();
                else
                    DropDownListYear.SelectedValue = DateTime.Now.Year.ToString();

                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));
                if (int.TryParse(Request["act"], out act))
                {
                    if (act == 0)
                    {
                        int id_cadreq = Helper.CADcostHelper.getLastCADreqNO() + 1;
                        string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).ToString();
                        LabelCADcostNO.Text = Helper.CADcostHelper.generateCADreqNO(sectcode);
                        LabelDepartment.Text = logInUser.SECT_NAME;
                        LabelRequester.Text = logInUser.EMP_ID + " - " + logInUser.FULL_NAME;
                        DropDownListProject.Items.Clear();
                        CADcostHelper.generateDDLProjectName(DropDownListProject);

                        DataTable dt = new DataTable();
                        dt.Columns.Add("No.", typeof(int));
                        dt.Columns.Add("NAME_SOFTWARE", typeof(string));
                        dt.Columns.Add("QUANTITY", typeof(string));
                        dt.Columns.Add("REMARK", typeof(string));
                        DataRow dtRow = dt.NewRow();
                        dt.Rows.Add(dtRow);

                        GridViewItem.DataSource = dt;
                        GridViewItem.DataBind();

                        GridViewItem.Rows[0].Visible = false;
                    }
                    else if (act == 1)
                    {
                        id_cadreq = (int)Session["id_cadreq"];
                        CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
                        LabelCADcostNO.Text = cadreq.CADREQ_NO;
                        LabelDepartment.Text = PFNATTHelper.getEMPDept(cadreq.CADREQ_BY);
                        LabelRequester.Text = cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY);
                        CADcostHelper.generateDDLProjectName(DropDownListProject);
                        DropDownListProject.SelectedValue = cadreq.JOBCODE;
                        DropDownListMonth.SelectedValue = cadreq.FOR_MONTH.ToString();
                        DropDownListYear.SelectedValue = cadreq.FOR_YEAR.ToString();
                        List<CADcostItem> items = CADcostHelper.getItem(id_cadreq, 0);
                        if (items.Count > 0)
                        {
                            GridViewItem.DataSource = items;
                            GridViewItem.DataBind();
                        }
                        else
                        {
                            DataTable dt = new DataTable();
                            dt.Columns.Add("No.", typeof(int));
                            dt.Columns.Add("NAME_SOFTWARE", typeof(string));
                            dt.Columns.Add("QUANTITY", typeof(string));
                            dt.Columns.Add("REMARK", typeof(string));
                            DataRow dtRow = dt.NewRow();
                            dt.Rows.Add(dtRow);

                            GridViewItem.DataSource = dt;
                            GridViewItem.DataBind();

                            GridViewItem.Rows[0].Visible = false;
                        }
                        ViewState[curItem] = items;
                        TextBoxDescription.Text = cadreq.DESCRIPTION;
                    }
                }
                
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "tes", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            GridViewRow row = GridViewItem.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareNameAdd") as DropDownList;
            TextBox tbQuantity = row.FindControl("TextBoxQuantityAdd") as TextBox;
            TextBox tbRemark = row.FindControl("TextBoxRemarkAdd") as TextBox;
            CADcostItem item = new CADcostItem(Convert.ToInt32(ddlSoftware.SelectedValue), ddlSoftware.SelectedItem.Text, Convert.ToInt32(tbQuantity.Text), tbRemark.Text, false);
            List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
            if (items == null) items = new List<CADcostItem>();
            items.Add(item);
            ViewState[curItem] = items;
            GridViewItem.DataSource = items;
            GridViewItem.DataBind();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            GridViewItemSummary.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItemSummary.DataBind();
            LabelProjectSummary.Text = DropDownListProject.SelectedItem.Text;
            LabelForMonth.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(DropDownListMonth.SelectedValue));
            LabelForYear.Text = DropDownListYear.SelectedValue;
            LabelDescriptionSummmary.Text = TextBoxDescription.Text;
            PanelSubmit.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#buttonSubmit').click()", true);
            
            
        }

        protected void ButtonSaveAsDraft_Click(object sender, EventArgs e)
        {
            if (int.TryParse(Request["act"], out act))
            {
                if (act == 0)
                {
                    logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                    CADREQ req = new CADREQ();
                    req.CADREQ_BY = logInUser.EMP_ID;
                    req.ID_ACTIVITY = 1;
                    string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).Trim().ToString();
                    req.CADREQ_NO = CADcostHelper.generateCADreqNO(sectcode);
                    req.FOR_MONTH = Convert.ToInt32(DropDownListMonth.SelectedValue);
                    req.FOR_YEAR = Convert.ToInt32(DropDownListYear.SelectedValue);
                    req.DESCRIPTION = TextBoxDescription.Text;
                    req.JOBCODE = DropDownListProject.SelectedValue;
                    if (logInUser.ISPARTICIPANT)
                    {
                        req.LASTACTIVITY_BY = logInUser.EMP_ID;
                    }
                    else
                    {
                        req.LASTACTIVITY_BY = logInUser.PARTICIPANT_ID;
                    }
                    req.PIC = logInUser.EMP_ID;
                    req.TIME_LASTACTIVITY = DateTime.Now;
                    req.TIME_REQUEST = DateTime.Now;
                    List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
                    CADcostHelper.SubmitCADreq(logInUser.SECT_ID, req, DropDownListProject.SelectedValue.ToString(), items);


                    ACTION_HISTORY action = new ACTION_HISTORY();
                    action.ID_ACTION = 2; //save as draft
                    action.ID_ACTIVITY = 1; //prepare request
                    action.ID_CADREQ = CADcostHelper.getLastCADreqNO();
                    action.JOBCODE = DropDownListProject.SelectedValue;
                    action.TIME_START = timeStart;
                    action.TIME_END = DateTime.Now;
                    action.COMMENT = TextBoxComment.Text;
                    if (logInUser.ISPARTICIPANT)
                    {
                        action.PARTICIPANT = logInUser.EMP_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;

                    }
                    else
                    {
                        action.PARTICIPANT = logInUser.PARTICIPANT_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                        action.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                    }

                    CADcostHelper.insertActionHistory(action);

                    Session["jobcode"] = DropDownListProject.SelectedValue;
                    var url = "~/CADcost_record.aspx";
                    Response.Redirect(url);
                }
                else if (act == 1)
                {
                    logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                    List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
                    CADcostHelper.UpdateCADreqDraft((int)Session["id_cadreq"], logInUser.EMP_ID, DropDownListProject.SelectedValue.ToString(), TextBoxDescription.Text, items);


                    ACTION_HISTORY action = new ACTION_HISTORY();
                    action.ID_ACTION = 2; //save as draft
                    action.ID_ACTIVITY = 1; //prepare request
                    action.ID_CADREQ = (int)Session["id_cadreq"];
                    action.JOBCODE = DropDownListProject.SelectedValue;
                    action.TIME_START = timeStart;
                    action.TIME_END = DateTime.Now;
                    action.COMMENT = TextBoxComment.Text;
                    if (logInUser.ISPARTICIPANT)
                    {
                        action.PARTICIPANT = logInUser.EMP_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                    }
                    else
                    {
                        action.PARTICIPANT = logInUser.PARTICIPANT_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                        action.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                    }

                    CADcostHelper.insertActionHistory(action);

                    Session["jobcode"] = DropDownListProject.SelectedValue;
                    var url = "~/CADcost_record.aspx";
                    Response.Redirect(url);
                }
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Beranda.aspx");
        }

        protected void ButtonConfirmOK_Click(object sender, EventArgs e)
        {
            if (int.TryParse(Request["act"], out act))
            {
                if (act == 0)
                {
                    logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                    CADREQ req = new CADREQ();
                    if (logInUser.ISPARTICIPANT)
                    {
                        req.CADREQ_BY = logInUser.EMP_ID;
                    }
                    else
                    {
                        req.CADREQ_BY = logInUser.PARTICIPANT_ID;
                    }
                    switch(CADcostHelper.getRole(logInUser.EMP_ID, DropDownListProject.SelectedValue).intRole)
                    {
                        case 2:
                        case 4:
                            req.ID_ACTIVITY = 6;
                            break;
                        case 3:
                            req.ID_ACTIVITY = 5;
                            break;
                        case 0:
                        case 1:
                        
                            req.ID_ACTIVITY = 2;
                            break;
                    }
                    
                    string sectcode = logInUser.SECT_NAME.Split('-').ElementAt(1).Trim().ToString();
                    req.CADREQ_NO = CADcostHelper.generateCADreqNO(sectcode);
                    req.FOR_MONTH = Convert.ToInt32(DropDownListMonth.SelectedValue);
                    req.FOR_YEAR = Convert.ToInt32(DropDownListYear.SelectedValue);
                    req.DESCRIPTION = TextBoxDescription.Text;
                    req.JOBCODE = DropDownListProject.SelectedValue;
                    if (logInUser.ISPARTICIPANT)
                    {
                        req.LASTACTIVITY_BY = logInUser.EMP_ID;
                    }
                    else
                    {
                        req.LASTACTIVITY_BY = logInUser.PARTICIPANT_ID;
                    }
                    req.PIC = logInUser.EMP_ID;
                    req.TIME_LASTACTIVITY = DateTime.Now;
                    req.TIME_REQUEST = DateTime.Now;
                    List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
                    CADcostHelper.SubmitCADreq(logInUser.SECT_ID, req, DropDownListProject.SelectedValue.ToString(), items);


                    ACTION_HISTORY action = new ACTION_HISTORY();
                    action.ID_CADREQ = CADcostHelper.getLastCADreqNO();
                    action.JOBCODE = DropDownListProject.SelectedValue;
                    action.TIME_START = timeStart;
                    action.TIME_END = DateTime.Now;
                    action.COMMENT = TextBoxComment.Text;
                    if (logInUser.ISPARTICIPANT)
                    {
                        action.PARTICIPANT = logInUser.EMP_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;

                    }
                    else
                    {
                        action.PARTICIPANT = logInUser.PARTICIPANT_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                        action.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                    }

                    switch (CADcostHelper.getRole(logInUser.EMP_ID, DropDownListProject.SelectedValue).intRole)
                    {
                        case 2:
                            action.ID_ACTION = 1; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            action.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action2 = CADcostHelper.Clone(action);
                            action2.ID_ACTION = 6; //approve request
                            action2.ID_ACTIVITY = 5; //fix request
                            action2.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action2);
                            ACTION_HISTORY action3 = CADcostHelper.Clone(action);
                            action3.ID_ACTION = 6; //approve request
                            action3.ID_ACTIVITY = 6; //fix request
                            action3.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action3);
                            WorkflowHelper.sendEmailToApproveENIT(CADcostHelper.getLastCADreqNO());
                            break;
                        case 3:
                            action.ID_ACTION = 1; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action4 = CADcostHelper.Clone(action);
                            action4.ID_ACTION = 6; //approve request
                            action4.ID_ACTIVITY = 5; //fix request
                            CADcostHelper.insertActionHistory(action4);
                            WorkflowHelper.sendEmailToApproveSecondRater(CADcostHelper.getLastCADreqNO());
                            break;
                        case 4:
                            action.ID_ACTION = 1; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action5 = CADcostHelper.Clone(action);
                            action5.ID_ACTION = 6; //approve request
                            action5.ID_ACTIVITY = 5; //fix request
                            action5.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action5);
                            ACTION_HISTORY action6 = CADcostHelper.Clone(action);
                            action6.ID_ACTION = 6; //approve request
                            action6.ID_ACTIVITY = 6; //fix request
                            action6.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action6);
                            WorkflowHelper.sendEmailToApproveENIT(CADcostHelper.getLastCADreqNO());
                            break;
                        case 0:
                        case 1:
                            action.ID_ACTION = 1; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            WorkflowHelper.sendEmailToApproveFirstRater(CADcostHelper.getLastCADreqNO());
                            break;
                    }

                    Session["jobcode"] = DropDownListProject.SelectedValue;
                    var url = "~/CADcost_record.aspx";
                    Response.Redirect(url);
                }
                else if (act == 1)
                {
                    logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                    List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
                    int prevActivity = CADcostHelper.UpdateCADreq((int)Session["id_cadreq"], logInUser.EMP_ID, DropDownListProject.SelectedValue.ToString(), TextBoxDescription.Text, items);
                    CADREQ req = CADcostHelper.getCADdetail((int)Session["id_cadreq"]);
                    ACTION_HISTORY action = new ACTION_HISTORY();
                    action.ID_CADREQ = (int)Session["id_cadreq"];
                    action.JOBCODE = DropDownListProject.SelectedValue;
                    action.TIME_START = timeStart;
                    action.TIME_END = DateTime.Now;
                    action.COMMENT = TextBoxComment.Text;
                    if (logInUser.ISPARTICIPANT)
                    {
                        action.PARTICIPANT = logInUser.EMP_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                    }
                    else
                    {
                        action.PARTICIPANT = logInUser.PARTICIPANT_ID;
                        action.COMPLETED_BY = logInUser.EMP_ID;
                        action.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                    }

                    switch (CADcostHelper.getRole(logInUser.EMP_ID, DropDownListProject.SelectedValue).intRole)
                    {
                        case 2:
                            action.ID_ACTION = 3; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action5 = CADcostHelper.Clone(action);
                            action5.ID_ACTION = 6; //approve request
                            action5.ID_ACTIVITY = 5; //fix request
                            CADcostHelper.insertActionHistory(action5);
                            ACTION_HISTORY action6 = CADcostHelper.Clone(action);
                            action6.ID_ACTION = 6; //approve request
                            action6.ID_ACTIVITY = 6; //fix request
                            CADcostHelper.insertActionHistory(action6);
                            WorkflowHelper.sendEmailToApproveENIT(CADcostHelper.getLastCADreqNO());
                            break;
                        case 3:
                            action.ID_ACTION = 3; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action7 = CADcostHelper.Clone(action);
                            action7.ID_ACTION = 6; //approve request
                            action7.ID_ACTIVITY = 5; //fix request
                            CADcostHelper.insertActionHistory(action7);
                            WorkflowHelper.sendEmailToApproveSecondRater(CADcostHelper.getLastCADreqNO());
                            break;
                        case 4:
                            action.ID_ACTION = 1; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            ACTION_HISTORY action8 = CADcostHelper.Clone(action);
                            action8.PARTICIPANT = CADcostHelper.getFirstRater(req.JOBCODE, PFNATTHelper.getEMPSect(req.PIC));
                            action8.COMPLETED_BY = logInUser.EMP_ID;
                            action8.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                            action8.ID_ACTION = 6; //approve request
                            action8.ID_ACTIVITY = 5; //fix request
                            action8.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action8);
                            ACTION_HISTORY action9 = CADcostHelper.Clone(action);
                            action9.PARTICIPANT = logInUser.PARTICIPANT_ID;
                            action9.COMPLETED_BY = logInUser.EMP_ID;
                            action9.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                            action9.ID_ACTION = 6; //approve request
                            action9.ID_ACTIVITY = 6; //fix request
                            action9.TIME_END = DateTime.Now;
                            CADcostHelper.insertActionHistory(action9);
                            WorkflowHelper.sendEmailToApproveENIT(CADcostHelper.getLastCADreqNO());
                            break;
                        case 0:
                        case 1:
                            action.ID_ACTION = 3; //submit request
                            action.ID_ACTIVITY = 2; //fix request
                            CADcostHelper.insertActionHistory(action);
                            WorkflowHelper.sendEmailToApproveFirstRater(CADcostHelper.getLastCADreqNO());
                            break;
                    }
                    
                    Session["jobcode"] = DropDownListProject.SelectedValue;
                    var url = "~/CADcost_record.aspx";
                    Response.Redirect(url);
                }
            }
        }

        protected void ButtonConfirmCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSubmit", "$('#submitClose').click()", true);
        }

        protected void GridViewItem_DataBound(object sender, EventArgs e)
        {
            GridViewRow row = GridViewItem.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareNameAdd") as DropDownList;
            Helper.CADcostHelper.generateDDLSoftwareName(ddlSoftware);
        }

        protected void GridViewItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewItem.EditIndex = index;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void GridViewItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            List<CADcostItem> items = (List<CADcostItem>)ViewState[curItem];
            GridViewRow row = GridViewItem.Rows[e.RowIndex];
            DropDownList ddl = row.FindControl("DropDownListSoftwareNameEdit") as DropDownList;
            TextBox tbQtty = row.FindControl("TextBoxQuantityEdit") as TextBox;
            TextBox tbRemark = row.FindControl("TextBoxRemarkEdit") as TextBox;
            items[e.RowIndex].ID_SOFTWARE = Convert.ToInt32(ddl.SelectedValue);
            items[e.RowIndex].NAME_SOFTWARE = ddl.SelectedItem.ToString();
            items[e.RowIndex].QUANTITY = Convert.ToInt32(tbQtty.Text);
            items[e.RowIndex].REMARK = tbRemark.Text;
            ViewState[curItem] = items;
            GridViewItem.EditIndex = -1;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void GridViewItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + myStringVariable + "');", true);
            ((List<CADcostItem>)ViewState[curItem]).RemoveAt(e.RowIndex);
            if (((List<CADcostItem>)ViewState[curItem]).Count == 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("No.", typeof(int));
                dt.Columns.Add("NAME_SOFTWARE", typeof(string));
                dt.Columns.Add("QUANTITY", typeof(string));
                dt.Columns.Add("REMARK", typeof(string));
                DataRow dtRow = dt.NewRow();
                dt.Rows.Add(dtRow);
                GridViewItem.DataSource = dt;
                GridViewItem.DataBind();
                GridViewItem.Rows[0].Visible = false;
            }
            else
            {
                GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
                GridViewItem.DataBind();
            }
        }

        protected void GridViewItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                CADcostItem itemedit = e.Row.DataItem as CADcostItem;
                DropDownList ddl = e.Row.FindControl("DropDownListSoftwareNameEdit") as DropDownList;
                CADcostHelper.generateDDLSoftwareName(ddl);
                ddl.SelectedValue = itemedit.ID_SOFTWARE.ToString();
            }
        }

        protected void GridViewItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewItem.EditIndex = -1;
            GridViewItem.DataSource = (List<CADcostItem>)ViewState[curItem];
            GridViewItem.DataBind();
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void ButtonSearchJobcode_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            PanelProject.Visible = true;
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
        }
        
        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            updatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            //ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

    }
}