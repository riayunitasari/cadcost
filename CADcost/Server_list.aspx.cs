﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Entities;
using CADcost.Helper;

namespace CADcost
{
    public partial class Server_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridViewServerList.DataSource = CADcostHelper.getAllServer();
                GridViewServerList.DataBind();
            }
        }
        
        protected void GridViewServerList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewServerList.EditIndex = index;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewServerList.Rows[e.RowIndex];
            HiddenField hfIDServer = row.FindControl("HiddenFieldIDServer") as HiddenField;
            CADcostHelper.deleteServer(Convert.ToInt32(hfIDServer.Value));
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewServerList.Rows[e.RowIndex];
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDServerEdit") as HiddenField;
            TextBox tbServerName = row.FindControl("TextBoxServerName") as TextBox;
            TextBox tbIPServer = row.FindControl("TextBoxIPServer") as TextBox;
            SERVER srvr = new SERVER();
            srvr.ID_SERVER = Convert.ToInt32(hfIDVendor.Value);
            srvr.SERVER_NAME = tbServerName.Text;
            srvr.IP_SERVER = tbIPServer.Text;
            CADcostHelper.updateServer(srvr);
            GridViewServerList.EditIndex = -1;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void GridViewServerList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewServerList.EditIndex = -1;
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }

        protected void ButtonAddServer_Click1(object sender, EventArgs e)
        {
            GridViewRow row = GridViewServerList.FooterRow;
            TextBox tbServerName = row.FindControl("TextBoxServerNameAdd") as TextBox;
            TextBox tbIPServer = row.FindControl("TextBoxIPServerAdd") as TextBox;
            SERVER server = new SERVER();
            server.SERVER_NAME = tbServerName.Text;
            server.IP_SERVER = tbIPServer.Text;
            CADcostHelper.insertServer(server);
            GridViewServerList.DataSource = CADcostHelper.getAllServer();
            GridViewServerList.DataBind();
        }
    }
}