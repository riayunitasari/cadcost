﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Delegate.aspx.cs" Inherits="CADcost.Delegate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
    <asp:GridView ID="GridViewRequester" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewRequester_RowEditing" OnRowDeleting="GridViewRequester_RowDeleting" OnRowUpdating="GridViewRequester_RowUpdating" OnRowCancelingEdit="GridViewRequester_RowCancelingEdit" OnRowDataBound="GridViewRequester_RowDataBound" HorizontalAlign="Center" Width="80%" CssClass="table table-striped table-bordered table-hover">
            <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delegator">
                <ItemTemplate>
                    <asp:Label ID="LabelEmployeeDelegating" runat="server" Text='<%# Eval("EMP_Delegating") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Delegated">
                <ItemTemplate>
                    <asp:Label ID="LabelEmployeeDelegated" runat="server" Text='<%# Eval("EMP_Delegated") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListEmployeeDelegatedEdit" runat="server" CssClass="form-control input-sm" Width="100%"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditDelegate" runat="server" CommandName="Edit" CommandArgument='<%# Eval("EMP_DelegatingID") %>' Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteDelegate" runat="server" CommandName="Delete" CommandArgument='<%# Eval("EMP_DelegatingID") %>' Text="Delete"/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateDelegate" runat="server" CommandName="Update" CommandArgument='<%# Eval("EMP_DelegatingID") %>' Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelDelegate" runat="server" CommandName="Cancel" CommandArgument='<%# Eval("EMP_DelegatingID") %>' Text="Cancel"/>
                </EditItemTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <legend><h3>Set Delegate</h3></legend>
        <button type="button" id="ButtonPopUpDelegate" class="btn btn-warning" data-toggle="modal" data-target="#dialogDelegate" style="display:none;">Delegate</button>
    <asp:Table runat="server" HorizontalAlign="Center">
        <asp:TableRow ID="RowEnit1">
            <asp:TableCell>
                <h5>Employee to be delegated</h5>
            </asp:TableCell>
            <asp:TableCell>
                : <asp:DropDownList ID="DropDownListEmployeeDelegated" runat="server" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpID1" runat="server" ErrorMessage="Required" ControlToValidate="DropDownListEmployeeDelegated" ValidationGroup="set"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="RowEnit2">
            <asp:TableCell>
                <h5>Set delegate to</h5>
            </asp:TableCell>
            <asp:TableCell>
                : <asp:DropDownList ID="DropDownListEmployeeDelegating" runat="server" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpID2" runat="server" ErrorMessage="Required" ControlToValidate="DropDownListEmployeeDelegating" ValidationGroup="set"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="RowEnit3">
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Button ID="ButtonSetDelegate" runat="server" Text="Set Delegate" OnClick="ButtonSetDelegate_Click" class="btn btn-primary" ValidationGroup="set"/>
                
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="RowRater1">
            <asp:TableCell>
                <h5>Employee to be delegated</h5>
            </asp:TableCell>
            <asp:TableCell>
                : <asp:DropDownList ID="DropDownListEmployeeDelegatedRater" runat="server" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpIDRater" runat="server" ErrorMessage="Required" ControlToValidate="DropDownListEmployeeDelegatedRater" ValidationGroup="set1"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="RowRater2">
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Button ID="ButtonSetDelegateRater" runat="server" Text="Set Delegate" OnClick="ButtonSetDelegateRater_Click" class="btn btn-primary" ValidationGroup="set1"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </div>

    <div class="modal fade" id="dialogDelegate" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="delegateClose">&times;</button>
          <h4 class="modal-title">Delegate</h4>
        </div>
          <div class="modal-body">
        <asp:UpdatePanel runat="server" ID="PanelDelegate" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Table ID="TableConfirmationEnit" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Set Delegation</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate for:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegated" runat="server" Text="Label" CssClass="input-sm"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate to:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegating" runat="server" Text="Label" CssClass="input-sm"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Set Delegate" ID="ButtonDelegateOK" OnClick="ButtonDelegateOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonDelegateCancel" OnClick="ButtonDelegateCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table ID="TableConfirmationRater" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Set Delegation</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate for:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegatedRater" runat="server" Text="Label" CssClass="input-sm"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Set Delegate" ID="ButtonDelegateOKRater" OnClick="ButtonDelegateOKRater_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonDelegateCancelRater" OnClick="ButtonDelegateCancelRater_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
                
        </asp:UpdatePanel>
            </div>
          </div>
        </div>
    </div>
</asp:Content>
