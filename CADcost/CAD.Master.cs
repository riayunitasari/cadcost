﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class CAD : System.Web.UI.MasterPage
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        DateTime timeStart = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                LabelLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                int projectCount = 0;

                List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                approve1.AddRange(CADcostHelper.getApproveENIT("0"));
                approve1 = approve1.Distinct(new ApproveComparer()).ToList();

                if (!CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)) && approve1.Count > 0)
                {
                //if (!logInUser.ISADMIN && approve1.Count > 0)
                //{
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideImpersonate", "hideImpersonate();", true);
                }

                if (logInUser.ISADMIN == false) // jika yang didelegasikan bukan admin
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideSoftware", "hideSoftwareMenu();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideReport", "hideReportMenu();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideSetting", "hideSettingMenu();", true);
                }

                if (!CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)) && approve1.Count == 0) // PC yang bersangkutan bukan admin
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "hideDelegate", "hideDelegate();", true);
        }

        protected void ButtonImpersonate_Click(object sender, EventArgs e)
        {
            if (CADcostHelper.setDelegate(TextBoxImpersonate.Text, PFNATTHelper.getEMPidbyUsername(Request.LogonUserIdentity.Name)))
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                LabelLogin.Text = logInUser.FULL_NAME;
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void ButtonRelease_Click(object sender, EventArgs e)
        {
            CADcostHelper.deleteDelegate(PFNATTHelper.getEMPidbyUsername(Request.LogonUserIdentity.Name));
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            LabelLogin.Text = logInUser.FULL_NAME;
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonDelegate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Delegate.aspx");
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }

    }
}