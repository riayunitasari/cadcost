﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;

namespace CADcost.Entities
{
    [Serializable]
    public class SViewState<T> : System.Runtime.Serialization.ISerializable where T:class
    {
        public SViewState() { }
        public SViewState(T obj)
        {
            ViewState = (T)obj;
        }
        private String _xml;
        public String SObject
        {
            get
            {
                _xml = ViewState.GetType().FullName + "<>" + SerializeHelper.SerializeXml(ViewState);
                return _xml;
            }
            set
            {
                int index = value.IndexOf("##");
                string typeName = value.Substring(0, index);
                _xml = value.Substring(index + 2);
                Type typ = Type.GetType(typeName);
                ViewState = (T)SerializeHelper.DeserializeXMl(typeof(T), _xml);
            }
        }
        [NonSerialized]
        private T _viewState = null;
        public T ViewState { get { return _viewState; } set { _viewState = value; } }

        protected SViewState(SerializationInfo info, StreamingContext context)
        {
            ViewState = (T)SerializeHelper.DeserializeXMl(typeof(T), info.GetString("_xml"));
        }
        [SecurityPermission(SecurityAction.Demand,SerializationFormatter = true)]

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("_xml", SerializeHelper.SerializeXml(ViewState));
        }
    }
}