﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace CADcost.Entities
{
    public class SerializeHelper
    {
        public static string SerializeXml(object value)
        {
            if (value.GetType() == typeof(string))
                return value.ToString();

            StringWriter stringWriter = new StringWriter();
            using (XmlWriter writer = XmlWriter.Create(stringWriter))
            {
                DataContractSerializer serializer = new
                  DataContractSerializer(value.GetType());
                serializer.WriteObject(writer, value);
            }

            return stringWriter.ToString();
        }
        public static object DeserializeXMl(Type type, string serializedValue)
        {
            if (type == typeof(string))
                return serializedValue;

            using (StringReader stringReader = new StringReader(serializedValue))
            {
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    DataContractSerializer serializer =
                     new DataContractSerializer((type));

                    object deserializedValue = serializer.ReadObject(reader);

                    return deserializedValue;
                }
            }
        } 
    }
}