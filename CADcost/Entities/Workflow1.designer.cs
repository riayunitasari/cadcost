﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CADcost.Entities
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Workflow")]
	public partial class WorkflowDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public WorkflowDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["WorkflowConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public WorkflowDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public WorkflowDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public WorkflowDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public WorkflowDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.InsertEmailSender")]
		public int InsertEmailSender([global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailFrom", DbType="VarChar(8000)")] string emailFrom, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailTo", DbType="VarChar(8000)")] string emailTo, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailCC", DbType="VarChar(8000)")] string emailCC, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailBCC", DbType="VarChar(8000)")] string emailBCC, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailSubject", DbType="VarChar(8000)")] string emailSubject, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EmailBody", DbType="VarChar(8000)")] string emailBody)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), emailFrom, emailTo, emailCC, emailBCC, emailSubject, emailBody);
			return ((int)(result.ReturnValue));
		}
	}
}
#pragma warning restore 1591
