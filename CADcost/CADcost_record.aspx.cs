﻿using CADcost.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;
using System.Globalization;

namespace CADcost
{
    public partial class CADcost_record : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            

            if (!IsPostBack)
            {
                int id_cadreq = Helper.CADcostHelper.getLastCADreqNO();
                DropDownListProject.Items.Add(new ListItem("ALL PROJECT", "0"));
                CADcostHelper.generateDDLProjectName(DropDownListProject);
                
                DropDownListMonth.Items.Add(new ListItem("January", "1"));
                DropDownListMonth.Items.Add(new ListItem("February", "2"));
                DropDownListMonth.Items.Add(new ListItem("March", "3"));
                DropDownListMonth.Items.Add(new ListItem("April", "4"));
                DropDownListMonth.Items.Add(new ListItem("May", "5"));
                DropDownListMonth.Items.Add(new ListItem("June", "6"));
                DropDownListMonth.Items.Add(new ListItem("July", "7"));
                DropDownListMonth.Items.Add(new ListItem("August", "8"));
                DropDownListMonth.Items.Add(new ListItem("September", "9"));
                DropDownListMonth.Items.Add(new ListItem("October", "10"));
                DropDownListMonth.Items.Add(new ListItem("November", "11"));
                DropDownListMonth.Items.Add(new ListItem("December", "12"));
                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));

                DropDownListStatus.Items.Add(new ListItem("ALL", "0"));
                DropDownListStatus.Items.Add(new ListItem("FIX_REQUEST", "2"));
                DropDownListStatus.Items.Add(new ListItem("CANCELED_REQUEST", "3"));
                DropDownListStatus.Items.Add(new ListItem("PREPARE_REQUEST", "1"));
                DropDownListStatus.Items.Add(new ListItem("REJECTED_REQUEST", "4"));
                DropDownListStatus.Items.Add(new ListItem("APPROVAL_BY_FIRST_RATER", "5"));
                DropDownListStatus.Items.Add(new ListItem("APPROVAL_BY_SECOND_RATER", "6"));
                DropDownListStatus.Items.Add(new ListItem("APPROVAL_BY_ENIT", "7"));
                DropDownListStatus.SelectedValue = "0";

                DropDownListAction.Items.Add(new ListItem("ALL", "0"));
                DropDownListAction.Items.Add(new ListItem("SUBMIT_REQUEST", "1"));
                DropDownListAction.Items.Add(new ListItem("CANCEL_REQUEST", "4"));
                DropDownListAction.Items.Add(new ListItem("SAVE_CHANGE_AND_SUBMIT_REQUEST", "3"));
                DropDownListAction.Items.Add(new ListItem("SAVE_AS_DRAFT", "2"));
                DropDownListAction.Items.Add(new ListItem("RETURN_TO_INITIATOR", "5"));
                DropDownListAction.Items.Add(new ListItem("APPROVE", "6"));
                DropDownListAction.SelectedValue = "0";

                DropDownListMonth.SelectedValue = DateTime.Now.Month.ToString();
                DropDownListYear.SelectedValue = DateTime.Now.Year.ToString();
                DropDownListProject_SelectedIndexChanged(sender, e);
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                    DropDownListProject_SelectedIndexChanged(sender, e);
                }
                
            }
            else
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                    DropDownListProject_SelectedIndexChanged(sender, e);
                }
                //DropDownListMonth.SelectedValue = DateTime.Now.Month.ToString();
                //DropDownListYear.SelectedValue = DateTime.Now.Year.ToString();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = null;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListStatus.SelectedValue));
            GridViewLicenseRequest.DataBind();
            GridViewActionHistory.DataSource = CADcostHelper.getAllActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListAction.SelectedValue));
            GridViewActionHistory.DataBind();
            
            updatepanelreqhistory.Update();
            
        }

        protected void GridViewLicenseRequest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("view"))
            {
                GridViewView.DataSource = CADcostHelper.getItem(Convert.ToInt32(e.CommandArgument.ToString()), 0);
                GridViewView.DataBind();
                GridViewCadreqActionHistory.DataSource = CADcostHelper.getCadreqActionHistory(Convert.ToInt32(e.CommandArgument.ToString()));
                GridViewCadreqActionHistory.DataBind();
                CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(e.CommandArgument.ToString()));
                LabelCADReqNo.Text = cadreq.CADREQ_NO;
                LabelProject.Text = cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE);
                LabelCADReqBy.Text = cadreq.PIC + " - " + PFNATTHelper.getEMPFullName(cadreq.PIC);
                LabelOnBehalfBy.Text = cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY);
                LabelDepartment.Text = PFNATTHelper.getSectName(PFNATTHelper.getEMPSect(cadreq.PIC));
                LabelForMonth.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(cadreq.FOR_MONTH);
                LabelForYear.Text = cadreq.FOR_YEAR.ToString();
                LabelTimeReq.Text = cadreq.TIME_REQUEST.ToString();
                LabelStatus.Text = CADcostHelper.getActivityName(cadreq.ID_ACTIVITY);
                LabelDescription.Text = cadreq.DESCRIPTION;
                LabelCommentView.Text = CADcostHelper.getLastActivityComment(Convert.ToInt32(e.CommandArgument.ToString()));
                ButtonViewCancel.CommandArgument = e.CommandArgument.ToString();
                PanelView.Update();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#buttonView').click()", true);
                
            }
            else if (e.CommandName.Equals("editrequest"))
            {
                Session["id_cadreq"] = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("~/CADcost_create.aspx?act=1");
            }
            else if (e.CommandName.Equals("cancelrequest"))
            {
                GridViewCancel.DataSource = CADcostHelper.getItem(Convert.ToInt32(e.CommandArgument.ToString()), 0);
                GridViewCancel.DataBind();
                CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(e.CommandArgument.ToString()));
                LabelCADReqNoCancel.Text = cadreq.CADREQ_NO;
                LabelProjectCancel.Text = cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE);
                LabelCADReqByCancel.Text = cadreq.PIC + " - " + PFNATTHelper.getEMPFullName(cadreq.PIC);
                LabelOnBehalfByCancel.Text = cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY);
                LabelDepartmentCancel.Text = PFNATTHelper.getSectName(PFNATTHelper.getEMPSect(cadreq.PIC));
                LabelForMonthCancel.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(cadreq.FOR_MONTH);
                LabelForYearCancel.Text = cadreq.FOR_YEAR.ToString();
                LabelTimeReqCancel.Text = cadreq.TIME_REQUEST.ToString();
                LabelDescriptionCancel.Text = cadreq.DESCRIPTION;
                LabelStatusCancel.Text = CADcostHelper.getActivityName(cadreq.ID_ACTIVITY);
                PanelCancel.Update();
                ButtonCancelOK.CommandArgument = e.CommandArgument.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#buttonCancel').click()", true);
            }
        }

        protected void GridViewLicenseRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.PageIndex = e.NewPageIndex;
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListStatus.SelectedValue));
            GridViewLicenseRequest.DataBind();
        }

        protected void GridViewActionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewActionHistory.PageIndex = e.NewPageIndex;
            GridViewActionHistory.DataSource = CADcostHelper.getAllActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListAction.SelectedValue));
            GridViewActionHistory.DataBind();
        }
        
        protected void ButtonViewCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#viewClose').click()", true);
        }

        protected void ButtonCancelCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#cancelClose').click()", true);
        }

        protected void ButtonCancelOK_Click(object sender, EventArgs e)
        {
            DateTime timeStart = DateTime.Now;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            Button btn = (Button)sender;
            
            CADcostHelper.cancelRequest(Convert.ToInt32(btn.CommandArgument), logInUser.EMP_ID);
            ACTION_HISTORY act = new ACTION_HISTORY();
            act.ID_ACTION = 4; //reject
            act.ID_CADREQ = Convert.ToInt32(btn.CommandArgument);
            act.JOBCODE = DropDownListProject.SelectedValue;
            act.TIME_START = timeStart;
            act.TIME_END = DateTime.Now;
            act.COMMENT = TextBoxCommentCancel.Text;
            if (logInUser.ISPARTICIPANT)
            {
                act.PARTICIPANT = logInUser.EMP_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
            }
            else
            {
                act.PARTICIPANT = logInUser.PARTICIPANT_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
                act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
            }
            CADcostHelper.insertActionHistory(act);
            WorkflowHelper.sendEmailCanceled(Convert.ToInt32(btn.CommandArgument));
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#cancelClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
        }

        protected void ButtonSearchJobcode_Click(object sender, EventArgs e)
        {
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            DropDownListProject_SelectedIndexChanged(sender, e);
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            updatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
        }

        protected void GridViewLicenseRequest_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label status = e.Row.FindControl("LabelStatus") as Label;
                Button lbCancelRequest = e.Row.FindControl("LinkButtonRequestHistoryCancel") as Button;
                Button lbEditRequest = e.Row.FindControl("LinkButtonRequestHistoryEdit") as Button;
                if (status.Text.Equals("FIX_REQUEST"))
                    status.CssClass = "label label-default";
                else if (status.Text.Equals("CANCELED_REQUEST"))
                {
                    status.CssClass = "label label-danger";
                    lbCancelRequest.Visible = false;
                    lbEditRequest.Visible = false;
                }
                else if (status.Text.Equals("PREPARE_REQUEST"))
                    status.CssClass = "label label-default";
                else if (status.Text.Equals("REJECTED_REQUEST"))
                    status.CssClass = "label label-danger";
                else if (status.Text.Equals("APPROVAL_BY_FIRST_RATER"))
                {
                    status.CssClass = "label label-info";
                    lbCancelRequest.Visible = false;
                    lbEditRequest.Visible = false;
                }
                else if (status.Text.Equals("APPROVAL_BY_SECOND_RATER"))
                {
                    status.CssClass = "label label-primary";
                    lbCancelRequest.Visible = false;
                    lbEditRequest.Visible = false;
                }
                else if (status.Text.Equals("APPROVAL_BY_ENIT"))
                {
                    status.CssClass = "label label-warning";
                    lbCancelRequest.Visible = false;
                    lbEditRequest.Visible = false;
                }
                else if (status.Text.Equals("APPROVAL_BY_ENIT_MANAGER"))
                {
                    status.CssClass = "label label-success";
                    lbCancelRequest.Visible = false;
                    lbEditRequest.Visible = false;
                }

                Label OnBehalf = e.Row.FindControl("LabelOnBehalf") as Label;
                if(OnBehalf.Text.Equals(logInUser.FULL_NAME))
                    OnBehalf.Text = "-";
            }
        }

        protected void GridViewActionHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label labelNameAction = e.Row.FindControl("LabelNameAction") as Label;
                if (labelNameAction.Text.Equals("SUBMIT_REQUEST"))
                    labelNameAction.CssClass = "label label-primary";
                else if (labelNameAction.Text.Equals("CANCEL_REQUEST"))
                    labelNameAction.CssClass = "label label-warning";
                else if (labelNameAction.Text.Equals("SAVE_CHANGE_AND_SUBMIT_REQUEST"))
                    labelNameAction.CssClass = "label label-info";
                else if (labelNameAction.Text.Equals("SAVE_AS_DRAFT"))
                    labelNameAction.CssClass = "label label-default";
                else if (labelNameAction.Text.Equals("RETURN_TO_INITIATOR"))
                    labelNameAction.CssClass = "label label-danger";
                else if (labelNameAction.Text.Equals("APPROVE"))
                    labelNameAction.CssClass = "label label-success";

                Label CADReqBy = e.Row.FindControl("LabelCADReqBy") as Label;
                Label OnBehalf = e.Row.FindControl("LabelOnBehalf") as Label;
                if (CADReqBy.Text.Equals(OnBehalf.Text))
                    OnBehalf.Text = "-";
                Label ActOnBehalf = e.Row.FindControl("LabelActOnBehalf") as Label;
                if (ActOnBehalf.Text.Equals(logInUser.FULL_NAME))
                    ActOnBehalf.Text = "-";
            }
        }

        protected void DropDownListMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = null;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListStatus.SelectedValue));
            GridViewLicenseRequest.DataBind();
            GridViewActionHistory.DataSource = CADcostHelper.getAllActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListAction.SelectedValue));
            GridViewActionHistory.DataBind();
            updatepanelreqhistory.Update();
        }

        protected void DropDownListYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = null;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListStatus.SelectedValue));
            GridViewLicenseRequest.DataBind();
            GridViewActionHistory.DataSource = CADcostHelper.getAllActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListAction.SelectedValue));
            GridViewActionHistory.DataBind();
            updatepanelreqhistory.Update();
        }

        protected void DropDownListCurrencyCancel_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewCancel.DataSource = CADcostHelper.getItem(Convert.ToInt32(ButtonViewCancel.CommandArgument.ToString()), 0);
            GridViewCancel.DataBind();
        }

        protected void DropDownListCurrencyView_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewView.DataSource = CADcostHelper.getItem(Convert.ToInt32(ButtonViewCancel.CommandArgument.ToString()), 0);
            GridViewView.DataBind();
            
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
        }

        protected void GridViewActionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("view"))
            {
                GridViewView.DataSource = CADcostHelper.getItem(Convert.ToInt32(e.CommandArgument.ToString()), 0);
                GridViewView.DataBind();
                CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(e.CommandArgument.ToString()));
                LabelCADReqNo.Text = cadreq.CADREQ_NO;
                LabelProject.Text = cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE);
                LabelCADReqBy.Text = cadreq.PIC + " - " + PFNATTHelper.getEMPFullName(cadreq.PIC);
                LabelOnBehalfBy.Text = cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY);
                LabelDepartment.Text = PFNATTHelper.getSectName(PFNATTHelper.getEMPSect(cadreq.PIC));
                LabelForMonth.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(cadreq.FOR_MONTH);
                LabelForYear.Text = cadreq.FOR_YEAR.ToString();
                LabelTimeReq.Text = cadreq.TIME_REQUEST.ToString();
                LabelStatus.Text = CADcostHelper.getActivityName(cadreq.ID_ACTIVITY);
                LabelDescription.Text = cadreq.DESCRIPTION;
                LabelCommentView.Text = CADcostHelper.getLastActivityComment(Convert.ToInt32(e.CommandArgument.ToString()));
                ButtonViewCancel.CommandArgument = e.CommandArgument.ToString();
                PanelView.Update();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#buttonView').click()", true);

            }
        }

        protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewLicenseRequest.DataSource = CADcostHelper.getLicenseHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListStatus.SelectedValue));
            GridViewLicenseRequest.DataBind();
            updatepanelreqhistory.Update();
        }

        protected void DropDownListAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            GridViewActionHistory.DataSource = CADcostHelper.getAllActionHistory(DropDownListProject.SelectedValue.ToString(), logInUser.EMP_ID, Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue), Convert.ToInt32(DropDownListAction.SelectedValue));
            GridViewActionHistory.DataBind();
            updatepanelreqhistory.Update();
        }

        protected void GridViewCadreqActionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewCadreqActionHistory.PageIndex = e.NewPageIndex;
            GridViewCadreqActionHistory.DataSource = CADcostHelper.getCadreqActionHistory(Convert.ToInt32(ButtonViewCancel.CommandArgument));
            GridViewCadreqActionHistory.DataBind();
            PanelView.Update();
        }
    }
}