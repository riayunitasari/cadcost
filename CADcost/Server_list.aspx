﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Server_list.aspx.cs" Inherits="CADcost.Server_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
        <legend>Server List</legend>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
    <asp:GridView ID="GridViewServerList" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true" OnRowEditing="GridViewServerList_RowEditing" OnRowDeleting="GridViewServerList_RowDeleting" OnRowUpdating="GridViewServerList_RowUpdating" OnRowCancelingEdit="GridViewServerList_RowCancelingEdit" Width="80%" HorizontalAlign="Center" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddServer" runat="server" Text="Add" OnClick="ButtonAddServer_Click1" CssClass="btn btn-primary"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Server Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServer" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:Label ID="LabelServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServerEdit" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:TextBox ID="TextBoxServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxServerNameAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IP Server">
                <ItemTemplate>
                    <asp:Label ID="LabelIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxIPServerAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateServer" runat="server" CommandName="Update" Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelServer" runat="server" CommandName="Cancel" Text="Cancel"/>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditServer" runat="server" CommandName="Edit" Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteServer" runat="server" CommandName="Delete" Text="Delete"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="LightGray" />
        <RowStyle HorizontalAlign="Center"></RowStyle>

    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
</asp:Content>
