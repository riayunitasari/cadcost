﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class Software_cost : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            switch (logInUser.ISADMIN)
            {
                case false:
                    Response.Redirect("~/Beranda.aspx");
                    break;
            }
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                CADcostHelper.generateDDLSoftwareName(DropDownListSoftware);
                DropDownListCurrency.Items.Add(new ListItem("USD", "0"));
                CADcostHelper.generateDDLCurrency(DropDownListCurrency);
                List<SoftwareCost> cost = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
                if (cost.Count > 0)
                {
                    GridViewSoftwareCost.DataSource = cost;
                    GridViewSoftwareCost.DataBind();
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("No.", typeof(int));
                    dt.Columns.Add("ID_COST", typeof(int));
                    dt.Columns.Add("NAME_SOFTWARE", typeof(string));
                    dt.Columns.Add("CAD_COST", typeof(decimal));
                    DataRow dtRow = dt.NewRow();
                    dt.Rows.Add(dtRow);

                    GridViewSoftwareCost.DataSource = dt;
                    GridViewSoftwareCost.DataBind();

                    GridViewSoftwareCost.Rows[0].Visible = false;
                }

                GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
                GridViewSoftwareCostHistory.DataBind();
            }
        }

        protected void ButtonAddCost_Click(object sender, EventArgs e)
        {
            GridViewRow row = GridViewSoftwareCost.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareAdd") as DropDownList;
            DropDownList ddlCurrency = row.FindControl("DropDownListCurrencyAdd") as DropDownList;
            TextBox tbType = row.FindControl("TextBoxTypeAdd") as TextBox;
            TextBox tbCADCost = row.FindControl("TextBoxCADCostAdd") as TextBox;
            SOFTWARE_COST cost = new SOFTWARE_COST();
            cost.ID_SOFTWARE = Convert.ToInt32(ddlSoftware.SelectedValue);
            cost.TYPE = tbType.Text;
            cost.COST = Math.Round(Convert.ToDecimal(tbCADCost.Text), 2);
            cost.COST_TO_COST = 0;
            cost.CAD_STARTDATE = DateTime.Now;
            CADcostHelper.insertSoftwareCost(cost, Convert.ToInt32(ddlCurrency.SelectedValue));
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
            GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCostHistory.DataBind();
        }
        
        protected void GridViewSoftwareCost_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewSoftwareCost.EditIndex = index;
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
        }

        protected void GridViewSoftwareCost_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewSoftwareCost.Rows[e.RowIndex];
            HiddenField hfIDCost = row.FindControl("HiddenFieldIDCost") as HiddenField;
            CADcostHelper.deleteSoftwareCost(Convert.ToInt32(hfIDCost.Value));
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
            GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCostHistory.DataBind();
        }

        protected void GridViewSoftwareCost_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewSoftwareCost.Rows[e.RowIndex];
            HiddenField hfIDCost = row.FindControl("HiddenFieldIDCostEdit") as HiddenField;
            TextBox tbCADCost = row.FindControl("TextBoxCADCost") as TextBox;
            TextBox tbType = row.FindControl("TextBoxType") as TextBox;
            SOFTWARE_COST cost = new SOFTWARE_COST();
            cost.ID_COST = Convert.ToInt32(hfIDCost.Value);
            cost.TYPE = tbType.Text;
            cost.COST = Convert.ToDecimal(tbCADCost.Text);
            cost.COST_TO_COST = 0;
            CADcostHelper.updateSoftwareCost(cost, Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.EditIndex = -1;
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
            GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCostHistory.DataBind();
        }

        protected void GridViewSoftwareCost_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewSoftwareCost.EditIndex = -1;
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
        }
        
        protected void DropDownListSoftware_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCostHistory.DataBind();
        }

        protected void GridViewSoftwareCost_DataBound(object sender, EventArgs e)
        {
            GridViewRow row = GridViewSoftwareCost.FooterRow;
            DropDownList ddlSoftware = row.FindControl("DropDownListSoftwareAdd") as DropDownList;
            Helper.CADcostHelper.generateDDLSoftwareName(ddlSoftware);
            DropDownList ddlCurrency = row.FindControl("DropDownListCurrencyAdd") as DropDownList;
            ddlCurrency.Items.Add(new ListItem("USD", "0"));
            CADcostHelper.generateDDLCurrency(ddlCurrency);
        }

        protected void DropDownListCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewSoftwareCost.DataSource = CADcostHelper.getAllSoftwareCost(Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCost.DataBind();
            GridViewSoftwareCostHistory.DataSource = CADcostHelper.getAllSoftwareCostHistory(Convert.ToInt32(DropDownListSoftware.SelectedValue), Convert.ToInt32(DropDownListCurrency.SelectedValue));
            GridViewSoftwareCostHistory.DataBind();
        }

        protected void GridViewSoftwareCost_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) != DataControlRowState.Edit)
            {
                Label type = e.Row.FindControl("LabelType") as Label;
                if (type.Text.Equals(""))
                    type.Text = "Default";
            }
        }

        protected void GridViewSoftwareCostHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label type = e.Row.FindControl("LabelType") as Label;
                if (type.Text.Equals(""))
                    type.Text = "Default";
            }
        }
    }
}