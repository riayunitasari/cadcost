﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class Software_list : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            switch (logInUser.ISADMIN)
            {
                case false:
                    Response.Redirect("~/Beranda.aspx");
                    break;
            }
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                GridViewSoftwareList.DataSource = CADcostHelper.getAllSoftwareVendor();
                GridViewSoftwareList.DataBind();
                GridViewVendor.DataSource = CADcostHelper.getAllVendor();
                GridViewVendor.DataBind();
                CADcostHelper.generateDDLVendorName(DropDownListVendor);
            }
        }

        protected void ButtonAddSoftwareOK_Click(object sender, EventArgs e)
        {
            SOFTWARE sfwr = new SOFTWARE();
            sfwr.NAME_SOFTWARE = TextBoxSoftwareName.Text;
            try
            {
                sfwr.DATE_RENEWAL = Convert.ToDateTime(TextBoxDateRenewal.Text);
            }
            catch
            {
 
            }
            CADcostHelper.insertSoftware(sfwr, Convert.ToInt32(DropDownListVendor.SelectedValue));
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#softwareClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonAddSoftwareCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#softwareClose').click()", true);
        }

        protected void ButtonAddSoftware_Click(object sender, EventArgs e)
        {
            CADcostHelper.generateDDLVendorName(DropDownListVendor);
            UpdatePanelSoftware.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertApprove", "$('#ButtonSoftware').click()", true);
        }

        protected void ButtonAddVendor_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertApprove", "$('#ButtonVendor').click()", true);
        }

        protected void ButtonAddVendorOK_Click(object sender, EventArgs e)
        {
            VENDOR vndr = new VENDOR();
            vndr.NAME_VENDOR = TextBoxVendorName.Text;
            vndr.CONTACT = TextBoxContact.Text;
            vndr.EMAIL = TextBoxEmail.Text;
            vndr.VENDOR_DESCRIPTION = TextBoxDescription.Text;
            vndr.WEBSITE = TextBoxWebsite.Text;
            CADcostHelper.insertVendor(vndr);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#vendorClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonAddVendorCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#vendorClose').click()", true);
        }

        protected void GridViewSoftwareList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewSoftwareList.EditIndex = index;
            GridViewSoftwareList.DataSource = CADcostHelper.getAllSoftwareVendor();
            GridViewSoftwareList.DataBind();
        }

        protected void GridViewSoftwareList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewSoftwareList.Rows[e.RowIndex];
            HiddenField hfIDSoftware = row.FindControl("HiddenFieldIDSoftware") as HiddenField;
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDVendor") as HiddenField;
            CADcostHelper.deleteSoftware(Convert.ToInt32(hfIDSoftware.Value), Convert.ToInt32(hfIDVendor.Value));
            GridViewSoftwareList.DataSource = CADcostHelper.getAllSoftwareVendor();
            GridViewSoftwareList.DataBind();
        }

        protected void GridViewSoftwareList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewSoftwareList.Rows[e.RowIndex];
            HiddenField hfIDSoftware = row.FindControl("HiddenFieldIDSoftwareEdit") as HiddenField;
            TextBox tbSoftwareName = row.FindControl("TextBoxSoftwareName") as TextBox;
            TextBox tbDateRenewal = row.FindControl("TextBoxDateRenewal") as TextBox;
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDVendorEdit") as HiddenField;
            DropDownList ddlVendor = row.FindControl("DropDownListVendorName") as DropDownList;
            SOFTWARE sfwr = new SOFTWARE();
            sfwr.ID_SOFTWARE = Convert.ToInt32(hfIDSoftware.Value);
            sfwr.NAME_SOFTWARE = tbSoftwareName.Text;
            try
            {
                sfwr.DATE_RENEWAL = Convert.ToDateTime(tbDateRenewal.Text);
            }
            catch
            {
 
            }
            CADcostHelper.updateSoftware(sfwr, Convert.ToInt32(ddlVendor.SelectedValue.ToString()));
            GridViewSoftwareList.EditIndex = -1;
            GridViewSoftwareList.DataSource = CADcostHelper.getAllSoftwareVendor();
            GridViewSoftwareList.DataBind();
        }

        protected void GridViewSoftwareList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewSoftwareList.EditIndex = -1;
            GridViewSoftwareList.DataSource = CADcostHelper.getAllSoftwareVendor();
            GridViewSoftwareList.DataBind();
        }

        protected void GridViewSoftwareList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                SoftwareVendor itemedit = e.Row.DataItem as SoftwareVendor;
                DropDownList ddl = e.Row.FindControl("DropDownListVendorName") as DropDownList;
                CADcostHelper.generateDDLVendorName(ddl);
                ddl.SelectedValue = itemedit.ID_VENDOR.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "datepicker('ContentPlaceHolder1_GridViewSoftwareList_TextBoxDateRenewal_" + e.Row.RowIndex + "');", true);
            }
        }

        protected void GridViewVendor_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewVendor.EditIndex = index;
            GridViewVendor.DataSource = CADcostHelper.getAllVendor();
            GridViewVendor.DataBind();
        }

        protected void GridViewVendor_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewVendor.Rows[e.RowIndex];
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDVendor") as HiddenField;
            CADcostHelper.deleteVendor(Convert.ToInt32(hfIDVendor.Value));
            GridViewVendor.DataSource = CADcostHelper.getAllVendor();
            GridViewVendor.DataBind();
        }

        protected void GridViewVendor_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewVendor.Rows[e.RowIndex];
            HiddenField hfIDVendor = row.FindControl("HiddenFieldIDVendorEdit") as HiddenField;
            TextBox tbVendorName = row.FindControl("TextBoxVendorName") as TextBox;
            TextBox tbContact = row.FindControl("TextBoxContact") as TextBox;
            TextBox tbEmail = row.FindControl("TextBoxEmail") as TextBox;
            TextBox tbWebsite = row.FindControl("TextBoxWebsite") as TextBox;
            TextBox tbVendorDescription = row.FindControl("TextBoxVendorDescription") as TextBox;
            VENDOR vndr = new VENDOR();
            vndr.ID_VENDOR = Convert.ToInt32(hfIDVendor.Value);
            vndr.NAME_VENDOR = tbVendorName.Text;
            vndr.CONTACT = tbContact.Text;
            vndr.EMAIL = tbEmail.Text;
            vndr.WEBSITE = tbWebsite.Text;
            vndr.VENDOR_DESCRIPTION = tbVendorDescription.Text;
            CADcostHelper.updateVendor(vndr);
            GridViewVendor.EditIndex = -1;
            GridViewVendor.DataSource = CADcostHelper.getAllVendor();
            GridViewVendor.DataBind();
        }

        protected void GridViewVendor_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewVendor.EditIndex = -1;
            GridViewVendor.DataSource = CADcostHelper.getAllVendor();
            GridViewVendor.DataBind();
        }

        
    }
}