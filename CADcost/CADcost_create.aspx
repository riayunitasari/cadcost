﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_create.aspx.cs" Inherits="CADcost.CADcost_create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <button type="button" id="buttonSubmit" class="btn btn-warning" data-toggle="modal" data-target="#dialogSubmit" style="display:none;">View</button>
    <asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Conditional">
                        <ContentTemplate>
                            
    <asp:Table ID="TableCreateCADcost" runat="server" Width="100%" GridLines="Horizontal" ForeColor="Black" CellPadding="10" CssClass="table table-striped table-hover">
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">CAD Cost Request NO : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelCADcostNO" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">Requester : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelRequester" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Department : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelDepartment" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Project : </asp:TableCell>
            <asp:TableCell ColumnSpan="2" >
                <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                    <ContentTemplate>
                <asp:DropDownList ID="DropDownListProject" runat="server" VerticalAlign="Middle" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                <asp:Label ID="LabelProject" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">For Month : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="DropDownListMonth" runat="server" CssClass="form-control"></asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">For Year : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="DropDownListYear" runat="server" CssClass="form-control"></asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">
                Item :
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                 <asp:GridView ID="GridViewItem" runat="server" HorizontalAlign="Center" Font-Size="Small" ShowFooter="True" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnDataBound="GridViewItem_DataBound"  Width="90%" OnRowEditing="GridViewItem_RowEditing" OnRowUpdating="GridViewItem_RowUpdating" OnRowDeleting="GridViewItem_RowDeleting" OnRowDataBound="GridViewItem_RowDataBound" OnRowCancelingEdit="GridViewItem_RowCancelingEdit" RowStyle-HorizontalAlign="Center" Font-Names="Trebuchet MS" CssClass="table table-striped table-bordered table-hover">

        <Columns>
            <asp:TemplateField HeaderText="No.">
                <FooterTemplate>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary" ValidationGroup="quantity"/>
                </FooterTemplate>
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Software Name">
                <ItemTemplate>

                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>' Width="80%"></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameEdit" runat="server" Width="80%" CssClass="form-control"></asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameAdd" runat="server" Width="80%" CssClass="form-control"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <ItemTemplate>

                        <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBoxQuantityEdit" runat="server" Text='<%# Eval("QUANTITY") %>' Width="80%" CssClass="form-control input-sm"></asp:TextBox>
                        
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="TextBoxQuantityAdd" runat="server" Width="80%"  CssClass="form-control input-sm"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorQuantityAdd" runat="server" ErrorMessage="Required" ControlToValidate="TextBoxQuantityAdd" ValidationGroup="quantity"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
                        
                        
                    </FooterTemplate>

                </asp:TemplateField>
            <asp:TemplateField HeaderText="Remark">
                <ItemTemplate>
                    <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxRemarkEdit" runat="server" Text='<%# Eval("REMARK") %>'  CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxRemarkAdd" runat="server" Text='<%# Eval("REMARK") %>' Width="100%"  CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdate" runat="server" CommandName="Update" Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancel" runat="server" CommandName="Cancel" Text="Cancel"/>

                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEdit" runat="server" CommandName="Edit" Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDelete" runat="server" CommandName="Delete" Text="Delete"/>

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>


<RowStyle HorizontalAlign="Center"></RowStyle>
<FooterStyle HorizontalAlign="Center" />
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    
   
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">
                Description:
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:TextBox ID="TextBoxDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                <asp:Label ID="LabelDescription" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" class="btn btn-primary" OnClick="ButtonSubmit_Click" CausesValidation="false"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSaveAsDraft" runat="server" Text="SaveAsDraft" class="btn btn-warning" OnClick="ButtonSaveAsDraft_Click"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="ButtonCancel_Click"/>
                </span>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
                        </ContentTemplate>
        </asp:UpdatePanel>
    <div class="modal fade" id="dialogSubmit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="submitClose">&times;</button>
          <h4 class="modal-title">Summary</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="PanelSubmit" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                <asp:Table ID="Table1" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <h3>Software License Request Summary</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Project</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelProjectSummary" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Month</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForMonth" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Year</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForYear" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:GridView ID="GridViewItemSummary" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Description</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDescriptionSummmary" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment:</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxComment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
            
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                <asp:Button runat="server" class="btn btn-success" Text="OK" ID="ButtonConfirmOK" OnClick="ButtonConfirmOK_Click"/>
                                <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonConfirmCancel" OnClick="ButtonConfirmCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                  </ContentTemplate>

              </asp:UpdatePanel>
    </div>
          </div>
        </div>
        </div>
     
    <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        <asp:Panel ID="PanelProject" runat="server">
            <asp:Table ID="Table2" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" class="btn btn-primary"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="5" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
              </div>
          </div>
        </div>
    </div>
</asp:Content>
