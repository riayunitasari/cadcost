﻿using CADcost.Entities;
using CADcost.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CADcost
{
    public partial class Software_server : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            switch (logInUser.ISADMIN)
            {
                case false:
                    Response.Redirect("~/Beranda.aspx");
                    break;
            }
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                Label linkLogin = Master.FindControl("LabelLogin") as Label;
                linkLogin.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                CADcostHelper.generateDDLProjectName(DropDownListProject);
                DropDownListMonth.Items.Add(new ListItem("January", "1"));
                DropDownListMonth.Items.Add(new ListItem("February", "2"));
                DropDownListMonth.Items.Add(new ListItem("March", "3"));
                DropDownListMonth.Items.Add(new ListItem("April", "4"));
                DropDownListMonth.Items.Add(new ListItem("May", "5"));
                DropDownListMonth.Items.Add(new ListItem("June", "6"));
                DropDownListMonth.Items.Add(new ListItem("July", "7"));
                DropDownListMonth.Items.Add(new ListItem("August", "8"));
                DropDownListMonth.Items.Add(new ListItem("September", "9"));
                DropDownListMonth.Items.Add(new ListItem("October", "10"));
                DropDownListMonth.Items.Add(new ListItem("November", "11"));
                DropDownListMonth.Items.Add(new ListItem("December", "12"));
                DropDownListMonth.SelectedValue = DateTime.Now.Month.ToString();
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                DropDownListYear.SelectedValue = DateTime.Now.Year.ToString();
                DropDownListStatus.Items.Add(new ListItem("Owned", "true"));
                DropDownListStatus.Items.Add(new ListItem("Additional", "false"));
                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));
                if (Session["jobcode"] != null)
                {
                    DropDownListProject.SelectedValue = Session["jobcode"].ToString();
                }
                if (Session["month"] != null)
                {
                    DropDownListMonth.SelectedValue = Session["month"].ToString();
                }
                if (Session["year"] != null)
                {
                    DropDownListYear.SelectedValue = Session["year"].ToString();
                }

                GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
                GridViewSoftwareServer.DataBind();
                
                CADcostHelper.generateDDLSoftwareName(DropDownListSoftware);
                DropDownListDepartment.Items.Add(new ListItem("ALL", "ALL"));
                PFNATTHelper.generateDDLDepartment(DropDownListDepartment);
                CADcostHelper.generateDDLServer(DropDownListServerAdd);
                CADcostHelper.updateCurrentSoftwareServer();
                UpdatePanelLicenseServer.Update();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
        }

        protected void ButtonAddLicenseServer_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertAddLicenseServer", "$('#ButtonLicense').click()", true);
        }

        protected void ButtonAddLicenseServerOK_Click(object sender, EventArgs e)
        {
            SOFTWARE_SERVER sfwrServer = new SOFTWARE_SERVER();
            sfwrServer.ID_SOFTWARE = Convert.ToInt32(DropDownListSoftware.SelectedValue.ToString());
            sfwrServer.DEPT = DropDownListDepartment.SelectedValue.ToString();
            sfwrServer.ID_SERVER = Convert.ToInt32(DropDownListServerAdd.SelectedValue.ToString());
            sfwrServer.JOBCODE_ID = DropDownListProject.SelectedValue.ToString();
            sfwrServer.LAST_UPDATED = DateTime.Now;

            if (DropDownListStatus.SelectedValue.Equals("true"))
            {
                sfwrServer.LICENSE_STATUS = true;
                sfwrServer.QUANTITY = Convert.ToInt32(TextBoxQuantity.Text);
            }
            else 
            {
                sfwrServer.LICENSE_STATUS = false;
                sfwrServer.QUANTITY = 0;
            }

            
            CADcostHelper.insertSoftwareServer(sfwrServer);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertLicenseClose", "$('#licenseClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonAddLicenseServerCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertLicenseClose", "$('#licenseClose').click()", true);
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = DropDownListProject.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
            UpdatePanel1.Update();
        }

        protected void DropDownListMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["month"] = DropDownListMonth.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
            UpdatePanel1.Update();
        }

        protected void DropDownListYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["year"] = DropDownListYear.SelectedValue;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
            UpdatePanel1.Update();
        }

        protected void GridViewSoftwareServer_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewSoftwareServer.EditIndex = index;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected bool FunctionHideQuantityTextbox(int id_software_server)
        {
            if (!CADcostHelper.getSoftwareServerStatus(id_software_server))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void GridViewSoftwareServer_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewSoftwareServer.Rows[e.RowIndex];
            HiddenField hfIDSoftwareServer = row.FindControl("HiddenFieldIDSoftwareServer") as HiddenField;
            CADcostHelper.deleteSoftwareServer(Convert.ToInt32(hfIDSoftwareServer.Value));
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewSoftwareServer.Rows[e.RowIndex];
            HiddenField hfIDSoftwareServer = row.FindControl("HiddenFieldIDSoftwareServer") as HiddenField;
            DropDownList ddlServer = row.FindControl("DropDownListServer") as DropDownList;
            TextBox tbOwned = row.FindControl("TextBoxOwned") as TextBox;
            Label lbStatus = row.FindControl("LabelLicenseStatus") as Label;
            TextBox tbRemark = row.FindControl("TextBoxRemark") as TextBox;
            CADcostHelper.updateSoftwareServer(Convert.ToInt32(hfIDSoftwareServer.Value), ((lbStatus.Text == "Owned") ? true:false), Convert.ToInt32(ddlServer.SelectedValue), Convert.ToInt32(tbOwned.Text), tbRemark.Text);
            GridViewSoftwareServer.EditIndex = -1;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewSoftwareServer.EditIndex = -1;
            GridViewSoftwareServer.DataSource = CADcostHelper.getAllSoftwareServerProject(DropDownListProject.SelectedValue.ToString(), Convert.ToInt32(DropDownListMonth.SelectedValue), Convert.ToInt32(DropDownListYear.SelectedValue));
            GridViewSoftwareServer.DataBind();
        }

        protected void GridViewSoftwareServer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                SoftwareServer itemedit = e.Row.DataItem as SoftwareServer;
                DropDownList ddl = e.Row.FindControl("DropDownListServer") as DropDownList;
                CADcostHelper.generateDDLServer(ddl);
                ddl.SelectedValue = itemedit.ID_SERVER.ToString();

                Label lbLicenseStatus = e.Row.FindControl("LabelLicenseStatus") as Label;
                Label lbQuantity = e.Row.FindControl("LabelQuantity") as Label;
                TextBox tbOwned = e.Row.FindControl("TextBoxOwned") as TextBox;
                if (lbLicenseStatus.Text.Equals("Owned"))
                {
                    lbQuantity.Text += "/";
                }
                else if (lbLicenseStatus.Text.Equals("Additional"))
                {
                    tbOwned.Visible = false;
                }
            } 
            else if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) != DataControlRowState.Edit)
            {
                Label lbLicenseStatus = e.Row.FindControl("LabelLicenseStatus") as Label;
                Label lbQuantity = e.Row.FindControl("LabelQuantity") as Label;
                Label lbOwned = e.Row.FindControl("LabelOwned") as Label;
                if (lbLicenseStatus.Text.Equals("Owned"))
                {
                    lbQuantity.Text += "/";
                }
                else if (lbLicenseStatus.Text.Equals("Additional"))
                {
                    lbOwned.Text = "";
                }
            }
        }

        protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListStatus.SelectedValue.Equals("false"))
            {
                TextBoxQuantity.ReadOnly = true;
                TextBoxQuantity.Text = "";
                UpdatePanelQuantity.Update();
            }
            else
            {
                TextBoxQuantity.ReadOnly = false;
                UpdatePanelQuantity.Update();
            }
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            //ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            DropDownListProject_SelectedIndexChanged(sender, e);
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            updatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
            
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }
    }
}