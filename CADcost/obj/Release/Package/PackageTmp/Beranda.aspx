﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Beranda.aspx.cs" Inherits="CADcost.Beranda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/stylesheet">
        #contents {
        color: black;
        }

        .header-center{
          text-align:center !important;
        }
    </script>
    <script type="text/javascript">
            function hideapproval() {
                    var loader = document.getElementById("approval");
                    loader.style.display = 'none';
                    var loader = document.getElementById("home");
                    loader.className = 'col-lg-12';
                    return false;
            }

            function buttonclick(id_cadreq) {
                var btn = document.getElementById("ButtonApprove");
                btn.innerHTML = id_cadreq;
                document.getElementById("ButtonApprove").click();
                
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-4" id="home">
            <div class="well" style="text-align:center">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/JGC-Icon.png" ImageAlign="Middle"/>
                <h4 style="text-align:center">License Request System</h4><br/><br />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/AddIcon.png" Width="50px" Height="50px"/>
                <asp:Button ID="ButtonCreateNew" runat="server" Text="Create Request" PostBackUrl="~/CADcost_create.aspx?act=0" class="btn btn-primary" /><br /><br />
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/HistoryIcon.png" Width="50px" Height="50px"/>
                <asp:Button ID="ButtonRecord" runat="server" Text="Request and Action History" PostBackUrl="~/CADcost_record.aspx" class="btn btn-primary"/><br />

            </div>
        </div>
        <div class="col-lg-8" id="approval">
            <div class="alert alert-danger">
    <div class="overlow post-holder-sm extra">
        
        <button type="button" id="ButtonApprove" class="btn btn-warning" data-toggle="modal" data-target="#dialogApprove" style="display:none;">View</button>
        <button type="button" id="ButtonServer" class="btn btn-warning" data-toggle="modal" data-target="#dialogServer" style="display:none;">View</button>
             
    <div class="well ">
        <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
            <ContentTemplate>
    <legend>Approval - Need to be Approved <asp:Label ID="LabelRole" runat="server" Text="" ></asp:Label></legend>
    <asp:Table ID="Table3" runat="server" HorizontalAlign="Center" Width="100%">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell>
                    <button type="button" id="ButtonSearchJobcode" class="btn btn-warning" data-toggle="modal" data-target="#dialogProject" >...</button>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table><br />
        
    <asp:GridView ID="GridViewToBeApproved" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" 
        AllowPaging="True" EmptyDataText="No Records Yet"
        OnPageIndexChanging="GridViewToBeApproved_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" 
        Width="100%" ForeColor="Black" CssClass="table table-striped table-bordered table-hover">

        <Columns>
            <asp:TemplateField HeaderText="No" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Req Number" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Req By" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DEPT" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Req" HeaderStyle-CssClass="header-center">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Req" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action"  HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton ID="ButtonApproveENIT" runat="server"  CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>' OnClick="ButtonApproveENIT_Click" >View</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />
    </asp:GridView>
                 </ContentTemplate>
    </asp:UpdatePanel>
    </div>
               
    </div>
    </div>
    
    </div>
    </div>



   <div class="modal fade" id="dialogApprove" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="approveClose">&times;</button>
          <h4 class="modal-title">Approval</h4>
        </div>
          <div class="modal-body">
        
               <asp:UpdatePanel ID="PanelApprove" runat="server" UpdateMode="Conditional">
                   <ContentTemplate>
                <asp:Table ID="Table1" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5> CAD Req No</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqNo" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Project</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelProject" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Month</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForMonth" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Year</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForYear" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>CAD Req By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqBy" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>On Behalf By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelOnBehalfBy" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Department</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDepartment" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Time Request</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelTimeReq" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Description</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDescription" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Price Currency</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListCurrencyView" runat="server" OnSelectedIndexChanged="DropDownListCurrencyView_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:UpdatePanel runat="server" ID="PanelViewGrid" UpdateMode="Conditional">
                                <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="DropDownListCurrencyView" EventName="SelectedIndexChanged"/>
                            </Triggers>
                                <ContentTemplate>
                            
                            <asp:GridView ID="GridViewApprove" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666" CssClass="table table-striped table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPrice" runat="server" Text='<%# Eval("PRICE", "{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                    </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Total Price</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelTotalPrice" runat="server" Text="0"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCommentApprove" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment for Approve : </h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxCommentApprove" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <h4>Do You Want to Approve?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Approve" ID="ButtonApproveOK" CommandArgument="" OnClick="ButtonApproveOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Reject" ID="ButtonApproveReject" CommandArgument="" OnClick="ButtonApproveReject_Click"/>
                            <asp:Button runat="server" class="btn btn-warning" Text="Cancel" ID="ButtonApproveCancel" OnClick="ButtonApproveCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                       </ContentTemplate>
            </asp:UpdatePanel>
     </div>
          </div>
        </div>
       </div>

    <div class="modal fade" id="dialogServer" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="serverClose">&times;</button>
          <h4 class="modal-title">Error Approve</h4>
        </div>
          <div class="modal-body">
            <asp:Panel ID="PanelServer" runat="server">
                <asp:Table ID="Table2" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>License Server / Cost Must Be Added</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewAddedServer" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Add Server?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="btn btn-primary" Text="Go To List Software Server" ID="ButtonAddServer" CommandArgument="" OnClick="ButtonAddServer_Click"/>
                            <asp:Button runat="server" class="btn btn-warning" Text="Go To List Software Cost" ID="ButtonAddCost" CommandArgument="" OnClick="ButtonAddCost_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddCancel" OnClick="ButtonAddCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
              </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        
            <asp:Table runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="3" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
              </div>
          </div>
        </div>
        
    </div>
    
</asp:Content>
