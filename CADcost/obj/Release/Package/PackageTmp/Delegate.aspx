﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Delegate.aspx.cs" Inherits="CADcost.Delegate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
    <legend><h3>Set Delegate</h3></legend>
    <asp:Table runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <h5>Employee to be delegated</h5>
            </asp:TableCell>
            <asp:TableCell>
                : <asp:TextBox ID="TextBoxEmployeeDelegated" runat="server" CssClass="input-sm"></asp:TextBox>
                
            </asp:TableCell>
            <asp:TableCell>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpID1" runat="server" ErrorMessage="Required" ControlToValidate="TextBoxEmployeeDelegated" ValidationGroup="set"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <h5>Set delegate to</h5>
            </asp:TableCell>
            <asp:TableCell>
                : <asp:TextBox ID="TextBoxEmployeeDelegating" runat="server" CssClass="input-sm"></asp:TextBox>
                
            </asp:TableCell>
            <asp:TableCell>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpID2" runat="server" ErrorMessage="Required" ControlToValidate="TextBoxEmployeeDelegating" ValidationGroup="set"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Button ID="ButtonSetDelegate" runat="server" Text="Set Delegate" OnClick="ButtonSetDelegate_Click" class="btn btn-primary" ValidationGroup="set"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        <legend><h3>Release Delegate</h3></legend>
        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell>
                    <h5>Employee To Be Released</h5>
                </asp:TableCell>
                <asp:TableCell>
                    : <asp:TextBox ID="TextBoxEmployeeRelease" runat="server" CssClass="input-sm"></asp:TextBox>
                    
                </asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmpID3" runat="server" ErrorMessage="Required" ControlToValidate="TextBoxEmployeeRelease" ValidationGroup="release"><h3 style="color:red">*</h3></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                    <asp:Button ID="ButtonReleaseDelegate" runat="server" Text="Release Delegate" OnClick="ButtonReleaseDelegate_Click" class="btn btn-primary" ValidationGroup="release"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>

    <div id="dialogDelegate">
        <asp:UpdatePanel runat="server" ID="PanelDelegate" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table3" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Set Delegation</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate for:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegated" runat="server" Text="Label" CssClass="input-sm"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate to:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegating" runat="server" Text="Label" CssClass="input-sm"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Set Delegate" ID="ButtonDelegateOK" OnClick="ButtonDelegateOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonDelegateCancel" OnClick="ButtonDelegateCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>
</asp:Content>
