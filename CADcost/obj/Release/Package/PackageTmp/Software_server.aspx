﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_server.aspx.cs" Inherits="CADcost.Software_server" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <button type="button" id="buttonLicense" class="btn btn-warning" data-toggle="modal" data-target="#dialogLicenseServer" style="display:none;">View</button>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <div class="well ">
        <asp:Table runat="server" Width="100%">
            <asp:TableRow Width="100%">
                <asp:TableCell>
                    <h5>Project:</h5>
                </asp:TableCell>
                <asp:TableCell >
                    <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
                <asp:TableCell>
                <button type="button" id="ButtonSearchJobcode" class="btn btn-warning" data-toggle="modal" data-target="#dialogProject" >...</button>
            </asp:TableCell>
            </asp:TableRow>
        <asp:TableRow >
            <asp:TableCell>
                <h5>Month:</h5> 
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:DropDownList ID="DropDownListMonth" runat="server" OnSelectedIndexChanged="DropDownListMonth_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Width="100%"></asp:DropDownList> 
            </asp:TableCell>
            
        </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                <h5>Year:</h5> 
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:DropDownList ID="DropDownListYear" runat="server" OnSelectedIndexChanged="DropDownListYear_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Width="100%"></asp:DropDownList><br />
            </asp:TableCell>
            </asp:TableRow>
            </asp:Table>
    <legend>License Server List</legend>
    <asp:Button ID="ButtonAddLicenseServer" runat="server" Text="Add License Server" OnClick="ButtonAddLicenseServer_Click" CssClass="btn btn-primary"/>
    <asp:GridView ID="GridViewSoftwareServer" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewSoftwareServer_RowEditing" OnRowDeleting="GridViewSoftwareServer_RowDeleting" OnRowUpdating="GridViewSoftwareServer_RowUpdating" OnRowCancelingEdit="GridViewSoftwareServer_RowCancelingEdit" OnRowDataBound="GridViewSoftwareServer_RowDataBound" Width="100%" HorizontalAlign="Center" CssClass="table table-striped table-bordered table-hover" >
        
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Software">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftwareServer" runat="server"  Value='<%# Eval("ID_SOFTWARE_SERVER") %>'/>
                    <asp:HiddenField ID="HiddenFieldIDSoftware" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="License Status">
                <ItemTemplate>
                    <asp:Label ID="LabelLicenseStatus" runat="server" Text='<%# Eval("LICENSE_STATUS") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="LabelDepartment" runat="server" Text='<%# Eval("DEPT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quantity">
                <ItemTemplate>
                    <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IP Server">
                <ItemTemplate>
                    <asp:Label ID="LabelIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListServer" runat="server" CssClass="form-control"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateCost" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelCost" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditCost" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteCost" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#666666" />

    </asp:GridView>

    <legend>Server List</legend>
    <asp:GridView ID="GridViewServerList" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true" OnRowEditing="GridViewServerList_RowEditing" OnRowDeleting="GridViewServerList_RowDeleting" OnRowUpdating="GridViewServerList_RowUpdating" OnRowCancelingEdit="GridViewServerList_RowCancelingEdit" Width="80%" HorizontalAlign="Center" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddServer" runat="server" Text="Add"  OnClick="ButtonAddServer_Click" CssClass="btn btn-primary"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Server Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServer" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:Label ID="LabelServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDServerEdit" runat="server"  Value='<%# Eval("ID_SERVER") %>'/>
                    <asp:TextBox ID="TextBoxServerName" runat="server" Text='<%# Eval("SERVER_NAME") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxServerNameAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IP Server">
                <ItemTemplate>
                    <asp:Label ID="LabelIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxIPServer" runat="server" Text='<%# Eval("IP_SERVER") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxIPServerAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateServer" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelServer" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditServer" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteServer" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="LightGray" />
        <RowStyle HorizontalAlign="Center"></RowStyle>

    </asp:GridView>
    </div>
            </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="dialogLicenseServer" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="licenseClose">&times;</button>
          <h4 class="modal-title">Add License Server</h4>
        </div>
          <div class="modal-body">
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <asp:Table ID="Table2" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Add License Server</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Software
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListSoftware" runat="server" CssClass="input-sm"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Department
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListDepartment" runat="server" CssClass="input-sm"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Server
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListServerAdd" runat="server" CssClass="input-sm"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Status
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListStatus" runat="server" OnSelectedIndexChanged="DropDownListStatus_SelectedIndexChanged" CssClass="input-sm" AutoPostBack="true"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Quantity
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:UpdatePanel ID="UpdatePanelQuantity" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DropDownListStatus" EventName="SelectedIndexChanged"/>
                                </Triggers>
                                <ContentTemplate>
                            : <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="input-sm"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Add" ID="ButtonAddLicenseServerOK" OnClick="ButtonAddLicenseServerOK_Click" CssClass="btn btn-primary"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonAddLicenseServerCancel" OnClick="ButtonAddLicenseServerCancel_Click" CssClass="btn btn-danger"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        <asp:Panel ID="PanelProject" runat="server">
            <asp:Table ID="Table1" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" class="btn btn-primary"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="3" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
              </div>
          </div>
        </div>
    </div>
</asp:Content>
