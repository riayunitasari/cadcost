﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_record.aspx.cs" Inherits="CADcost.CADcost_record" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                        <ContentTemplate>
    <div class="well ">
        <legend>
            <asp:Table runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell>
                        <h4>Project:</h4>
                    </asp:TableCell>
                    <asp:TableCell>
                        
                        <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        
                    </asp:TableCell>
                    <asp:TableCell>
                        <button type="button" id="ButtonSearchJobcode" class="btn btn-warning" data-toggle="modal" data-target="#dialogProject" >...</button>
                        <button type="button" id="buttonView" class="btn btn-warning" data-toggle="modal" data-target="#dialogView" style="display:none">View</button>
                        <button type="button" id="buttonCancel" class="btn btn-warning" data-toggle="modal" data-target="#dialogCancel" style="display:none">View</button>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <h4>Month:</h4>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        
                        <asp:DropDownList ID="DropDownListMonth" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListMonth_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <h4>Year:</h4>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        
                        <asp:DropDownList ID="DropDownListYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListYear_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </legend>
    <div class="panel panel-primary">
    <div class="panel-heading"><h4>Request History</h4></div>
        <div class="panel-body">
            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updatepanelreqhistory">
                <ContentTemplate>
    <asp:GridView ID="GridViewLicenseRequest" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" 
        OnRowCommand="GridViewLicenseRequest_RowCommand" AllowPaging="True" EmptyDataText="No Records Yet" 
        OnPageIndexChanging="GridViewLicenseRequest_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" 
        CellPadding="10" Width="100%" CssClass="table table-striped table-bordered table-hover"
         OnRowDataBound="GridViewLicenseRequest_RowDataBound">

        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="On Behalf By">
                <ItemTemplate>
                    <asp:Label ID="LabelOnBehalf" runat="server" Text='<%# Eval("ONBEHALF_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="LabelStatus" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action By">
                <ItemTemplate>
                    <asp:Label ID="Label19" runat="server" Text='<%# Eval("ACTION_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonRequestHistoryView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ")%>'>View</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryEdit" runat="server" CommandName="editrequest" CommandArgument='<%# Eval("ID_CADREQ")%>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryCancel" runat="server" CommandName="cancelrequest" CommandArgument='<%# Eval("ID_CADREQ")%>'>Cancel</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" HorizontalAlign="Right"/>
        <RowStyle HorizontalAlign="Center" Font-Names="Trebuchet MS" />
    </asp:GridView>
                    </ContentTemplate>
            </asp:UpdatePanel>
            </div>
    </div>
    <div class="panel panel-danger">
    <div class="panel-heading"><h4>Action History</h4></div>
    <div class="panel-body">
    <asp:GridView ID="GridViewActionHistory" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewActionHistory_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" CellPadding="10" Width="100%" CssClass="table table-striped table-bordered table-hover" OnRowDataBound="GridViewActionHistory_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req Number">
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req By">
                <ItemTemplate>
                    <asp:Label ID="LabelCADReqBy" runat="server" Text='<%# Eval("CADREQ_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="On Behalf By">
                <ItemTemplate>
                    <asp:Label ID="LabelOnBehalf" runat="server" Text='<%# Eval("ONBEHALF_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dept">
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("DEPT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req Time">
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Label ID="LabelNameAction" runat="server" Text='<%# Eval("NAME_ACTION") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action On Behalf">
                <ItemTemplate>
                    <asp:Label ID="LabelActOnBehalf" runat="server" Text='<%# Eval("ACTION_ONBEHALF") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action Time">
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("TIME_END") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle HorizontalAlign="Right"/>

<RowStyle HorizontalAlign="Center" Font-Names="Trebuchet MS"></RowStyle>
    </asp:GridView>
    </div>
    </div>
    </div>
                            </ContentTemplate>
        </asp:UpdatePanel>

    <div class="modal fade" id="dialogView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="viewClose">&times;</button>
          <h4 class="modal-title">View</h4>
        </div>
          <div class="modal-body">
        <asp:UpdatePanel ID="PanelView" runat="server" UpdateMode="Conditional">
            
            <ContentTemplate>
                <asp:Table ID="Table2" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5> CAD Req No</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqNo" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Project</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelProject" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Month</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForMonth" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Year</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForYear" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>CAD Req By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqBy" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>On Behalf By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelOnBehalfBy" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Department</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDepartment" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Time Request</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelTimeReq" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Status</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelStatus" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Description</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDescription" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            
                            <asp:GridView ID="GridViewView" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666" CssClass="table table-striped table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                            </asp:GridView>
                                    
                        </asp:TableCell>
                        
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCommentView" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2"><asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonViewCancel" OnClick="ButtonViewCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
          </div>
        </div>
        </div>

    <div class="modal fade" id="dialogCancel" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="cancelClose">&times;</button>
          <h4 class="modal-title">Cancel Request</h4>
        </div>
          <div class="modal-body">
        <asp:UpdatePanel ID="PanelCancel" runat="server" UpdateMode="Conditional">
            
            <ContentTemplate>
                <asp:Table ID="Table3" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5> CAD Req No</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqNoCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Project</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelProjectCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Month</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForMonthCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>For Year</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelForYearCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>CAD Req By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelCADReqByCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>On Behalf By</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelOnBehalfByCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Department</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDepartmentCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Status</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelStatusCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Time Request</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelTimeReqCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Description</h5>
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:Label ID="LabelDescriptionCancel" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:GridView ID="GridViewCancel" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666" CssClass="table table-striped table-bordered table-hover">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <h5>Comment : </h5><asp:Label ID="LabelCommentCancel" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <h5>Comment for Cancel Request : </h5>
                            <asp:TextBox ID="TextBoxCommentCancel" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <h4>Do You Want to Cancel Request?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Cancel Request" ID="ButtonCancelOK" OnClick="ButtonCancelOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonCancelCancel" OnClick="ButtonCancelCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
          </div>
        </div>
        </div>

    <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        <asp:Panel ID="PanelProject" runat="server">
            <asp:Table ID="Table1" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="3" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
              </div>
          </div>
        </div>
    </div>
</asp:Content>
