﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Entities;
using CADcost.Helper;
using OfficeOpenXml;
using System.IO;
using System.Net;
using System.Globalization;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;

namespace CADcost
{
    public partial class Report : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                ViewState[curUser] = logInUser;
                switch (logInUser.ISADMIN)
                {
                    case false:
                        Response.Redirect("~/Beranda.aspx");
                        break;
                }
                CADcostHelper.generateDDLProjectName(DropDownListProject);
                DropDownListMonthCADcost.Items.Add(new ListItem("January", "1"));
                DropDownListMonthCADcost.Items.Add(new ListItem("February", "2"));
                DropDownListMonthCADcost.Items.Add(new ListItem("March", "3"));
                DropDownListMonthCADcost.Items.Add(new ListItem("April", "4"));
                DropDownListMonthCADcost.Items.Add(new ListItem("May", "5"));
                DropDownListMonthCADcost.Items.Add(new ListItem("June", "6"));
                DropDownListMonthCADcost.Items.Add(new ListItem("July", "7"));
                DropDownListMonthCADcost.Items.Add(new ListItem("August", "8"));
                DropDownListMonthCADcost.Items.Add(new ListItem("September", "9"));
                DropDownListMonthCADcost.Items.Add(new ListItem("October", "10"));
                DropDownListMonthCADcost.Items.Add(new ListItem("November", "11"));
                DropDownListMonthCADcost.Items.Add(new ListItem("December", "12"));

                DropDownListStartMonth.Items.Add(new ListItem("January", "1"));
                DropDownListStartMonth.Items.Add(new ListItem("February", "2"));
                DropDownListStartMonth.Items.Add(new ListItem("March", "3"));
                DropDownListStartMonth.Items.Add(new ListItem("April", "4"));
                DropDownListStartMonth.Items.Add(new ListItem("May", "5"));
                DropDownListStartMonth.Items.Add(new ListItem("June", "6"));
                DropDownListStartMonth.Items.Add(new ListItem("July", "7"));
                DropDownListStartMonth.Items.Add(new ListItem("August", "8"));
                DropDownListStartMonth.Items.Add(new ListItem("September", "9"));
                DropDownListStartMonth.Items.Add(new ListItem("October", "10"));
                DropDownListStartMonth.Items.Add(new ListItem("November", "11"));
                DropDownListStartMonth.Items.Add(new ListItem("December", "12"));

                DropDownListEndMonth.Items.Add(new ListItem("January", "1"));
                DropDownListEndMonth.Items.Add(new ListItem("February", "2"));
                DropDownListEndMonth.Items.Add(new ListItem("March", "3"));
                DropDownListEndMonth.Items.Add(new ListItem("April", "4"));
                DropDownListEndMonth.Items.Add(new ListItem("May", "5"));
                DropDownListEndMonth.Items.Add(new ListItem("June", "6"));
                DropDownListEndMonth.Items.Add(new ListItem("July", "7"));
                DropDownListEndMonth.Items.Add(new ListItem("August", "8"));
                DropDownListEndMonth.Items.Add(new ListItem("September", "9"));
                DropDownListEndMonth.Items.Add(new ListItem("October", "10"));
                DropDownListEndMonth.Items.Add(new ListItem("November", "11"));
                DropDownListEndMonth.Items.Add(new ListItem("December", "12"));

                DropDownListHighlightMonth.Items.Add(new ListItem("January", "1"));
                DropDownListHighlightMonth.Items.Add(new ListItem("February", "2"));
                DropDownListHighlightMonth.Items.Add(new ListItem("March", "3"));
                DropDownListHighlightMonth.Items.Add(new ListItem("April", "4"));
                DropDownListHighlightMonth.Items.Add(new ListItem("May", "5"));
                DropDownListHighlightMonth.Items.Add(new ListItem("June", "6"));
                DropDownListHighlightMonth.Items.Add(new ListItem("July", "7"));
                DropDownListHighlightMonth.Items.Add(new ListItem("August", "8"));
                DropDownListHighlightMonth.Items.Add(new ListItem("September", "9"));
                DropDownListHighlightMonth.Items.Add(new ListItem("October", "10"));
                DropDownListHighlightMonth.Items.Add(new ListItem("November", "11"));
                DropDownListHighlightMonth.Items.Add(new ListItem("December", "12"));

                DropDownListMonthIntegraph.Items.Add(new ListItem("January", "1"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("February", "2"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("March", "3"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("April", "4"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("May", "5"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("June", "6"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("July", "7"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("August", "8"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("September", "9"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("October", "10"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("November", "11"));
                DropDownListMonthIntegraph.Items.Add(new ListItem("December", "12"));

                DropDownListYearCADcost.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListYearCADcost.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListYearCADcost.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                DropDownListStartYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListStartYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListStartYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                DropDownListEndYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListEndYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListEndYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                DropDownListHighlightYear.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListHighlightYear.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListHighlightYear.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                DropDownListYearIntegraph.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
                DropDownListYearIntegraph.Items.Add(new ListItem((DateTime.Now.Year).ToString(), (DateTime.Now.Year).ToString()));
                DropDownListYearIntegraph.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
                

                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
        }

        protected void ButtonCreateCADcostEvidence_Click(object sender, EventArgs e)
        {
            string template = "";
            string newFile = "";
            string fileName = "";
            try
            {
                template = Server.MapPath("~/ReportFile/CADCost_Evidence.xlsx");
                fileName = "CADCost_Evidence_" + DropDownListYearCADcost.SelectedValue + "yr_" + DropDownListMonthCADcost.SelectedValue + "_" + DropDownListYearCADcost.SelectedValue;
                newFile = Server.MapPath("~/ReportFile/" + fileName + ".xlsx");

                if (!File.Exists(newFile))
                {
                    File.Copy(template, newFile, true);
                    File.SetLastWriteTime(newFile, DateTime.Now);
                }

                string[] projectName = DropDownListProject.SelectedItem.Text.Split(' ');



                ReportHelper.createCADcostEvidence(newFile, DropDownListProject.SelectedValue, projectName[2], Convert.ToInt32(DropDownListMonthCADcost.SelectedValue), Convert.ToInt32(DropDownListYearCADcost.SelectedValue));

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "on download error", "showException('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                FileStream fs = new FileStream(newFile, FileMode.Open, FileAccess.Read);
                int Length = (int)fs.Length;
                byte[] Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                File.Delete(newFile);
            }
        }

        protected void ButtonCreateJINDusingSoftware_Click(object sender, EventArgs e)
        {
            int month = (Convert.ToInt32(DropDownListEndMonth.SelectedValue) - Convert.ToInt32(DropDownListStartMonth.SelectedValue)) + 12 * (Convert.ToInt32(DropDownListEndYear.SelectedValue) - Convert.ToInt32(DropDownListStartYear.SelectedValue));
            if (month > 11 || month < 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Month range must not over than 12 months and not not negative');", true);
            }
            else
            {
                string template = "";
                string newFile = "";
                string fileName = "";
                try
                {
                    template = Server.MapPath("~/ReportFile/JIND_using_software.xlsx");
                    fileName = "JIND_using_software_" + DropDownListStartMonth.SelectedValue + "_" + DropDownListStartYear.SelectedValue + "_" + DropDownListEndMonth.SelectedValue + "_" + DropDownListEndYear.SelectedValue;
                    newFile = Server.MapPath("~/ReportFile/" + fileName + ".xlsx");

                    if (!File.Exists(newFile))
                    {
                        File.Copy(template, newFile, true);
                        File.SetLastWriteTime(newFile, DateTime.Now);
                    }

                    ReportHelper.createJINDusingSoftware(newFile, Convert.ToInt32(DropDownListHighlightMonth.SelectedValue), Convert.ToInt32(DropDownListHighlightYear.SelectedValue), Convert.ToInt32(DropDownListStartMonth.SelectedValue), Convert.ToInt32(DropDownListStartYear.SelectedValue), Convert.ToInt32(DropDownListEndMonth.SelectedValue), Convert.ToInt32(DropDownListEndYear.SelectedValue));
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "on download error", "showException('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
                }
                finally
                {
                    FileStream fs = new FileStream(newFile, FileMode.Open, FileAccess.Read);
                    int Length = (int)fs.Length;
                    byte[] Buffer = new byte[Length];
                    fs.Read(Buffer, 0, Length);
                    fs.Close();

                    Response.BinaryWrite(Buffer);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                    Response.Flush();
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    File.Delete(newFile);
                }
            }
        }

        protected void ButtonIntegraphLicenseRequest_Click(object sender, EventArgs e)
        {
            string template = "";
            string newFile = "";
            string fileName = "";
            try
            {
                template = "C:\\Users\\Ria.Yunita\\Desktop\\Report\\CADCost\\IntegraphLicenseDistribution.xlsx";
                fileName = "IntegraphLicenseDistribution_" + DropDownListYearIntegraph.SelectedValue + DropDownListMonthIntegraph.SelectedValue;
                newFile = "C:\\Users\\Ria.Yunita\\Desktop\\Report\\CADCost\\" + fileName + ".xlsx";

                if (!File.Exists(newFile))
                {
                    File.Copy(template, newFile, true);
                    File.SetLastWriteTime(newFile, DateTime.Now);
                }

                ReportHelper.createIntegraphLicenseDistribution(newFile, Convert.ToInt32(DropDownListMonthIntegraph.SelectedValue), Convert.ToInt32(DropDownListYearIntegraph.SelectedValue));
                
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "on download error", "showException('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                FileStream fs = new FileStream(newFile, FileMode.Open, FileAccess.Read);
                int Length = (int)fs.Length;
                byte[] Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //File.Delete(newFile);
                //Response.End();

                newFile = newFile.Replace(".xlsx", ".pdf");
                fs = new FileStream(newFile, FileMode.Open, FileAccess.Read);
                Length = (int)fs.Length;
                Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();
                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".pdf");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                File.Delete(newFile);

                newFile = newFile.Replace(".pdf", ".xlsx");
                File.Delete(newFile);
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void DropDownListStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
        }

        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            UpdatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
        }

        protected void ButtonIntegraphLicenseRequestExcel_Click(object sender, EventArgs e)
        {
            string template = "";
            string newFile = "";
            string fileName = "";
            try
            {
                template = "C:\\Users\\Ria.Yunita\\Desktop\\Report\\CADCost\\IntegraphLicenseDistribution.xlsx";
                fileName = "IntegraphLicenseDistribution_" + DropDownListYearIntegraph.SelectedValue + DropDownListMonthIntegraph.SelectedValue;
                newFile = "C:\\Users\\Ria.Yunita\\Desktop\\Report\\CADCost\\" + fileName + ".xlsx";
                template = Server.MapPath("~/ReportFile/IntegraphLicenseDistribution.xlsx");
                newFile = Server.MapPath("~/ReportFile/"+ fileName + ".xlsx");
                if (!File.Exists(newFile))
                {
                    File.Copy(template, newFile, true);
                    File.SetLastWriteTime(newFile, DateTime.Now);
                }

                ReportHelper.createIntegraphLicenseDistribution(newFile, Convert.ToInt32(DropDownListMonthIntegraph.SelectedValue), Convert.ToInt32(DropDownListYearIntegraph.SelectedValue));


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "on download error", "showException('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                FileStream fs = new FileStream(newFile, FileMode.Open, FileAccess.Read);
                int Length = (int)fs.Length;
                byte[] Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xlsx");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                File.Delete(newFile);
                Response.End();

            }
        }

        protected void ButtonIntegraphLicenseRequestPDF_Click(object sender, EventArgs e)
        {
            string template = Server.MapPath("~/ReportFile/Software_Chart.rpt");
            ReportHelper.createSoftwareChart(template, Convert.ToInt32(DropDownListMonthIntegraph.SelectedValue), Convert.ToInt32(DropDownListYearIntegraph.SelectedValue));
            string filePath = template.Replace(".rpt", "_" + DropDownListMonthIntegraph.SelectedValue + "_" + DropDownListYearIntegraph.SelectedValue + ".pdf");
            string fileName = "Software_Chart" + "_" + DropDownListMonthIntegraph.SelectedValue + "_" + DropDownListYearIntegraph.SelectedValue + ".pdf";
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                int Length = (int)fs.Length;
                byte[] Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnDownload errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                File.Delete(filePath);
            }

        }

        
    }
}