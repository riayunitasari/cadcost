﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_list.aspx.cs" Inherits="CADcost.Software_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <button type="button" id="ButtonSoftware" class="btn btn-warning" data-toggle="modal" data-target="#dialogSoftware" style="display:none;">View</button>
        <button type="button" id="ButtonVendor" class="btn btn-warning" data-toggle="modal" data-target="#dialogVendor" style="display:none;">View</button>
    <asp:UpdatePanel runat="server" ID="updatepanel1" UpdateMode="Conditional">
        <ContentTemplate>
    <div class="well ">
    <legend>Software List</legend>
    <asp:Button ID="ButtonAddSoftware" runat="server" Text="Add Software" OnClick="ButtonAddSoftware_Click" CssClass="btn btn-primary"/>
    <asp:GridView ID="GridViewSoftwareList" runat="server"  AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewSoftwareList_RowEditing" OnRowDeleting="GridViewSoftwareList_RowDeleting" OnRowUpdating="GridViewSoftwareList_RowUpdating" OnRowCancelingEdit="GridViewSoftwareList_RowCancelingEdit" OnRowDataBound="GridViewSoftwareList_RowDataBound" Width="80%" HorizontalAlign="Center" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Software Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftware" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftwareEdit" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:TextBox ID="TextBoxSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Renewal">
                <ItemTemplate>
                    <asp:Label ID="LabelDateRenewal" runat="server" Text='<%# Eval("DATE_RENEWAL") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxDateRenewal" runat="server" Text='<%# Eval("DATE_RENEWAL") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vendor Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendor" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:Label ID="LabelVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendorEdit" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:DropDownList ID="DropDownListVendorName" runat="server" ></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateSoftware" runat="server" CommandName="Update" Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelSoftware" runat="server" CommandName="Cancel" Text="Cancel"/>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditSoftware" runat="server" CommandName="Edit" Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteSoftware" runat="server" CommandName="Delete" Text="Delete"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
<RowStyle HorizontalAlign="Center"></RowStyle>

    </asp:GridView>

    <legend>Vendor List</legend>
    <asp:Button ID="ButtonAddVendor" runat="server" Text="Add Vendor" OnClick="ButtonAddVendor_Click" CssClass="btn btn-primary"/>
    <asp:GridView ID="GridViewVendor" runat="server"  AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewVendor_RowEditing" OnRowDeleting="GridViewVendor_RowDeleting" OnRowUpdating="GridViewVendor_RowUpdating" OnRowCancelingEdit="GridViewVendor_RowCancelingEdit" Width="100%" HorizontalAlign="Center" CssClass="table table-striped table-bordered table-hover">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Vendor">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendor" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:Label ID="LabelVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:Label>

                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendorEdit" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:TextBox ID="TextBoxVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact">
                <ItemTemplate>
                    <asp:Label ID="LabelContact" runat="server" Text='<%# Eval("CONTACT") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxContact" runat="server" Text='<%# Eval("CONTACT") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <asp:Label ID="LabelEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Website">
                <ItemTemplate>
                    <asp:Label ID="LabelWebsite" runat="server" Text='<%# Eval("WEBSITE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxWebsite" runat="server" Text='<%# Eval("WEBSITE") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vendor Description">
                <ItemTemplate>
                    <asp:Label ID="LabelVendorDescription" runat="server" Text='<%# Eval("VENDOR_DESCRIPTION") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxVendorDescription" runat="server" Text='<%# Eval("VENDOR_DESCRIPTION") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonUpdateVendor" runat="server" CommandName="Update" Text="Update"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonCancelVendor" runat="server" CommandName="Cancel" Text="Cancel"/>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Button CssClass="btn-primary" ID="LinkButtonEditVendor" runat="server" CommandName="Edit" Text="Edit"/>
                    <asp:Button CssClass="btn-warning" ID="LinkButtonDeleteVendor" runat="server" CommandName="Delete" Text="Delete"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle HorizontalAlign="Center"></RowStyle>
    </asp:GridView>
    
    </div>
            </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="dialogSoftware" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="softwareClose">&times;</button>
          <h4 class="modal-title">Add Software</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanelSoftware" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                <asp:Table ID="Table2" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell>
                            Software Name
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxSoftwareName" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Software Vendor
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:DropDownList ID="DropDownListVendor" runat="server" CssClass="input-sm"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Date Renewal
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxDateRenewal" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="ButtonAddSoftwareOK" OnClick="ButtonAddSoftwareOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddSoftwareCancel" OnClick="ButtonAddSoftwareCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
                      </ContentTemplate>
              </asp:UpdatePanel>
    </div>
          </div>
        </div>
        </div>
    
    <div class="modal fade" id="dialogVendor" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="vendorClose">&times;</button>
          <h4 class="modal-title">Add Vendor</h4>
        </div>
          <div class="modal-body">
              <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional"><ContentTemplate>
                <asp:Table ID="Table1" runat="server" BackColor="White" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell>
                            Vendor Name
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxVendorName" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Contact
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxContact" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Email
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Website
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxWebsite" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Vendor Description
                        </asp:TableCell>
                        <asp:TableCell>
                            : <asp:TextBox ID="TextBoxDescription" runat="server" CssClass="input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="ButtonAddVendorOK" OnClick="ButtonAddVendorOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddVendorCancel" OnClick="ButtonAddVendorCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
                  </ContentTemplate></asp:UpdatePanel>
    </div>
          </div>
        </div>
        </div>
</asp:Content>
