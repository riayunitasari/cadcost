﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;
using CADcost.Entities;
using System.Data;

namespace CADcost
{
    public partial class Delegate : System.Web.UI.Page
    {
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                
                if(!CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
                {
                    Response.Redirect("~/Beranda.aspx");
                }

                List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                approve1.AddRange(CADcostHelper.getApproveENIT("0"));
                approve1 = approve1.Distinct(new ApproveComparer()).ToList();

                //if (approve1.Count > 0 && !CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
                if (approve1.Count > 0 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
                {
                    RowEnit1.Visible = false;
                    RowEnit2.Visible = false;
                    RowEnit3.Visible = false;
                    TableConfirmationEnit.Visible = false;

                    List<Delegation> deleg = CADcostHelper.getAllDelegationEmpID(logInUser.EMP_ID);
                    GridViewRequester.DataSource = deleg;
                    GridViewRequester.DataBind();
                }
                else
                {
                    RowRater1.Visible = false;
                    RowRater2.Visible = false;
                    TableConfirmationRater.Visible = false;

                    List<Delegation> deleg = CADcostHelper.getAllDelegation();
                    GridViewRequester.DataSource = deleg;
                    GridViewRequester.DataBind();
                }

                PFNATTHelper.generateDDLEmployee(DropDownListEmployeeDelegated, "ALL");
                PFNATTHelper.generateDDLEmployee(DropDownListEmployeeDelegating, "ALL");
                PFNATTHelper.generateDDLEmployee(DropDownListEmployeeDelegatedRater, "ALL");
                
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployeeDelegated", "dropdown('ContentPlaceHolder1_DropDownListEmployeeDelegated');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployeeDelegating", "dropdown('ContentPlaceHolder1_DropDownListEmployeeDelegating');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployeeDelegatedRater", "dropdown('ContentPlaceHolder1_DropDownListEmployeeDelegatedRater');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployeeRelease", "dropdown('ContentPlaceHolder1_DropDownListEmployeeRelease');", true);
        }

        protected void ButtonDelegateOK_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseDelegate", "$('#delegateClose').click()", true);
            if (CADcostHelper.setDelegate(DropDownListEmployeeDelegated.SelectedValue, DropDownListEmployeeDelegating.SelectedValue))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alertDelegate", "alert('Set Delegate Success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alertDelegate", "alert('Set Delegate Failed\nUser Have Been Delegated Before');", true);
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void ButtonDelegateCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseDelegate", "$('#delegateClose').click()", true);
        }

        protected void GridViewRequester_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewRequester.EditIndex = index;

            List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
            approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
            approve1.AddRange(CADcostHelper.getApproveENIT("0"));
            approve1 = approve1.Distinct(new ApproveComparer()).ToList();

            //if (approve1.Count > 0 && !CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
            if (approve1.Count > 0 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegationEmpID(logInUser.EMP_ID);
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();
                
            }
            else
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegation();
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();
            }
        }

        protected void GridViewRequester_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewRequester.Rows[e.RowIndex];
            Button lbDelegate = row.FindControl("LinkButtonDeleteDelegate") as Button;
            CADcostHelper.releaseDelegate(lbDelegate.CommandArgument);

            List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
            approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
            approve1.AddRange(CADcostHelper.getApproveENIT("0"));
            approve1 = approve1.Distinct(new ApproveComparer()).ToList();

            //if (approve1.Count > 0 && !CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
            if (approve1.Count > 0 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegationEmpID(logInUser.EMP_ID);
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();

            }
            else
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegation();
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();
            }
        }

        protected void GridViewRequester_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewRequester.Rows[e.RowIndex];
            Button lbDelegate = row.FindControl("LinkButtonUpdateDelegate") as Button;
            Label lbEmpDelegating = row.FindControl("LabelEmployeeDelegating") as Label;
            DropDownList ddlEmpDelegated = row.FindControl("DropDownListEmployeeDelegatedEdit") as DropDownList;
            CADcostHelper.editDelegate(ddlEmpDelegated.SelectedValue, lbDelegate.CommandArgument);
            GridViewRequester.EditIndex = -1;


            List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
            approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
            approve1.AddRange(CADcostHelper.getApproveENIT("0"));
            approve1 = approve1.Distinct(new ApproveComparer()).ToList();

            //if (approve1.Count > 0 && !CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
            if (approve1.Count > 0 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegationEmpID(logInUser.EMP_ID);
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();

            }
            else
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegation();
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();
            }
        }

        protected void GridViewRequester_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewRequester.EditIndex = -1;

            List<Approve> approve1 = CADcostHelper.getApproveFirstRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
            approve1.AddRange(CADcostHelper.getApproveSecondRater("0", PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
            approve1.AddRange(CADcostHelper.getApproveENIT("0"));
            approve1 = approve1.Distinct(new ApproveComparer()).ToList();

            //if (approve1.Count > 0 && !CADcostHelper.isAdmin(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)))
            if (approve1.Count > 0 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegationEmpID(logInUser.EMP_ID);
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();

            }
            else
            {
                List<Delegation> deleg = CADcostHelper.getAllDelegation();
                GridViewRequester.DataSource = deleg;
                GridViewRequester.DataBind();
            }
        }

        protected void GridViewRequester_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                Delegation itemedit = e.Row.DataItem as Delegation;
                DropDownList ddlEmployeeDelegated = e.Row.FindControl("DropDownListEmployeeDelegatedEdit") as DropDownList;
                PFNATTHelper.generateDDLEmployee(ddlEmployeeDelegated, "ALL");
                ddlEmployeeDelegated.SelectedValue = itemedit.Emp_delegatedID;
                ScriptManager.RegisterStartupScript(this, GetType(), "alertFirstRater", "dropdown('ContentPlaceHolder1_GridViewRequester_DropDownListEmployeeDelegatedEdit_" + e.Row.RowIndex + "');", true);
            }
        }

        protected void ButtonSetDelegate_Click(object sender, EventArgs e)
        {
            LabelDelegated.Text = PFNATTHelper.getEMPFullName(DropDownListEmployeeDelegated.SelectedValue) + " (" + PFNATTHelper.getEMPDept(DropDownListEmployeeDelegated.SelectedValue) + ")";
            LabelDelegating.Text = PFNATTHelper.getEMPFullName(DropDownListEmployeeDelegating.SelectedValue) + " (" + PFNATTHelper.getEMPDept(DropDownListEmployeeDelegating.SelectedValue) + ")";
            TableConfirmationRater.Visible = false;
            PanelDelegate.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDelegate", "$('#ButtonPopUpDelegate').click()", true);
        }

        protected void ButtonSetDelegateRater_Click(object sender, EventArgs e)
        {
            LabelDelegatedRater.Text = PFNATTHelper.getEMPFullName(DropDownListEmployeeDelegatedRater.SelectedValue) + " (" + PFNATTHelper.getEMPDept(DropDownListEmployeeDelegatedRater.SelectedValue) + ")";
            TableConfirmationEnit.Visible = false;
            PanelDelegate.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDelegateRater", "$('#ButtonPopUpDelegate').click()", true);
        }

        protected void ButtonDelegateOKRater_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseDelegate", "$('#delegateClose').click()", true);
            //if (CADcostHelper.setDelegate(PFNATTHelper.getEMPidbyUsername(CADcostHelper.getUsername(Request.LogonUserIdentity.Name)), DropDownListEmployeeDelegatedRater.SelectedValue))
            if (CADcostHelper.setDelegate(logInUser.EMP_ID, DropDownListEmployeeDelegatedRater.SelectedValue))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alertDelegate", "alert('Set Delegate Success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alertDelegate", "alert('Set Delegate Failed\nUser Have Been Delegated Before');", true);
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void ButtonDelegateCancelRater_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseDelegate", "$('#delegateClose').click()", true);
        }
    }
}