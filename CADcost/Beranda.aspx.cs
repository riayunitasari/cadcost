﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Entities;
using CADcost.Helper;
using System.Data;
using System.Globalization;

namespace CADcost
{
    public partial class Beranda : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();
        int projectCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            ViewState[curUser] = logInUser;
            
            if (!IsPostBack)
            {
                

                DropDownListProject.Items.Clear();
                DropDownListProject.Items.Add(new ListItem("ALL PROJECT", "0"));
                if (CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1)
                {
                    projectCount = PFNATTHelper.generateDDLProjectNameFiltered(DropDownListProject, logInUser.EMP_ID);
                    if (projectCount == 0) 
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alertHideApproval", "hideapproval();", true);
                }
                else
                    CADcostHelper.generateDDLProjectName(DropDownListProject);

                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));

                    switch (CADcostHelper.getRole(logInUser.EMP_ID, "0").intRole)
                    {
                        case 1:
                            LabelRole.Text = "ENIT";
                            List<Approve> approve1 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                            approve1.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                            approve1.AddRange(CADcostHelper.getApproveENIT(DropDownListProject.SelectedValue.ToString()));
                            approve1 = approve1.Distinct(new ApproveComparer()).ToList();
                            GridViewToBeApproved.DataSource = approve1;
                            GridViewToBeApproved.DataBind();
                            break;
                        default:
                            LabelRole.Text = "";
                            List<Approve> approve = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                            approve.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                            approve = approve.Distinct(new ApproveComparer()).ToList();
                            GridViewToBeApproved.DataSource = approve;
                            GridViewToBeApproved.DataBind();
                            break;
                    }
                    DropDownListCurrencyView.Items.Add(new ListItem("USD", "0"));
                    CADcostHelper.generateDDLCurrency(DropDownListCurrencyView);
            }
            


            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
            
        }

        protected void DropDownListProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["jobcode"] = null;
            //logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            int role;
            role = CADcostHelper.getRole(logInUser.EMP_ID, DropDownListProject.SelectedValue.ToString()).intRole;
            if (DropDownListProject.SelectedValue.ToString().Equals("0"))
            {
                switch (role)
                {
                    
                    case 1:
                        LabelRole.Text = "ENIT";
                        List<Approve> approve1 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve1.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve1.AddRange(CADcostHelper.getApproveENIT(DropDownListProject.SelectedValue.ToString()));
                        approve1 = approve1.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve1;
                        GridViewToBeApproved.DataBind();
                        break;
                    default:
                        LabelRole.Text = "";
                        List<Approve> approve2 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve2.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve2 = approve2.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve2;
                        GridViewToBeApproved.DataBind();
                        break;
                }
            }
            else
            {
                switch (role)
                {
                    case 0:
                        GridViewToBeApproved.DataSource = null;
                        GridViewToBeApproved.DataBind();
                        break;
                    case 1:
                        LabelRole.Text = "ENIT";
                        List<Approve> approve1 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve1.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve1.AddRange(CADcostHelper.getApproveENIT(DropDownListProject.SelectedValue.ToString()));
                        approve1 = approve1.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve1;
                        GridViewToBeApproved.DataBind();
                        break;
                    case 2:
                        LabelRole.Text = "First Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                    case 3:
                        LabelRole.Text = "First Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                    case 4:
                        LabelRole.Text = "Second Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                }
            }
            updatePanelDropdown.Update();
        }

        protected void ButtonAddServer_Click(object sender, EventArgs e)
        {
            CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(ButtonApproveOK.CommandArgument));
            Session["jobcode"] = cadreq.JOBCODE;
            Response.Redirect("~/Software_server.aspx");
        }

        protected void ButtonAddCost_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Software_cost.aspx");
        }

        protected void ButtonAddCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseServer", "$('#serverClose').click()", true);
            
        }

        protected void ButtonApproveOK_Click(object sender, EventArgs e)
        {
            if (TextBoxNull.Text.Equals(""))
            {
                RowButton.Visible = false;
                RowConfirmationApprove.Visible = true;
            }
        }

        protected void ButtonApproveCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#approveClose').click()", true);
        }

        protected void ButtonApproveReject_Click(object sender, EventArgs e)
        {
            RowButton.Visible = false;
            RowConfirmationReject.Visible = true;
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            if (CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 2)
                GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectByEmpId(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text, CADcostHelper.getFilteredProject(logInUser.EMP_ID));
            else
                GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            DropDownListProject_SelectedIndexChanged(sender, e);
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            updatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
        }

        protected void GridViewToBeApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewToBeApproved.PageIndex = e.NewPageIndex;
            Session["jobcode"] = null;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            int role;
            role = CADcostHelper.getRole(logInUser.EMP_ID, DropDownListProject.SelectedValue.ToString()).intRole;
            if (DropDownListProject.SelectedValue.ToString().Equals("0"))
            {
                switch (role)
                {

                    case 1:
                        LabelRole.Text = "ENIT";
                        List<Approve> approve1 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve1.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve1.AddRange(CADcostHelper.getApproveENIT(DropDownListProject.SelectedValue.ToString()));
                        approve1 = approve1.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve1;
                        GridViewToBeApproved.DataBind();
                        break;
                    default:
                        LabelRole.Text = "";
                        List<Approve> approve2 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve2.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve2 = approve2.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve2;
                        GridViewToBeApproved.DataBind();
                        break;
                }
            }
            else
            {
                switch (role)
                {
                    case 0:
                        GridViewToBeApproved.DataSource = null;
                        GridViewToBeApproved.DataBind();
                        break;
                    case 1:
                        LabelRole.Text = "ENIT";
                        List<Approve> approve1 = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        approve1.AddRange(CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID));
                        approve1.AddRange(CADcostHelper.getApproveENIT(DropDownListProject.SelectedValue.ToString()));
                        approve1 = approve1.Distinct(new ApproveComparer()).ToList();
                        GridViewToBeApproved.DataSource = approve1;
                        GridViewToBeApproved.DataBind();
                        break;
                    case 2:
                        LabelRole.Text = "First Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                    case 3:
                        LabelRole.Text = "First Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveFirstRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                    case 4:
                        LabelRole.Text = "Second Rater";
                        GridViewToBeApproved.DataSource = CADcostHelper.getApproveSecondRater(DropDownListProject.SelectedValue.ToString(), PFNATTHelper.getEMPSect(logInUser.EMP_ID), logInUser.EMP_ID);
                        GridViewToBeApproved.DataBind();
                        break;
                }
            }
            updatePanelDropdown.Update();
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            if (CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 2)
                GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectByEmpId(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text, CADcostHelper.getFilteredProject(logInUser.EMP_ID));
            else
                GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
        }

        protected void ButtonApproveENIT_Click(object sender, EventArgs e)
        {
            
        }

        protected void DropDownListCurrencyView_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewApprove.DataSource = CADcostHelper.getItem(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue));
            GridViewApprove.DataBind();
            LabelTotalPrice.Text = String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)) == null ? 0 : CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)));
            GridViewSoftwareCost2.DataSource = (List<CADcostItem>)ViewState["cost"];
            GridViewSoftwareCost2.DataBind();
            PanelViewGrid.Update();
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.LoadComplete += new System.EventHandler(this.Beranda_LoadComplete);

        }

        private void Beranda_LoadComplete(object sender, EventArgs e)
        {
            if (logInUser.ISADMIN == false)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideSoftware", "hideSoftwareMenu();", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "hideReport", "hideReportMenu();", true);
            }
            if (CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 1 && CADcostHelper.getRole(logInUser.EMP_ID, "").intRole != 2)
            {
                projectCount = PFNATTHelper.generateDDLProjectNameFiltered(DropDownListProject, logInUser.EMP_ID);
                if (projectCount == 0) Page.ClientScript.RegisterStartupScript(this.GetType(), "alertHideApproval", "hideapproval()", true);
            }
            else
                CADcostHelper.generateDDLProjectName(DropDownListProject);
        }

        protected void GridViewSoftwareCost_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CADcostItem itemedit = e.Row.DataItem as CADcostItem;
                RadioButtonList rbl = e.Row.FindControl("RadioButtonListPrice") as RadioButtonList;
                List<SoftwareCost> sc = new List<SoftwareCost>();
                rbl.DataTextField = "Value";
                rbl.DataValueField = "Key";
                rbl.DataSource = CADcostHelper.getSoftwareCost(out sc, itemedit.ID_SOFTWARE, Convert.ToInt32(DropDownListCurrencyView.SelectedValue));
                rbl.DataBind();
            }
        }

        protected void GridViewLicenseServer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CADcostItem itemedit = e.Row.DataItem as CADcostItem;
                RadioButtonList rbl = e.Row.FindControl("RadioButtonListServer") as RadioButtonList;
                List<SoftwareServer> ss = new List<SoftwareServer>();
                rbl.DataTextField = "Value";
                rbl.DataValueField = "Key";
                rbl.DataSource = CADcostHelper.getSoftwareServer(out ss, Convert.ToInt32(ButtonApproveOK.CommandArgument), itemedit.ID_SOFTWARE);
                rbl.DataBind();
            }
        }

        protected void ButtonConfirm_Click(object sender, EventArgs e)
        {
            
        }

        protected void GridViewApprove_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CADcostItem item= e.Row.DataItem as CADcostItem;
                if (item.PRICE == null)
                {
                    Label lb = e.Row.FindControl("LabelPrice") as Label;
                    lb.Text = "N/A";
                }
            }
        }

        protected void GridViewCadreqActionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewCadreqActionHistory.PageIndex = e.NewPageIndex;
            GridViewCadreqActionHistory.DataSource = CADcostHelper.getCadreqActionHistory(Convert.ToInt32(ButtonApproveOK.CommandArgument));
            GridViewCadreqActionHistory.DataBind();
            PanelApprove.Update();
        }

        protected void ButtonConfirmApproveYes_Click(object sender, EventArgs e)
        {
            DateTime timeStart = DateTime.Now;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            Button btn = (Button)sender;
            CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(btn.CommandArgument.ToString()));
            ACTION_HISTORY act = new ACTION_HISTORY();
            act.ID_ACTION = 6; //approve request
            act.ID_CADREQ = Convert.ToInt32(btn.CommandArgument.ToString());
            act.JOBCODE = DropDownListProject.SelectedValue;
            act.TIME_START = timeStart;
            act.TIME_END = DateTime.Now;
            act.COMMENT = TextBoxCommentApprove.Text;
            if (logInUser.ISPARTICIPANT)
            {
                act.PARTICIPANT = logInUser.EMP_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
            }
            else
            {
                act.PARTICIPANT = logInUser.PARTICIPANT_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
                act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
            }

            switch (CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole)
            {
                case 1: //enit

                    List<CADcostItem> cost = (List<CADcostItem>)ViewState["cost"];
                    List<CADcostItem> server = (List<CADcostItem>)ViewState["server"];
                    foreach(GridViewRow row in GridViewSoftwareCost2.Rows)
                    {
                        RadioButtonList rbl = row.FindControl("RadioButtonListPrice") as RadioButtonList;
                        cost[row.RowIndex].ID_COST = Convert.ToInt32(rbl.SelectedItem.Value);
                    }
                    foreach (GridViewRow row in GridViewLicenseServer2.Rows)
                    {
                        RadioButtonList rbl = row.FindControl("RadioButtonListServer") as RadioButtonList;
                        server[row.RowIndex].ID_SOFTWARE_SERVER = Convert.ToInt32(rbl.SelectedItem.Value);
                    }
                    List<CADcostItem> items = new List<CADcostItem>();
                    if(cost!=null)
                    items.AddRange(cost);
                    if (server != null)
                    items.AddRange(server);
                    CADcostHelper.updateCADcostItem(Convert.ToInt32(ButtonApproveOK.CommandArgument), items);
                    ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseServer", "$('#serverClose').click()", true);
                    GridViewApprove.DataSource = CADcostHelper.getItem(Convert.ToInt32(ButtonApproveOK.CommandArgument), 0);
                    GridViewApprove.DataBind();
                    CADcostHelper.UpdateTotalItem(Convert.ToInt32(ButtonApproveOK.CommandArgument));
                    LabelTotalPrice.Text = String.Format("{0:N2}", CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)) == null ? "N/A" : CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)).ToString()); 
                    PanelApprove.Update();

                    act.ID_ACTIVITY = 7;
                    CADcostHelper.approveRequest(Convert.ToInt32(btn.CommandArgument.ToString()), logInUser.EMP_ID, CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole);
                    CADcostHelper.insertActionHistory(act);
                    break;
                case 2:
                    CADcostHelper.insertActionHistory(act);
                    act.ID_ACTIVITY = 6;
                    ACTION_HISTORY act2 = CADcostHelper.Clone(act);
                    CADcostHelper.insertActionHistory(act2);
                    CADcostHelper.approveRequest(Convert.ToInt32(btn.CommandArgument.ToString()), logInUser.EMP_ID, CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole);
                    WorkflowHelper.sendEmailToApproveENIT(Convert.ToInt32(btn.CommandArgument.ToString()));
                    break;
                case 3: //first rater
                    act.ID_ACTIVITY = 5;
                    CADcostHelper.approveRequest(Convert.ToInt32(btn.CommandArgument.ToString()), logInUser.EMP_ID, CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole);
                    CADcostHelper.insertActionHistory(act);
                    if (cadreq.PIC.Equals(CADcostHelper.getSecondRater(cadreq.JOBCODE, PFNATTHelper.getEMPSect(cadreq.PIC))))
                    {
                        act.PARTICIPANT = cadreq.PIC;
                        act.COMPLETED_BY = logInUser.EMP_ID;
                        act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
                        CADcostHelper.insertActionHistory(act);
                        WorkflowHelper.sendEmailToApproveENIT(Convert.ToInt32(btn.CommandArgument.ToString()));
                    }
                    else
                    {
                        WorkflowHelper.sendEmailToApproveSecondRater(Convert.ToInt32(btn.CommandArgument.ToString()));
                    }
                    break;
                case 4:
                    act.ID_ACTIVITY = 6;
                    CADcostHelper.approveRequest(Convert.ToInt32(btn.CommandArgument.ToString()), logInUser.EMP_ID, CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole);
                    CADcostHelper.insertActionHistory(act);
                    WorkflowHelper.sendEmailToApproveENIT(Convert.ToInt32(btn.CommandArgument.ToString()));
                    break;
            }
            
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#approveClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonConfirmApproveNo_Click(object sender, EventArgs e)
        {
            RowButton.Visible = true;
            RowConfirmationApprove.Visible = false;
        }
        
        protected void RadioButtonListPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<CADcostItem> cost = (List<CADcostItem>)ViewState["cost"];
            foreach (GridViewRow row in GridViewSoftwareCost2.Rows)
            {
                RadioButtonList rbl = row.FindControl("RadioButtonListPrice") as RadioButtonList;
                cost[row.RowIndex].ID_COST = Convert.ToInt32(rbl.SelectedItem.Value);
            }
            List<CADcostItem> items = new List<CADcostItem>();
            if (cost != null)
                items.AddRange(cost);
            CADcostHelper.updateCADcostItem(Convert.ToInt32(ButtonApproveOK.CommandArgument), items);
            GridViewApprove.DataSource = CADcostHelper.getItem(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue));
            GridViewApprove.DataBind();
            CADcostHelper.UpdateTotalItem(Convert.ToInt32(ButtonApproveOK.CommandArgument));
            LabelTotalPrice.Text = String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)) == null ? 0 : CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)));
            PanelApprove.Update();
        }

        protected void ButtonConfirmRejectYes_Click(object sender, EventArgs e)
        {
            DateTime timeStart = DateTime.Now;
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            Button btn = (Button)sender;
            CADcostHelper.rejectRequest(Convert.ToInt32(btn.CommandArgument.ToString()), logInUser.EMP_ID);
            ACTION_HISTORY act = new ACTION_HISTORY();
            act.ID_ACTION = 5; //reject
            act.ID_CADREQ = Convert.ToInt32(btn.CommandArgument.ToString());
            act.JOBCODE = DropDownListProject.SelectedValue;
            act.TIME_START = timeStart;
            act.TIME_END = DateTime.Now;
            act.COMMENT = TextBoxCommentApprove.Text;
            if (logInUser.ISPARTICIPANT)
            {
                act.PARTICIPANT = logInUser.EMP_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
            }
            else
            {
                act.PARTICIPANT = logInUser.PARTICIPANT_ID;
                act.COMPLETED_BY = logInUser.EMP_ID;
                act.PARTICIPANT_ONBEHALF = logInUser.EMP_ID;
            }
            CADcostHelper.insertActionHistory(act);
            WorkflowHelper.sendEmailRejected(Convert.ToInt32(btn.CommandArgument.ToString()));
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseApprove", "$('#approveClose').click()", true);
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonConfirmRejectNo_Click(object sender, EventArgs e)
        {
            RowButton.Visible = true;
            RowConfirmationReject.Visible = false;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value.Equals("isi"));
        }
        
        protected void GridViewToBeApproved_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("View"))
            {
                ViewState["cost"] = null;
                ViewState["server"] = null;
                PanelSoftwareCost2.Visible = false;
                PanelLicenseServer2.Visible = false;

                ButtonApproveOK.CommandArgument = e.CommandArgument.ToString();
                ButtonApproveReject.CommandArgument = e.CommandArgument.ToString();
                ButtonConfirmApproveYes.CommandArgument = e.CommandArgument.ToString();
                ButtonConfirmRejectYes.CommandArgument = e.CommandArgument.ToString();

                GridViewCadreqActionHistory.DataSource = CADcostHelper.getCadreqActionHistory(Convert.ToInt32(e.CommandArgument.ToString()));
                GridViewCadreqActionHistory.DataBind();
                LabelCommentApprove.Text = CADcostHelper.getLastActivityComment(Convert.ToInt32(e.CommandArgument.ToString()));
                CADREQ cadreq = CADcostHelper.getCADdetail(Convert.ToInt32(e.CommandArgument.ToString()));
                LabelCADReqNo.Text = cadreq.CADREQ_NO;
                LabelProject.Text = cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE);
                LabelCADReqBy.Text = cadreq.PIC + " - " + PFNATTHelper.getEMPFullName(cadreq.PIC);
                if (PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY).Equals(PFNATTHelper.getEMPFullName(cadreq.PIC)))
                    LabelOnBehalfBy.Text = "-";
                else
                    LabelOnBehalfBy.Text = cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY);
                LabelDepartment.Text = PFNATTHelper.getSectName(PFNATTHelper.getEMPSect(cadreq.PIC));
                LabelForMonth.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(cadreq.FOR_MONTH);
                LabelForYear.Text = cadreq.FOR_YEAR.ToString();
                LabelTimeReq.Text = cadreq.TIME_REQUEST.ToString();
                LabelDescription.Text = cadreq.DESCRIPTION;

                if (CADcostHelper.getRole(logInUser.EMP_ID, cadreq.JOBCODE).intRole == 1)
                {
                    List<CADcostItem> itemsAdded = new List<CADcostItem>();
                    List<CADcostItem> cost = new List<CADcostItem>();
                    List<CADcostItem> sserver = new List<CADcostItem>();
                    bool costnull, sservernull;
                    itemsAdded = CADcostHelper.UpdateSoftwareServer(out cost, out costnull, out sserver, out sservernull, Convert.ToInt32(e.CommandArgument.ToString()));
                    bool popup = false;
                    bool duplicate = false;
                    if (itemsAdded.Count > 0) // jika tidak ada sama sekali
                    {
                        GridViewAddedServer2.DataSource = itemsAdded;
                        GridViewAddedServer2.DataBind();
                        popup = true;
                        PanelNull2.Visible = true;
                        PanelNull2.Update();
                        TextBoxNull.Text = "isi";
                    }

                    if (cost.Count > 0) // jika costnya ada lebih dari 1
                    {
                        GridViewSoftwareCost2.DataSource = cost;
                        GridViewSoftwareCost2.DataBind();
                        popup = true;
                        PanelSoftwareCost2.Visible = true;
                        PanelSoftwareCost2.Update();
                        ViewState["cost"] = cost;
                        
                    }
                    else if(costnull)
                    {
                        duplicate = true;
                        ButtonAddCost2.Visible = true;
                        
                        LabelMustBeAdded.Text = "Software Cost Must Be Added";
                    }

                    if (sserver.Count > 0) // jika sofware servernya lebih dari 1
                    {
                        GridViewLicenseServer2.DataSource = sserver;
                        GridViewLicenseServer2.DataBind();
                        popup = true;
                        PanelLicenseServer2.Visible = true;
                        PanelLicenseServer2.Update();
                        ButtonAddServer.Visible = false;
                        ButtonConfirm.Visible = true;
                        ViewState["server"] = cost;
                    }
                    else if (sservernull)
                    {
                        ButtonAddServer2.Visible = true;
                        if (!duplicate)
                            LabelMustBeAdded.Text = "Software Server Must Be Added";
                        else
                        {
                            ButtonAddCost2.Visible = true;
                            ButtonAddServer2.Visible = true;
                            LabelMustBeAdded.Text = "Software Cost & Server Must Be Added";
                        }
                    }

                    if (popup)
                        PanelError2.Update();
                }
                else
                {
                    GridViewApprove.Columns[GridViewApprove.Columns.Count - 1].Visible = false;
                    RowTotalPrice.Visible = false;
                    RowCurrency.Visible = false;
                }
                CADcostHelper.UpdateTotalItem(Convert.ToInt32(e.CommandArgument.ToString()));
                LabelTotalPrice.Text = String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)) == null ? 0 : CADcostHelper.getTotalItemAmount(Convert.ToInt32(ButtonApproveOK.CommandArgument), Convert.ToInt32(DropDownListCurrencyView.SelectedValue)));
                PanelApprove.Update();
                GridViewApprove.DataSource = CADcostHelper.getItem(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(DropDownListCurrencyView.SelectedValue));
                GridViewApprove.DataBind();
                RowConfirmationApprove.Visible = false;
                RowConfirmationReject.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "alertApprove", "$('#ButtonApprove').click()", true);
            }
        }

    }
}