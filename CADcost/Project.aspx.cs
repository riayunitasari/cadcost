﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Entities;
using CADcost.Helper;
using System.Data;

namespace CADcost
{
    public partial class Project : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                ViewState[curUser] = logInUser;

                switch (logInUser.ISADMIN)
                {
                    case false:
                        Response.Redirect("~/Beranda.aspx");
                        break;
                }

                PFNATTHelper.generateDDLProjectName(DropDownListProject);
                DropDownListSearchBy.Items.Add(new ListItem("JOBCODE", "JOBCODE_ID"));
                DropDownListSearchBy.Items.Add(new ListItem("PROJECT NAME", "PROJECT_NAME"));
                GridViewProject.DataSource = CADcostHelper.getAllProject();
                GridViewProject.DataBind();
                
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdown", "dropdown('ContentPlaceHolder1_DropDownListProject');", true);
        }

        protected void ButtonAddProject_Click(object sender, EventArgs e)
        {
            PROJECT prjt = new PROJECT();
            prjt.JOBCODE_ID = DropDownListProject.SelectedValue;
            prjt.PROJECT_NAME = PFNATTHelper.getProjectName(DropDownListProject.SelectedValue);
            prjt.DATE_REMINDER = Convert.ToInt32(TextBoxDateReminder.Text);
            CADcostHelper.insertProject(prjt);
            GridViewProject.DataSource = CADcostHelper.getAllProject();
            GridViewProject.DataBind();
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void GridViewProjectSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewProjectSearch.SelectedRow;
            Label labelJobcode = row.FindControl("LabelJobcodeID") as Label;
            DropDownListProject.SelectedValue = labelJobcode.Text;
            GridViewProjectSearch.DataSource = null;
            GridViewProjectSearch.DataBind();
            updatePanelDropdown.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseSearch", "$('#projectClose').click()", true);
        }

        protected void GridViewProjectSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewProjectSearch.PageIndex = e.NewPageIndex;
            ScriptManager.RegisterStartupScript(this, GetType(), "alertOpenSearch", "opendialog('dialogProject');", true);
            GridViewProjectSearch.DataSource = PFNATTHelper.getFilteredProjectName(DropDownListSearchBy.SelectedValue.ToString(), TextBoxSearch.Text);
            GridViewProjectSearch.DataBind();
            UpdatePanelProject.Update();
        }

        protected void GridViewRater_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewRater.EditIndex = index;
            List<ProjectRater> rater = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            GridViewRater.DataSource = rater;
            GridViewRater.DataBind();
        }

        protected void GridViewRater_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            
            GridViewRow row = GridViewRater.Rows[e.RowIndex];
            HiddenField hfIDRater = row.FindControl("HiddenFieldIDRater") as HiddenField;
            HiddenField hfSectID = row.FindControl("HiddenFieldSectID") as HiddenField;
            DropDownList ddlFirstRater = row.FindControl("DropDownListFirstRaterEdit") as DropDownList;
            DropDownList ddlSecondRater = row.FindControl("DropDownListSecondRaterEdit") as DropDownList;
            RATER rate = new RATER();
            rate.JOBCODE_ID = ButtonBack.CommandArgument;
            rate.ID_RATER = Convert.ToInt32(hfIDRater.Value);
            rate.SECT_ID = hfSectID.Value;
            rate.FirstRater = ddlFirstRater.SelectedValue.ToString();
            rate.SecondRater = ddlSecondRater.SelectedValue.ToString();
            CADcostHelper.updateRater(rate);
            GridViewRater.EditIndex = -1;
            List<ProjectRater> rater = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            GridViewRater.DataSource = rater;
            GridViewRater.DataBind();
        }

        protected void GridViewRater_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewRater.EditIndex = -1;
            List<ProjectRater> rater = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            GridViewRater.DataSource = rater;
            GridViewRater.DataBind();
        }

        protected void GridViewRater_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewRater.Rows[e.RowIndex];
            Button lbIDRater = row.FindControl("LinkButtonRaterDelete") as Button;
            CADcostHelper.deleteRater(Convert.ToInt32(lbIDRater.CommandArgument));
            List<ProjectRater> rater = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            if (rater.Count == 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("No", typeof(int));
                dt.Columns.Add("Department", typeof(string));
                dt.Columns.Add("FirstRater", typeof(string));
                dt.Columns.Add("SecondRater", typeof(string));
                dt.Columns.Add("ID_RATER", typeof(string));
                dt.Columns.Add("SECT_ID", typeof(string));

                DataRow dtRow = dt.NewRow();
                dt.Rows.Add(dtRow);

                GridViewRater.DataSource = dt;
                GridViewRater.DataBind();

                GridViewRater.Rows[0].Visible = false;
            }
            else
            {
                GridViewRater.DataSource = rater;
                GridViewRater.DataBind();
            }
            
        }

        protected void GridViewProject_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("View"))
            {
                LabelEditRater.Text = "Edit Project - " + e.CommandArgument.ToString() +" - "+ PFNATTHelper.getProjectName(e.CommandArgument.ToString());
                ButtonBack.CommandArgument = e.CommandArgument.ToString();
                List<ProjectRater> rater= CADcostHelper.getAllProjectRater(e.CommandArgument.ToString());
                if (rater.Count == 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("No", typeof(int));
                    dt.Columns.Add("Department", typeof(string));
                    dt.Columns.Add("FirstRater", typeof(string));
                    dt.Columns.Add("SecondRater", typeof(string));
                    dt.Columns.Add("ID_RATER", typeof(string));
                    dt.Columns.Add("SECT_ID", typeof(string));

                    DataRow dtRow = dt.NewRow();
                    dt.Rows.Add(dtRow);

                    GridViewRater.DataSource = dt;
                    GridViewRater.DataBind();

                    GridViewRater.Rows[0].Visible = false;
                }
                else
                {
                    GridViewRater.DataSource = rater;
                    GridViewRater.DataBind();
                }
                //ViewState["curRater"] = rater;
                UpdatePanelRater.Update();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertApprove", "$('#ButtonEdit').click()", true);
            }
        }

        protected void GridViewRater_DataBound(object sender, EventArgs e)
        {
            GridViewRow row = GridViewRater.FooterRow;
            DropDownList rpDepartmrnt = row.FindControl("DropDownListDepartmentAdd") as DropDownList;
            PFNATTHelper.generateDDLDepartment(rpDepartmrnt);
            rpDepartmrnt.DataBind();
            DropDownList rpFirstRater = row.FindControl("DropDownListFirstRaterAdd") as DropDownList;
            PFNATTHelper.generateDDLEmployee(rpFirstRater, "ALL");
            rpFirstRater.DataBind();
            DropDownList rpSecondRater = row.FindControl("DropDownListSecondRaterAdd") as DropDownList;
            PFNATTHelper.generateDDLEmployee(rpSecondRater, "ALL");
            rpSecondRater.DataBind();
            UpdatePanelRater.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertFirstRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListFirstRaterAdd');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertSecondRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListSecondRaterAdd');", true);
        }

        protected void ButtonAddRater_Click(object sender, EventArgs e)
        {
            string jobcode_id = ButtonBack.CommandArgument;
            GridViewRow row = GridViewRater.FooterRow;
            DropDownList rpDepartmrnt = row.FindControl("DropDownListDepartmentAdd") as DropDownList;
            DropDownList rpFirstRater = row.FindControl("DropDownListFirstRaterAdd") as DropDownList;
            DropDownList rpSecondRater = row.FindControl("DropDownListSecondRaterAdd") as DropDownList;
            RATER rater = new RATER();
            rater.JOBCODE_ID=jobcode_id;
            rater.SECT_ID = rpDepartmrnt.SelectedValue.ToString();
            rater.FirstRater = rpFirstRater.SelectedValue.ToString();
            rater.SecondRater = rpSecondRater.SelectedValue.ToString();
            CADcostHelper.insertRater(rater);
            List<ProjectRater> rater2 = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            GridViewRater.DataSource = rater2;
            GridViewRater.DataBind();
        }

        protected void DropDownListDepartmentAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridViewRater.FooterRow;
            DropDownList rpDepartmrnt = row.FindControl("DropDownListDepartmentAdd") as DropDownList;
            DropDownList rpFirstRater = row.FindControl("DropDownListFirstRaterAdd") as DropDownList;
            DropDownList rpSecondRater = row.FindControl("DropDownListSecondRaterAdd") as DropDownList;
            PFNATTHelper.generateDDLEmployee(rpFirstRater, "ALL");
            rpFirstRater.DataBind();
            PFNATTHelper.generateDDLEmployee(rpSecondRater, "ALL");
            rpSecondRater.DataBind();
            ScriptManager.RegisterStartupScript(this, GetType(), "alertFirstRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListFirstRaterAdd');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "alertSecondRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListSecondRaterAdd');", true);
            UpdatePanelRater.Update();
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertCloseEdit", "$('#editClose').click()", true);
        }

        protected void GridViewRater_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                ProjectRater itemedit = e.Row.DataItem as ProjectRater;
                DropDownList ddlFirstRater = e.Row.FindControl("DropDownListFirstRaterEdit") as DropDownList;
                PFNATTHelper.generateDDLEmployee(ddlFirstRater, "ALL");
                if (itemedit.FirstRaterID!=null)
                ddlFirstRater.SelectedValue = itemedit.FirstRaterID.ToString();
                DropDownList ddlSecondRater = e.Row.FindControl("DropDownListSecondRaterEdit") as DropDownList;
                PFNATTHelper.generateDDLEmployee(ddlSecondRater, "ALL");
                if (itemedit.SecondRaterID!=null)
                ddlSecondRater.SelectedValue = itemedit.SecondRaterID.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "alertFirstRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListFirstRaterEdit_"+e.Row.RowIndex+"');", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "alertSecondRater", "dropdown('ContentPlaceHolder1_GridViewRater_DropDownListSecondRaterEdit_" + e.Row.RowIndex + "');", true);
            }
        }

        protected void GridViewProject_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int index = e.NewEditIndex;
            GridViewProject.EditIndex = index;
            GridViewProject.DataSource = CADcostHelper.getAllProject();
            GridViewProject.DataBind();
        }

        protected void GridViewProject_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridViewProject.Rows[e.RowIndex];
            Button lbProject = row.FindControl("LinkButtonUpdateProject") as Button;
            TextBox tbDateReminder = row.FindControl("TextBoxDateReminder") as TextBox;
            PROJECT project = new PROJECT();
            project.JOBCODE_ID = lbProject.CommandArgument;
            project.DATE_REMINDER = Convert.ToInt32(tbDateReminder.Text);
            CADcostHelper.updateProject(project);
            GridViewProject.EditIndex = -1;
            GridViewProject.DataSource = CADcostHelper.getAllProject();
            GridViewProject.DataBind();
        }

        protected void GridViewProject_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewProject.Rows[e.RowIndex];
            Button lbProject = row.FindControl("LinkButtonDeleteProject") as Button;
            CADcostHelper.deleteProject(lbProject.CommandArgument);
            List<PROJECT> prjt = CADcostHelper.getAllProject();
            if (prjt.Count == 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("No", typeof(int));
                dt.Columns.Add("JOBCODE_ID", typeof(string));
                dt.Columns.Add("PROJECT_NAME", typeof(string));
                dt.Columns.Add("DATE_REMINDER", typeof(string));

                DataRow dtRow = dt.NewRow();
                dt.Rows.Add(dtRow);

                GridViewProject.DataSource = dt;
                GridViewProject.DataBind();

                GridViewProject.Rows[0].Visible = false;
            }
            else
            {
                GridViewProject.DataSource = prjt;
                GridViewProject.DataBind();
            }
        }

        protected void GridViewProject_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridViewProject.EditIndex = -1;
            GridViewProject.DataSource = CADcostHelper.getAllProject();
            GridViewProject.DataBind();
        }

        protected void GridViewRater_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewRater.PageIndex = e.NewPageIndex;
            GridViewRater.DataSource = CADcostHelper.getAllProjectRater(ButtonBack.CommandArgument.ToString());
            GridViewRater.DataBind();
        }

        protected void ButtonReset_Click(object sender, EventArgs e)
        {
            CADcostHelper.deleteAllRater(ButtonBack.CommandArgument);
            DataTable dt = new DataTable();
            dt.Columns.Add("No", typeof(int));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("FirstRater", typeof(string));
            dt.Columns.Add("SecondRater", typeof(string));
            dt.Columns.Add("ID_RATER", typeof(string));
            dt.Columns.Add("SECT_ID", typeof(string));

            DataRow dtRow = dt.NewRow();
            dt.Rows.Add(dtRow);

            GridViewRater.DataSource = dt;
            GridViewRater.DataBind();

            GridViewRater.Rows[0].Visible = false;
        }
    }
}