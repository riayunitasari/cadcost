﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CADcost.Helper;
using CADcost.Entities;

namespace CADcost
{
    public partial class Requester : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridViewRequester.DataSource = CADcostHelper.getAllRequester();
                GridViewRequester.DataBind();
                PFNATTHelper.generateDDLDepartment(DropDownListDepartment);
                PFNATTHelper.generateDDLEmployee(DropDownListEmployee, DropDownListDepartment.SelectedValue);
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployee", "dropdown('ContentPlaceHolder1_DropDownListEmployee');", true);
        }

        protected void ButtonAddEmployee_Click(object sender, EventArgs e)
        {
            REQUESTER req = new REQUESTER();
            req.EMP_ID = DropDownListEmployee.SelectedValue;
            req.FULL_NAME = DropDownListEmployee.SelectedItem.Text.Split('-').GetValue(1).ToString();
            req.SECT_ID = DropDownListDepartment.SelectedValue;
            req.SECT_NAME = DropDownListDepartment.SelectedItem.Text;
            req.DATE_REGISTER = DateTime.Now;
            CADcostHelper.insertRequester(req);
            GridViewRequester.DataSource = CADcostHelper.getAllRequester();
            GridViewRequester.DataBind();
        }

        protected void GridViewRequester_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridViewRequester.Rows[e.RowIndex];
            Button lbDelete = row.FindControl("LinkButtonDelete") as Button;
            CADcostHelper.deleteRequester(lbDelete.CommandArgument);
            GridViewRequester.DataSource = CADcostHelper.getAllRequester();
            GridViewRequester.DataBind();
        }

        protected void DropDownListDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            PFNATTHelper.generateDDLEmployee(DropDownListEmployee, DropDownListDepartment.SelectedValue.ToString());
            ScriptManager.RegisterStartupScript(this, GetType(), "alertDropdownEmployee", "dropdown('ContentPlaceHolder1_DropDownListEmployee');", true);
            UpdatePanelEmployee.Update();
        }
    }
}