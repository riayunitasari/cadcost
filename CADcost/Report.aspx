﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="CADcost.Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
    <div class="well ">
    <legend><h2>Print CAD Cost Evidence</h2></legend>
    <asp:Table ID="Table1" runat="server" Width="100%">
        <asp:TableRow>
            <asp:TableCell>
                Project :
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:UpdatePanel ID="UpdatePanelDropdown" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                <asp:DropDownList ID="DropDownListProject" runat="server" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
                </asp:UpdatePanel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Month :
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:DropDownList ID="DropDownListMonthCADcost" runat="server" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Year :
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                 <asp:DropDownList ID="DropDownListYearCADcost" runat="server" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell >
                <asp:Button ID="ButtonCreateCADcostEvidence" runat="server" Text="Generate Excel" OnClick="ButtonCreateCADcostEvidence_Click" CssClass="btn-primary" />
            </asp:TableCell>
            <asp:TableCell>
                
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-6">
    <div class="well ">
    <legend><h2>Print JIND Using Software</h2></legend>
    <asp:Table ID="Table2" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                From :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListStartMonth" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListStartYear" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                To :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListEndMonth" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListEndYear" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Highlighted :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListHighlightMonth" runat="server" Width="100%" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListHighlightYear" runat="server" Width="100%" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell >
                <asp:Button ID="ButtonCreateJINDusingSoftware" runat="server" Text="Generate Excel" OnClick="ButtonCreateJINDusingSoftware_Click" CssClass="btn-primary"/>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        <div class="col-lg-6">
    <div class="well ">
    <legend><h2>Print Integraph License Distribution</h2></legend>
    <asp:Table ID="Table3" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListMonthIntegraph" runat="server" Width="100%" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListYearIntegraph" runat="server" Width="100%" CssClass="input-sm"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:Button ID="ButtonIntegraphLicenseRequestExcel" runat="server" Text="Generate Excel" OnClick="ButtonIntegraphLicenseRequestExcel_Click" CssClass="btn-primary"/>
                <asp:Button ID="ButtonIntegraphLicenseRequestPDF" runat="server" Text="Generate PDF" OnClick="ButtonIntegraphLicenseRequestPDF_Click" CssClass="btn-danger"/>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        </div>
     <div class="modal fade" id="dialogProject" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="projectClose">&times;</button>
          <h4 class="modal-title">Project</h4>
        </div>
          <div class="modal-body">
        
            <asp:Table ID="Table4" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" CssClass="table table-striped table-bordered table-hover" AllowPaging="true" PageSize="5" PagerStyle-HorizontalAlign="Right"  OnPageIndexChanging="GridViewProjectSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
              </div>
          </div>
        </div>
        
    </div>
</asp:Content>
