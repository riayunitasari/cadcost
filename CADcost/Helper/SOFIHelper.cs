﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CADcost.Entities;

namespace CADcost.Helper
{
    public class SOFIHelper
    {
        public static void generateDDLSoftwareName(DropDownList ddl)
        {
            ddl.Items.Clear();
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            List<tblPart> softwareName = getAllSoftwareName();
            foreach (tblPart sfwr in softwareName)
            {
                ddl.Items.Add(new ListItem(sfwr.prtName.ToString(), sfwr.PartID.ToString()));
            }
        }

        public static List<tblPart> getAllSoftwareName()
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            Func <int, string, tblPart> makePart = (t1,t2) => new tblPart{ PartID = t1, prtName = t2 };
            List<tblPart> softwareName = new List<tblPart>();
            softwareName =(from software in data.tblParts
                        join grouping in data.tblGroupings on software.GroupingID equals grouping.GroupingID
                        where grouping.gngName.Equals("software")
                        select makePart(software.PartID, software.prtName)).ToList();
            return softwareName;
        }

        
    }
}