﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CADcost.Entities;

namespace CADcost.Helper
{
    public class WorkflowHelper
    {
        public static void sendEmailToApproveENIT(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            List<string> enit = CADcostHelper.getAllENITEmail();
            string emailTo = "";
            foreach (string str in enit)
            {
                emailTo += str;
                emailTo += ';';
            }
            string emailSubject = "[APPROVAL CADcost] - Need To Be Approved By ENIT - "+cadreq.CADREQ_NO;
            string emailBody = "Please Approve License Request Below:</br><table><tr><td>CAD Req Number :</td><td>"+cadreq.CADREQ_NO+"</td></tr><tr><td>Requester :</td><td>"+cadreq.CADREQ_BY+" - "+PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY)+"</td></tr><tr><td>Project :</td><td>"+cadreq.JOBCODE+" - "+PFNATTHelper.getProjectName(cadreq.JOBCODE)+"</td></tr><tr><td>Status :</td><td>APPROVAL_BY_SECOND_RATER</td></tr></table></br> Please Approve By Access CAD Cost System </br>"+emailTo;
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailToApproveENITManager(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);

            string emailSubject = "[APPROVAL CADcost] - Need To Be Approved By ENIT Manager - " + cadreq.CADREQ_NO;
            string emailBody = "Please Approve License Request Below:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>APPROVAL_BY_ENIT</td></tr></table></br> Please Approve By Access CAD Cost System";
            //data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "Tomi.Hermanto@jgc-indonesia.com", "", "", emailSubject, emailBody);
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailToApproveFirstRater(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);

            string keyPerson = (from fwbs in data2.TBL_PROJECT_FWBs
                                where fwbs.JOBCODE_ID.Equals(cadreq.JOBCODE) && fwbs.CATEGORY_ID.Equals(PFNATTHelper.getEMPSect(cadreq.PIC) + "13F1")
                                select fwbs.KEYPERSON).FirstOrDefault();

            string clientJobcode = (from fwbs in data2.TBL_PROJECT_FWBs
                                    where fwbs.JOBCODE_ID.Equals(cadreq.JOBCODE)
                                    select fwbs.CLIENT_JOBCODE).FirstOrDefault();

            string firstRater = data2.Funct_WhoMustApproved(keyPerson, cadreq.JOBCODE, clientJobcode, PFNATTHelper.getEMPSect(cadreq.PIC), PFNATTHelper.getEMPSect(cadreq.PIC) + "13F1", 1);

            string emailSubject = "[APPROVAL CADcost] - Need To Be Approved By First Rater - " + cadreq.CADREQ_NO;
            string emailBody = "Please Approve License Request Below:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>FIX_REQUEST</td></tr></table></br> Please Approve By Access CAD Cost System </br>" + PFNATTHelper.getEMPEmail(firstRater);
            //data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", PFNATTHelper.getEMPEmail(firstRater), "", "", emailSubject, emailBody);
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailToApproveSecondRater(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);

            string keyPerson = (from fwbs in data2.TBL_PROJECT_FWBs
                                where fwbs.JOBCODE_ID.Equals(cadreq.JOBCODE) && fwbs.CATEGORY_ID.Equals(PFNATTHelper.getEMPSect(cadreq.CADREQ_BY) + "13F1")
                                select fwbs.KEYPERSON).FirstOrDefault();

            string clientJobcode = (from fwbs in data2.TBL_PROJECT_FWBs
                                    where fwbs.JOBCODE_ID.Equals(cadreq.JOBCODE)
                                    select fwbs.CLIENT_JOBCODE).FirstOrDefault();

            string secondRater = data2.Funct_WhoMustApproved(keyPerson, cadreq.JOBCODE, clientJobcode, PFNATTHelper.getEMPSect(cadreq.PIC), PFNATTHelper.getEMPSect(cadreq.PIC) + "13F1", 2);

            string emailSubject = "[APPROVAL CADcost] - Need To Be Approved By Second Rater - " + cadreq.CADREQ_NO;
            string emailBody = "Please Approve License Request Below:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>APPROVAL_BY_FIRST_RATER</td></tr></table></br> Please Approve By Access CAD Cost System <br/>"+PFNATTHelper.getEMPEmail(secondRater);
            //data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", PFNATTHelper.getEMPEmail(secondRater), "", "", emailSubject, emailBody);
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailDoneApproval(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            List<string> enit = CADcostHelper.getAllENITEmail();
            string emailTo = "";
            foreach (string str in enit)
            {
                emailTo += str;
                emailTo += ';';
            }
            string emailSubject = "[APPROVAL CADcost] - Approval Done - " + cadreq.CADREQ_NO;
            string emailBody = "Approval of License Request Below is Done:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>APPROVAL_BY_ENIT_MANAGER</td></tr></table></br> Please Check By Access CAD Cost System</br>"+emailTo;
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailRejected(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            List<string> enit = CADcostHelper.getAllENITEmail();
            string emailTo = "";
            foreach (string str in enit)
            {
                emailTo += str;
                emailTo += ';';
            }
            string emailSubject = "[APPROVAL CADcost] - Rejected Request - " + cadreq.CADREQ_NO;
            string emailBody = "Your Request Has Been Rejected:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>REJECTED_REQUEST</td></tr></table></br> Please Check By Access CAD Cost System";
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }

        public static void sendEmailCanceled(int id_cadreq)
        {
            WorkflowDataContext data = new WorkflowDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            List<string> enit = CADcostHelper.getAllENITEmail();
            string emailTo = "";
            foreach (string str in enit)
            {
                emailTo += str;
                emailTo += ';';
            }
            string emailSubject = "[APPROVAL CADcost] - Canceled Request - " + cadreq.CADREQ_NO;
            string emailBody = "Your Request Has Been Canceled:</br><table><tr><td>CAD Req Number :</td><td>" + cadreq.CADREQ_NO + "</td></tr><tr><td>Requester :</td><td>" + cadreq.CADREQ_BY + " - " + PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY) + "</td></tr><tr><td>Project :</td><td>" + cadreq.JOBCODE + " - " + PFNATTHelper.getProjectName(cadreq.JOBCODE) + "</td></tr><tr><td>Status :</td><td>CANCELED_REQUEST</td></tr></table></br> Please Check By Access CAD Cost System";
            data.InsertEmailSender("ITD-ENGINEERING@jgc-indonesia.com", "ria.yunita@jgc-indonesia.com", "", "", emailSubject, emailBody);
        }
    }
}