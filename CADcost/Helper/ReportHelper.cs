﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CADcost.Entities;
using OfficeOpenXml;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using Spire.Xls;
using Spire.Pdf;
using Spire.Xls.Converter;
using System.Drawing;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CADcost.Helper
{
    public class CellData
    {
        public string software;
        public string sheet;
        public int row;
        public int column;
    }

    public class ReportHelper
    {
        public static void createCADcostEvidence(string fileName, string jobcode_id, string project_name, int month, int year)
        {
            FileInfo file = new FileInfo(fileName);
            ExcelPackage Excelpackage = new ExcelPackage(file);
            ExcelWorksheet Excelworksheet = Excelpackage.Workbook.Worksheets["DEPT"];
            List<Approve> approvedCADreq = CADcostHelper.getDoneApproval(jobcode_id, month, year);
            List<SoftwareCost> software = CADcostHelper.getAllSoftwareCost(0);
            int i = 1;
            string sect = "";
            ExcelWorksheet Excelworksheet2 = Excelpackage.Workbook.Worksheets["DEPT"];
            int j=0;
            foreach (Approve app in approvedCADreq)
            {
                
                if (!app.SECT_NAME.Equals(sect))
                {
                    Excelworksheet2 = Excelworksheet.Workbook.Worksheets.Copy("DEPT", project_name + "-" + app.SECT_NAME);
                     
                    i = 1;

                    Excelworksheet2.Cells[2, 1].Value += " " + year.ToString();

                    j = 0;
                    foreach (SoftwareCost sc in software)
                    {
                        Excelworksheet2.Cells[6 + i - 1, j + 6].Value = sc.NAME_SOFTWARE + " " + sc.TYPE;
                        if (j != software.Count - 1)
                        {
                            Excelworksheet2.InsertColumn(j + 7, 1, 6);
                            Excelworksheet2.Column(j + 7).Width = Excelworksheet2.Column(6).Width;
                        }
                        j++;
                    }

                    Excelworksheet2.Cells[5, 6, 5, j + 5].Merge = true;

                    Excelworksheet2.Cells[7 + i - 1, j + 6].Value = app.TOTAL_AMOUNT;

                    

                }
                else
                {
                    i++;
                    Excelworksheet2.Cells[7 + i - 1, j + 6].Value = app.TOTAL_AMOUNT;

                }
                
                Excelworksheet2.Cells[3, 3].Value = app.CADREQ_NO;
                Excelworksheet2.Cells[7 + i - 1, 1].Value = i;
                Excelworksheet2.Cells[7 + i - 1, 2].Value = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(app.FOR_MONTH) + " " + app.FOR_YEAR;
                Excelworksheet2.Cells[7 + i - 1, 3].Value = jobcode_id;
                Excelworksheet2.Cells[7 + i - 1, 4].Value = project_name;

                Excelworksheet2.Cells[7 + i - 1, j + 7].Value = app.DESCRIPTION;
               
                
                
                List<CADcostItem> items = CADcostHelper.getItem(app.ID_CADREQ, 0);
                int columnNumber;
                foreach (CADcostItem item in items)
                {
                    columnNumber = software.Where(x => x.ID_COST.Equals(item.ID_COST)).Select(x => software.IndexOf(x)).FirstOrDefault();
                    Excelworksheet2.Cells[7 + i - 1, 6 + columnNumber].Value = item.QUANTITY;

                }

                List<ActionHistoryDetail> history = CADcostHelper.getApprovalHistory(app.ID_CADREQ);
                int k = 16;
                foreach (ActionHistoryDetail act in history)
                {
                    Excelworksheet2.Cells[k, 5].Value = act.COMPLETED_BY;
                    Excelworksheet2.Cells[k, j + 7].Value = act.TIME_END;
                    k = k + 2;
                }
                
                sect = app.SECT_NAME;
            }
            Excelworksheet = Excelpackage.Workbook.Worksheets["DEPT"];
            Excelworksheet.Workbook.Worksheets.Delete(Excelpackage.Workbook.Worksheets["DEPT"]);
            Excelpackage.Save();
        }

        public static void createJINDusingSoftware(string fileName, int month, int year, int startmonth, int startyear, int endmonth, int endyear)
        {
            FileInfo file = new FileInfo(fileName);
            ExcelPackage Excelpackage = new ExcelPackage(file);
            ExcelWorksheet Excelworksheet = Excelpackage.Workbook.Worksheets["SOFTWARE"];
            List<CADREQ> approvedCADreq = CADcostHelper.getDoneApprovalAll(startmonth, startyear, endmonth, endyear);
            List<CADcostItem> items = new List<CADcostItem>();
            Dictionary<string, int> colSoftware = new Dictionary<string,int>();
            string sect = "";
            string jobcode = "";

            /*
            List<CellData> celldata = new List<CellData>()
            {
                new CellData{software = "SPR", sheet = "SPR", row=}
            };
             * */
            
            /*
            foreach(CADREQ cadreq in approvedCADreq)
            {
                items.AddRange(CADcostHelper.getItem(cadreq.ID_CADREQ));
            }
            items.OrderBy(x => x.ID_SOFTWARE);
            */

            int type2 = 0;
            foreach (CADREQ cadreq in approvedCADreq)
            {
                items = CADcostHelper.getItem(cadreq.ID_CADREQ, 0);
                foreach (CADcostItem item in items)
                {
                    switch (item.TYPE)
                    {
                        case true:

                            if (Excelpackage.Workbook.Worksheets[item.NAME_SOFTWARE] == null)
                            {
                                Excelworksheet = Excelpackage.Workbook.Worksheets["SOFTWARE"];
                                ExcelWorksheet Excelworksheet2 = Excelworksheet.Workbook.Worksheets.Copy("SOFTWARE", item.NAME_SOFTWARE);
                                colSoftware.Add(item.NAME_SOFTWARE, 0);
                                Excelworksheet2.Cells[10, 1].Value = "JIND";
                                Excelworksheet2.Cells[10, 2].Value = item.NAME_SOFTWARE;
                                Excelworksheet2.Cells[10, 3].Value = cadreq.JOBCODE;
                                Excelworksheet2.Cells[10, 4].Value = PFNATTHelper.getProjectName(cadreq.JOBCODE);
                                Excelworksheet2.Cells[11, 4].Value = Excelworksheet2.Cells[4, 4].Text;
                                Excelworksheet2.Cells[12, 4].Value = Excelworksheet2.Cells[5, 4].Text;
                                Excelworksheet2.Cells[13, 4].Value = Excelworksheet2.Cells[6, 4].Text;
                                Excelworksheet2.Cells[14, 4].Value = Excelworksheet2.Cells[7, 4].Text;
                                Excelworksheet2.Cells[15, 4].Value = Excelworksheet2.Cells[8, 4].Text;
                                Excelworksheet2.Cells[16, 4].Value = Excelworksheet2.Cells[9, 4].Text;
                                switch(PFNATTHelper.getEMPSect(cadreq.PIC))
                                {
                                    case "E280": //ITD
                                        if (cadreq.FOR_MONTH < startmonth)
                                        {
                                            Excelworksheet2.Cells[16, 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                        }
                                        else
                                        {
                                            Excelworksheet2.Cells[16, 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                        }
                                        break;
                                    case "E230": //PID
                                        if (cadreq.FOR_MONTH < startmonth)
                                        {
                                            Excelworksheet2.Cells[11, 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                        }
                                        else
                                        {
                                            Excelworksheet2.Cells[11, 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                        }
                                        break;
                                    case "E220": //CVD
                                        if (cadreq.FOR_MONTH < startmonth)
                                        {
                                            Excelworksheet2.Cells[12, 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                        }
                                        else
                                        {
                                            Excelworksheet2.Cells[12, 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                        }
                                        break;
                                    case "E250": //ELD
                                        if (cadreq.FOR_MONTH < startmonth)
                                        {
                                            Excelworksheet2.Cells[13, 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                        }
                                        else
                                        {
                                            Excelworksheet2.Cells[13, 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                        }
                                        break;
                                    case "E240": //INSD
                                        if (cadreq.FOR_MONTH < startmonth)
                                        {
                                            Excelworksheet2.Cells[14, 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                        }
                                        else
                                        {
                                            Excelworksheet2.Cells[14, 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                        }
                                        break;
                                }
                            }
                            else //kalo sheet softwarenya sudah ada
                            {
                                Excelworksheet = Excelpackage.Workbook.Worksheets[item.NAME_SOFTWARE];
                                if (!cadreq.JOBCODE.Equals(jobcode)) // jika jobcode belum ada di sheet tsb
                                {
                                    colSoftware[item.NAME_SOFTWARE]++;
                                }
                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 6), 1].Value = "JIND";
                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 6), 2].Value = item.NAME_SOFTWARE;
                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 6), 3].Value = cadreq.JOBCODE;
                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = PFNATTHelper.getProjectName(cadreq.JOBCODE);
                                Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[4, 4].Text;
                                Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[5, 4].Text;
                                Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[6, 4].Text;
                                Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[7, 4].Text;
                                Excelworksheet.Cells[15 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[8, 4].Text;
                                Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 4].Value = Excelworksheet.Cells[9, 4].Text;
                                    switch (PFNATTHelper.getEMPSect(cadreq.PIC))
                                    {
                                        case "E280": //ITD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                if (Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value != null)
                                                {
                                                    Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                                }
                                            }
                                            else
                                            {
                                                if (Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value != null)
                                                {
                                                    Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[16 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                                }
                                            }
                                            break;
                                        case "E230": //PID
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                if (Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value != null)
                                                {
                                                    Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                                }
                                            }
                                            else
                                            {
                                                if (Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value != null)
                                                {
                                                    Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                                }
                                            }
                                            break;
                                        case "E220": //CVD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                if (Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value != null)
                                                {
                                                    Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                                }
                                            }
                                            else
                                            {
                                                if (Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value != null)
                                                {
                                                    Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                                }
                                            }
                                            break;
                                        case "E250": //ELD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                if (Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value != null)
                                                {
                                                    Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                                }
                                            }
                                            else
                                            {
                                                if (Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value != null)
                                                {
                                                    Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                                }
                                            }
                                            break;
                                        case "E240": //INSD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                if (Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value != null)
                                                {
                                                    Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = item.QUANTITY;
                                                }
                                            }
                                            else
                                            {
                                                if (Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value != null)
                                                {
                                                    Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value.ToString()) + item.QUANTITY;
                                                }
                                                else
                                                {
                                                    Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 6), 5 + (cadreq.FOR_MONTH - startmonth)].Value = item.QUANTITY;
                                                }
                                            }
                                            break;
                                    }
                            }
                            break;
                        case false:
                            /*
                            ExcelWorksheet Excelworksheet3 = Excelpackage.Workbook.Worksheets["Others"];
                                if (!cadreq.JOBCODE.Equals(jobcode)) // jika jobcode belum ada di sheet tsb
                                {
                                    colSoftware[item.NAME_SOFTWARE]++;
                                }
                                Excelworksheet3.Cells[9 + (colSoftware[item.NAME_SOFTWARE] * 5), 1].Value = "JIND";
                                Excelworksheet3.Cells[9 + (colSoftware[item.NAME_SOFTWARE] * 5), 2].Value = item.NAME_SOFTWARE;
                                Excelworksheet3.Cells[9 + (colSoftware[item.NAME_SOFTWARE] * 5), 3].Value = cadreq.JOBCODE;
                                Excelworksheet3.Cells[9 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = PFNATTHelper.getProjectName(cadreq.JOBCODE);
                                Excelworksheet3.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = Excelworksheet.Cells[4, 4].Text;
                                Excelworksheet3.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = Excelworksheet.Cells[5, 4].Text;
                                Excelworksheet3.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = Excelworksheet.Cells[6, 4].Text;
                                Excelworksheet3.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = Excelworksheet.Cells[7, 4].Text;
                                Excelworksheet3.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 5), 4].Value = Excelworksheet.Cells[8, 4].Text;
                                    switch (PFNATTHelper.getEMPSect(cadreq.PIC))
                                    {
                                        case "E280": //ITD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Text) + item.QUANTITY;
                                            }
                                            else
                                            {
                                                Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[14 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Text) + item.QUANTITY;
                                            }
                                            break;
                                        case "E230": //PID
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Text) + item.QUANTITY;
                                            }
                                            else
                                            {
                                                Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[10 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Text)+item.QUANTITY;
                                            }
                                            break;
                                        case "E220": //CVD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Text) + item.QUANTITY;
                                            }
                                            else
                                            {
                                                Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[11 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Text) + item.QUANTITY;
                                            }
                                            break;
                                        case "E250": //ELD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Text) + item.QUANTITY;
                                            }
                                            else
                                            {
                                                Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[12 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Text) + item.QUANTITY;
                                            }
                                            break;
                                        case "E240": //INSD
                                            if (cadreq.FOR_MONTH < startmonth)
                                            {
                                                Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Value = Convert.ToInt32(Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (12 - startmonth + cadreq.FOR_MONTH)].Text) + item.QUANTITY;
                                            }
                                            else
                                            {
                                                Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Value = Convert.ToInt32(Excelworksheet.Cells[13 + (colSoftware[item.NAME_SOFTWARE] * 5), 5 + (cadreq.FOR_MONTH - startmonth)].Text) + item.QUANTITY;
                                            }
                                            break;
                                    }*/
                            break;
                             
                    }
                    jobcode = cadreq.JOBCODE;
                }
                
                sect = PFNATTHelper.getEMPSect(cadreq.PIC);
            }
            Excelworksheet = Excelpackage.Workbook.Worksheets["SOFTWARE"];
            Excelworksheet.Workbook.Worksheets.Delete(Excelpackage.Workbook.Worksheets["SOFTWARE"]);
            Excelworksheet.Workbook.Worksheets.Delete(Excelpackage.Workbook.Worksheets["Others"]);
            Excelpackage.Save();

            Excelpackage = new ExcelPackage(file);
            int range = endmonth - startmonth + 12 * (endyear - startyear);
            int j;
            int monthloop;
            string test = "";
            for (int i = 1; i <= colSoftware.Count; i++)
            {
                j=0;
                monthloop = startmonth;
                Excelworksheet = Excelpackage.Workbook.Worksheets[i];
                Excelworksheet.Cells[1, 5].Value = startyear;
                while (j<=range)
                {
                    Excelworksheet.Cells[2, 5 + j].Value = (new DateTime(2015, monthloop, 1)).ToString("MMMM");
                    //test = Excelworksheet.Cells[2, 5 + j].Value.ToString();
                    if (monthloop < 12) monthloop++;
                    else
                    {
                        if(j!=range)
                        Excelworksheet.Cells[1, 5 + j + 1].Value = endyear;
                        monthloop = 1;
                    }
                    j++;
                }
                int row = Convert.ToInt32(colSoftware.Values.ElementAtOrDefault(i));
                Excelworksheet.Cells[2, 4, 16 + (row * 6), 5 + range].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                Excelworksheet.Cells[2, 4, 16 + (row * 6), 5 + range].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                Excelworksheet.Cells[2, 4, 16 + (row * 6), 5 + range].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                Excelworksheet.Cells[2, 4, 16 + (row * 6), 5 + range].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //Excelworksheet.Cells[2, 5, 16 + (row * 6), 5 + range].Value = 0 + Excelworksheet.Cells[2, 5, 16 + (row * 6), 5 + range].Value;
                if (year == startyear)
                {
                    Excelworksheet.SelectedRange[1, (5 + month - startmonth), (16 + (row * 6)), (5 + month - startmonth)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    Excelworksheet.SelectedRange[1, (5 + month - startmonth), (16 + (row * 6)), (5 + month - startmonth)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                }
                else if (year == endyear)
                {
                    Excelworksheet.SelectedRange[1, (5 + 12 - startmonth + month), (16 + (row * 6)), (5 + 12 - startmonth + month)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    Excelworksheet.SelectedRange[1, (5 + 12 - startmonth + month), (16 + (row * 6)), (5 + 12 - startmonth + month)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                }
            }
            Excelpackage.Save();
             
        }

        public static void createIntegraphLicenseDistribution(string fileName, int month, int year)
        {
            FileInfo file = new FileInfo(fileName);
            
            ExcelPackage Excelpackage = new ExcelPackage(file);
            ExcelWorksheet Excelworksheet = Excelpackage.Workbook.Worksheets["Cover"];
            Excelworksheet.Cells[12, 2].Value = (new DateTime(year, month, 1)).ToString(DateTimeFormatInfo.CurrentInfo.YearMonthPattern);
            Excelworksheet = Excelpackage.Workbook.Worksheets["Integraph SPLM"];
            List<SoftwareServer> softwareServer = CADcostHelper.getAllSoftwareServer(month, year);
            int i = 0;
            
            foreach (SoftwareServer ss in softwareServer)
            {
                Excelworksheet.Cells[5 + i, 1].Value = (i + 1).ToString();
                Excelworksheet.Cells[5 + i, 2].Value = ss.PROJECT_NAME;
                Excelworksheet.Cells[5 + i, 3].Value = ss.NAME_SOFTWARE;
                Excelworksheet.Cells[5 + i, 4].Value = ss.DEPT;
                Excelworksheet.Cells[5 + i, 5].Value = ss.QUANTITY;
                Excelworksheet.Cells[5 + i, 6].Value = ss.IP_SERVER;
                Excelworksheet.Cells[5 + i, 7].Value = ss.LICENSE_STATUS;
                Excelworksheet.Cells[5 + i, 8].Value = ss.PIC;
                
                i++;
            }
            Excelpackage.Save();
            file.IsReadOnly = false;
                
                Excel.Application app;
                Excel.Workbook wkb;
                Excel.Worksheet wks;
            
                app = new Excel.Application();
                app.DisplayAlerts = false;
                wkb = app.Workbooks.Open(fileName);
                wks = wkb.Worksheets["Chart"] as Excel.Worksheet;

                try
                {


                    wkb.RefreshAll();
                    wkb.Save();

                }
                catch (Exception e)
                { }
                finally
                {

                    if (wkb != null)
                    {
                        
                        wkb.Close(false, Type.Missing, Type.Missing);
                        //wkb = null;
                        Marshal.ReleaseComObject(wkb);
                        Marshal.ReleaseComObject(wks);
                        wks = null;
                        wkb = null;
                        
                    }
                    if (app != null)
                    {
                        app.Quit();
                        //app = null;
                        Marshal.ReleaseComObject(app);
                        app = null;
                    }

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    file.IsReadOnly = false;
                }
                    /*
                    object paramMissing = Type.Missing;
                    string paramSourceBookPath = fileName;
                    app = new Excel.Application();
            
                    app.DisplayAlerts = false;
                    wkb = app.Workbooks.Open(fileName);
                    wks = wkb.Worksheets["Integraph SPLM"] as Excel.Worksheet;
                    
                    string filePath = fileName.Replace(".xlsx", ".pdf");
                    wks.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, filePath);



                    if (wkb != null)
                    {
                        
                        wkb.Close(false, Type.Missing, Type.Missing);
                        //wkb = null;
                        Marshal.ReleaseComObject(wkb);
                        Marshal.ReleaseComObject(wks);
                        wks = null;
                        wkb = null;

                    }
                    if (app != null)
                    {
                        app.Quit();
                        //app = null;
                        Marshal.ReleaseComObject(app);
                        app = null;
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    /*
                        try
                        {
                            Spire.Xls.Workbook wb = new Spire.Xls.Workbook();
                            wb.LoadFromFile(fileName);
                    
                            Spire.Xls.Worksheet ws = wb.Worksheets["Chart"];
                            Spire.Xls.Chart chart = ws.Charts[0];
                            chart.
                            //Image[] imgs = wb.SaveChartAsImage(ws);
                            //foreach (Image img in imgs)
                                //img.Save(string.Format("img-{0}.png", i), System.Drawing.Imaging.ImageFormat.Png);

                            fileName = fileName.Replace(".xlsx", ".pdf");
                    
                            FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                            ws.SaveToPdfStream(fs);
                            System.Diagnostics.Process.Start(filePath);
                            fs.Flush();
                            fs.Close();
            
                            Image[] imgs = wb.SaveChartAsImage(ws);
                            foreach (Image img in imgs)
                                img.Save(string.Format("img-{0}.png", i), System.Drawing.Imaging.ImageFormat.Png);

                            filePath = fileName.Replace(".xlsx", ".pdf");
                            PdfDocument pdfd = new PdfDocument();

                            PdfConverter pdfc = new PdfConverter(ws);

                            pdfd = pdfc.Convert();

                            pdfd.SaveToFile(filePath);
                       

                    
                        }
                        catch (Exception ex)
                        { }
                        */


                    //wks.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, filePath);

                    /*
                    if (wkb != null)
                    {

                        wkb.Close(false, Type.Missing, Type.Missing);
                        //wkb = null;
                        Marshal.ReleaseComObject(wks);
                        Marshal.ReleaseComObject(wkb);
                    
                        wks = null;
                        wkb = null;
                    }
                    if (app != null)
                    {
                        app.Quit();
                        //app = null;
                        Marshal.ReleaseComObject(app);
                        app = null;
                    }

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    */
                //}
        }

        public static void createSoftwareChart(string fileNameTemplate, int month, int year)
        {
            try
            {
                ReportDocument report = new ReportDocument();
                Stream Stream;
                report.Load(fileNameTemplate);
                report.SetDatabaseLogon("sa", "m1c4system",
                              "172.16.17.41", "CADcost");
                //ForMonth
                ParameterValues ForMonthValues;
                ParameterDiscreteValue ForMonthV = new ParameterDiscreteValue();
                ParameterFieldDefinition ForMonthD = report.DataDefinition.ParameterFields["@ForMonth"];
                ForMonthV.Value = month;
                ForMonthValues = ForMonthD.CurrentValues;
                ForMonthValues.Add(ForMonthV);
                ForMonthD.ApplyCurrentValues(ForMonthValues);

                //ForYear
                ParameterValues ForYearValues;
                ParameterDiscreteValue ForYearV = new ParameterDiscreteValue();
                ParameterFieldDefinition ForYearD = report.DataDefinition.ParameterFields["@ForYear"];
                ForYearV.Value = year;
                ForYearValues = ForYearD.CurrentValues;
                ForYearValues.Add(ForYearV);
                ForYearD.ApplyCurrentValues(ForYearValues);

                CrystalDecisions.CrystalReports.Engine.TextObject TextTitle;
                TextTitle = report.ReportDefinition.ReportObjects["TextTitle"] as TextObject;
                TextTitle.Text += " "+ CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month) +" "+ year.ToString();

                Stream = report.ExportToStream(ExportFormatType.PortableDocFormat);
                report.Close();
                report.Dispose();

                int Length = (int)Stream.Length;
                byte[] Buffer = new byte[Length];
                Stream.Read(Buffer, 0, Length);
                Stream.Close();

                string filePath = fileNameTemplate.Replace(".rpt", "_" + month + "_" + year + ".pdf");
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
                fs.Write(Buffer, 0, Length);
                fs.Flush();
                fs.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}