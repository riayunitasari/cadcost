﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CADcost.Entities;
using CADcost.Overtime;
using System.Web.UI.WebControls;
using System.Globalization;

namespace CADcost.Helper
{
    [Serializable]
    public class CADcostItem
    {
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public int QUANTITY { get; set; }
        public decimal? PRICE { get; set; }
        public string STATUS { get; set; }
        public string IP_SERVER { get; set; }
        public string REMARK { get; set; }
        public bool TYPE { get; set; }
        public int ID_CADDETAIL { get; set; }
        public int? ID_COST { get; set; }
        public int? ID_SOFTWARE_SERVER { get; set; }

        public CADcostItem() { }
        public CADcostItem(int id, string name, int qtty, string remark, bool type)
        {
            ID_SOFTWARE = id;
            NAME_SOFTWARE = name;
            QUANTITY = qtty;
            REMARK = remark;
            TYPE = type;
        }
        public CADcostItem(int id, decimal price)
        {
            ID_SOFTWARE = id;
            PRICE = price;
        }

    }

    public class Approve
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string FULL_NAME { get; set; }
        public string SECT_NAME { get; set; }
        public string DATE_REQUEST { get; set; }
        public string TIME_REQUEST { get; set; }
        public int ID_ACTIVITY { get; set; }
        public string NAME_ACTIVITY { get; set; }
        public string DESCRIPTION { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public int FOR_MONTH { get; set; }
        public int FOR_YEAR { get; set; }
    }

    public class LicenseHistory
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string ONBEHALF_BY { get; set; }
        public string DATE_REQUEST { get; set; }
        public string TIME_REQUEST { get; set; }
        public string NAME_ACTIVITY { get; set; }
        public string ACTION_BY { get; set; }
        public string DESCRIPTION { get; set; }
        public string COMMENT { get; set; }
    }

    public class ActionHistoryDetail
    {
        public int ID_CADREQ { get; set; }
        public string CADREQ_NO { get; set; }
        public string CADREQ_BY { get; set; }
        public string ONBEHALF_BY { get; set; }
        public string DEPT { get; set; }
        public string TIME_REQUEST { get; set; }
        public string NAME_ACTION { get; set; }
        public string ACTION_ONBEHALF { get; set; }
        public string TIME_END { get; set; }
        public string DESCRIPTION { get; set; }
        public string COMMENT { get; set; }
        public string COMPLETED_BY { get; set; }
    }

    public class EmployeeData
    {
        public string SECT_ID { get; set; }
        public string SECT_NAME { get; set; }
        public string EMP_ID { get; set; }
        public string EMP_NAME { get; set; }
        public string POSITION { get; set; }
    }

    public class SoftwareVendor
    {
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public string DATE_RENEWAL { get; set; }
        public int ID_VENDOR { get; set; }
        public string NAME_VENDOR { get; set; }
    }

    public class SoftwareCost
    {
        public SoftwareCost() { COST_TO_COST = 0; }
        public int ID_COST { get; set; }
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public string TYPE { get; set; }
        public decimal CAD_COST { get; set; }
        public decimal COST_TO_COST { get; set; }
        public decimal REQ_COST { get; set; }
        public string CAD_STARTDATE { get; set; }
    }

    public class SoftwareServer
    {
        public int ID_SOFTWARE_SERVER { get; set; }
        public int ID_SOFTWARE { get; set; }
        public string NAME_SOFTWARE { get; set; }
        public string LICENSE_STATUS { get; set; }
        public string DEPT { get; set; }
        public int QUANTITY { get; set; }
        public int OWNED { get; set; }
        public int ID_SERVER { get; set; }
        public string IP_SERVER { get; set; }
        public string PROJECT_NAME { get; set; }
        public string PIC { get; set; }
        public string REMARK { get; set; }
    }

    public class ProjectRater
    {
        public int ID_RATER {get; set;}
        public string SECT_ID { get; set; }
        public string DEPARTMENT {get; set;}
        public string FirstRaterID { get; set; }
        public string FirstRater {get; set;}
        public string SecondRaterID { get; set; }
        public string SecondRater {get; set;}
    }

    public class Delegation
    {
        public string Emp_delegatingID{get; set;}
        public string Emp_delegating{get; set;}
        public string Emp_delegatedID{get; set;}
        public string Emp_delegated{get; set;}
    }

    public class ItemComparer : IEqualityComparer<CADcostItem>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(CADcostItem x, CADcostItem y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.ID_SOFTWARE == y.ID_SOFTWARE;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(CADcostItem product)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(product, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = product.ID_SOFTWARE == null ? 0 : product.ID_SOFTWARE.GetHashCode();

            //Get hash code for the Code field.
            int hashProductCode = product.ID_SOFTWARE.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }

    }

    public class ApproveComparer : IEqualityComparer<Approve>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(Approve x, Approve y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.ID_CADREQ == y.ID_CADREQ;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Approve cadreq)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(cadreq, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = cadreq.ID_CADREQ == null ? 0 : cadreq.ID_CADREQ.GetHashCode();

            //Get hash code for the Code field.
            int hashProductCode = cadreq.ID_CADREQ.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }

    }

    public class Role : IEquatable<Role>
    {
        public int intRole { get; set; }
        public string Sect { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Role objAsPart = obj as Role;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }

        public bool Equals(Role other)
        {
            if (other == null) return false;
            return (this.intRole.Equals(other.intRole) && this.Sect.Equals(other.Sect));
        }
    }

    public class Sect
    {
        string SectId { get; set; }

    }

    public class CADcostHelper
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";


        public static T Clone<T>(T source)
        {
            var dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(T));
            using (var ms = new System.IO.MemoryStream())
            {
                dcs.WriteObject(ms, source);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)dcs.ReadObject(ms);
            }
        }

        public static List<PROJECT> getAllActiveProjectName()
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<string, string, PROJECT> makeProject = (t1, t2) => new PROJECT { JOBCODE_ID = t1, PROJECT_NAME = t2 };
            List<PROJECT> project = (from proj in data.PROJECTs
                                     select makeProject(proj.JOBCODE_ID, proj.PROJECT_NAME)).ToList();
            return project;
        }

        public static void generateDDLProjectName(DropDownList ddl)
        {
            List<PROJECT> projectName = getAllActiveProjectName();
            foreach (PROJECT prjct in projectName)
            {
                ddl.Items.Add(new ListItem(prjct.JOBCODE_ID.ToString() + " - " + prjct.PROJECT_NAME.ToString(), prjct.JOBCODE_ID.ToString()));
            }
        }

        public static void generateDDLCurrency(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<CURRENCY> currency = new List<CURRENCY>();
            currency = (from curr in data.CURRENCies select curr).ToList();
            foreach (CURRENCY cur in currency)
            {
                ddl.Items.Add(new ListItem(cur.NAME_CURRENCY, cur.ID_CURRENCY.ToString()));
            }
        }

        public static List<CADcostItem>getItem(int id_cadreq, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<CADcostItem> result = new List<CADcostItem>();
            List<CADcostItem> result2 = new List<CADcostItem>();

            Func<int, string, int, decimal?, string, bool, int,int?, int?, CADcostItem> makeCADcostItem = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new CADcostItem {ID_SOFTWARE = p1, NAME_SOFTWARE = p2, QUANTITY = p3, PRICE=p4, REMARK = p5, TYPE=p6, ID_CADDETAIL=p7, ID_COST=p8, ID_SOFTWARE_SERVER=p9};
            Func<int, decimal, CADcostItem> makeCADcostItem2 = (p1, p2) => new CADcostItem { ID_SOFTWARE = p1, PRICE = p2 };

                result2 = (from item in data.CADDETAILs
                           join software in data.SOFTWAREs on item.ID_SOFTWARE equals software.ID_SOFTWARE
                           where item.ID_CADREQ.Equals(id_cadreq)
                           select makeCADcostItem(item.ID_SOFTWARE, software.NAME_SOFTWARE, item.QUANTITY, null, item.REMARK, software.TYPE, item.ID_CADDETAIL, item.ID_COST, item.ID_SOFTWARE_SERVER)).ToList();
            decimal rates =1;
            if (id_currency != 0)
               rates  = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.RATES).FirstOrDefault();

            foreach (CADcostItem cci in result2)
            {
                if(cci.ID_COST!=null)
                 cci.PRICE = getSoftwareCostItem((int)cci.ID_COST) * rates * cci.QUANTITY;
            }

            return result2;
        }

        public static List<CADcostItem> getNullItem(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<int, string, int, decimal?, string, bool, int, int?, int?, CADcostItem> makeCADcostItem = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new CADcostItem { ID_SOFTWARE = p1, NAME_SOFTWARE = p2, QUANTITY = p3, PRICE = p4, REMARK = p5, TYPE = p6, ID_CADDETAIL = p7, ID_COST = p8, ID_SOFTWARE_SERVER = p9 };
            List<CADcostItem> result2 = new List<CADcostItem>();
            result2 = (from item in data.CADDETAILs
                       join software in data.SOFTWAREs on item.ID_SOFTWARE equals software.ID_SOFTWARE
                       where item.ID_CADREQ.Equals(id_cadreq) && (item.ID_COST==null || item.ID_SOFTWARE_SERVER==null)
                       select makeCADcostItem(item.ID_SOFTWARE, software.NAME_SOFTWARE, item.QUANTITY, null, item.REMARK, software.TYPE, item.ID_CADDETAIL, item.ID_COST, item.ID_SOFTWARE_SERVER)).ToList();
            return result2;
        }

        public static decimal getSoftwareCostItem(int id_cost)
        {
            CADcostDataContext data = new CADcostDataContext();
            decimal cost = (decimal)data.SOFTWARE_COSTs.Where(x => x.ID_COST.Equals(id_cost)).FirstOrDefault().COST;
            return cost;
        }

        public static List<string> getAllENITEmail()
        {
            CADcostDataContext data = new CADcostDataContext();

            List<string> enitEmail = (from admin in data.ADMINs
                          select admin.Email).ToList();

            return enitEmail;
        }

        public static List<string> getFilteredProject(string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();

            List<string> jobcode = (from rater in data.RATERs
                                    where rater.FirstRater.Equals(emp_id) || rater.SecondRater.Equals(emp_id)
                                    select rater.JOBCODE_ID).ToList();
            return jobcode;
        }

        public static bool isRequester(string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();

            return data.REQUESTERs.Any(x => x.EMP_ID.Equals(emp_id));
        }

        public static List<Approve> getApproveENIT(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<Approve> result = new List<Approve>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal?, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new Approve {ID_CADREQ=p1, CADREQ_NO = p2, FULL_NAME=p3, SECT_NAME=p4, DATE_REQUEST=p5, TIME_REQUEST=p6, ID_ACTIVITY=p7, NAME_ACTIVITY=p8, DESCRIPTION=p9, TOTAL_AMOUNT=p10};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID=p1, SECT_NAME=p2, EMP_ID=p3, EMP_NAME=p4 };

            tempEmployee =(from employee in data2.TBL_EMPs
                           join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                           let sect = section.SECT_NAME.Split('-') 
                           select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            if(!jobcode_id.Equals("0"))
            result = (from employee in tempEmployee
                      join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(6)
                      orderby cadreq.TIME_REQUEST descending
                      select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), cadreq.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            else
            result = (from employee in tempEmployee
                        join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                        join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                        where cadreq.ID_ACTIVITY.Equals(6)
                        orderby cadreq.TIME_REQUEST descending
                      select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), cadreq.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            return result;
        }

        public static List<Approve> getApproveENITManager(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<Approve> result = new List<Approve>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal?, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new Approve { ID_CADREQ = p1, CADREQ_NO = p2, FULL_NAME = p3, SECT_NAME = p4, DATE_REQUEST = p5, TIME_REQUEST = p6, ID_ACTIVITY=p7, NAME_ACTIVITY = p8, DESCRIPTION = p9, TOTAL_AMOUNT=p10};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            if (!jobcode_id.Equals("0"))
            result = (from employee in tempEmployee
                      join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(7)
                      orderby cadreq.TIME_REQUEST descending
                      select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            else
            result = (from employee in tempEmployee
                        join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                        join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                        where cadreq.ID_ACTIVITY.Equals(7)
                        orderby cadreq.TIME_REQUEST descending
                      select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            return result;
        }

        public static List<Approve> getApproveFirstRater(string jobcode_id, string sect_id, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<Approve> result = new List<Approve>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal?, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new Approve { ID_CADREQ = p1, CADREQ_NO = p2, FULL_NAME = p3, SECT_NAME = p4, DATE_REQUEST = p5, TIME_REQUEST = p6, ID_ACTIVITY=p7, NAME_ACTIVITY = p8, DESCRIPTION = p9, TOTAL_AMOUNT=p10};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

                tempEmployee = (from employee in data2.TBL_EMPs
                                join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                                let sect = section.SECT_NAME.Split('-')
                                select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
                if (!jobcode_id.Equals("0"))
                result = (from employee in tempEmployee
                          join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                          join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                          where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(2) && ((getRole(emp_id, cadreq.JOBCODE).intRole == 3 || getRole(emp_id, cadreq.JOBCODE).intRole == 2) && PFNATTHelper.getEMPSect(cadreq.PIC).Equals(getRole(emp_id, cadreq.JOBCODE).Sect))
                          orderby cadreq.TIME_REQUEST descending
                          orderby cadreq.TIME_REQUEST descending
                          select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
                else
                    result = (from employee in tempEmployee
                              join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                              join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                              where cadreq.ID_ACTIVITY.Equals(2) && ((getRole(emp_id, cadreq.JOBCODE).intRole == 3 || getRole(emp_id, cadreq.JOBCODE).intRole == 2) && PFNATTHelper.getEMPSect(cadreq.PIC).Equals(getRole(emp_id, cadreq.JOBCODE).Sect))
                              orderby cadreq.TIME_REQUEST descending
                              select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            return result;
        }

        public static List<Approve> getApproveSecondRater(string jobcode_id, string sect_id, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<Approve> result = new List<Approve>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal?, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new Approve { ID_CADREQ = p1, CADREQ_NO = p2, FULL_NAME = p3, SECT_NAME = p4, DATE_REQUEST = p5, TIME_REQUEST = p6, ID_ACTIVITY=p7, NAME_ACTIVITY = p8, DESCRIPTION = p9, TOTAL_AMOUNT=p10};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

                tempEmployee = (from employee in data2.TBL_EMPs
                                join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                                let sect = section.SECT_NAME.Split('-')
                                select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
                if (!jobcode_id.Equals("0"))
                result = (from employee in tempEmployee
                          join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                          join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                          where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(5) && ((getRole(emp_id, cadreq.JOBCODE).intRole == 4 || getRole(emp_id, cadreq.JOBCODE).intRole == 2) && PFNATTHelper.getEMPSect(cadreq.PIC).Equals(getRole(emp_id, cadreq.JOBCODE).Sect))
                          orderby cadreq.TIME_REQUEST descending
                          select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
                else
                result = (from employee in tempEmployee
                            join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                            join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                            where cadreq.ID_ACTIVITY.Equals(5) && ((getRole(emp_id, cadreq.JOBCODE).intRole == 4 || getRole(emp_id, cadreq.JOBCODE).intRole == 2) && PFNATTHelper.getEMPSect(cadreq.PIC).Equals(getRole(emp_id, cadreq.JOBCODE).Sect))
                            orderby cadreq.TIME_REQUEST descending
                          select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT)).ToList();
            return result;
        }

        public static List<Approve> getDoneApproval(string jobcode_id, int month, int year)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<Approve> result = new List<Approve>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal?, int, int, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12) => new Approve { ID_CADREQ = p1, CADREQ_NO = p2, FULL_NAME = p3, SECT_NAME = p4, DATE_REQUEST = p5, TIME_REQUEST = p6, ID_ACTIVITY = p7, NAME_ACTIVITY = p8, DESCRIPTION = p9, TOTAL_AMOUNT = p10, FOR_MONTH=p11, FOR_YEAR=p12};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();

            result = (from employee in tempEmployee
                      join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.JOBCODE.Contains(jobcode_id) && cadreq.ID_ACTIVITY.Equals(7) && cadreq.FOR_MONTH.Equals(month) && cadreq.FOR_YEAR.Equals(year)
                      orderby employee.SECT_ID descending, cadreq.TIME_REQUEST ascending
                      select makeApprove(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_ID + " - " + employee.EMP_NAME, employee.SECT_NAME, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), cadreq.ID_ACTIVITY, activity.NAME_ACTIVITY, cadreq.DESCRIPTION, cadreq.TOTAL_AMOUNT, cadreq.FOR_MONTH, cadreq.FOR_YEAR)).ToList();

            return result;
        }

        public static List<CADREQ> getDoneApprovalAll(int startmonth, int startyear, int endmonth, int endyear)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<CADREQ> result = new List<CADREQ>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();

            Func<int, string, string, string, string, string, int, string, string, decimal, Approve> makeApprove = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new Approve { ID_CADREQ = p1, CADREQ_NO = p2, FULL_NAME = p3, SECT_NAME = p4, DATE_REQUEST = p5, TIME_REQUEST = p6, ID_ACTIVITY = p7, NAME_ACTIVITY = p8, DESCRIPTION = p9, TOTAL_AMOUNT = p10 };
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            
            
            result = (from employee in tempEmployee
                      join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                      join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                      where cadreq.ID_ACTIVITY.Equals(7) && ((cadreq.FOR_MONTH-startmonth)+12*(cadreq.FOR_YEAR-startyear)) >=0 && ((endmonth-cadreq.FOR_MONTH)+12*(endyear-cadreq.FOR_YEAR)) >=0
                      orderby cadreq.FOR_YEAR, cadreq.FOR_MONTH, cadreq.JOBCODE, employee.SECT_ID descending
                      select cadreq).ToList();

            return result;
        }

        public static string getLastActivityComment(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            string comment="";
            comment = (from activity in data.ACTION_HISTORies
                       group activity by activity.ID_CADREQ into timeact
                       where timeact.Key.Equals(id_cadreq)
                       select timeact.First(x => x.TIME_END == timeact.Max(y => y.TIME_END)).COMMENT).FirstOrDefault();
            return comment;
        }

        public static List<LicenseHistory> getLicenseHistory(string jobcode_id, string emp_id, int month, int year, int status)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<LicenseHistory> result = new List<LicenseHistory>();
            Func<int, string, string, string, string, string, string, string, string, LicenseHistory> makeLicenseHistory = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new LicenseHistory { ID_CADREQ=p1, CADREQ_NO = p2, ONBEHALF_BY=p3, DATE_REQUEST = p4, TIME_REQUEST = p5, NAME_ACTIVITY = p6, ACTION_BY=p7, DESCRIPTION=p8, COMMENT=p9};
            if (!jobcode_id.Equals("0"))
            {
                if(status==0)
                result = (from act in data.ACTION_HISTORies
                          group act by act.ID_CADREQ into timeact
                          join cadreq in data.CADREQs on timeact.Key equals cadreq.ID_CADREQ
                          join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                          where cadreq.JOBCODE.Equals(jobcode_id) && cadreq.PIC.Equals(emp_id) && cadreq.FOR_MONTH.Equals(month) && cadreq.FOR_YEAR.Equals(year)
                          orderby cadreq.TIME_REQUEST descending
                          select makeLicenseHistory(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.CADREQ_BY, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.LASTACTIVITY_BY, cadreq.DESCRIPTION, timeact.OrderByDescending(x => x.TIME_END).First().COMMENT)).ToList();
                else
                result = (from act in data.ACTION_HISTORies
                            group act by act.ID_CADREQ into timeact
                            join cadreq in data.CADREQs on timeact.Key equals cadreq.ID_CADREQ
                            join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                            where cadreq.JOBCODE.Equals(jobcode_id) && cadreq.PIC.Equals(emp_id) && cadreq.FOR_MONTH.Equals(month) && cadreq.FOR_YEAR.Equals(year) && cadreq.ID_ACTIVITY.Equals(status)
                            orderby cadreq.TIME_REQUEST descending
                          select makeLicenseHistory(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.CADREQ_BY, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.LASTACTIVITY_BY, cadreq.DESCRIPTION, timeact.OrderByDescending(x => x.TIME_END).First().COMMENT)).ToList();
            }
            else
            {
                if(status == 0)
                result = (from act in data.ACTION_HISTORies
                          group act by act.ID_CADREQ into timeact
                          join cadreq in data.CADREQs on timeact.Key equals cadreq.ID_CADREQ
                          join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                          where cadreq.PIC.Equals(emp_id) && cadreq.TIME_REQUEST.Month.Equals(month) && cadreq.TIME_REQUEST.Year.Equals(year)
                          orderby cadreq.TIME_REQUEST descending
                          select makeLicenseHistory(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.CADREQ_BY, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.LASTACTIVITY_BY, cadreq.DESCRIPTION, timeact.OrderByDescending(x => x.TIME_END).First().COMMENT)).ToList();
                else
                result = (from act in data.ACTION_HISTORies
                            group act by act.ID_CADREQ into timeact
                            join cadreq in data.CADREQs on timeact.Key equals cadreq.ID_CADREQ
                            join activity in data.ACTIVITies on cadreq.ID_ACTIVITY equals activity.ID_ACTIVITY
                            where cadreq.PIC.Equals(emp_id) && cadreq.TIME_REQUEST.Month.Equals(month) && cadreq.TIME_REQUEST.Year.Equals(year) && cadreq.ID_ACTIVITY.Equals(status)
                            orderby cadreq.TIME_REQUEST descending
                          select makeLicenseHistory(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.CADREQ_BY, cadreq.TIME_REQUEST.Date.ToString("dd-MM-yyyy", CultureInfo.CreateSpecificCulture("en-US")), cadreq.TIME_REQUEST.ToShortTimeString(), activity.NAME_ACTIVITY, cadreq.LASTACTIVITY_BY, cadreq.DESCRIPTION, timeact.OrderByDescending(x => x.TIME_END).First().COMMENT)).ToList();
            }

            foreach (LicenseHistory lcHistory in result)
            {
                lcHistory.ONBEHALF_BY = lcHistory.ONBEHALF_BY+"-"+PFNATTHelper.getEMPFullName(lcHistory.ONBEHALF_BY);
                lcHistory.ACTION_BY = lcHistory.ACTION_BY + "-" + PFNATTHelper.getEMPFullName(lcHistory.ACTION_BY);
            }
            return result;
        }

        public static List<ActionHistoryDetail> getAllActionHistory(string jobcode_id, string emp_id, int month, int year, int action_type)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<ActionHistoryDetail> acthistory = new List<ActionHistoryDetail>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();
            Func<int, string, string, string, string, string, string, string, string, string, string, ActionHistoryDetail> makeActionHistoryDetail = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11) => new ActionHistoryDetail { ID_CADREQ = p1, CADREQ_NO = p2, CADREQ_BY = p3, ONBEHALF_BY=p4, DEPT = p5, TIME_REQUEST = p6, NAME_ACTION = p7, ACTION_ONBEHALF = p8, TIME_END = p9, DESCRIPTION = p10, COMMENT = p11 };
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };
            
            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            if (!jobcode_id.Equals("0"))
            {
                if(action_type == 0)
                    acthistory = (from employee in tempEmployee
                                  join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                                  join act in data.ACTION_HISTORies on cadreq.ID_CADREQ equals act.ID_CADREQ
                                  join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                                  where act.COMPLETED_BY.ToString().Equals(emp_id) && act.JOBCODE.Equals(jobcode_id) && act.TIME_END.Value.Month.Equals(month) && act.TIME_END.Value.Year.Equals(year)
                                  select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY), employee.SECT_NAME, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, PFNATTHelper.getEMPFullName(act.PARTICIPANT), act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT)).ToList();
                else
                acthistory = (from employee in tempEmployee
                                join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                                join act in data.ACTION_HISTORies on cadreq.ID_CADREQ equals act.ID_CADREQ
                                join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                                where act.COMPLETED_BY.ToString().Equals(emp_id) && act.JOBCODE.Equals(jobcode_id) && act.TIME_END.Value.Month.Equals(month) && act.TIME_END.Value.Year.Equals(year) && act.ID_ACTION.Equals(action_type)
                                select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY), employee.SECT_NAME, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, PFNATTHelper.getEMPFullName(act.PARTICIPANT), act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT)).ToList();
            }
            else
            {
                if(action_type == 0)
                acthistory = (from employee in tempEmployee
                              join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                              join act in data.ACTION_HISTORies on cadreq.ID_CADREQ equals act.ID_CADREQ
                              join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                              where act.COMPLETED_BY.ToString().Equals(emp_id) && act.TIME_END.Value.Month.Equals(month) && act.TIME_END.Value.Year.Equals(year)
                              select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY), employee.SECT_NAME, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, PFNATTHelper.getEMPFullName(act.PARTICIPANT), act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT)).ToList();
                else
                acthistory = (from employee in tempEmployee
                                join cadreq in data.CADREQs on employee.EMP_ID equals cadreq.PIC
                                join act in data.ACTION_HISTORies on cadreq.ID_CADREQ equals act.ID_CADREQ
                                join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                                where act.COMPLETED_BY.ToString().Equals(emp_id) && act.TIME_END.Value.Month.Equals(month) && act.TIME_END.Value.Year.Equals(year) && act.ID_ACTION.Equals(action_type)
                                select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, employee.EMP_NAME, PFNATTHelper.getEMPFullName(cadreq.CADREQ_BY), employee.SECT_NAME, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, PFNATTHelper.getEMPFullName(act.PARTICIPANT), act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT)).ToList();
            }
            acthistory = acthistory.OrderByDescending(x => x.TIME_END).ThenBy(x => x.NAME_ACTION).ToList();
            return acthistory;
        }

        public static List<ActionHistoryDetail> getApprovalHistory(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            List<ActionHistoryDetail> acthistory = new List<ActionHistoryDetail>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();
            
            Func<int, string, string, string, string, string, string, string, ActionHistoryDetail> makeActionHistoryDetail = (p1, p2, p3, p4, p5, p6, p7, p8) => new ActionHistoryDetail { ID_CADREQ = p1, CADREQ_NO = p2, TIME_REQUEST = p3, NAME_ACTION = p4, TIME_END = p5, DESCRIPTION = p6, COMMENT = p7, COMPLETED_BY = p8};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };

            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            acthistory = (from employee in tempEmployee
                          join act in data.ACTION_HISTORies on employee.EMP_ID equals act.COMPLETED_BY
                          join cadreq in data.CADREQs on act.ID_CADREQ equals cadreq.ID_CADREQ
                          join action in data.ACTIONs on act.ID_ACTION equals action.ID_ACTION
                          where cadreq.ID_CADREQ.Equals(id_cadreq) && (act.ID_ACTIVITY.Equals(5) || act.ID_ACTIVITY.Equals(6) || act.ID_ACTIVITY.Equals(7) || act.ID_ACTIVITY.Equals(8))
                          select makeActionHistoryDetail(cadreq.ID_CADREQ, cadreq.CADREQ_NO, cadreq.TIME_REQUEST.ToString(), action.NAME_ACTION, act.TIME_END.ToString(), cadreq.DESCRIPTION, act.COMMENT, employee.EMP_NAME)).Take(4).ToList();
            acthistory = acthistory.OrderByDescending(x => x.TIME_END).ThenBy(x => x.NAME_ACTION).ToList();
            acthistory.Reverse();

            return acthistory;
        }

        public static List<ActionHistoryDetail> getCadreqActionHistory(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            Func<string, string, string, string, string, ActionHistoryDetail> makeActionHistoryDetail = (p1, p2, p3, p4, p5) => new ActionHistoryDetail { NAME_ACTION=p1, COMPLETED_BY=p2, ONBEHALF_BY=p3, TIME_END=p4, COMMENT=p5};
            Func<string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4 };
            List<ActionHistoryDetail> acthistory = new List<ActionHistoryDetail>();
            List<EmployeeData> tempEmployee = new List<EmployeeData>();
            /*
            tempEmployee = (from employee in data2.TBL_EMPs
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME)).ToList();
            acthistory = (from history in data.ACTION_HISTORies
                          join action in data.ACTIONs on history.ID_ACTION equals action.ID_ACTION
                          join emp1 in tempEmployee on history.COMPLETED_BY equals emp1.EMP_ID
                          join emp2 in tempEmployee on history.PARTICIPANT equals emp2.EMP_ID
                          where history.ID_CADREQ.Equals(id_cadreq)
                          select makeActionHistoryDetail(action.NAME_ACTION, emp1.EMP_NAME, emp2.EMP_NAME, history.TIME_END.ToString(), history.COMMENT)).ToList();
             * */

            //List<ACTION_HISTORY> acthist = data.ACTION_HISTORies.Where(x => x.ID_CADREQ.Equals(id_cadreq)).OrderBy(x => x.TIME_END).ToList();

            acthistory = (from history in data.ACTION_HISTORies
                          join action in data.ACTIONs on history.ID_ACTION equals action.ID_ACTION
                          where history.ID_CADREQ.Equals(id_cadreq)
                          select makeActionHistoryDetail(action.NAME_ACTION, history.COMPLETED_BY, history.PARTICIPANT, history.TIME_END.ToString(), history.COMMENT)).ToList();

            acthistory = acthistory.OrderByDescending(x => x.TIME_END).ThenBy(x => x.NAME_ACTION).ToList();

            foreach (ActionHistoryDetail act in acthistory)
            {
                act.COMPLETED_BY = act.COMPLETED_BY+"-"+PFNATTHelper.getEMPFullName(act.COMPLETED_BY);
                act.ONBEHALF_BY = act.ONBEHALF_BY + "-" + PFNATTHelper.getEMPFullName(act.ONBEHALF_BY);
            }

            return acthistory;

        }

        public static void approveRequest(int id_cadreq, string emp_id, int role) //update cadreq
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();

            CADREQ cadreq = (from req in data.CADREQs
                            where req.ID_CADREQ.Equals(id_cadreq)
                            select req).FirstOrDefault();

            TBL_EMP sect = (from sect2 in data2.TBL_EMPs
                                    where sect2.EMP_ID.Equals(cadreq.PIC)
                                    select sect2).FirstOrDefault();

            

            switch (role)
            {
                case 1:
                     cadreq.ID_ACTIVITY = 7;
                    break;
                case 2:
                    cadreq.ID_ACTIVITY = 6;
                    break;
                case 3:
                    cadreq.ID_ACTIVITY = 5;
                    if(cadreq.PIC.Equals(getSecondRater(cadreq.JOBCODE, PFNATTHelper.getEMPSect(cadreq.PIC))))
                        cadreq.ID_ACTIVITY = 6;
                    break;
                case 4:
                    cadreq.ID_ACTIVITY = 6;
                    break;
            }
            
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.LASTACTIVITY_BY = emp_id;
            data.SubmitChanges();
        }

        public static void cancelRequest(int id_cadreq, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = (from req in data.CADREQs
                             where req.ID_CADREQ.Equals(id_cadreq)
                             select req).FirstOrDefault();
            cadreq.ID_ACTIVITY = 3;
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.LASTACTIVITY_BY = emp_id;
            data.SubmitChanges();
        }

        public static void rejectRequest(int id_cadreq, string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = (from req in data.CADREQs
                             where req.ID_CADREQ.Equals(id_cadreq)
                             select req).FirstOrDefault();
            cadreq.ID_ACTIVITY = 4;
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.LASTACTIVITY_BY = emp_id;
            data.SubmitChanges();
        }

        public static void insertActionHistory(ACTION_HISTORY act)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.ACTION_HISTORies.InsertOnSubmit(act);
            data.SubmitChanges();
        }

        public static int getLastCADreqNO()
        {
            CADcostDataContext data = new CADcostDataContext();
            try
            {
                return (int)data.CADREQs.OrderByDescending(x => x.ID_CADREQ).Select(x => x.ID_CADREQ).First();
            }
            catch
            {
                return 0;
            }
        }

        public static string generateCADreqNO(string sectcode)
        {
            CADcostDataContext data = new CADcostDataContext();
            return (getLastCADreqNO()+1).ToString() + '/' + sectcode + '/' + DateTime.Now.Month.ToString() + '/' + DateTime.Now.Year.ToString();
        }

        public static decimal? generateTotalItemAmount(int id_cadreq)
        {
            
            List<CADcostItem> items = new List<CADcostItem>();
            items = getItem(id_cadreq, 0);
            /*
            CADcostDataContext data = new CADcostDataContext();
            
            result = getAllSoftwareCost(0);
            foreach(CADcostItem item in items)
            {
                getSoftwareCost(out result, item.ID_SOFTWARE);
                if(result.Count>1)
                {
                    item.PRICE = null;
                }
                else if (result.Count == 1)
                {
                    item.PRICE = result[0].CAD_COST;
                }
                else
                {
                    item.PRICE = null;
                }
            }*/

            decimal total = 0;
            bool isnull = false;
            foreach (CADcostItem item in items)
            {
                if (item.PRICE == null) isnull = true;
                else
                total += (decimal)item.PRICE;
            }
            if (isnull == false)
                return total;
             
            else return null;
        }

        public static decimal? getTotalItemAmount(int id_cadreq, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            decimal totalprice = 0;
            decimal rates  = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.RATES).FirstOrDefault();

            if ((from cadreq in data.CADREQs
                 where cadreq.ID_CADREQ.Equals(id_cadreq)
                 select cadreq.TOTAL_AMOUNT).FirstOrDefault() != null)
            {
                totalprice = (decimal)(from cadreq in data.CADREQs
                                       where cadreq.ID_CADREQ.Equals(id_cadreq)
                                       select cadreq.TOTAL_AMOUNT).FirstOrDefault();
                if (id_currency != 0) totalprice *= rates;
            }
            else return null;
            
            return totalprice;
        }

        public static string getCADreqPIC(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            string emp_id = "";
            emp_id = (from cadreq in data.CADREQs
                          where cadreq.ID_CADREQ.Equals(id_cadreq)
                          select cadreq.PIC).FirstOrDefault();
            return emp_id;
        }

        public static string getCADreqJobcode(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            string jobcode = "";
            jobcode = (from cadreq in data.CADREQs
                      where cadreq.ID_CADREQ.Equals(id_cadreq)
                      select cadreq.JOBCODE).FirstOrDefault();
            return jobcode;
        }

        public static CADREQ getCADdetail(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = new CADREQ();
            cadreq = (from cadreq2 in data.CADREQs
                      where cadreq2.ID_CADREQ.Equals(id_cadreq)
                      select cadreq2).FirstOrDefault();
            return cadreq;
        }

        public static string getActivityName(int id_activity)
        {
            CADcostDataContext data = new CADcostDataContext();
            string activityname = (from activity in data.ACTIVITies
                      where activity.ID_ACTIVITY.Equals(id_activity)
                      select activity.NAME_ACTIVITY).FirstOrDefault();
            return activityname;
        }

        public static void generateDDLSoftwareName(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SOFTWARE> software = (from sfwr in data.SOFTWAREs select sfwr).ToList();
            foreach (SOFTWARE sw in software)
            {
                ddl.Items.Add(new ListItem(sw.NAME_SOFTWARE.ToString(), sw.ID_SOFTWARE.ToString()));
            }
        }

        public static void generateDDLVendorName(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<VENDOR> vendor = (from vndr in data.VENDORs select vndr).ToList();
            ddl.Items.Clear();
            foreach (VENDOR vndr in vendor)
            {
                ddl.Items.Add(new ListItem(vndr.NAME_VENDOR.ToString(), vndr.ID_VENDOR.ToString()));
            }
        }

        public static void generateDDLServer(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SERVER> server = (from srvr in data.SERVERs select srvr).ToList();
            foreach (SERVER srvr in server)
            {
                ddl.Items.Add(new ListItem(srvr.IP_SERVER.ToString(), srvr.ID_SERVER.ToString()));
            }
        }
        /*
        public static void generateDDLAdmin(DropDownList ddl)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            Func<string, string, string, string,string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4, p5) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4, POSITION=p5};
            Func<string, string, string, int, string, ADMIN> makeAdmin = (p1, p2, p3, p4, p5) => new ADMIN { EMP_ID = p1, WindowsCredential=p2, IMPERSONATE=p3, Role=p4, Delegate=p5};
            List <EmployeeData> tblemp = new List<EmployeeData>();
            List <ADMIN> tbladmin = new List<ADMIN>();

            tbladmin = (from admin in data.ADMINs 
                        select makeAdmin(admin.EMP_ID, admin.WindowsCredential, admin.IMPERSONATE, Convert.ToInt32(admin.Role), admin.Delegate)).ToList();

            tblemp = (from admin2 in tbladmin 
                          join employee in data2.TBL_EMPs on admin2.EMP_ID equals employee.EMP_ID
                            join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                            let sect = section.SECT_NAME.Split('-')
                            select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME, positionName(Convert.ToInt32(admin2.Role)))).ToList();


            foreach (EmployeeData emp in tblemp)
            {
                ddl.Items.Add(new ListItem(emp.EMP_ID + " - " + emp.EMP_NAME + " - [" + emp.SECT_NAME + " - " + emp.POSITION + "]", emp.EMP_ID.ToString()));
            }
                      
            
        }
        
        public static void generateDDLApprover(DropDownList ddl, string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            Func<string, string, string, string, string, EmployeeData> makeEmployeeData = (p1, p2, p3, p4, p5) => new EmployeeData { SECT_ID = p1, SECT_NAME = p2, EMP_ID = p3, EMP_NAME = p4, POSITION = p5 };
            Func<string, string, ADMIN> makeAdmin = (p1, p2) => new ADMIN { EMP_ID = p1, WindowsCredential = p2};
            List<EmployeeData> tblemp = new List<EmployeeData>();
            List<ADMIN> tbladmin = new List<ADMIN>();

            tbladmin = (from admin in data.ADMINs
                        select makeAdmin(admin.EMP_ID, admin.WindowsCredential)).ToList();

            tblemp = (from admin2 in tbladmin
                      join employee in data2.TBL_EMPs on admin2.EMP_ID equals employee.EMP_ID
                      join section in data2.TBL_SECTIONs on employee.SECT_ID equals section.SECT_ID
                      let sect = section.SECT_NAME.Split('-')
                      select makeEmployeeData(section.SECT_ID, sect.GetValue(1).ToString().Trim(), employee.EMP_ID, employee.FULL_NAME, "ENIT")).ToList();


            foreach (EmployeeData emp in tblemp)
            {
                ddl.Items.Add(new ListItem(emp.EMP_ID + " - " + emp.EMP_NAME + " - [" + emp.SECT_NAME + " - " + emp.POSITION + "]", emp.EMP_ID.ToString()));
            }

            string keyPerson = (from fwbs in data2.TBL_PROJECT_FWBs
                                where fwbs.JOBCODE_ID.Equals(jobcode_id)
                                select fwbs.KEYPERSON).FirstOrDefault();

            string clientJobcode = (from fwbs in data2.TBL_PROJECT_FWBs
                                    where fwbs.JOBCODE_ID.Equals(jobcode_id)
                                    select fwbs.CLIENT_JOBCODE).FirstOrDefault();

            string firstRater = data2.Funct_WhoMustApproved(keyPerson, jobcode_id, clientJobcode, sect_id, sect_id + "13F1", 1);
        }
        */
        public static string positionName(int role)
        {
            switch (role)
            {
                case 1:
                    return "ENIT";
                    break;
                case 2:
                    return "ENIT MANAGER";
                    break;
                case 3:
                    return "GROUP MANAGER";
                    break;
                case 4:
                    return "DEPARTMENT MANAGER";
                    break;
                default:
                    return "";
            }
            
        }

        public static List<SoftwareVendor> getAllSoftwareVendor()
        {
            CADcostDataContext data = new CADcostDataContext();
            List <SoftwareVendor> softwareVendor = new List<SoftwareVendor>();
            Func<int, string, string, int, string, SoftwareVendor> makeSoftwareVendor = (p1, p2, p3, p4, p5) => new SoftwareVendor {ID_SOFTWARE=p1, NAME_SOFTWARE=p2, DATE_RENEWAL=p3, ID_VENDOR=p4, NAME_VENDOR=p5};
            softwareVendor = (from software in data.SOFTWAREs
                              join sfwrVendor in data.SOFTWARE_VENDORs on software.ID_SOFTWARE equals sfwrVendor.ID_SOFTWARE
                              join vendor in data.VENDORs on sfwrVendor.ID_VENDOR equals vendor.ID_VENDOR
                              select makeSoftwareVendor(software.ID_SOFTWARE, software.NAME_SOFTWARE, software.DATE_RENEWAL.ToString(), vendor.ID_VENDOR, vendor.NAME_VENDOR)).ToList();
            return softwareVendor;
        }

        public static List <SoftwareCost> getAllSoftwareCost(int id_currency)
        {
            List<SoftwareCost> result = new List<SoftwareCost>();
            CADcostDataContext data = new CADcostDataContext();
            Func<int, int, string,string, decimal, string, SoftwareCost> makeSoftwareCost = (p1,p2, p3, p4, p5, p6) => new SoftwareCost { ID_COST = p1, ID_SOFTWARE=p2, NAME_SOFTWARE = p3, TYPE=p4, CAD_COST=p5, CAD_STARTDATE=p6};
            if (id_currency == 0)
                result = (from software in data.SOFTWAREs
                          join cost in data.SOFTWARE_COSTs on software.ID_SOFTWARE equals cost.ID_SOFTWARE
                          group cost by new { cost.ID_SOFTWARE, software.NAME_SOFTWARE, cost.TYPE } into pricegroup
                          select makeSoftwareCost(Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST), pricegroup.Key.ID_SOFTWARE, pricegroup.Key.NAME_SOFTWARE, pricegroup.Key.TYPE, Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST), pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_STARTDATE.ToString())).ToList();
            else
            {
                decimal rates = (from cur in data.CURRENCies
                            where cur.ID_CURRENCY.Equals(id_currency)
                            select cur.RATES).FirstOrDefault();
                result = (from software in data.SOFTWAREs
                          join cost in data.SOFTWARE_COSTs on software.ID_SOFTWARE equals cost.ID_SOFTWARE
                          group cost by new { cost.ID_SOFTWARE, software.NAME_SOFTWARE, cost.TYPE} into pricegroup
                          select makeSoftwareCost(Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST), pricegroup.Key.ID_SOFTWARE, pricegroup.Key.NAME_SOFTWARE, pricegroup.Key.TYPE, Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST) * rates, pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_STARTDATE.ToString())).ToList();
            }
            return result;
        }

        public static List<SoftwareCost> getAllSoftwareCostHistory(int id_software, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SoftwareCost> softwareCost = new List<SoftwareCost>();
            Func<string, string, decimal, string, SoftwareCost> makeSoftwareCost = (p2, p3, p4, p5) => new SoftwareCost {NAME_SOFTWARE = p2, TYPE = p3, CAD_COST = p4, CAD_STARTDATE = p5};
            if (id_currency == 0)
                softwareCost = (from sfwrCost in data.SOFTWARE_COSTs
                                group sfwrCost by new { sfwrCost.ID_SOFTWARE, sfwrCost.TYPE, sfwrCost.CAD_STARTDATE.Year } into swcost
                                join software in data.SOFTWAREs on swcost.Key.ID_SOFTWARE equals software.ID_SOFTWARE
                                where swcost.Key.ID_SOFTWARE.Equals(id_software)
                                select makeSoftwareCost(software.NAME_SOFTWARE, swcost.Key.TYPE, Convert.ToDecimal(swcost.OrderByDescending(a => a.CAD_STARTDATE).First().COST), swcost.Key.Year.ToString())).ToList();
            else
            {
                decimal rates = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.RATES).FirstOrDefault();
                softwareCost = (from sfwrCost in data.SOFTWARE_COSTs
                                group sfwrCost by new { sfwrCost.ID_SOFTWARE, sfwrCost.TYPE, sfwrCost.CAD_STARTDATE.Year } into swcost
                                join software in data.SOFTWAREs on swcost.Key.ID_SOFTWARE equals software.ID_SOFTWARE
                                where swcost.Key.ID_SOFTWARE.Equals(id_software)
                                select makeSoftwareCost(software.NAME_SOFTWARE, swcost.Key.TYPE, Convert.ToDecimal(swcost.OrderByDescending(a => a.CAD_STARTDATE).First().COST)*rates, swcost.Key.Year.ToString())).ToList();
            }
            return softwareCost;
        }

        public static List<SoftwareCost> getAllLastSoftwareCost(int id_software, int id_currency)
        {
            List<SoftwareCost> result = new List<SoftwareCost>();
            CADcostDataContext data = new CADcostDataContext();
            Func<int, int, string, decimal, string, SoftwareCost> makeSoftwareCost = (p1, p2, p4, p5, p6) => new SoftwareCost { ID_COST = p1, ID_SOFTWARE = p2, TYPE = p4, CAD_COST = p5, CAD_STARTDATE = p6 };
            if (id_currency == 0)
                result = (from software in data.SOFTWAREs
                          join cost in data.SOFTWARE_COSTs on software.ID_SOFTWARE equals cost.ID_SOFTWARE
                          group cost by new {software.ID_SOFTWARE, cost.TYPE } into pricegroup
                          where pricegroup.Key.ID_SOFTWARE.Equals(id_software)
                          select makeSoftwareCost(Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST), pricegroup.Key.ID_SOFTWARE, pricegroup.Key.TYPE, Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST), pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_STARTDATE.ToString())).ToList();
            else
            {
                decimal rates = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.RATES).FirstOrDefault();
                result = (from software in data.SOFTWAREs
                          join cost in data.SOFTWARE_COSTs on software.ID_SOFTWARE equals cost.ID_SOFTWARE
                          group cost by new { cost.ID_SOFTWARE, cost.TYPE } into pricegroup
                          where pricegroup.Key.ID_SOFTWARE.Equals(id_software)
                          select makeSoftwareCost(Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST), pricegroup.Key.ID_SOFTWARE, pricegroup.Key.TYPE, Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST) * rates, pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_STARTDATE.ToString())).ToList();
            }
            return result;
        }

        public static List<PROJECT> getAllProject()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<PROJECT> projects = (from projects2 in data.PROJECTs
                                      select projects2).ToList();
            return projects;
        }

        public static List<REQUESTER> getAllRequester()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<REQUESTER> req = (from req2 in data.REQUESTERs
                                      select req2).ToList();
            return req;
        }

        public static List<ProjectRater> getAllProjectRater(string jobcode_id)
        {
            Func<int, string,string, string, string, string,string, ProjectRater> makeProjectRater = (p1, p2, p3, p4, p5, p6, p7) => new ProjectRater { ID_RATER = p1, SECT_ID=p2, DEPARTMENT = p3, FirstRaterID=p4, FirstRater = p5, SecondRaterID=p6, SecondRater = p7 };
            List<TBL_EMP> emp = new List<TBL_EMP>();
            List<TBL_SECTION> sect = new List<TBL_SECTION>();
            List<ProjectRater> rater2 = new List<ProjectRater>();
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            /*
            sect = (from sect2 in data2.TBL_SECTIONs select sect2).ToList();
            emp = (from emp2 in data2.TBL_EMPs select emp2).ToList();
            rater2 = (from rater in data.RATERs
                      join sect3 in sect on rater.SECT_ID equals sect3.SECT_ID
                      join emp3 in emp on rater.FirstRater equals emp3.EMP_ID
                      join emp4 in emp on rater.SecondRater equals emp4.EMP_ID
                     select makeProjectRater(rater.ID_RATER, rater.SECT_ID, rater.SECT_ID + "-" + sect3.SECT_NAME,rater.FirstRater, rater.FirstRater + "-" + emp3.FULL_NAME, rater.SecondRater, rater.SecondRater + "-" + emp4.FULL_NAME)).ToList();
             * */
            foreach(RATER rater in data.RATERs.Where(x => x.JOBCODE_ID.Equals(jobcode_id)))
            {
                rater2.Add(makeProjectRater(rater.ID_RATER, rater.SECT_ID, PFNATTHelper.getSectName(rater.SECT_ID).Split('-').GetValue(1).ToString().Trim(), rater.FirstRater, rater.FirstRater + "-" + PFNATTHelper.getEMPFullName(rater.FirstRater), rater.SecondRater, rater.SecondRater + "-" + PFNATTHelper.getEMPFullName(rater.SecondRater)));
            }
            return rater2;
        }

        public static List<Delegation> getAllDelegation()
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<string, string, string, string, Delegation> makeDelegate = (p1, p2, p3, p4) => new Delegation{ Emp_delegatingID=p1, Emp_delegating = p2, Emp_delegatedID = p3, Emp_delegated = p4};
            List<Delegation> deleg = (from del in data.DELEGATEs
                                      select makeDelegate(del.EMP_Delegating, del.EMP_Delegating, del.EMP_Delegated, del.EMP_Delegated)).ToList();
            foreach (Delegation deleg1 in deleg)
            {
                deleg1.Emp_delegating = deleg1.Emp_delegating+"-"+PFNATTHelper.getEMPFullName(deleg1.Emp_delegating);
                deleg1.Emp_delegated = deleg1.Emp_delegated+"-"+PFNATTHelper.getEMPFullName(deleg1.Emp_delegated);
            }

            return deleg;
        }

        public static List<Delegation> getAllDelegationEmpID(string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<string, string, string, string, Delegation> makeDelegate = (p1, p2, p3, p4) => new Delegation { Emp_delegatingID = p1, Emp_delegating = p2, Emp_delegatedID = p3, Emp_delegated = p4 };
            List<Delegation> deleg = (from del in data.DELEGATEs where del.EMP_Delegated.Equals(emp_id)
                                      select makeDelegate(del.EMP_Delegating, del.EMP_Delegating, del.EMP_Delegated, del.EMP_Delegated)).ToList();
            foreach (Delegation deleg1 in deleg)
            {
                deleg1.Emp_delegating = deleg1.Emp_delegating + "-" + PFNATTHelper.getEMPFullName(deleg1.Emp_delegating);
                deleg1.Emp_delegated = deleg1.Emp_delegated + "-" + PFNATTHelper.getEMPFullName(deleg1.Emp_delegated);
            }

            return deleg;
        }

        public static string getFirstRater(string jobcode, string sect_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            string firstrater = (from rater in data.RATERs
                                 where rater.JOBCODE_ID.Equals(jobcode) && rater.SECT_ID.Equals(sect_id)
                                 select rater.FirstRater).FirstOrDefault();
            return firstrater;
        }
        
        public static string getSecondRater(string jobcode, string sect_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            string secondrater = (from rater in data.RATERs
                                 where rater.JOBCODE_ID.Equals(jobcode) && rater.SECT_ID.Equals(sect_id)
                                 select rater.SecondRater).FirstOrDefault();
            return secondrater;
        }

        public static PROJECT getProjectDetail(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PROJECT project = new PROJECT();
            project = data.PROJECTs.Where(x => x.JOBCODE_ID.Equals(jobcode_id)).FirstOrDefault();
            return project;
        }

        public static void updateCurrentSoftwareServer()
        {
            int currentmonth = DateTime.Now.Month;
            int currentyear = DateTime.Now.Year;
            CADcostDataContext data = new CADcostDataContext();
            List<SOFTWARE_SERVER> sfwrsvr = new List<SOFTWARE_SERVER>();
            List<SOFTWARE_SERVER_FOR_MONTH_YEAR> sfwrsvrmonthyear = new List<SOFTWARE_SERVER_FOR_MONTH_YEAR>();
            sfwrsvr = (from sfwrsvr2 in data.SOFTWARE_SERVERs where sfwrsvr2.LICENSE_STATUS == false select sfwrsvr2).ToList();
            sfwrsvrmonthyear = (from sfwrsvrmonthyear2 in data.SOFTWARE_SERVER_FOR_MONTH_YEARs
                                where sfwrsvrmonthyear2.FOR_MONTH.Equals(currentmonth) && sfwrsvrmonthyear2.FOR_YEAR.Equals(currentyear)
                                select sfwrsvrmonthyear2).ToList();
            List<SOFTWARE_SERVER_FOR_MONTH_YEAR> sfwrsvrmonthyear3 = new List<SOFTWARE_SERVER_FOR_MONTH_YEAR>();
            foreach (SOFTWARE_SERVER ss in sfwrsvr)
            {
                sfwrsvrmonthyear3 = sfwrsvrmonthyear.Where(x => x.ID_SOFTWARE_SERVER.Equals(ss.ID_SOFTWARE_SERVER) && x.FOR_MONTH.Equals(currentmonth) && x.FOR_YEAR.Equals(currentyear)).ToList();
                if (ss.LICENSE_STATUS == false && sfwrsvrmonthyear3.Count() == 0)
                    ss.QUANTITY = 0;
                else
                {
                    foreach (SOFTWARE_SERVER_FOR_MONTH_YEAR ssmy in sfwrsvrmonthyear3)
                    ss.QUANTITY += ssmy.QUANTITY;
                }
                ss.LAST_UPDATED = DateTime.Now;
            }
            data.SubmitChanges();
        }

        public static List <SoftwareServer> getAllSoftwareServerProject(string jobcode_id, int month, int year)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SOFTWARE_SERVER_FOR_MONTH_YEAR> sfwrsvrmonthyear = new List<SOFTWARE_SERVER_FOR_MONTH_YEAR>();
            List<SoftwareServer> softwareServer = new List<SoftwareServer>();
            Func<int, int, string, string, string, int, int,int, string, string, SoftwareServer> makeSoftwareServer = (p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) => new SoftwareServer { ID_SOFTWARE_SERVER=p1, ID_SOFTWARE=p2, NAME_SOFTWARE=p3, LICENSE_STATUS=p4, DEPT=p5, QUANTITY=p6, OWNED=p7, ID_SERVER=p8, IP_SERVER=p9, REMARK=p10};
            softwareServer = (from software in data.SOFTWAREs
                            join sfwrServer in data.SOFTWARE_SERVERs on software.ID_SOFTWARE equals sfwrServer.ID_SOFTWARE
                            let dept = sfwrServer.DEPT.Equals("ALL") ? new string [] {"", "ALL"} : PFNATTHelper.getSectName(sfwrServer.DEPT).Split('-')
                            join server in data.SERVERs on sfwrServer.ID_SERVER equals server.ID_SERVER
                            where sfwrServer.JOBCODE_ID.Equals(jobcode_id)
                            orderby sfwrServer.ID_SOFTWARE
                              select makeSoftwareServer(Convert.ToInt32(sfwrServer.ID_SOFTWARE_SERVER), Convert.ToInt32(sfwrServer.ID_SOFTWARE), software.NAME_SOFTWARE, (Convert.ToBoolean(sfwrServer.LICENSE_STATUS) == true ? "Owned" : "Additional"), dept.GetValue(1).ToString().Trim(), 0, sfwrServer.QUANTITY, server.ID_SERVER, server.IP_SERVER, sfwrServer.REMARK)).ToList();
            sfwrsvrmonthyear = (from sfwrsvrmonthyear2 in data.SOFTWARE_SERVER_FOR_MONTH_YEARs
                                where sfwrsvrmonthyear2.FOR_MONTH.Equals(month) && sfwrsvrmonthyear2.FOR_YEAR.Equals(year)
                                select sfwrsvrmonthyear2).ToList();
            List<SOFTWARE_SERVER_FOR_MONTH_YEAR> sfwrsvrmonthyear3 = new List<SOFTWARE_SERVER_FOR_MONTH_YEAR>();
            foreach (SoftwareServer ss in softwareServer)
            {
                sfwrsvrmonthyear3 = sfwrsvrmonthyear.Where(x => x.ID_SOFTWARE_SERVER.Equals(ss.ID_SOFTWARE_SERVER) && x.FOR_MONTH.Equals(month) && x.FOR_YEAR.Equals(year)).ToList();
                if (sfwrsvrmonthyear3.Count() == 0)
                    ss.QUANTITY = 0;
                else 
                {
                    foreach (SOFTWARE_SERVER_FOR_MONTH_YEAR ssmy in sfwrsvrmonthyear3)
                        ss.QUANTITY += ssmy.QUANTITY;
                }
            }

            return softwareServer;
        }

        public static List <SoftwareServer> getAllSoftwareServer(int month, int year)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SOFTWARE_SERVER_FOR_MONTH_YEAR> sfwrsvrmonthyear = new List<SOFTWARE_SERVER_FOR_MONTH_YEAR>();
            List<SoftwareServer> softwareServer = new List<SoftwareServer>();
            Func<int, int, string, string, string, int, int, string, string, SoftwareServer> makeSoftwareServer = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new SoftwareServer { ID_SOFTWARE_SERVER = p1, ID_SOFTWARE = p2, NAME_SOFTWARE = p3, LICENSE_STATUS = p4, DEPT = p5, QUANTITY = p6, ID_SERVER = p7, IP_SERVER = p8, PROJECT_NAME=p9};
            softwareServer = (from software in data.SOFTWAREs
                              join sfwrServer in data.SOFTWARE_SERVERs on software.ID_SOFTWARE equals sfwrServer.ID_SOFTWARE
                              let dept = sfwrServer.DEPT.Equals("ALL") ? new string[] { "", "ALL" } : PFNATTHelper.getSectName(sfwrServer.DEPT).Split('-')
                              join server in data.SERVERs on sfwrServer.ID_SERVER equals server.ID_SERVER
                              select makeSoftwareServer(Convert.ToInt32(sfwrServer.ID_SOFTWARE_SERVER), Convert.ToInt32(sfwrServer.ID_SOFTWARE), software.NAME_SOFTWARE, (Convert.ToBoolean(sfwrServer.LICENSE_STATUS) == true ? "Owned" : "Additional"), dept.GetValue(1).ToString().Trim(), sfwrServer.QUANTITY, server.ID_SERVER, server.IP_SERVER, PFNATTHelper.getProjectName(sfwrServer.JOBCODE_ID))).ToList();
            sfwrsvrmonthyear = (from sfwrsvrmonthyear2 in data.SOFTWARE_SERVER_FOR_MONTH_YEARs
                                where sfwrsvrmonthyear2.FOR_MONTH.Equals(month) && sfwrsvrmonthyear2.FOR_YEAR.Equals(year)
                                select sfwrsvrmonthyear2).ToList();
            foreach (SoftwareServer ss in softwareServer)
            {
                if (ss.LICENSE_STATUS.Equals("Additional") && sfwrsvrmonthyear.Where(x => x.ID_SOFTWARE_SERVER.Equals(ss.ID_SOFTWARE_SERVER) && x.FOR_MONTH.Equals(month) && x.FOR_YEAR.Equals(year)).FirstOrDefault() == null)
                    ss.QUANTITY = 0;
                else if (sfwrsvrmonthyear.Where(x => x.ID_SOFTWARE_SERVER.Equals(ss.ID_SOFTWARE_SERVER) && x.FOR_MONTH.Equals(month) && x.FOR_YEAR.Equals(year)).FirstOrDefault() != null)
                {
                    ss.QUANTITY = sfwrsvrmonthyear.Where(x => x.ID_SOFTWARE_SERVER.Equals(ss.ID_SOFTWARE_SERVER) && x.FOR_MONTH.Equals(month) && x.FOR_YEAR.Equals(year)).Select(x => x.QUANTITY).FirstOrDefault();
                }
            }

            return softwareServer;
        }

        public static List<VENDOR> getAllVendor()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<VENDOR> vendor = new List<VENDOR>();
            vendor = (from vndr in data.VENDORs select vndr).ToList();
            return vendor;
        }

        public static List<SERVER> getAllServer()
        {
            CADcostDataContext data = new CADcostDataContext();
            List<SERVER> server = new List<SERVER>();
            server = (from srvr in data.SERVERs select srvr).ToList();
            return server;
        }

        public static void SubmitCADreq(string dept, CADREQ req, string jobcode_id, List<CADcostItem> items)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.CADREQs.InsertOnSubmit(req);
            data.SubmitChanges();
            InsertCADItems(req.ID_CADREQ, items);
        }

        public static int UpdateCADreq(int id_cadreq, string emp_id, string jobcode_id, string description, List<CADcostItem> items)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = new CADREQ();
            cadreq = (from cadreq2 in data.CADREQs
                      where cadreq2.ID_CADREQ.Equals(id_cadreq)
                      select cadreq2).FirstOrDefault();
            int prevActivity = cadreq.ID_ACTIVITY;
            cadreq.JOBCODE = jobcode_id;
            cadreq.LASTACTIVITY_BY = emp_id;
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.DESCRIPTION = description;
            cadreq.ID_ACTIVITY = 2;
            if (cadreq.PIC.Equals(CADcostHelper.getFirstRater(cadreq.JOBCODE, PFNATTHelper.getEMPSect(cadreq.PIC))))
                cadreq.ID_ACTIVITY = 5;
            data.SubmitChanges();
            UpdateCADItems(id_cadreq, items);
            return prevActivity;
        }

        public static void UpdateCADreqDraft(int id_cadreq, string emp_id, string jobcode_id, string description, List<CADcostItem> items)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = new CADREQ();
            cadreq = (from cadreq2 in data.CADREQs
                      where cadreq2.ID_CADREQ.Equals(id_cadreq)
                      select cadreq2).FirstOrDefault();
            cadreq.JOBCODE = jobcode_id;
            cadreq.LASTACTIVITY_BY = emp_id;
            cadreq.TIME_LASTACTIVITY = DateTime.Now;
            cadreq.DESCRIPTION = description;
            cadreq.ID_ACTIVITY = 1;
            data.SubmitChanges();
            UpdateCADItems(id_cadreq, items);
        }

        public static List<CADcostItem> getNotNullItem(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<int, string, int, decimal?, string, bool, int, int?, int?, CADcostItem> makeCADcostItem = (p1, p2, p3, p4, p5, p6, p7, p8, p9) => new CADcostItem { ID_SOFTWARE = p1, NAME_SOFTWARE = p2, QUANTITY = p3, PRICE = p4, REMARK = p5, TYPE = p6, ID_CADDETAIL = p7, ID_COST = p8, ID_SOFTWARE_SERVER = p9 };
            List<CADcostItem> result2 = new List<CADcostItem>();
            result2 = (from item in data.CADDETAILs
                       join software in data.SOFTWAREs on item.ID_SOFTWARE equals software.ID_SOFTWARE
                       where item.ID_CADREQ.Equals(id_cadreq) && (item.ID_COST!=null || item.ID_SOFTWARE_SERVER!=null)
                       select makeCADcostItem(item.ID_SOFTWARE, software.NAME_SOFTWARE, item.QUANTITY, null, item.REMARK, software.TYPE, item.ID_CADDETAIL, item.ID_COST, item.ID_SOFTWARE_SERVER)).ToList();
            return result2;
        }

        public static void InsertCADItems(int id_cadreq, List<CADcostItem> items)
        {
            //Func<int, int, bool, int, DateTime, string, 
            CADcostDataContext data = new CADcostDataContext();
            CADDETAIL cd = new CADDETAIL();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            if (items != null)
            {
                foreach (CADcostItem cci in items)
                {
                    cd = new CADDETAIL();
                    cd.ID_CADREQ = id_cadreq;
                    cd.ID_SOFTWARE = cci.ID_SOFTWARE;
                    cd.QUANTITY = cci.QUANTITY;
                    cd.REMARK = cci.REMARK;

                    List<SoftwareCost> cost = new List<SoftwareCost>();
                    getSoftwareCost(out cost, cci.ID_SOFTWARE, 0);
                    if (cost.Count == 1)
                        cd.ID_COST = cost[0].ID_COST;

                    List<SoftwareServer> sserver = new List<SoftwareServer>();
                    getSoftwareServer(out sserver, id_cadreq, cci.ID_SOFTWARE);
                    if (sserver.Count == 1)
                    {
                        cd.ID_SOFTWARE_SERVER = sserver[0].ID_SOFTWARE_SERVER;
                    }
                    
                    data.CADDETAILs.InsertOnSubmit(cd);
                    data.SubmitChanges();

                }
            }
            if (cadreq.PIC.Equals(CADcostHelper.getFirstRater(cadreq.JOBCODE, PFNATTHelper.getEMPSect(cadreq.PIC))))
            {
                
            }

        }

        public static void InsertCADItem(int id_cadreq, CADcostItem item) 
        {
            CADcostDataContext data = new CADcostDataContext();
            CADDETAIL cd = new CADDETAIL();
            cd = new CADDETAIL();
            cd.ID_CADREQ = id_cadreq;
            cd.ID_SOFTWARE = item.ID_SOFTWARE;
            cd.QUANTITY = item.QUANTITY;
            cd.REMARK = item.REMARK;
            data.CADDETAILs.InsertOnSubmit(cd);
            data.SubmitChanges();
        }

        public static void UpdateProjectRater(string jobcode_id, List<ProjectRater> rater)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<RATER> deletedItems = new List<RATER>();

            deletedItems = (from item in data.RATERs
                            where item.JOBCODE_ID.Equals(jobcode_id)
                            select item).ToList();
            data.RATERs.DeleteAllOnSubmit(deletedItems);
            data.SubmitChanges();

            RATER rt = new RATER();
            foreach (ProjectRater prt in rater)
            {
                rt.JOBCODE_ID = jobcode_id;
                rt.SECT_ID = prt.SECT_ID;
                rt.FirstRater = prt.FirstRaterID;
                rt.SecondRater = prt.SecondRaterID;
                data.RATERs.InsertOnSubmit(rt);
                data.SubmitChanges();
            }
        }

        public static void UpdateCADItems(int id_cadreq, List<CADcostItem> items)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<CADDETAIL> deletedItems = new List<CADDETAIL>();

            deletedItems = (from item in data.CADDETAILs
                            where item.ID_CADREQ.Equals(id_cadreq)
                            select item).ToList();
            data.CADDETAILs.DeleteAllOnSubmit(deletedItems);
            data.SubmitChanges();

            CADDETAIL cd = new CADDETAIL();
            foreach (CADcostItem cci in items)
            {
                cd = new CADDETAIL();
                cd.ID_CADREQ = id_cadreq;
                cd.ID_SOFTWARE = cci.ID_SOFTWARE;
                cd.QUANTITY = cci.QUANTITY;
                cd.REMARK = cci.REMARK;
                List<SoftwareCost> cost = new List<SoftwareCost>();
                getSoftwareCost(out cost, cci.ID_SOFTWARE, 0);
                if (cost.Count == 1)
                    cd.ID_COST = cost[0].ID_COST;

                List<SoftwareServer> sserver = new List<SoftwareServer>();
                getSoftwareServer(out sserver, id_cadreq, cci.ID_SOFTWARE);
                if (sserver.Count == 1)
                {
                    cd.ID_SOFTWARE_SERVER = sserver[0].ID_SOFTWARE_SERVER;
                }
                data.CADDETAILs.InsertOnSubmit(cd);
                data.SubmitChanges();
            }
        }

        public static void UpdateTotalItem(int id_cadreq)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = data.CADREQs.Where(x => x.ID_CADREQ.Equals(id_cadreq)).FirstOrDefault();
            cadreq.TOTAL_AMOUNT = generateTotalItemAmount(id_cadreq);
            data.SubmitChanges();
        }

        public static List<CADcostItem> UpdateSoftwareServer(out List<CADcostItem> cost, out bool costnull, out List<CADcostItem> sserver, out bool sservernull, int id_cadreq) //update yg double dan yang tidak ada
        {
            cost = new List<CADcostItem>();
            sserver = new List<CADcostItem>();
            CADcostDataContext data = new CADcostDataContext();
            List<CADcostItem> items = getItem(id_cadreq, 0);
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            string dept = "";
            string jobcode_id = "";
            List<CADcostItem> needAdded = new List<CADcostItem>();
            List<CADcostItem> needUpdate = new List<CADcostItem>();
            costnull = false;
            sservernull = false;
            foreach (CADcostItem cci in items)
            {
                dept = PFNATTHelper.getEMPSect(CADcostHelper.getCADreqPIC(id_cadreq));
                jobcode_id = CADcostHelper.getCADreqJobcode(id_cadreq);
                List<SoftwareCost> sc = new List<SoftwareCost>();
                sc = getAllLastSoftwareCost(cci.ID_SOFTWARE, 0);

                bool duplicate=false;

                if (sc.Count == 0) //jika tidak ada cost, act: needadded, price = 0
                {
                    cci.PRICE = 0;
                    needAdded.Add(cci);
                    costnull = true;
                }
                else if (sc.Count > 1) //jika cost lebih dari 1, act: cost, duplicate
                {
                    cost.Add(cci);
                    duplicate = true;
                }
                else
                {
                    cci.ID_COST = sc[0].ID_COST;
                    needUpdate.Add(cci);
                }

                List<SOFTWARE_SERVER> ss2 = new List<SOFTWARE_SERVER>();
                ss2 = (from softsrv in data.SOFTWARE_SERVERs
                        where softsrv.ID_SOFTWARE.Equals(cci.ID_SOFTWARE) && softsrv.JOBCODE_ID.Equals(jobcode_id) && (softsrv.DEPT.Equals(dept) || softsrv.DEPT.Equals("ALL"))
                        select softsrv).ToList();
                if(ss2.Count>1) //kalau di software server for month year nya tidak ada, tapi ada di software server dan duplicate, act: duplicate sserver
                {
                    sserver.Add(cci);
                        
                }
                else if (ss2.Count == 0) // kalau tidak ada di daftar software server, act: needadded
                {
                    sservernull = true;
                    if (duplicate == false)
                        needAdded.Add(cci);
                }
                else
                {
                    cci.ID_SOFTWARE_SERVER = ss2[0].ID_SOFTWARE_SERVER;
                    needUpdate.Add(cci);
                }
            }
            List<CADcostItem> items2 = getNotNullItem(id_cadreq);
            if (items2 != null)
                updateCADcostItem(id_cadreq, items2);
            needUpdate = needUpdate.Distinct(new ItemComparer()).ToList();
            updateCADcostItem(id_cadreq, needUpdate);
            needAdded = needAdded.Distinct(new ItemComparer()).ToList();
            return needAdded;
        }

        public static void updateCADcostItem(int id_cadreq, List<CADcostItem> itemupdate)
        {
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            /*
            List<CADDETAIL> items = new List<CADDETAIL>();
            items = (from detail in data.CADDETAILs join item in itemupdate on detail.ID_CADDETAIL equals item.ID_CADDETAIL select detail).ToList();
            foreach (CADDETAIL cd in items)
            {
                cd.ID_COST = itemupdate.Where(x => x.ID_CADDETAIL.Equals(cd.ID_CADDETAIL)).FirstOrDefault().ID_COST;
                cd.ID_SOFTWARE_SERVER = itemupdate.Where(x => x.ID_CADDETAIL.Equals(cd.ID_CADDETAIL)).FirstOrDefault().ID_SOFTWARE_SERVER;
            }*/
            CADDETAIL cd = new CADDETAIL();
            foreach (CADcostItem item in itemupdate)
            {
                cd = data.CADDETAILs.Where(x => x.ID_CADDETAIL.Equals(item.ID_CADDETAIL)).FirstOrDefault();
                if(item.ID_COST!=null)
                cd.ID_COST = item.ID_COST;
                if (item.ID_SOFTWARE_SERVER != null)
                {
                    cd.ID_SOFTWARE_SERVER = item.ID_SOFTWARE_SERVER;
                    SOFTWARE_SERVER_FOR_MONTH_YEAR ssmy = data.SOFTWARE_SERVER_FOR_MONTH_YEARs.Where(x => x.ID_SOFTWARE_SERVER.Equals(item.ID_SOFTWARE_SERVER) && x.ID_CADREQ.Equals(id_cadreq)).FirstOrDefault();
                    if (ssmy != null)
                    {
                        ssmy.QUANTITY = item.QUANTITY;
                    }
                    else
                    {
                        SOFTWARE_SERVER_FOR_MONTH_YEAR ssmy2 = new SOFTWARE_SERVER_FOR_MONTH_YEAR();
                        ssmy2.ID_SOFTWARE_SERVER = (int)item.ID_SOFTWARE_SERVER;
                        ssmy2.ID_CADREQ = id_cadreq;
                        ssmy2.FOR_MONTH = cadreq.FOR_MONTH;
                        ssmy2.FOR_YEAR = cadreq.FOR_YEAR;
                        ssmy2.QUANTITY = item.QUANTITY;
                        data.SOFTWARE_SERVER_FOR_MONTH_YEARs.InsertOnSubmit(ssmy2);
                    }
                }
                data.SubmitChanges();
            }
            
        }

        public static Dictionary<string, string> getSoftwareCost(out List<SoftwareCost> cost, int id_software, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            Func<int, string, decimal, decimal, string, SoftwareCost> makeSoftwareCost = (p1, p2, p3, p4, p5) => new SoftwareCost { ID_COST = p1, TYPE = p2, CAD_COST = p3, COST_TO_COST = p4, CAD_STARTDATE = p5 };
            cost = new List<SoftwareCost>();
            Dictionary<string, string> sc = new Dictionary<string, string>();
            decimal rates=1;
            if (id_currency != 0)
            {
                rates = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.RATES).FirstOrDefault();
            }

            cost = (from softcost in data.SOFTWARE_COSTs
                  where softcost.ID_SOFTWARE.Equals(id_software)
                  group softcost by softcost.TYPE into pricegroup
                  select makeSoftwareCost(Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST), (pricegroup.Key == "" ? "Default": pricegroup.Key),Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST), Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST_TO_COST), pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().CAD_STARTDATE.ToString())).ToList();
            foreach (SoftwareCost sc3 in cost)
            {
                if (id_currency == 0)
                sc.Add(sc3.ID_COST.ToString(), sc3.TYPE+"-"+String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}",sc3.CAD_COST));
                else
                sc.Add(sc3.ID_COST.ToString(), sc3.TYPE + "-" + String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", sc3.CAD_COST*rates));
            }
            //new ListItem((pricegroup.Key == "" ? "Default" : pricegroup.Key) + "-" + Convert.ToDecimal(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().COST).ToString(), Convert.ToInt32(pricegroup.OrderByDescending(x => x.CAD_STARTDATE).First().ID_COST).ToString())

            return sc;
        }

        public static Dictionary<string, string> getSoftwareServer(out List<SoftwareServer> sserver, int id_cadreq, int id_software)
        {
            Func<int, string, string, int, string, SoftwareServer> makeSoftwareServer = (p1, p2, p3, p4, p5) => new SoftwareServer { ID_SOFTWARE_SERVER = p1, LICENSE_STATUS = p2, DEPT=p3, OWNED = p4, IP_SERVER = p5 };
            Func<string, string, ListItem> makeListItem = (p1, p2) => new ListItem { Text = p1, Value = p2 };
            CADcostDataContext data = new CADcostDataContext();
            CADREQ cadreq = CADcostHelper.getCADdetail(id_cadreq);
            sserver = new List<SoftwareServer>();
            Dictionary<string, string> li = new Dictionary<string, string>();
            sserver = (from softsrv in data.SOFTWARE_SERVERs
                   let dept = softsrv.DEPT.Equals("ALL") ? new string [] {"", "ALL"} : PFNATTHelper.getSectName(softsrv.DEPT).Split('-')
                   join server in data.SERVERs on softsrv.ID_SERVER equals server.ID_SERVER
                   where softsrv.ID_SOFTWARE.Equals(id_software) && softsrv.JOBCODE_ID.Equals(cadreq.JOBCODE) && (softsrv.DEPT.Equals(PFNATTHelper.getEMPSect(cadreq.PIC)) || softsrv.DEPT.Equals("ALL"))
                   select makeSoftwareServer(softsrv.ID_SOFTWARE_SERVER, (Convert.ToBoolean(softsrv.LICENSE_STATUS) == true ? "Owned" : "Additional"), dept.GetValue(1).ToString().Trim(), softsrv.QUANTITY, server.IP_SERVER)).ToList();
            foreach (SoftwareServer ss in sserver)
            {
                li.Add(ss.ID_SOFTWARE_SERVER.ToString(), ss.DEPT + "-" + ss.LICENSE_STATUS + "-" + ss.IP_SERVER);
            }
            //dept.GetValue(1).ToString().Trim() + (Convert.ToBoolean(softsrv.LICENSE_STATUS) == true ? "Owned" : "Additional") + server.IP_SERVER, softsrv.ID_SOFTWARE_SERVER.ToString()
            return li;
        }

        public static int getLastIDSoftware()
        {
            CADcostDataContext data = new CADcostDataContext();
            return (int) data.SOFTWAREs.OrderByDescending(x => x.ID_SOFTWARE).Select(x => x.ID_SOFTWARE).FirstOrDefault();
        }

        public static void insertSoftware(SOFTWARE software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SOFTWAREs.InsertOnSubmit(software);
            data.SubmitChanges();
            SOFTWARE_VENDOR sfwrVndr = new SOFTWARE_VENDOR();
            sfwrVndr.ID_SOFTWARE = getLastIDSoftware();
            sfwrVndr.ID_VENDOR = id_vendor;
            data.SOFTWARE_VENDORs.InsertOnSubmit(sfwrVndr);
            data.SubmitChanges();
        }

        public static void updateSoftware(SOFTWARE software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE sfwr = new SOFTWARE();
            sfwr = (from sfwr2 in data.SOFTWAREs
                   where sfwr2.ID_SOFTWARE.Equals(software.ID_SOFTWARE)
                   select sfwr2).FirstOrDefault();
            sfwr.NAME_SOFTWARE = software.NAME_SOFTWARE;
            sfwr.DATE_RENEWAL = software.DATE_RENEWAL;
            data.SubmitChanges();
            SOFTWARE_VENDOR sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor = (from sfwrVendor2 in data.SOFTWARE_VENDORs
                          where sfwrVendor2.ID_SOFTWARE.Equals(software.ID_SOFTWARE)
                          select sfwrVendor2).FirstOrDefault();
            data.SOFTWARE_VENDORs.DeleteOnSubmit(sfwrVendor);
            data.SubmitChanges();
            sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor.ID_SOFTWARE = software.ID_SOFTWARE;
            sfwrVendor.ID_VENDOR = id_vendor;
            data.SOFTWARE_VENDORs.InsertOnSubmit(sfwrVendor);
            data.SubmitChanges();
        }

        public static void deleteSoftware(int id_software, int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_VENDOR sfwrVendor = new SOFTWARE_VENDOR();
            sfwrVendor = (from softwareVendor in data.SOFTWARE_VENDORs
                          where softwareVendor.ID_SOFTWARE.Equals(id_software)
                          select softwareVendor).FirstOrDefault();
            data.SOFTWARE_VENDORs.DeleteOnSubmit(sfwrVendor);
            data.SubmitChanges();
            SOFTWARE sfwr = new SOFTWARE();
            sfwr = (from software in data.SOFTWAREs
                    where software.ID_SOFTWARE.Equals(id_software)
                    select software).FirstOrDefault();
            data.SOFTWAREs.DeleteOnSubmit(sfwr);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE" + ", RESEED)");
        }

        public static void insertVendor(VENDOR vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.VENDORs.InsertOnSubmit(vendor);
            data.SubmitChanges();
        }

        public static void updateVendor(VENDOR vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            VENDOR vndr = new VENDOR();
            vndr = (from vndr2 in data.VENDORs
                    where vndr2.ID_VENDOR.Equals(vendor.ID_VENDOR)
                    select vndr2).FirstOrDefault();
            vndr.NAME_VENDOR = vendor.NAME_VENDOR;
            vndr.CONTACT = vendor.CONTACT;
            vndr.EMAIL = vendor.EMAIL;
            vndr.WEBSITE = vendor.WEBSITE;
            vndr.VENDOR_DESCRIPTION = vendor.VENDOR_DESCRIPTION;
            data.SubmitChanges();
        }

        public static void deleteVendor(int id_vendor)
        {
            CADcostDataContext data = new CADcostDataContext();
            VENDOR vendor = new VENDOR();
            vendor = (from vendor2 in data.VENDORs
                          where vendor2.ID_VENDOR.Equals(id_vendor)
                          select vendor2).FirstOrDefault();
            data.VENDORs.DeleteOnSubmit(vendor);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "VENDOR" + ", RESEED)");
        }

        public static void insertServer(SERVER server)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SERVERs.InsertOnSubmit(server);
            data.SubmitChanges();
        }

        public static void updateServer(SERVER server)
        {
            CADcostDataContext data = new CADcostDataContext();
            SERVER srvr = new SERVER();
            srvr = (from srvr2 in data.SERVERs
                    where srvr2.ID_SERVER.Equals(server.ID_SERVER)
                    select srvr2).FirstOrDefault();
            srvr.SERVER_NAME = server.SERVER_NAME;
            srvr.IP_SERVER = server.IP_SERVER;
            data.SubmitChanges();
        }

        public static void deleteServer(int id_server)
        {
            CADcostDataContext data = new CADcostDataContext();
            SERVER server = new SERVER();
            server = (from server2 in data.SERVERs
                      where server2.ID_SERVER.Equals(id_server)
                      select server2).FirstOrDefault();
            data.SERVERs.DeleteOnSubmit(server);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT ("+"SERVER"+", RESEED)");
        }

        public static void insertRater(RATER rater)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.RATERs.InsertOnSubmit(rater);
            data.SubmitChanges();
        }

        public static void updateRater(RATER rater)
        {
            CADcostDataContext data = new CADcostDataContext();
            RATER rate = new RATER();
            rate = (from rate2 in data.RATERs
                    where rate2.ID_RATER.Equals(rater.ID_RATER)
                    select rate2).FirstOrDefault();
            rate.FirstRater = rater.FirstRater;
            rate.SecondRater = rater.SecondRater;
            data.SubmitChanges();
        }

        public static void deleteRater(int id_rater)
        {
            CADcostDataContext data = new CADcostDataContext();
            RATER rate = new RATER();
            rate = (from rate2 in data.RATERs
                    where rate2.ID_RATER.Equals(id_rater)
                    select rate2).FirstOrDefault();
            data.RATERs.DeleteOnSubmit(rate);
            data.SubmitChanges();
        }

        public static void deleteAllRater(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            List<RATER> rate = new List<RATER>();
            rate = (from rate2 in data.RATERs
                    where rate2.JOBCODE_ID.Equals(jobcode_id)
                    select rate2).ToList() ;
            data.RATERs.DeleteAllOnSubmit(rate);
            data.SubmitChanges();
        }

        public static void insertProject(PROJECT project)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.PROJECTs.InsertOnSubmit(project);
            data.SubmitChanges();
            OvertimeDataContext data2 = new OvertimeDataContext();
            List<RATER> rater = new List<RATER>();
            Func<string, string, string,string, RATER> makeRater = (p1, p2, p3, p4) => new RATER {JOBCODE_ID=p1, SECT_ID=p2, FirstRater=p3, SecondRater=p4};
            rater = (from rater2 in data2.TBL_OVERTIMEREQ_APPs
                     where rater2.Jobcode_id.Equals(project.JOBCODE_ID) && DateTime.Now >= rater2.Startdate && DateTime.Now <= rater2.EndDate
                         select makeRater(rater2.Jobcode_id, rater2.Sect_Id, rater2.FirstRater, rater2.SecondRater)).ToList();
            data.RATERs.InsertAllOnSubmit(rater);
            data.SubmitChanges();

        }

        public static void updateProject(PROJECT project)
        {
            CADcostDataContext data = new CADcostDataContext();
            PROJECT prjt = new PROJECT();
            prjt = (from prjt2 in data.PROJECTs
                    where prjt2.JOBCODE_ID.Equals(project.JOBCODE_ID)
                    select prjt2).FirstOrDefault();
            prjt.DATE_REMINDER = project.DATE_REMINDER;
            data.SubmitChanges();
        }

        public static void deleteProject(string jobcode_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PROJECT prjt = new PROJECT();
            prjt = (from prjt2 in data.PROJECTs
                      where prjt2.JOBCODE_ID.Equals(jobcode_id)
                      select prjt2).FirstOrDefault();
            data.PROJECTs.DeleteOnSubmit(prjt);
            data.SubmitChanges();
            List<RATER> rater = new List<RATER>();
            rater = (from rater2 in data.RATERs
                     where rater2.JOBCODE_ID.Equals(jobcode_id)
                     select rater2).ToList();
            data.RATERs.DeleteAllOnSubmit(rater);
            data.SubmitChanges();
        }

        public static void insertMICARater(string jobcode_id)
        {
            Func<int, string,string, string, string, string,string, ProjectRater> makeProjectRater = (p1, p2, p3, p4, p5, p6, p7) => new ProjectRater { ID_RATER = p1, SECT_ID=p2, DEPARTMENT = p3, FirstRaterID=p4, FirstRater = p5, SecondRaterID=p6, SecondRater = p7 };
            CADcostDataContext data = new CADcostDataContext();
            List<ProjectRater> rater = new List<ProjectRater>();
            rater=(from rater2 in data.RATERs
                   where rater2.JOBCODE_ID.Equals(jobcode_id)
                   select makeProjectRater(rater2.ID_RATER, rater2.SECT_ID, PFNATTHelper.getSectName(rater2.SECT_ID), rater2.FirstRater, PFNATTHelper.getEMPFullName(rater2.FirstRater), rater2.SecondRater, PFNATTHelper.getEMPFullName(rater2.SecondRater))).ToList();
        }

        public static void insertRequester(REQUESTER req)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.REQUESTERs.InsertOnSubmit(req);
            data.SubmitChanges();
        }

        public static void deleteRequester(string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            REQUESTER req = data.REQUESTERs.Where(x => x.EMP_ID.Equals(emp_id)).FirstOrDefault();
            data.REQUESTERs.DeleteOnSubmit(req);
            data.SubmitChanges();
        }

        public static bool getSoftwareServerStatus(int id_software_server)
        {
            CADcostDataContext data = new CADcostDataContext();
            bool status = (from sfwrsvr in data.SOFTWARE_SERVERs
                           where sfwrsvr.ID_SOFTWARE_SERVER.Equals(id_software_server)
                           select sfwrsvr.LICENSE_STATUS).FirstOrDefault();
            return status;
        }

        public static void insertSoftwareServer(SOFTWARE_SERVER sfwrServer)
        {
            CADcostDataContext data = new CADcostDataContext();
            data.SOFTWARE_SERVERs.InsertOnSubmit(sfwrServer);
            data.SubmitChanges();
        }

        public static void updateSoftwareServer(int id_software_server, bool status, int id_server, int quantity, string remark)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_SERVER sfwrServer2 = new SOFTWARE_SERVER();
            sfwrServer2 = (from softwareServer in data.SOFTWARE_SERVERs
                           where softwareServer.ID_SOFTWARE_SERVER.Equals(id_software_server)
                           select softwareServer).FirstOrDefault();
            sfwrServer2.ID_SERVER = id_server;
            if(status==true)
            sfwrServer2.QUANTITY = quantity;
            sfwrServer2.LAST_UPDATED = DateTime.Now;
            sfwrServer2.REMARK = remark;
            data.SubmitChanges();
        }

        public static void deleteSoftwareServer(int id_software_server)
        {
            Func<int, int, string, int, int, int, int, CADDETAIL> makeCADdetail = (p1, p2,p3, p4,p5, p6,p7) => new CADDETAIL { ID_CADREQ=p1, QUANTITY = p2, REMARK=p3, ID_SOFTWARE=p4, ID_COST=p5, ID_SOFTWARE_SERVER=p6, ID_CADDETAIL=p7};
            CADcostDataContext data = new CADcostDataContext();
            List<CADDETAIL> caddetail = (from detail in data.CADDETAILs
                                   where detail.ID_SOFTWARE_SERVER.Equals(id_software_server)
                                   select detail).ToList();
            caddetail.ForEach(i=> i.ID_SOFTWARE_SERVER = null);
            data.SubmitChanges();
            SOFTWARE_SERVER sfwrServer = new SOFTWARE_SERVER();
            sfwrServer = (from softwareServer in data.SOFTWARE_SERVERs
                          where softwareServer.ID_SOFTWARE_SERVER.Equals(id_software_server)
                          select softwareServer).FirstOrDefault();
            data.SOFTWARE_SERVERs.DeleteOnSubmit(sfwrServer);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE_SERVER" + ", RESEED)");
        }

        public static void insertSoftwareCost(SOFTWARE_COST sfwrCost, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            if(id_currency!=0)
            {
                decimal inverse  = (from cur in data.CURRENCies
                                 where cur.ID_CURRENCY.Equals(id_currency)
                                 select cur.INVERSE).FirstOrDefault();
                sfwrCost.COST *= inverse;
                sfwrCost.COST_TO_COST *= inverse;
            }
            data.SOFTWARE_COSTs.InsertOnSubmit(sfwrCost);
            data.SubmitChanges();
        }

        public static void updateSoftwareCost(SOFTWARE_COST sfwrCost, int id_currency)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_COST cost = new SOFTWARE_COST();
            cost = (from cost2 in data.SOFTWARE_COSTs
                    where cost2.ID_COST.Equals(sfwrCost.ID_COST)
                    select cost2).FirstOrDefault();
            SOFTWARE_COST cost3 = new SOFTWARE_COST();
            cost3.ID_SOFTWARE = cost.ID_SOFTWARE;
            cost3.TYPE = sfwrCost.TYPE;
            cost3.COST = sfwrCost.COST;
            if (id_currency != 0)
            {
                decimal inverse = (from cur in data.CURRENCies
                                   where cur.ID_CURRENCY.Equals(id_currency)
                                   select cur.INVERSE).FirstOrDefault();
                cost3.COST *= inverse;
                cost3.COST_TO_COST *= inverse;
            }
            cost3.CAD_STARTDATE = DateTime.Now;
            data.SOFTWARE_COSTs.InsertOnSubmit(cost3);
            data.SubmitChanges();
        }

        public static void deleteSoftwareCost(int id_cost)
        {
            CADcostDataContext data = new CADcostDataContext();
            SOFTWARE_COST cost = new SOFTWARE_COST();
            cost = (from cost2 in data.SOFTWARE_COSTs
                          where cost2.ID_COST.Equals(id_cost)
                          select cost2).FirstOrDefault();
            List<SOFTWARE_COST> listcost = new List<SOFTWARE_COST>();
            listcost = (from cost3 in data.SOFTWARE_COSTs
                        where cost3.ID_SOFTWARE.Equals(cost.ID_SOFTWARE) && cost3.TYPE.Equals(cost.TYPE)
                        select cost3).ToList();
            data.SOFTWARE_COSTs.DeleteAllOnSubmit(listcost);
            data.SubmitChanges();
            data.ExecuteCommand("DBCC CHECKIDENT (" + "SOFTWARE_COST" + ", RESEED)");
        }

        public static bool setDelegate(string delegated, string delegating)
        {
            CADcostDataContext data = new CADcostDataContext();
            DELEGATE delegate2 = new DELEGATE();
            delegate2.EMP_Delegated = delegated;
            delegate2.EMP_Delegating = delegating;
                delegate2.IMPERSONATE = true;
            data.DELEGATEs.InsertOnSubmit(delegate2);
            try
            {
                data.SubmitChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool releaseDelegate(string emp_id)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            DELEGATE delegate2 = data.DELEGATEs.FirstOrDefault(x => x.EMP_Delegating.Equals(emp_id));
            
            try
            {
                data.DELEGATEs.DeleteOnSubmit(delegate2);
                data.SubmitChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static void editDelegate(string delegated, string delegating)
        {
            CADcostDataContext data = new CADcostDataContext();
            DELEGATE deleg = new DELEGATE();
            deleg = (from del in data.DELEGATEs
                     where del.EMP_Delegating.Equals(delegating)
                     select del).FirstOrDefault();
            deleg.EMP_Delegated = delegated;
            data.SubmitChanges();
        }

        public static void deleteDelegate(string delegating)
        {
            CADcostDataContext data = new CADcostDataContext();
            DELEGATE deleg = new DELEGATE();
            deleg = (from del in data.DELEGATEs
                     where del.EMP_Delegating.Equals(delegating)
                    select del).FirstOrDefault();
            data.DELEGATEs.DeleteOnSubmit(deleg);
            data.SubmitChanges();
        }

        public static bool insertImpersonate(string winCredential, string impersonate)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            string username = getUsername(winCredential);
            string logonEmployeeID = data2.TBL_EMAILs.FirstOrDefault(x => x.UserName.Equals(username)).Emp_Id;
            DELEGATE delegate2 = data.DELEGATEs.FirstOrDefault(x => (x.EMP_Delegated.Equals(impersonate) && (x.EMP_Delegating.Equals(logonEmployeeID))));
            if (delegate2 != null)
            {
                    delegate2.IMPERSONATE = true;
                    data.SubmitChanges();
                    return true;
            }
            else
            {
                return false;
            }
        }

        public static void releaseImpersonate(string winCredential)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            string username = getUsername(winCredential);
            string logonEmployeeID = data2.TBL_EMAILs.FirstOrDefault(x => x.UserName.Equals(username)).Emp_Id;
            List<DELEGATE> delegate2 = data.DELEGATEs.Where(x => (x.EMP_Delegating.Equals(logonEmployeeID))).ToList();
            if (delegate2 != null)
            {
                foreach (DELEGATE del in delegate2)
                {
                    del.IMPERSONATE = false;
                }
                data.SubmitChanges();
            }
        }

        public static bool isImpersonate(string username, out string impersonate, out bool isAdmin)
        {
            CADcostDataContext data = new CADcostDataContext(Connection.CADcostConn);
            PFNATTDataContext data2 = new PFNATTDataContext();
            string logonEmployeeID = data2.TBL_EMAILs.FirstOrDefault(x => x.UserName.Equals(username)).Emp_Id;
            isAdmin = CADcostHelper.isAdmin(username);
            impersonate = "";
            if(data.DELEGATEs.Any(x => (x.EMP_Delegating.Equals(logonEmployeeID) && (x.IMPERSONATE.Equals(true)))))
            {
                DELEGATE delegate2 = data.DELEGATEs.FirstOrDefault(x => x.EMP_Delegating.Equals(logonEmployeeID));
                impersonate = delegate2.EMP_Delegated;
                isAdmin = CADcostHelper.isAdmin(PFNATTHelper.getEMPCredential(impersonate));
                return true;
            }
            else
                return false;
        }

        public static bool isAdmin(string username)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();
            string logonEmployeeID = data2.TBL_EMAILs.FirstOrDefault(x => x.UserName.Equals(username)).Emp_Id;
            return data.ADMINs.Any(x => x.EMP_ID.Equals(logonEmployeeID));
        }

        public static string getUsername(string username)
        {
            int i = username.IndexOf("\\");
            username = username.Substring(i + 1);

            return username;
        }
        
        public static Role getRole(string emp_id, string jobcode_id) //per jobcode
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();

            Role role = new Role();
            List<RATER> listFirstRater;
            List<RATER> listSecondRater;
            RATER FirstRater;
            RATER SecondRater;
            string sect_id = PFNATTHelper.getEMPSect(emp_id);
            ADMIN enit = (from admin in data.ADMINs
                          where admin.EMP_ID.Equals(emp_id)
                          select admin).FirstOrDefault();

            listFirstRater = (from rater in data.RATERs
                              where rater.JOBCODE_ID.Equals(jobcode_id)
                              select rater).ToList();

            listSecondRater = (from rater in data.RATERs
                               where rater.JOBCODE_ID.Equals(jobcode_id)
                               select rater).ToList();

            FirstRater = listFirstRater.Where(x => x.FirstRater.Equals(emp_id)).FirstOrDefault();
            SecondRater = listSecondRater.Where(x => x.SecondRater.Equals(emp_id)).FirstOrDefault();

            if (enit != null)
            {
                role.intRole = 1;
                role.Sect = "ALL";
            }
            else if (FirstRater != null && SecondRater != null)
            {
                role.intRole = 2;
                role.Sect = FirstRater.SECT_ID;
            }
            else if (FirstRater != null)
            {
                role.intRole = 3;
                role.Sect = FirstRater.SECT_ID;
            }
            else if (SecondRater != null)
            {
                role.intRole = 4;
                role.Sect = SecondRater.SECT_ID;
            }
            else
            {
                role.intRole = 0;
                role.Sect = "ALL";
            }

            return role;
        }

    }
}