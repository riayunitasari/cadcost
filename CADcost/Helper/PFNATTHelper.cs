﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using CADcost.Entities;

namespace CADcost.Helper
{
    public class PFNATTHelper
    {
        public static void generateDDLProjectName(DropDownList ddl)
        {
            List<TBL_PROJECT> projectName = getAllActiveProjectName();
            foreach (TBL_PROJECT prjct in projectName)
            {
                ddl.Items.Add(new ListItem(prjct.JOBCODE_ID.ToString()+" - "+prjct.PROJECT_NAME.ToString(), prjct.JOBCODE_ID.ToString()));
            }
        }

        public static int generateDDLProjectNameFiltered(DropDownList ddl, string emp_id)
        {
            Func<string, string, TBL_PROJECT> makeProject = (t1, t2) => new TBL_PROJECT { JOBCODE_ID = t1, PROJECT_NAME = t2 };
            List<TBL_PROJECT> activeProject = getAllActiveProjectName();
            List<string> filteredProject = CADcostHelper.getFilteredProject(emp_id);
            List<TBL_PROJECT> projectName = new List<TBL_PROJECT>();
            projectName =(from project in activeProject
                                             join filter in filteredProject on project.JOBCODE_ID equals filter
                                             select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();
            foreach (TBL_PROJECT prjct in projectName)
            {
                ddl.Items.Add(new ListItem(prjct.JOBCODE_ID.ToString() + " - " + prjct.PROJECT_NAME.ToString(), prjct.JOBCODE_ID.ToString()));
            }
            return projectName.Count;
        }

        public static string getProjectName(string jobcode_id)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            return data.TBL_PROJECTs.FirstOrDefault(x => x.JOBCODE_ID.Equals(jobcode_id)).PROJECT_NAME;
        }

        public static void generateDDLDepartment(DropDownList ddl)
        {
            List<TBL_SECTION> department = getAllEngineeringDept();
            foreach (TBL_SECTION dept in department)
            {
                ddl.Items.Add(new ListItem(dept.SECT_NAME.Split('-').GetValue(1).ToString().Trim(), dept.SECT_ID.ToString()));
            }
        }

        public static void generateDDLEmployee(DropDownList ddl, string sect_id)
        {
            List<TBL_EMP> employee = getAllEmployeeDept(sect_id);
            ddl.Items.Clear();
            foreach (TBL_EMP emp in employee)
            {
                ddl.Items.Add(new ListItem(emp.EMP_ID+"-"+emp.FULL_NAME, emp.EMP_ID));
            }
        }

        public static List<TBL_PROJECT> getFilteredProjectByEmpId(string searchBy, string search, List<string> jobcodes)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            List<TBL_PROJECT> tblProject = new List<TBL_PROJECT>();
            List<TBL_PROJECT> activeProject = getAllActiveProjectName();
            Func<string, string, TBL_PROJECT> makeProject = (t1, t2) => new TBL_PROJECT { JOBCODE_ID = t1, PROJECT_NAME = t2 };
            tblProject = (from project in activeProject
                          join listjobcode in jobcodes on project.JOBCODE_ID equals listjobcode
                          select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();

            
            List<TBL_PROJECT> projectName = new List<TBL_PROJECT>();

            if (searchBy.Equals("JOBCODE_ID"))
            {
                projectName = (from project in tblProject
                               where project.JOBCODE_ID.Contains(search)
                               select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();
            }
            else if (searchBy.Equals("PROJECT_NAME"))
            {
                projectName = (from project in tblProject
                               where project.PROJECT_NAME.Contains(search)
                               select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();
            }

            return projectName;
        }

        public static List<TBL_PROJECT> getFilteredProjectName(string searchBy, string search)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            Func<string, string, TBL_PROJECT> makeProject = (t1, t2) => new TBL_PROJECT { JOBCODE_ID = t1, PROJECT_NAME = t2 };
            List<TBL_PROJECT> projectName = new List<TBL_PROJECT>();

            if (searchBy.Equals("JOBCODE_ID"))
            {
                projectName = (from project in data.TBL_PROJECTs
                               join project2 in
                                   (from project3 in data.TBL_PROJECTs group project3 by project3.JOBCODE_ID into g select new { JOBCODE_ID = g.Key, VERSION = g.Max(v => v.VERSION), REVISION = g.Max(rv => rv.REVISION) }) on project.JOBCODE_ID equals project2.JOBCODE_ID
                               where (project.STATUS == '1' || project.STATUS == '0') && project.VERSION == project2.VERSION && project.REVISION == project2.REVISION && project.JOBCODE_ID.Contains(search)
                               select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();
            }
            else if (searchBy.Equals("PROJECT_NAME"))
            {
                projectName = (from project in data.TBL_PROJECTs
                               join project2 in
                                   (from project3 in data.TBL_PROJECTs group project3 by project3.JOBCODE_ID into g select new { JOBCODE_ID = g.Key, VERSION = g.Max(v => v.VERSION), REVISION = g.Max(rv => rv.REVISION) }) on project.JOBCODE_ID equals project2.JOBCODE_ID
                               where (project.STATUS == '1' || project.STATUS == '0') && project.VERSION == project2.VERSION && project.REVISION == project2.REVISION && project.PROJECT_NAME.Contains(search)
                               select makeProject(project.JOBCODE_ID, project.PROJECT_NAME)).ToList();
            }

            return projectName;
        }

        public static List<TBL_SECTION> getAllEngineeringDept()
        {
            PFNATTDataContext data = new PFNATTDataContext();
            Func<string, string, string, string, string, string, string, TBL_SECTION> makeSection = (t1, t2, t3, t4, t5, t6, t7) => new TBL_SECTION { SECT_ID = t1, SECT_NAME = t2, DEPT_ID = t3, MGR_EMP_ID = t4, GEC_DEPT = t5, CATEGORY = t6, PHONE_NO = t7 };
            List<TBL_SECTION> department = new List<TBL_SECTION>();
            department = (from dprtmnt in data.TBL_SECTIONs
                          where dprtmnt.SECT_ID.Contains("E")
                          select makeSection(dprtmnt.SECT_ID, dprtmnt.SECT_NAME, dprtmnt.DEPT_ID, dprtmnt.MGR_EMP_ID, dprtmnt.GEC_DEPT, dprtmnt.CATEGORY, dprtmnt.PHONE_NO)).ToList();
            return department;
        }

        public static List<TBL_PROJECT> getAllActiveProjectName()
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            Func<string, string, TBL_PROJECT> makeProject = (t1, t2) => new TBL_PROJECT { JOBCODE_ID = t1, PROJECT_NAME = t2 };
            List<TBL_PROJECT> projectName = new List<TBL_PROJECT>();

            projectName = (from prjt in data.V_PMPC_ProjectLists
                           where prjt.STATUS == '1' || prjt.STATUS == '3'
                           select makeProject(prjt.JOBCODE_ID, prjt.PROJECT_NAME)).ToList();

            return projectName;
        }
        
        public static List<TBL_EMP> getAllEmployeeDept(string sect_id)
        {
            List<TBL_EMP> emp = new List<TBL_EMP>();
            PFNATTDataContext data = new PFNATTDataContext();
            if(sect_id.Equals("ALL"))
            emp = data.TBL_EMPs.Where(x=> x.STATUS=='1').ToList();
            else
                emp = data.TBL_EMPs.Where(x => x.SECT_ID.Equals(sect_id) && x.STATUS == '1').ToList();
            return emp;
        }

        public static List<TBL_PROJECT> getActiveJobCode(string searchBy, string search)
        {
            string jobcode = "";
            string pName = "";

            if (searchBy.Equals("JobCode"))
                jobcode = search;
            else pName = search;

            SqlConnection connection = new SqlConnection(Connection.CADcostConn);

            SqlCommand command = new SqlCommand("getActiveJobCode", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@projectName", pName);

            connection.Open();
            List<TBL_PROJECT> reqs = new List<TBL_PROJECT>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new TBL_PROJECT
                        {
                            JOBCODE_ID = req["JOBCODE_ID"].ToString(),
                            PROJECT_NAME = req["PROJECT_NAME"].ToString(),
                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static List<TBL_PROJECT> getAllJobCode(string searchBy, string search)
        {
            string jobcode = "";
            string pName = "";

            if (searchBy.Equals("JobCode"))
                jobcode = search;
            else pName = search;

            SqlConnection connection = new SqlConnection(Connection.CADcostConn);

            SqlCommand command = new SqlCommand("getJobCode", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@projectName", pName);

            connection.Open();
            List<TBL_PROJECT> reqs = new List<TBL_PROJECT>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new TBL_PROJECT
                        {
                            JOBCODE_ID = req["JOBCODE_ID"].ToString(),
                            PROJECT_NAME = req["PROJECT_NAME"].ToString(),
                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static string getFirstApproverEmail(string jobcode, string sect)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();

            string keyPerson = (from fwbs in data2.TBL_PROJECT_FWBs
                                where fwbs.JOBCODE_ID.Equals(jobcode)
                                select fwbs.KEYPERSON).FirstOrDefault();

            string clientJobcode = (from fwbs in data2.TBL_PROJECT_FWBs
                                    where fwbs.JOBCODE_ID.Equals(jobcode)
                                    select fwbs.CLIENT_JOBCODE).FirstOrDefault();

            string firstRater = data2.Funct_WhoMustApproved(keyPerson, jobcode, clientJobcode, sect, sect + "13F1", 1);

            return firstRater;
        }

        public static string getSecondApproverEmail(string jobcode, string sect)
        {
            CADcostDataContext data = new CADcostDataContext();
            PFNATTDataContext data2 = new PFNATTDataContext();

            string keyPerson = (from fwbs in data2.TBL_PROJECT_FWBs
                                where fwbs.JOBCODE_ID.Equals(jobcode)
                                select fwbs.KEYPERSON).FirstOrDefault();

            string clientJobcode = (from fwbs in data2.TBL_PROJECT_FWBs
                                    where fwbs.JOBCODE_ID.Equals(jobcode)
                                    select fwbs.CLIENT_JOBCODE).FirstOrDefault();

            string secondRater = data2.Funct_WhoMustApproved(keyPerson, jobcode, clientJobcode, sect, sect + "13F1", 2);

            return secondRater;
        }

        public static bool isEMPExist(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            return data.TBL_EMPs.Where(x => x.EMP_ID.Equals(empID) && x.STATUS.Equals("1")).Any();
        }

        public static List<Employee> getEmployee(string searchBy, string search)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            List<Employee> empData = new List<Employee>();

                empData = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          where emp.STATUS.Equals("1") &&
                          sect.SECT_NAME.Contains(searchBy.Equals("Department") ? search : "") &&
                          emp.FULL_NAME.Contains(searchBy.Equals("Name") ? search : "") &&
                          emp.EMP_ID.Contains(searchBy.Equals("Employee ID") ? search : "")
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                          }).ToList();

                return empData;
        }

        private static string getUsername(string username)
        {
            int i = username.IndexOf("\\");
            username = username.Substring(i + 1);

            return username;
        }

        public static Employee getEmpDataByUsername(string winCredential)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            IEnumerable<Employee> empData = null;
            string impersonate = "";
            //int appRater = 0;
            bool isAdmin;
            string username = getUsername(winCredential);
            if (CADcostHelper.isImpersonate(username, out impersonate, out isAdmin))
            {
                
                string empParticipantID = (from emp in data.TBL_EMPs
                                          join email in data.TBL_EMAILs on emp.EMP_ID equals email.Emp_Id
                                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                                          where email.UserName.ToLower() == username.ToLower()
                                          orderby email.GeneratedOn descending
                                          select emp.EMP_ID).FirstOrDefault().ToString();
                //is impersonate
                empData = from emp in data.TBL_EMPs
                          join email in data.TBL_EMAILs on emp.EMP_ID equals email.Emp_Id
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where emp.EMP_ID.Equals(impersonate)
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              USERNAME = email.UserName,
                              EMAIL = email.Email,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                              DEPT_ID = dept.DEPT_ID,
                              DEPT_NAME = dept.DEPT_NAME,
                              ISADMIN = isAdmin,
                              ISPARTICIPANT = false,
                              PARTICIPANT_ID = empParticipantID,
                              //ROLE = CADcostHelper.getRole(emp.EMP_ID)
                              //ISAPPROVER = Helper.RequisitionHelper.isApprover(emp.EMP_ID),
                              //APPRATER = appRater
                          };
            }
            else
            {
                //is not impersonate
                empData = from emp in data.TBL_EMPs
                          join email in data.TBL_EMAILs on emp.EMP_ID equals email.Emp_Id
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where email.UserName.ToLower() == username.ToLower()
                          orderby email.GeneratedOn descending
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              USERNAME = email.UserName,
                              EMAIL = email.Email,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                              DEPT_ID = dept.DEPT_ID,
                              DEPT_NAME = dept.DEPT_NAME,
                              ISADMIN = isAdmin,
                              ISPARTICIPANT = true,
                              PARTICIPANT_ID = "",
                              //ROLE = CADcostHelper.getRole(emp.EMP_ID)
                              //ISAPPROVER = Helper.RequisitionHelper.isApprover(emp.EMP_ID),

                          };
            }
            return empData.FirstOrDefault();
        }

        public static string getEMPFullName(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            if (empID != null)
                return data.TBL_EMPs.FirstOrDefault(x => x.EMP_ID.Equals(empID)).FULL_NAME;
            else
                return "";
        }

        public static string getEMPDept(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var empDept = from emp in data.TBL_EMPs
                          join dept in data.TBL_SECTIONs on emp.SECT_ID equals dept.SECT_ID
                          where emp.EMP_ID.Equals(empID)
                          select dept.SECT_NAME;

            foreach (var i in empDept)
                return  i.ToString();

            return null;
        }

        public static string getEMPSect(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            return data.TBL_EMPs.FirstOrDefault(x => x.EMP_ID.Equals(empID)).SECT_ID;
        }

        public static string getEMPCredential(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext();
            string credential = "";
            credential = (from email in data.TBL_EMAILs
                          where email.Emp_Id.Equals(empID)
                          select email.UserName).FirstOrDefault();
            return credential;
        }

        public static string getEMPidbyUsername(string username)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            return data.TBL_EMAILs.FirstOrDefault(x => x.UserName.Equals(getUsername(username))).Emp_Id;
        }

        public static string getMgrGA()
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var mgr = (from emp in data.TBL_EMPs
                       join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                       where sect.SECT_NAME.Contains("GAD")
                       select sect.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static string getMgrDept(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var mgr = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          where emp.EMP_ID.Equals(empID)
                          select sect.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static string getMgrDiv(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string mgr = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where emp.EMP_ID.Equals(empID)
                          select dept.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static bool isPMExist(string jobcode)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            return data.TBL_PROJECTs.Where(x => x.JOBCODE_ID.Equals(jobcode) && x.KEYP_EMP_ID != null).Any();
        }

        public static string getProjectMgr(string jobcode)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string PM = null;
            PM = data.TBL_PROJECTs.Where(x => x.JOBCODE_ID.Equals(jobcode)).Select(x => x.KEYP_EMP_ID).FirstOrDefault();
            return PM;
        }

        public static string getJGCManagement()
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string mgr = data.TBL_EMPs.FirstOrDefault(x => x.STATUS.Equals("1") && x.POSITION_ID.Contains("PDR")).EMP_ID;
            return mgr;
        }

        public static string getSectName(string sect_id)
        {
            PFNATTDataContext data = new PFNATTDataContext();
            TBL_SECTION sect = new TBL_SECTION();
            sect = (from sect2 in data.TBL_SECTIONs
                   where sect2.SECT_ID.Equals(sect_id)
                   select sect2).FirstOrDefault();
            return sect.SECT_NAME;
        }

        public static string getEMPEmail(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext();
            string email = (from tbl_email in data.TBL_EMAILs
                            where tbl_email.Emp_Id.Equals(empID)
                            select tbl_email.Email).FirstOrDefault();
            return email;
        }

    }
}