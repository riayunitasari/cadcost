﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertInGridView2.aspx.cs" Inherits="WebApplication2.InsertInGridView2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>"
            DeleteCommand="DELETE FROM [Customers] WHERE [CustomerID] = @CustomerID" InsertCommand="INSERT INTO [Customers] ([CustomerID], [CompanyName], [ContactName], [Country]) VALUES (@CustomerID, @CompanyName, @ContactName, @Country)"
            SelectCommand="SELECT [CustomerID], [CompanyName], [ContactName], [Country] FROM [Customers] ORDER BY [CustomerID]"
            UpdateCommand="UPDATE [Customers] SET [CompanyName] = @CompanyName, [ContactName] = @ContactName, [Country] = @Country WHERE [CustomerID] = @CustomerID">
            <DeleteParameters>
                <asp:Parameter Name="CustomerID" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="ContactName" Type="String" />
                <asp:Parameter Name="Country" Type="String" />
                <asp:Parameter Name="CustomerID" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="CustomerID" Type="String" />
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="ContactName" Type="String" />
                <asp:Parameter Name="Country" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
    
    </div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CustomerID" DataSourceID="SqlDataSource1"
            ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" ReadOnly="True" SortExpression="CustomerID" />
                <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName" />
                <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:ButtonField CommandName="Insert" Text="Insert" />
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EmptyDataTemplate>
                &nbsp;<asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False"
                    CellPadding="4" DataKeyNames="CustomerID" DataSourceID="SqlDataSource1" DefaultMode="Insert"
                    ForeColor="#333333" GridLines="None" Height="50px" OnItemInserted="DetailsView1_ItemInserted"
                    Width="100%">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                    <EditRowStyle BackColor="#999999" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <Fields>
                        <asp:BoundField DataField="CustomerID" HeaderText="Customer ID :" ReadOnly="True"
                            SortExpression="CustomerID" />
                        <asp:BoundField DataField="CompanyName" HeaderText="Company Name :" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="Contact Name :" SortExpression="ContactName" />
                        <asp:BoundField DataField="Country" HeaderText="Country :" SortExpression="Country" />
                        <asp:CommandField ShowInsertButton="True" />
                    </Fields>
                    <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" HorizontalAlign="Right" VerticalAlign="Top"
                        Width="10%" Wrap="False" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:DetailsView>
            </EmptyDataTemplate>
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <br />
        <br />
    </form>
</body>
</html>
