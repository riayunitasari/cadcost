﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Requisition.aspx.cs" Inherits="IntegratedPurchasingSystem.Requisition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=8,9,11" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/jquery.min.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script src="Script/autoNumeric.js"></script>
    <script src="Script/PageScript/requisitionPage.js"></script>

</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Purchase Data
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm">
                            <tr id="GAReqNo" runat="server">
                                <td class="contentFormField">GA Request No</td>
                                <td class="contentFormData">
                                    <table>
                                        <tr>
                                            <td>
                                                <%-- <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>--%>
                                                <asp:Label ID="lblGAReqNo" runat="server" Text=""></asp:Label>
                                                <%--</ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnGenerateGAReqNo" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>--%>
                                            </td>
                                            <td>
                                                <%--<asp:Button ID="btnGenerateGAReqNo" runat="server" Text="Generate No" OnClick="btnGenerateGAReqNo_Click" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="contentFormField">Date Created</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblDateCreated" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">Dept. Request No</td>
                                <td class="contentFormData">
                                    <div class="onEdit" runat="server">
                                        <asp:TextBox ID="txtDeptReqNo" runat="server" CssClass="validate[required]"></asp:TextBox>
                                    </div>
                                    <div class="onView" runat="server">
                                        <asp:Label ID="LblReqNo" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                                <td class="contentFormField">Department</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblDepartment" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Requested By</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblReqBy" runat="server" Text=""></asp:Label></td>
                                <td class="contentFormField">Request Type</td>
                                <td class="contentFormData">
                                    <div class="onEdit" runat="server">
                                        <asp:DropDownList ID="ddlReqType" runat="server" CssClass="validate[required]">
                                            <asp:ListItem Text="-- Select Type --" Value=""></asp:ListItem>
                                            <asp:ListItem Text="General Requisition" Value="RS"></asp:ListItem>
                                            <asp:ListItem Text="Stationary Requisition" Value="ST"></asp:ListItem>
                                            <asp:ListItem Text="IT Equipment Requisition" Value="SF"></asp:ListItem>
                                            <asp:ListItem Text="Advance" Value="AD"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="onView" runat="server">
                                        <asp:Label ID="lblReqType" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <div class="onEdit" runat="server" id="divJobcode">
                                        <asp:TextBox ID="txtJobCode" runat="server" CssClass="validate[required]"></asp:TextBox>
                                        &nbsp
                                        <asp:Button ID="btnJobCode" runat="server" Text=" ... " />
                                        &nbsp
                                        <asp:Button ID="btnResetJobCode" runat="server" Text="Reset"  />
                                    </div>
                                    <div class="onView" runat="server">
                                        <asp:Label ID="lblJobCode" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                                <td class="contentFormField">Project Name</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Currency</td>
                                <td class="contentFormData">
                                    <div class="onEdit" id="divCurrency" runat="server">
                                        <asp:DropDownList ID="ddlCurrency" runat="server"></asp:DropDownList>
                                        <div class="remark">* Currency can be filled or not</div>
                                    </div>
                                    <div class="onView" runat="server">
                                        <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>

                                    </div>
                                </td>
                                <td class="contentFormField">Exchage Rate</td>
                                <td class="contentFormData">
                                    <div class="onEdit" id="divExchangeRate" runat="server">
                                        <asp:TextBox ID="txtExchangeRate" runat="server"></asp:TextBox>
                                        <div class="remark">* Exchange rate mandatory if currency selected</div>
                                    </div>
                                    <div class="onView" runat="server">
                                        <asp:Label ID="lblExchangeRate" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField" style="font-weight: bold">Status</td>
                                <td class="contentFormData" style="font-weight: bold">
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></td>
                                <td class="contentFormField">
                                    <div runat="server" id="reqReportLabel">Document</div>
                                </td>
                                <td class="contentFormData">
                                    <div runat="server" id="reqReport">
                                        <asp:LinkButton ID="linkReport" runat="server">Print Requisition</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table runat="server" id="divPO">
                            <tr>
                                <td class="contentFormField">Purchase Order</td>
                                <td class="contentFormData">
                                    <asp:LinkButton ID="linkPO" runat="server" OnClick="linkPO_Click" Text=""></asp:LinkButton>
                                    </td>
                            </tr>         
                        </table>
                    </div>
                </div>


                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Item
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <div style="padding: 20px 20px 0px;">
                            <asp:Button ID="btnAddItem" runat="server" Text="Add Item" />
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnAdvance" runat="server" Text="Generate to Advance" OnClick="btnAdvance_Click" CssClass="hidden" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnSubmitToAdvance" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView CssClass="mGrid"
                                            ID="gridItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                            ShowHeaderWhenEmpty="true" OnRowCommand="gridItem_RowCommand"
                                            OnRowEditing="gridItem_RowEditing" OnRowUpdating="gridItem_RowUpdating"
                                            OnRowCancelingEdit="gridItem_RowCancelingEdit" OnPreRender="gridItem_PreRender">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("PART_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemID" runat="server" Text='<%# Eval("PART_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Metric">
                                                    <EditItemTemplate>
                                                        <div class="metricSingular">
                                                            <asp:DropDownList ID="ddlMetricPlural" runat="server">
                                                                <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                                <asp:ListItem Text="pcs"></asp:ListItem>
                                                                <asp:ListItem Text="boxes"></asp:ListItem>
                                                                <asp:ListItem Text="packs"></asp:ListItem>
                                                                <asp:ListItem Text="units"></asp:ListItem>
                                                                <asp:ListItem Text="seats"></asp:ListItem>
                                                                <asp:ListItem Text="kg"></asp:ListItem>
                                                                <asp:ListItem Text="grams"></asp:ListItem>
                                                                <asp:ListItem Text="meters"></asp:ListItem>
                                                                <asp:ListItem Text="cm"></asp:ListItem>
                                                                <asp:ListItem Text="lot"></asp:ListItem>
                                                                <asp:ListItem Text="dozens"></asp:ListItem>
                                                                <asp:ListItem Text="reams"></asp:ListItem>
                                                                <asp:ListItem Text="sets"></asp:ListItem>
                                                                <asp:ListItem Text="pairs"></asp:ListItem>
                                                                <asp:ListItem Text="pails"></asp:ListItem>
                                                                <asp:ListItem Text="rolls"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="metricPlural">
                                                            <asp:DropDownList ID="ddlMetricSingular" runat="server">
                                                                <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                                <asp:ListItem Text="pc"></asp:ListItem>
                                                                <asp:ListItem Text="box"></asp:ListItem>
                                                                <asp:ListItem Text="pack"></asp:ListItem>
                                                                <asp:ListItem Text="unit"></asp:ListItem>
                                                                <asp:ListItem Text="seat"></asp:ListItem>
                                                                <asp:ListItem Text="kg"></asp:ListItem>
                                                                <asp:ListItem Text="gram"></asp:ListItem>
                                                                <asp:ListItem Text="meter"></asp:ListItem>
                                                                <asp:ListItem Text="cm"></asp:ListItem>
                                                                <asp:ListItem Text="dozen"></asp:ListItem>
                                                                <asp:ListItem Text="ream"></asp:ListItem>
                                                                <asp:ListItem Text="set"></asp:ListItem>
                                                                <asp:ListItem Text="pair"></asp:ListItem>
                                                                <asp:ListItem Text="pail"></asp:ListItem>
                                                                <asp:ListItem Text="roll"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMetric" runat="server" Text='<%# Eval("METRIC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Price">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remark">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("REMARK")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <span><%# Eval("REMARK")%></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<a href="Requisition.aspx">Requisition.aspx</a>--%>
                                                <asp:TemplateField HeaderText="Quotation">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkQuotation" runat="server" CommandArgument='<%# Eval("ID_QUO")%>'
                                                            Text='<%# ((int)Eval("ID_QUO") == 0) ? "" : "Quotation No."+Eval("ID_QUO")%>' CommandName="SeeQuotation"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Advance Payment">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkAdvance" runat="server" CommandArgument='<%# Eval("ID_ADVANCE")%>'
                                                            Text='<%# ((int)Eval("ID_ADVANCE") == 0) ? "" : "Advance No."+Eval("ID_ADVANCE")%>' CommandName="SeeAdvance"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("PART_ID")%>'
                                                            Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                        <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("PART_ID")%>'
                                                            Text="Delete" CommandName="Delete Item" OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="linkUpdate" runat="server" CausesValidation="False" CommandName="Update" Text="Update" CommandArgument='<%# Eval("PART_ID") + ";" %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="linkCancelUpdate" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CommandArgument='<%# Eval("PART_ID") + ";" %>'></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Generate To Advance">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="checkAdvance" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSaveDetail" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSubmitToAdvance" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div style="float: right;" id="submitAdvance" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="remark">*Please check item that will be generated to Advance Payment and click Submit</div>
                                                </td>
                                                <td>
                                                        <asp:Button ID="btnSubmitToAdvance" runat="server" Text="Submit" OnClick="btnSubmitToAdvance_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnAdvance" />
                                </Triggers>
                            </asp:UpdatePanel>


                        </div>
                        <table class="contentForm">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="contentFormField" style="vertical-align: top">Attachment</td>
                                            <td class="contentFormData">
                                                <div class="onEdit" id="divAttachment" runat="server">
                                                    <asp:FileUpload ID="fileAttachmentUpload" runat="server" Width="300px" />
                                                    <br />
                                                    <ul class="remark">
                                                        <li>Only if requisition need some attachment file</li>
                                                        <li>Please put all needed attachment to one PDF file</li>
                                                        <li>File limit is 10MB</li>
                                                    </ul>
                                                </div>
                                                <%--<div class="onView">--%>
                                                <asp:LinkButton ID="linkPDF" runat="server"></asp:LinkButton>
                                                <%--</div>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentFormField">Total Estimation Amount</td>
                                            <td class="contentFormData">
                                                <asp:UpdatePanel ID="uptEstimation" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblCurrencyEst" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblTotalEst" runat="server"></asp:Label>
                                                        
                                                    </ContentTemplate>
                                                    <Triggers>
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentFormField">Items Additional Information </td>
                                            <td class="contentFormData">
                                                <div class="onEdit" runat="server">
                                                    <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                                <div class="onView" runat="server">
                                                    <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentFormField">Request Remark</td>
                                            <td class="contentFormData">
                                                <div class="onEdit" runat="server">
                                                    <asp:TextBox ID="txtReqRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                                <div class="onView" runat="server">
                                                    <asp:Label ID="lblReqRemark" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--                        <tr>
                                <td class="contentFormFieldTitle onEdit">
                                    <hr class="lineDivider" />
                                </td>
                            </tr>--%>
                        </table>

                    </div>
                </div>
                <div id="actionHistory" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action History
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnHiddenAction" runat="server" Text="Button" CausesValidation="false" Style="display: none;" />
                                    <asp:GridView ShowHeaderWhenEmpty="true"
                                        CssClass="mGrid" ID="gridActionHistory" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Participant" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("PARTICIPANT")%> - <%# Eval("PARTICIPANT_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Completed By" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("COMPLETED_BY")%> - <%# Eval("COMPLETED_BY_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Activity" DataField="ACTIVITY_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Action" DataField="ACTION_TEXT" />
                                            <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%#  Eval("START_DATE", "{0:dd MMM yyyy - HH:mm }") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Convert.ToString(Eval("END_DATE")).Contains("1/1/1975")?"":Eval("END_DATE", "{0:dd MMM yyyy - HH:mm}") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Comment" DataField="COMMENT" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnHiddenAction" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

                <div id="actionDiv" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action 
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <table>
                                <tr id="actionField" runat="server">
                                    <td class="contentFormField">Action</td>
                                    <td class="contentFormData">
                                        <asp:DropDownList ID="ddlAction" runat="server" CssClass="validate[required]">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentFormField">Comment</td>
                                    <td class="contentFormData">
                                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentFormField"></td>
                                    <td class="contentFormData">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Execute" OnClick="btnSubmit_Click" />
                                        <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div style="display: none">

                    <div id="dialogDetail">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <table class="contentForm" style="text-align: left;">

                                    <tr style="display: none;">
                                        <td>ID</td>
                                        <td>
                                            <asp:TextBox ID="txtItemID" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Code</td>
                                        <td>
                                            <asp:TextBox ID="txtItemCode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Part ID</td>
                                        <td>
                                            <asp:TextBox ID="txtPartTypeID" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Part Name</td>
                                        <td>
                                            <asp:TextBox ID="txtPartTypeName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Item</td>
                                        <td>
                                            <asp:TextBox ID="txtNewItem" runat="server" CssClass="validate[required]"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="contentFormData">Quantity</td>
                                        <td>
                                            <asp:TextBox ID="txtNewQuantity" runat="server" CssClass="validate[required, custom[float]]"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Metric</td>
                                        <td>
                                            <asp:DropDownList ID="ddlMetricPlural" runat="server" CssClass="validate[required] metricPlural">
                                                <asp:ListItem Text="-- Select Metric --" Value=""></asp:ListItem>
                                                <asp:ListItem Text="pcs" Value="pcs"></asp:ListItem>
                                                <asp:ListItem Text="boxes" Value="boxes"></asp:ListItem>
                                                <asp:ListItem Text="packs" Value="packs"></asp:ListItem>
                                                <asp:ListItem Text="units" Value="units"></asp:ListItem>
                                                <asp:ListItem Text="seats" Value="seats"></asp:ListItem>
                                                <asp:ListItem Text="kg" Value="kg"></asp:ListItem>
                                                <asp:ListItem Text="grams" Value="grams"></asp:ListItem>
                                                <asp:ListItem Text="meters" Value="meters"></asp:ListItem>
                                                <asp:ListItem Text="cm" Value="cm"></asp:ListItem>
                                                <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                <asp:ListItem Text="dozens" Value="dozens"></asp:ListItem>
                                                <asp:ListItem Text="reams" Value="reams"></asp:ListItem>
                                                <asp:ListItem Text="sets" Value="sets"></asp:ListItem>
                                                <asp:ListItem Text="pairs"></asp:ListItem>
                                                <asp:ListItem Text="pails"></asp:ListItem>
                                                <asp:ListItem Text="rolls"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlMetricSingular" runat="server" CssClass="validate[required] metricSingular">
                                                <asp:ListItem Text="-- Select Metric --" Value=""></asp:ListItem>
                                                <asp:ListItem Text="pc" Value="pc"></asp:ListItem>
                                                <asp:ListItem Text="box" Value="box"></asp:ListItem>
                                                <asp:ListItem Text="pack" Value="pack"></asp:ListItem>
                                                <asp:ListItem Text="unit" Value="unit"></asp:ListItem>
                                                <asp:ListItem Text="seat" Value="seat"></asp:ListItem>
                                                <asp:ListItem Text="kg" Value="kg"></asp:ListItem>
                                                <asp:ListItem Text="gram" Value="gram"></asp:ListItem>
                                                <asp:ListItem Text="meter" Value="meter"></asp:ListItem>
                                                <asp:ListItem Text="cm" Value="cm"></asp:ListItem>
                                                <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                <asp:ListItem Text="dozen" Value="dozen"></asp:ListItem>
                                                <asp:ListItem Text="ream" Value="ream"></asp:ListItem>
                                                <asp:ListItem Text="set" Value="set"></asp:ListItem>
                                                <asp:ListItem Text="pair"></asp:ListItem>
                                                <asp:ListItem Text="pail"></asp:ListItem>
                                                <asp:ListItem Text="roll"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Price</td>
                                        <td>
                                            <asp:TextBox ID="txtNewPrice" runat="server"></asp:TextBox><br />
                                            <div class="remark">* Price can be filled or not</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Remark</td>
                                        <td>
                                            <asp:TextBox ID="txtNewRemark" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="gridItemSearch" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                        <div style="width: 100%; text-align: center">
                            <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click" />
                            &nbsp<asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" />
                        </div>
                    </div>
                    <div id="dialogItem">
                        <div>
                            <table>
                                <tr>
                                    <td>Search Item By</td>
                                    <td>
                                        <asp:DropDownList ID="ddlItem" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Type"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormField">
                                        <asp:TextBox ID="txtSearchItem" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlItemType" runat="server"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSearchItem" runat="server" Text="Search" OnClick="btnSearchItem_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPanelItem" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridItemSearch_PageIndexChanging"
                                            ID="gridItemSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridItemSearch_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PART_ID" HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="PART_CODE" HeaderText="Item Code" />
                                                <asp:BoundField DataField="PART_NAME" HeaderText="Item Name" />
                                                <asp:BoundField DataField="TYPE_ID" HeaderText="ID Type" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchItem" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogJobCode">
                        <table class="contentForm">
                            <tr>
                                <td style="min-width: 80px;">Search By</td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCode" runat="server">
                                        <asp:ListItem Text="JobCode"></asp:ListItem>
                                        <asp:ListItem Text="Project Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchJobCode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearchJobCode" runat="server" Text="Search" OnClick="btnSearchJobCode_Click" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridJobCode_PageIndexChanging" CellPadding="2"
                                            ID="gridJobCode" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridJobCode_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE_ID" />
                                                <asp:BoundField runat="server" HeaderText="Project Name" DataField="PROJECT_NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>

                    <div id="divSuccess">
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>

                    <div id="dialogPrint">
                        <table>
                            <tr>
                                <td class="contentFormField">Include Item Price
                                </td>
                                <td class="contentFormData">
                                    <asp:RadioButtonList ID="rdoPrice" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="1" />
                                        <asp:ListItem Text="No" Value="0" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr id="printReqNo" runat="server">
                                <td class="contentFormField">REQ No. Format
                                </td>
                                <td class="contentFormData">
                                    <asp:RadioButtonList ID="rdoReqNo" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Dept Req No." Value="0" />
                                        <asp:ListItem Text="GA Req No." Value="1" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp
                                </td>
                                <td>
                                    <div style="padding-top: 20px;">
                                        <asp:Button ID="btnPrint" runat="server" Text="OK" UseSubmitBehavior="false" OnClick="btnPrint_Click" OnClientClick="ShowProgress();"  />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>


                    
                    <asp:Button ID="btnHiddenSave" runat="server" Text="Button" CausesValidation="false" OnClick="btnHiddenSave_Click" />
                    <asp:Button ID="btnReloadPage" runat="server" Text="" OnClick="btnReloadPage_Click" />
                    <asp:Button ID="btnDownload" runat="server" Text="Button" OnClick="btnDownload_Click" UseSubmitBehavior="false" />
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
