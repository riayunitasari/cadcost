﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;
using System.Drawing;
using System.IO;
using System.Web.Hosting;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Threading;

namespace IntegratedPurchasingSystem
{
    public partial class ReqSummary : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        const string jobCodeState = "current_JobCode_state";
        const string curState = "current_Req_State";
        const string itemstate = "dialog_Item_State";
        const string mergeItem = "merge_Item";
        const string curSum = "current_summary_requisition";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                ViewState[curUser] = logInUser;
                lblUsername.Text = logInUser.FULL_NAME;
                litMenu.Text = Helper.MenuHelper.getMenu();
                ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(DateTime.Now.Month.ToString()));
                Helper.GeneralHelper.generateDDLCurrency(ddlCurrency);
                Helper.PartTypeHelper.generateDDLType(ddlItemType);
                int idReq = 0;
                gridMergedItem.Columns[11].Visible = false;
                btnAdvance.Visible = false;
                submitAdvance.Visible = false;
                ViewState[jobCodeState] = "";
                if (int.TryParse(Request["ViewSummary"], out idReq))
                {
                    RequisitionObject req = Helper.RequisitionHelper.getRequisitionByID(idReq);
                    logInUser = Helper.GeneralHelper.isParticipant(logInUser, 2, req.ID_REQ, "REQ");
                    actionDiv.Visible = false;
                    if (!logInUser.ISPARTICIPANT && !logInUser.ISADMIN)
                        Response.Redirect("NotAuthorized.aspx");

                    if (req != null)
                    {
                        actionHistory.Visible = true;
                        loadActionHistory(req.ID_REQ);
                        Helper.GeneralHelper.generateDDLAction(ddlAction, 2, req.ACTIVITY_ID);
                        ViewState[curSum] = req;
                        List<SummaryItems> items = Helper.SummaryHelper.getSummaryItems(req.ID_REQ);
                        ViewState[mergeItem] = items;
                        ViewState[curState] = "view";
                        onViewState();

                        if (req.ACTIVITY_ID == Variable.Status.FixRequest && logInUser.IS_CUR_PARTICIPANT)
                        {
                            ViewState[curState] = "edit";
                            onEditState();
                        }
                        if (req.ACTIVITY_ID == Variable.Status.WaitingFordelivered || req.ACTIVITY_ID == Variable.Status.OnDeliveryProgress)
                            if (!items.Any(x => x.ID_QUO != 0))
                            {
                                btnAdvance.Visible = true;
                                if (items.Count == items.Count(x => x.ID_ADVANCE != 0))
                                    btnAdvance.Visible = false;
                            }
                    }
                    else
                        Response.Redirect("NotAuthorized.aspx");
                    floatRendering();
                }
                else
                {
                    if (!logInUser.ISADMIN)
                        Response.Redirect("NotAuthorized.aspx");

                    ViewState[curState] = "new";
                    //btnEdit.Visible = false;
                    lblUsername.Text = logInUser.FULL_NAME;
                    lblDepartment.Text = logInUser.SECT_NAME;
                    lblReqBy.Text = logInUser.EMP_ID + " - " + logInUser.FULL_NAME;
                    lblDateCreated.Text = DateTime.Now.ToString("dd MMM yyyy");
                    ViewState[mergeItem] = new List<SummaryItems>();
                    actionDiv.Visible = true;
                    actionHistory.Visible = false;
                    Helper.GeneralHelper.generateDDLAction(ddlAction, 2, Variable.Status.PrepareRequest);
                    loadSelectedItem();
                    loadNewItem();
                    loadMergeItems();
                    lblStatus.Text = "PREPARE REQUEST";
                    gridMergedItem.Columns[8].Visible = false;
                    reportDiv.Visible = false;
                    lblExchangeRate.Visible = false;
                    
                    floatRendering();
                }
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnDownload);
            }
        }

        private void floatRendering()
        {   
            double floatValue = 0;
            bool isFloat = false;
            isFloat = double.TryParse(lblExchangeRate.Text.Replace(",", ""), out floatValue);
            lblExchangeRate.Text = string.Format("{0:#,##0.00}", floatValue);

            isFloat = double.TryParse(lblTotalEst.Text.Replace(",", ""), out floatValue);

            lblTotalEst.Text = string.Format("{0:#,##0.00}", floatValue);
        }

        private void loadActionHistory(int idReq)
        {
            gridActionHistory.DataSource = Helper.ActionLogHelper.getActionLog(idReq, "REQ");
            gridActionHistory.DataBind();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridRequisition, this);
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            Helper.GeneralHelper.gridRender(gridItemSearch, this);
            base.Render(writer);
        }

        private void onViewState()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curSum];
            //btnEdit.Visible = true;
            btnGenerateGAReqNo.Visible = false;
            LblReqNo.Text = !string.IsNullOrEmpty(req.GA_REQ_NO) && !string.IsNullOrWhiteSpace(req.GA_REQ_NO) ? req.GA_REQ_NO : "None";
            lblDateCreated.Text = req.DATE_CREATED.ToString("dd MMM yyyy");
            lblReqBy.Text = req.REQ_BY + " - " + req.REQ_BY_NAME;
            lblDepartment.Text = req.REQ_BY_DEPT;
            lblJobCode.Text = req.JOBCODE;
            lblProjectName.Text = req.PROJECT_NAME;
            lblCurrencyEst.Text = req.CURRENCY_CODE;
            lblStatus.Text = req.ACTIVITY_TEXT;
            lblCurrency.Text = req.CURRENCY_CODE == null || req.CURRENCY_CODE.Equals(string.Empty) ? "none" : req.CURRENCY_CODE;
            lblExchangeRate.Text = req.EXCHANGE_RATE.ToString();
            lblDesc.Text = req.DESCRIPTION == null || req.DESCRIPTION.Equals(string.Empty) ? "none" : req.DESCRIPTION;
            lblReqRemark.Text = req.REMARKS == null || req.REMARKS.Equals(string.Empty) ? "none" : req.REMARKS;
            lblTotalEst.Text = req.TOTAL_EST_AMOUNT.ToString();
            lblSumType.Text = req.REQ_TYPE_TEXT;
            gridItem.ShowFooter = false;
            gridItem.Columns[8].Visible = false;
            gridSelectedItem.Columns[9].Visible = false;
            gridMergedItem.Columns[10].Visible = false;
            loadMergeItems();
            loadNewItem();
            loadSelectedItem();
            logInUser = (Employee)ViewState[curUser];
            actionDiv.Visible = false;
            if (logInUser.IS_CUR_PARTICIPANT)
                if (req.ACTIVITY_ID != Variable.Status.DoneRequest && req.ACTIVITY_ID != Variable.Status.CanceledRequest)
                    actionDiv.Visible = true;
            
            reportDiv.Visible = true;
            //ScriptManager.RegisterStartupScript(this, GetType(), "setOnView", "setOnView();", true);
            ddlCurrency.Visible = false;
            txtExchangeRate.Visible = false;
            btnAddRequisition.Visible = false;
            btnNewItem.Visible = false;
            txtDesc.Visible = false;
            txtReqRemark.Visible = false;
            divJobCode.Visible = false;
            ddlSumType.Visible = false;
        }

        private void onEditState()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curSum];
            if (req != null)
            {
                btnGenerateGAReqNo.Visible = true;
                if (!string.IsNullOrEmpty(req.GA_REQ_NO) && !string.IsNullOrWhiteSpace(req.GA_REQ_NO) &&
                    !req.GA_REQ_NO.Contains("None"))
                    btnGenerateGAReqNo.Visible = false;
                LblReqNo.Text = !string.IsNullOrEmpty(req.GA_REQ_NO) && !string.IsNullOrWhiteSpace(req.GA_REQ_NO) ? req.GA_REQ_NO : "None";
                txtJobCode.Text = req.JOBCODE;
                ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(req.CURRENCY_CODE));
                txtExchangeRate.Text = req.EXCHANGE_RATE.ToString();
                txtDesc.Text = req.DESCRIPTION;
                txtReqRemark.Text = req.REMARKS;
                lblCurrencyEst.Text = req.CURRENCY_CODE;
                lblTotalEst.Text = req.TOTAL_EST_AMOUNT.ToString();
                ddlSumType.SelectedIndex = ddlSumType.Items.IndexOf(ddlSumType.Items.FindByValue(req.REQ_TYPE));
                gridItem.ShowFooter = true;
                gridItem.Columns[8].Visible = true;
                gridSelectedItem.Columns[9].Visible = true;
                gridMergedItem.Columns[8].Visible = true;
                gridItem.Columns[9].Visible = true;
                gridItem.Columns[11].Visible = true;
                actionHistory.Visible = true;
                logInUser = (Employee)ViewState[curUser];
                actionDiv.Visible = false;
                

                if (logInUser.IS_CUR_PARTICIPANT)
                    if (req.ACTIVITY_ID != Variable.Status.DoneRequest && req.ACTIVITY_ID != Variable.Status.CanceledRequest)
                        actionDiv.Visible = true;
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "setOnEdit", "setOnEdit();", true);
            lblJobCode.Visible = false;
            lblSumType.Visible = false;
            lblCurrency.Visible = false;
            lblExchangeRate.Visible = false;
            lblDesc.Visible = false;
            lblReqRemark.Visible = false;

        }

        #region JobCode
        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;
            string pName = gridJobCode.SelectedRow.Cells[2].Text;
            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            if (ViewState[jobCodeState].ToString().Contains("onDialog"))
                txtJobCodeReq.Text = jobcode;
            else
                txtJobCode.Text = jobcode;

            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "', '" + pName + "','" + ViewState[jobCodeState].ToString() + "');", true);
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            try
            {
                gridJobCode.DataSource = Helper.PFNATTHelper.getActiveJobCode(ddlJobCode.SelectedItem.Text, search);
                gridJobCode.DataBind();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showMsg('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            loadSearchResultJobCode();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString()+"hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set to center", "setCenter('dialogJobCode');", true);
        }

        #endregion

        #region Item
        protected void gridItemReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemReq.PageIndex = e.NewPageIndex;
            loadItemOnselectedReq();
        }

        private void loadSelectedItem()
        {    // nanti untuk bagian edit , jika new id_req detail != 0 , jika edit id_req != current id_req
            List<RelatedDetail> items = new List<RelatedDetail>();
            items = ((List<SummaryItems>)ViewState[mergeItem]).SelectMany(x => x.RELATED_DETAIL.Where(y => y.ISADDITIONALITEM == 0)).ToList();
            gridSelectedItem.DataSource = items;
            gridSelectedItem.DataBind();

            ScriptManager.RegisterStartupScript(this, GetType(), "load sum ddlMetric", "ddlMetricOnNew();", true);

        }

        private void loadNewItem()
        {
            List<RelatedDetail> items = new List<RelatedDetail>();
            items = ((List<SummaryItems>)ViewState[mergeItem]).SelectMany(x => x.RELATED_DETAIL.Where(y => y.ISADDITIONALITEM == 1)).ToList(); ;
            gridItem.DataSource = items;
            gridItem.DataBind();
            //ScriptManager.RegisterStartupScript(this, GetType(), "load new ddlMetric", "ddlMetricOnNew();", true);
        }

        private bool isItemExist(int item)//, bool isAdditionalItem)
        {
            bool isExist = ((List<SummaryItems>)ViewState[mergeItem]).Where(x => x.PART_ID == item).Any();
            if (isExist)
            {
                List<RelatedDetail> items = new List<RelatedDetail>();
                items = ((List<SummaryItems>)ViewState[mergeItem]).FirstOrDefault(x => x.PART_ID == item).RELATED_DETAIL.Where(y => y.ISADDITIONALITEM == 1).ToList();
                isExist = items.Where(x => x.PART_ID.Equals(item)).Any();
            }
            return isExist;
        }

        protected void btnGetSelectedItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in gridItemReq.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (row.Cells[10].FindControl("checkItem") as CheckBox);
                        if (chkRow.Checked)
                        {
                            int idReq = int.Parse(row.Cells[0].Text);
                            int idDetail = int.Parse(row.Cells[1].Text);
                            RelatedDetail item = Helper.SummaryHelper.getItemFromReqDetail(idDetail);
                            addMergedItem(idReq, idDetail, item.DEPT_REQ_NO, item.PART_ID, item.PART_NAME, item.PART_CODE, item.TYPE_ID, item.TYPE_NAME, item.QUANTITY, 0, item.METRIC, item.PRICE, item.REMARK, 0);
                            loadSelectedItem();
                            loadMergeItems();
                            loadAvalaibleRequisition();
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "close item req", "closeItemReq();", true);
            }
            catch (Exception sltItem)
            {
                string er = HttpUtility.HtmlEncode(sltItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item select Error", "showMsg('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            }
        }

        private bool isDetailAlreadyAdded(int idDetail)
        {
            List<RelatedDetail> detail = ((List<SummaryItems>)ViewState[mergeItem]).SelectMany(x => x.RELATED_DETAIL).ToList();
            return detail.Where(x => x.ID_REQ_DETAIL == idDetail).Any();
        }

        protected void gridItemReq_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridItemReq.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label price = (Label)row.FindControl("lblPrice");
                    if (price != null)
                        price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text.Replace(",", "")));

                    Label quantity = (Label)row.FindControl("lblQuantity");
                    if (quantity != null)
                        quantity.Text = string.Format("{0:#,##0.00}", double.Parse(quantity.Text.Replace(",", "")));

                    if (isDetailAlreadyAdded(int.Parse(row.Cells[1].Text)))
                    {
                        CheckBox chkRow = (row.Cells[7].FindControl("checkItem") as CheckBox);
                        if (chkRow != null)
                        {
                            row.BackColor = Color.FromName("#EDBD9D");
                            chkRow.Visible = false;
                        }
                    }
                }
            }
        }

        #endregion

        #region Requisition

        private void loadAvalaibleRequisition()
        {
            string jobcode = "";
            if (ViewState[jobCodeState].ToString().Equals("onDialog"))
                jobcode = txtJobCodeReq.Text;
            else jobcode = txtJobCode.Text;
            gridRequisition.DataSource = Helper.SummaryHelper.getRequisitionForSummary(txtReqNo.Text, int.Parse(ddlMonth.SelectedItem.Value),
                jobcode, ddlReqType.SelectedItem.Value);
            gridRequisition.DataBind();
        }

        protected void btnSearchReq_Click(object sender, EventArgs e)
        {
            loadAvalaibleRequisition();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "center requisition", "setCenter('dialogRequisition');", true);
        }

        protected void gridRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridRequisition.PageIndex = e.NewPageIndex;
            loadAvalaibleRequisition();
            ScriptManager.RegisterStartupScript(this, GetType(), "center requisition", "setCenter('dialogRequisition');", true);
        }

        private void loadItemOnselectedReq()
        {
            int id = int.Parse(lblIDReq.Text);
            gridItemReq.DataSource = Helper.RequisitionHelper.getRequisitionDetail(id);
            gridItemReq.DataBind();
        }

        protected void gridRequisition_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = int.Parse(gridRequisition.SelectedRow.Cells[0].Text);
            lblIDReq.Text = id.ToString();

            loadItemOnselectedReq();

            ScriptManager.RegisterStartupScript(this, GetType(), "open item req", "openDialogItemReq();", true);
        }

        protected void btnAddRequisition_Click(object sender, EventArgs e)
        {
            ViewState[jobCodeState] = "onDialog";
            //gridItem.ShowFooter = false;
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "open dialog req", "openDialogRequisition();", true);
        }

        private bool isReqAlreadyAdded(int idReq)
        {
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            List<RelatedDetail> detail = items.SelectMany(x => x.RELATED_DETAIL).ToList();
            return detail.Where(x => x.ID_REQ == idReq).Any();
        }

        protected void gridRequisition_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridRequisition.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    if (isReqAlreadyAdded(int.Parse(row.Cells[0].Text)))
                    {
                        row.BackColor = Color.FromName("#EDBD9D");
                    }
                }
            }
        }
        #endregion

        private void loadSearchItem()
        {
            string search = "";
            if (ddlItem.SelectedItem.Text.Equals("Type"))
                search = ddlItemType.SelectedItem.Text;
            else
                search = txtSearchItem.Text;
            try
            {
                gridItemSearch.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
                gridItemSearch.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "set item center", "setCenter('dialogItem')", true);
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showMsg('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            loadSearchItem();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "metricload", "ddlMetricOnNew();", true);
        }

        protected void gridItemSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int idItem = int.Parse(gridItemSearch.SelectedRow.Cells[1].Text);
            //string itemName = gridItemSearch.SelectedRow.Cells[2].Text;

            //TextBox txtName = gridItem.FooterRow.FindControl("txtNewItem") as TextBox;
            //txtName.Text = itemName;

            //TextBox txtID = gridItem.FooterRow.FindControl("txtNewItemID") as TextBox;
            //txtID.Text = idItem.ToString();

            //ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogItem", "closeDialogItem();", true);

            int idItem = 0, typeID = 0;
            bool isIdItemINT = int.TryParse(gridItemSearch.SelectedRow.Cells[1].Text, out idItem);
            string itemCode = gridItemSearch.SelectedRow.Cells[2].Text;
            string itemName = gridItemSearch.SelectedRow.Cells[3].Text;
            bool isTypeIDINT = int.TryParse(gridItemSearch.SelectedRow.Cells[4].Text, out typeID);
            string typeName = gridItemSearch.SelectedRow.Cells[5].Text;

            if (isIdItemINT && isTypeIDINT)
            {
                //loadGridViewItem();
                txtItemID.Text = idItem.ToString();
                txtItemCode.Text = itemCode;
                txtPartTypeID.Text = typeID.ToString();
                txtPartTypeName.Text = typeName;
                txtNewItem.Text = itemName.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogItem", "closeDialogItem();", true);
            }
        }

        protected void gridItemSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemSearch.PageIndex = e.NewPageIndex;
            loadSearchItem();
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            gridItem.ShowFooter = true;
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "metricload", "ddlMetricOnNew();", true);
        }

        protected void gridItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete Item"))
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                int idItem = int.Parse(arg[0]);
                int idDetail;
                if (!int.TryParse(arg[1], out idDetail))
                    idDetail = 0;
                deleteMergedItem(idDetail, idItem);
                //btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
                loadNewItem();
                loadMergeItems();
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
            }
            else if (e.CommandName.Equals("Cancel"))
            {
                gridItem.ShowFooter = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "on cancel", "onRowCancelInsert();", true);
            }
        }

        protected void gridItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = gridItem.Rows[e.NewEditIndex];
            Label metric = row.FindControl("lblMetric") as Label;

            Label quantity = row.FindControl("lblQuantity") as Label;

            gridItem.ShowFooter = false;

            if (metric != null && quantity != null)
            {
                gridItem.EditIndex = e.NewEditIndex;
                loadNewItem();
                ScriptManager.RegisterStartupScript(this, GetType(), "metric load on Edit", "setDLLMetricOnEdit('gridItem'," + quantity.Text + ", '" + metric.Text + "'," + e.NewEditIndex + ")", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "on item editing", "onRowEditing();", true);
            }
        }

        protected void gridItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            double valQuantity = 0, valPrice = 0;
            Label itemName = (Label)gridItem.Rows[e.RowIndex].FindControl("lblItemName");
            Label itemID = (Label)gridItem.Rows[e.RowIndex].Cells[2].FindControl("lblItemID");
            Label detailID = (Label)gridItem.Rows[e.RowIndex].FindControl("lblDetailID");
            TextBox quantity = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtQuantity");
            DropDownList metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricSingular");
            bool isQNumber = double.TryParse(quantity.Text.Replace(",", ""), out valQuantity);
            TextBox price = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtPrice");
            bool isPNumber = double.TryParse(price.Text.Replace(",", ""), out valPrice);
            TextBox remark = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtRemarks");

            if (valQuantity > 1)
                metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricPlural");

            if (itemName.Text.Equals(string.Empty) || quantity.Text.Equals(string.Empty) || metric.SelectedItem.Text.Contains("-- ") ||
                        !isQNumber || (!price.Text.Equals(string.Empty) && !isPNumber) || string.IsNullOrEmpty(itemID.Text))
            {
                gridItem.EditIndex = -1;
                loadNewItem();
                loadMergeItems();
                ScriptManager.RegisterStartupScript(this, GetType(), "metricload", "ddlMetricOnNew();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "update item failed", "showMsg('Error!','Invalid input! Please check your input')", true);
            }
            else
            {
                List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];

                int idDetail = 0;
                if (ViewState[curState].ToString().Equals("edit") && !detailID.Text.Equals(string.Empty))
                    idDetail = int.Parse(detailID.Text);
                RelatedDetail item = items.FirstOrDefault(x => x.PART_ID == int.Parse(itemID.Text)).RELATED_DETAIL.FirstOrDefault(y => y.ID_REQ_DETAIL == idDetail);
                updateMergedItem(idDetail, int.Parse(itemID.Text), valQuantity, 0, metric.SelectedItem.Text, valPrice, remark.Text);
                
                gridItem.EditIndex = -1;
                loadNewItem();
                loadMergeItems();
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "on item updating", "onRowCompleteEditing();", true);
        }

        protected void gridItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridItem.EditIndex = -1;
            loadNewItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "on item cancel", "onRowCompleteEditing();", true);
        }

        protected void gridSelectedItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete Item"))
            {
                string[] args = e.CommandArgument.ToString().Split(';');
                deleteMergedItem(int.Parse(args[0]), int.Parse(args[1]));
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
                loadSelectedItem();
                loadMergeItems();
            }
            else if (e.CommandName.Equals("See Requisition"))
            {
                string args = e.CommandArgument.ToString();
                Response.Redirect("Requisition.aspx?ViewRequisition=" + args);
            }
        }

        protected void gridSelectedItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gridSelectedItem.EditIndex = e.NewEditIndex;
            loadSelectedItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "on selected item editing", "onRowEditing();", true);
        }

        protected void gridSelectedItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label detailNo = (Label)gridSelectedItem.Rows[e.RowIndex].FindControl("lblIDDetail");
            Label itemID = (Label)gridSelectedItem.Rows[e.RowIndex].FindControl("lblItemID");
            TextBox addQuantity = (TextBox)gridSelectedItem.Rows[e.RowIndex].FindControl("txtAddQuantity");
            TextBox remark = (TextBox)gridSelectedItem.Rows[e.RowIndex].FindControl("txtRemark");
            double valAddQuant = 0;
            bool isNumber = double.TryParse(addQuantity.Text.Replace(",", ""), out valAddQuant);
            if (detailNo != null && addQuantity != null && itemID != null && isNumber)
            {
                List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
                RelatedDetail item = items.FirstOrDefault(x => x.PART_ID == int.Parse(itemID.Text)).RELATED_DETAIL.FirstOrDefault(y => y.ID_REQ_DETAIL == int.Parse(detailNo.Text));
                updateMergedItem(int.Parse(detailNo.Text), int.Parse(itemID.Text), item.QUANTITY, valAddQuant, item.METRIC, item.PRICE, remark.Text);
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
                gridSelectedItem.EditIndex = -1;
                loadSelectedItem();
                loadMergeItems();
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
                ScriptManager.RegisterStartupScript(this, GetType(), "on selected item updating", "onRowCompleteEditing();", true);
            }
        }

        protected void gridSelectedItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridSelectedItem.EditIndex = -1;
            loadSelectedItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "on selected item cancel", "onRowCompleteEditing();", true);
        }

        //protected void btnNewItem_Click(object sender, EventArgs e)
        //{
        //    gridItem.ShowFooter = true;
        //    loadNewItem();
        //    List<RelatedDetail> items = ((List<SummaryItems>)ViewState[mergeItem]).SelectMany(x => x.RELATED_DETAIL).Where(y => y.ISADDITIONALITEM == 1).ToList();
        //    if (items.Count <= 0)
        //    {
        //        var obj = new List<RelatedDetail>();
        //        obj.Add(new RelatedDetail());
        //        gridItem.DataSource = obj;
        //        gridItem.DataBind();

        //        gridItem.Rows[0].Visible = false;
        //        lblTotalEst.Text = "0";
        //    }

        //    ScriptManager.RegisterStartupScript(this, GetType(), "load ddlMetric", "ddlMetricOnNew();", true);
        //    ScriptManager.RegisterStartupScript(this, GetType(), "on insert", "onRowInsert();", true);
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                logInUser = (Employee)ViewState[curUser];
                DateTime modifiedTime = DateTime.Now;
                List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
                string currency = null;
                if (!ddlCurrency.SelectedItem.Text.Contains("None"))
                    currency = ddlCurrency.SelectedItem.Value;

                double exchangeRate;
                if (!double.TryParse(txtExchangeRate.Text.Replace(",", ""), out exchangeRate))
                    exchangeRate = 0;

                string requester = ((Employee)ViewState[curUser]).EMP_ID;
                double total = items.Sum(x => (x.PRICE * x.QUANTITY));

                List<RelatedDetail> selectedItems = new List<RelatedDetail>();
                List<RelatedDetail> newItems = new List<RelatedDetail>();
                int action = int.Parse(ddlAction.SelectedItem.Value);

                if (ViewState[curState].ToString().Equals("new"))
                {
                    RequisitionObject req = new RequisitionObject(LblReqNo.Text, txtJobCode.Text, txtDesc.Text, txtReqRemark.Text,
                            requester, modifiedTime, currency, exchangeRate, ddlSumType.SelectedItem.Value, total, "");

                    bool isReqNoRight = false;
                    bool isReqGenerated = false;

                    int reqNo = 0;
                    if (LblReqNo.Text.Length > 0)
                        isReqGenerated = int.TryParse(LblReqNo.Text.Substring(LblReqNo.Text.IndexOf("-") + 1, 5), out reqNo);
                    if (Helper.RequisitionHelper.getNextReqNo() == reqNo && isReqGenerated)
                        isReqNoRight = true;

                    if (isReqNoRight && isReqGenerated)
                    {
                        selectedItems = items.SelectMany(x => x.RELATED_DETAIL).Where(y => y.ISADDITIONALITEM == 0).ToList();
                        newItems = items.SelectMany(x => x.RELATED_DETAIL).Where(y => y.ISADDITIONALITEM == 1).ToList();

                        int id = 0;
                        req.GA_REQ_NO = LblReqNo.Text;
                        req.DEPT_REQ_NO = LblReqNo.Text;

                        Helper.RequisitionHelper.insertRequisition(req, out id); // Insert requisition
                        if (id > 0)
                        {
                            req.ID_REQ = id;
                            ViewState[curSum] = req;
                            items = Helper.SummaryHelper.insertAdditionalItem(items, newItems, id);
                            Helper.SummaryHelper.insertSummaryToRequisition(items, id);
                            Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, id, Variable.ReqType.RequisitionRequest, Variable.Action.SubmitRequest,
                               Variable.Status.PrepareRequest, txtComment.Text, exchangeRate, total, txtJobCode.Text);
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessSave", "showSuccessMsg();", true);
                        }
                    }
                    else if (!isReqNoRight && isReqGenerated)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "generate no req warning", "showMsg('WARNING!', 'GA Requisition No already used! Please, generate Requisition No. again.');", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "set On Edit", "setOnEdit();", true);
                    }
                    else if (!isReqGenerated)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "not generated yet", "showMsg('WARNING!', 'Please, generate GA Requisition No. first!');", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "set edit", "setOnEdit();", true);
                    }
                }
                else if (ViewState[curState].ToString().Equals("edit"))
                {
                    selectedItems = items.SelectMany(x => x.RELATED_DETAIL).Where(y => y.ISADDITIONALITEM == 0).ToList();
                    newItems = items.SelectMany(x => x.RELATED_DETAIL).Where(y => y.ISADDITIONALITEM == 1).ToList();

                    RequisitionObject req = (RequisitionObject)ViewState[curSum];
                    req.GA_REQ_NO = LblReqNo.Text;
                    req.DEPT_REQ_NO = LblReqNo.Text;
                    req.JOBCODE = txtJobCode.Text;
                    req.CURRENCY_CODE = currency;
                    req.EXCHANGE_RATE = exchangeRate;
                    req.DESCRIPTION = txtDesc.Text;
                    req.REMARKS = txtReqRemark.Text;
                    req.TOTAL_EST_AMOUNT = total;

                    Helper.SummaryHelper.updateSummaryItem(req.ID_REQ, items);
                    Helper.RequisitionHelper.updateRequisition(req);
                    Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, req.ID_REQ, req.REQ_TYPE, action, req.ACTIVITY_ID, txtComment.Text,
                    req.EXCHANGE_RATE, req.TOTAL_EST_AMOUNT, req.JOBCODE);
                    ScriptManager.RegisterStartupScript(this, GetType(), "setOnView after saving edit", "setOnView();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessEdit", "showSuccessMsg();", true);
                }
                else if (ViewState[curState].Equals("view"))
                {
                    RequisitionObject req = (RequisitionObject)ViewState[curSum];
                    int actionID = int.Parse(ddlAction.SelectedItem.Value);
                    Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, req.ID_REQ, req.REQ_TYPE, actionID, req.ACTIVITY_ID, txtComment.Text,
                        req.EXCHANGE_RATE, req.TOTAL_EST_AMOUNT, req.JOBCODE);
                    ScriptManager.RegisterStartupScript(this, GetType(), "setOnView", "setOnView();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessEdit", "showSuccessMsg();", true);
                }
            }
            catch (Exception sExp)
            {
                onEditState();
                ScriptManager.RegisterStartupScript(this, GetType(), "load on New", "ddlMetricOnNew();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showMsg('Error','" + HttpUtility.HtmlEncode(sExp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            }
        }

        private void addMergedItem(int idReq, int idReqDetail, string reqNo, int idItem, string itemName, string itemCode, int typeID, string typeName, double quantity, double addQuantity, string metric,
            double price, string remark, int isAdditional)
        {
            if (reqNo.Equals(string.Empty) || reqNo == null)
                reqNo = "Additional Item";

            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            if (items.Where(x => x.PART_ID == idItem).Any()) //if item already exist
            {
                SummaryItems item = items.FirstOrDefault(x => x.PART_ID == idItem);
                item.QUANTITY += quantity + addQuantity;
                item.RELATED_DETAIL.Add(new RelatedDetail(idReq, idReqDetail, item.ID_REQ_DETAIL, reqNo, idItem, itemName, quantity, addQuantity, metric, price, remark, isAdditional));
            }
            else // item not exist
            {
                SummaryItems item = new SummaryItems(idItem, itemName, itemCode, typeID, typeName, quantity, metric, price, remark);
                item.RELATED_DETAIL.Add(new RelatedDetail(idReq, idReqDetail, 0, reqNo, idItem, itemName, quantity, addQuantity, metric, price, remark, isAdditional));
                items.Add(item);
            }
            lblTotalEst.Text = items.Sum(x => (x.PRICE * x.QUANTITY)).ToString();
            ViewState[mergeItem] = items;
        }

        private void updateMergedItem(int idReqDetail, int idItem, double quantity, double addQuantity, string metric,
            double price, string remark)
        {
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            if (items.Where(x => x.PART_ID == idItem).Any())
            {
                SummaryItems item = items.FirstOrDefault(x => x.PART_ID == idItem);
                RelatedDetail detail = item.RELATED_DETAIL.FirstOrDefault(x => x.ID_REQ_DETAIL == idReqDetail);
                detail.QUANTITY = quantity;
                detail.ADDITIONAL_QUANTITY = addQuantity;
                detail.METRIC = metric;
                detail.PRICE = price;
                detail.REMARK = remark;
            }
            ViewState[mergeItem] = items;
        }

        private void deleteMergedItem(int idReqDetail, int idItem)
        {
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            if (items.Where(x => x.PART_ID == idItem).Any())
            {
                SummaryItems item = items.FirstOrDefault(x => x.PART_ID == idItem);
                RelatedDetail detail = item.RELATED_DETAIL.FirstOrDefault(x => x.ID_REQ_DETAIL == idReqDetail);
                if (detail != null)
                {
                    item.QUANTITY -= (detail.QUANTITY + detail.ADDITIONAL_QUANTITY);
                    item.RELATED_DETAIL.RemoveAt(item.RELATED_DETAIL.FindIndex(x => x.ID_REQ_DETAIL == idReqDetail));
                }
                if (item.RELATED_DETAIL.Count <= 0)
                    items.RemoveAt(items.FindIndex(x => x.PART_ID == idItem));
            }
            ViewState[mergeItem] = items;
        }

        private void loadMergeItems()
        {
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            foreach (var i in items)
                i.QUANTITY = i.RELATED_DETAIL.Sum(x => (x.ADDITIONAL_QUANTITY + x.QUANTITY));
            gridMergedItem.DataSource = items;
            gridMergedItem.DataBind();
            if (items.Count == items.Count(x => x.ID_ADVANCE != 0))
            {
                btnAdvance.Visible = false;
                submitAdvance.Visible = false;
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "load ddlMetric merged", "ddlMetricMergedItem();", true);
        }

        protected void gridDetailItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gridMergedItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("SeeDetail"))
            {
                int idItem = int.Parse(e.CommandArgument.ToString());
                List<RelatedDetail> listDetail = (((List<SummaryItems>)ViewState[mergeItem]).Where(x => x.PART_ID == idItem).FirstOrDefault()).RELATED_DETAIL;
                gridDetailItem.DataSource = listDetail;
                gridDetailItem.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "open dialog detail item", "openDetailItem();", true);
            }
            else if (e.CommandName.Equals("SeeQuotation"))
            {
                int id = 0;
                if (int.TryParse(e.CommandArgument.ToString(), out id))
                    if (id > 0)
                        Response.Redirect("Quotation.aspx?ViewQuotation=" + id.ToString());

            }
            else if (e.CommandName.Equals("Delete Item"))
            {
                List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
                int idItem = int.Parse(e.CommandArgument.ToString());
                if (items.Where(x => x.PART_ID == idItem).Any())
                {
                    items.RemoveAt(items.FindIndex(x => x.PART_ID == idItem));
                    ViewState[mergeItem] = items;
                    loadMergeItems();
                    loadSelectedItem();
                    loadNewItem();
                    btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
                }
            }
            else if (e.CommandName.Equals("SeeAdvance"))
            {
                int id = 0;
                if (int.TryParse(e.CommandArgument.ToString(), out id))
                    if (id > 0)
                        Response.Redirect("http://jindsrv0202/RFAP/");
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            ViewState[curState] = "edit";
            onEditState();
        }

        protected void gridMergedItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = gridMergedItem.Rows[e.NewEditIndex];
            Label metric = row.FindControl("lblMetric") as Label;
            Label quantity = row.FindControl("lblQuantity") as Label;
            if (metric != null && quantity != null)
            {
                gridMergedItem.Columns[7].Visible = false;
                gridMergedItem.EditIndex = e.NewEditIndex;
                loadMergeItems();
                ScriptManager.RegisterStartupScript(this, GetType(), "metric load on Edit", "setDLLMetricOnEdit('gridMergedItem'," + quantity.Text + ", '" + metric.Text + "'," + e.NewEditIndex + ")", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "on merged item editing", "onRowEditing();", true);
                //script to diasble all button except update and cancel on this gridview onRowEditing()
            }
        }

        protected void gridMergedItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            double valQuantity, valPrice = 0;
            Label itemID = (Label)gridMergedItem.Rows[e.RowIndex].Cells[2].FindControl("lblItemID");
            DropDownList metric = (DropDownList)gridMergedItem.Rows[e.RowIndex].FindControl("ddlMetricSingular");
            TextBox remark = (TextBox)gridMergedItem.Rows[e.RowIndex].FindControl("txtRemarks");
            Label quantity = (Label)gridMergedItem.Rows[e.RowIndex].FindControl("lblQuantity");
            bool isQNumber = double.TryParse(quantity.Text.Replace(",", ""), out valQuantity);
            TextBox price = (TextBox)gridMergedItem.Rows[e.RowIndex].FindControl("txtPrice");
            bool isPNumber = double.TryParse(price.Text.Replace(",", ""), out valPrice);

            if (valQuantity > 1)
                metric = (DropDownList)gridMergedItem.Rows[e.RowIndex].FindControl("ddlMetricPlural");

            if (itemID != null && metric != null && remark != null && quantity != null && isQNumber && isPNumber)
            {
                List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
                SummaryItems item = items.FirstOrDefault(x => x.PART_ID == int.Parse(itemID.Text));

                if (item != null)
                {
                    item.METRIC = metric.SelectedItem.Text;
                    item.PRICE = valPrice;
                    item.REMARK = remark.Text;
                }

                ViewState[mergeItem] = items;
                gridMergedItem.EditIndex = -1;
                loadMergeItems();
                btnChangeTotalEst_Click(btnChangeTotalEst, new EventArgs());
            }
            gridMergedItem.Columns[7].Visible = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "on merged item updating", "onRowCompleteEditing();", true);

        }

        protected void gridMergedItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridMergedItem.Columns[7].Visible = true;
            gridMergedItem.EditIndex = -1;
            loadMergeItems();
            ScriptManager.RegisterStartupScript(this, GetType(), "on merged item cancel", "onRowCompleteEditing();", true);
        }

        protected void btnResetJobCodeState_Click(object sender, EventArgs e)
        {
            ViewState[jobCodeState] = "";
            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);

        }

        protected void btnReloadPage_Click(object sender, EventArgs e)
        {
            reloadPage();
        }

        private void reloadPage()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curSum];
            if (req.ID_REQ > 0)
                Response.Redirect("ReqSummary.aspx?ViewSummary=" + req.ID_REQ.ToString());
            Response.Redirect(Request.RawUrl);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (ViewState[curState].ToString().Equals("new"))
                Response.Redirect("RequisitionTracking.aspx");
            else
                reloadPage();
        }

        protected void btnSaveAdditional_Click(object sender, EventArgs e)
        {
            int idItem = 0, typeID = 0;
            double quantity = 0, price = 0;
            bool isIdItemINT = int.TryParse(txtItemID.Text, out idItem);
            bool isTypeIDINT = int.TryParse(txtPartTypeID.Text, out typeID);
            bool isQuantityFloat = double.TryParse(txtNewQuantity.Text.Replace(",", ""), out quantity);
            bool isPriceFloat = true;
            string metric = "";
            if (!string.IsNullOrEmpty(txtNewPrice.Text))
                isPriceFloat = double.TryParse(txtNewPrice.Text.Replace(",", ""), out price);

            if (quantity > 1)
                metric = ddlMetricPlural.SelectedItem.Text;
            else
                metric = ddlMetricSingular.SelectedItem.Text;

            if (isIdItemINT && isTypeIDINT && isQuantityFloat && isPriceFloat && !metric.Contains("-- Select"))
            {
                if (!(isItemExist(idItem)))
                {
                    addMergedItem(0, 0, "", idItem, txtNewItem.Text, txtItemCode.Text, typeID, txtPartTypeName.Text, quantity, 0,
                     metric, price, txtNewRemark.Text, 1);
                    gridItem.ShowFooter = false;
                    loadNewItem();
                    loadMergeItems();
                    ScriptManager.RegisterStartupScript(this, GetType(), "close dialog detail", "closeDialogAdditional();", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "insert item failed", "showMsg('Error!','Can not add item! Item already added!')", true);
            }
            else
            {
                loadNewItem();
                loadMergeItems();
                ScriptManager.RegisterStartupScript(this, GetType(), "insert item failed", "showMsg('Error!','Invalid input! Please check your input')", true);
            }
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        protected void btnGenerateGAReqNo_Click(object sender, EventArgs e)
        {
            int reqNo = Helper.RequisitionHelper.getNextReqNo();
            LblReqNo.Text = "REQ-" + reqNo.ToString().PadLeft(5, '0') + "/" + DateTime.Now.Year.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCurrencyEst.Text = ddlCurrency.SelectedItem.Value;
        }

        protected void btnChangeTotalEst_Click(object sender, EventArgs e)
        {
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            lblTotalEst.Text = items.Sum(x => (x.PRICE * x.QUANTITY)).ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        private void exportVehicleReportToPDF()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curSum];
            try
            {
                ReportDocument report = new ReportDocument();
                Stream Stream;
                var connectionInfo = new System.Data.SqlClient.SqlConnectionStringBuilder(Connection.PurchaseSysConn);
                string Template = Server.MapPath("~/Report/RequisitionNoPrice.rpt");
                report.Load(Template);
                report.SetDatabaseLogon("sa", "m1c4system",
                              "172.16.17.41", "PurchaseSystem");
                //idReq
                ParameterValues idReqValues;
                ParameterDiscreteValue idReqDV = new ParameterDiscreteValue();
                ParameterFieldDefinition idReqFD = report.DataDefinition.ParameterFields["@idReq"];
                idReqDV.Value = req.ID_REQ;
                idReqValues = idReqFD.CurrentValues;
                idReqValues.Add(idReqDV);
                idReqFD.ApplyCurrentValues(idReqValues);

                //reqType
                ParameterValues reqTypeValues;
                ParameterDiscreteValue reqTypeDV = new ParameterDiscreteValue();
                ParameterFieldDefinition reqTypeFD = report.DataDefinition.ParameterFields["@reqType"];
                reqTypeDV.Value = req.REQ_TYPE;
                reqTypeValues = reqTypeFD.CurrentValues;
                reqTypeValues.Add(reqTypeDV);
                reqTypeFD.ApplyCurrentValues(reqTypeValues);


                CrystalDecisions.CrystalReports.Engine.TextObject AppField;
                AppField = report.ReportDefinition.ReportObjects["AppField"] as TextObject;
                AppField.Text = ApprovalHelper.getApprovalListReport(req.ID_REQ, Variable.ReqType.RequisitionRequest);

                CrystalDecisions.CrystalReports.Engine.TextObject ReqNoField;
                ReqNoField = report.ReportDefinition.ReportObjects["ReqNoField"] as TextObject;
                ReqNoField.Text = req.GA_REQ_NO;

                CrystalDecisions.CrystalReports.Engine.TextObject DateField;
                DateField = report.ReportDefinition.ReportObjects["DateField"] as TextObject;
                DateField.Text = req.DATE_CREATED.ToString("dd-MMM-yyyy");

                CrystalDecisions.CrystalReports.Engine.TextObject JobCodeField;
                JobCodeField = report.ReportDefinition.ReportObjects["JobCodeField"] as TextObject;
                JobCodeField.Text = req.JOBCODE;

                CrystalDecisions.CrystalReports.Engine.TextObject FromField;
                FromField = report.ReportDefinition.ReportObjects["FromField"] as TextObject;
                FromField.Text = req.REQ_BY_NAME;

                CrystalDecisions.CrystalReports.Engine.TextObject DeptField;
                DeptField = report.ReportDefinition.ReportObjects["DeptField"] as TextObject;
                DeptField.Text = req.REQ_BY_DEPT;//.Substring(req.REQ_BY_DEPT.IndexOf("-")+1);

                CrystalDecisions.CrystalReports.Engine.TextObject AdditioanlInfoField;
                AdditioanlInfoField = report.ReportDefinition.ReportObjects["AdditionalInfoField"] as TextObject;
                AdditioanlInfoField.Text = req.DESCRIPTION;

                


                Stream = report.ExportToStream(ExportFormatType.PortableDocFormat);
                report.Close();
                report.Dispose();

                int Length = (int)Stream.Length;
                byte[] Buffer = new byte[Length];
                Stream.Read(Buffer, 0, Length);
                Stream.Close();

                string reportName = req.GA_REQ_NO.Replace("/", "-").Replace("\\", "-");

                ViewState[flName] = reportName;
                ViewState[dlFile] = Buffer;
            }
            catch (Exception e)
            {
                onViewState();
                ScriptManager.RegisterStartupScript(this, GetType(), "errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(e.Message) + "')", true);
            }

        }

        protected void linkReport_Click(object sender, EventArgs e)
        {
            exportVehicleReportToPDF();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "download file", "downloadFile();", true);
        }

        protected void gridSelectedItem_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridSelectedItem.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label price = (Label)row.FindControl("lblPrice");
                    if (price != null)
                        price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text.Replace(",", "")));

                    Label quantity = (Label)row.FindControl("lblQuantity");
                    if (quantity != null)
                        quantity.Text = string.Format("{0:#,##0.00}", double.Parse(quantity.Text.Replace(",", "")));
                    
                }
            }
        }

        protected void gridItem_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridItem.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label price = (Label)row.FindControl("lblPrice");
                    if (price != null)
                        price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text.Replace(",", "")));

                    Label quantity = (Label)row.FindControl("lblQuantity");
                    if (quantity != null)
                        quantity.Text = string.Format("{0:#,##0.00}", double.Parse(quantity.Text.Replace(",", "")));
                }
            }
        }

        protected void gridMergedItem_PreRender(object sender, EventArgs e)
        {
            bool isAnyAdvance = false, isAnyQuo = false;

            foreach (GridViewRow row in gridMergedItem.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label price = (Label)row.FindControl("lblPrice");
                    if (price != null)
                        price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text.Replace(",", "")));

                    Label quantity = (Label)row.FindControl("lblQuantity");
                    if (quantity != null)
                        quantity.Text = string.Format("{0:#,##0.00}", double.Parse(quantity.Text.Replace(",", "")));

                    var linkQuo = (LinkButton)row.FindControl("linkQuotation");
                    var linkAdv = (LinkButton)row.FindControl("linkAdvance");
                    if (linkQuo != null)
                        if (linkQuo.Text.Any(c => char.IsDigit(c)))
                            isAnyQuo = true;
                    if (linkAdv != null)
                        if (linkAdv.Text.Any(c => char.IsDigit(c)))
                            isAnyAdvance = true;

                    CheckBox chkRow = (row.FindControl("checkAdvance") as CheckBox);
                    if (chkRow != null)
                    {
                        bool chkVisible = !(linkAdv.Text.Any(c => char.IsDigit(c)));
                        chkRow.Visible = chkVisible;
                    }

                    gridMergedItem.Columns[9].Visible = isAnyAdvance;
                    gridMergedItem.Columns[8].Visible = isAnyQuo;

                }
            }
        }

        protected void btnAdvance_Click(object sender, EventArgs e)
        {
            gridMergedItem.Columns[11].Visible = true;
            submitAdvance.Visible = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "set Jquery button", "setButton();", true);
        }

        protected void btnSubmitToAdvance_Click(object sender, EventArgs e)
        {
            List<SummaryItems> itemAdvance = new List<SummaryItems>();
            List<SummaryItems> items = (List<SummaryItems>)ViewState[mergeItem];
            RequisitionObject req = (RequisitionObject)ViewState[curSum];
            logInUser = (Employee)ViewState[curUser];
            int idAdvance = 0;
            foreach (GridViewRow row in gridMergedItem.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[8].FindControl("checkAdvance") as CheckBox);
                    if (chkRow != null)
                    {
                        if (chkRow.Checked)
                        {
                            if (idAdvance == 0)
                                idAdvance = RequisitionHelper.insertIntoAdvance(req.ID_REQ,logInUser.EMP_ID, req.JOBCODE);

                            if (idAdvance > 0)
                            {
                                int idItem = 0;
                                Label lblID = (row.Cells[2].FindControl("lblItemID") as Label);
                                if (lblID != null)
                                    if (int.TryParse(lblID.Text, out idItem))
                                    {
                                        SummaryItems item = items.FirstOrDefault(x => x.PART_ID == idItem);
                                        itemAdvance.Add(item);
                                    }
                            }
                        }
                    }
                }
            }

            List<RequisitionItems> convertedItems = new List<RequisitionItems>();
            foreach (var i in itemAdvance)
            {
                RequisitionItems item = new RequisitionItems(i.ID_REQ_DETAIL, i.PART_ID, i.PART_CODE, i.PART_NAME, i.TYPE_ID, i.TYPE_NAME, i.QUANTITY, i.METRIC, i.PRICE, i.REMARK);
                convertedItems.Add(item);
            }

            RequisitionHelper.updateReqDetailAdvance(convertedItems, idAdvance);
            items = Helper.SummaryHelper.getSummaryItems(req.ID_REQ);
            ViewState[mergeItem] = items;
            loadMergeItems();
            ScriptManager.RegisterStartupScript(this, GetType(), "set Jquery button", "setButton();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
                RequisitionObject req = (RequisitionObject)ViewState[curSum];

                string reportName = ViewState[flName].ToString();

                byte[] Buffer = (byte[])ViewState[dlFile];

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + reportName + ".pdf");
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnDownload errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Thread.Sleep(1);
            }

        }

    }
}