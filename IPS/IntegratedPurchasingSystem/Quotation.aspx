﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Quotation.aspx.cs" Inherits="IntegratedPurchasingSystem.Quotation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery.min.js"></script>
    <script src="Script/jquery-ui.min.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/jquery.ui.timepicker.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/PageScript/quotationPage.js"></script>
    <script src="Script/autoNumeric.js"></script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off" enctype="MULTIPART/FORM-DATA">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Quotation&nbsp; Data
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                                <table class="contentForm" contenteditable="false">
                                    <tr class="hideOnCreateNew">
                                        <td class="contentFormField">GA Requisition No</td>
                                        <td class="contentFormData">
                                            <asp:LinkButton ID="linkGAReq" runat="server" OnClick="linkGAReq_Click"></asp:LinkButton>&nbsp
                                            <asp:LinkButton ID="linkSelectReq" runat="server" OnClick="linkSelectReq_Click">Select Requisition</asp:LinkButton>
                                        </td>
                                        <td class="contentFormField">Date Created</td>
                                        <td class="contentFormData">
                                            <asp:Label ID="lblDateCreated" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Request By</td>
                                        <td class="contentFormData">
                                            <asp:Label ID="lblReqBy" runat="server" Text=""></asp:Label></td>
                                        <td class="contentFormField">Department
                                        </td>
                                        <td class="contentFormData">
                                            <asp:Label ID="lblDepartment" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">JobCode</td>
                                        <td class="contentFormData">
                                            <div id="Div2" class="onView" runat="server">
                                                <asp:Label ID="lblJobCode" runat="server" Text=""></asp:Label>
                                            </div>
                                        </td>
                                        <td class="contentFormField">Project Name</td>
                                        <td class="contentFormData">
                                            <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Requsted Item</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtReqItem" runat="server" CssClass="validate[required]"></asp:TextBox>
                                            <asp:Label ID="lblReqItem" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="contentFormField">Requestor</td>
                                        <td class="contentFormData">
                                            <asp:DropDownList ID="ddlRequestor" runat="server" CssClass="validate[required]">
                                            </asp:DropDownList><br />
                                            <asp:TextBox ID="txtRequestor" runat="server" CssClass="hidden validate[required]"></asp:TextBox>
                                            <asp:Label ID="lblRequestor" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField" style="font-weight: bold">Status</td>
                                        <td class="contentFormData" style="font-weight: bold">
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></td>
                                        <td class="contentFormField">
                                            <asp:Label ID="lblDoc" runat="server" Text="Document"></asp:Label></td>
                                        <td class="contentFormData">
                                            <asp:LinkButton ID="linkPrint" runat="server">Print Quotation</asp:LinkButton>
                                        </td>
                                    </tr>

                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnGetSelectedItem" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <table runat="server" id="divPO">
                            <tr>
                                <td class="contentFormField">Purchase Order</td>
                                <td class="contentFormData">
                                    <asp:LinkButton ID="linkPO" runat="server" OnClick="linkPO_Click" Text=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Quotation Item
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail" style="padding: 20px;">
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView CssClass="mGrid"
                                        ID="gridItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                        ShowHeaderWhenEmpty="true" OnPreRender="gridItem_PreRender">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Item Name" DataField="PART_NAME" />
                                            <asp:BoundField HeaderText="Item ID" DataField="PART_ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                            <asp:TemplateField HeaderText="Quantity">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Metric" DataField="METRIC" />
                                            <asp:TemplateField HeaderText="Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Currency" DataField="CURRENCY" />
                                            <asp:BoundField HeaderText="Remark" DataField="REMARK" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnGetSelectedItem" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Vendor
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <div style="padding: 20px 20px 0px;">
                            <asp:Button ID="btnAddVendor" runat="server" Text="Add Vendor" />
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView CssClass="mGrid"
                                            ID="gridSelectedVendor" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                            ShowHeaderWhenEmpty="True" OnRowCommand="gridSelectedVendor_RowCommand" OnPreRender="gridSelectedVendor_PreRender">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ID Detail" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIDDetail" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VENDOR_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor ID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVendorID" runat="server" Text='<%# Eval("ID_VENDOR")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quotation No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuotNo" runat="server" Text='<%# Eval("QUOT_NO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("QUOT_DATE", "{0:dd MMM yyyy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Technical Aspect">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTechAspect" runat="server" Text='<%# Eval("TECHNICAL_ASPECT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency">
                                                    <ItemTemplate>
                                                        <span><%#Eval("CURRENCY") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exchange Rate">
                                                    <ItemTemplate>
                                                        <span><%#Eval("EXCHANGE_RATE") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quoted Price">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuoPrice" runat="server" Text='<%# Eval("QUO_PRICE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Discount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDisc" runat="server" Text='<%# Eval("DISCOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="VAT 10%">
                                                    <ItemTemplate>
                                                        <span><%# Eval("VAT") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Net Price">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNetPrice" runat="server" Text='<%# Eval("NET_PRICE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivery Time">
                                                    <ItemTemplate>
                                                        <span><%# Eval("DELIVERY_TIME") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Warranty">
                                                    <ItemTemplate>
                                                        <span><%# Eval("WARRANTY") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment Terms">
                                                    <ItemTemplate>
                                                        <span><%# Eval("PAYMENT_TERMS") %></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("ID_VENDOR")%>'
                                                            Text="Edit" CommandName="Edit Vendor"></asp:LinkButton>
                                                        <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("ID_VENDOR")%>'
                                                            Text="Delete" CommandName="Delete Vendor" OnClientClick="return confirm('Do you want to delete this record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="btnSaveDetail" EventName="Click" />--%>
                                        <asp:AsyncPostBackTrigger ControlID="gridSelectedVendor" EventName="RowCommand" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <table class="contentForm">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="contentFormField" style="vertical-align: top">Attachment</td>
                                            <td class="contentFormData">
                                                <div id="uploadDiv" class="onEdit" runat="server">
                                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                        <ContentTemplate>
                                                            <asp:FileUpload ID="fileAttachmentUpload" runat="server" Width="300px" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnSubmit" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <br />
                                                    <ul class="remark">
                                                        <li>Please put all needed attachment to one PDF file</li>
                                                        <li>File limit is 20MB</li>
                                                    </ul>
                                                </div>
                                                <asp:LinkButton ID="linkPDF" runat="server"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentFormField">Recommended Vendor</td>
                                            <td class="contentFormData">
                                                <div id="recommVendorOnEdit" runat="server">
                                                    <asp:UpdatePanel ID="updatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlRecommVendor" runat="server" CssClass="validate[required]"></asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveDetail" EventName="Click" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div id="recommVendorOnView" runat="server">
                                                    <asp:Label ID="lblrecommVendor" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentFormField">Quotation Remark</td>
                                            <td class="contentFormData">
                                                <div id="Div11" class="onEdit" runat="server">
                                                    <asp:TextBox ID="txtQuoRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                                <div id="Div12" class="onView" runat="server">
                                                    <asp:Label ID="lblQuoRemark" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div id="actionHistory" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action History
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnHiddenAction" runat="server" Text="Button" CausesValidation="false" Style="display: none;" />
                                    <asp:GridView ShowHeaderWhenEmpty="true"
                                        CssClass="mGrid" ID="gridActionHistory" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Participant" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("PARTICIPANT")%> - <%# Eval("PARTICIPANT_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Completed By" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("COMPLETED_BY")%> - <%# Eval("COMPLETED_BY_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Activity" DataField="ACTIVITY_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Action" DataField="ACTION_TEXT" />
                                            <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%#  Eval("START_DATE", "{0:dd MMM yyyy - HH:mm }") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Convert.ToString(Eval("END_DATE")).Contains("1/1/1975")?"":Eval("END_DATE", "{0:dd MMM yyyy - HH:mm}") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Comment" DataField="COMMENT" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnHiddenAction" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <div id="actionDiv" runat="server">
                            <div class="contentPanel">
                                <div class="contentPanelHeader">
                                    <div class="contentPanelText">
                                        Action 
                                    </div>
                                    <div class="contentPanelExpand">
                                    </div>
                                </div>
                                <div class="contentPanelDetail" style="padding: 20px;">
                                    <table>
                                        <tr>
                                            <td>
                                                <table class="onEdit">
                                                    <tr id="actionField" runat="server">
                                                        <td class="contentFormField">Action</td>
                                                        <td class="contentFormData">
                                                            <asp:DropDownList ID="ddlAction" runat="server" CssClass="validate[required]">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentFormField">Comment</td>
                                                        <td class="contentFormData">
                                                            <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentFormField"></td>
                                                        <td class="contentFormData">
                                                            <asp:Button ID="btnSubmit" runat="server" Text="Execute" OnClick="btnSubmit_Click" />

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGetSelectedItem" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <div style="display: none">
                    <div id="dialogVendor">
                        <div>
                            <table>
                                <tr>
                                    <td class="contentFormField">Search Vendor By</td>
                                    <td class="contentFormData">
                                        <asp:DropDownList ID="ddlVendor" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Address"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:TextBox ID="txtSearchVendor" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:Button ID="btnSearchVendor" runat="server" Text="Search" OnClick="btnSearchVendor_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPaneVendor" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10"
                                            ID="gridVendorSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridVendorSearch_SelectedIndexChanged" OnPageIndexChanging="gridVendorSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="vendorID" HeaderText="Vendor ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="vendorCD" HeaderText="Vendor Code" />
                                                <asp:BoundField DataField="vndName" HeaderText="Vendor Name" />
                                                <asp:BoundField DataField="vndAddress" HeaderText="Address" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchVendor" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogDetail">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <div style="display: none;">
                                    <asp:Button ID="btnGeneratePrice" runat="server" Text="" OnClick="btnGeneratePrice_Click" UseSubmitBehavior="false" />
                                    <%--<asp:Button ID="btnGenerateVAT" runat="server" Text="Button" OnClick="btnGenerateVAT_Click" />--%>
                                </div>
                                <table class="contentForm" style="text-align: left;">
                                    <tr style="display: none;">
                                        <td>Dialog Status</td>
                                        <td>
                                            <asp:TextBox ID="txtDetailStatus" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>ID </td>
                                        <td>
                                            <asp:TextBox ID="txtIDDetail" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>ID Vendor</td>
                                        <td>
                                            <asp:TextBox ID="txtVendorID" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Vendor</td>
                                        <td class="contentFormData">
                                            <%--<asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                <ContentTemplate>--%>
                                            <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox>&nbsp
                                            <asp:Button ID="btnVendor" runat="server" Text=" ... " />&nbsp
                                            <asp:Button ID="btnResetVendor" runat="server" Text="Reset" />
                                            <%--</ContentTemplate>
                                                <Triggers>--%>

                                            <%--</Triggers>
                                            </asp:UpdatePanel>--%>
                                        </td>
                                        <td class="contentFormField">Quotation No.</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtQuotationNo" runat="server"></asp:TextBox>
                                            <%--<div class="remark">* Can be empty</div>--%>
                                            <asp:LinkButton ID="btnNoQuote" runat="server" OnClick="btnNoQuote_Click">No Quote</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Date</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtQuoDate" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="contentFormField">Technical Aspect</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtTechAspect" runat="server" TextMode="MultiLine" Width="200"></asp:TextBox><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Quoted Price</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtQuoPrice" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="contentFormField">Discount</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtDiscount" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">VAT</td>
                                        <td class="contentFormData">
                                            <asp:DropDownList ID="ddlVAT" runat="server">
                                                <asp:ListItem Text="-- Select Option --" Value="" />
                                                <asp:ListItem Text="No VAT" Value="No VAT" />
                                                <asp:ListItem Text="Include" Value="Include" />
                                                <asp:ListItem Text="Exclude" Value="Exclude" />
                                            </asp:DropDownList><br />

                                            <%-- <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtVATAmount" runat="server" ></asp:TextBox>
                                                </ContentTemplate>      
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnGenerateVAT" />
                                                </Triggers>
                                            </asp:UpdatePanel>--%>
                                            
                                        </td>
                                        <td class="contentFormField">Net Price</td>
                                        <td class="contentFormData">
                                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="lblNetPrice" runat="server" Text=""></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnGeneratePrice" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Currency</td>
                                        <td class="contentFormData">
                                            <asp:DropDownList ID="ddlCurrency" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="contentFormField">Exchange Rate</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtExchangeRate" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Delivery Time</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtDeliveryTime" runat="server"></asp:TextBox>&nbsp day/days
                                        </td>
                                        <td class="contentFormField">Payment Terms</td>
                                        <td class="contentFormData">
                                            <asp:DropDownList ID="ddlPaymentTerms" runat="server">
                                                <asp:ListItem Text="-- Select Option --" Value="" />
                                                <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery" />
                                                <asp:ListItem Text="14 days" Value="14 days" />
                                                <asp:ListItem Text="30 days" Value="30 days" />
                                                <asp:ListItem Text="Others" Value="Others" />
                                            </asp:DropDownList>
                                            <br />
                                            <asp:TextBox ID="txtPayTermOthers" runat="server" CssClass="hidden"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormField">Warranty</td>
                                        <td class="contentFormData">
                                            <asp:TextBox ID="txtWarranty" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="contentFormField"></td>
                                        <td class="contentFormData"></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnNoQuote" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="gridVendorSearch" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                        <div style="width: 100%; text-align: center">
                            <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click" />
                            &nbsp<asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" />
                        </div>
                    </div>

                    <div id="dialogRequisition" style="height: auto; min-height: 230px;">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormField">Requisition No.</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtReqNo" runat="server"></asp:TextBox>
                                </td>
                                <td class="contentFormField">Month Created</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtJobCodeReq" runat="server"></asp:TextBox></td>
                                            <td style="padding: 0px 5px 0px 0px;">
                                                <asp:Button ID="btnJobCodeReq" runat="server" Text="..." />
                                            </td>
                                            <td style="padding: 0px 5px 0px 0px;">
                                                <asp:Button ID="btnResetJobCodeReq" runat="server" Text="Reset" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="contentFormField">Request Type</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlReqType" runat="server">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Stationary Requisition" Value="ST"></asp:ListItem>
                                        <asp:ListItem Text="General Requisition" Value="RS"></asp:ListItem>
                                        <asp:ListItem Text="IT Equipment Requisition" Value="SF"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Contain Item</td>
                                <td class="contentFormData">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtItemID" runat="server" CssClass="hidden"></asp:TextBox>
                                                        <asp:TextBox ID="txtItem" runat="server"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="gridItemSearch" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="padding: 0px 5px 0px 0px;">
                                                <asp:Button ID="btnItem" runat="server" Text="..." /></td>
                                            <td style="padding: 0px 5px 0px 0px;">
                                                <asp:Button ID="btnResetItem" runat="server" Text="Reset" /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="contentFormField"></td>
                                <td class="contentFormData"></td>
                            </tr>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnSearchReq" runat="server" Text="Search" OnClick="btnSearchReq_Click" />
                        </div>
                        <br />
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:GridView AllowPaging="True" PageSize="5" OnPageIndexChanging="gridRequisition_PageIndexChanging"
                                        ID="gridRequisition" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                        OnSelectedIndexChanged="gridRequisition_SelectedIndexChanged" OnPreRender="gridRequisition_PreRender">
                                        <Columns>
                                            <asp:BoundField runat="server" HeaderText="ID" DataField="ID_REQ" HeaderStyle-CssClass="hidden" ControlStyle-CssClass="hidden"
                                                ItemStyle-CssClass="hidden" />
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Requisition No." DataField="DEPT_REQ_NO" />
                                            <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE" />
                                            <asp:BoundField runat="server" HeaderText="Type" DataField="REQ_TYPE_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Date Created" DataField="DATE_CREATED" DataFormatString="{0:dd-MMM-yyyy}" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearchReq" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                    <div id="dialogItem">
                        <div>
                            <table>
                                <tr>
                                    <td>Search Item By</td>
                                    <td>
                                        <asp:DropDownList ID="ddlItem" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Type"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormField">
                                        <asp:TextBox ID="txtSearchItem" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlItemType" runat="server"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSearchItem" runat="server" Text="Search" OnClick="btnSearchItem_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPanelItem" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridItemSearch_PageIndexChanging"
                                            ID="gridItemSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridItemSearch_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PART_ID" HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="PART_CODE" HeaderText="Item Code" />
                                                <asp:BoundField DataField="PART_NAME" HeaderText="Item Name" />
                                                <asp:BoundField DataField="TYPE_ID" HeaderText="ID Type" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchItem" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogItemReq">
                        <div style="display: none;">
                            <asp:Label ID="lblIDReq" runat="server" Text=""></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridItemReq_PageIndexChanging" CellPadding="2"
                                    ID="gridItemReq" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                    OnPreRender="gridItemReq_PreRender">
                                    <Columns>
                                        <asp:BoundField runat="server" HeaderText="ID Req" DataField="ID_REQ" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="ID" DataField="ID_DETAIL" HeaderStyle-CssClass="hidden"
                                            ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="Item ID" DataField="PART_ID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField runat="server" HeaderText="Item Name" DataField="PART_NAME" />
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <%# Eval("QUANTITY") %>  <%# Eval("METRIC") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField runat="server" HeaderText="Quotation" DataField="ID_QUO" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="Advance" DataField="ID_ADVANCE" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="Remark">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemarkItem" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="checkItem" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div style="float: right; padding: 20px;">
                            <asp:Button ID="btnGetSelectedItem" runat="server" Text="Submit" OnClick="btnGetSelectedItem_Click" CausesValidation="false" />
                            <input type="button" id="btnCancelItemReq" value="Cancel" />
                        </div>
                    </div>

                    <div id="dialogJobCode">
                        <table class="contentForm">
                            <tr>
                                <td style="min-width: 80px;">Search By</td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCode" runat="server">
                                        <asp:ListItem Text="JobCode"></asp:ListItem>
                                        <asp:ListItem Text="Project Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchJobCode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearchJobCode" runat="server" Text="Search" OnClick="btnSearchJobCode_Click" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridJobCode_PageIndexChanging" CellPadding="2"
                                            ID="gridJobCode" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridJobCode_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE_ID" />
                                                <asp:BoundField runat="server" HeaderText="Project Name" DataField="PROJECT_NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogPrint">
                        <table>
                            <tr>
                                <td class="contentFormField">Document Format
                                </td>
                                <td class="contentFormData">
                                    <asp:RadioButtonList ID="rdoFormat" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="PDF" Value="1" />
                                        <asp:ListItem Text="Excel" Value="2" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp
                                </td>
                                <td>
                                    <div style="padding-top: 20px;">
                                        <asp:Button ID="btnPrint" runat="server" Text="OK" OnClick="btnPrint_Click"  />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>

                    <div id="divSuccess">
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <asp:Button ID="btnHiddenSave" runat="server" Text="Button" CausesValidation="false" UseSubmitBehavior="false" />
                    <asp:Button ID="btnReloadPage" runat="server" Text="Button" OnClick="btnReloadPage_Click" UseSubmitBehavior="false" />
                    <asp:Button ID="btnDownloadDoc" runat="server" Text="Button" OnClick="btnDownloadDoc_Click" UseSubmitBehavior="false" />
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

