﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Setting.aspx.cs" Inherits="IntegratedPurchasingSystem.Setting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu').jqsimplemenu();
        });

        function showErrorMsg(titles, error) {

            $('#lblErrorMsg').text(error);
            $("#divError").dialog({
                title: titles,
                modal: true,
                resizable: false,
                width: 'auto',
                position: ['center'],
                create: function (event, ui) { }
            });
            $("#divError").parent().appendTo($("form:first"));
            return false;
        }

        $(function () {
            $("input[type=submit], button")
            .button()
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                     <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div style="width: 100%; margin: auto; text-align: center; padding: 30px;">
                    <div style="width: 550px; margin: auto; padding: 20px; background-color: #c3c1c1;">
                        You're Log In as
                            <asp:Label ID="lblLogIn" runat="server" Text=""></asp:Label>
                        <br />
                        <table>
                            <tr>
                                <td>Change User?</td>
                                <td>
                                    <asp:TextBox ID="txtChangeUser" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>or
                                    <asp:LinkButton ID="linkRelease" runat="server" OnClick="lnkRelease_Click">Release Impersonate</asp:LinkButton></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="divError" style="display: none;">
                    <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
