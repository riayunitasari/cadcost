﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem
{
    public partial class ViewPDF : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int idReq = 0;
                try
                {
                    logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                    if (int.TryParse(Request["ViewAttachment"], out idReq) && !Request["Type"].Equals(""))
                    {
                        string type = Request["Type"];
                        if (type.Equals("REQ"))
                        {
                            RequisitionObject req = Helper.RequisitionHelper.getRequisitionByID(idReq);
                            logInUser = Helper.GeneralHelper.isParticipant(logInUser, 2, req.ID_REQ, type);
                            if ((logInUser.ISADMIN || logInUser.ISPARTICIPANT) && req.ISDELETED == 0)
                                showPDF(req.ATTACHMENT);
                            else
                                Response.Redirect("NotAuthorized.aspx");
                        }
                        else
                        {
                            QuoObject req = Helper.QuotHelper.getQuotationByID(idReq);
                            logInUser = Helper.GeneralHelper.isParticipant(logInUser, 2, req.ID_QUO, type);
                            if ((logInUser.ISADMIN || logInUser.ISPARTICIPANT) && req.ISDELETED == 0)
                                showPDF(req.ATTACHMENT);
                            else
                                Response.Redirect("NotAuthorized.aspx");
                        }
                    }
                    else
                        Response.Redirect("NotAuthorized.aspx");
                }
                catch(Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
                }
            }
        }

        private void showPDF(string fileName)
        {
            string AttachmentDir = Server.MapPath("/IPS/PDF");
            AttachmentDir = AttachmentDir + "\\" + fileName;
            //string FilePath = Server.MapPath("");
            WebClient User = new WebClient();
            Byte[] FileBuffer = User.DownloadData(AttachmentDir);

            if (FileBuffer != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", FileBuffer.Length.ToString());
                Response.BinaryWrite(FileBuffer);
            }
        }
    }
}