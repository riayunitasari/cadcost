﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Item.aspx.cs" Inherits="IntegratedPurchasingSystem.Item" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script src="Script/PageScript/itemPage.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Item
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <%--<div class="contentPanelDetail">
                        <div style="padding: 20px;">
                            <asp:Button ID="btnNewItem" runat="server" Text="New Item" OnClick="btnNewItem_Click" />
                            &nbsp
                        
                        </div>
                        <div>
                            <div style="padding: 0px 20px 20px;">
                                <div class="barSearch">
                                    <div class="divField">Search Item By</div>
                                    <div class="divData">
                                        <asp:DropDownList ID="ddlItem" runat="server">
                                            <asp:ListItem Text="-- Select --"></asp:ListItem>
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Category"></asp:ListItem>
                                            <asp:ListItem Text="Type"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="divData">
                                        <asp:TextBox ID="txtSearchItem" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlItemCategory" runat="server"></asp:DropDownList>
                                        <asp:DropDownList ID="ddlItemType" runat="server">
                                            <asp:ListItem Text="-- Select Type --" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Asset" Value="Asset"></asp:ListItem>
                                            <asp:ListItem Text="Consumable" Value="Consumable"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="divData">
                                        <asp:Button ID="btnSearchItem" runat="server" Text="Search" CausesValidation="false" OnClick="btnSearchItem_Click" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="uptPan" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="30" ShowHeaderWhenEmpty="true"
                                            CssClass="mGrid" OnRowCommand="gridItem_RowCommand"
                                            ID="gridItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gridItem_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ITEM_NAME" HeaderText="Item Name" />
                                                <asp:BoundField DataField="ITEM_ID" HeaderText="Item ID" />
                                                <asp:BoundField DataField="TYPE" HeaderText="Type" />
                                                <asp:BoundField DataField="CATEGORY" HeaderText="Category" />
                                                <asp:BoundField DataField="DESCRIPTION" HeaderText="Description" />
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="lbtnEdit" Text="Edit" runat="server" CommandName="Edit Item" title="Edit Item"
                                                            CommandArgument='<%#Eval("ITEM_ID") %>'></asp:LinkButton>

                                                        <asp:LinkButton ID="lbtnDelete" Text="Delete" runat="server" CommandName="Delete Item" title="Delete Item"
                                                            CommandArgument='<%#Eval("ITEM_ID") %>' OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchItem" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSaveItem" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnCancelItem" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>--%>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Category
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                   <%-- <div class="contentPanelDetail">
                        <div style="padding: 20px;">
                            <asp:Button ID="btnNewCategory" runat="server" Text="New Category Item" OnClick="btnNewCategory_Click" />
                        </div>
                        <div>
                            <div style="padding: 0px 20px 20px 20px;">
                                <div class="barSearch">
                                    <div class="divField">Search Category</div>

                                    <div class="divData">
                                        <asp:TextBox ID="txtSearchCategory" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="divData">
                                        <asp:Button ID="btnSearchCategory" runat="server" Text="Search" CausesValidation="false" OnClick="btnSearchCategory_Click" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="5" ShowHeaderWhenEmpty="true"
                                            CssClass="mGrid" OnPageIndexChanging="gridCategory_PageIndexChanging" OnRowCommand="gridCategory_RowCommand"
                                            ID="gridCategory" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CATEGORY_ITEM1" HeaderText="Category" />
                                                <asp:BoundField DataField="ID" HeaderText="Category ID" />
                                                <asp:BoundField DataField="DESCRIPTION" HeaderText="Description" />
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" Text="Edit" runat="server" CommandName="Edit Category" title="Edit Vehicle"
                                                            CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnDelete" Text="Delete" runat="server" CommandName="Delete Category" title="Delete Vehicle"
                                                            CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchCategory" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSaveCategory" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnCancelCategory" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>--%>
                </div>

                <%--<div style="display: none">
                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>

                     <div id="dialogItem">
                         <%--<div style="display:none"><asp:Button ID="btnHiddenSave" runat="server" Text="" OnClick="btnHiddenSave_Click" /></div>--%>
                            <div>
                                
                                        <table class="contentForm">
                                            <tr style="display:none;">
                                                <td>ID</td>
                                                <td>
                                                    <asp:TextBox ID="txtItemID" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td>
                                                    <asp:TextBox ID="txtItemName" runat="server" CssClass="validate[required]"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="validate[required]">
                                                        <asp:ListItem Text="-- Select Type --" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Asset" Value="Asset"></asp:ListItem>
                                                        <asp:ListItem Text="Consumable" Value="Consumable"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Category</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="validate[required]"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Descripton</td>
                                                <td>
                                                    <asp:TextBox ID="txtItemDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                <div style="width: 100%; text-align: center">
                                    <asp:Button ID="btnSaveItem" runat="server" Text="Save" OnClick="btnSaveItem_Click" />
                                    &nbsp
                            <asp:Button ID="btnCancelItem" runat="server" Text="Cancel" />
                                </div>
                                   
                            </div>
                        </div>


                    <div id="dialogCategory">
                            <div>
                                
                                        <table class="contentForm">
                                            <tr style="display:none;">
                                                <td>ID</td>
                                                <td>
                                                    <asp:TextBox ID="txtCategoryID" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td>
                                                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="validate[required]"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Descripton</td>
                                                <td>
                                                    <asp:TextBox ID="txtCategoryDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                <div style="width: 100%; text-align: center">
                                    <asp:Button ID="btnSaveCategory" runat="server" Text="Save" OnClick="btnSaveCategory_Click" />
                                    &nbsp
                            <asp:Button ID="btnCancelCategory" runat="server" Text="Cancel" />
                                </div>
                                   
                            </div>
                        </div>

                </div>--%>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
