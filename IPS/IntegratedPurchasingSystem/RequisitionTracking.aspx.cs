﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using System.Data;
using System.Drawing;
using System.IO;

namespace IntegratedPurchasingSystem
{
    public partial class PurchaseTracking : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        const string curReqs = "filtered_Requisitions";
        const string curItems = "filtered_Requisitions_Items";
        const string curQuos = "filtered_Quotations";
        List<RequisitionObject> filteredReq = new List<RequisitionObject>();
        List<QuoObject> filteredQuo = new List<QuoObject>();
        List<RequisitionItems> filteredItems = new List<RequisitionItems>();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            if (!IsPostBack)
            {
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;

                Helper.RequisitionHelper.setDDLYearForRequisition(ddlYear);
                btnSummaryReport.Visible = false;

                ViewState[curReqs] = filteredReq;
                ViewState[curQuos] = filteredQuo;

                if (!logInUser.ISADMIN)
                    onlyEmployee(logInUser.EMP_ID, logInUser.FULL_NAME, logInUser.SECT_NAME);
                else if (logInUser.ISADMIN)
                {
                    Helper.GeneralHelper.setDDLDepartment(ddlDept);
                    pnlEmployee.Attributes.Add("class", "hidden");
                    pnlEmployeeDept.Attributes.Add("class", "hidden");
                    btnSummaryReport.Visible = true;
                }
                lblDept.Text = logInUser.SECT_NAME;
                lblRequestedBy.Text = logInUser.EMP_ID + " - " + logInUser.FULL_NAME;

                loadRequisition();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridEmployee, this);
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            Helper.GeneralHelper.gridRender(gridItemSearch, this);
            base.Render(writer);
        }

        private void loadRequisition()
        {
            Employee user = (Employee)ViewState[curUser];
            string requester = txtRequestedBy.Text.Equals(string.Empty) ? "" : txtRequestedBy.Text.Substring(0, txtRequestedBy.Text.IndexOf(" - "));
            string dept= "";
            if (user.ISADMIN)
                dept = ddlDept.SelectedItem.Text;
            else
            {
                requester = lblRequestedBy.Text.Equals(string.Empty) ? "" : lblRequestedBy.Text.Substring(0, lblRequestedBy.Text.IndexOf(" - ")); ;
                dept = lblDept.Text;
            }

            int idItem = 0;
            bool isIDItem = int.TryParse(txtIDItem.Text, out idItem);
            filteredReq = new List<RequisitionObject>();
            filteredQuo = new List<QuoObject>();
            filteredItems = new List<RequisitionItems>();

            try
            {
                filteredReq = Helper.RequisitionHelper.getFilteredRequisition(int.Parse(ddlMonth.SelectedItem.Value),
                    int.Parse(ddlYear.SelectedItem.Text), ddlReqType.SelectedItem.Value, int.Parse(ddlFilterStatus.SelectedItem.Value),
                    requester, dept, txtJobCode.Text, user.EMP_ID, idItem);
                gridRequisition.DataSource = filteredReq;
                gridRequisition.DataBind();

                ViewState[curReqs] = filteredReq;


                ScriptManager.RegisterStartupScript(this, GetType(), "setButton", "setButton();", true);
            }
            catch (Exception erReq)
            {
                string er = HttpUtility.HtmlEncode(erReq.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        private void onlyEmployee(string empID, string empName, string deptName)
        {
            txtRequestedBy.Text = empID + " - " + empName;
            ddlDept.SelectedIndex = ddlDept.Items.IndexOf(ddlDept.Items.FindByText(deptName));

            pnlNonEmployee.Attributes.Add("class", "hidden");
            pnlNonEmployeeDept.Attributes.Add("class", "hidden");

        }

        protected void gridRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridRequisition.PageIndex = e.NewPageIndex;
            loadRequisition();
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            loadRequisition();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString()+"hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set button", "setButton() ;", true);
        }

        protected void gridRequisition_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("View"))
                {
                    string[] arg = new string[2];
                    arg = e.CommandArgument.ToString().Split(';');
                    if (arg[1].Contains(Variable.ReqType.SummaryStationary) || arg[1].Contains(Variable.ReqType.SummaryOthers))
                        Response.Redirect("ReqSummary.aspx?ViewSummary=" + arg[0]);
                    else
                        Response.Redirect("Requisition.aspx?ViewRequisition=" + arg[0]);
                }
                else if (e.CommandName.Equals("Delete Req"))
                {
                    string id = e.CommandArgument.ToString();
                    RequisitionObject req = Helper.RequisitionHelper.getRequisitionByID(int.Parse(id));
                    
                    string AttachmentDir = Server.MapPath("/IPS/PDF");
                    AttachmentDir = AttachmentDir + "\\" + req.ATTACHMENT;
                    FileInfo file = new FileInfo(AttachmentDir);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    Helper.RequisitionHelper.deleteRequisition(int.Parse(id));

                    loadRequisition();
                }
            }
            catch(Exception exp)
            {
                string er = HttpUtility.HtmlEncode(exp.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;

            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            txtJobCode.Text = jobcode;
            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "');", true);
        }

        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            try
            {
                gridJobCode.DataSource = Helper.PFNATTHelper.getAllJobCode(ddlJobCode.SelectedItem.Text, search);
                gridJobCode.DataBind();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            loadSearchResultJobCode();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set to center", "setCenter('dialogJobCode');", true);
        }

        private void loadEmployee()
        {
            string searchBy = ddlUser.SelectedItem.Text;
            string search = txtUser.Text;

            try
            {
                gridEmployee.DataSource = Helper.PFNATTHelper.getEmployee(searchBy, search);
                gridEmployee.DataBind();
            }
            catch (Exception uEr)
            {
                string er = HttpUtility.HtmlEncode(uEr.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "em Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void gridEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridEmployee.PageIndex = e.NewPageIndex;
            loadEmployee();
        }

        protected void gridEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            string emp = gridEmployee.SelectedRow.Cells[1].Text + " - " + gridEmployee.SelectedRow.Cells[2].Text;
            txtUser.Text = "";
            ddlUser.ClearSelection();
            ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogUser", "closeDialogUser('" + emp + "');", true);

        }

        protected void btnSearchUser_Click(object sender, EventArgs e)
        {
            loadEmployee();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set to center", "setCenter('dialogUser');", true);
        }

        protected void gridRequisition_PreRender(object sender, EventArgs e)
        {
            logInUser = (Employee)ViewState[curUser];
            

            for (int i = 0; i < gridRequisition.Rows.Count; i++)
            {
                var linkDelete = (LinkButton)gridRequisition.Rows[i].FindControl("btnDelete");
                if (!logInUser.ISADMIN && linkDelete != null)
                    linkDelete.Visible = false;

                //string status = gridRequisition.Rows[i].Cells[6].Text;
                //if (status.Contains("DONE "))
                //    gridRequisition.Rows[i].BackColor = Color.FromName("#B4C1A8");
                //else if (status.Contains("REJECTED"))
                //    gridRequisition.Rows[i].BackColor = Color.FromName("#EC6A77");
                //else if (status.Contains("CANCELED"))
                //    gridRequisition.Rows[i].BackColor = Color.FromName("#EDBD9D");
                //else if (status.Contains("APPROVAL"))
                //{
                //    if (!status.Contains("APPROVAL BY DEPT. MANAGER"))
                //    gridRequisition.Rows[i].BackColor = Color.FromName("#F2EBC4");
                //}
                
            }
        }

        private void loadSearchResultItem()
        {
            string search = "";
            if (ddlItem.SelectedItem.Text.Equals("Type"))
                search = ddlItemType.SelectedItem.Text;
            else
                search = txtSearchItem.Text;
            try
            {
                gridItemSearch.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
                gridItemSearch.DataBind();
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            loadSearchResultItem();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set Center dialog item", "setCenter('dialogItem');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "show grid item search", "showItem('#gridItemSearch');", true);
        }

        protected void gridItemSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemSearch.PageIndex = e.NewPageIndex;
            loadSearchResultItem();
        }

        protected void gridItemSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idItem = 0, typeID = 0;
            bool isIdItemINT = int.TryParse(gridItemSearch.SelectedRow.Cells[1].Text, out idItem);
            string itemCode = gridItemSearch.SelectedRow.Cells[2].Text;
            string itemName = gridItemSearch.SelectedRow.Cells[3].Text;
            bool isTypeIDINT = int.TryParse(gridItemSearch.SelectedRow.Cells[4].Text, out typeID);

            if (isIdItemINT && isTypeIDINT)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogItem", 
                    "closeDialogItem('" + idItem.ToString() + "','"+itemName.ToString()+"');", true);
            }
        }

        protected void btnSummaryReport_Click(object sender, EventArgs e)
        {

        }
    }
}