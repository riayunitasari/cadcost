﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReqSummary.aspx.cs" Inherits="IntegratedPurchasingSystem.ReqSummary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script src="Script/autoNumeric.js"></script>
    <script src="Script/PageScript/reqSummaryPage.js"></script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Requisition Summary Data
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <%--<div style="padding: 10px 20px 0px;">
                            <asp:Button ID="btnEdit" runat="server" Text="Edit Requisition" OnClick="btnEdit_Click" />
                        </div>--%>

                        <table class="contentForm">
                            <tr class="hideOnCreateNew">
                                <td class="contentFormField">GA Requisition No</td>
                                <td class="contentFormData">
                                    <table>
                                        <tr><td>
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                        <asp:Label ID="LblReqNo" runat="server" Text="" ></asp:Label>
                                                </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnGenerateGAReqNo" EventName="Click" />
                                            </Triggers>
                                            </asp:UpdatePanel></td>
                                            <td>
                                    <asp:Button ID="btnGenerateGAReqNo" runat="server" Text="Generate No" OnClick="btnGenerateGAReqNo_Click"  />
                                                </td>
                                            </tr>
                                        </table>
                                </td>
                                <td class="contentFormField">Date Created</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblDateCreated" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Request By</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblReqBy" runat="server" Text=""></asp:Label></td>
                                <td class="contentFormField">Department</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblDepartment" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <div id="divJobCode" class="onEdit" runat="server">
                                        <asp:TextBox ID="txtJobCode" runat="server" CssClass="validate[required]"></asp:TextBox>
                                        <asp:Button ID="btnJobCode" runat="server" Text=" ... " />
                                        <asp:Button ID="btnResetJobCode" runat="server" Text="Reset" />
                                    </div>
                                    <div id="Div2" class="onView" runat="server">
                                        <asp:Label ID="lblJobCode" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                                <td class="contentFormField">Project Name</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Summary Type</td>
                                <td class="contentFormData">
                                    <div class="onEdit">
                                    <asp:DropDownList ID="ddlSumType" runat="server" CssClass="validate[required]">
                                        <asp:ListItem Value="" Text="-- Select Type --"></asp:ListItem>
                                        <asp:ListItem Value="SST" Text="Stationary"></asp:ListItem>
                                        <asp:ListItem Value="SOT" Text="Others"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>
                                    <div class="onView">
                                        <asp:Label ID="lblSumType" runat="server" Text=""></asp:Label>
                                        </div>
                                </td>
                                <td class="contentFormField">Currency</td>
                                <td class="contentFormData">
                                    <div id="Div5" class="onEdit" runat="server">
                                        <asp:DropDownList ID="ddlCurrency" runat="server" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div id="Div6" class="onView" runat="server">
                                        <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField" style="font-weight: bold">Status</td>
                                <td class="contentFormData" style="font-weight: bold">
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="contentFormField">Exchage Rate</td>
                                <td class="contentFormData">
                                    <div id="Div7" class="onEdit" runat="server">
                                        <asp:TextBox ID="txtExchangeRate" runat="server"></asp:TextBox>
                                    </div>
                                    <div id="Div8" class="onView" runat="server">
                                        <asp:Label ID="lblExchangeRate" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr runat="server" id="reportDiv">
                                <td class="contentFormField">Document</td>
                                <td class="contentFormData">
                                    <asp:LinkButton ID="linkReport" runat="server" OnClick="linkReport_Click" OnClientClick="ShowProgress();" >Print Requisition</asp:LinkButton>
                                </td>
                                <td class="contentFormField"></td>
                                <td class="contentFormData"></td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Requisition
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormFieldTitle">Existing Requisition Items
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px;">
                                    <div class="onEdit"><asp:Button ID="btnAddRequisition" runat="server" Text="Add Item" OnClick="btnAddRequisition_Click" />
                                    <br /></div>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView CssClass="mGrid"
                                                ID="gridSelectedItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                                ShowHeaderWhenEmpty="true" OnRowCommand="gridSelectedItem_RowCommand" OnRowEditing="gridSelectedItem_RowEditing"
                                                OnRowUpdating="gridSelectedItem_RowUpdating" OnRowCancelingEdit="gridSelectedItem_RowCancelingEdit" OnPreRender="gridSelectedItem_PreRender">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="No.">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Request No.">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkReqNo" runat="server" Text='<%# Eval("DEPT_REQ_NO")%>' CommandArgument='<%# Eval("ID_REQ")%>' CommandName="See Requisition"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Item Name">
                                                        <ItemTemplate>
                                                            <%# Eval("PART_NAME") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Metric">
                                                        <ItemTemplate>
                                                            <%# Eval("METRIC") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Price">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remark">
                                                        <ItemTemplate>
                                                            <%# Eval("REMARK") %>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRemark" runat="server" Text='<%# Eval("REMARK") %>' ></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Additional Quantity">
                                                        <ItemTemplate>
                                                            <span><%# Eval("ADDITIONAL_QUANTITY") %></span>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtAddQuantity" runat="server" Text='<%# Eval("ADDITIONAL_QUANTITY") %>' ></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="No." ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIDDetail" runat="server" Text='<%# Eval("ID_REQ_DETAIL") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("ID_REQ_DETAIL") +";"+ Eval("PART_ID") %>'
                                                                Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                            <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("ID_REQ_DETAIL") +";"+ Eval("PART_ID") %>'
                                                                Text="Delete" CommandName="Delete Item" OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="linkUpdate" runat="server" CausesValidation="False" CommandName="Update" Text="Update" CommandArgument='<%# Eval("ID_REQ_DETAIL") +";"+ Eval("PART_ID") %>'></asp:LinkButton>
                                                            <asp:LinkButton ID="linkCancelUpdate" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CommandArgument='<%# Eval("ID_REQ_DETAIL") +";"+ Eval("PART_ID") %>'></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Item ID" ControlStyle-CssClass="hidden" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemID" runat="server" Text='<%# Eval("PART_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnHiddenAddReq" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnGetSelectedItem" EventName="Click" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormFieldTitle">
                                    <hr class="lineDivider" />
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormFieldTitle">Additional Items
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px;">
                                    <div class="onEdit"><asp:Button ID="btnNewItem" runat="server" Text="Add Item" />
                                    <br /></div>
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView CssClass="mGrid"
                                            ID="gridItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                            ShowHeaderWhenEmpty="true" OnRowCommand="gridItem_RowCommand"
                                            OnRowEditing="gridItem_RowEditing" OnRowUpdating="gridItem_RowUpdating"
                                            OnRowCancelingEdit="gridItem_RowCancelingEdit" OnPreRender="gridItem_PreRender">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("PART_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDetailID" runat="server" Text='<%# Eval("ID_REQ_DETAIL")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"> 
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemID" runat="server" Text='<%# Eval("PART_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Metric">
                                                    <EditItemTemplate>
                                                        <%--<div class="metricPlural">--%>
                                                        <asp:DropDownList ID="ddlMetricPlural" runat="server">
                                                            <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                            <asp:ListItem Text="pcs"></asp:ListItem>
                                                            <asp:ListItem Text="boxes"></asp:ListItem>
                                                            <asp:ListItem Text="packs"></asp:ListItem>
                                                            <asp:ListItem Text="units"></asp:ListItem>
                                                            <asp:ListItem Text="seats"></asp:ListItem>
                                                            <asp:ListItem Text="kg"></asp:ListItem>
                                                            <asp:ListItem Text="grams"></asp:ListItem>
                                                            <asp:ListItem Text="meters"></asp:ListItem>
                                                            <asp:ListItem Text="cm"></asp:ListItem>
                                                            <asp:ListItem Text="lot"></asp:ListItem>
                                                            <asp:ListItem Text="dozens"></asp:ListItem>
                                                            <asp:ListItem Text="reams"></asp:ListItem>
                                                            <asp:ListItem Text="sets"></asp:ListItem>
                                                            <asp:ListItem Text="pairs"></asp:ListItem>
                                                            <asp:ListItem Text="pails"></asp:ListItem>
                                                            <asp:ListItem Text="rolls"></asp:ListItem>
                                                        </asp:DropDownList>
                                                            <%--</div>--%>
                                                        <%--<div class="metricSingular">--%>
                                                        <asp:DropDownList ID="ddlMetricSingular" runat="server">
                                                            <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                            <asp:ListItem Text="pc"></asp:ListItem>
                                                            <asp:ListItem Text="box"></asp:ListItem>
                                                            <asp:ListItem Text="pack"></asp:ListItem>
                                                            <asp:ListItem Text="unit"></asp:ListItem>
                                                            <asp:ListItem Text="seat"></asp:ListItem>
                                                            <asp:ListItem Text="kg"></asp:ListItem>
                                                            <asp:ListItem Text="gram"></asp:ListItem>
                                                            <asp:ListItem Text="meter"></asp:ListItem>
                                                            <asp:ListItem Text="cm"></asp:ListItem>
                                                            <asp:ListItem Text="dozen"></asp:ListItem>
                                                            <asp:ListItem Text="ream"></asp:ListItem>
                                                            <asp:ListItem Text="set"></asp:ListItem>
                                                            <asp:ListItem Text="pair"></asp:ListItem>
                                                            <asp:ListItem Text="pail"></asp:ListItem>
                                                            <asp:ListItem Text="roll"></asp:ListItem>
                                                        </asp:DropDownList>
                                                            <%--</div>--%>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMetric" runat="server" Text='<%# Eval("METRIC")%>'></asp:Label>
                                                    </ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Price">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                       <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remark">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("REMARK")%>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <span><%# Eval("REMARK")%></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("PART_ID")%>'
                                                            Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                        <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("PART_ID")+";"+Eval("ID_REQ_DETAIL") %>'
                                                            Text="Delete" CommandName="Delete Item" OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="linkUpdate" runat="server" CausesValidation="False" CommandName="Update" Text="Update" CommandArgument='<%# Eval("PART_ID") + ";" %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="linkCancelUpdate" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CommandArgument='<%# Eval("PART_ID") + ";" %>'></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSaveAdditional" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormFieldTitle">
                                    <hr class="lineDivider" />
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormFieldTitle">All Items
                                </td>
                            </tr>
                             <tr>
                                <td style="padding:0px 20px 20px;">
                                    <br />

                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnAdvance" runat="server" Text="Generate to Advance" OnClick="btnAdvance_Click" CssClass="hidden" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnSubmitToAdvance" />
                                </Triggers>
                            </asp:UpdatePanel>

                                       <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                         <%--Item Name : <asp:Label runat="server" Text="Label" ID="lblItemName"></asp:Label>--%>
                                        <asp:GridView 
                                            ID="gridMergedItem" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnRowCommand="gridMergedItem_RowCommand" OnRowEditing="gridMergedItem_RowEditing" OnRowUpdating="gridMergedItem_RowUpdating"
                                                OnRowCancelingEdit="gridMergedItem_RowCancelingEdit" ShowHeaderWhenEmpty="true" OnPreRender="gridMergedItem_PreRender">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemID" runat="server" Text='<%# Eval("PART_ID") %>'></asp:Label> 
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item Name">
                                                    <ItemTemplate>
                                                        <%# Eval("PART_NAME") %> 
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Metric">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMetric" runat="server" Text='<%# Eval("METRIC") %>'></asp:Label> 
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <div class="metricPlural">
                                                            <asp:DropDownList ID="ddlMetricPlural" runat="server">
                                                                <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                                <asp:ListItem Text="pcs"></asp:ListItem>
                                                                <asp:ListItem Text="boxes"></asp:ListItem>
                                                                <asp:ListItem Text="packs"></asp:ListItem>
                                                                <asp:ListItem Text="units"></asp:ListItem>
                                                                <asp:ListItem Text="seats"></asp:ListItem>
                                                                <asp:ListItem Text="kg"></asp:ListItem>
                                                                <asp:ListItem Text="grams"></asp:ListItem>
                                                                <asp:ListItem Text="meters"></asp:ListItem>
                                                                <asp:ListItem Text="cm"></asp:ListItem>
                                                                <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                                <asp:ListItem Text="dozens" Value="dozens"></asp:ListItem>
                                                                <asp:ListItem Text="reams" Value="reams"></asp:ListItem>
                                                                <asp:ListItem Text="sets" Value="sets"></asp:ListItem>
                                                                <asp:ListItem Text="pairs"></asp:ListItem>
                                                                <asp:ListItem Text="pails"></asp:ListItem>
                                                                <asp:ListItem Text="rolls"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        <div class="metricSingular">
                                                            <asp:DropDownList ID="ddlMetricSingular" runat="server">
                                                                <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                                <asp:ListItem Text="pc"></asp:ListItem>
                                                                <asp:ListItem Text="box"></asp:ListItem>
                                                                <asp:ListItem Text="pack"></asp:ListItem>
                                                                <asp:ListItem Text="unit"></asp:ListItem>
                                                                <asp:ListItem Text="seat"></asp:ListItem>
                                                                <asp:ListItem Text="kg"></asp:ListItem>
                                                                <asp:ListItem Text="gram"></asp:ListItem>
                                                                <asp:ListItem Text="meter"></asp:ListItem>
                                                                <asp:ListItem Text="cm"></asp:ListItem>
                                                                <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                                <asp:ListItem Text="dozen" Value="dozen"></asp:ListItem>
                                                                <asp:ListItem Text="ream" Value="ream"></asp:ListItem>
                                                                <asp:ListItem Text="set" Value="set"></asp:ListItem>
                                                                <asp:ListItem Text="pair"></asp:ListItem>
                                                                <asp:ListItem Text="pail"></asp:ListItem>
                                                                <asp:ListItem Text="roll"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            </div>
                                                        </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Price">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remark">
                                                    <ItemTemplate>
                                                        <%# Eval("REMARK") %> 
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                            <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("REMARK")%>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Detail">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkSeeDetail" runat="server" CausesValidation="False" 
                                                            CommandName="SeeDetail" Text="See Detail" CommandArgument='<%# Eval("PART_ID")%>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Quotation">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkQuotation" runat="server" CommandArgument='<%# Eval("ID_QUO")%>'
                                                            Text='<%# ((int)Eval("ID_QUO") == 0) ? "" : "Quotation No."+Eval("ID_QUO")%>' CommandName="SeeQuotation"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Advance Payment">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="linkAdvance" runat="server" CommandArgument='<%# Eval("ID_ADVANCE")%>'
                                                            Text='<%# ((int)Eval("ID_ADVANCE") == 0) ? "" : "Advance No."+Eval("ID_ADVANCE")%>' CommandName="SeeAdvance"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("PART_ID")%>'
                                                                Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                            <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("PART_ID")  %>'
                                                                Text="Delete" CommandName="Delete Item" OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="linkUpdate" runat="server" CausesValidation="False" CommandName="Update" Text="Update" CommandArgument='<%# Eval("PART_ID")  %>'></asp:LinkButton>
                                                            <asp:LinkButton ID="linkCancelUpdate" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CommandArgument='<%# Eval("PART_ID")  %>'></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Generate To Advance">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="checkAdvance" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnHiddenLoadMerge" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSubmitToAdvance" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>

                              <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                                    <div style="float: right;" id="submitAdvance" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div class="remark">*Please check item that will be generated to Advance Payment and click Submit</div>
                                                </td>
                                                <td>
                                                        <asp:Button ID="btnSubmitToAdvance" runat="server" Text="Submit" OnClick="btnSubmitToAdvance_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnAdvance" />
                                </Triggers>
                            </asp:UpdatePanel>

                                </td>
                            </tr>
                             <tr>
                                <td class="contentFormFieldTitle">
                                    <hr class="lineDivider" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="contentForm">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="contentFormField">Total Estimation Amount</td>
                                                        <td class="contentFormData">
                                                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                                <ContentTemplate>
                                                           <asp:Label ID="lblCurrencyEst" runat="server" Text=""></asp:Label>&nbsp
                                                           <asp:Label ID="lblTotalEst" runat="server" Text="0"></asp:Label>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlCurrency" EventName="SelectedIndexChanged"  />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnSaveAdditional" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnGetSelectedItem" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnChangeTotalEst" EventName="Click" />
                                                                </Triggers>
                                                                    </asp:UpdatePanel>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentFormField">Items Additional Information </td>
                                                        <td class="contentFormData">
                                                            <div id="Div9" class="onEdit" runat="server">
                                                                <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                            <div id="Div10" class="onView" runat="server">
                                                                <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentFormField">Request Remark</td>
                                                        <td class="contentFormData">
                                                            <div id="Div11" class="onEdit" runat="server">
                                                                <asp:TextBox ID="txtReqRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                            <div id="Div12" class="onView" runat="server">
                                                                <asp:Label ID="lblReqRemark" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div id="actionHistory" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action History
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnHiddenAction" runat="server" Text="Button" CausesValidation="false" Style="display: none;" />
                                    <asp:GridView ShowHeaderWhenEmpty="true"
                                        CssClass="mGrid" ID="gridActionHistory" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Participant" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("PARTICIPANT")%> - <%# Eval("PARTICIPANT_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Completed By" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("COMPLETED_BY")%> - <%# Eval("COMPLETED_BY_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Activity" DataField="ACTIVITY_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Action" DataField="ACTION_TEXT" />
                                            <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%#  Eval("START_DATE", "{0:dd MMM yyyy - HH:mm }") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Convert.ToString(Eval("END_DATE")).Contains("1/1/1975")?"":Eval("END_DATE", "{0:dd MMM yyyy - HH:mm}") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Comment" DataField="COMMENT" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnHiddenAction" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="actionDiv" runat="server">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Action
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <div style="padding: 20px;">
                            <table>
                                <tr>
                                    <td class="contentFormField">Action</td>
                                    <td class="contentFormData">
                                        <asp:DropDownList ID="ddlAction" runat="server" CssClass="validate[required]">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentFormField">Comment</td>
                                    <td class="contentFormData">
                                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentFormField"></td>
                                    <td class="contentFormData">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <div style="display: none">
                    <div id="dialogRequisition">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormField">Requisition No.</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtReqNo" runat="server"></asp:TextBox>
                                </td>
                                <td class="contentFormField">Month Created</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtJobCodeReq" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnJobCodeReq" runat="server" Text="..." />
                                    <asp:Button ID="btnResetJobCodeReq" runat="server" Text="Reset" />
                                </td>
                                <td class="contentFormField">Request Type</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlReqType" runat="server">
                                        <asp:ListItem Text="Stationary Requisition" Value="ST"></asp:ListItem>
                                        <asp:ListItem Text="General Requisition" Value="RS"></asp:ListItem>
                                        <asp:ListItem Text="IT Equipment Requisition" Value="SF"></asp:ListItem>
                                        <asp:ListItem Text="Advance" Value="AD"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnSearchReq" runat="server" Text="Search" OnClick="btnSearchReq_Click" />
                        </div>
                        <br />
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridRequisition_PageIndexChanging"
                                        ID="gridRequisition" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                        OnSelectedIndexChanged="gridRequisition_SelectedIndexChanged" OnPreRender="gridRequisition_PreRender">
                                        <Columns>
                                            <asp:BoundField runat="server" HeaderText="ID" DataField="ID_REQ" HeaderStyle-CssClass="hidden" ControlStyle-CssClass="hidden"
                                                ItemStyle-CssClass="hidden" />
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Requisition No." DataField="DEPT_REQ_NO" />
                                            <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE" />
                                            <asp:BoundField runat="server" HeaderText="Type" DataField="REQ_TYPE_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Date Created" DataField="DATE_CREATED" DataFormatString="{0:dd-MMM-yyyy}" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearchReq" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div id="dialogJobCode">
                        <table class="contentForm">
                            <tr>
                                <td style="min-width: 80px;">Search By</td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCode" runat="server">
                                        <asp:ListItem Text="JobCode"></asp:ListItem>
                                        <asp:ListItem Text="Project Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchJobCode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearchJobCode" runat="server" Text="Search" OnClick="btnSearchJobCode_Click" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridJobCode_PageIndexChanging" CellPadding="2"
                                            ID="gridJobCode" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridJobCode_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE_ID" />
                                                <asp:BoundField runat="server" HeaderText="Project Name" DataField="PROJECT_NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnResetJobCodeState" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogItemReq">
                        <div style="display: none;">
                            <asp:Label ID="lblIDReq" runat="server" Text=""></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridItemReq_PageIndexChanging" CellPadding="2"
                                    ID="gridItemReq" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                    OnPreRender="gridItemReq_PreRender">
                                    <Columns>
                                        <asp:BoundField runat="server" HeaderText="ID Req" DataField="ID_REQ" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="ID" DataField="ID_DETAIL" HeaderStyle-CssClass="hidden"
                                            ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="Item ID" DataField="PART_ID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField runat="server" HeaderText="Item ID" DataField="TYPE_ID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField runat="server" HeaderText="Item Name" DataField="PART_NAME" />
                                        <asp:BoundField runat="server" HeaderText="Code" DataField="PART_CODE" />
                                        <asp:BoundField runat="server" HeaderText="Type" DataField="TYPE_NAME" />
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>  <%# Eval("METRIC") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Price">
                                            <ItemTemplate>
                                                <%# Eval("CURRENCY") %> <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="checkItem" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div style="float: right; padding: 20px;">
                            <asp:Button ID="btnGetSelectedItem" runat="server" Text="Submit" OnClick="btnGetSelectedItem_Click" CausesValidation="false" />
                            <input type="button" id="btnCancelItemReq" value="Cancel" />
                        </div>
                    </div>

                    <div id="dialogItem">
                        <div>
                            <table id="searchPaneItem" runat="server">
                                <tr>
                                    <td class="contentFormField">Search Item By</td>
                                    <td class="contentFormData">
                                        <asp:DropDownList ID="ddlItem" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Type"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:TextBox ID="txtSearchItem" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlItemType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:Button ID="btnSearchItem" runat="server" Text="Search" OnClick="btnSearchItem_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPanelItem" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10"
                                            ID="gridItemSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridItemSearch_SelectedIndexChanged" OnPageIndexChanging="gridItemSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PART_ID" HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="PART_CODE" HeaderText="Item Code" />
                                                <asp:BoundField DataField="PART_NAME" HeaderText="Item Name" />
                                                <asp:BoundField DataField="TYPE_ID" HeaderText="ID Type" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchItem" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnAddRequisition" EventName="Click" />
                                        <%--<asp:AsyncPostBackTrigger ControlID="btnMerge" EventName="Click" />--%>
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                   <div id="dialogDetailItem">
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10"
                                            ID="gridDetailItem" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                             OnPageIndexChanging="gridDetailItem_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID_REQ_DETAIL" HeaderText="id detail" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="DEPT_REQ_NO" HeaderText="Request No." />
                                                <asp:BoundField DataField="QUANTITY" HeaderText="Quantity" />
                                                <asp:BoundField DataField="ADDITIONAL_QUANTITY" HeaderText="Additional Quantity" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnDetailItem" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                     <div id="dialogAdditionalItem">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <table class="contentForm" style="text-align: left;" id="newAdditional">
                                    
                                    <tr style="display: none;">
                                        <td>ID</td>
                                        <td>
                                            <asp:TextBox ID="txtItemID" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Code</td>
                                        <td>
                                            <asp:TextBox ID="txtItemCode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr style="display: none;">
                                        <td>Part ID</td>
                                        <td>
                                            <asp:TextBox ID="txtPartTypeID" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Part Name</td>
                                        <td>
                                            <asp:TextBox ID="txtPartTypeName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Item</td>
                                        <td>
                                            <asp:TextBox ID="txtNewItem" runat="server" CssClass="validate[required]"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="contentFormData">Quantity</td>
                                        <td>
                                            <asp:TextBox ID="txtNewQuantity" runat="server" CssClass="validate[required, custom[float]]"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Metric</td>
                                        <td>
                                             <asp:DropDownList ID="ddlMetricPlural" runat="server" CssClass="validate[required] metricPlural">
                                                            <asp:ListItem Text="-- Select Metric --" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="pcs" Value="pcs"></asp:ListItem>
                                                            <asp:ListItem Text="boxes" Value="boxes"></asp:ListItem>
                                                            <asp:ListItem Text="packs" Value="packs"></asp:ListItem>
                                                            <asp:ListItem Text="units" Value="units"></asp:ListItem>
                                                            <asp:ListItem Text="seats" Value="seats"></asp:ListItem>
                                                            <asp:ListItem Text="kg" Value="kg"></asp:ListItem>
                                                            <asp:ListItem Text="grams" Value="grams"></asp:ListItem>
                                                            <asp:ListItem Text="meters" Value="meters"></asp:ListItem>
                                                            <asp:ListItem Text="cm" Value="cm"></asp:ListItem>
                                                            <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                            <asp:ListItem Text="dozens" Value="dozens"></asp:ListItem>
                                                            <asp:ListItem Text="reams" Value="reams"></asp:ListItem>
                                                            <asp:ListItem Text="sets" Value="sets"></asp:ListItem>
                                                            <asp:ListItem Text="pairs"></asp:ListItem>
                                                            <asp:ListItem Text="pails"></asp:ListItem>
                                                            <asp:ListItem Text="rolls"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlMetricSingular" runat="server" CssClass="validate[required] metricSingular">
                                                            <asp:ListItem Text="-- Select Metric --" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="pc" Value="pc"></asp:ListItem>
                                                            <asp:ListItem Text="box" Value="box"></asp:ListItem>
                                                            <asp:ListItem Text="pack" Value="pack"></asp:ListItem>
                                                            <asp:ListItem Text="unit" Value="unit"></asp:ListItem>
                                                            <asp:ListItem Text="seat" Value="seat"></asp:ListItem>
                                                            <asp:ListItem Text="kg" Value="kg"></asp:ListItem>
                                                            <asp:ListItem Text="gram" Value="gram"></asp:ListItem>
                                                            <asp:ListItem Text="meter" Value="meter"></asp:ListItem>
                                                            <asp:ListItem Text="cm" Value="cm"></asp:ListItem>
                                                            <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                            <asp:ListItem Text="dozen" Value="dozen"></asp:ListItem>
                                                            <asp:ListItem Text="ream" Value="ream"></asp:ListItem>
                                                            <asp:ListItem Text="set" Value="set"></asp:ListItem>
                                                            <asp:ListItem Text="lot" Value="lot"></asp:ListItem>
                                                            <asp:ListItem Text="pair"></asp:ListItem>
                                                            <asp:ListItem Text="pail"></asp:ListItem>
                                                            <asp:ListItem Text="roll"></asp:ListItem>
                                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Price</td>
                                        <td>
                                            <asp:TextBox ID="txtNewPrice" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentFormData">Remark</td>
                                        <td>
                                            <asp:TextBox ID="txtNewRemark" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="gridItemSearch" EventName="SelectedIndexChanged" />
                            </Triggers>
                            </asp:UpdatePanel>
                        <br />
                        <div style="width: 100%; text-align: center">
                            <asp:Button ID="btnSaveAdditional" runat="server" Text="Save" OnClick="btnSaveAdditional_Click" />
                            &nbsp<asp:Button ID="btnCancelAdditional" runat="server" Text="Cancel" />
                        </div>
                    </div>

                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <asp:Button ID="btnDetailItem" runat="server" Text="" />
                    <asp:Button ID="btnReloadPage" runat="server" Text="" OnClick="btnReloadPage_Click" />
                    <asp:Button ID="btnHiddenAddReq" runat="server" Text="" />
                    <asp:Button ID="btnHiddenLoadMerge" runat="server" Text="" />
                    <asp:Button ID="btnResetJobCodeState" runat="server" Text="" OnClick="btnResetJobCodeState_Click" CausesValidation="false" />
                    <asp:Button ID="btnChangeTotalEst" runat="server" Text="Button" OnClick="btnChangeTotalEst_Click" />
                    <asp:Button ID="btnDownload" runat="server" Text="Button" OnClick="btnDownload_Click" UseSubmitBehavior="false" />

                </div>
            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

