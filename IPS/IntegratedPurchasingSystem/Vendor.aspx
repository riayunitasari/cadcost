﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Vendor.aspx.cs" Inherits="IntegratedPurchasingSystem.Vendor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script src="Script/PageScript/vendorPage.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu').jqsimplemenu();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Vendor
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <div style="padding: 20px;">
                            <asp:Button ID="btnNewVendor" runat="server" Text="New Vendor" OnClick="btnNewVendor_Click" />
                        </div>
                        <div>
                            <div style="padding: 0px 20px 20px;">
                                <div class="barSearch">
                                    <div class="divField">Search Vendor</div>
                                    <div class="divData">
                                        <asp:TextBox ID="txtSearchVendor" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="divData">
                                        <asp:Button ID="btnSearchVendor" runat="server" Text="Search" CausesValidation="false" OnClick="btnSearchVendor_Click" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="uptPan" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="20" ShowHeaderWhenEmpty="true"
                                            CssClass="mGrid" OnRowCommand="gridVendor_RowCommand"
                                            ID="gridVendor" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gridVendor_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID_VENDOR" HeaderText="Vendor ID" ControlStyle-CssClass="hidden" ItemStyle-CssClass="hidden"
                                                    HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="CODE" HeaderText="Vendor Code" />
                                                <asp:BoundField DataField="VENDOR_NAME" HeaderText="Name" />
                                                <asp:BoundField DataField="ADDRESS" HeaderText="Address" />
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="lbtnEdit" Text="Edit" runat="server" CommandName="Edit Vendor" title="Edit Vendor"
                                                            CommandArgument='<%#Eval("ID_VENDOR") %>'></asp:LinkButton>

                                                        <asp:LinkButton ID="lbtnDelete" Text="Delete" runat="server" CommandName="Delete Vendor" title="Delete Vendor"
                                                            CommandArgument='<%#Eval("ID_VENDOR") %>' OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchVendor" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSaveVendor" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="display: none">
                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>

                    <div id="dialogVendor">
                        <div>
                            <asp:UpdatePanel ID="uptDialogVendor" runat="server">
                                <ContentTemplate>
                                    <table class="contentForm">
                                        <tr style="display: none;">
                                            <td>ID</td>
                                            <td>
                                                <asp:TextBox ID="txtVendorID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Code</td>
                                            <td>
                                                <div style="vertical-align:middle;">
                                                    <div style="float: left">
                                                        <asp:TextBox ID="txtVendorCode" runat="server" CssClass="validate[required]"></asp:TextBox>&nbsp</div>
                                                    <div style="float: left">
                                                        <asp:Image ID="imgCheckCode" runat="server" Height="25px" Width="25px" CssClass="imgDiv" ImageUrl="~/Content/images/noPic.png" />&nbsp</div>
                                                    <div style="float: left;">
                                                        <asp:LinkButton ID="btnCheckCode" runat="server" OnClick="btnCheckCode_Click">Check Code</asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Name</td>
                                            <td>
                                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="validate[required]" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>
                                                <asp:TextBox ID="txtVendorAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnCheckCode" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div style="width: 100%; text-align: center">
                                <asp:Button ID="btnSaveVendor" runat="server" Text="Save" OnClick="btnSaveVendor_Click" />
                                &nbsp
                            <asp:Button ID="btnCancelVendor" runat="server" Text="Cancel" />
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
