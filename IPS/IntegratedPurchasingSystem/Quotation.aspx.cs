﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using System.Web.Http;
using System.Data.SqlTypes;

namespace IntegratedPurchasingSystem
{
    public partial class Quotation : Page
    {

        const string curVendor = "list_vendor_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Quo_State";
        const string curQuo = "current_Quotation";
        const string curItem = "quot_item_cnst";
        const string quoState = "current_Quotation_State";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        const string flFormat = "downloaded_File_Format";

        IntegratedPurchasingSystem.Entities.Employee logInUser = new IntegratedPurchasingSystem.Entities.Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                int idQuo = 0;
                GeneralHelper.generateDDLCurrency(ddlCurrency);

                if (int.TryParse(Request["ViewQuotation"], out idQuo))
                {
                    ViewState[curState] = "view";
                    QuoObject quot = QuotHelper.getQuotationByID(idQuo);
                    ViewState[curQuo] = quot;
                    //logInUser = new Employee();
                    if (quot != null)
                    {
                        logInUser = GeneralHelper.isParticipant(logInUser, 2, quot.ID_QUO, "QUO");
                        List<QuoVendor> detail = QuotHelper.getQuotationDetail(idQuo);
                        List<RequisitionItems> items = QuotHelper.getQuotationItem(idQuo);
                        ViewState[curVendor] = detail;
                        ViewState[curItem] = items;
                        GeneralHelper.generateDDLAction(ddlAction, 2, quot.ACTIVITY_ID);
                        linkSelectReq.Visible = false;
                        loadSelectedVendor();
                        loadQuoItem();
                        loadActionHistory();

                        if (quot.ID_PO > 0)
                            linkPO.Text = "Purchase Order No. " + quot.ID_PO;
                        else
                            divPO.Visible = false;

                        if (logInUser.ISPARTICIPANT || logInUser.ISADMIN)
                        {
                            if ((logInUser.IS_CUR_PARTICIPANT && quot.ACTIVITY_ID == Variable.Status.FixRequest) ||
                                (logInUser.ISADMIN && quot.ACTIVITY_ID == Variable.Status.PrepareQuotation))
                            {
                                ViewState[curState] = "edit";
                                onEditState();
                            }
                            else
                            {
                                ViewState[curState] = "view";
                                onViewState();
                            }
                        }
                        else
                            Response.Redirect("NotAuthorized.aspx");

                    }
                    else
                        Response.Redirect("NotAuthorized.aspx");
                }
                else
                {
                    ViewState[curState] = "new";
                    QuoObject quot = new QuoObject();
                    quot.CREATED_BY = logInUser.EMP_ID;
                    ViewState[curQuo] = quot;
                    onEditState();
                    Helper.GeneralHelper.generateDDLAction(ddlAction, 2, Variable.Status.PrepareQuotation);
                    loadSelectedVendor();
                    loadQuoItem();
                    actionHistory.Visible = false;
                    lblDoc.Visible = false;
                    linkPrint.Visible = false;
                }
                
            }

            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.btnPrint);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridVendorSearch, this);
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            Helper.GeneralHelper.gridRender(gridRequisition, this);
            Helper.GeneralHelper.gridRender(gridItemSearch, this);

            base.Render(writer);
        }


        private void onEditState()
        {
            QuoObject quot = (QuoObject)ViewState[curQuo];
            logInUser = (Employee)ViewState[curUser];
            if (quot.ID_REQ == 0) //new
            {
                quot.ACTIVITY_ID = Variable.Status.PrepareQuotation;
                quot.ACTIVITY_TEXT = "PREPARE QUOTATION";
                linkGAReq.Visible = false;
                lblDateCreated.Text = DateTime.Now.ToString("dd MMM yyyy");
                List<RequisitionItems> items = new List<RequisitionItems>();
                ViewState[curItem] = items;
                List<QuoVendor> detail = new List<QuoVendor>();
                ViewState[curVendor] = detail;
                actionDiv.Visible = false;
                lblStatus.Text = quot.ACTIVITY_TEXT;
                generateRequestor();
                generateRecommVendor();
            }
            else
            {
                actionDiv.Visible = true;
                btnAddVendor.Visible = true;
                linkGAReq.Visible = true;
                linkGAReq.Text = quot.REQ_NO;
                lblJobCode.Text = quot.JOBCODE;
                lblProjectName.Text = quot.PROJECT_NAME;
                lblReqBy.Text = quot.REQ_BY_NAME;
                lblDepartment.Text = quot.REQ_BY_DEPT;
                lblDateCreated.Text = quot.DATE_CREATED.ToString("dd MMM yyyy");
                txtQuoRemark.Visible = true;
                txtReqItem.Text = quot.REQUEST_ITEM;
                txtQuoRemark.Text = quot.REMARKS;
                lblStatus.Text = quot.ACTIVITY_TEXT;
                gridSelectedVendor.Columns[16].Visible = true;
                lblQuoRemark.Visible = false;
                lblReqItem.Visible = false;
                recommVendorOnView.Visible = false;
                recommVendorOnEdit.Visible = true;
                lblRequestor.Visible = false;
                //uploadDiv.Visible = true;
                linkPDF.Text = quot.ATTACHMENT;
                generateRequestor();
                generateRecommVendor();
                if ((int)quot.RECOMM_VENDOR != 0)
                    ddlRecommVendor.SelectedIndex = ddlRecommVendor.Items.IndexOf(ddlRecommVendor.Items.FindByValue(quot.RECOMM_VENDOR.ToString()));
                if (quot.REQUESTOR != null)
                {
                    int index = ddlRequestor.Items.IndexOf(ddlRequestor.Items.FindByText(quot.REQUESTOR));
                    if (index > 0)
                        ddlRequestor.SelectedIndex = ddlRequestor.Items.IndexOf(ddlRequestor.Items.FindByText(quot.REQUESTOR));
                    else
                    {
                        ddlRequestor.SelectedIndex = ddlRequestor.Items.IndexOf(ddlRequestor.Items.FindByText("Others"));
                        txtRequestor.CssClass = txtRequestor.CssClass.Replace("hidden", "");
                        txtRequestor.Text = quot.REQUESTOR;
                    }
                }

                if (ViewState[curState].ToString().Equals("edit"))
                    linkSelectReq.Visible = false;
            }
            setLinkPDF();
        }

        private void onViewState()
        {
            QuoObject quot = (QuoObject)ViewState[curQuo];
            logInUser = (Employee)ViewState[curUser];
            btnAddVendor.Visible = true;
            linkGAReq.Text = quot.REQ_NO;
            lblJobCode.Text = quot.JOBCODE;
            lblProjectName.Text = quot.PROJECT_NAME;
            lblReqBy.Text = quot.REQ_BY_NAME;
            lblDepartment.Text = quot.REQ_BY_DEPT;
            lblDateCreated.Text = quot.DATE_CREATED.ToString("dd MMM yyyy");
            lblQuoRemark.Visible = true;
            lblReqItem.Visible = true;
            lblReqItem.Text = quot.REQUEST_ITEM;
            lblQuoRemark.Text = quot.REMARKS;
            lblStatus.Text = quot.ACTIVITY_TEXT;
            gridSelectedVendor.Columns[16].Visible = false;
            txtQuoRemark.Visible = false;
            txtReqItem.Visible = false;
            recommVendorOnView.Visible = true;
            recommVendorOnEdit.Visible = false;
            ddlRecommVendor.Visible = false;
            //uploadDiv.Visible = false;
            linkPDF.Visible = true;
            linkPDF.Text = quot.ATTACHMENT;
            setLinkPDF();
            lblrecommVendor.Visible = true;
            lblrecommVendor.Text = quot.RECOMM_VENDOR_NAME;

            ddlRequestor.Visible = false;
            lblRequestor.Visible = true;
            lblRequestor.Text = quot.REQUESTOR;
            uploadDiv.Visible = false;
            btnAddVendor.Visible = false;

            actionDiv.Visible = false;
            if (logInUser.IS_CUR_PARTICIPANT && quot.ACTIVITY_ID != Variable.Status.CanceledRequest &&
                quot.ACTIVITY_ID != Variable.Status.DoneRequest)
                actionDiv.Visible = true;
        }

        private void loadSelectedVendor()
        {
            List<QuoVendor> detail = (List<QuoVendor>)ViewState[curVendor];
            gridSelectedVendor.DataSource = detail;
            gridSelectedVendor.DataBind();
        }

        private void loadQuoItem()
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            gridItem.DataSource = items;
            gridItem.DataBind();
        }


        private void loadActionHistory()
        {
            QuoObject quot = (QuoObject)ViewState[curQuo];
            gridActionHistory.DataSource = Helper.ActionLogHelper.getActionLog(quot.ID_QUO, "QUO");
            gridActionHistory.DataBind();
        }

        private void generateRecommVendor()
        {
            ddlRecommVendor.Items.Clear();
            List<QuoVendor> detail = (List<QuoVendor>)ViewState[curVendor];
            ddlRecommVendor.Items.Add(new ListItem("-- Select Vendor --", ""));
            if (detail.Count > 0)
            {
                foreach (var i in detail)
                    ddlRecommVendor.Items.Add(new ListItem(i.VENDOR_NAME, i.ID_VENDOR.ToString()));
            }
        }

        private void generateRequestor()
        {
            ddlRequestor.Items.Clear();
            QuoObject quot = (QuoObject)ViewState[curQuo];
            if (quot.ID_REQ == 0)
                ddlRequestor.Visible = false;
            else
            {
                ddlRequestor.Visible = true;
                ddlRequestor.Items.Add(new ListItem("-- Select Requestor --", ""));
                ddlRequestor.Items.Add(new ListItem(quot.REQ_BY_DEPT, quot.REQ_BY_DEPT));
                ddlRequestor.Items.Add(new ListItem(quot.PROJECT_NAME, quot.PROJECT_NAME));
                ddlRequestor.Items.Add("Others");
            }
        }

        private void loadGridVendor()
        {
            gridVendorSearch.DataSource = VendorHelper.getVendor(ddlVendor.SelectedItem.Text, txtSearchVendor.Text);
            gridVendorSearch.DataBind();
            ScriptManager.RegisterStartupScript(this, GetType(), "set vendor center", "setCenter('dialogVendor');", true);
        }

        private void deletePDF(string fileName)
        {
            string AttachmentDir = Server.MapPath("/IPS/PDF");
            AttachmentDir = AttachmentDir + "\\" + fileName;
            FileInfo file = new FileInfo(AttachmentDir);
            if (file.Exists)
            {
                file.Delete();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                QuoObject quo = (QuoObject)ViewState[curQuo];
                DateTime modifiedTime = DateTime.Now;
                List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                List<QuoVendor> vendors = (List<QuoVendor>)ViewState[curVendor];

                logInUser = (Employee)ViewState[curUser];
                int actionID = 0;
                bool isAction;
                isAction = int.TryParse(ddlAction.SelectedItem.Value, out actionID);
                string strFilename = "", strMessage = "";
                bool isUploaded = false;
                if (fileAttachmentUpload.HasFile)
                {
                    if (quo.ATTACHMENT != null)
                        if (quo.ATTACHMENT.Length > 0 && linkPDF.Text.Length > 0)
                            deletePDF(quo.ATTACHMENT);
                    strFilename = DateTime.Now.ToString("yyyy-MM-dd") + "_" + quo.REQ_NO.Replace("/", "-") + "_QUO_" + quo.ID_QUO + ".pdf";
                    if (actionID > 0)
                        isUploaded = uploadFile(strFilename, out strMessage);
                }
                else
                {
                    if (linkPDF.Text.Length > 0 && quo.ATTACHMENT != null)
                        if (quo.ATTACHMENT.Length > 0)
                            strFilename = quo.ATTACHMENT;
                    isUploaded = true; //tidak ada file yang diupload
                }


                if (ViewState[curState].ToString().Equals("new"))
                {
                    int id = 0, vendorID = 0;

                    if (ddlRequestor.SelectedItem.Text.Contains("Others"))
                        quo.REQUESTOR = txtRequestor.Text;
                    else quo.REQUESTOR = ddlRequestor.SelectedItem.Text;
                    bool idVendor = int.TryParse(ddlRecommVendor.SelectedItem.Value, out vendorID);
                    quo.RECOMM_VENDOR = vendorID;
                    quo.RECOMM_VENDOR_NAME = ddlRecommVendor.SelectedItem.Text;
                    quo.REQUEST_ITEM = txtReqItem.Text;
                    quo.ATTACHMENT = strFilename;
                    quo.REMARKS = txtQuoRemark.Text;
                    Helper.QuotHelper.insertQuotation(quo, out id);
                    quo.ID_QUO = id;
                    QuotHelper.insertDetailVendor(vendors, id);
                    RequisitionHelper.updateReqDetailQuot(items, id);
                    ActionLogHelper.insertQuotHistory(logInUser.EMP_ID, id, actionID,
                       Variable.Status.PrepareQuotation, txtComment.Text);

                    ViewState[curQuo] = quo;
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessNew", "saveSuccess('Success','Your quotation has succesfully recorded');", true);
                }
                else if (ViewState[curState].ToString().Equals("edit"))
                {
                    quo.REQUEST_ITEM = txtReqItem.Text;
                    int vendorID = 0;
                    if (ddlRequestor.SelectedItem.Text.Contains("Others"))
                        quo.REQUESTOR = txtRequestor.Text;
                    else quo.REQUESTOR = ddlRequestor.SelectedItem.Text;

                    bool idVendor = int.TryParse(ddlRecommVendor.SelectedItem.Value, out vendorID);
                    quo.RECOMM_VENDOR = vendorID;
                    quo.RECOMM_VENDOR_NAME = ddlRecommVendor.SelectedItem.Text;
                    quo.ATTACHMENT = strFilename;
                    quo.REMARKS = txtQuoRemark.Text;
                    Helper.QuotHelper.updateQuotation(quo);
                    Helper.QuotHelper.updateDetailQuotation(vendors, quo.ID_QUO);
                    isAction = int.TryParse(ddlAction.SelectedItem.Value, out actionID);
                    Helper.ActionLogHelper.insertQuotHistory(logInUser.EMP_ID, quo.ID_QUO, actionID, quo.ACTIVITY_ID, txtComment.Text);

                    ScriptManager.RegisterStartupScript(this, GetType(), "btnsubmit saveSuccessNew", "saveSuccess('Success','Your change has been saved successfully!');", true);
                }
                else if (ViewState[curState].ToString().Equals("view"))
                {
                    isAction = int.TryParse(ddlAction.SelectedItem.Value, out actionID);
                    Helper.ActionLogHelper.insertQuotHistory(logInUser.EMP_ID, quo.ID_QUO, actionID, quo.ACTIVITY_ID, txtComment.Text);
                    ScriptManager.RegisterStartupScript(this, GetType(), "btnsubmit saveSuccessEdit", "saveSuccess('Success','Your change has been saved successfully!');", true);
                }
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnsubmit Error", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnsubmit hide progress", "closeLoading();", true);
            }
        }

        protected void btnSearchVendor_Click(object sender, EventArgs e)
        {
            try
            {
                loadGridVendor();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchVendor show error", "showException('Error', '" + HttpUtility.HtmlEncode(ex.Message) + "');", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchVendor hide progress", "closeLoading();", true);
            }
           
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            try
            {
                double quoPrice = 0, discount = 0, vat = 0, exchangeRate = 0;
                int idVendor = 0;
                QuoObject quo = (QuoObject)ViewState[curQuo];
                string error = "",payTerms = "", vatText = "", currency = "";
                DateTime quoDate = (DateTime)SqlDateTime.MinValue;
                if (btnNoQuote.Text.Equals("Insert Quote"))
                {
                    if (txtVendor.Text.Equals("") || !int.TryParse(txtVendorID.Text, out idVendor))
                        error += "- Vendor field is required <br />";
                }
                else
                {
                    if (txtVendor.Text.Equals("") || !int.TryParse(txtVendorID.Text, out idVendor))
                        error += "- Vendor field is required <br />";
                    if (txtQuoDate.Text.Equals(""))
                        error += "- Quotation Date field is required <br />";
                    else
                        quoDate = Convert.ToDateTime(txtQuoDate.Text);

                    if (txtTechAspect.Text.Equals(""))
                        error += "- Technical Aspect field is required <br />";
                    if (!double.TryParse(txtQuoPrice.Text.Replace(",", ""), out quoPrice))
                        error += "- Quoted Price must be a valid floating number <br />";
                    if (!double.TryParse(txtDiscount.Text.Replace(",", ""), out discount))
                        error += "- Discount must be a valid floating number <br />";

                    if (ddlVAT.SelectedItem.Value.Equals(""))
                    {
                        error += "- VAT field is required <br />";
                        vat = 0;
                    }
                    else
                    {
                        vat = 0;
                        if (ddlVAT.SelectedItem.Text.Contains("Exclude"))
                            vat = quoPrice * 0.1;
                        vatText = ddlVAT.SelectedItem.Value;
                    }

                    if (txtDeliveryTime.Text.Equals(""))
                        error += "- Delivery Time field is required <br />";

                    if (ddlPaymentTerms.SelectedItem.Value.Equals(""))
                        error += "- Payment Term is required <br />";
                    else
                    {
                        if (ddlPaymentTerms.SelectedItem.Text.Equals("Others"))
                        {
                            if (txtPayTermOthers.Text.Equals(""))
                                error += "- Payment Term is required <br />";
                            else
                                payTerms = txtPayTermOthers.Text;
                        }
                        else
                            payTerms = ddlPaymentTerms.SelectedItem.Text;
                    }

                    currency = ddlCurrency.SelectedItem.Value;
                    if (string.IsNullOrEmpty(currency))
                        error += "- Currency is required <br />";
                    else
                    {
                        bool isExchangerate = double.TryParse(txtExchangeRate.Text.Replace(",", ""), out exchangeRate);
                        if(!currency.Equals("IDR"))
                        {   
                            if(exchangeRate == 0)
                                error += "- Exchange Rate is required <br />";
                        }    
                    }
                }

                if (error.Equals("")) // no error
                {
                    double netPrice = quoPrice - discount + vat;

                    List<QuoVendor> details = (List<QuoVendor>)ViewState[curVendor];
                    if (details.Any(x => x.ID_VENDOR == idVendor)) //edit
                    {
                        QuoVendor detail = details.FirstOrDefault(x => x.ID_VENDOR == idVendor);
                        detail.QUOT_DATE = quoDate;
                        detail.QUOT_NO = txtQuotationNo.Text;
                        detail.QUO_PRICE = quoPrice;
                        detail.DISCOUNT = discount;
                        detail.VAT = vatText;
                        detail.VAT_AMOUNT = vat;
                        detail.NET_PRICE = netPrice;
                        detail.CURRENCY = currency;
                        detail.DELIVERY_TIME = txtDeliveryTime.Text;
                        detail.WARRANTY = txtWarranty.Text;
                        detail.PAYMENT_TERMS = payTerms;
                        detail.EXCHANGE_RATE = exchangeRate;
                        detail.TECHNICAL_ASPECT = txtTechAspect.Text;
                    }
                    else //new
                    {
                        QuoVendor newDetail = new QuoVendor(0, quo.ID_QUO, idVendor, txtVendor.Text,
                            quoDate, txtQuotationNo.Text, quoPrice, discount, vatText, vat, netPrice,
                            currency, exchangeRate, txtDeliveryTime.Text, txtWarranty.Text, payTerms, txtTechAspect.Text);
                        details.Add(newDetail);
                    }
                    ViewState[curVendor] = details;
                    loadSelectedVendor();
                    generateRecommVendor();
                    if (details.Count == 4)
                        btnAddVendor.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), "close dialog detail", "closeDialogDetail();", true);
                }
                else
                {
                    error = "Following error occurs: <br />" + error;
                    ScriptManager.RegisterStartupScript(this, GetType(), "input detail error", "showMsg('Error', '" + error + "');", true);
                }
            }
            catch (Exception exc)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showException('Error','" + HttpUtility.HtmlEncode(exc.Message) + "')", true);
            }
            finally
            {
                txtQuoDate.Enabled = true;
                txtTechAspect.Enabled = true;
                txtQuoPrice.Enabled = true;
                txtDiscount.Enabled = true;
                ddlVAT.ClearSelection();
                ddlVAT.Enabled = true;
                ddlCurrency.Enabled = true;
                txtDeliveryTime.Enabled = true;
                ddlPaymentTerms.Enabled = true;
                txtPayTermOthers.Enabled = true;
                txtWarranty.Enabled = true;
                txtExchangeRate.Enabled = true;
                btnNoQuote.Text = "No Quote";
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSaveDetail hide progress", "closeLoading();", true);
            }

        }

        protected void gridVendorSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idVendor = 0;
            List<QuoVendor> details = (List<QuoVendor>)ViewState[curVendor];

            bool isIdItemINT = int.TryParse(gridVendorSearch.SelectedRow.Cells[1].Text, out idVendor);
            if (details.Any(x => x.ID_VENDOR == idVendor))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "vendor already exists", "showMsg('Error','Selected vendor already added to Quotation vendor list<br />Please choose another vendor or edit your vendor data');", true);
            }
            else
            {
                string vendorName = gridVendorSearch.SelectedRow.Cells[3].Text;
                ScriptManager.RegisterStartupScript(this, GetType(), "set vendor center", "closeDialogVendor('" + idVendor + "', '" + vendorName + "');", true);
            }
        }

        protected void gridVendorSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridVendorSearch.PageIndex = e.NewPageIndex;
            loadGridVendor();
        }

        private void loadAvalaibleRequisition()
        {
            string jobcode = txtJobCodeReq.Text;
            int idItem = 0;
            bool isIDItem = int.TryParse(txtItemID.Text, out idItem);
            gridRequisition.DataSource = Helper.QuotHelper.getRequisitionForQuotation(txtReqNo.Text, int.Parse(ddlMonth.SelectedItem.Value),
                jobcode, ddlReqType.SelectedItem.Value, idItem);
            gridRequisition.DataBind();
        }

        protected void linkGAReq_Click(object sender, EventArgs e)
        {
            try
            {
                QuoObject quo = (QuoObject)ViewState[curQuo];
                if (quo.ID_REQ != 0)
                {
                    Response.Redirect("Requisition.aspx?ViewRequisition=" + quo.ID_REQ.ToString());
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "linkGAReq show error", "showException('Error', '" + HttpUtility.HtmlEncode(ex.Message) + "');", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "linkGAReq hide progress", "closeLoading();", true);
            }
        }

        #region JobCode
        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;
            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            txtJobCodeReq.Text = jobcode;

            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "');", true);
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            gridJobCode.DataSource = Helper.PFNATTHelper.getActiveJobCode(ddlJobCode.SelectedItem.Text, search);
            
        }

        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            try
            {
                loadSearchResultJobCode();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchJobCode hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchJobCode set to center", "setCenter('dialogJobCode');", true);
            }
        }

        #endregion

        protected void btnSearchReq_Click(object sender, EventArgs e)
        {
            try
            {
                loadAvalaibleRequisition();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchReq hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchReq set to center", "setCenter('dialogJobCode');", true);
            }
        }

        protected void gridRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridRequisition.PageIndex = e.NewPageIndex;
            loadAvalaibleRequisition();
            ScriptManager.RegisterStartupScript(this, GetType(), "center requisition", "setCenter('dialogRequisition');", true);
        }

        private void loadItemOnselectedReq(int idReq)
        {
            gridItemReq.DataSource = Helper.QuotHelper.getRequisitionDetail(idReq);
            gridItemReq.DataBind();
        }

        protected void gridRequisition_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = int.Parse(gridRequisition.SelectedRow.Cells[0].Text);
            lblIDReq.Text = id.ToString();
            loadItemOnselectedReq(id);
            ScriptManager.RegisterStartupScript(this, GetType(), "open item req", "openDialogItemReq();", true);
        }

        protected void gridItemReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemReq.PageIndex = e.NewPageIndex;
            int id = int.Parse(lblIDReq.Text);
            loadItemOnselectedReq(id);
            ScriptManager.RegisterStartupScript(this, GetType(), "center requisition", "setCenter('dialogItemReq');", true);
        }

        private void generateReqToQuo(RequisitionObject req)
        {
            QuoObject quo = (QuoObject)ViewState[curQuo];
            quo.ID_REQ = req.ID_REQ;
            quo.JOBCODE = req.JOBCODE;
            quo.PROJECT_NAME = req.PROJECT_NAME;
            quo.REQ_BY = req.REQ_BY;
            quo.REQ_BY_DEPT_ID = req.REQ_BY_DEPT_ID;
            quo.REQ_BY_NAME = req.REQ_BY_NAME;
            quo.REQ_BY_DEPT = req.REQ_BY_DEPT;
            quo.REQ_NO = req.GA_REQ_NO;
            quo.DATE_CREATED = DateTime.Now;
            ViewState[curQuo] = quo;
            onEditState();
        }

        protected void btnGetSelectedItem_Click(object sender, EventArgs e)
        {
            try
            {
                int idReq = 0;
                if (int.TryParse(lblIDReq.Text, out idReq))
                {
                    QuoObject quo = (QuoObject)ViewState[curQuo];
                    RequisitionObject req = Helper.RequisitionHelper.getRequisitionByID(idReq);
                    List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                    List<QuoVendor> vendors = (List<QuoVendor>)ViewState[curVendor];

                    if (quo.ID_REQ > 0)
                    {
                        quo = new QuoObject();
                        items = new List<RequisitionItems>();
                        vendors = new List<QuoVendor>();
                        generateRecommVendor();
                        generateRequestor();
                    }


                    foreach (GridViewRow row in gridItemReq.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkRow = (row.Cells[8].FindControl("checkItem") as CheckBox);
                            if (chkRow.Checked)
                            {
                                if (quo.ID_REQ <= 0)
                                    generateReqToQuo(req);
                                int idDetail = int.Parse(row.Cells[1].Text);
                                RequisitionItems item = Helper.QuotHelper.getItemFromReqDetail(idDetail);
                                items.Add(item);
                                ViewState[curItem] = items;
                            }

                        }
                    }
                    foreach (GridViewRow row in gridItemReq.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkRow = (row.Cells[8].FindControl("checkItem") as CheckBox);
                            chkRow.Checked = false;
                        }
                    }
                    loadQuoItem();

                    ScriptManager.RegisterStartupScript(this, GetType(), "close item req", "closeItemReq();", true);
                }
            }
            catch (Exception sltItem)
            {
                string er = HttpUtility.HtmlEncode(sltItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGetSelectedItem item select Error", "showMsg('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGetSelectedItem hide progress", "closeLoading();", true);
            }
        }

        protected void gridItemReq_PreRender(object sender, EventArgs e)
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            QuoObject quo = (QuoObject)ViewState[curQuo];
            int idReq = 0;
            bool isIDReq = int.TryParse(lblIDReq.Text, out idReq);

            foreach (GridViewRow row in gridItemReq.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    int idQuo = 0, idAdvance = 0;
                    bool isQuo = int.TryParse(row.Cells[6].Text, out idQuo);
                    bool isAdvance = int.TryParse(row.Cells[7].Text, out idAdvance);

                    if ((idQuo != 0 || idAdvance != 0)
                        || (quo.ID_REQ == idReq && items.Any(x => x.PART_ID == int.Parse(row.Cells[2].Text))))
                    {
                        CheckBox chkRow = (row.FindControl("checkItem") as CheckBox);
                        Label lblRemarkItem = (row.FindControl("lblRemarkItem") as Label);
                        if (chkRow != null)
                        {
                            row.BackColor = Color.FromName("#EDBD9D");
                            chkRow.Visible = false;
                            if (lblRemarkItem != null)
                            {
                                if (idAdvance != 0)
                                    lblRemarkItem.Text = "Advance No. " + idAdvance.ToString();
                                else if (idQuo != 0)
                                    lblRemarkItem.Text = "Quotation No. " + idQuo.ToString();
                            }
                        }
                    }
                }
            }
        }

        private void loadRequisitionItem()
        {
            string search = "";
            if (ddlItem.SelectedItem.Text.Equals("Type"))
                search = ddlItemType.SelectedItem.Text;
            else
                search = txtSearchItem.Text;

                gridItemSearch.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
                gridItemSearch.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "set item center", "setCenter('dialogItem')", true);
            
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            try
            {
                loadRequisitionItem();
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showMsg('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem set Item Center", "setCenter('dialogItem');", true);
            }
        }

        protected void gridItemSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemSearch.PageIndex = e.NewPageIndex;
            loadRequisitionItem();
        }

        protected void gridItemSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idItem = 0;
            bool isIdItemINT = int.TryParse(gridItemSearch.SelectedRow.Cells[1].Text, out idItem);
            string itemName = gridItemSearch.SelectedRow.Cells[3].Text;

            if (isIdItemINT)
            {
                txtItemID.Text = idItem.ToString();
                txtItem.Text = itemName.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "close Dialog Item", "closeDialogItem();", true);
            }
        }

        protected void gridSelectedVendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int id = int.Parse(e.CommandArgument.ToString());
                List<QuoVendor> vendors = (List<QuoVendor>)ViewState[curVendor];
                QuoVendor vendor = vendors.FirstOrDefault(x => x.ID_VENDOR == id);   
                if (e.CommandName.Equals("Delete Vendor"))
                {
                    vendors.Remove(vendor);
                    ViewState[curVendor] = vendors;
                    loadSelectedVendor();
                    generateRecommVendor();
                    btnAddVendor.Visible = true;
                }
                else if (e.CommandName.Equals("Edit Vendor"))
                {
                    if (vendor.QUOT_NO.Equals("No Quote"))
                    {
                        txtQuoDate.Enabled = false;
                        txtTechAspect.Enabled = false;
                        txtQuoPrice.Enabled = false;
                        txtDiscount.Enabled = false;
                        ddlVAT.ClearSelection();
                        ddlVAT.Enabled = false;
                        ddlCurrency.Enabled = false;
                        txtDeliveryTime.Enabled = false;
                        ddlPaymentTerms.Enabled = false;
                        txtPayTermOthers.Enabled = false;
                        txtWarranty.Enabled = false;
                    }
                    else
                    {
                        txtQuoDate.Enabled = true;
                        txtTechAspect.Enabled = true;
                        txtQuoPrice.Enabled = true;
                        txtDiscount.Enabled = true;
                        ddlVAT.ClearSelection();
                        ddlVAT.Enabled = true;
                        ddlCurrency.Enabled = true;
                        txtDeliveryTime.Enabled = true;
                        ddlPaymentTerms.Enabled = true;
                        txtPayTermOthers.Enabled = true;
                        txtWarranty.Enabled = true;
                        txtExchangeRate.Enabled = true;
                    }
                    lblNetPrice.Text = string.Format("{0:#,##0.00}", vendor.NET_PRICE);
                    txtQuotationNo.Text = vendor.QUOT_NO;
                    txtQuoPrice.Text = vendor.QUO_PRICE.ToString();
                    txtQuoDate.Text = vendor.QUOT_DATE.ToString("MM/dd/yyyy").Equals("01/01/1753") ? "" : vendor.QUOT_DATE.ToString("MM/dd/yyyy");
                    txtIDDetail.Text = vendor.ID.ToString();
                    txtVendorID.Text = vendor.ID_VENDOR.ToString();
                    txtVendor.Text = vendor.VENDOR_NAME;
                    txtTechAspect.Text = vendor.TECHNICAL_ASPECT;
                    txtQuoPrice.Text = vendor.QUO_PRICE.ToString();
                    txtDiscount.Text = vendor.DISCOUNT.ToString();
                    ddlVAT.SelectedIndex = ddlVAT.Items.IndexOf(ddlVAT.Items.FindByValue(vendor.VAT));
                    ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(vendor.CURRENCY));
                    txtDeliveryTime.Text = vendor.DELIVERY_TIME;
                    ddlPaymentTerms.SelectedIndex = ddlPaymentTerms.Items.IndexOf(ddlPaymentTerms.Items.FindByValue(vendor.PAYMENT_TERMS));
                    //if(!vendor.PAYMENT_TERMS.Equals("Cash On Delivery") && !vendor.PAYMENT_TERMS.Equals("14 days") && !vendor.PAYMENT_TERMS.Equals("30 days"))
                    //    txtPayTermOthers.Attributes["class"] = txtPayTermOthers.Attributes["class"].Replace("hidden", "");
                    //else{
                    //    if (txtPayTermOthers.Attributes["class"].Contains("hidden"))
                    //        txtPayTermOthers.Attributes.Add("class", "hidden");
                    //}
                    txtWarranty.Text = vendor.WARRANTY;
                    txtExchangeRate.Text = vendor.EXCHANGE_RATE.ToString();
                    //ScriptManager.RegisterStartupScript(this, GetType(), "for editing",
                    //    "dialogDetailOnEdit('" + vendor.ID + "', '" + vendor.ID_VENDOR + "', '" + vendor.VENDOR_NAME +
                    //    "', '" + vendor.QUOT_NO + "', '" + vendor.QUOT_DATE.ToString("MM/dd/yyyy") + "', '" + vendor.TECHNICAL_ASPECT + "', '" + vendor.QUO_PRICE +
                    //    "', '" + vendor.DISCOUNT + "', '" + vendor.VAT + "', '" + vendor.VAT_AMOUNT + "', '" + vendor.NET_PRICE + "', '" + vendor.CURRENCY + "', '" + vendor.DELIVERY_TIME +
                    //    "', '" + vendor.PAYMENT_TERMS + "', '" + vendor.WARRANTY + "');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "sqwe", "attachDate();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "ozxs", "editDialogDetail('"+vendor.QUOT_DATE.ToString("MM/dd/yyyy")+"', '"+vendor.PAYMENT_TERMS+"', '"+vendor.EXCHANGE_RATE+"');", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "open dialog for editing", "openDialogDetail();", true);
                    
                }
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "on edit error", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
        }

        protected void linkSelectReq_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "open dialog req", "openDialogRequisition();", true);
        }

        protected void btnGeneratePrice_Click(object sender, EventArgs e)
        {
            try
            {
                double quoPrice = 0, disc = 0, vat = 0;
                bool isPrice = double.TryParse(txtQuoPrice.Text.Replace(",", ""), out quoPrice);
                bool isDisc = double.TryParse(txtDiscount.Text.Replace(",", ""), out disc);
                if (isPrice && ddlVAT.SelectedItem.Text.Contains("Exclude"))
                    vat = quoPrice * 0.1;
                else vat = 0;
                lblNetPrice.Text = string.Format("{0:#,##0.00}", (quoPrice - disc + vat));

                
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGeneratePrice on edit error", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGeneratePrice hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGeneratePrice set Exchage Rate", "setExchangeRate();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGeneratePrice set Jquery button", "setButton();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnGeneratePrice attach date", "attachDate();", true);
            }
        }

        protected void btnReloadPage_Click(object sender, EventArgs e)
        {
            QuoObject quo = (QuoObject)ViewState[curQuo];
            if (quo.ID_QUO > 0)
                Response.Redirect("Quotation.aspx?ViewQuotation=" + quo.ID_QUO.ToString());
            Response.Redirect(Request.RawUrl);
        }

        //protected void linkPrint_Click(object sender, EventArgs e)
        //{

            

        //}

        private void printQuotation(int formatPrint)
        {
            string template = "";
            string newFile = "";
            string fileName = "";
            string fileFormat = "";
            QuoObject quo = (QuoObject)ViewState[curQuo];
            try
            {
                List<QuoVendor> vendors = (List<QuoVendor>)ViewState[curVendor];
                template = Server.MapPath("~/Report/Quotation.xlsx");
                newFile = "Quotation_" + quo.REQUEST_ITEM.Replace(" ", "_") + "_" + DateTime.Now.ToString("dd-MMM-yyyy");
                newFile = Server.MapPath("~/Report/" + newFile + ".xlsx");

                if (!File.Exists(newFile))
                {
                    File.Copy(template, newFile);
                }

                ReportHelper.quotationToExcel(newFile, quo, vendors);
                if (formatPrint == 1)
                {
                    fileName = ReportHelper.excelToPDF(newFile);
                    fileFormat = "application/pdf";
                }
                else if (formatPrint == 2)
                {
                    fileName = newFile;
                    fileFormat = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                
                int Length = (int)fs.Length;
                byte[] Buffer = new byte[Length];
                fs.Read(Buffer, 0, Length);
                fs.Close();
                ViewState[dlFile] = Buffer;
                ViewState[flName] = fileName;
                ViewState[flFormat] = fileFormat;
               
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "on edit error", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
            finally
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "hide - loading", "$(function(){ $('#divLoading').hide(); };)", true);
                FileInfo pdfFile = new FileInfo(fileName);
                if (pdfFile.Exists)
                    pdfFile.Delete();

                FileInfo excFile = new FileInfo(newFile);
                if (excFile.Exists)
                    excFile.Delete();
                //Response.Redirect("Quotation.aspx?ViewQuotation=" + quo.ID_QUO);
            }

        }

        protected void gridSelectedVendor_PreRender(object sender, EventArgs e)
        {
            for (int i = 0; i < gridSelectedVendor.Rows.Count; i++)
            {
                Label price = (Label)gridSelectedVendor.Rows[i].FindControl("lblQuoPrice");
                Label disc = (Label)gridSelectedVendor.Rows[i].FindControl("lblDisc");
                Label netPrice = (Label)gridSelectedVendor.Rows[i].FindControl("lblNetPrice");
                Label lblDate = (Label)gridSelectedVendor.Rows[i].FindControl("lblDate");
                
                if (price != null && disc != null && netPrice != null && lblDate != null)
                {
                    price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text));
                    disc.Text = string.Format("{0:#,##0.00}", double.Parse(disc.Text));
                    netPrice.Text = string.Format("{0:#,##0.00}", double.Parse(netPrice.Text));
                    lblDate.Text = lblDate.Text.Contains("01 Jan 1753") ? "" : lblDate.Text;
                }
            }
        }

        protected void gridItem_PreRender(object sender, EventArgs e)
        {
            for (int i = 0; i < gridItem.Rows.Count; i++)
            {
                var quantity = (Label)gridItem.Rows[i].FindControl("lblQuantity");
                var price = (Label)gridItem.Rows[i].FindControl("lblPrice");
                if (price != null && quantity != null)
                {
                    price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text));
                    quantity.Text = string.Format("{0:#,##0.00}", double.Parse(quantity.Text));
                }
            }
        }

        protected void gridRequisition_PreRender(object sender, EventArgs e)
        {
            QuoObject quo = (QuoObject)ViewState[curQuo];
            foreach (GridViewRow row in gridRequisition.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    if (row.Cells[0].Text.Equals(quo.ID_REQ.ToString()))
                        row.BackColor = Color.FromName("#EDBD9D");
                }
            }
        }

        public bool uploadFile(string fileName, out string message)
        {
            if (fileName == "")
            {
                message = "Invalid filename supplied";
                return false;
            }
            if (fileAttachmentUpload.PostedFile.ContentLength == 0)
            {
                message = "Invalid file content";
                return false;
            }
            fileName = System.IO.Path.GetFileName(fileName);
            try
            {
                if (fileAttachmentUpload.PostedFile.ContentLength <= 20971520)
                {
                    string AttachmentDir = Server.MapPath("/IPS/PDF");
                    AttachmentDir = AttachmentDir + "\\" + fileName;
                    fileAttachmentUpload.PostedFile.SaveAs(AttachmentDir);
                    message = "File uploaded successfully";
                    return true;
                }
                else
                {
                    message = "Unable to upload,file exceeds maximum limit";
                    return false;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        protected void btnNoQuote_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnNoQuote.Text.Equals("No Quote"))
                {
                    txtQuotationNo.Text = "No Quote";
                    btnNoQuote.Text = "Insert Quote";
                    txtQuoDate.Text = "";
                    txtQuoDate.Enabled = false;
                    txtTechAspect.Text = "";
                    txtTechAspect.Enabled = false;
                    txtQuoPrice.Text = "";
                    txtQuoPrice.Enabled = false;
                    txtDiscount.Text = "";
                    txtDiscount.Enabled = false;
                    ddlVAT.ClearSelection();
                    ddlVAT.Enabled = false;
                    lblNetPrice.Text = "";
                    ddlCurrency.Text = "";
                    ddlCurrency.Enabled = false;
                    txtDeliveryTime.Text = "";
                    txtDeliveryTime.Enabled = false;
                    ddlPaymentTerms.ClearSelection();
                    ddlPaymentTerms.Enabled = false;
                    txtPayTermOthers.Text = "";
                    txtPayTermOthers.Enabled = false;
                    txtWarranty.Text = "";
                    txtWarranty.Enabled = false;
                    txtExchangeRate.Text = "0";
                    txtExchangeRate.Enabled = false;
                }
                else
                {
                    txtQuotationNo.Text = "";
                    btnNoQuote.Text = "No Quote";
                    txtQuoDate.Enabled = true;
                    txtTechAspect.Enabled = true;
                    txtQuoPrice.Enabled = true;
                    txtDiscount.Enabled = true;
                    ddlVAT.ClearSelection();
                    ddlVAT.Enabled = true;
                    ddlCurrency.Enabled = true;
                    txtDeliveryTime.Enabled = true;
                    ddlPaymentTerms.Enabled = true;
                    txtPayTermOthers.Enabled = true;
                    txtWarranty.Enabled = true;
                    txtExchangeRate.Enabled = true;
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "btnNoQuote set button", "setButton();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnNoQuote set button", "attachDate();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnNoQuote show error", "showException('Error', '" + HttpUtility.HtmlEncode(ex.Message) + "');", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnNoQuote hide progress", "closeLoading();", true);
            }
        }

        private void setLinkPDF()
        {
            QuoObject quo= (QuoObject)ViewState[curQuo];
            if (quo.ATTACHMENT != null)
            {
                if (quo.ATTACHMENT.Length > 0)
                {
                    string pageURL = "http://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath +
                       "/ViewPDF.aspx?ViewAttachment=" + quo.ID_QUO + "&Type=QUO";
                    linkPDF.OnClientClick = String.Format("window.open('{0}','_newtab'); return false;", ResolveUrl(pageURL));
                }
            }
            else
            {   
                linkPDF.OnClientClick = "return false;";
                linkPDF.Visible = false;
            }
        }

        protected void linkPO_Click(object sender, EventArgs e)
        {
            try
            {
                QuoObject quo = (QuoObject)ViewState[curQuo];
                if (quo.ID_PO > 0)
                    Response.Redirect("PurchaseOrder.aspx?ViewPO=" + quo.ID_PO);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "linkPO hide progress", "closeLoading();", true);
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdoFormat.SelectedItem != null)
                {
                    printQuotation(int.Parse(rdoFormat.SelectedItem.Value));
                    ScriptManager.RegisterStartupScript(this, GetType(), "btnPrint download_file", "downloadFile();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "btnPrint show error", "showMsg('Error', 'Please select document format');", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnPrint show exception", "showException('Error', '" + HttpUtility.HtmlEncode(ex.Message) + "');", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnPrint hide progress", "closeLoading();", true);
            }
        }

        protected void btnDownloadDoc_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = ViewState[flName].ToString();
                string fileFormat = ViewState[flFormat].ToString();
                byte[] Buffer = (byte[])ViewState[dlFile];

                string docName = (fileName.Substring(fileName.LastIndexOf("\\") + 1)).Replace("\\", "");
                Response.Clear();
                Response.ClearHeaders();

                Response.BinaryWrite(Buffer);
                Response.ContentType = fileFormat;
                Response.AddHeader("content-disposition", "attachment; filename=" + docName);
                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnDownloadDoc show error", "showException('Error', '" + HttpUtility.HtmlEncode(ex.Message) + "');", true);
            }
            finally
            {
                try
                {
                    HttpContext.Current.Response.End();
                }
                finally
                {
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    ScriptManager.RegisterStartupScript(this, GetType(), "btnDownloadDoc hide progress", "closeLoading();", true);
                }
            }

        }

    }

}