﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;

namespace IntegratedPurchasingSystem
{
    public partial class QuotTracking : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            if (!IsPostBack)
            {
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                QuotHelper.setDDLYearForQuotation(ddlYear);
                if(!logInUser.ISADMIN && !QuotHelper.isQuotationParticipant(logInUser.EMP_ID))
                    Response.Redirect("NotAuthorized.aspx");

                loadQuotation();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            base.Render(writer);
        }

        private void loadQuotation()
        {
            logInUser = (Employee)ViewState[curUser];
            try
            {
                gridQuotation.DataSource = Helper.QuotHelper.getFilteredQuotation(int.Parse(ddlMonth.SelectedItem.Value),
                    int.Parse(ddlYear.SelectedItem.Text), txtReqNo.Text , int.Parse(ddlFilterStatus.SelectedItem.Value),
                    txtRequestor.Text, txtJobCode.Text, logInUser.EMP_ID);
                gridQuotation.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "setButton", "setButton();", true);
            }
            catch (Exception erReq)
            {
                string er = HttpUtility.HtmlEncode(erReq.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void gridQuotation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridQuotation.PageIndex = e.NewPageIndex;
            loadQuotation();
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            loadQuotation();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        protected void gridQuotation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();

            if (e.CommandName.Equals("View"))
                Response.Redirect("Quotation.aspx?ViewQuotation=" + id);
            else if (e.CommandName.Equals("Delete Quo"))
            {
                Helper.QuotHelper.deleteQuotation(int.Parse(id));
                loadQuotation();
            }
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;

            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            txtJobCode.Text = jobcode;
            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "');", true);
        }

        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            try
            {
                gridJobCode.DataSource = Helper.PFNATTHelper.getActiveJobCode(ddlJobCode.SelectedItem.Text, search);
                gridJobCode.DataBind();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            loadSearchResultJobCode();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set to center", "setCenter('dialogJobCode');", true);
        }

 
    }
}