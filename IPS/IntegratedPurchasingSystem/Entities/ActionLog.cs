﻿using System;

namespace IntegratedPurchasingSystem.Entities
{
    public class ActionLog
    {
        public int ID_ACTION { get; set; }
        public string MODIFIED_BY { get; set; }
        public string MODIFIED_BY_NAME { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public int ACTION_ID { get; set; }
        public string ACTION_TEXT { get; set; }
        public int REQ_NO { get; set; }
        public string REQ_TYPE { get; set; }
        public string COMMENT { get; set; }
    }
}