﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class 
        RelatedDetail
    {
        public RelatedDetail() { }
        
        public RelatedDetail(int idReq, int idReqDetail, string reqNo, double quantity, double addQuantity)
        {
            ID_REQ = idReq;
            ID_REQ_DETAIL = idReqDetail;
            DEPT_REQ_NO = reqNo;
            QUANTITY = quantity;
            ADDITIONAL_QUANTITY = addQuantity;
        }

        public RelatedDetail(int idReq, int idReqDetail, int idSumDetail, double addQuantity, int isAdditionalItem, string remark)
        {
            ID_REQ = idReq;
            ID_REQ_DETAIL = idReqDetail;
            ID_SUM_DETAIL = idSumDetail;
            ADDITIONAL_QUANTITY = addQuantity;
            ISADDITIONALITEM = isAdditionalItem;
            REMARK = remark;
        }

        public RelatedDetail(int idReq, int idReqDetail, int idSumDetail,  string reqNo, int itemID, string itemName, double quantity, 
            double addQuantity, string metric, double price, string remark, int isAdditional)
        {
            ID_REQ = idReq;
            ID_REQ_DETAIL = idReqDetail;
            ID_SUM_DETAIL = idSumDetail;
            DEPT_REQ_NO = reqNo;
            PART_ID = itemID;
            PART_NAME = itemName;
            QUANTITY = quantity;
            ADDITIONAL_QUANTITY = addQuantity;
            METRIC = metric;
            PRICE = price;
            REMARK = remark;
            ISADDITIONALITEM = isAdditional;
        }

        public int ID { get; set; }
        public int ID_SUM_DETAIL { get; set; }
        public int ID_REQ_DETAIL { get; set; }
        public int ID_REQ { get; set; }
        public string DEPT_REQ_NO { get; set; }
        public int PART_ID { get; set; }
        public string PART_NAME { get; set; }
        public string PART_CODE { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public double QUANTITY { get; set; }
        public double ADDITIONAL_QUANTITY { get; set; }
        public string METRIC { get; set; }
        public double PRICE { get; set; }
        public string REMARK { get; set; }
        public int ISADDITIONALITEM { get; set; }
    }
}