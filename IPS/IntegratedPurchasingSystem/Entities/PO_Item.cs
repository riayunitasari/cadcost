﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class PO_Item
    {
        public PO_Item(){ }
        public int ID { get; set; }
        public int ID_PO { get; set; }
        public int ID_ITEM { get; set; }
        public string ITEM_NAME { get; set; }
        public double QUANTITY { get; set; }
        public string METRIC { get; set; }
        public double PRICE { get; set; }
        public string REMARK { get; set; }
    }
}