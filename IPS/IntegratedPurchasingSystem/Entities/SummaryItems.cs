﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class SummaryItems
    {
        public SummaryItems() { }
        public SummaryItems(int itemID, string itemName, string partCode, int typeID, string typeName, double quantity, string metric, double price,
                string remark)
        {
            PART_ID = itemID;
            PART_NAME = itemName;
            PART_CODE = partCode;
            TYPE_ID = typeID;
            TYPE_NAME = typeName;
            QUANTITY = quantity;
            METRIC = metric;
            PRICE = price;
            REMARK = remark;
            RELATED_DETAIL = new List<RelatedDetail>();
            
        }

        public int ID_REQ_SUM_DETAIL { get; set; }
        public int PART_ID { get; set; }
        public string PART_CODE { get; set; }
        public string PART_NAME { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public double ADDITIONAL_QUANTITY { get; set; }
        public double QUANTITY { get; set; }
        public string METRIC { get; set; }
        public double PRICE { get; set; }
        public string CURRENCY { get; set; }
        public double AMOUNT { get; set; }
        public string REMARK { get; set; }
        public string DESCRIPTION { get; set; }
        public int ID_REQ { get; set; }
        public string REQ_NO { get; set; }
        public int ID_REQ_DETAIL { get; set; }
        public int ID_QUO { get; set; }
        public int ID_ADVANCE { get; set; }
        public List<RelatedDetail> RELATED_DETAIL { get; set; }
    }
}