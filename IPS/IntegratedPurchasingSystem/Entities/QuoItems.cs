﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    

    public class QuoItems
    {
        public QuoItems() { }

        public QuoItems(int itemID, string itemName, double quantity, string metric, double price,
            string remark) { 
            ID_ITEM = itemID;
            ITEM_NAME = itemName;
            QUANTITY = quantity;
            METRIC = metric;
            PRICE = price;
            REMARK = remark;
        }
        public int ID { get; set; }
        public int ID_REQ_DETAIL { get; set; }
        public int ID_ITEM { get; set; }
        public string ITEM_NAME { get; set; }
        public double QUANTITY { get; set; }
        public string METRIC { get; set; }
        public double PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string REMARK { get; set; }

        public int ID_QUO { get; set; }
    }
}