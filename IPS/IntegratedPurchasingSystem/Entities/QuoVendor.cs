﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{   
    [Serializable]
    public class QuoVendor
    {

        public QuoVendor() { }
        public QuoVendor(int idDetail, int idQuo, int idVendor, string vendorName, DateTime quoDate, string quoNo, 
            double quoPrice, double disc, string vat, double vatAmount, double netPrice, string currency, double exchangeRate, string delTime, 
            string warranty, string payTerms, string techSpec )
        {
            ID = idDetail;
            ID_QUO = idQuo;
            ID_VENDOR = idVendor;
            VENDOR_NAME = vendorName;
            QUOT_DATE = quoDate;
            QUOT_NO = quoNo;
            QUO_PRICE = quoPrice;
            DISCOUNT = disc;
            VAT = vat;
            VAT_AMOUNT = vatAmount;
            NET_PRICE = netPrice;
            CURRENCY = currency;
            EXCHANGE_RATE = exchangeRate;
            DELIVERY_TIME = delTime;
            WARRANTY = warranty;
            PAYMENT_TERMS = payTerms;
            TECHNICAL_ASPECT = techSpec;
        }

        public int ID { get; set; }
        public int ID_QUO { get; set; }
        public int ID_VENDOR { get; set; }
        public string VENDOR_NAME { get; set; }
        public DateTime QUOT_DATE { get; set; }
        public string QUOT_NO { get; set; }
        public double QUO_PRICE { get; set; }
        public double DISCOUNT { get; set; }
        public string VAT { get; set; }
        public double VAT_AMOUNT { get; set; }
        public double NET_PRICE { get; set; }
        public string CURRENCY { get; set; }
        public double EXCHANGE_RATE { get; set; }
        public string DELIVERY_TIME { get; set; }
        public string WARRANTY { get; set; }
        public string PAYMENT_TERMS { get; set; }
        public string TECHNICAL_ASPECT { get; set; }
        public string REMARKS { get; set; }

    }
}