﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    public class Vendor
    {
        public Vendor(int vndID, string vendorName)
        {
            vendorID = vndID;
            vndName = vendorName;

        }

        public int vendorID { get; set; }
        public string vendorCD { get; set; }
        public string vndName { get; set; }
        public string vndAddress { get; set; }
    }
}