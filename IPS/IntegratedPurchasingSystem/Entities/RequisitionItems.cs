﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    
    //dapat digunakan sebagai REQ_DETAIL atau ITEM
    public class RequisitionItems
    {
        public RequisitionItems() { }

        //public RequisitionItems(int itemID, string itemName, double quantity, string metric, double price,
        //    string remark)
        //{
        //    PART_ID = itemID;
        //    PART_NAME = itemName;
        //    QUANTITY = quantity;
        //    METRIC = metric;
        //    PRICE = price;
        //    REMARK = remark;
        //}

        public RequisitionItems(int itemID, string itemCode, string itemName, int idType, string typeName, double quantity, string metric, double price,
            string remark) { 
            PART_ID = itemID;
            PART_NAME = itemName;
            PART_CODE = itemCode;
            TYPE_ID = idType;
            TYPE_NAME = typeName;
            QUANTITY = quantity;
            METRIC = metric;
            PRICE = price;
            REMARK = remark;
        }
        public RequisitionItems(int idDetail, int itemID, string itemCode, string itemName, int idType, string typeName, double quantity, string metric, double price,
            string remark)
        {
            ID_DETAIL = idDetail;
            PART_ID = itemID;
            PART_NAME = itemName;
            PART_CODE = itemCode;
            TYPE_ID = idType;
            TYPE_NAME = typeName;
            QUANTITY = quantity;
            METRIC = metric;
            PRICE = price;
            REMARK = remark;
        }

        public int ID_REQ { get; set; }
        public int ID_DETAIL { get; set; }
        public int PART_ID { get; set; }
        public string PART_CODE { get; set; }
        public string PART_NAME { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public string TYPE { get; set; }
        public double QUANTITY { get; set; }
        public string METRIC { get; set; }
        public double PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string REMARK { get; set; }
        public int ID_QUO { get; set; }
        public string QUOTATION { get; set; }
        public int ID_ADVANCE { get; set; }

        //untuk bagian summary report
        public QuoObject QUOTATION_OBJECT { get; set; }
    }
}