﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class Employee
    {
        public Employee() { }

        public Employee(string id, string fullname, string sectName)
        {
            EMP_ID = id;
            FULL_NAME = fullname;
            SECT_NAME = sectName;
        }

        public string EMP_ID { get; set; }
        public string USERNAME { get; set; }
        public string EMAIL { get; set; }
        public string FULL_NAME { get; set; }
        public string SECT_ID { get; set; }
        public string SECT_NAME { get; set; }
        public string DEPT_ID { get; set; }
        public string DEPT_NAME { get; set; }
        public bool ISADMIN { get; set; }
        public bool ISPARTICIPANT { get; set; }
        public bool IS_CUR_PARTICIPANT { get; set; }
        public int APPRATER { get; set; }
        public int USER_ID { get; set; }
    }
}