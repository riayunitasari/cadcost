﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    public class Purchase
    {
        public Purchase() { }
        public int ID_REQ { get; set; }
        public List<int> ID_QUO { get; set; }
        public List<int> ID_ADVANCE { get; set; }
        public List<int> ID_PO { get; set; }
        public int ID_REQ_SUM { get; set; }
        public string SUM_REQ_NO { get; set; }
        public string REQ_NO { get; set; }
        public string GA_REQ_NO { get; set; }
    }
}