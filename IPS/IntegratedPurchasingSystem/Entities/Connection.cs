﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace IntegratedPurchasingSystem.Entities
{
    public class Connection
    {
        public static string WFPCDebugConn
        { get { return ConfigurationManager.ConnectionStrings["WorkflowConnectionString"].ConnectionString; } }

        public static string PurchaseSysConn
        { get { return ConfigurationManager.ConnectionStrings["PurchaseSystemConnectionString"].ConnectionString; } }

        public static string PFNATTConn
        { get { return ConfigurationManager.ConnectionStrings["PFNATTConnectionString"].ConnectionString; } }

        public static string SOFIConn
        { get { return ConfigurationManager.ConnectionStrings["PertaBEConnectionString"].ConnectionString; } }

        public static string GA_APP_Conn
        { get { return ConfigurationManager.ConnectionStrings["GA_APPConnectionString"].ConnectionString; } }
    }
}