﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class RequisitionObject
    {
        
        public RequisitionObject() { }
        public RequisitionObject(string deptReqNo, string jobcode, string desc, string remark, string reqBy, DateTime createdDate, 
            string currency, double exchangeRate, string reqType, double total_est, string attachment) {
                DEPT_REQ_NO = deptReqNo;
                JOBCODE = jobcode;
                DESCRIPTION = desc;
                REMARKS = remark;
                REQ_BY = reqBy;
                DATE_CREATED = createdDate;
                CURRENCY_CODE = currency;
                EXCHANGE_RATE = exchangeRate;
                REQ_TYPE = reqType;
                TOTAL_EST_AMOUNT = total_est;
                ATTACHMENT = attachment;
        }

        public int ID_REQ { get; set; }
        public string GA_REQ_NO { get; set; }
        public string DEPT_REQ_NO { get; set; }
        public string JOBCODE { get; set; }
        public string PROJECT_NAME { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public string REQ_BY { get; set; }
        public string REQ_BY_NAME { get; set; }
        public string REQ_BY_DEPT_ID { get; set; }
        public string REQ_BY_DEPT { get; set; }
        public string DESCRIPTION { get; set; }
        public string REMARKS { get; set; }
        public double TOTAL_EST_AMOUNT { get; set; }
        public string CURRENCY_CODE { get; set; }
        public double EXCHANGE_RATE { get; set; }
        public string REQ_TYPE { get; set; }
        public string REQ_TYPE_TEXT { get; set; }
        public int ISDELETED { get; set; }
        public int CONV_TO_QUOT { get; set; }
        public int CONV_TO_SUM { get; set; }
        public int ACTIVITY_ID { get; set; }
        public string ACTIVITY_TEXT { get; set; }
        public string ATTACHMENT { get; set; }
        public int ID_PO { get; set; }
        public string PO_NO { get; set; }
        //untuk summary report
        public  List<RequisitionItems> ITEMS {get; set;}

    }
}