﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    public class PurchaseDetail
    {
        public PurchaseDetail() { }
        public int ID_REQ { get; set; }
        public int REQ_ACTIVITY_ID { get; set; }
        public string REQ_ACTIVITY_TEXT { get; set; }
        public int ID_REQ_DETAIL { get; set; }
        public string ITEM_NAME { get; set; }
        public int ID_QUO { get; set; }
        public int QUO_ACTIVITY_ID { get; set; }
        public string QUO_ACTIVITY_TEXT { get; set; }
        public int ID_ADVANCE { get; set; }
        public int ADVANCE_ACTIVITY_ID { get; set; }
        public string ADVANCE_ACTIVITY_TEXT { get; set; }
        public int ID_PO { get; set; }
        public int PO_ACTIVITY_ID { get; set; }
        public string PO_ACTIVITY_TEXT { get; set; }
        public int ID_REQ_SUM { get; set; }
        public string SUM_REQ_NO { get; set; }
        public string REQ_NO { get; set; }
        public string GA_REQ_NO { get; set; }
    }
}