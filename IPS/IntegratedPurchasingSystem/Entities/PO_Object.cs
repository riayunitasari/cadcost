﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class PO_Object
    {
        public PO_Object() { }

        public int ID { get; set; }
        public int ID_REQUEST { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string REQUEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_BY_NAME { get; set; }
        public string REQUESTED_BY { get; set; }
        public string REQUESTED_BY_NAME { get; set; }
        public string JOBCODE { get; set; }
        public string PROJECT_NAME { get; set; }
        public int VENDOR_ID { get; set; }
        public string VENDOR_NAME { get; set; }
        public string VENDOR_PIC { get; set; }
        public string VENDOR_ADDRESS { get; set; }
        public string DELIVERY_TIME { get; set; }
        public string PLACE { get; set; }
        public string PAYMENT_TERM { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public string REMARK { get; set; }
        public string CURRENCY_CODE { get; set; }
        public double EXCHANGE_RATE { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public int ISDELETED { get; set; }
        public int ISSUBMITTED { get; set; }
        public int STATUSID { get; set; }
        public string STATUS_TEXT { get; set; }
        public int ACTIVITY_ID { get; set; }
        public string ACTIVITY_TEXT { get; set; }
        public string VAT { get; set; }
        public double VAT_AMOUNT { get; set; }
        public double DISCOUNT { get; set; }
        public double QUO_NET_PRICE { get; set; }
    }
}