﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Entities
{
    [Serializable]
    public class QuoObject
    {
        
        public QuoObject() { }
        //public QuoObject(int idReq, string reqNo, string jobcode, string remark, string reqBy, string creator, 
        //    int recommVendor, string recommVendorName)
        //{
        //    ID_REQ = idReq;
        //    REQ_NO = reqNo;
        //    JOBCODE = jobcode;
        //    REMARKS = remark;
        //    REQ_BY = reqBy;
        //    CREATED_BY = creator;
        //    RECOMM_VENDOR = recommVendor;
        //    RECOMM_VENDOR_NAME = recommVendorName;
        //}

        public int ID_QUO { get; set; }
        public int ID_REQ { get; set; }
        public string REQ_NO { get; set; }
        public string JOBCODE { get; set; }
        public string PROJECT_NAME { get; set; }
        public DateTime DATE_CREATED { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_BY_NAME { get; set; }
        public string REQUESTOR { get; set; }
        public string REQ_BY { get; set; }
        public string REQ_BY_NAME { get; set; }
        public string REQ_BY_DEPT_ID { get; set; }
        public string REQ_BY_DEPT { get; set; }
        public string REMARKS { get; set; }
        public int RECOMM_VENDOR { get; set; }
        public string RECOMM_VENDOR_NAME { get; set; }
        public string REQUEST_ITEM { get; set; }
        public int ISDELETED { get; set; }
        public int ISSUBMITED { get; set; }
        public int ACTIVITY_ID { get; set; }
        public string ACTIVITY_TEXT { get; set; }
        public string ATTACHMENT { get; set; }
        public int ID_PO { get; set;}
        public string PO_NO { get; set; }
        //untuk summar report
        public List<QuoVendor> VENDOR { get; set; }
    }
}