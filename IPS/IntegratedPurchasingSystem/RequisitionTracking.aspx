﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequisitionTracking.aspx.cs" Inherits="IntegratedPurchasingSystem.PurchaseTracking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/PageScript/reqTrackingPage.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Requisition Tracking
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormField">Requested Month</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Requested Year</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlYear" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">Request Type
                                </td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlReqType" runat="server">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                        <asp:ListItem Text="General Requisition" Value="RS"></asp:ListItem>
                                        <asp:ListItem Text="Stationary Requisition" Value="ST"></asp:ListItem>
                                        <asp:ListItem Text="IT Equipment Requisition" Value="SF"></asp:ListItem>
                                        <asp:ListItem Text="Advance" Value="AD"></asp:ListItem>
                                        <asp:ListItem Text="Summary - Stationary" Value="SST"></asp:ListItem>
                                        <asp:ListItem Text="Summary - Others" Value="SOT"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Status</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlFilterStatus" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="PREPARE REQUEST" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="FIX REQUEST" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY DEPT. MANAGER" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY PROJECT MANAGER" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY DIV. MANAGER" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY MANAGEMENT" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY GA MANAGER" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="GENERATE REQUEST" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="WAITING FOR ITEM DELIVERED" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="ON DELIVERY PROGRESS" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="DONE REQUEST" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="CANCELED REQUEST" Value="9"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtJobCode" runat="server"></asp:TextBox>
                                   <asp:Button ID="btnJobCode" runat="server" Text="..." />
                                   <asp:Button ID="btnResetJobCode" runat="server" Text="Reset" />
                                </td>
                                <td class="contentFormField">Contain Item</td>
                                <td class="contentFormData">
                                   <asp:TextBox ID="txtIDItem" runat="server" CssClass="hidden" ></asp:TextBox>
                                   <asp:TextBox ID="txtItem" runat="server" ></asp:TextBox>
                                   <asp:Button ID="btnItem" runat="server"  Text="..." />
                                   <asp:Button ID="btnResetItem" runat="server"  Text="Reset" />
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Requested By
                                </td>
                                <td class="contentFormData">
                                    <asp:Panel ID="pnlNonEmployee" runat="server">
                                        <asp:TextBox ID="txtRequestedBy" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnRequesteBy" runat="server" Text="..." />
                                        <asp:Button ID="btnResetRequestedBy" runat="server" Text="Reset" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmployee" runat="server">
                                        <asp:Label ID="lblRequestedBy" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </td>
                                <td class="contentFormField">Department</td>
                                <td class="contentFormData">
                                    <asp:Panel ID="pnlNonEmployeeDept" runat="server">
                                        <asp:DropDownList ID="ddlDept" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmployeeDept" runat="server">
                                        <asp:Label ID="lblDept" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </td>

                            </tr>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnFilter" runat="server" Text="Search" OnClick="btnFilter_Click" />
                        </div>
                        <br />
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Result
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail" style="padding: 20px;">
                        <asp:UpdatePanel ID="uptPanReservation" runat="server">
                            <ContentTemplate>
                                <asp:GridView AllowPaging="True" PageSize="10" ShowHeaderWhenEmpty="true" CssClass="mGrid"
                                    ID="gridRequisition" EmptyDataText="No records!" runat="server" AutoGenerateColumns="false" OnPreRender="gridRequisition_PreRender"
                                    OnPageIndexChanging="gridRequisition_PageIndexChanging" OnRowCommand="gridRequisition_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Request No" DataField="DEPT_REQ_NO" />
                                        <asp:TemplateField HeaderText="Requested By" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqBy" runat="server" Text='<%# Eval("REQ_BY")+" - "+ Eval("REQ_BY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Department" DataField="REQ_BY_DEPT" />
                                        <asp:BoundField DataField="JOBCODE" HeaderText="JobCode" />
                                        <asp:BoundField DataField="REQ_TYPE_TEXT" HeaderText="Request Type" />
                                        <asp:BoundField DataField="ACTIVITY_TEXT" HeaderText="Approval Status" />
                                        <asp:BoundField DataField="TOTAL_EST_AMOUNT" HeaderText="Total" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="EXCHANGE_RATE" HeaderText="ExchangeRate" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                                <div style="min-width: 150px; text-align:center;">
                                                    <div class="generalAction" style="display:inline-block;">
                                                        <asp:LinkButton ID="btnView" Text="View" runat="server" CommandName="View" title="View Detail"
                                                            CommandArgument='<%#Eval("ID_REQ") +";"+ Eval("REQ_TYPE")%> '></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="CancelR" title="Cancel"
                                                            CommandArgument='<%#Eval("ID_REQ") %>' OnClientClick="return confirm('Do you Want to Cancel this Request?');"></asp:LinkButton> --%>
                                                        <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Delete Req" title="Delete"
                                                            CommandArgument='<%#Eval("ID_REQ") %>' OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </div>
                                                    <%--<div class="approvalAction" runat="server" style="display:inline-block;">
                                                        <asp:LinkButton ID="btnApprove" Text="Approve" runat="server" CommandName="Approve" title="Approve"
                                                            CommandArgument='<%#Eval("ID_REQ") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="btnReject" Text="Return To Initiator" runat="server" CommandName="Return To Initiator" title="Return To Initiator"
                                                            CommandArgument='<%#Eval("ID_REQ") %>'></asp:LinkButton>
                                                    </div>--%>
                                                </div>
                                                   
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                        <asp:Button ID="btnSummaryReport" runat="server" Text="Summary Report" OnClick="btnSummaryReport_Click" />
                    </div>
                    
                </div>

                <div style="display: none">

                    <div id="dialogJobCode">
                        <table class="contentForm">
                            <tr>
                                <td style="min-width: 80px;">Search By</td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCode" runat="server">
                                        <asp:ListItem Text="JobCode"></asp:ListItem>
                                        <asp:ListItem Text="Project Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchJobCode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearchJobCode" runat="server" Text="Search" OnClick="btnSearchJobCode_Click" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridJobCode_PageIndexChanging" CellPadding="2"
                                            ID="gridJobCode" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridJobCode_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE_ID" />
                                                <asp:BoundField runat="server" HeaderText="Project Name" DataField="PROJECT_NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="dialogUser">
                    <table class="contentForm">
                        <tr>
                            <td style="min-width: 80px;">Search By</td>
                            <td>
                                <asp:DropDownList ID="ddlUser" runat="server">
                                    <asp:ListItem Text="Employee ID"></asp:ListItem>
                                    <asp:ListItem Text="Name"></asp:ListItem>
                                    <asp:ListItem Text="Department"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnSearchUser" runat="server" Text="Search" OnClick="btnSearchUser_Click" CausesValidation="false" /></td>
                        </tr>
                    </table>
                    <br />
                    <div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridEmployee_PageIndexChanging" 
                                        ID="gridEmployee" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                        OnSelectedIndexChanged="gridEmployee_SelectedIndexChanged" CssClass="mGrid">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="ID" DataField="EMP_ID" />
                                            <asp:BoundField HeaderText="Name" DataField="FULL_NAME" />
                                            <asp:BoundField HeaderText="Department" DataField="SECT_NAME" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearchUser" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

                    <div id="dialogItem">
                        <div>
                            <table>
                                <tr>
                                    <td>Search Item By</td>
                                    <td>
                                        <asp:DropDownList ID="ddlItem" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Type"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormField">
                                        <asp:TextBox ID="txtSearchItem" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlItemType" runat="server"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSearchItem" runat="server" Text="Search" OnClick="btnSearchItem_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPanelItem" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridItemSearch_PageIndexChanging"
                                            ID="gridItemSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridItemSearch_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="PART_ID" HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="PART_CODE" HeaderText="Item Code" />
                                                <asp:BoundField DataField="PART_NAME" HeaderText="Item Name" />
                                                <asp:BoundField DataField="TYPE_ID" HeaderText="ID Type" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="Type" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchItem" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>


                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
