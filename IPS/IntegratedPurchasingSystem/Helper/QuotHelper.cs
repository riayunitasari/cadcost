﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using IntegratedPurchasingSystem.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedPurchasingSystem.Helper
{
    public class QuotHelper
    {
        public static void setDDLYearForQuotation(DropDownList ddl)
        {
            ddl.ClearSelection();
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            var year = data.QUOTATIONs.GroupBy(x => x.DATE_CREATED.Value.Year).Select(x => x.First()).ToList();
            foreach (var i in year)
                ddl.Items.Add(i.DATE_CREATED.Value.Year.ToString());

            if (!year.Any())
                ddl.Items.Add(DateTime.Now.Year.ToString());
        }

        public static QuoObject getQuotationByID(int id)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getQuotationByID", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idQuot", id);

            connection.Open();
            QuoObject quo = new QuoObject();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                quo = (from r in reader.Cast<DbDataRecord>()
                       select new QuoObject
                       {
                           ID_QUO = (int)r["ID_QUO"],
                           ID_REQ = (int)r["ID_REQ"],
                           REQ_NO = r["REQ_NO"].ToString(),
                           JOBCODE = r["JOBCODE"].ToString(),
                           PROJECT_NAME = r["PROJECT_NAME"].ToString(),
                           DATE_CREATED = Convert.ToDateTime(r["DATE_CREATED"].ToString()),
                           REQ_BY = r["REQ_BY"].ToString(),
                           REQ_BY_NAME = r["REQ_BY_NAME"].ToString(),
                           REQ_BY_DEPT = r["REQ_BY_DEPT"].ToString(),
                           REMARKS = r["REMARKS"].ToString(),
                           ACTIVITY_ID = (int)r["ACTIVITY_ID"],
                           ACTIVITY_TEXT = r["ACTIVITY_TEXT"].ToString(),
                           RECOMM_VENDOR = (int)r["RECOMM_VENDOR"],
                           RECOMM_VENDOR_NAME = r["RECOMM_VENDOR_NAME"].ToString(),
                           REQUEST_ITEM = r["REQUEST_ITEM"].ToString(),
                           ISDELETED = (int)r["ISDELETED"],
                           ISSUBMITED = (int)r["ISSUBMITED"],
                           CREATED_BY = r["CREATED_BY"].ToString(),
                           CREATED_BY_NAME = r["CREATED_BY_NAME"].ToString(),
                           REQUESTOR = r["REQUESTOR"].ToString(),
                           ATTACHMENT = r["ATTACHMENT"].ToString(),
                           ID_PO = (int)r["ID_PO"]
                       }).FirstOrDefault();
            }
            connection.Close();

            return quo;
        }

        public static int getQuotationIDOfReq(int idReq)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            if (data.QUOTATIONs.Any(x => x.ID_REQ == idReq))
                return (int)data.QUOTATIONs.FirstOrDefault(x => x.ID_REQ == idReq).ID_QUO;
            else return 0;
        }

        public static List<QuoVendor> getQuotationDetail(int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<QuoVendor> details = (from q in data.QUO_DETAILs
                                        where q.ID_QUO == idQuo
                                        select new QuoVendor
                                        {
                                            ID = q.ID,
                                            ID_QUO = (int)q.ID_QUO,
                                            ID_VENDOR = (int)q.ID_VENDOR,
                                            NET_PRICE = (double)q.NET_PRICE,
                                            REMARKS = q.REMARKS,
                                            QUO_PRICE = (double)q.QUO_PRICE,
                                            DISCOUNT = (double)q.DISCOUNT,
                                            DELIVERY_TIME = q.DELIVERY_TIME,
                                            PAYMENT_TERMS = q.PAYMENT_TERM,
                                            WARRANTY = q.WARRANTY,
                                            QUOT_NO = q.QUOT_NO,
                                            QUOT_DATE = (DateTime)q.QUOT_DATE,
                                            VENDOR_NAME = q.VENDOR_NAME,
                                            TECHNICAL_ASPECT = q.TECHNICAL_ASPECT,
                                            VAT = q.VAT,
                                            VAT_AMOUNT = (double)q.VAT_AMOUNT,
                                            CURRENCY = q.CURRENCY,
                                            EXCHANGE_RATE = (double)q.EXCHANGE_RATE
                                        }).ToList(); ;
            return details;
        }

        public static List<RequisitionObject> getRequisitionForQuotation(string reqNo, int month, string jobcode, string type, int idItem)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getRequisitionForQuotation", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@reqMonth", month);
            command.Parameters.AddWithValue("@reqNo", reqNo);
            command.Parameters.AddWithValue("@reqType", type);
            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@idItem", idItem);

            connection.Open();
            List<RequisitionObject> reqs = new List<RequisitionObject>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new RequisitionObject
                        {
                            ID_REQ = (int)req["ID_REQ"],
                            DEPT_REQ_NO = req["REQ_NO"].ToString(),
                            JOBCODE = req["JOBCODE"].ToString(),
                            REQ_BY = req["REQ_BY"].ToString(),
                            REQ_BY_NAME = req["REQ_BY_NAME"].ToString(),
                            DATE_CREATED = (DateTime)req["DATE_CREATED"],
                            REQ_TYPE_TEXT = req["REQ_TYPE_TEXT"].ToString(),
                            REQ_BY_DEPT = req["REQ_BY_DEPT"].ToString(),

                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static List<RequisitionItems> getRequisitionDetail(int idReq)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getReqDetailItemForQuotation", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@idReq", idReq);

            connection.Open();
            List<RequisitionItems> items = new List<RequisitionItems>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                items = (from req in reader.Cast<DbDataRecord>()
                         select new RequisitionItems
                        {
                            ID_DETAIL = (int) req["ID"],
                            ID_REQ = (int) req["ID_REQ"],
                            PART_ID = (int) req["ID_ITEM"],
                            PART_NAME = req["ITEM_NAME"].ToString(), 
                            QUANTITY = Convert.ToDouble(req["QUANTITY"]),
                            METRIC = req["METRIC_CODE"].ToString(),
                            ID_QUO = (int)req["ID_QUO"],
                            ID_ADVANCE = (int)req["ID_ADVANCE"]
                        }).ToList();
            }
            connection.Close();

            return items;
        }

        public static RequisitionItems getItemFromReqDetail(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            RequisitionItems item = (from d in data.REQ_DETAILs
                                  join r in data.REQUISITIONs on d.ID_REQ equals r.ID_REQ
                                  where d.ID == idDetail
                                     select new RequisitionItems
                                  {
                                      ID_DETAIL = d.ID,
                                      ID_REQ = (int)d.ID_REQ,
                                      PART_ID = (int)d.ID_ITEM,
                                      PART_NAME = d.ITEM_NAME,
                                      PART_CODE = d.PART_CODE,
                                      TYPE_ID = Convert.ToInt32(d.PART_TYPE_ID),
                                      TYPE_NAME = d.PART_TYPE_NAME,
                                      QUANTITY = (double)d.QUANTITY,
                                      PRICE = (double)d.PRICE,
                                      CURRENCY = r.CURRENCY_CODE,
                                      METRIC = d.METRIC_CODE,
                                      REMARK = d.REMARKS,
                                  }).FirstOrDefault();
            return item;
        }

        public static void insertQuotation(QuoObject quo, out int id)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            id = 0;
            QUOTATION q = new QUOTATION();
            q.ID_REQ = quo.ID_REQ;
            q.REQ_NO = quo.REQ_NO;
            q.REQ_BY = quo.REQ_BY;
            q.JOBCODE = quo.JOBCODE;
            q.REQUESTOR = quo.REQUESTOR;
            q.DATE_CREATED = DateTime.Now;
            q.RECOMM_VENDOR = quo.RECOMM_VENDOR;
            q.RECOMM_VENDOR_NAME = quo.RECOMM_VENDOR_NAME;
            q.REQUEST_ITEM = quo.REQUEST_ITEM;
            q.ISDELETED = 0;
            q.ISSUBMITED = 0;
            q.CREATED_BY = quo.CREATED_BY;
            q.REMARKS = quo.REMARKS;
            q.REQUESTOR = quo.REQUESTOR;
            q.ATTACHMENT = quo.ATTACHMENT;
            q.ID_PO = 0;
            data.QUOTATIONs.InsertOnSubmit(q);
            data.SubmitChanges();
            id = q.ID_QUO;
        }

        public static void insertDetailVendor(List<QuoVendor> vendors, int idQuo)
        {
            foreach (var i in vendors)
                insertSingleDetailVendor(i, idQuo);
        }

        public static void insertSingleDetailVendor(QuoVendor vendor, int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            QUO_DETAIL detail = new QUO_DETAIL();
            detail.ID_QUO = idQuo;
            detail.ID_VENDOR = vendor.ID_VENDOR;
            detail.QUO_PRICE = vendor.QUO_PRICE;
            detail.NET_PRICE = vendor.NET_PRICE;
            detail.DISCOUNT = vendor.DISCOUNT;
            detail.VAT = vendor.VAT;
            detail.VAT_AMOUNT = vendor.VAT_AMOUNT;
            detail.QUOT_NO = vendor.QUOT_NO;
            detail.QUOT_DATE = vendor.QUOT_DATE;
            detail.DELIVERY_TIME = vendor.DELIVERY_TIME;
            detail.PAYMENT_TERM = vendor.PAYMENT_TERMS;
            detail.WARRANTY = vendor.WARRANTY;
            detail.VENDOR_NAME = vendor.VENDOR_NAME;
            detail.TECHNICAL_ASPECT = vendor.TECHNICAL_ASPECT;
            detail.CURRENCY = vendor.CURRENCY;
            detail.EXCHANGE_RATE = vendor.EXCHANGE_RATE;
            detail.REMARKS = vendor.REMARKS;

            data.QUO_DETAILs.InsertOnSubmit(detail);
            data.SubmitChanges();
        }

        public static void updateQuotation(QuoObject quo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            QUOTATION quot = data.QUOTATIONs.FirstOrDefault(x => x.ID_QUO == quo.ID_QUO);
            quot.REQUEST_ITEM = quo.REQUEST_ITEM;
            quot.REQUESTOR = quo.REQUESTOR;
            quot.RECOMM_VENDOR = quo.RECOMM_VENDOR;
            quot.RECOMM_VENDOR_NAME = quo.RECOMM_VENDOR_NAME;
            quot.ATTACHMENT = quo.ATTACHMENT;
            quot.REMARKS = quo.REMARKS;
            data.SubmitChanges();
        }
 
        public static void updateDetailQuotation(List<QuoVendor> vendors, int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<QUO_DETAIL> details = data.QUO_DETAILs.Where(x => x.ID_QUO == idQuo).ToList();
            for (int i = 0; i < details.Count; i++) //delete
            {
                bool isExist = vendors.Any(x => x.ID.Equals(details[i].ID));
                if (!isExist)
                    deleteSingleDetailVendor(details[i].ID);
            }

            for (int i = 0; i < vendors.Count; i++)// add new
            {
                bool isExist = details.Any(x => x.ID.Equals(vendors[i].ID));
                if (!isExist)
                    insertSingleDetailVendor(vendors[i], idQuo); //new
                else //already exist, update then
                    updateSingleDetailVendor(vendors[i].ID, vendors[i].QUO_PRICE, vendors[i].DISCOUNT, vendors[i].VAT,
                        vendors[i].VAT_AMOUNT, vendors[i].NET_PRICE, vendors[i].DELIVERY_TIME, vendors[i].PAYMENT_TERMS,
                        vendors[i].WARRANTY, vendors[i].QUOT_NO, vendors[i].QUOT_DATE, vendors[i].TECHNICAL_ASPECT,
                        vendors[i].CURRENCY, vendors[i].EXCHANGE_RATE, vendors[i].REMARKS);
            }
        }

        public static void deleteSingleDetailVendor(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            QUO_DETAIL d = data.QUO_DETAILs.Where(x => x.ID == idDetail).SingleOrDefault();
            data.QUO_DETAILs.DeleteOnSubmit(d);
            data.SubmitChanges();
        }

        public static void updateSingleDetailVendor(int idDetail, double quoPrice, double disc, string vat, double vatAmount,
            double netPrice, string delTime, string payTerms, string warranty, string quoNo, DateTime quoDate, string techAspect, 
            string currency, double exchangeRate, string remark)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            QUO_DETAIL detail = data.QUO_DETAILs.FirstOrDefault(x => x.ID == idDetail);
            if (detail != null)
            {
                detail.QUO_PRICE = quoPrice;
                detail.DISCOUNT = disc;
                detail.VAT = vat;
                detail.VAT_AMOUNT = vatAmount;
                detail.NET_PRICE = netPrice;
                detail.DELIVERY_TIME = delTime;
                detail.PAYMENT_TERM = payTerms;
                detail.WARRANTY = warranty;
                detail.QUOT_NO = quoNo;
                detail.QUOT_DATE = quoDate;
                detail.TECHNICAL_ASPECT = techAspect;
                detail.CURRENCY = currency;
                detail.REMARKS = remark;
                detail.EXCHANGE_RATE = exchangeRate;
            }
            data.SubmitChanges();
        }

        public static List<RequisitionItems> getQuotationItem(int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<RequisitionItems> items = (from d in data.REQ_DETAILs
                                            where d.ID_QUO == idQuo
                                            select new RequisitionItems
                                            {
                                                ID_REQ = (int)d.ID_REQ,
                                                ID_DETAIL = d.ID,
                                                PART_ID = (int)d.ID_ITEM,
                                                PART_NAME = d.ITEM_NAME,
                                                PART_CODE = d.PART_CODE,
                                                TYPE_ID = (int)d.PART_TYPE_ID,
                                                TYPE_NAME = d.PART_TYPE_NAME,
                                                QUANTITY = (double)d.QUANTITY,
                                                PRICE = (double)d.PRICE,
                                                METRIC = d.METRIC_CODE,
                                                REMARK = d.REMARKS,
                                            }).ToList();
            return items;
        }

        public static List<QuoObject> getFilteredQuotation(int reqMonth, int reqYear, string reqNo, int status, 
            string requestor, string jobcode, string empID)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getFilteredQuotation", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@reqMonth", reqMonth);
            command.Parameters.AddWithValue("@reqYear", reqYear);
            command.Parameters.AddWithValue("@reqNo", reqNo);
            command.Parameters.AddWithValue("@status", status);
            command.Parameters.AddWithValue("@requestor", requestor);
            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@empID", empID);

            connection.Open();
            List<QuoObject> quo = new List<QuoObject>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                quo = (from req in reader.Cast<DbDataRecord>()
                       select new QuoObject
                        {
                            ID_QUO = (int)req["ID_QUO"],
                            ID_REQ = (int)req["ID_REQ"],
                            REQ_NO = req["REQ_NO"].ToString(),
                            JOBCODE = req["JOBCODE"].ToString(),
                            DATE_CREATED = (DateTime)req["DATE_CREATED"],
                            REQUESTOR = req["REQUESTOR"].ToString(),
                            REQUEST_ITEM = req["REQUEST_ITEM"].ToString(),
                            ACTIVITY_ID = (int)req["ACTIVITY_ID"],
                            ACTIVITY_TEXT = req["ACTIVITY_TEXT"].ToString(),
                            RECOMM_VENDOR = (int)req["RECOMM_VENDOR"],
                            RECOMM_VENDOR_NAME = req["RECOMM_VENDOR_NAME"].ToString()
                        }).ToList();
            }
            connection.Close();

            return quo;
        }

        public static bool isQuotationParticipant(string empID)
        {
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);

            if (data.TBL_ACTION_HISTORies.Any(x => x.APP_ID == 2 && x.REQ_TYPE.Equals("QUO")
                && (x.PARTICIPANT.Contains(empID) || x.PARTICIPANT_ONBEHALF.Contains(empID))))
                return true;

            return false;
        }

        public static void deleteQuotation(int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            QUOTATION r = data.QUOTATIONs.FirstOrDefault(x => x.ID_QUO == idQuo);
            if (r != null)
                r.ISDELETED = 1;
            data.SubmitChanges();
            List<REQ_DETAIL> details = data.REQ_DETAILs.Where(x => x.ID_QUO == idQuo).ToList();
            foreach (var i in details)
            {
                i.ID_QUO = 0;
                data.SubmitChanges();
            }
        }
    }
}