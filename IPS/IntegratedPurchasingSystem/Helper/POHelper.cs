﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using IntegratedPurchasingSystem.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedPurchasingSystem.Helper
{
    public class POHelper
    {
        public static void setDDLYearForPO(DropDownList ddl)
        {
            ddl.ClearSelection();
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            var year = data.POs.GroupBy(x => x.CREATED_DATE.Value.Year).Select(x => x.First()).ToList();
            foreach (var i in year)
                ddl.Items.Add(i.CREATED_DATE.Value.Year.ToString());

            if (!year.Any())
                ddl.Items.Add(DateTime.Now.Year.ToString());
        }

        public static PO_Object getPOByID(int id){
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getPOByID", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idPO", id);

            connection.Open();
            PO_Object po = new PO_Object();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                po = (from r in reader.Cast<DbDataRecord>()
                       select new PO_Object
                       {
                            ID = (int)r["ID"],
                            ID_REQUEST = (int)r["ID_REQUEST"],
                            REQUEST_TYPE = r["REQUEST_TYPE"].ToString(),
                            REQUEST_NO = r["REQUEST_NO"].ToString(),
                            ORDER_NO =  r["ORDER_NO"].ToString(),
                            CREATED_DATE = (DateTime) r["CREATED_DATE"],
                            CREATED_BY = r["CREATED_BY"].ToString(),
                            CREATED_BY_NAME = r["CREATED_BY_NAME"].ToString(),
                            REQUESTED_BY = r["REQUESTED_BY"].ToString(),
                            REQUESTED_BY_NAME = r["REQUESTED_BY_NAME"].ToString(),
                            JOBCODE = r["JOBCODE"].ToString(),
                            PROJECT_NAME = r["PROJECT_NAME"].ToString(),
                            VENDOR_ID = (int)r["VENDOR_ID"],
                            VENDOR_NAME = r["VENDOR_NAME"].ToString(),
                            VENDOR_PIC = r["VENDOR_PIC"].ToString(),
                            DELIVERY_TIME = r["DELIVERY_TIME"].ToString(),
                            PLACE = r["PLACE"].ToString(),
                            PAYMENT_TERM = r["PAYMENT_TERM"].ToString(),
                            ITEM_DESCRIPTION = r["ITEM_DESCRIPTION"].ToString(),
                            REMARK =  r["REMARK"].ToString(),
                            CURRENCY_CODE = r["CURRENCY"].ToString(),
                            EXCHANGE_RATE = decimal.ToDouble((decimal)r["EXCHANGE_RATE"]),
                            TOTAL_AMOUNT = decimal.ToDouble((decimal)r["TOTAL_AMOUNT"]),
                            ISDELETED = (int)r["ISDELETED"],
                            ISSUBMITTED = (int)r["ISSUBMITTED"],
                            STATUSID = (int)r["STATUSID"],
                            STATUS_TEXT = r["STATUS_TEXT"].ToString(),
                            ACTIVITY_ID = (int)r["ACTIVITY_ID"],
                            ACTIVITY_TEXT = r["ACTIVITY_TEXT"].ToString(),
                            VENDOR_ADDRESS = r["VENDOR_ADDRESS"].ToString(),
                            VAT = r["VAT"].ToString(),
                            VAT_AMOUNT = decimal.ToDouble((decimal)r["VAT_AMOUNT"]),
                            DISCOUNT = decimal.ToDouble((decimal)r["DISCOUNT"]),
                            QUO_NET_PRICE = decimal.ToDouble((decimal)r["QUO_NET_PRICE"])
                       }).FirstOrDefault();
            }
            connection.Close();

            return po;
        }

        public static List<PO_Item> getPOItem(int idPO)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<PO_Item> items = (from d in data.PO_DETAILs
                                            where d.ID_PO == idPO
                                            select new PO_Item
                                            {   
                                                ID = (int)d.ID,
                                                ID_PO = (int)d.ID_PO,
                                                ID_ITEM = (int)d.ID_ITEM,
                                                ITEM_NAME = d.ITEM_NAME,
                                                QUANTITY = decimal.ToDouble((decimal)d.QUANTITY),
                                                PRICE = decimal.ToDouble((decimal)d.PRICE),
                                                METRIC = d.METRIC_CODE,
                                                REMARK = d.REMARK,
                                            }).ToList();
            return items;
        }

        public static void updatePO(PO_Object po)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            PO p = data.POs.FirstOrDefault(x => x.ID == po.ID);
            if (p != null)
            {
                p.PAYMENT_TERM = po.PAYMENT_TERM;
                p.VENDOR_ID = po.VENDOR_ID;
                p.VENDOR_NAME = po.VENDOR_NAME;
                p.VENDOR_PIC = po.VENDOR_PIC;
                p.VENDOR_ADDRESS = po.VENDOR_ADDRESS;
                p.DELIVERY_TIME = po.DELIVERY_TIME;
                p.CURRENCY = po.CURRENCY_CODE;
                p.EXCHANGE_RATE = (decimal?)po.EXCHANGE_RATE;
                p.ITEM_DESCRIPTION = po.ITEM_DESCRIPTION;
                p.VAT = po.VAT;
                p.VAT_AMOUNT = (decimal?)po.VAT_AMOUNT;
                p.DISCOUNT = (decimal?)po.DISCOUNT;
                p.TOTAL_AMOUNT = (decimal?)po.TOTAL_AMOUNT;
                p.REMARK = po.REMARK;
                data.SubmitChanges();
            }
        }

        public static void deleteSingleDetailPO(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            PO_DETAIL d = data.PO_DETAILs.Where(x => x.ID == idDetail).SingleOrDefault();
            data.PO_DETAILs.DeleteOnSubmit(d);
            data.SubmitChanges();
        }

        public static void updateSingleDetailPO(PO_Item item)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            PO_DETAIL detail = data.PO_DETAILs.FirstOrDefault(x => x.ID == item.ID);
            if (detail != null)
            {
                detail.PRICE = (decimal?)item.PRICE;
                detail.METRIC_CODE = item.METRIC;
                detail.REMARK = item.REMARK;
                data.SubmitChanges();
            }
        }

        public static void updateDetailPO(int idPo, List<PO_Item> items)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            List<PO_DETAIL> details = data.PO_DETAILs.Where(x => x.ID_PO == idPo).ToList();
            for (int i = 0; i < details.Count; i++) //delete
            {
                bool isExist = items.Any(x => x.ID.Equals(details[i].ID));
                if (!isExist)
                    deleteSingleDetailPO(details[i].ID);
                else
                    updateSingleDetailPO(items.FirstOrDefault(x => x.ID == details[i].ID));
            }
        }

        public static List<PO_Object> getFilteredPO(int reqMonth, int reqYear, string orderNo, int statusID,
            int activityID, string jobcode, int vendorID, int itemID)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getFilteredPO", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@reqMonth", reqMonth);
            command.Parameters.AddWithValue("@reqYear", reqYear);
            command.Parameters.AddWithValue("@orderNo", orderNo);
            command.Parameters.AddWithValue("@statusID", statusID);
            command.Parameters.AddWithValue("@activityID", activityID);
            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@vendorID", vendorID);
            command.Parameters.AddWithValue("@itemID", itemID);

            connection.Open();
            List<PO_Object> quo = new List<PO_Object>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                quo = (from r in reader.Cast<DbDataRecord>()
                       select new PO_Object
                       {
                           ID = (int)r["ID"],
                           ID_REQUEST = (int)r["ID_REQUEST"],
                           REQUEST_TYPE = r["REQUEST_TYPE"].ToString(),
                           REQUEST_NO = r["REQUEST_NO"].ToString(),
                           ORDER_NO = r["ORDER_NO"].ToString(),
                           CREATED_DATE = (DateTime)r["CREATED_DATE"],
                           CREATED_BY = r["CREATED_BY"].ToString(),
                           CREATED_BY_NAME = r["CREATED_BY_NAME"].ToString(),
                           REQUESTED_BY = r["REQUESTED_BY"].ToString(),
                           REQUESTED_BY_NAME = r["REQUESTED_BY_NAME"].ToString(),
                           JOBCODE = r["JOBCODE"].ToString(),
                           PROJECT_NAME = r["PROJECT_NAME"].ToString(),
                           VENDOR_ID = (int)r["VENDOR_ID"],
                           VENDOR_NAME = r["VENDOR_NAME"].ToString(),
                           VENDOR_PIC = r["VENDOR_PIC"].ToString(),
                           DELIVERY_TIME = r["DELIVERY_TIME"].ToString(),
                           PLACE = r["PLACE"].ToString(),
                           PAYMENT_TERM = r["PAYMENT_TERM"].ToString(),
                           ITEM_DESCRIPTION = r["ITEM_DESCRIPTION"].ToString(),
                           REMARK = r["REMARK"].ToString(),
                           CURRENCY_CODE = r["CURRENCY"].ToString(),
                           EXCHANGE_RATE = decimal.ToDouble((decimal)r["EXCHANGE_RATE"]),
                           TOTAL_AMOUNT = decimal.ToDouble((decimal)r["TOTAL_AMOUNT"]),
                           ISDELETED = (int)r["ISDELETED"],
                           ISSUBMITTED = (int)r["ISSUBMITTED"],
                           STATUSID = (int)r["STATUSID"],
                           STATUS_TEXT = r["STATUS_TEXT"].ToString(),
                           ACTIVITY_ID = (int)r["ACTIVITY_ID"],
                           ACTIVITY_TEXT = r["ACTIVITY_TEXT"].ToString(),
                           VENDOR_ADDRESS = r["VENDOR_ADDRESS"].ToString()
                       }).ToList();
            }
            connection.Close();

            return quo;
        }

        public static void deletePO(int idPO)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            PO po = data.POs.FirstOrDefault(x => x.ID == idPO);
            po.ISDELETED = 1;
            data.SubmitChanges();
        }
    }
}