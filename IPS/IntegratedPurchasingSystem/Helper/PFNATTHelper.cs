﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class PFNATTHelper
    {
        public static List<TBL_PROJECT> getActiveJobCode(string searchBy, string search)
        {
            string jobcode = "";
            string pName = "";

            if (searchBy.Equals("JobCode"))
                jobcode = search;
            else pName = search;

            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getActiveJobCode", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@projectName", pName);

            connection.Open();
            List<TBL_PROJECT> reqs = new List<TBL_PROJECT>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new TBL_PROJECT
                        {
                            JOBCODE_ID = req["JOBCODE_ID"].ToString(),
                            PROJECT_NAME = req["PROJECT_NAME"].ToString(),
                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static List<TBL_PROJECT> getAllJobCode(string searchBy, string search)
        {
            string jobcode = "";
            string pName = "";

            if (searchBy.Equals("JobCode"))
                jobcode = search;
            else pName = search;

            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getJobCode", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@projectName", pName);

            connection.Open();
            List<TBL_PROJECT> reqs = new List<TBL_PROJECT>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new TBL_PROJECT
                        {
                            JOBCODE_ID = req["JOBCODE_ID"].ToString(),
                            PROJECT_NAME = req["PROJECT_NAME"].ToString(),
                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static bool isEMPExist(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            return data.TBL_EMPs.Where(x => x.EMP_ID.Equals(empID) && x.STATUS.Equals("1")).Any();
        }

        public static List<Employee> getEmployee(string searchBy, string search)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            List<Employee> empData = new List<Employee>();

                empData = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          where emp.STATUS.Equals("1") &&
                          sect.SECT_NAME.Contains(searchBy.Equals("Department") ? search : "") &&
                          emp.FULL_NAME.Contains(searchBy.Equals("Name") ? search : "") &&
                          emp.EMP_ID.Contains(searchBy.Equals("Employee ID") ? search : "")
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                          }).ToList();

                return empData;
        }

        private static bool isImpersonate(string winCredential, out string impersonate, out bool isAdmin)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            impersonate = "";
            isAdmin = false;
            if (data.ADMINs.Where(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower())).Any())
            {
                ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
                impersonate = a.IMPERSONATE;
                isAdmin = true;
                if (impersonate != null)
                {
                    isAdmin = data.ADMINs.Any(x => x.EMP_ID.Contains(a.IMPERSONATE));
                    return true;
                }
            }
            return false;
        }

        private static string getUsername(string username)
        {
            int i = username.IndexOf("\\");
            username = username.Substring(i + 1);

            return username;
        }

        public static Employee getEmpDataByUsername(string winCredential)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            IEnumerable<Employee> empData = null;
            string impersonate = "";
            //int appRater = 0;
            bool isAdmin;
            if (isImpersonate(winCredential, out impersonate, out isAdmin))
            {
                empData = from emp in data.TBL_EMPs
                          join email in data.TBL_EMAILs on emp.EMP_ID equals email.Emp_Id
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where emp.EMP_ID.Equals(impersonate)
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              USERNAME = email.UserName,
                              EMAIL = email.Email,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                              DEPT_ID = dept.DEPT_ID,
                              DEPT_NAME = dept.DEPT_NAME,
                              ISADMIN = isAdmin,
                              //ISAPPROVER = Helper.RequisitionHelper.isApprover(emp.EMP_ID),
                              //APPRATER = appRater
                          };
            }
            else
            {
                string username = getUsername(winCredential);
                empData = from emp in data.TBL_EMPs
                          join email in data.TBL_EMAILs on emp.EMP_ID equals email.Emp_Id
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where email.UserName.ToLower() == username.ToLower()
                          orderby email.GeneratedOn descending
                          select new Employee
                          {
                              EMP_ID = emp.EMP_ID,
                              USERNAME = email.UserName,
                              EMAIL = email.Email,
                              FULL_NAME = emp.FULL_NAME,
                              SECT_ID = sect.SECT_ID,
                              SECT_NAME = sect.SECT_NAME,
                              DEPT_ID = dept.DEPT_ID,
                              DEPT_NAME = dept.DEPT_NAME,
                              ISADMIN = isAdmin,
                              //ISAPPROVER = Helper.RequisitionHelper.isApprover(emp.EMP_ID),
                              
                          };
            }
            return empData.FirstOrDefault();
        }

        public static string getEMPFullName(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            return data.TBL_EMPs.FirstOrDefault(x => x.EMP_ID.Equals(empID)).FULL_NAME;
        }

        public static string getEMPDept(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var empDept = from emp in data.TBL_EMPs
                          join dept in data.TBL_SECTIONs on emp.SECT_ID equals dept.SECT_ID
                          where emp.EMP_ID.Equals(empID)
                          select dept.SECT_NAME;

            foreach (var i in empDept)
                return  i.ToString();

            return null;
        }

        public static string getMgrGA()
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var mgr = (from emp in data.TBL_EMPs
                       join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                       where sect.SECT_NAME.Contains("GAD")
                       select sect.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static string getMgrDept(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            var mgr = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          where emp.EMP_ID.Equals(empID)
                          select sect.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static string getMgrDiv(string empID)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string mgr = (from emp in data.TBL_EMPs
                          join sect in data.TBL_SECTIONs on emp.SECT_ID equals sect.SECT_ID
                          join dept in data.TBL_DEPTs on sect.DEPT_ID equals dept.DEPT_ID
                          where emp.EMP_ID.Equals(empID)
                          select dept.MGR_EMP_ID).FirstOrDefault();
            return mgr;
        }

        public static bool isPMExist(string jobcode)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);
            return data.TBL_PROJECTs.Where(x => x.JOBCODE_ID.Equals(jobcode) && x.KEYP_EMP_ID != null).Any();
        }

        public static string getProjectMgr(string jobcode)
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string PM = null;
            PM = data.TBL_PROJECTs.Where(x => x.JOBCODE_ID.Equals(jobcode)).Select(x => x.KEYP_EMP_ID).FirstOrDefault();
            return PM;
        }

        public static string getJGCManagement()
        {
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            string mgr = data.TBL_EMPs.FirstOrDefault(x => x.STATUS.Equals("1") && x.POSITION_ID.Contains("PDR")).EMP_ID;
            return mgr;
        }
    }
}