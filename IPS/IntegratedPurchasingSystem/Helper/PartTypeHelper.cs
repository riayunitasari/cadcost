﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class PartTypeHelper
    {
        public static void generateDDLType(DropDownList ddl)
        {
            ddl.Items.Clear();
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);

            List<tblPartType> cat = getAllCatgeory();
            ddl.Items.Add(new ListItem("-- Select Type --", ""));
            foreach (var i in cat)
            {
                ddl.Items.Add(new ListItem(i.ptyName, i.PartTypeID.ToString()));
            }
        }

        public static List<tblPartType> getAllCatgeory()
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            return data.tblPartTypes.ToList();
        }

        //public static CATEGORY_ITEM getCategoryByID(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    return data.CATEGORY_ITEMs.FirstOrDefault(x => x.ID == id);
        //}

        //public static void insertCategory(string name, string desc)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    CATEGORY_ITEM c = new CATEGORY_ITEM();
        //    c.CATEGORY_ITEM1 = name;
        //    c.DESCRIPTION = desc;
        //    c.ISDELETED = 0;

        //    data.CATEGORY_ITEMs.InsertOnSubmit(c);
        //    data.SubmitChanges();
        //}

        //public static void updateCategory(int id, string name, string desc)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    CATEGORY_ITEM c = data.CATEGORY_ITEMs.FirstOrDefault(x => x.ID == id);
        //    if (c != null)
        //    {
        //        c.CATEGORY_ITEM1 = name;
        //        c.DESCRIPTION = desc;
        //        data.SubmitChanges();
        //    }
        //}

        //public static void deleteCategory(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    CATEGORY_ITEM c = data.CATEGORY_ITEMs.FirstOrDefault(x => x.ID == id);
        //    if (c != null)
        //        c.ISDELETED = 1;
        //    data.SubmitChanges();
        //}

        //public static List<CATEGORY_ITEM> getFilteredCategory(string search)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    return data.CATEGORY_ITEMs.Where(x => x.CATEGORY_ITEM1.Contains(search) ||
        //        x.ID.ToString().Contains(search) || x.DESCRIPTION.Contains(search)).ToList();
        //}
    }
}