﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class ItemHelper
    {

        //public static List<RequisitionItems> getItems(string searchBy, string text)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    IQueryable<RequisitionItems> items = from item in data.ITEMs
        //             join category in data.CATEGORY_ITEMs on item.ID_CATEGORY equals category.ID
        //             where item.ISDELETED != 1
        //             select new RequisitionItems
        //             {
        //                 ITEM_ID = item.ID_ITEM,
        //                 ITEM_NAME = item.ITEM_NAME,
        //                 CATEGORY_ID = category.ID,
        //                 CATEGORY = category.CATEGORY_ITEM1,
        //                 TYPE = item.TYPE,
        //                 DESCRIPTION = item.DESCRIPTION
        //             };

        //    if (searchBy.Equals("Name"))
        //        return items.Where(x => x.ITEM_NAME.Contains(text)).AsEnumerable<RequisitionItems>().ToList();
        //    else if (searchBy.Equals("Category"))
        //        return items.Where(x => x.CATEGORY.Contains(text)).AsEnumerable<RequisitionItems>().ToList();
        //    else if (searchBy.Equals("Type"))
        //        return items.Where(x => x.TYPE.Contains(text)).AsEnumerable<RequisitionItems>().ToList();
        //    else
        //        return items.AsEnumerable<RequisitionItems>().ToList();


        //}

        //public static void insertNewItem(string itemName, string type, int category, string desc)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    ITEM item = new ITEM();
        //    item.ITEM_NAME = itemName;
        //    item.TYPE = type;
        //    item.ID_CATEGORY = category;
        //    item.DESCRIPTION = desc;
        //    item.ISDELETED = 0;

        //    data.ITEMs.InsertOnSubmit(item);
        //    data.SubmitChanges();
        //}

        //public static ITEM getItemByID(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    return data.ITEMs.FirstOrDefault(x => x.ID_ITEM == id);
        //}

        //public static void updateItem(int id, string name, string type, int category, string desc)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    ITEM i = data.ITEMs.FirstOrDefault(x => x.ID_ITEM == id);
        //    if(i != null)
        //    {
        //        i.ITEM_NAME = name;
        //        i.TYPE = type;
        //        i.ID_CATEGORY = category;
        //        i.DESCRIPTION = desc;
        //        data.SubmitChanges();
        //    }
        //}

        //public static void deleteItem(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    ITEM i = data.ITEMs.FirstOrDefault(x => x.ID_ITEM == id);
        //    if (i != null)
        //        i.ISDELETED = 1;
        //    data.SubmitChanges();
        //}

        public static List<RequisitionItems> getItems(string searchBy, string text)
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            string name = "", code = "", typeName = "";

            if (searchBy.Equals("Name"))
                name = text;
            else if (searchBy.Equals("Code"))
                code = text;
            else if (searchBy.Equals("Type"))
                typeName = text;

            List<RequisitionItems> items = (from item in data.tblParts
                                                 join type in data.tblPartTypes on item.PartTypeID equals type.PartTypeID
                                                 where Convert.ToInt32(item.Deleted) != 1 &&
                                                 item.prtName.Contains(name) && item.PartCD.Contains(code) && type.ptyName.Contains(typeName)
                                                 select new RequisitionItems
                                                 {
                                                     PART_ID = item.PartID,
                                                     PART_CODE = item.PartCD,
                                                     PART_NAME = item.prtName,
                                                     TYPE_ID = type.PartTypeID,
                                                     TYPE_NAME = type.ptyName,
                                                 }).ToList();;


            return items;
        }

        public static int getPartType(int itemID, out string partName)
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            int partType = data.tblParts.FirstOrDefault(x => x.PartID == itemID).PartTypeID;
            partName = data.tblPartTypes.FirstOrDefault(x => x.PartTypeID == partType).ptyName;

            return partType;

        }

    }
}