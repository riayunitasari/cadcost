﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;


namespace IntegratedPurchasingSystem.Helper
{
    public class MenuHelper
    {
        public static string getMenu()
        {
            Employee logInUser = Helper.PFNATTHelper.getEmpDataByUsername(HttpContext.Current.Request.LogonUserIdentity.Name); 
            string menu = 
             "<ul class='menu'>"
                       +" <li><a href='#'>Requisition</a>"
                            +"<ul style='list-style-type: none;'>"
                                + "<li><a href='RequisitionTracking.aspx'>Requisition Tracking</a></li>"
                                +"<li><a href='Requisition.aspx'>Create Requisition</a></li>"
                                +"<li><a href='ReqSummary.aspx'>Create Requisition Summary</a></li>"
                            +"</ul>"
                        +"</li>"
                        +"<li><a href='#'>Quotation</a>"
                            +"<ul style='list-style-type: none;'>"
                                +"<li><a href='QuoTracking.aspx'>Quotation Tracking</a></li>"
                                +"<li><a href='Quotation.aspx'>Create Quotation</a></li>"
                            +"</ul>"
                        +"</li>"
                        //+"<li><a href='Item.aspx'>Item</a></li>"
                        //+"<li><a href='Vendor.aspx'>Vendor</a></li>"
                        + "<li><a href='POTracking.aspx'>Purchase Order</a></li>"
                        + "<li><a href='PurchaseTracking.aspx'>Purchase Tracking</a></li>"
                        +"<li><a href='Setting.aspx'>Setting</a></li>"
                        +"<li><a href='http://portal/'>JIND Portal</a></li>"
                    +"</ul>";

            return menu;
        }
    }
}