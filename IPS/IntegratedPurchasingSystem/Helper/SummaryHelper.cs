﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class SummaryHelper
    {
        public static List<RequisitionObject> getRequisitionForSummary(string reqNo, int month, string jobcode, string type)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getRequisitionForSummary", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@reqMonth", month);
            command.Parameters.AddWithValue("@reqNo", reqNo);
            command.Parameters.AddWithValue("@reqType", type);
            command.Parameters.AddWithValue("@jobcode", jobcode);

            connection.Open();
            List<RequisitionObject> reqs = new List<RequisitionObject>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new RequisitionObject
                        {
                            ID_REQ = (int)req["ID_REQ"],

                            DEPT_REQ_NO = req["REQ_NO"].ToString(),
                            JOBCODE = req["JOBCODE"].ToString(),
                            REQ_BY = req["REQ_BY"].ToString(),
                            REQ_BY_NAME = req["REQ_BY_NAME"].ToString(),
                            DATE_CREATED = (DateTime)req["DATE_CREATED"],
                            REQ_TYPE_TEXT = req["REQ_TYPE_TEXT"].ToString(),
                            REQ_BY_DEPT = req["REQ_BY_DEPT"].ToString(),

                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static RelatedDetail getItemFromReqDetail(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            RelatedDetail item = (from d in data.REQ_DETAILs
                                 join r in data.REQUISITIONs on d.ID_REQ equals r.ID_REQ
                                 where d.ID == idDetail
                                 select new RelatedDetail
                                 {
                                     ID_REQ_DETAIL = d.ID,
                                     ID_REQ = (int)d.ID_REQ,
                                     PART_ID = (int)d.ID_ITEM,
                                     PART_NAME = d.ITEM_NAME,
                                     PART_CODE = d.PART_CODE,
                                     TYPE_ID = Convert.ToInt32(d.PART_TYPE_ID),
                                     TYPE_NAME = d.PART_TYPE_NAME,
                                     ADDITIONAL_QUANTITY = 0,
                                     QUANTITY = (double)d.QUANTITY,
                                     PRICE = (double)d.PRICE,
                                     METRIC = d.METRIC_CODE,
                                     REMARK = d.REMARKS,
                                     DEPT_REQ_NO = r.REQ_NO
                                 }).FirstOrDefault();
            return item;
        }

        public static void insertSummaryToRequisition(List<SummaryItems> summaryItems, int idReq)
        {
            List<RequisitionItems> items = new List<RequisitionItems>();
            foreach (var i in summaryItems)
            {
                int idDetail = 0;
                RequisitionItems item = new RequisitionItems(i.PART_ID, i.PART_CODE, i.PART_NAME, i.TYPE_ID, i.TYPE_NAME, i.QUANTITY, i.METRIC, i.PRICE, i.REMARK);
                Helper.RequisitionHelper.insertSingleDetialItem(item, idReq, out idDetail);
                insertRelatedDetail(idDetail, i.RELATED_DETAIL);
            }
        }

        public static List<SummaryItems> insertAdditionalItem(List<SummaryItems> mergeItems, List<RelatedDetail> items, int idReq)
        {
            List<RequisitionItems> tempItems = items.ConvertAll(z => new RequisitionItems
            {
                PART_ID = z.PART_ID,
                PART_NAME = z.PART_NAME,
                PART_CODE = z.PART_CODE,
                TYPE_ID = z.TYPE_ID,
                TYPE_NAME = z.TYPE_NAME,
                QUANTITY = z.QUANTITY,
                METRIC = z.METRIC,
                PRICE = z.PRICE,
                REMARK = z.REMARK
            });

            foreach (var i in tempItems)
            {
                int detailId = 0;
                Helper.RequisitionHelper.insertSingleDetialItem(i, idReq, out detailId);
                RelatedDetail ii = mergeItems.FirstOrDefault(x => x.PART_ID == i.PART_ID).RELATED_DETAIL.FirstOrDefault(y => y.ISADDITIONALITEM == 1);
                ii.ID_REQ_DETAIL = detailId;
                ii.ID_REQ = idReq;
            }
            return mergeItems;
        }

        private static void insertSingleRelatedDetail(int idSumDetail, RelatedDetail detail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            RELATED_DETAIL link = new RELATED_DETAIL();
            link.ID_REQ = detail.ID_REQ;
            link.ID_SUM_DETAIL = idSumDetail;
            link.ID_REQ_DETAIL = detail.ID_REQ_DETAIL;
            link.ADDITIONAL_QUANTITY = detail.ADDITIONAL_QUANTITY;
            link.IS_ADDITIONAL_DETAIL = detail.ISADDITIONALITEM;
            link.REMARK = detail.REMARK;
            data.RELATED_DETAILs.InsertOnSubmit(link);
            data.SubmitChanges();
        }

        public static void insertRelatedDetail(int sumDetail, List<RelatedDetail> reqDetails)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            foreach (var i in reqDetails)
            {
                insertSingleRelatedDetail(sumDetail, i);
            }
        }

       public static List<SummaryItems> getSummaryItems(int idReqSum)
       {
           PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

           List<SummaryItems> items = (from r in data.REQUISITIONs
                                       join d in data.REQ_DETAILs on r.ID_REQ equals d.ID_REQ
                                       join rl in data.RELATED_DETAILs on d.ID equals rl.ID_SUM_DETAIL
                                       where r.ID_REQ == idReqSum
                                       select new SummaryItems
                                       {
                                           ID_REQ = r.ID_REQ,
                                           ID_REQ_DETAIL = d.ID,
                                           REQ_NO = r.REQ_NO,
                                           PART_ID = (int)d.ID_ITEM,
                                           PART_NAME = d.ITEM_NAME,
                                           PART_CODE = d.PART_CODE,
                                           TYPE_ID = (int)d.PART_TYPE_ID,
                                           TYPE_NAME = d.PART_TYPE_NAME,
                                           QUANTITY = (double)d.QUANTITY,
                                           METRIC = d.METRIC_CODE,
                                           PRICE = (double)d.PRICE,
                                           REMARK = d.REMARKS,
                                           ID_QUO = (int)d.ID_QUO,
                                           ID_ADVANCE = (int)d.ID_ADVANCE
                                       }).ToList().GroupBy(x => x.PART_ID).Select(g => g.First()).ToList();
          
           foreach (var ii in items) // selected item from another requisition
           {
               List<RelatedDetail> detail = (from r in data.REQUISITIONs
                                             join d in data.REQ_DETAILs on r.ID_REQ equals d.ID_REQ
                                             join rl in data.RELATED_DETAILs on d.ID equals rl.ID_REQ_DETAIL
                                             where ii.ID_REQ_DETAIL == rl.ID_SUM_DETAIL 
                                             select new RelatedDetail
                                             {  
                                                 ID = rl.ID_RELATED,
                                                 ID_SUM_DETAIL = (int)rl.ID_SUM_DETAIL,
                                                 ID_REQ = rl.ID_REQ,
                                                 DEPT_REQ_NO = r.REQ_NO,
                                                 ID_REQ_DETAIL = rl.ID_REQ_DETAIL,
                                                 PART_ID = (int)d.ID_ITEM,
                                                 PART_NAME = d.ITEM_NAME,
                                                 PART_CODE = d.PART_CODE,
                                                 TYPE_ID = (int)d.PART_TYPE_ID,
                                                 TYPE_NAME = d.PART_TYPE_NAME,
                                                 QUANTITY = (double)d.QUANTITY,
                                                 ADDITIONAL_QUANTITY = (double)rl.ADDITIONAL_QUANTITY,
                                                 METRIC = d.METRIC_CODE,
                                                 PRICE = (double)d.PRICE,
                                                 REMARK = rl.REMARK,
                                                 ISADDITIONALITEM = (int) rl.IS_ADDITIONAL_DETAIL,
                                             }).ToList();
               ii.RELATED_DETAIL = detail;
           }

           return items;
       }


       public static void deleteSingleReqDetail(int idDetail)
       {
           PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
           REQ_DETAIL detail = data.REQ_DETAILs.FirstOrDefault(x => x.ID == idDetail);
           data.REQ_DETAILs.DeleteOnSubmit(detail);
           data.SubmitChanges();
       } 

        public static void deleteRelatedSumDetail(int idRelatedSum)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            RELATED_DETAIL detail = data.RELATED_DETAILs.FirstOrDefault(x => x.ID_RELATED == idRelatedSum);
            data.RELATED_DETAILs.DeleteOnSubmit(detail);
            data.SubmitChanges();
        }

        public static void updateRelatedDetail(int idRelated, double addQuantity, string remark)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            RELATED_DETAIL detail = data.RELATED_DETAILs.FirstOrDefault(x => x.ID_RELATED == idRelated);
            if (detail != null)
            {
                detail.ADDITIONAL_QUANTITY = addQuantity;
                detail.REMARK = remark;
            }
            data.SubmitChanges();
        }

        public static void updateSummaryItem(int idReq, List<SummaryItems> items)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            //mengambil req_detail yg merupakan merged Item / bukan additional item
            List<REQ_DETAIL> detail = data.REQ_DETAILs.Where(x => x.ID_REQ == idReq
                && (data.RELATED_DETAILs.Select(y => y.ID_SUM_DETAIL)).Contains(x.ID)).ToList();
            List<RelatedDetail> relatedDetail = new List<RelatedDetail>();
            List<RELATED_DETAIL> rlDetail = new List<RELATED_DETAIL>();
            List<RELATED_DETAIL> selectedDetail = new List<RELATED_DETAIL>();
            List<RELATED_DETAIL> additionalDetail = new List<RELATED_DETAIL>();

            for (int i = 0; i < detail.Count; i++) //delete
            {
                bool isExist = items.Any(x => x.ID_REQ_DETAIL == detail[i].ID);
                if (isExist)
                    relatedDetail = items.FirstOrDefault(x => x.PART_ID == detail[i].ID_ITEM).RELATED_DETAIL.ToList();
                else relatedDetail = new List<RelatedDetail>();
                //database -  all related detail
                rlDetail = data.RELATED_DETAILs.Where(x => x.ID_SUM_DETAIL == detail[i].ID).ToList();
                //database - selected item
                selectedDetail = data.RELATED_DETAILs.Where(x => x.IS_ADDITIONAL_DETAIL == 0 && detail[i].ID == x.ID_SUM_DETAIL).ToList();
                //database - additional item
                additionalDetail = rlDetail.Where(x => x.IS_ADDITIONAL_DETAIL == 1 && detail[i].ID == x.ID_SUM_DETAIL).ToList();

                foreach (var x in selectedDetail)
                {
                    if (!relatedDetail.Any(y => y.ID_REQ_DETAIL == x.ID_REQ_DETAIL))
                        deleteRelatedSumDetail(x.ID_RELATED);
                }

                foreach (var x in additionalDetail)
                {
                    if (!relatedDetail.Any(y => y.ID_REQ_DETAIL == x.ID_REQ_DETAIL))
                    {
                        deleteRelatedSumDetail(x.ID_RELATED);
                        Helper.RequisitionHelper.deleteSingleDetail(x.ID_RELATED);
                    }
                }

                if (!isExist)
                    Helper.RequisitionHelper.deleteSingleDetail(detail[i].ID);
            }

            for (int i = 0; i < items.Count; i++)// add new
            {
                //int idDetail = 0;
                bool isExist = detail.Any(x => x.ID_ITEM == items[i].PART_ID);
                int idSumDetail = items[i].ID_REQ_DETAIL;
                relatedDetail = items.FirstOrDefault(x => x.PART_ID == items[i].PART_ID).RELATED_DETAIL.ToList();
                List<RelatedDetail> selectedDetailCurReqItem = relatedDetail.Where(x => x.ISADDITIONALITEM == 0).ToList();
                List<RelatedDetail> additionalDetailCurReqItem = relatedDetail.Where(x => x.ISADDITIONALITEM == 1).ToList();

                rlDetail = data.RELATED_DETAILs.Where(x => x.ID_SUM_DETAIL == items[i].ID_REQ_DETAIL).ToList();
                selectedDetail = data.RELATED_DETAILs.Where(x => x.IS_ADDITIONAL_DETAIL == 0 && items[i].ID_REQ_DETAIL == x.ID_SUM_DETAIL).ToList();
                additionalDetail = rlDetail.Where(x => x.IS_ADDITIONAL_DETAIL == 1 && items[i].ID_REQ_DETAIL == x.ID_SUM_DETAIL).ToList();

                foreach (var x in selectedDetailCurReqItem)
                {
                    if (!selectedDetail.Any(y => y.ID_REQ_DETAIL == x.ID_REQ_DETAIL)) // new
                    {
                       // int idDetailNew = x.ID_REQ_DETAIL;
                        RequisitionItems item = new RequisitionItems(items[i].PART_ID, items[i].PART_CODE, items[i].PART_NAME, items[i].TYPE_ID, items[i].TYPE_NAME,
                            items[i].QUANTITY, items[i].METRIC, items[i].PRICE, items[i].REMARK);
                        if (!isExist)
                        {
                            Helper.RequisitionHelper.insertSingleDetialItem(item, idReq, out idSumDetail);
                            isExist = true;
                        }
                        RelatedDetail newRelatedDetail = new RelatedDetail(x.ID_REQ, x.ID_REQ_DETAIL, idSumDetail, x.ADDITIONAL_QUANTITY, 0, x.REMARK);

                        insertSingleRelatedDetail(idSumDetail, newRelatedDetail);
                    }
                    else
                    { //update
                        updateRelatedDetail(x.ID, x.ADDITIONAL_QUANTITY, x.REMARK);
                    }
                    //update nilai pada summary item detail di database

                }

                rlDetail = data.RELATED_DETAILs.Where(x => x.ID_SUM_DETAIL == items[i].ID_REQ_DETAIL).ToList();
                selectedDetail = data.RELATED_DETAILs.Where(x => x.IS_ADDITIONAL_DETAIL == 0 && items[i].ID_REQ_DETAIL == x.ID_SUM_DETAIL).ToList();
                additionalDetail = rlDetail.Where(x => x.IS_ADDITIONAL_DETAIL == 1 && items[i].ID_REQ_DETAIL == x.ID_SUM_DETAIL).ToList();

                foreach (var x in additionalDetailCurReqItem)
                {
                    if (!additionalDetail.Any(y => y.ID_REQ_DETAIL == x.ID_REQ_DETAIL)) //new
                    {
                        int idDetailNew = 0;
                        //main item
                        RequisitionItems item = new RequisitionItems(items[i].PART_ID, items[i].PART_CODE, items[i].PART_NAME, items[i].TYPE_ID, items[i].TYPE_NAME,
                            items[i].QUANTITY, items[i].METRIC, items[i].PRICE, items[i].REMARK);
                        //additional item
                        RequisitionItems additionalItem = new RequisitionItems(items[i].PART_ID, items[i].PART_CODE, items[i].PART_NAME, items[i].TYPE_ID, items[i].TYPE_NAME,
                            x.QUANTITY, x.METRIC, x.PRICE, x.REMARK);
                        Helper.RequisitionHelper.insertSingleDetialItem(additionalItem, idReq, out idDetailNew);
                        if (!isExist)
                        {
                            Helper.RequisitionHelper.insertSingleDetialItem(item, idReq, out idSumDetail);
                            isExist = true;
                        }

                        RelatedDetail newRelatedDetail = new RelatedDetail(idReq, idDetailNew, idSumDetail, x.ADDITIONAL_QUANTITY, 1, x.REMARK);
                        insertSingleRelatedDetail(idSumDetail, newRelatedDetail);
                    }
                    else // update
                    {
                        Helper.RequisitionHelper.updateSingleDetail(x.ID_REQ_DETAIL, x.PRICE, x.QUANTITY, x.METRIC, x.REMARK);
                    }
                }

                if (isExist)
                    RequisitionHelper.updateSingleDetail(items[i].ID_REQ_DETAIL, items[i].PRICE, items[i].QUANTITY, items[i].METRIC, items[i].REMARK);
            }
        }


    }
}