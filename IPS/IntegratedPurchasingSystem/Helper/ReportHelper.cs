﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using OfficeOpenXml;
using System.Runtime.InteropServices;

namespace IntegratedPurchasingSystem.Helper
{
    public class ReportHelper
    {
        public static void quotationToExcel(string fileName, QuoObject quo, List<QuoVendor> vendors)
        {
            FileInfo file = new FileInfo(fileName);
            ExcelPackage ExcelPackage = new ExcelPackage(file);
            ExcelWorksheet ExcelWorksheet = ExcelPackage.Workbook.Worksheets["Sheet1"];

            ExcelWorksheet.Cells[3, 3].Value = quo.REQ_NO;
            ExcelWorksheet.Cells[4, 3].Value = quo.DATE_CREATED.ToString("dd-MMM-yyyy");
            ExcelWorksheet.Cells[3, 7].Value = "JOBCODE : " + quo.JOBCODE;
            ExcelWorksheet.Cells[4, 7].Value = "USER / REQUESTOR : " + quo.REQUESTOR;
            ExcelWorksheet.Cells[4, 5].Value = quo.REQUEST_ITEM;
            ExcelWorksheet.Cells[21, 4].Value = quo.RECOMM_VENDOR_NAME;
            ExcelWorksheet.Cells[23, 1].Value = "Prepared By :";
            ExcelWorksheet.Cells[24, 1].Value = "- "+ quo.CREATED_BY_NAME;

            vendors = vendors.OrderBy(x => x.TECHNICAL_ASPECT).ToList();

            for (int i = 0; i < vendors.Count; i++)
            {
                ExcelWorksheet.Cells[6, 4 + i].Value = vendors[i].VENDOR_NAME;
                ExcelWorksheet.Cells[8, 4 + i].Value = vendors[i].QUOT_NO;
                if (!vendors[i].QUOT_NO.ToLower().Contains("no quote"))
                {
                    ExcelWorksheet.Cells[9, 4 + i].Value = vendors[i].QUOT_DATE;
                    ExcelWorksheet.Cells[11, 4 + i].Value = vendors[i].TECHNICAL_ASPECT;
                    ExcelWorksheet.Cells[14, 4 + i].Value = vendors[i].QUO_PRICE;
                    ExcelWorksheet.Cells[15, 4 + i].Value = vendors[i].DISCOUNT;
                    ExcelWorksheet.Cells[16, 4 + i].Value = vendors[i].VAT_AMOUNT;
                    ExcelWorksheet.Cells[17, 4 + i].Value = vendors[i].NET_PRICE;
                    if (vendors[i].DELIVERY_TIME.Equals("1"))
                        ExcelWorksheet.Cells[18, 4 + i].Value = vendors[i].DELIVERY_TIME + " day";
                    else
                        ExcelWorksheet.Cells[18, 4 + i].Value = vendors[i].DELIVERY_TIME + " days";
                    ExcelWorksheet.Cells[19, 4 + i].Value = vendors[i].WARRANTY;
                    ExcelWorksheet.Cells[20, 4 + i].Value = vendors[i].PAYMENT_TERMS;
                }
            }

            List<TBL_ACTION_HISTORY> app = ApprovalHelper.getApprovalList(quo.ID_QUO, Variable.ReqType.QuotationRequest);
            if (app.Count > 0)
            {   
                ExcelWorksheet.Cells[23, 5].Value = "Approved By :";
                for(int i = 0; i < app.Count; i++)
                {
                    string text = "- " + app[i].COMPLETED_BY_NAME + " (";
                    if (!app[i].PARTICIPANT.Equals(app[i].COMPLETED_BY))
                        text+= "On Behalf Of - ";
                    if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByDeptMgr)
                        text+= "Department Manager)";
                    else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByDivMgr)
                        text+= "Division Manager)";
                    else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByGADept)
                        text+= "General Affair Department Manager)";
                    else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByManagement)
                        text+= "Management)";
                    else if(app[i].ACTIVITY_ID == Variable.Status.CheckedByCOD)
                        text+= "Checked By COD)";
                    text += " - " + ((DateTime)app[i].END_DATE).ToString("dd MMM yyyy");

                    ExcelWorksheet.Cells[24 + i, 5].Value = text;
                }
            }
            
            ExcelPackage.Save();
        }

        public static string excelToPDF(string filePath)
        {
            Excel.Application app = new Excel.Application();
            Excel.Workbook wkb = app.Workbooks.Open(filePath);
            filePath = filePath.Replace(".xlsx", ".pdf");
            wkb.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, filePath);

            if (wkb != null)
            {
                wkb.Close(false, Type.Missing, Type.Missing);
                //wkb = null;
                Marshal.ReleaseComObject(wkb);
            }
            if (app != null)
            {
                app.Quit();
                //app = null;
                Marshal.ReleaseComObject(app);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return filePath;
        }

        public static void purchaseSummary(string fileName)
        {
            //FileInfo file = new FileInfo(fileName);
            //ExcelPackage ExcelPackage = new ExcelPackage(file);
            //ExcelWorksheet ExcelWorksheet = ExcelPackage.Workbook.Worksheets["Summary"];

            //ExcelWorksheet.Cells[3, 3].Value = quo.REQ_NO;
            //ExcelWorksheet.Cells[4, 3].Value = quo.DATE_CREATED.ToString("dd-MMM-yyyy");
            //ExcelWorksheet.Cells[3, 7].Value = "JOBCODE : " + quo.JOBCODE;
            //ExcelWorksheet.Cells[4, 7].Value = "USER / REQUESTOR : " + quo.REQUESTOR;
            //ExcelWorksheet.Cells[4, 5].Value = quo.REQUEST_ITEM;
            //ExcelWorksheet.Cells[23, 4].Value = quo.RECOMM_VENDOR_NAME;

            //for (int i = 0; i < vendors.Count; i++)
            //{
            //    ExcelWorksheet.Cells[6, 4 + i].Value = vendors[i].VENDOR_NAME;
            //    ExcelWorksheet.Cells[8, 4 + i].Value = vendors[i].QUOT_NO;
            //    ExcelWorksheet.Cells[9, 4 + i].Value = vendors[i].QUOT_DATE;
            //    ExcelWorksheet.Cells[12, 4 + i].Value = vendors[i].TECHNICAL_ASPECT;
            //    ExcelWorksheet.Cells[16, 4 + i].Value = vendors[i].QUO_PRICE;
            //    ExcelWorksheet.Cells[17, 4 + i].Value = vendors[i].DISCOUNT;
            //    ExcelWorksheet.Cells[18, 4 + i].Value = vendors[i].VAT_AMOUNT;
            //    ExcelWorksheet.Cells[19, 4 + i].Value = vendors[i].NET_PRICE;
            //    if (vendors[i].DELIVERY_TIME.Equals("1"))
            //        ExcelWorksheet.Cells[20, 4 + i].Value = vendors[i].DELIVERY_TIME + " day";
            //    else
            //        ExcelWorksheet.Cells[20, 4 + i].Value = vendors[i].DELIVERY_TIME + " days";
            //    ExcelWorksheet.Cells[21, 4 + i].Value = vendors[i].WARRANTY;
            //    ExcelWorksheet.Cells[22, 4 + i].Value = vendors[i].PAYMENT_TERMS;
            //}

            //List<TBL_ACTION_HISTORY> app = ApprovalHelper.getApprovalList(quo.ID_QUO, Variable.ReqType.QuotationRequest);
            //if (app.Count > 0)
            //{
            //    ExcelWorksheet.Cells[25, 1].Value = "Approved By :";
            //    for (int i = 0; i < app.Count; i++)
            //    {
            //        string text = "- " + app[i].COMPLETED_BY_NAME + " (";
            //        if (!app[i].PARTICIPANT.Equals(app[i].COMPLETED_BY))
            //            text += "On Behalf Of - ";
            //        if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByDeptMgr)
            //            text += "Department Manager)";
            //        else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByDivMgr)
            //            text += "Division Manager)";
            //        else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByGADept)
            //            text += "General Affair Department Manager)";
            //        else if (app[i].ACTIVITY_ID == Variable.Status.ApprovalByManagement)
            //            text += "Management)";
            //        text += " - " + ((DateTime)app[i].END_DATE).ToString("dd MMM yyyy");

            //        ExcelWorksheet.Cells[26 + i, 2].Value = text;
            //    }
            //}

            //ExcelPackage.Save();
        }


    }
}