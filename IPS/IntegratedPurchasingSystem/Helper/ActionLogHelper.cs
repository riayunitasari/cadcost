﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class ActionLogHelper
    {
        public static void insertReqHistory(string empID, int idReq, string reqType, int actionID, int activityID, string comment,
            double exchangeRate, double totalAmount, string jobcode)
        {
            if (exchangeRate == 0)
                exchangeRate = 1;

            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            connection.Open();
            SqlCommand command = new SqlCommand("getRequisitionTaskWorkflow", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@totalAmount", totalAmount);
            command.Parameters.AddWithValue("@exchangeRate", exchangeRate);
            command.Parameters.AddWithValue("@reqType", reqType);
            command.Parameters.AddWithValue("@empID", empID);
            command.Parameters.AddWithValue("@activityID", activityID);
            command.Parameters.AddWithValue("@actionID", actionID);
            command.Parameters.AddWithValue("@comment", comment);

            command.ExecuteNonQuery();
            connection.Close();
        }

        public static List<TBL_ACTION_HISTORY> getActionLog(int idReq, string reqType)
        {
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);

            List<TBL_ACTION_HISTORY> log = data.TBL_ACTION_HISTORies.Where(x =>
                x.PROCESS_ID == idReq && x.REQ_TYPE.Equals(reqType)).ToList();

            return log;
        }

        public static void insertQuotHistory(string empID, int idQuo, int actionID, int activityID, string comment)
        {

            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            connection.Open();
            SqlCommand command = new SqlCommand("getQuotationTaskWorkFlow", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idQuo", idQuo);
            command.Parameters.AddWithValue("@empID", empID);
            command.Parameters.AddWithValue("@activityID", activityID);
            command.Parameters.AddWithValue("@actionID", actionID);
            command.Parameters.AddWithValue("@comment", comment);

            command.ExecuteNonQuery();
            connection.Close();
        }

        public static void insertPOHistory(string empID, int idQuo, int actionID, int activityID, string comment)
        {

            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            connection.Open();
            SqlCommand command = new SqlCommand("getPOTaskWorkflow", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idPO", idQuo);
            command.Parameters.AddWithValue("@empID", empID);
            command.Parameters.AddWithValue("@activityID", activityID);
            command.Parameters.AddWithValue("@actionID", actionID);
            command.Parameters.AddWithValue("@comment", comment);

            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}