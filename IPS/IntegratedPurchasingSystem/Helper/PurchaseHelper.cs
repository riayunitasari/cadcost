﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedPurchasingSystem.Helper
{
    public class PurchaseHelper
    {
        public static void setDDLYearForPurchase(DropDownList ddl)
        {
            ddl.ClearSelection();
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getYearForPurchase", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            List<int> year = new List<int>();
            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                    year.Add((int)reader["CREATED_YEAR"]);
            }
            connection.Close();

            foreach (var i in year)
                ddl.Items.Add(i.ToString());

            if (!year.Any())
                ddl.Items.Add(DateTime.Now.Year.ToString());
        }

        public static List<Purchase> getFilteredPurchase(int month, int year, string lastUpdate, int activity, string reqType, int progress)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getPurchaseTracking", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@month", month);
            command.Parameters.AddWithValue("@year", year);
            command.Parameters.AddWithValue("@lastUpdateOn", lastUpdate);
            command.Parameters.AddWithValue("@activityID", activity);

            List<Purchase> purchase = new List<Purchase>();
            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                purchase = (from req in reader.Cast<DbDataRecord>()
                            select new Purchase
                        {
                            ID_REQ = (int)req["ID_REQ"],
                            REQ_NO = req["REQ_NO"].ToString(),
                            GA_REQ_NO = req["GA_REQ_NO"].ToString(),
                            
                        }).ToList();
            }
            connection.Close();

            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            foreach (var i in purchase)
            {
                i.ID_QUO = getQuotationForPurchaseTracking(i.ID_REQ, reqType, progress);
                i.ID_ADVANCE = getAdvanceForPurchaseTracking(i.ID_REQ, reqType, progress);
                i.ID_PO = getPOForPurchaseTracking(i.ID_REQ, reqType, progress);

                if (data.RELATED_DETAILs.Any(x => x.ID_REQ == i.ID_REQ))
                {
                    int idSumDetail = 0;
                    idSumDetail = (int)data.RELATED_DETAILs.FirstOrDefault(x => x.ID_REQ == i.ID_REQ).ID_SUM_DETAIL;
                    if (idSumDetail > 0)
                        if (data.REQ_DETAILs.Any(x => x.ID == idSumDetail))
                        {
                            i.ID_REQ_SUM = (int)data.REQ_DETAILs.FirstOrDefault(x => x.ID == idSumDetail).ID_REQ;
                            i.SUM_REQ_NO = data.REQUISITIONs.FirstOrDefault(x => x.ID_REQ == i.ID_REQ_SUM).GA_REQ_NO;
                        }
                }
            }
            return purchase;
        }

        public static List<int> getQuotationForPurchaseTracking(int idReq, string reqType, int progress)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            SqlCommand command = new SqlCommand("getQuotationForPurchaseTracking ", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            command.Parameters.AddWithValue("@reqType", reqType);
            command.Parameters.AddWithValue("@progress", progress);
            connection.Open();
            List<int> quo = new List<int>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                    quo.Add((int)reader["ID_QUO"]);
            }
            connection.Close();

            return quo;
        }

        public static List<int> getAdvanceForPurchaseTracking(int idReq, string reqType, int progress)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            SqlCommand command = new SqlCommand("getAdvanceForPurchaseTracking", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            command.Parameters.AddWithValue("@reqType", reqType);
            command.Parameters.AddWithValue("@progress", progress);
            connection.Open();
            List<int> ad = new List<int>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                    ad.Add((int)reader["ID_ADVANCE"]);
            }
            connection.Close();

            return ad;
        }

        public static List<int> getPOForPurchaseTracking(int idReq, string reqType, int progress)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);
            SqlCommand command = new SqlCommand("getPOForPurchaseTracking", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            command.Parameters.AddWithValue("@reqType", reqType);
            command.Parameters.AddWithValue("@progress", progress);
            connection.Open();
            List<int> po = new List<int>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                    po.Add((int)reader["ID_PO"]);
            }
            connection.Close();

            return po;
        }

        public static List<PurchaseDetail> getDetailPurchaseDetail(int idReq)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getPurchaseTrackingDetail", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            connection.Open();
            List<PurchaseDetail> reqs = new List<PurchaseDetail>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                        select new PurchaseDetail
                        {
                            ID_REQ = (int)req["ID_REQ"],
                            REQ_NO = req["REQ_NO"].ToString(),
                            GA_REQ_NO = req["GA_REQ_NO"].ToString(),
                            ID_QUO = (int)req["ID_QUO"],
                            ID_PO = (int)req["ID_PO"],
                            ID_ADVANCE = (int)req["ID_ADVANCE"],
                            ID_REQ_SUM = (int)req["ID_REQ_SUM"],
                            SUM_REQ_NO = req["SUM_REQ_NO"].ToString(),
                            REQ_ACTIVITY_ID = (int)req["REQ_ACTIVITY_ID"],
                            REQ_ACTIVITY_TEXT = req["REQ_ACTIVITY_TEXT"].ToString(),
                            QUO_ACTIVITY_ID = (int)req["QUO_ACTIVITY_ID"],
                            QUO_ACTIVITY_TEXT = req["QUO_ACTIVITY_TEXT"].ToString(),
                            ADVANCE_ACTIVITY_ID = (int)req["ADVANCE_ACTIVITY_ID"],
                            ADVANCE_ACTIVITY_TEXT = req["ADVANCE_ACTIVITY_TEXT"].ToString(),
                            PO_ACTIVITY_ID = (int)req["PO_ACTIVITY_ID"],
                            PO_ACTIVITY_TEXT = req["PO_ACTIVITY_TEXT"].ToString(),
                            ID_REQ_DETAIL = (int)req["ID_REQ_DETAIL"],
                            ITEM_NAME = req["ITEM_NAME"].ToString(),
                        }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static void generateDDLStatus(DropDownList ddl, string requestType)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<ACTIVITY_MAPPING> act = new List<ACTIVITY_MAPPING>();
            act = data.ACTIVITY_MAPPINGs.Where(x => x.REQUEST_TYPE.Equals(requestType)).ToList();

            if (requestType.Equals("SUM")) //summary and requisition has same activities
                requestType = "REQ";

            ddl.Items.Clear();

            if (act.Count == 0 && requestType.Equals("AD"))
                ddl.Items.Add(new ListItem("None", "0"));
            else if (requestType.Equals(""))
                ddl.Items.Add(new ListItem("ALL", "0"));
            else
            {
                ddl.Items.Add(new ListItem("ALL", "0"));
                foreach (var i in act)
                    ddl.Items.Add(new ListItem(i.ACTIVITY_TEXT, i.ACTIVITY_ID.ToString()));
            }
        }

        
    }
}