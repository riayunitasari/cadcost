﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;

namespace IntegratedPurchasingSystem.Helper
{
    public class ApprovalHelper
    {
        public static string getApprovalListReport(int idRequest, string type)
        {
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);
            string app = "";

            List<TBL_ACTION_HISTORY> actions = data.TBL_ACTION_HISTORies.Where(x =>
               x.APP_ID == 2 && x.PROCESS_ID == idRequest && x.REQ_TYPE.Equals(type) 
               &&  x.ACTION_ID == Variable.Action.Approve && x.ACTIVITY_TEXT.Contains("APPROVAL")).ToList();

            if (actions.Count > 0)
            {
                app = "Approved By : " + Environment.NewLine;
                foreach (var i in actions)
                {
                    app += "- " + i.COMPLETED_BY_NAME +" (";
                    if(!i.PARTICIPANT.Equals(i.COMPLETED_BY))
                        app+= "On Behalf Of - ";
                    if (i.ACTIVITY_ID == Variable.Status.ApprovalByDeptMgr)
                        app += "Department Manager)";
                    else if (i.ACTIVITY_ID == Variable.Status.ApprovalByPM)
                        app += "Project Manager)";
                    else if (i.ACTIVITY_ID == Variable.Status.ApprovalByDivMgr)
                        app += "Division Manager)";
                    else if (i.ACTIVITY_ID == Variable.Status.ApprovalByGADept)
                        app += "General Affair Department Manager)";
                    else if (i.ACTIVITY_ID == Variable.Status.ApprovalByManagement)
                        app += "Management)";
                    app += " - " + ((DateTime)i.END_DATE).ToString("dd MMM yyyy")+ Environment.NewLine;
                }
            }
            return app;
        }

        public static List<TBL_ACTION_HISTORY> getApprovalList(int idRequest, string type)
        {
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);
            string app = "";

            List<TBL_ACTION_HISTORY> actions = data.TBL_ACTION_HISTORies.Where(x =>
               x.APP_ID == 2 && x.PROCESS_ID == idRequest && x.REQ_TYPE.Equals(type)
               && ((x.ACTION_ID == Variable.Action.Approve && x.ACTIVITY_TEXT.Contains("APPROVAL")) ||
               (x.ACTION_ID == Variable.Action.ProceedToNextApp || x.ACTIVITY_ID == Variable.Status.CheckedByCOD ))).ToList();

            
            return actions;
        }
    }
}