﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegratedPurchasingSystem.Helper
{
    public class GeneralHelper
    {
        public static void generateDDLCurrency(DropDownList ddl)
        {
            ddl.Items.Clear();
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);

            List<tblCurrency> cur = data.tblCurrencies.ToList();
            ddl.Items.Add(new ListItem("None", ""));
            foreach (var i in cur)
            {
                ddl.Items.Add(new ListItem(i.curName, i.CurrencyCD));
            }
        }

        public static void generateDDLMetricPlural(DropDownList ddlPlural)
        {
            ddlPlural.Items.Clear();

            ddlPlural.Items.Add("-- Select Metric --");
            ddlPlural.Items.Add("pcs");
            ddlPlural.Items.Add("boxes");
            ddlPlural.Items.Add("packs");
            ddlPlural.Items.Add("units");
            ddlPlural.Items.Add("seats");
            ddlPlural.Items.Add("kg");
            ddlPlural.Items.Add("g");
            ddlPlural.Items.Add("m");
            ddlPlural.Items.Add("cm");
        }

        public static void generateDDLMetricSingular(DropDownList ddlSingular)
        {
            ddlSingular.Items.Clear();

            ddlSingular.Items.Add("-- Select Metric --");
            ddlSingular.Items.Add("pc");
            ddlSingular.Items.Add("box");
            ddlSingular.Items.Add("pack");
            ddlSingular.Items.Add("unit");
            ddlSingular.Items.Add("seat");
            ddlSingular.Items.Add("kg");
            ddlSingular.Items.Add("g");
            ddlSingular.Items.Add("m");
            ddlSingular.Items.Add("cm");

        }


        public static void setDDLDepartment(DropDownList ddl)
        {
            ddl.ClearSelection();
            PFNATTDataContext data = new PFNATTDataContext(Connection.PFNATTConn);

            List<TBL_SECTION> dept = data.TBL_SECTIONs.ToList();
            ddl.Items.Add(new ListItem("ALL", ""));

            foreach (var i in dept)
            {
                ddl.Items.Add(new ListItem(i.SECT_NAME, i.SECT_ID));
            }
        }

        public static void gridRender(GridView grid, Page page)
        {
            foreach (GridViewRow r in grid.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    r.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
                    r.Attributes["onmouseout"] = "this.style.textDecoration='none';";
                    r.ToolTip = "Click to select row";
                    r.Attributes["onclick"] = page.ClientScript.GetPostBackClientHyperlink(grid, "Select$" + r.RowIndex, true);

                }
            }
        }

        public static void insertImpersonate(string winCredential, string impersonate)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
            if (a != null)
            {
                a.IMPERSONATE = impersonate;
                data.SubmitChanges();
            }
        }

        public static void releaseImpersonate(string winCredential)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            ADMIN a = data.ADMINs.FirstOrDefault(x => x.WindowsCredential.ToLower().Equals(winCredential.ToLower()));
            if (a != null)
            {
                a.IMPERSONATE = null;
                data.SubmitChanges();
            }
        }

        public static bool isAdmin(string winCredential)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            return data.ADMINs.Any(x => x.WindowsCredential.ToLower().Contains(winCredential.ToLower()));
        }

        public static void generateDDLAction(DropDownList ddl, int appID, int activityID)
        {
            ddl.Items.Clear();
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);

            List<TBL_ACTION_MAPPING> cur = data.TBL_ACTION_MAPPINGs.Where(x => x.APP_ID == appID && x.ACTIVITY_ID == activityID).ToList();
            ddl.Items.Add(new ListItem("-- Select Action --", ""));
            foreach (var i in cur)
            {
                ddl.Items.Add(new ListItem(i.ACTION_TEXT, i.ID_ACTION.ToString()));
            }
        }

        public static Employee isParticipant(Employee logInUser, int appID, int processID, string type)
        {
            GA_APPDataContext data = new GA_APPDataContext(Connection.GA_APP_Conn);

            List<TBL_ACTION_HISTORY> acts = data.TBL_ACTION_HISTORies.Where(
                   x => x.APP_ID == appID && x.PROCESS_ID == processID && x.REQ_TYPE.Equals(type)).ToList();

            TBL_ACTION_HISTORY act = (acts).OrderByDescending(y => y.ID_HISTORY).FirstOrDefault();
            if (act != null)
            {
                if (act.PARTICIPANT.Equals(logInUser.EMP_ID))
                    logInUser.IS_CUR_PARTICIPANT = true;

                if (act.PARTICIPANT_ONBEHALF != null)
                    if (act.PARTICIPANT_ONBEHALF.Contains(logInUser.EMP_ID))
                        logInUser.IS_CUR_PARTICIPANT = true;

                if (acts.Any(x => x.PARTICIPANT.Equals(logInUser.EMP_ID)))
                    logInUser.ISPARTICIPANT = true;

                foreach (var i in acts)
                    if (i.PARTICIPANT_ONBEHALF != null)
                        if (i.PARTICIPANT_ONBEHALF.Contains(logInUser.EMP_ID))
                            logInUser.ISPARTICIPANT = true;
            }
            return logInUser;
        }

        public static double getExchangeRate(string currency)
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            double exchangeRate = 0;
            int currencyID = data.tblCurrencies.FirstOrDefault(x => x.CurrencyCD.Equals(currency)).CurrencyID;
            if (currencyID > 0)
            {
                double buyRate = decimal.ToDouble(data.tblCurConvertions.OrderByDescending(x => x.DateID).FirstOrDefault(x => x.CurrencyID == currencyID).cucBuyRate);
                double sellRate = decimal.ToDouble(data.tblCurConvertions.OrderByDescending(x => x.DateID).FirstOrDefault(x => x.CurrencyID == currencyID).cucSellRate);
                exchangeRate = (buyRate + sellRate) / 2;
            }

            return exchangeRate;
        }

        public static void mergeAllSameValueCells(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (row.Cells[i].Text == previousRow.Cells[i].Text)
                    {
                        row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                               previousRow.Cells[i].RowSpan + 1;
                        previousRow.Cells[i].Visible = false;
                    }
                }
            }
        }

        public static void mergeColumn(GridView gridView, int columnMerge)
        {
            for (int i = gridView.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gridView.Rows[i];
                GridViewRow previousRow = gridView.Rows[i - 1];
                if (!string.IsNullOrEmpty(row.Cells[columnMerge].Text) && !string.IsNullOrEmpty(previousRow.Cells[columnMerge].Text))
                    if (row.Cells[columnMerge].Text == previousRow.Cells[columnMerge].Text)
                    {
                        if (previousRow.Cells[columnMerge].RowSpan == 0)
                        {
                            if (row.Cells[columnMerge].RowSpan == 0)
                            {
                                previousRow.Cells[columnMerge].RowSpan += 2;
                            }
                            else
                            {
                                previousRow.Cells[columnMerge].RowSpan = row.Cells[columnMerge].RowSpan + 1;
                            }
                            row.Cells[columnMerge].Visible = false;
                        }
                    }
            }
        }

        public static void mergeLinkButtonColumn(GridView gridView, int columnMerge, string linkButtonName)
        {
            for (int i = gridView.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gridView.Rows[i];
                GridViewRow previousRow = gridView.Rows[i - 1];

                LinkButton curCell = (LinkButton)row.Cells[columnMerge].FindControl(linkButtonName);
                LinkButton prevCell = (LinkButton)previousRow.Cells[columnMerge].FindControl(linkButtonName);

                if (curCell != null && prevCell != null)
                    if (!string.IsNullOrEmpty(curCell.Text) && !string.IsNullOrEmpty(prevCell.Text))
                        if (curCell.Text.Equals(prevCell.Text))
                        {
                            if (previousRow.Cells[columnMerge].RowSpan == 0)
                            {
                                if (row.Cells[columnMerge].RowSpan == 0)
                                {
                                    previousRow.Cells[columnMerge].RowSpan += 2;
                                }
                                else
                                {
                                    previousRow.Cells[columnMerge].RowSpan = row.Cells[columnMerge].RowSpan + 1;
                                }
                                row.Cells[columnMerge].Visible = false;
                            }
                        }
            }
        }

    }
}