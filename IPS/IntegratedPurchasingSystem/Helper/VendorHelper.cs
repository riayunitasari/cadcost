﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class VendorHelper
    {
        //public static List<VENDOR> getVendor(string search)
        //{   
        //    PurchaseSystemDataContext data = new  PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    List<VENDOR> vendors = new List<VENDOR>();

        //    vendors = data.VENDORs.Where( x => (x.CODE.Contains(search) || x.VENDOR_NAME.Contains(search) || x.ADDRESS.Contains(search))
        //        && x.ISDELETED != 1).OrderByDescending(y => y.ID_VENDOR).ToList();

        //    return vendors;
        //}

        //public static void insertNewVendor(string code, string name, string address)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    VENDOR vendor = new VENDOR();
        //    vendor.CODE = code;
        //    vendor.VENDOR_NAME = name;
        //    vendor.ADDRESS = address;
        //    vendor.ISDELETED = 0;

        //    data.VENDORs.InsertOnSubmit(vendor);
        //    data.SubmitChanges();
        //}

        //public static VENDOR getVendorByID(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

        //    return data.VENDORs.FirstOrDefault(x => x.ID_VENDOR == id);
        //}

        //public static void updateVendor(int id, string code, string name, string address)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    VENDOR vendor = data.VENDORs.FirstOrDefault(x => x.ID_VENDOR == id);
        //    if (vendor != null)
        //    {
        //        vendor.CODE = code;
        //        vendor.VENDOR_NAME = name;
        //        vendor.ADDRESS = address;

        //        data.SubmitChanges();
        //    }
        //}

        //public static void deleteVendor(int id)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    VENDOR vendor = data.VENDORs.FirstOrDefault(x => x.ID_VENDOR == id);
        //    if (vendor != null)
        //        vendor.ISDELETED = 1;
        //    data.SubmitChanges();
        //}

        //public static bool isCodeAlreadyExist(string code)
        //{
        //    PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
        //    return data.VENDORs.Any(x => x.CODE.ToLower().Equals(code.ToLower()));
        //}

        public static List<tblVendor> getVendor(string searchBy, string text)
        {
            SOFIDataContext data = new SOFIDataContext(Connection.SOFIConn);
            string name = "", code = "", address = "";

            if (searchBy.Equals("Name"))
                name = text;
            else if (searchBy.Equals("Code"))
                code = text;
            else if (searchBy.Equals("Address"))
                address = text;

            List<tblVendor> vendor = data.tblVendors.Where(x => x.vndName.Contains(name) && x.VendorCD.Contains(code)
                && x.vndAddress.Contains(address)).ToList(); ;


            return vendor;
        }


    }
}