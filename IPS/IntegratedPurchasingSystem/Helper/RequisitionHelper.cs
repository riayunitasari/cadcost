﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem.Helper
{
    public class RequisitionHelper
    {
        public static void setDDLYearForRequisition(DropDownList ddl)
        {
            ddl.ClearSelection();
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            var year = data.REQUISITIONs.GroupBy(x => x.DATE_CREATED.Value.Year).Select(x => x.First()).ToList();
            foreach (var i in year)
                ddl.Items.Add(i.DATE_CREATED.Value.Year.ToString());

            if (!year.Any())
                ddl.Items.Add(DateTime.Now.Year.ToString());
        }

        public static List<RequisitionObject> getFilteredRequisition(
            int reqMonth, int reqYear, string reqType, int status, string reqBy, string dept, string jobcode, string empID, int partID)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getFilteredRequisition", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@reqMonth", reqMonth);
            command.Parameters.AddWithValue("@reqYear", reqYear);
            command.Parameters.AddWithValue("@reqType", reqType);
            command.Parameters.AddWithValue("@status", status);
            command.Parameters.AddWithValue("@reqBy", reqBy);
            command.Parameters.AddWithValue("@reqDept", dept);
            command.Parameters.AddWithValue("@jobcode", jobcode);
            command.Parameters.AddWithValue("@empID", empID);
            command.Parameters.AddWithValue("@partID", partID);

            connection.Open();
            List<RequisitionObject> reqs = new List<RequisitionObject>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reqs = (from req in reader.Cast<DbDataRecord>()
                      select new RequisitionObject
                      {
                          ID_REQ = (int)req["ID_REQ"],
                          DEPT_REQ_NO = req["REQ_NO"].ToString(),
                          JOBCODE = req["JOBCODE"].ToString(),
                          REQ_BY = req["REQ_BY"].ToString(),
                          REQ_BY_NAME = req["REQ_BY_NAME"].ToString(),
                          DATE_CREATED = (DateTime)req["DATE_CREATED"],
                          REQ_TYPE_TEXT = req["REQ_TYPE_TEXT"].ToString(),
                          ACTIVITY_ID = (int)req["ACTIVITY_ID"],
                          ACTIVITY_TEXT = req["ACTIVITY_TEXT"].ToString(),
                          REQ_BY_DEPT = req["REQ_BY_DEPT"].ToString(),
                          REQ_TYPE = req["REQ_TYPE"].ToString()
                      }).ToList();
            }
            connection.Close();

            return reqs;
        }

        public static void insertRequisition(RequisitionObject req, out int id)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            string prevGANo = "";
            REQUISITION r = new REQUISITION();
            if (string.IsNullOrEmpty(req.GA_REQ_NO) && req.GA_REQ_NO.Length > 0)
            {
                prevGANo = r.GA_REQ_NO;
                r.GA_REQ_NO = req.GA_REQ_NO;
                 
            }
            r.REQ_NO = req.DEPT_REQ_NO;
            r.REQ_BY = req.REQ_BY;
            r.DATE_CREATED = req.DATE_CREATED;
            r.JOBCODE = req.JOBCODE;
            r.REQ_TYPE = req.REQ_TYPE;
            r.DESCRIPTION = req.DESCRIPTION;
            r.REMARKS = req.REMARKS;
            r.CURRENCY_CODE = req.CURRENCY_CODE;
            r.EXCHANGE_RATE = req.EXCHANGE_RATE;
            r.TOTAL_EST_AMOUNT = req.TOTAL_EST_AMOUNT;
            r.ISDELETED = Variable.DeleteStatus.NotDeleted;
            r.ISDELETED = 0;
            r.ATTACHMENT = req.ATTACHMENT;
            r.ID_PO = 0;
            data.REQUISITIONs.InsertOnSubmit(r);
            data.SubmitChanges();

            id = r.ID_REQ;

            if (req.GA_REQ_NO.Length > 0 && !req.GA_REQ_NO.Equals(prevGANo))
                generateDocNo();

        }

        public static void generateDocNo()
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            DOC_NO no = data.DOC_NOs.FirstOrDefault();
            no.NO_REQ += 1;
            data.SubmitChanges();
        }

        public static int getNextReqNo()
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            return (int)data.DOC_NOs.FirstOrDefault(x => x.TYPE.Equals("REQ")).NO_REQ ;
        }

        public static void insertSingleDetialItem(RequisitionItems item, int idReq, out int detailID)
        {
            int partTypeID = 0;
            string partTypeName = "";

            partTypeID = ItemHelper.getPartType(item.PART_ID, out partTypeName);

            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            REQ_DETAIL detail = new REQ_DETAIL();
            detail.ID_ITEM = item.PART_ID;
            detail.ITEM_NAME = item.PART_NAME;
            detail.PART_CODE = item.PART_CODE;
            detail.PART_TYPE_ID = (short)partTypeID;
            detail.PART_TYPE_NAME = partTypeName;
            detail.ID_REQ = idReq;
            detail.PRICE = item.PRICE;
            detail.QUANTITY = item.QUANTITY;
            detail.METRIC_CODE = item.METRIC;
            detail.AMOUNT = item.QUANTITY * item.PRICE;
            detail.REMARKS = item.REMARK;
            detail.ID_QUO = 0;
            detail.ID_ADVANCE = 0;
            data.REQ_DETAILs.InsertOnSubmit(detail);
            data.SubmitChanges();
            detailID = detail.ID;
        }

        public static void insertDetailItem(List<RequisitionItems> items, int idReq)
        {   
            foreach (var i in items)
            {
                int detailId = 0;
                insertSingleDetialItem(i, idReq, out detailId);
            }
        }

        public static bool isApprover(string empID)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            
            //if (data.REQUISITIONs.Where(x => x.FIRSTRATER.Equals(empID) || x.SECONDRATER.Contains(empID)
            //    || x.JGC_RATER.Equals(empID)).Any())
            //{
                //if (data.REQUISITIONs.Where(x => x.FIRSTRATER.Equals(empID) && x.ID_REQ == idReq).Any())
                //    appRater = 1;
                //else if (data.REQUISITIONs.Where(x => x.SECONDRATER.Contains(empID) && x.ID_REQ == idReq).Any())
                //    appRater = 2;
                //else if (data.REQUISITIONs.Where(x => x.JGC_RATER.Equals(empID) && x.ID_REQ == idReq).Any())
                //    appRater = 3;
            //    return true;
            //}
            return false;
        }

        

        public static RequisitionObject getRequisitionByID(int id)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("getRequisitionByID", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", id);

            connection.Open();
            RequisitionObject req = new RequisitionObject();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                req = (from r in reader.Cast<DbDataRecord>() orderby r.GetInt32(0) descending
                        select new RequisitionObject
                        {
                            ID_REQ = (int)r["ID_REQ"],
                            GA_REQ_NO = r["GA_REQ_NO"].ToString(),
                            DEPT_REQ_NO = r["REQ_NO"].ToString(),
                            JOBCODE = r["JOBCODE"].ToString(),
                            PROJECT_NAME = r["PROJECT_NAME"].ToString(),
                            REQ_BY = r["REQ_BY"].ToString(),
                            REQ_BY_NAME = r["REQ_BY_NAME"].ToString(),
                            REQ_BY_DEPT = r["REQ_BY_DEPT"].ToString(),
                            REMARKS = r["REMARKS"].ToString(),
                            TOTAL_EST_AMOUNT = (double) r["TOTAL_EST_AMOUNT"],
                            CURRENCY_CODE = r["CURRENCY_CODE"].ToString(),
                            EXCHANGE_RATE = (double)r["EXCHANGE_RATE"],
                            DATE_CREATED = (DateTime)r["DATE_CREATED"],
                            REQ_TYPE = r["REQ_TYPE"].ToString(),
                            REQ_TYPE_TEXT = r["REQ_TYPE_TEXT"].ToString(),
                            ACTIVITY_ID = (int)r["ACTIVITY_ID"],
                            ACTIVITY_TEXT = r["ACTIVITY_TEXT"].ToString(),
                            ATTACHMENT = r["ATTACHMENT"].ToString(),
                            ISDELETED = (int)r["ISDELETED"],
                            DESCRIPTION = r["DESCRIPTION"].ToString(),
                            ID_PO = (int)r["ID_PO"]
                        }).FirstOrDefault();
            }
            connection.Close();

            return req;
        }

        public static List<RequisitionItems> getRequisitionDetail(int idReq)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<RequisitionItems> items = (from d in data.REQ_DETAILs
                                            where d.ID_REQ == idReq
                                            select new RequisitionItems
                                            {
                                                ID_REQ = (int)d.ID_REQ,
                                                ID_DETAIL = d.ID,
                                                PART_ID = (int)d.ID_ITEM,
                                                PART_NAME = d.ITEM_NAME,
                                                PART_CODE = d.PART_CODE,
                                                TYPE_ID = (int)d.PART_TYPE_ID,
                                                TYPE_NAME = d.PART_TYPE_NAME,
                                                QUANTITY = (double)d.QUANTITY,
                                                PRICE = (double)d.PRICE,
                                                METRIC = d.METRIC_CODE,
                                                REMARK = d.REMARKS,
                                                ID_QUO = (int)d.ID_QUO,
                                                ID_ADVANCE = (int) d.ID_ADVANCE
                                            }).ToList();
            return items;
        }

        public static void updateRequisition(RequisitionObject req)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            string prevGANo = "";

            REQUISITION r = data.REQUISITIONs.FirstOrDefault(x => x.ID_REQ == req.ID_REQ);
            if (r != null)
            {
                prevGANo = r.GA_REQ_NO;
                r.GA_REQ_NO = req.GA_REQ_NO;
                r.REQ_NO = req.DEPT_REQ_NO;
                r.JOBCODE = req.JOBCODE;
                r.CURRENCY_CODE = req.CURRENCY_CODE;
                r.EXCHANGE_RATE = req.EXCHANGE_RATE;
                r.DESCRIPTION = req.DESCRIPTION;
                r.REMARKS = req.REMARKS;
                r.TOTAL_EST_AMOUNT = req.TOTAL_EST_AMOUNT;
                r.ATTACHMENT = req.ATTACHMENT;
            }
            data.SubmitChanges();
            if (req.GA_REQ_NO.Length > 0 && !req.GA_REQ_NO.Equals(prevGANo))
                generateDocNo();

        }

        public static void deleteSingleDetail(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            REQ_DETAIL d = data.REQ_DETAILs.Where(x => x.ID == idDetail).SingleOrDefault();

            if (data.RELATED_DETAILs.Any(x => x.ID_REQ_DETAIL == idDetail))
                data.RELATED_DETAILs.DeleteAllOnSubmit(data.RELATED_DETAILs.Where(y => y.ID_REQ_DETAIL == idDetail));

            data.REQ_DETAILs.DeleteOnSubmit(d);
            data.SubmitChanges();
        }

        public static void updateSingleDetail(int idDetail, double price, double quantity, string metric, string remark)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            REQ_DETAIL detail = data.REQ_DETAILs.FirstOrDefault(x => x.ID == idDetail);
            if (detail != null)
            {
                detail.PRICE = price;
                detail.QUANTITY = quantity;
                detail.METRIC_CODE = metric;
                detail.REMARKS = remark;
            }
            data.SubmitChanges();
        }

        public static void updateDetailRequisition(List<RequisitionItems> items, int idReq)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            List<REQ_DETAIL> detail = data.REQ_DETAILs.Where(x => x.ID_REQ == idReq).ToList();
            for (int i = 0; i < detail.Count; i++) //delete
            {
                bool isExist = items.Any(x => x.ID_DETAIL.Equals(detail[i].ID));
                if (!isExist)
                    deleteSingleDetail(detail[i].ID);
            }

            for (int i = 0; i < items.Count; i++)// add new
            {
                int idDetail = 0;
                bool isExist = detail.Any(x => x.ID.Equals(items[i].ID_DETAIL));
                if (!isExist)
                    insertSingleDetialItem(items[i], idReq, out idDetail); //new
                else //already exist, update then
                    updateSingleDetail(items[i].ID_DETAIL, items[i].PRICE, items[i].QUANTITY, items[i].METRIC, items[i].REMARK);
            }
        }

        public static void deleteRequisition(int idReq)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            REQUISITION r = data.REQUISITIONs.FirstOrDefault(x => x.ID_REQ == idReq);
            if (r != null)
            {
                r.ISDELETED = 1;
                r.ATTACHMENT = "";
            }
            if (data.RELATED_DETAILs.Any(x => x.ID_SUM_DETAIL == idReq))
            {
                List<RELATED_DETAIL> sums = data.RELATED_DETAILs.Where(x => x.ID_SUM_DETAIL == idReq).ToList();
                data.RELATED_DETAILs.DeleteAllOnSubmit(sums);
            }
            data.SubmitChanges();
        }

        public static RequisitionItems getSingleDetailByID(int idDetail)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            RequisitionItems item = (from d in data.REQ_DETAILs
                                            select new RequisitionItems
                                            {
                                                ID_REQ = (int)d.ID_REQ,
                                                ID_DETAIL = d.ID,
                                                PART_ID = (int)d.ID_ITEM,
                                                PART_NAME = d.ITEM_NAME,
                                                QUANTITY = (double)d.QUANTITY,
                                                PRICE = (double)d.PRICE,
                                                METRIC = d.METRIC_CODE,
                                                REMARK = d.REMARKS
                                            }).FirstOrDefault();
            return item;
        }

        public static void updateReqDetailQuot(List<RequisitionItems> details, int idQuo)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            foreach (var i in details)
            {
                REQ_DETAIL detail = data.REQ_DETAILs.FirstOrDefault(x => x.ID == i.ID_DETAIL);
                detail.ID_QUO = idQuo;
                data.SubmitChanges();
            }
        }

        public static void updateReqDetailAdvance(List<RequisitionItems> details, int idAdvance)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);
            foreach (var i in details)
            {
                REQ_DETAIL detail = data.REQ_DETAILs.FirstOrDefault(x => x.ID == i.ID_DETAIL);
                detail.ID_ADVANCE = idAdvance;
                data.SubmitChanges();
            }
        }

        public static int insertIntoAdvance(int idReq, string empID, string jobcode)
        {
            SqlConnection connection = new SqlConnection(Connection.PurchaseSysConn);

            SqlCommand command = new SqlCommand("insertIntoAdvance", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idReq", idReq);
            command.Parameters.AddWithValue("@empID", empID);
            command.Parameters.AddWithValue("@jobcode", jobcode);

            connection.Open();
            int idAdvance = 0;
            idAdvance = (int)command.ExecuteScalar();
            connection.Close();

            return idAdvance;
        }

        public static List<RequisitionObject> getReqForReportSummary(List<RequisitionObject> requisitions)
        {
            PurchaseSystemDataContext data = new PurchaseSystemDataContext(Connection.PurchaseSysConn);

            foreach (var req in requisitions)
            {
                req.ITEMS = RequisitionHelper.getRequisitionDetail(req.ID_REQ);
                foreach (var i in req.ITEMS)
                {
                    if (i.ID_QUO > 0)
                    {
                        i.QUOTATION_OBJECT = QuotHelper.getQuotationByID(i.ID_QUO);
                        i.QUOTATION_OBJECT.VENDOR = QuotHelper.getQuotationDetail(i.ID_QUO);
                    }
                }

            }

            return requisitions;
        }
    }
}