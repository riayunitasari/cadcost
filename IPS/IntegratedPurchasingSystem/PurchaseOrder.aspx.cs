﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;

namespace IntegratedPurchasingSystem
{
    public partial class PurchaseOrder : System.Web.UI.Page
    {
        const string curUser = "current_Log_In_User";
        const string curPO = "current_Purchase_Order";
        const string curItem = "current_Purchase_Order_Item";
        const string curState = "current_PO_State";
        Employee logInUser = new Employee();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                Helper.GeneralHelper.generateDDLCurrency(ddlCurrency);
                ViewState[curPO] = new PO_Object();
                ViewState[curItem] = new List<PO_Item>();
                int idPO = 0;
                if (int.TryParse(Request["ViewPO"], out idPO))
                {
                    PO_Object po = POHelper.getPOByID(idPO);
                    if (po != null)
                    {   
                        logInUser = GeneralHelper.isParticipant(logInUser, 2, po.ID , "PO");
                        List<PO_Item> items = POHelper.getPOItem(idPO);
                        ViewState[curPO] = po;
                        ViewState[curItem] = items;
                        
                        if (logInUser.IS_CUR_PARTICIPANT &&
                            (po.ACTIVITY_ID == Variable.Status.FixRequest || po.ACTIVITY_ID == Variable.Status.PreparePO))
                            ViewState[curState] = "edit";
                        else
                            ViewState[curState] = "view";

                        bindPO();
                        GeneralHelper.generateDDLAction(ddlAction, 2, po.ACTIVITY_ID);
                    }

                }
                //else
                //    Response.Redirect("PurchaseTracking.aspx");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridVendorSearch, this);

            base.Render(writer);
        }

        private void onViewState()
        {
            PO_Object po = (PO_Object)ViewState[curPO];

            vendorSearch.Visible = false;
            lblVendor.Text = po.VENDOR_NAME;
            lblVendorPIC.Text = po.VENDOR_PIC;
            lblVendorAddress.Text = po.VENDOR_ADDRESS;
            lblPlace.Text = po.PLACE;
            lblDelTime.Text = po.DELIVERY_TIME;
            lblPayTerm.Text = po.PAYMENT_TERM;
            lblCurrency.Text = po.CURRENCY_CODE;
            //lblQuotedPrice.Text = po.QUO_NET_PRICE.ToString();
            lblVAT.Text = po.VAT;
            //lblDiscount.Text = po.DISCOUNT.ToString();
            lblRemark.Text = po.REMARK;
            vendorSearch.Visible = false;
            actionDiv.Visible = false;
            gridItem.Columns[7].Visible = false;
            ddlCurrency.Visible = false;
            txtPlace.Visible = false;
            ddlPayTerms.Visible = false;
            txtDelTime.Visible = false;
            txtRemark.Visible = false;
            txtDescription.Visible = false;
            ddlVAT.Visible = false;
            txtDiscount.Visible = false;
        }

        private void onEditState()
        {   
            PO_Object po = (PO_Object)ViewState[curPO];

            lblVendor.Visible = false;
            lblDelTime.Visible = false;
            lblPlace.Visible = false;
            lblPayTerm.Visible = false;
            lblCurrency.Visible = false;
            vendorSearch.Visible = true;
            txtVendor.Text = po.VENDOR_NAME;           
            ddlPayTerms.SelectedIndex = ddlPayTerms.Items.IndexOf(ddlPayTerms.Items.FindByText(po.PAYMENT_TERM));
            txtPlace.Text = po.PLACE;
            txtDelTime.Text = po.DELIVERY_TIME;
            ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(po.CURRENCY_CODE));
            txtDescription.Text = po.ITEM_DESCRIPTION;
            txtRemark.Text = po.REMARK;
            actionDiv.Visible = true;
            gridItem.Columns[7].Visible = true;
            lblDiscount.Visible = false;
            ddlVAT.SelectedIndex = ddlVAT.Items.IndexOf(ddlVAT.Items.FindByValue(po.VAT));
            txtDiscount.Text = po.DISCOUNT.ToString();
        }

        private void bindPO()
        {
            PO_Object po = (PO_Object)ViewState[curPO];
            List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
            linkRequestNo.Text = po.REQUEST_NO;
            lblDateCreated.Text = po.CREATED_DATE.ToString("dd MMM yyyy");
            lblJobCode.Text = po.JOBCODE;
            lblProjectName.Text = po.PROJECT_NAME;
            lblVendorID.Text = po.VENDOR_ID.ToString();
            lblVendorPIC.Text = po.VENDOR_PIC;
            lblVendorAddress.Text = po.VENDOR_ADDRESS;
            lblExchangeRate.Text = string.Format("{0:#,##0.00}", po.EXCHANGE_RATE);
            lblTotalAmount.Text = string.Format("{0:#,##0.00}", po.TOTAL_AMOUNT);
            lblActivity.Text = po.ACTIVITY_TEXT;
            lblStatus.Text = po.STATUSID <= 0 ? "None" : po.STATUS_TEXT;
            lblDiscount.Text = string.Format("{0:#,##0.00}", po.DISCOUNT);
            lblQuotedPrice.Text = string.Format("{0:#,##0.00}", po.QUO_NET_PRICE);
            lblTotAmountOfItem.Text = string.Format("{0:#,##0.00}", items.Sum(x => x.QUANTITY * x.PRICE));
            loadActionHistory();
            loadGridItem();
            if (ViewState[curState].Equals("edit"))
                onEditState();
            else if(ViewState[curState].Equals("view")) 
                onViewState();
            //btnGenerateTotalAmount_Click(btnGenerateTotalAmount, EventArgs.Empty);
        }

        private void loadActionHistory()
        {
            PO_Object po = (PO_Object)ViewState[curPO];
            gridActionHistory.DataSource = Helper.ActionLogHelper.getActionLog(po.ID, "PO");
            gridActionHistory.DataBind();
        }

        private void loadGridItem()
        {
            List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
            gridItem.DataSource = items;
            gridItem.DataBind();
        }

        protected void gridItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = gridItem.Rows[e.NewEditIndex];
            Label metric = row.FindControl("lblMetric") as Label;
            Label quantity = row.FindControl("lblQuantity") as Label;

            gridItem.EditIndex = e.NewEditIndex;
            loadGridItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "metric load on Edit", "gridItemOnEdit(" + quantity.Text + ", '" + metric.Text + "'," + e.NewEditIndex + ")", true);
        }

        protected void gridItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridItem.EditIndex = -1;
            loadGridItem(); 
        }

        protected void gridItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            double valQuantity = 0, valPrice = 0;
            Label itemID = (Label)gridItem.Rows[e.RowIndex].FindControl("lblItemID");
            Label quantity = (Label)gridItem.Rows[e.RowIndex].FindControl("lblQuantity");
            DropDownList metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricSingular");
            TextBox price = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtPrice");
            bool isQNumber = double.TryParse(quantity.Text.Replace(",", ""), out valQuantity);
            bool isPNumber = double.TryParse(price.Text.Replace(",", ""), out valPrice);
            TextBox remark = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtRemarks");

            if (valQuantity > 1)
                metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricPlural");

            if (!isPNumber)
            {
                gridItem.EditIndex = -1;
                loadGridItem();
                ScriptManager.RegisterStartupScript(this, GetType(), "metricload", "resetMetric();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "update item failed", "showMsg('Error!','Invalid input!<br /> Please check your input')", true);
            }
            else
            {
                List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
                PO_Item item = items.FirstOrDefault(x => x.ID_ITEM == int.Parse(itemID.Text));

                item.METRIC = metric.SelectedItem.Text;
                item.PRICE = valPrice;
                item.REMARK = remark.Text;

                ViewState[curItem] = items;
                gridItem.EditIndex = -1;
                loadGridItem();
                btnGenerateTotalAmount_Click(btnGenerateTotalAmount, EventArgs.Empty);
            }

        }

        protected void gridItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label itemID = (Label)gridItem.Rows[e.RowIndex].FindControl("lblItemID");
            List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
            PO_Item item = items.FirstOrDefault(x => x.ID_ITEM == int.Parse(itemID.Text));
            items.Remove(item);
            ViewState[curItem] = items;
            loadGridItem();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                PO_Object po = (PO_Object)ViewState[curPO];
                List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
                logInUser = (Employee)ViewState[curUser];
                int actionID = 0;
                bool isAction;
                isAction = int.TryParse(ddlAction.SelectedItem.Value, out actionID);
                string vatText = "";
                double vat = 0, discount = 0, totalAmountItem = 0, totalAmount = 0;

                if (ViewState[curState].ToString().Equals("edit"))
                {
                    bool isTotalItem = double.TryParse(lblTotAmountOfItem.Text, out totalAmountItem);
                    bool isDiscount = double.TryParse(txtDiscount.Text, out discount);
                    bool isTotalAmount = double.TryParse(lblTotalAmount.Text, out totalAmount);

                    if (ddlPayTerms.SelectedItem.Text.Contains("Others"))
                        po.PAYMENT_TERM = txtPayTermOther.Text;
                    else po.PAYMENT_TERM = ddlPayTerms.SelectedItem.Text;

                    vat = 0;
                    if (ddlVAT.SelectedItem.Text.Contains("Exclude"))
                        vat = (totalAmountItem - discount) * 0.1;
                    vatText = ddlVAT.SelectedItem.Value;

                    totalAmount = (totalAmountItem - discount) + vat;

                    po.VENDOR_ID = int.Parse(lblVendorID.Text);
                    po.VENDOR_NAME = txtVendor.Text;
                    po.VENDOR_PIC = lblVendorPIC.Text;
                    po.VENDOR_ADDRESS = lblVendorAddress.Text;
                    po.DELIVERY_TIME = txtDelTime.Text;
                    po.CURRENCY_CODE = ddlCurrency.SelectedItem.Value;
                    po.EXCHANGE_RATE = double.Parse(lblExchangeRate.Text);
                    po.ITEM_DESCRIPTION = txtDescription.Text;
                    po.DISCOUNT = discount;
                    po.VAT = vatText;
                    po.VAT_AMOUNT = vat;
                    po.TOTAL_AMOUNT = totalAmount;
                    po.REMARK = txtRemark.Text;
                    Helper.POHelper.updatePO(po);
                    Helper.POHelper.updateDetailPO(po.ID, items);
                    Helper.ActionLogHelper.insertPOHistory(logInUser.EMP_ID, po.ID, actionID, po.ACTIVITY_ID, txtComment.Text);

                    ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessNew", "saveSuccess('Success','Your change has been saved successfully!');", true);
                }
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "submitError", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            }
        }

        #region Vendor
        private void loadGridVendor()
        {
            gridVendorSearch.DataSource = VendorHelper.getVendor(ddlVendor.SelectedItem.Text, txtSearchVendor.Text);
            gridVendorSearch.DataBind();
            ScriptManager.RegisterStartupScript(this, GetType(), "set vendor center", "setCenter('dialogVendor');", true);
        }

        protected void btnSearchVendor_Click(object sender, EventArgs e)
        {
            loadGridVendor();
            ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
        }

        protected void gridVendorSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idVendor = 0;
            bool isIdItemINT = int.TryParse(gridVendorSearch.SelectedRow.Cells[1].Text, out idVendor);
               
            string vendorName = gridVendorSearch.SelectedRow.Cells[3].Text;
            string address = gridVendorSearch.SelectedRow.Cells[4].Text;
            string contact = gridVendorSearch.SelectedRow.Cells[5].Text;
            lblVendorID.Text = idVendor.ToString();
            txtVendor.Text = vendorName;
            lblVendorPIC.Text = contact;
            lblVendorAddress.Text = address;
            ScriptManager.RegisterStartupScript(this, GetType(), "set vendor center", "closeDialogVendor();", true);
        }

        protected void gridVendorSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridVendorSearch.PageIndex = e.NewPageIndex;
            loadGridVendor();
        }
        #endregion

        private void reloadPage()
        {
            PO_Object po = (PO_Object)ViewState[curPO];
            if (po.ID > 0)
                Response.Redirect("PurchaseOrder.aspx?ViewPO=" + po.ID.ToString());
            Response.Redirect(Request.RawUrl);
            //ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        protected void btnReloadPage_Click(object sender, EventArgs e)
        {
            reloadPage();
            //ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        protected void btnGetExchangeRate_Click(object sender, EventArgs e)
        {
            if (ddlCurrency.SelectedItem.Value.Equals(""))
                lblExchangeRate.Text = string.Format("{0:#,##0.00}", 0);
            else if (ddlCurrency.SelectedItem.Value.Equals("IDR"))
                lblExchangeRate.Text = string.Format("{0:#,##0.00}", 1);
            else lblExchangeRate.Text = string.Format("{0:#,##0.00}", (GeneralHelper.getExchangeRate(ddlCurrency.SelectedItem.Value)));
        }

        protected void gridItem_PreRender(object sender, EventArgs e)
        {
            for (int i = 0; i < gridItem.Rows.Count; i++)
            {
                var price = (Label)gridItem.Rows[i].FindControl("lblPrice");
                if (price != null)
                    price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text));
            }
        }

        protected void btnGenerateTotalAmount_Click(object sender, EventArgs e)
        {
            List<PO_Item> items = (List<PO_Item>)ViewState[curItem];
            double totalAmount = items.Sum(x => x.QUANTITY * x.PRICE);

            double disc = 0, vat = 0;
            bool isDisc = double.TryParse(txtDiscount.Text.Replace(",", ""), out disc);
            if (ddlVAT.SelectedItem.Text.Contains("Exclude"))
                vat = (totalAmount - disc) * 0.1;
            else vat = 0;
            lblTotalAmount.Text = string.Format("{0:#,##0.00}", (totalAmount - disc + vat));
            lblTotAmountOfItem.Text = string.Format("{0:#,##0.00}", totalAmount);
            ScriptManager.RegisterStartupScript(this, GetType(), "set button again", "setButton();", true);
            
        }

        protected void linkRequestNo_Click(object sender, EventArgs e)
        {
            PO_Object po = (PO_Object)ViewState[curPO];
            if (po.REQUEST_TYPE.Equals("REQ"))
                Response.Redirect("Requisition.aspx?ViewRequisition=" + po.ID_REQUEST);
            else
                Response.Redirect("Quotation.aspx?ViewQuotation=" + po.ID_REQUEST);
        }
    }
}