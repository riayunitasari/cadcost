﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IntegratedPurchasingSystem.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <title>Purchase System</title>
    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu').jqsimplemenu();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png"/>
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
        </div>
            <div class="content-wrapper">
                 <div class="float-left">
                  <ul class="menu">
                            <li><a href='#'>Puchase</a>
                                <ul style="list-style-type:none;">
                                    <li><a href="PurchaseTracking.aspx">Purchase Tracking</a></li>
                                    <li><a href="Purchase.aspx">New Purchase</a></li>
                                </ul>
                            </li>
                            <li><a href='#'>Quotation</a>
                                <ul style="list-style-type:none;">
                                    <li><a href="QuotTracking.aspx">Quotation Tracking</a></li>
                                    <li><a href="Quotation.aspx">New Quotation</a></li>
                                </ul>
                            </li>
                            <li><a href='Item.aspx'>Item</a></li>
                            <li><a href='Vendor.aspx'>Vendor</a></li>
                            <li><a href='Setting.aspx'>Setting</a></li>
                            <li><a href='http://portal/'>JIND Portal</a></li>
                   </ul>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello, <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                
            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
