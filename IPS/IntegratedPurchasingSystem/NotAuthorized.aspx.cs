﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem
{
    public partial class NotAuthorized : System.Web.UI.Page
    {
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
            if (!IsPostBack)
            {
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblLogin.Text = logInUser.FULL_NAME;
                lblUsername.Text = logInUser.FULL_NAME;
            }
        }
    }
}