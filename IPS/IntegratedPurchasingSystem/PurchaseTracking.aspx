﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseTracking.aspx.cs" Inherits="IntegratedPurchasingSystem.PurchaseTracking1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/PageScript/purchaseTrackingPage.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="imgLoading">
            <img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Purchase Tracking
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormField">Requested Month</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Requested Year</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlYear" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">Request Type
                                </td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlRequestType" runat="server">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Requisition" Value="REQ"></asp:ListItem>
                                        <asp:ListItem Text="Requisition Summary" Value="SUM"></asp:ListItem>
                                        <asp:ListItem Text="Quotation" Value="QUO"></asp:ListItem>
                                        <asp:ListItem Text="Advance" Value="AD"></asp:ListItem>
                                        <asp:ListItem Text="Purchase Order" Value="PO"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Progress</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlProgress" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Done Request" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">Last Update On
                                </td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlLastUpdateOn" runat="server">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Requisition" Value="REQ"></asp:ListItem>
                                        <asp:ListItem Text="Requisition Summary" Value="SUM"></asp:ListItem>
                                        <asp:ListItem Text="Quotation" Value="QUO"></asp:ListItem>
                                        <asp:ListItem Text="Advance" Value="AD"></asp:ListItem>
                                        <asp:ListItem Text="Purchase Order" Value="PO"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Activity</td>
                                <td class="contentFormData">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlFilterStatus" runat="server">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnInitiateActivity" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnFilter" runat="server" Text="Search" OnClick="btnFilter_Click" />
                        </div>
                        <br />
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Result
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail" style="padding: 20px;">
                        <asp:UpdatePanel ID="uptPanReservation" runat="server">
                            <ContentTemplate>
                                <asp:GridView AllowPaging="True" PageSize="10" ShowHeaderWhenEmpty="true" CssClass="mGrid"
                                    ID="gridPurchase" EmptyDataText="No records!" runat="server" AutoGenerateColumns="false"
                                    OnPageIndexChanging="gridPurchase_PageIndexChanging" OnRowCommand="gridPurchase_RowCommand"
                                    OnRowDataBound="gridPurchase_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dept Requisition" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnDeptReqNo" Text='<%# Eval("REQ_NO") %>' runat="server"
                                                    CommandName="ViewRequisition" title="View Requisition" CommandArgument='<%# Eval("ID_REQ") %> '></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="GA Requisition" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnGAReqNo" Text='<%# Eval("GA_REQ_NO") %>' runat="server"
                                                    CommandName="ViewRequisition" title="View Requisition" CommandArgument='<%# Eval("ID_REQ") %> '></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Requisition Summary" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnReqSum" Text='<%# ((int)Eval("ID_REQ_SUM")) == 0  ? "" : Eval("SUM_REQ_NO") == null ? "Summary No."+Eval("ID_REQ_SUM") : Eval("SUM_REQ_NO") %>' runat="server"
                                                    CommandName="ViewSummary" title="View Requisition Summary" CommandArgument='<%# Eval("ID_REQ_SUM") %> '></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Quotation" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DataList ID="gridQuotation" RepeatDirection="Horizontal" runat="server" OnItemCommand="gridQuotation_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnQuotation" Text='<%# Container.DataItem.ToString().Equals("0") ? "" : "Quotation No." + Container.DataItem.ToString() %>' runat="server"
                                                            CommandName="ViewQuotation" title="View Quotation" CommandArgument='<%# Container.DataItem.ToString() %> '></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Advance" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DataList RepeatDirection="Horizontal" ID="gridAdvance" runat="server" OnItemCommand="gridAdvance_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnAdvance" Text='<%# Container.DataItem.ToString().Equals("0") ? "" : "Advance No."+ Container.DataItem.ToString() %>' runat="server"
                                                            CommandName="ViewAdvance" title="View Advance Payment" CommandArgument='<%# Container.DataItem.ToString() %> '></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Purchase Order" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DataList RepeatDirection="Horizontal" ID="gridPO" runat="server" OnItemCommand="gridPO_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnPO" Text='<%# Container.DataItem.ToString().Equals("0") ? "" : "PO No."+ Container.DataItem.ToString() %>' runat="server"
                                                            CommandName="ViewPO" title="View Purchase Order" CommandArgument='<%# Container.DataItem.ToString() %> '></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkSeeDetail" runat="server" CommandName="ViewDetail" title="View Detail" CommandArgument='<%# Eval("ID_REQ") %> ' OnClientClick="ShowProgress();">See Detail</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />

                    </div>

                </div>

            </div>
        </div>

        <div style="display: none">
            <div id="dialogDetail">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView CssClass="mGrid"
                            ID="gridDetail" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False" OnPreRender="gridDetail_PreRender"
                            ShowHeaderWhenEmpty="true" OnRowCreated="gridDetail_RowCreated" OnRowDataBound="gridDetail_RowDataBound" OnRowCommand="gridDetail_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dept. No." ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDeptReqNo" Text='<%# Eval("REQ_NO") %>' runat="server"
                                            CommandName="ViewRequisition" title="View Requisition" CommandArgument='<%# Eval("ID_REQ") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="GA No." ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnGAReqNo" Text='<%# Eval("GA_REQ_NO") %>' runat="server"
                                            CommandName="ViewRequisition" title="View Requisition" CommandArgument='<%# Eval("ID_REQ") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Activity" ItemStyle-HorizontalAlign="Center" DataField="REQ_ACTIVITY_TEXT" />
                                <asp:BoundField HeaderText="Item" ItemStyle-HorizontalAlign="Center" DataField="ITEM_NAME" />

                                <asp:TemplateField HeaderText="Requisition Summary" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnReqSum" Text='<%# ((int)Eval("ID_REQ_SUM")) == 0  ? "" : Eval("SUM_REQ_NO") == null ? "Summary No."+Eval("ID_REQ_SUM") : Eval("SUM_REQ_NO") %>' runat="server"
                                            CommandName="ViewSummary" title="View Requisition Summary" CommandArgument='<%# Eval("ID_REQ_SUM") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnQuotation" Text='<%# ((int)Eval("ID_QUO") == 0) ? "" : "Quotation No." + Eval("ID_QUO")  %>' runat="server"
                                            CommandName="ViewRequisition" title="View Requisition" CommandArgument='<%# Eval("ID_QUO") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Activity" ItemStyle-HorizontalAlign="Center" DataField="QUO_ACTIVITY_TEXT" />

                                <asp:TemplateField HeaderText="Advance" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnAdvance" Text='<%# (int)(Eval("ID_ADVANCE")) == 0 ? "" : "Advance No." + Eval("ID_ADVANCE") %>' runat="server"
                                            CommandName="ViewAdvance" title="View Advance" CommandArgument='<%# Eval("ID_ADVANCE") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnPO" Text='<%#  (int)(Eval("ID_PO")) == 0 ? "" : "PO No." + Eval("ID_PO") %>' runat="server"
                                            CommandName="ViewPO" title="View Purchase Order" CommandArgument='<%# Eval("ID_PO") %> '></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Activity" ItemStyle-HorizontalAlign="Center" DataField="PO_ACTIVITY_TEXT" />

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gridPurchase" EventName="RowCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="divError">
                <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
            </div>

            <asp:Button ID="btnInitiateActivity" runat="server" Text="Button" OnClick="btnInitiateActivity_Click" />

        </div>
    </form>
</body>
</html>
