﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoTracking.aspx.cs" Inherits="IntegratedPurchasingSystem.QuotTracking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/moment.js"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/PageScript/reqTrackingPage.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Quotation Tracking
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm">
                            <tr>
                                <td class="contentFormField">Requested Month</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlMonth" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="contentFormField">Requested Year</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlYear" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField">Requestor
                                </td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtRequestor" runat="server"></asp:TextBox>
                                </td>
                                <td class="contentFormField">Status</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlFilterStatus" runat="server">
                                        <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="PREPARE QUOTATION" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="FIX REQUEST" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY DEPT. MANAGER" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY PROJECT MANAGER" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="CHECKED BY COD" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY DIV. MANAGER" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="APPROVAL BY MANAGEMENT" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="GENERATE QUOTATION" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="DONE REQUEST" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="CANCELED REQUEST" Value="9"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtJobCode" runat="server"></asp:TextBox>
                                   <asp:Button ID="btnJobCode" runat="server" Text="..." />
                                   <asp:Button ID="btnResetJobCode" runat="server" Text="Reset" />
                                </td>
                                <td class="contentFormField">Requisition No.</td>
                                <td class="contentFormData">
                                   <asp:TextBox ID="txtReqNo" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="contentFormField">&nbsp;</td>
                                <td class="contentFormData">
                                    <asp:Panel ID="pnlNonEmployee" runat="server">
                                        <asp:TextBox ID="txtRequestedBy" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnRequesteBy" runat="server" Text="..." />
                                        <asp:Button ID="btnResetRequestedBy" runat="server" Text="Reset" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmployee" runat="server">
                                        <asp:Label ID="lblRequestedBy" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </td>
                                <td class="contentFormField">Department</td>
                                <td class="contentFormData">
                                    <asp:Panel ID="pnlNonEmployeeDept" runat="server">
                                        <asp:DropDownList ID="ddlDept" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmployeeDept" runat="server">
                                        <asp:Label ID="lblDept" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </td>

                            </tr>--%>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnFilter" runat="server" Text="Search" OnClick="btnFilter_Click" />
                        </div>
                        <br />
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Result
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail" style="padding: 20px;">
                        <asp:UpdatePanel ID="uptPanReservation" runat="server">
                            <ContentTemplate>
                                <asp:GridView AllowPaging="True" PageSize="10" ShowHeaderWhenEmpty="true" CssClass="mGrid"
                                    ID="gridQuotation" EmptyDataText="No records!" runat="server" AutoGenerateColumns="false" 
                                    OnPageIndexChanging="gridQuotation_PageIndexChanging" OnRowCommand="gridQuotation_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Request No" DataField="REQ_NO" />
                                        <asp:BoundField DataField="DATE_CREATED" HeaderText="Date" />
                                        <asp:BoundField DataField="REQUESTOR" HeaderText="Requestor" />
                                        <asp:BoundField DataField="REQUEST_ITEM" HeaderText="Request Item" />
                                        <asp:BoundField DataField="JOBCODE" HeaderText="JobCode" />
                                        <asp:BoundField DataField="RECOMM_VENDOR_NAME" HeaderText="Recommended Vendor" />
                                        <asp:BoundField DataField="ACTIVITY_TEXT" HeaderText="Activity" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                                <div style="min-width: 150px; text-align:center;">
                                                    <div class="generalAction" style="display:inline-block;">
                                                        <asp:LinkButton ID="btnView" Text="View" runat="server" CommandName="View" title="View Detail"
                                                            CommandArgument='<%#Eval("ID_QUO") %> '></asp:LinkButton>
                                                        <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Delete Quo" title="Delete"
                                                            CommandArgument='<%#Eval("ID_QUO") %>' OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                   
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnFilter" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div style="display: none">

                    <div id="dialogJobCode">
                        <table class="contentForm">
                            <tr>
                                <td style="min-width: 80px;">Search By</td>
                                <td>
                                    <asp:DropDownList ID="ddlJobCode" runat="server">
                                        <asp:ListItem Text="JobCode"></asp:ListItem>
                                        <asp:ListItem Text="Project Name"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchJobCode" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearchJobCode" runat="server" Text="Search" OnClick="btnSearchJobCode_Click" /></td>
                            </tr>
                        </table>
                        <br />
                        <div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10" OnPageIndexChanging="gridJobCode_PageIndexChanging" CellPadding="2"
                                            ID="gridJobCode" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridJobCode_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField runat="server" HeaderText="JobCode" DataField="JOBCODE_ID" />
                                                <asp:BoundField runat="server" HeaderText="Project Name" DataField="PROJECT_NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchJobCode" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
