﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem
{
    public partial class Setting : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "curUser_cnst";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Helper.GeneralHelper.isAdmin(Request.LogonUserIdentity.Name))
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                lblLogIn.Text = logInUser.FULL_NAME;
            }
            else
                Response.Redirect("NotAuthorized.aspx");

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Helper.PFNATTHelper.isEMPExist(txtChangeUser.Text))
                {
                    Helper.GeneralHelper.insertImpersonate(Request.LogonUserIdentity.Name, txtChangeUser.Text);
                    Response.Redirect(Request.RawUrl, true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "vError", "showErrorMsg('Error!','Can not retrieve employee data! Please check if employee data exist')", true);
                }
            }
            catch (Exception vEr)
            {
                string er = HttpUtility.HtmlEncode(vEr.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "vError", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        protected void lnkRelease_Click(object sender, EventArgs e)
        {
            Helper.GeneralHelper.releaseImpersonate(Request.LogonUserIdentity.Name);
            Response.Redirect(Request.RawUrl, true);
        }
    }
}