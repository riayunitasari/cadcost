﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPDF.aspx.cs" Inherits="IntegratedPurchasingSystem.ViewPDF" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase System</title>

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <script type="text/javascript">
        function showErrorMsg(titles, error) {
            $("#lblErrorMsg").text(error);
            $("#divError").dialog({
                title: titles,
                modal: true,
                resizable: false,
                width: 'auto',
                position: ['center'],
                create: function (event, ui) { },
                buttons: {
                    OK: function () {
                        $(this).dialog("close");
                        window.close();
                        return false;
                    }
                },
            });
            $("#divError").parent().appendTo($("form:first"));
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="display: none">
                <div id="divError">
                    <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
