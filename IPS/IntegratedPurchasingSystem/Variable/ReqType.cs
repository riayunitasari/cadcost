﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Variable
{
    public class ReqType
    {
        public static string getType(string type)
        {
            if (type.Equals("ST"))
                return "Stationary Requisition";
            else if (type.Equals("AD"))
                return "Advance";
            else if (type.Equals("RS"))
                return "General Requisition";
            else if (type.Equals("SUM"))
                return "Summary";

            return string.Empty;
        }

        //public static string Summary
        //{ get { return "SUM"; } }

        public static string SoftwareRequisition
        { get { return "SF"; } }

        public static string SummaryStationary
        { get { return "SST"; } }

        public static string SummaryOthers
        { get { return "SOT"; } }

        public static string Stationary
        { get { return "ST"; } }

        public static string Advance
        { get { return "AD"; } }

        public static string GeneralRequisition
        { get { return "RS"; } }

        public static string RequisitionRequest
        { get { return "REQ"; } }

        public static string QuotationRequest
        { get { return "QUO"; } }
    }
}