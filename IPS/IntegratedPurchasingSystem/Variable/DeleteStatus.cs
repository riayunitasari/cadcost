﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Variable
{
    public class DeleteStatus
    {
        public static int Deleted
        { get { return 1; } }

        public static int NotDeleted
        { get { return 0; } }
    }
}