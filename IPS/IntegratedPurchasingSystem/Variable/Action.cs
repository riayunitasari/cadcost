﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Variable
{
    public class Action
    {
        public static int SubmitRequest
        { get { return 1; } }

        public static int CancelRequest
        { get { return 2; } }

        public static int SaveChange
        { get { return 3; } }

        public static int Approve
        { get { return 4; } }

        public static int ReturnToInitiator
        { get { return 5; } }

        public static int GenerateQuotation
        { get { return 6; } }

        public static int GenerateGaNo
        { get { return 7; } }

        public static int ProceedToNextApp
        { get { return 11; } } 
    }
}