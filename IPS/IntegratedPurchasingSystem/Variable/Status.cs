﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegratedPurchasingSystem.Variable
{
    public class Status
    {
        public static int PrepareRequest
        { get { return 1; } }

        public static int FixRequest
        { get { return 2; } }

        public static int ApprovalByDeptMgr
        { get { return 3; } }

        public static int ApprovalByPM
        { get { return 4; } }

        public static int ApprovalByDivMgr
        { get { return 5; } }

        public static int ApprovalByManagement
        { get { return 6; } }

        public static int ApprovalByGADept
        { get { return 7; } }

        public static int DoneRequest
        { get { return 8; } }

        public static int CanceledRequest
        { get { return 9; } }

        public static int PrepareQuotation
        { get { return 10; } }

        public static int GenerateRequest
        { get { return 11; } }

        public static int WaitingFordelivered
        { get { return 12; } }

        public static int OnDeliveryProgress
        { get { return 13; } }

        public static int CheckedByCOD
        { get { return 14; } }

        public static int GenerateQuotation
        { get { return 15; } }

        public static int PreparePO
        { get { return 16; } }

        public static int POSubmittedToSOFI
        { get { return 17; } }
    }
}