﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;

namespace IntegratedPurchasingSystem
{
    public partial class POTracking : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            if (!IsPostBack)
            {
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                POHelper.setDDLYearForPO(ddlYear);
                if (!logInUser.ISADMIN )
                    Response.Redirect("NotAuthorized.aspx");
                loadPurchaseOrder();
            }
        }


        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            Helper.GeneralHelper.gridRender(gridItemSearch, this);
            Helper.GeneralHelper.gridRender(gridVendorSearch, this);
            base.Render(writer);
        }

        private void loadPurchaseOrder()
        {
            try
            {   
                int idVendor = 0, idItem = 0;
                bool isVendorSelected = false, isItemSelected = false;
                if(txtVendor.Text.Length > 0)
                    isVendorSelected = int.TryParse(txtVendorID.Text, out idVendor);
                if(txtItem.Text.Length > 0)
                    isItemSelected = int.TryParse(txtItemID.Text, out idItem);

                gridPO.DataSource = Helper.POHelper.getFilteredPO(int.Parse(ddlMonth.SelectedItem.Value),
                    int.Parse(ddlYear.SelectedItem.Text), txtOrderNo.Text, int.Parse(ddlStatus.SelectedItem.Value), int.Parse(ddlActivityStatus.SelectedItem.Value),
                    txtJobCode.Text, idVendor, idItem);
                gridPO.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "setButton", "setButton();", true);
            }
            catch (Exception erReq)
            {
                string er = HttpUtility.HtmlEncode(erReq.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showException('Error!','" + er + "')", true);
            }
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;

            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            txtJobCode.Text = jobcode;
            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "');", true);
        }

        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            try
            {
                gridJobCode.DataSource = Helper.PFNATTHelper.getActiveJobCode(ddlJobCode.SelectedItem.Text, search);
                gridJobCode.DataBind();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "jobcode Error", "showException('Error!','" + er + "')", true);
            }
        }

        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            try
            {
                loadSearchResultJobCode();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchJobCode jobcode Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchJobCode hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchJobCode set to center", "setCenter('dialogJobCode');", true);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                loadPurchaseOrder();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnFilter jobcode Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnFilter hide progress", "closeLoading();", true);
            }
        }

        protected void gridPO_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridPO.PageIndex = e.NewPageIndex;
            loadPurchaseOrder();
        }

        protected void gridPO_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();

            if (e.CommandName.Equals("View"))
                Response.Redirect("PurchaseOrder.aspx?ViewPO=" + id);
            else if (e.CommandName.Equals("Delete PO"))
            {
                Helper.POHelper.deletePO(int.Parse(id));
                loadPurchaseOrder();
            }
        }

        private void loadSearchResultItem()
        {
            string search = "";
            if (ddlItem.SelectedItem.Text.Equals("Type"))
                search = ddlItemType.SelectedItem.Text;
            else
                search = txtSearchItem.Text;

                gridItemSearch.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
                gridItemSearch.DataBind();
            
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            try
            {
                loadSearchResultItem();
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem set Center dialog item", "setCenter('dialogItem');", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchItem show grid item search", "showItem('#gridItemSearch');", true);
            }
        }

        protected void gridItemSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemSearch.PageIndex = e.NewPageIndex;
            loadSearchResultItem();
        }

        protected void gridItemSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idItem = 0, typeID = 0;
            bool isIdItemINT = int.TryParse(gridItemSearch.SelectedRow.Cells[1].Text, out idItem);
            string itemCode = gridItemSearch.SelectedRow.Cells[2].Text;
            string itemName = gridItemSearch.SelectedRow.Cells[3].Text;
            bool isTypeIDINT = int.TryParse(gridItemSearch.SelectedRow.Cells[4].Text, out typeID);

            if (isIdItemINT && isTypeIDINT)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogItem",
                    "closeDialogItem('" + idItem.ToString() + "','" + itemName.ToString() + "');", true);
            }
        }

        private void loadGridVendor()
        {
            gridVendorSearch.DataSource = VendorHelper.getVendor(ddlVendor.SelectedItem.Text, txtSearchVendor.Text);
            gridVendorSearch.DataBind();
            
        }

        protected void btnSearchVendor_Click(object sender, EventArgs e)
        {
            try
            {
                loadGridVendor();
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchVendor Error", "showException('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchVendor hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSearchVendor set vendor center", "setCenter('dialogVendor');", true);
            }
        }

        protected void gridVendorSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
           int idVendor = 0;
           string vendorName = gridVendorSearch.SelectedRow.Cells[3].Text;
           idVendor = int.Parse(gridVendorSearch.SelectedRow.Cells[1].Text);
           ScriptManager.RegisterStartupScript(this, GetType(), "set vendor center", "closeDialogVendor('" + idVendor + "', '" + vendorName + "');", true);
        }

        protected void gridVendorSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridVendorSearch.PageIndex = e.NewPageIndex;
            loadGridVendor();
        }
    }
}