﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;

namespace IntegratedPurchasingSystem
{
    public partial class PurchaseTracking1 : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        protected void Page_Load(object sender, EventArgs e)
        {
            logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);

            if (!IsPostBack)
            {
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                PurchaseHelper.setDDLYearForPurchase(ddlYear);
                PurchaseHelper.generateDDLStatus(ddlFilterStatus, ddlLastUpdateOn.SelectedItem.Value);
                loadPurchaseMonitoring();
            }
        }

        private void loadPurchaseMonitoring()
        {
            int month = int.Parse(ddlMonth.SelectedItem.Value);
            int year = int.Parse(ddlYear.SelectedItem.Value);
            int activity = int.Parse(ddlFilterStatus.SelectedItem.Value);
            int progress = int.Parse(ddlProgress.SelectedItem.Value);
            gridPurchase.DataSource = Helper.PurchaseHelper.getFilteredPurchase(month, year, ddlLastUpdateOn.SelectedItem.Value, activity, ddlRequestType.SelectedItem.Value, progress);
            gridPurchase.DataBind();
        }


        protected void gridPurchase_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridPurchase.PageIndex = e.NewPageIndex;
            loadPurchaseMonitoring();
        }

        protected void gridPurchase_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string id = e.CommandArgument.ToString();

                if (e.CommandName.Equals("ViewDetail"))
                {
                    gridDetail.DataSource = PurchaseHelper.getDetailPurchaseDetail(int.Parse(id));
                    gridDetail.DataBind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "open detail", "openDialogDetail();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "close loading", "closeLoading();", true);
                }
                else if (e.CommandName.Equals("ViewRequisition"))
                {
                    Response.Redirect("Requisition.aspx?ViewRequisition=" + id);
                }
                else if (e.CommandName.Equals("ViewSummary"))
                    Response.Redirect("ReqSummary.aspx?ViewSummary=" + id);


            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "seeDetailError", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
        }

        protected void gridPurchase_PreRender(object sender, EventArgs e)
        {

        }

        protected void gridPurchase_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Purchase purchase = (Purchase)e.Row.DataItem;
                DataList gridQuo = ((DataList)e.Row.FindControl("gridQuotation"));
                List<int> coba = new List<int>() { 1, 2, 3, 4 };
                gridQuo.DataSource = purchase.ID_QUO;
                gridQuo.DataBind();

                DataList gridAdv = ((DataList)e.Row.FindControl("gridAdvance"));
                gridAdv.DataSource = purchase.ID_ADVANCE;
                gridAdv.DataBind();

                DataList gridPO = ((DataList)e.Row.FindControl("gridPO"));
                gridPO.DataSource = purchase.ID_PO;
                gridPO.DataBind();


            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                loadPurchaseMonitoring();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "error", "showException('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), sender.ToString() + "hide progress", "closeLoading();", true);
            }
        }

        protected void gridDetail_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView detail = (GridView)sender;
                GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "No.";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.RowSpan = 2;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Requisition";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.ColumnSpan = 4;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Requisition Summary";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.RowSpan = 2;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Quotation";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Advance";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.RowSpan = 2;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Purchase Order";
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.CssClass = "headerStyle";
                HeaderRow.Cells.Add(HeaderCell);

                detail.Controls[0].Controls.AddAt(0, HeaderRow);
            }
        }

        protected void gridDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[8].Visible = false;
            }

        }

        protected void gridPO_ItemCommand(object source, DataListCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName.Equals("ViewPO"))
                Response.Redirect("PurchaseOrder.aspx?ViewPO=" + id);
        }

        protected void gridAdvance_ItemCommand(object source, DataListCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName.Equals("ViewAdvance"))
                Response.Redirect("http://jindsrv0202/RFAP/");
        }

        protected void gridQuotation_ItemCommand(object source, DataListCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();

            if (e.CommandName.Equals("ViewQuotation"))
                Response.Redirect("Quotation.aspx?ViewQuotation=" + id);

        }

        protected void gridDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string id = e.CommandArgument.ToString();

                if (e.CommandName.Equals("ViewRequisition"))
                    Response.Redirect("Requisition.aspx?ViewRequisition=" + id);
                else if (e.CommandName.Equals("ViewSummary"))
                    Response.Redirect("ReqSummary.aspx?ViewSummary=" + id);
                else if (e.CommandName.Equals("ViewAdvance"))
                    Response.Redirect("http://jindsrv0202/RFAP/");
                else if (e.CommandName.Equals("ViewQuotation"))
                    Response.Redirect("Quotation.aspx?ViewQuotation=" + id);
                else if (e.CommandName.Equals("ViewPO"))
                    Response.Redirect("PurchaseOrder.aspx?ViewPO=" + id);
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "gridDetail seeDetailError", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
        }

        protected void gridDetail_PreRender(object sender, EventArgs e)
        {
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 1, "btnDeptReqNo");
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 2, "btnGAReqNo");
            GeneralHelper.mergeColumn(gridDetail, 3);
            GeneralHelper.mergeColumn(gridDetail, 4);
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 5, "btnReqSum");
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 6, "btnQuotation");
            GeneralHelper.mergeColumn(gridDetail, 7);
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 8, "btnAdvance");
            GeneralHelper.mergeLinkButtonColumn(gridDetail, 9, "btnPO");
            GeneralHelper.mergeColumn(gridDetail, 10);
        }

        protected void btnInitiateActivity_Click(object sender, EventArgs e)
        {
            try
            {
                PurchaseHelper.generateDDLStatus(ddlFilterStatus, ddlLastUpdateOn.SelectedItem.Value);
            }
            catch (Exception exp)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnInitiateActivity seeDetailError", "showException('Error','" + HttpUtility.HtmlEncode(exp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnInitiateActivity hide progress", "closeLoading();", true);
            }
        }
    }
}