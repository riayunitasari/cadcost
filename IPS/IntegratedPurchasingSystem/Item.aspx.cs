﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem
{
    public partial class Item : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        const string curState = "current_State";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                ViewState[curState] = "view";
                //Helper.CategoryHelper.generateDDLCategory(ddlItemCategory);
                //Helper.CategoryHelper.generateDDLCategory(ddlCategory);
                //loadItems();
                //loadCategory();
            }
        }

        //private void loadItems()
        //{
        //    string search = "";
        //    if (ddlItem.SelectedItem.Text.Equals("Name"))
        //        search = txtSearchItem.Text;
        //    else if (ddlItem.SelectedItem.Text.Equals("Category"))
        //        search = ddlItemCategory.SelectedItem.Text;
        //    else
        //        search = ddlItemType.SelectedItem.Text;
        //    try
        //    {
        //        gridItem.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
        //        gridItem.DataBind();
        //    }
        //    catch (Exception erItem)
        //    {
        //        string er = HttpUtility.HtmlEncode(erItem.Message);
        //        ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showMsg('Error!','" + er + "')", true);
        //    }
        //}

        //private void loadCategory()
        //{
        //    gridCategory.DataSource = Helper.CategoryHelper.getAllCatgeory();
        //    gridCategory.DataBind();
        //}

        //protected void btnSearchItem_Click(object sender, EventArgs e)
        //{
        //    loadItems();
        //}

        //protected void btnNewItem_Click(object sender, EventArgs e)
        //{
        //    txtItemID.Text = "";
        //    txtItemName.Text = "";
        //    ddlType.ClearSelection();
        //    ddlCategory.ClearSelection();
        //    txtItemDesc.Text = "";
        //    ViewState[curState] = "new";
        //    ScriptManager.RegisterStartupScript(this, GetType(), "create Item", "openDialogItem();", true);
        //}

        //protected void btnSearchCategory_Click(object sender, EventArgs e)
        //{
        //    gridCategory.DataSource = Helper.CategoryHelper.getFilteredCategory(txtSearchCategory.Text);
        //    gridCategory.DataBind();
        //}

        //protected void gridItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gridItem.PageIndex = e.NewPageIndex;
        //    loadItems();
        //}

        //protected void gridCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gridCategory.PageIndex = e.NewPageIndex;
        //    loadCategory();
        //}

        //protected void btnSaveItem_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ViewState[curState].ToString().Equals("new"))
        //        {
        //            Helper.ItemHelper.insertNewItem(txtItemName.Text, ddlType.SelectedItem.Text, int.Parse(ddlCategory.SelectedItem.Value), txtItemDesc.Text);
        //            loadItems();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "new item Success", "showMsg('Success','Succesfully added new Item!');", true);
        //        }
        //        else if (ViewState[curState].ToString().Equals("edit"))
        //        {
        //            Helper.ItemHelper.updateItem(int.Parse(txtItemID.Text), txtItemName.Text, ddlType.SelectedItem.Text, int.Parse(ddlCategory.SelectedItem.Value), txtItemDesc.Text);
        //            loadItems();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "edit item success", "showMsg('Success','Item record had been updated!')", true);
        //        }
        //    }
        //    catch (Exception sExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showMsg('Error','" + HttpUtility.HtmlEncode(sExp.Message) + "')", true);
        //    }
        //}

        //protected void gridItem_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        int id = int.Parse(e.CommandArgument.ToString());
        //        if (e.CommandName.Equals("Delete Item"))
        //        {
        //            Helper.ItemHelper.deleteItem(id);
        //            loadItems();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "delete", "showMsg('Success', 'Selected item record had been deleted');", true);
        //        }
        //        else
        //        {
        //            ViewState[curState] = "edit";
        //            ITEM item = Helper.ItemHelper.getItemByID(id);
        //            ScriptManager.RegisterStartupScript(this, GetType(), "setEdit", "openEditItemDialog('"+id.ToString()+"', '"+item.ITEM_NAME+"', '"+item.ID_CATEGORY.ToString()+"', '"+item.TYPE+"', '"+item.DESCRIPTION+"')", true);
        //        }
        //    }
        //    catch (Exception acExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "actionError", "showMsg('Error','" + HttpUtility.HtmlEncode(acExp.Message) + "')", true);
        //    }
        //}

        //protected void btnSaveCategory_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ViewState[curState].ToString().Equals("new"))
        //        {
        //            Helper.CategoryHelper.insertCategory(txtCategoryName.Text, txtCategoryDesc.Text);
        //            loadCategory();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "new category Success", "showMsg('Success','Succesfully added new Category!');", true);
        //        }
        //        else if (ViewState[curState].ToString().Equals("edit"))
        //        {
        //            Helper.CategoryHelper.updateCategory(int.Parse(txtCategoryID.Text), txtCategoryName.Text, txtCategoryDesc.Text);
        //            loadCategory();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "edit category success", "showMsg('Success','Category record had been updated!')", true);
        //        }
        //    }
        //    catch (Exception sExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showMsg('Error','" + HttpUtility.HtmlEncode(sExp.Message) + "')", true);
        //    }
        //}

        //protected void btnNewCategory_Click(object sender, EventArgs e)
        //{
        //    txtCategoryID.Text = "";
        //    txtCategoryName.Text = "";
        //    txtCategoryDesc.Text = "";
        //    ViewState[curState] = "new";
        //    ScriptManager.RegisterStartupScript(this, GetType(), "create category", "openDialogCategory();", true);
        //}

        //protected void gridCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        int id = int.Parse(e.CommandArgument.ToString());
        //        if (e.CommandName.Equals("Delete Category"))
        //        {
        //            Helper.CategoryHelper.deleteCategory(id);
        //            loadCategory();
        //            ScriptManager.RegisterStartupScript(this, GetType(), "delete cat", "showMsg('Success', 'Selected category record had been deleted');", true);
        //        }
        //        else
        //        {
        //            ViewState[curState] = "edit";
        //            CATEGORY_ITEM cat = Helper.CategoryHelper.getCategoryByID(id);
        //            ScriptManager.RegisterStartupScript(this, GetType(), "setEdit", "openEditCategoryDialog('" + id.ToString() + "', '" + cat.CATEGORY_ITEM1 + "', '"+cat.DESCRIPTION +"')", true);
        //        }
        //    }
        //    catch (Exception acExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "actionError", "showMsg('Error','" + HttpUtility.HtmlEncode(acExp.Message) + "')", true);
        //    }
        //}

    }
}