﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Entities;
using IntegratedPurchasingSystem.Helper;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Threading;
using System.Net;
using System.IO;
using System.Web.Hosting;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Drawing;

namespace IntegratedPurchasingSystem
{
    public partial class Requisition : System.Web.UI.Page
    {
        const string curItem = "list_Item_cnst";
        const string curUser = "current_Log_In_User";
        const string curState = "current_Req_State";
        const string curReq = "current_Requisition";
        const string GANoState = "is_GA_REQ_NO_Generated";
        const string quotID = "quotation_ID_Of_curReq";
        const string dlFile = "downloaded_File";
        const string flName = "downloaded_FileName";
        Employee logInUser = new Employee();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                int idReq = 0;
                Helper.GeneralHelper.generateDDLCurrency(ddlCurrency);
                Helper.PartTypeHelper.generateDDLType(ddlItemType);

                reqReport.Visible = false;
                reqReportLabel.Visible = false;
                ViewState[quotID] = 0;


                gridItem.Columns[10].Visible = false;
                btnAdvance.Visible = false;
                submitAdvance.Visible = false;
                
                if (int.TryParse(Request["ViewRequisition"], out idReq))
                {
                    ViewState[curState] = "view";
                    GAReqNo.Visible = true;
                    RequisitionObject req = Helper.RequisitionHelper.getRequisitionByID(idReq);
                    logInUser = GeneralHelper.isParticipant(logInUser, 2, req.ID_REQ, "REQ");
                    if ((!logInUser.ISPARTICIPANT && !logInUser.ISADMIN) || req.ISDELETED == 1)
                    {
                        Response.Redirect("NotAuthorized.aspx");
                    }

                    ViewState[curReq] = req;
                    List<RequisitionItems> items = Helper.RequisitionHelper.getRequisitionDetail(req.ID_REQ);
                    ViewState[curItem] = items;
                    ViewState[GANoState] = "false";
                    onViewState();
                    Helper.GeneralHelper.generateDDLAction(ddlAction, 2, req.ACTIVITY_ID);
                    if ((req.ACTIVITY_ID == Variable.Status.FixRequest || req.ACTIVITY_ID == Variable.Status.GenerateRequest)
                        && logInUser.IS_CUR_PARTICIPANT)
                    {
                        ViewState[curState] = "edit";
                        onEditState();
                    }
                    if (req.ACTIVITY_ID > Variable.Status.PrepareRequest)
                    {
                        reqReport.Visible = true;
                        reqReportLabel.Visible = true;
                        if (req.ACTIVITY_ID == Variable.Status.WaitingFordelivered || req.ACTIVITY_ID == Variable.Status.OnDeliveryProgress)
                            if (items.Any(x => x.ID_QUO == 0))
                            {
                                btnAdvance.Visible = true;
                                if (items.Count == items.Count(x => x.ID_ADVANCE != 0))
                                    btnAdvance.Visible = false;
                                if (req.ID_PO > 0)
                                    btnAdvance.Visible = false;
                            }
                    }
                    if (req.ID_PO > 0)
                    {
                        linkPO.Text = "Porchase Order No. " + req.ID_PO;
                    }
                    else
                        divPO.Visible = false;

                    setPrint();
                    int idQuot = QuotHelper.getQuotationIDOfReq(idReq);
                    //setQuotLink(idQuot, logInUser.ISADMIN);
                    ViewState[quotID] = idQuot;
                    floatRendering();
                }
                else
                {
                    ViewState[curState] = "new";
                    ViewState[curReq] = new RequisitionObject();
                    ViewState[curItem] = new List<RequisitionItems>();
                    onEditState();
                    GAReqNo.Visible = false;
                    lblDepartment.Text = logInUser.SECT_NAME;
                    lblReqBy.Text = logInUser.EMP_ID + " - " + logInUser.FULL_NAME;
                    lblStatus.Text = "PREPARE REQUEST";
                    lblDateCreated.Text = DateTime.Now.ToString("dd MMM yyyy");
                    actionHistory.Visible = false;
                    actionDiv.Visible = true;
                    loadGridViewItem();
                    ViewState[GANoState] = "false";
                    Helper.GeneralHelper.generateDDLAction(ddlAction, 2, Variable.Status.PrepareRequest);
                    setPrint();
                    gridItem.Columns[7].Visible = false;
                    floatRendering();
                    
                    //ScriptManager.RegisterStartupScript(this, GetType(), "setOnEdit", "setOnEdit();", true);
                }

                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnDownload);
            }
        }

        private void floatRendering()
        {
            double floatValue = 0;
            bool isFloat = false;
            isFloat = double.TryParse(lblExchangeRate.Text.Replace(",", ""), out floatValue);
            lblExchangeRate.Text = string.Format("{0:#,##0.00}", floatValue);

            isFloat = double.TryParse(lblTotalEst.Text.Replace(",", ""), out floatValue);

            lblTotalEst.Text = string.Format("{0:#,##0.00}", floatValue);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Helper.GeneralHelper.gridRender(gridItemSearch, this);
            Helper.GeneralHelper.gridRender(gridJobCode, this);
            base.Render(writer);
        }

        private void userPriviledge()
        {
            actionHistory.Visible = true;
            logInUser = (Employee)ViewState[curUser];
            RequisitionObject req = (RequisitionObject)ViewState[curReq];

            actionDiv.Visible = false;
            if (logInUser.IS_CUR_PARTICIPANT)
                if (req.ACTIVITY_ID != Variable.Status.DoneRequest && req.ACTIVITY_ID != Variable.Status.CanceledRequest)
                    actionDiv.Visible = true;
        }

        //private void setQuotLink(int idQuot, bool isAdmin)
        //{   
        //    if(idQuot > 0 && isAdmin)
        //        linkQuot.Visible = true;
        //    else
        //        linkQuot.Visible = false;
        //}

        private void onViewState()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            btnAddItem.Visible = false;
            lblGAReqNo.Text = string.IsNullOrEmpty(req.GA_REQ_NO) ? "None" : req.GA_REQ_NO;
            LblReqNo.Text = req.DEPT_REQ_NO;
            lblDateCreated.Text = req.DATE_CREATED.ToString("dd MMM yyyy");
            lblReqBy.Text = req.REQ_BY + " - " + req.REQ_BY_NAME;
            lblDepartment.Text = req.REQ_BY_DEPT;
            lblJobCode.Text = req.JOBCODE;
            lblProjectName.Text = req.PROJECT_NAME;
            lblReqType.Text = req.REQ_TYPE_TEXT;
            lblTotalEst.Text = string.Format("{0:#,##0.00}", req.TOTAL_EST_AMOUNT);
            lblCurrencyEst.Text = req.CURRENCY_CODE == null || req.CURRENCY_CODE.Equals(string.Empty) ? "" : req.CURRENCY_CODE; ;
            lblStatus.Text = req.ACTIVITY_TEXT;
            lblCurrency.Text = req.CURRENCY_CODE == null || req.CURRENCY_CODE.Equals(string.Empty) ? "none" : req.CURRENCY_CODE;
            lblExchangeRate.Text = req.EXCHANGE_RATE.ToString();
            lblDesc.Text = req.DESCRIPTION == null || req.DESCRIPTION.Equals(string.Empty) ? "none" : req.DESCRIPTION;
            lblReqRemark.Text = req.REMARKS == null || req.REMARKS.Equals(string.Empty) ? "none" : req.REMARKS;
            linkPDF.Text = req.ATTACHMENT;
            setLinkPDF();
            gridItem.ShowFooter = false;
            gridItem.Columns[9].Visible = false;
            loadGridViewItem();
            loadActionHistory(req.ID_REQ);
            userPriviledge();

            txtDesc.Visible = false;
            txtReqRemark.Visible = false;
            txtDeptReqNo.Visible = false;
            ddlReqType.Visible = false;
            divJobcode.Visible = false;
            divCurrency.Visible = false;
            divExchangeRate.Visible = false;
            divAttachment.Visible = false;
            //ScriptManager.RegisterStartupScript(this, GetType(), "setOnView", "setOnView();", true);
        }

        private void setPrint()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            if (req.GA_REQ_NO == null) printReqNo.Visible = false;
            else
            {
                if (!req.GA_REQ_NO.Any(x => char.IsDigit(x))) printReqNo.Visible = false;
                else printReqNo.Visible = true;
            }
        }

        private void onEditState()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];

            btnAddItem.Visible = true;
            txtDeptReqNo.Text = req.DEPT_REQ_NO;
            txtJobCode.Text = req.JOBCODE;
            ddlReqType.SelectedIndex = ddlReqType.Items.IndexOf(ddlReqType.Items.FindByValue(req.REQ_TYPE));
            ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(req.CURRENCY_CODE));
            txtExchangeRate.Text = req.EXCHANGE_RATE.ToString();
            txtDesc.Text = req.DESCRIPTION;
            txtReqRemark.Text = req.REMARKS;
            lblCurrencyEst.Text = req.CURRENCY_CODE;
            lblTotalEst.Text = string.Format("{0:#,##0.00}", req.TOTAL_EST_AMOUNT);
            //lblTotalEst.Text = req.TOTAL_EST_AMOUNT.ToString();
            gridItem.Columns[9].Visible = true;
            loadGridViewItem();
            actionHistory.Visible = false;
            userPriviledge();

            LblReqNo.Visible = false;
            lblReqType.Visible = false;
            lblJobCode.Visible = false;
            lblCurrency.Visible = false;
            lblExchangeRate.Visible = false;
            lblDesc.Visible = false;
            lblReqRemark.Visible = false;


            //ScriptManager.RegisterStartupScript(this, GetType(), "setOnEdit", "setOnEdit();", true);
        }

        private void loadActionHistory(int idReq)
        {
            gridActionHistory.DataSource = Helper.ActionLogHelper.getActionLog(idReq, "REQ");
            gridActionHistory.DataBind();
        }

        #region Item
        private void loadSearchResultItem()
        {
            string search = "";
            if (ddlItem.SelectedItem.Text.Equals("Type"))
                search = ddlItemType.SelectedItem.Text;
            else
                search = txtSearchItem.Text;
            try
            {
                gridItemSearch.DataSource = Helper.ItemHelper.getItems(ddlItem.SelectedItem.Text, search);
                gridItemSearch.DataBind();
            }
            catch (Exception erItem)
            {
                string er = HttpUtility.HtmlEncode(erItem.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showErrorMsg('Error!','" + er + "')", true);
            }
        }

        private void loadGridViewItem()
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            gridItem.DataSource = items;
            gridItem.DataBind();

            double total = items.Sum(x => (x.PRICE * x.QUANTITY));
            lblTotalEst.Text = string.Format("{0:#,##0.00}", total);
            if (ViewState[curState].Equals("new") || ViewState[curState].Equals("edit"))
                lblCurrencyEst.Text = ddlCurrency.SelectedItem.Value;

            if (items.Count == items.Count(x => x.ID_ADVANCE != 0))
            {
                btnAdvance.Visible = false;
                submitAdvance.Visible = false;
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        private bool isItemExist(int item)
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            return items.Any(x => x.PART_ID == item);
        }

        protected void gridItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete Item"))
            {
                int id = int.Parse(e.CommandArgument.ToString());
                List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                RequisitionItems item = items.FirstOrDefault(x => x.PART_ID == id);
                items.Remove(item);
                ViewState[curItem] = items;
                loadGridViewItem();
            }
            else if (e.CommandName.Equals("SeeQuotation"))
            {
                int id = 0;
                if (int.TryParse(e.CommandArgument.ToString(), out id))
                    if (id > 0)
                        Response.Redirect("Quotation.aspx?ViewQuotation=" + id.ToString());

            }
            else if (e.CommandName.Equals("SeeAdvance"))
            {
                int id = 0;
                if (int.TryParse(e.CommandArgument.ToString(), out id))
                    if (id > 0)
                        Response.Redirect("http://jindsrv0202/RFAP/");
            }
        }

        protected void gridItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = gridItem.Rows[e.NewEditIndex];
            Label metric = row.FindControl("lblMetric") as Label;

            Label quantity = row.FindControl("lblQuantity") as Label;

            gridItem.EditIndex = e.NewEditIndex;
            loadGridViewItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "metric load on Edit", "setDDLMetricOnEdit(" + quantity.Text + ", '" + metric.Text + "'," + e.NewEditIndex + ")", true);
        }

        protected void gridItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            double valQuantity = 0, valPrice = 0;
            Label itemName = (Label)gridItem.Rows[e.RowIndex].FindControl("lblItemName");
            Label itemID = (Label)gridItem.Rows[e.RowIndex].FindControl("lblItemID");
            TextBox quantity = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtQuantity");
            DropDownList metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricSingular");
            bool isQNumber = double.TryParse(quantity.Text.Replace(",", ""), out valQuantity);
            TextBox price = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtPrice");
            bool isPNumber = double.TryParse(price.Text.Replace(",", ""), out valPrice);
            TextBox remark = (TextBox)gridItem.Rows[e.RowIndex].FindControl("txtRemarks");

            if (valQuantity > 1)
                metric = (DropDownList)gridItem.Rows[e.RowIndex].FindControl("ddlMetricPlural");

            if (itemName.Text.Equals(string.Empty) || quantity.Text.Equals(string.Empty) || metric.SelectedItem.Text.Contains("-- ") ||
                        !isQNumber || (!price.Text.Equals(string.Empty) && !isPNumber))
            {
                gridItem.EditIndex = -1;
                loadGridViewItem();
                ScriptManager.RegisterStartupScript(this, GetType(), "metricload", "ddlMetricOnNew();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "update item failed", "showErrorMsg('Error!','Invalid input! Please check your input')", true);
            }
            else
            {
                List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                RequisitionItems item = items.FirstOrDefault(x => x.PART_ID == int.Parse(itemID.Text));

                item.QUANTITY = valQuantity;
                item.METRIC = metric.SelectedItem.Text;
                item.PRICE = valPrice;
                item.REMARK = remark.Text;

                ViewState[curItem] = items;
                gridItem.EditIndex = -1;
                loadGridViewItem();
            }
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            loadSearchResultItem();
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set Center dialog item", "setCenter('dialogItem');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "show grid item search", "showItem('#gridItemSearch');", true);
        }

        protected void gridItemSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridItemSearch.PageIndex = e.NewPageIndex;
            loadSearchResultItem();
        }

        protected void gridItemSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idItem = 0, typeID = 0;
            bool isIdItemINT = int.TryParse(gridItemSearch.SelectedRow.Cells[1].Text, out idItem);
            string itemCode = gridItemSearch.SelectedRow.Cells[2].Text;
            string itemName = gridItemSearch.SelectedRow.Cells[3].Text;
            bool isTypeIDINT = int.TryParse(gridItemSearch.SelectedRow.Cells[4].Text, out typeID);
            string typeName = gridItemSearch.SelectedRow.Cells[5].Text;

            if (isIdItemINT && isTypeIDINT)
            {
                //loadGridViewItem();
                txtItemID.Text = idItem.ToString();
                txtItemCode.Text = itemCode;
                txtPartTypeID.Text = typeID.ToString();
                txtPartTypeName.Text = typeName;
                txtNewItem.Text = itemName.ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "closeDialogItem", "closeDialogItem();", true);
            }

        }

        protected void gridItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridItem.EditIndex = -1;
            loadGridViewItem();
        }

        #endregion


        #region JobCode
        protected void btnSearchJobCode_Click(object sender, EventArgs e)
        {
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            loadSearchResultJobCode();
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "set center", "setCenter('dialogJobCode');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "load on New", "ddlMetricOnNew();", true);
        }

        protected void gridJobCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jobcode = gridJobCode.SelectedRow.Cells[1].Text;
            string pName = gridJobCode.SelectedRow.Cells[2].Text.Replace("&quot;", "\"");

            gridJobCode.DataSource = null;
            gridJobCode.DataBind();
            txtJobCode.Text = jobcode;
            lblProjectName.Text = pName;
            ScriptManager.RegisterStartupScript(this, GetType(), "closeJobCode", "closeJobCode('" + jobcode + "', '" + pName + "');", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "load on New", "ddlMetricOnNew();", true);
        }

        protected void gridJobCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridJobCode.PageIndex = e.NewPageIndex;
            loadSearchResultJobCode();
            //gridViewItemOnEmpty();


            ScriptManager.RegisterStartupScript(this, GetType(), "load ddlMetric", "ddlMetricOnNew();", true);
        }

        private void loadSearchResultJobCode()
        {
            string search = txtSearchJobCode.Text;
            try
            {
                gridJobCode.DataSource = Helper.PFNATTHelper.getActiveJobCode(ddlJobCode.SelectedItem.Text, search);
                gridJobCode.DataBind();
            }
            catch (Exception erJob)
            {
                string er = HttpUtility.HtmlEncode(erJob.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "item Error", "showErrorMsg('Error!','" + er + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
            }
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            logInUser = ((Employee)ViewState[curUser]);
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            DateTime modifiedTime = DateTime.Now;
            List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
            string currency = null;
            if (!ddlCurrency.SelectedItem.Text.Contains("None"))
                currency = ddlCurrency.SelectedItem.Value;
            int action = int.Parse(ddlAction.SelectedItem.Value);
            double exchangeRate;
            if (!double.TryParse(txtExchangeRate.Text.Replace(",", ""), out exchangeRate))
                exchangeRate = 0;

            string requester = ((Employee)ViewState[curUser]).EMP_ID;
            double total = items.Sum(x => (x.PRICE * x.QUANTITY));

            string strFilename = "", strMessage = "";
            bool isUploaded = false;
            if (!string.IsNullOrEmpty(fileAttachmentUpload.FileName) && !fileAttachmentUpload.Equals(""))
            {
                if (req.ATTACHMENT != null)
                    if (req.ATTACHMENT.Length > 0 && linkPDF.Text.Length > 0)
                        deletePDF(req.ATTACHMENT);
                strFilename = DateTime.Now.ToString("yyyy-MM-dd") + "_" + txtDeptReqNo.Text.Replace("/", "-") + ".pdf";//fileAttachmentUpload.PostedFile.FileName.ToString();
                if (action == Variable.Action.SubmitRequest)
                    isUploaded = uploadFile(strFilename, out strMessage);
            }
            else
            {
                if (linkPDF.Text.Length > 0 && req.ATTACHMENT != null)
                    if (req.ATTACHMENT.Length > 0)
                        strFilename = req.ATTACHMENT;
                isUploaded = true; //tidak ada file yang diupload
            }

            try
            {
                if (ViewState[curState].ToString().Equals("new"))
                {
                    int id = 0;
                    if (action == Variable.Action.SubmitRequest)
                    {
                        if (isUploaded)
                        {
                            req = new RequisitionObject(txtDeptReqNo.Text, txtJobCode.Text, txtDesc.Text, txtReqRemark.Text,
                                requester, modifiedTime, currency, exchangeRate, ddlReqType.SelectedItem.Value, total, strFilename);
                            req.GA_REQ_NO = lblGAReqNo.Text;
                            Helper.RequisitionHelper.insertRequisition(req, out id);
                            req.ID_REQ = id;
                            Helper.RequisitionHelper.insertDetailItem(items, id);
                            Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, id, Variable.ReqType.RequisitionRequest, Variable.Action.SubmitRequest,
                               Variable.Status.PrepareRequest, txtComment.Text, exchangeRate, total, txtJobCode.Text);
                            loadGridViewItem();
                            ViewState[curReq] = req;
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessNew", "saveSuccess('Your requisition has succesfully recorded and being sent to GAD');", true);
                        }
                        else
                        {
                            onEditState();
                            ScriptManager.RegisterStartupScript(this, GetType(), "can not upload on new", "showErrorMsg('WARNING!', '" + strMessage + "');", true);
                           // ScriptManager.RegisterStartupScript(this, GetType(), "set edit on failed insert", "setOnEdit();", true);
                        }
                    }
                    else
                        Response.Redirect("RequisitionTracking.aspx");
                }
                else if (ViewState[curState].ToString().Equals("edit"))
                {
                    if (action != Variable.Action.CancelRequest)
                    {
                        if (isUploaded)
                        {
                            req.DEPT_REQ_NO = txtDeptReqNo.Text;
                            req.JOBCODE = txtJobCode.Text;
                            req.CURRENCY_CODE = currency;
                            req.EXCHANGE_RATE = exchangeRate;
                            req.DESCRIPTION = txtDesc.Text;
                            req.REMARKS = txtReqRemark.Text;
                            req.TOTAL_EST_AMOUNT = total;
                            req.ATTACHMENT = strFilename;
                            ViewState[curReq] = req;
                            Helper.RequisitionHelper.updateRequisition(req);
                            Helper.RequisitionHelper.updateDetailRequisition(items, req.ID_REQ);
                            Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, req.ID_REQ, req.REQ_TYPE, action, req.ACTIVITY_ID, txtComment.Text,
                            req.EXCHANGE_RATE, req.TOTAL_EST_AMOUNT, req.JOBCODE);

                            onEditState();

                           // ScriptManager.RegisterStartupScript(this, GetType(), "setOnEdit", "setOnEdit();", true);

                            int idQuot = QuotHelper.getQuotationIDOfReq(req.ID_REQ);
                            if (action == Variable.Action.GenerateQuotation && idQuot > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "go to Quotation", "saveSuccess('Your change has been saved successfully!');", true);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessEdit", "saveSuccess('Your change has been saved successfully!');", true);
                        }
                        else
                        {
                            onEditState();
                            ScriptManager.RegisterStartupScript(this, GetType(), "can not upload on edit", "showErrorMsg('WARNING!', '" + strMessage + "');", true);

                        }
                    }
                    else if (action == Variable.Action.CancelRequest)
                    {
                        Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, req.ID_REQ, req.REQ_TYPE, action, req.ACTIVITY_ID, txtComment.Text,
                             req.EXCHANGE_RATE, req.TOTAL_EST_AMOUNT, req.JOBCODE);
                        onViewState();
                        //ScriptManager.RegisterStartupScript(this, GetType(), "set On View on cancel", "setOnView();", true);
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessCancelStatus", "saveSuccess('Request has been canceled!');", true);
                    }

                }
                else if (ViewState[curState].ToString().Equals("view"))
                {
                    logInUser = (Employee)ViewState[curUser];
                    int actionID = int.Parse(ddlAction.SelectedItem.Value);
                    Helper.ActionLogHelper.insertReqHistory(logInUser.EMP_ID, req.ID_REQ, req.REQ_TYPE, actionID, req.ACTIVITY_ID, txtComment.Text,
                        req.EXCHANGE_RATE, req.TOTAL_EST_AMOUNT, req.JOBCODE);
                    onViewState();
                    //ScriptManager.RegisterStartupScript(this, GetType(), "setOnView", "setOnView();", true);
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveSuccessEditStatus", "saveSuccess('Your change has been saved successfully!');", true);
                }

            }
            catch (Exception sExp)
            {
                loadGridViewItem();
                ScriptManager.RegisterStartupScript(this, GetType(), "load on New", "ddlMetricOnNew();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(sExp.Message) + "')", true);
            }
            finally
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
            }

        }

        protected void btnHiddenSave_Click(object sender, EventArgs e)
        {
            reloadPage();
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            int idItem = 0, typeID = 0;
            double quantity = 0, price = 0;
            bool isIdItemINT = int.TryParse(txtItemID.Text, out idItem);
            bool isTypeIDINT = int.TryParse(txtPartTypeID.Text, out typeID);
            bool isQuantityFloat = double.TryParse(txtNewQuantity.Text.Replace(",", ""), out quantity);
            bool isPriceFloat = true;
            string metric = "";
            if (!string.IsNullOrEmpty(txtNewPrice.Text))
                isPriceFloat = double.TryParse(txtNewPrice.Text.Replace(",", ""), out price);

            if (quantity > 1)
                metric = ddlMetricPlural.SelectedItem.Text;
            else
                metric = ddlMetricSingular.SelectedItem.Text;

            if (isIdItemINT && isTypeIDINT && isQuantityFloat && isPriceFloat)
            {
                if (!(isItemExist(idItem)))
                {
                    RequisitionItems item = new RequisitionItems(idItem, txtItemCode.Text, txtNewItem.Text, typeID, txtPartTypeName.Text, quantity, metric, price, txtNewRemark.Text);

                    List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                    items.Add(item);
                    ViewState[curItem] = items;
                    loadGridViewItem();
                    ScriptManager.RegisterStartupScript(this, GetType(), "close dialog detail", "closeDialogAddDetail();", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "insert item failed", "showErrorMsg('Error!','Can not add item! Item already added!')", true);
            }
            else
            {
                List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];

                //if (items.Count <= 0)
                //gridViewItemOnEmpty();
                ScriptManager.RegisterStartupScript(this, GetType(), "metric load", "ddlMetricOnNew();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "insert item failed", "showErrorMsg('Error!','Invalid input! Please check your input')", true);
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        #region File Attachment


        public bool uploadFile(string fileName, out string message)
        {
            if (fileName == "")
            {
                message = "Invalid filename supplied";
                return false;
            }
            if (fileAttachmentUpload.PostedFile.ContentLength == 0)
            {
                message = "Invalid file content";
                return false;
            }
            fileName = System.IO.Path.GetFileName(fileName);
            try
            {

                if (fileAttachmentUpload.PostedFile.ContentLength <= 10485760)
                {
                    string AttachmentDir = Server.MapPath("/IPS/PDF");
                    AttachmentDir = AttachmentDir + "\\" + fileName;
                    fileAttachmentUpload.PostedFile.SaveAs(AttachmentDir);
                    message = "File uploaded successfully";
                    return true;
                }
                else
                {
                    message = "Unable to upload,file exceeds maximum limit";
                    return false;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        private void setLinkPDF()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            if (req.ATTACHMENT != null)
            {
                if (req.ATTACHMENT.Length > 0)
                {
                    string pageURL = "http://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath +
                       "/ViewPDF.aspx?ViewAttachment=" + req.ID_REQ + "&Type=REQ";
                    linkPDF.OnClientClick = String.Format("window.open('{0}','_newtab'); return false;", ResolveUrl(pageURL));
                }
            }
            else
            {
                linkPDF.OnClientClick = "return false;";
                linkPDF.Visible = false;
            }
        }

        private void deletePDF(string fileName)
        {
            string AttachmentDir = Server.MapPath("/IPS/PDF");
            AttachmentDir = AttachmentDir + "\\" + fileName;
            FileInfo file = new FileInfo(AttachmentDir);
            if (file.Exists)
            {
                file.Delete();
            }
        }
        #endregion

        private void reloadPage()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            if (req.ID_REQ > 0)
                Response.Redirect("Requisition.aspx?ViewRequisition=" + req.ID_REQ.ToString());
            Response.Redirect(Request.RawUrl);
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        protected void btnReloadPage_Click(object sender, EventArgs e)
        {
            reloadPage();
            ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
        }

        #region Print Requisition
        private void exportVehicleReportToPDF()
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            try
            {
                int reqNo = 0, price = 0;

                if (rdoReqNo.SelectedItem != null)
                    reqNo = int.Parse(rdoReqNo.SelectedItem.Value);
                if (rdoPrice.SelectedItem != null)
                    price = int.Parse(rdoPrice.SelectedItem.Value);

                if (req.GA_REQ_NO != null)
                    if (!req.GA_REQ_NO.Any(x => char.IsDigit(x)) || req.GA_REQ_NO.Contains("None"))
                        reqNo = 0;

                ReportDocument report = new ReportDocument();
                Stream Stream;
                var connectionInfo = new System.Data.SqlClient.SqlConnectionStringBuilder(Connection.PurchaseSysConn);
                string Template = "";
                if (price == 0)
                    Template = Server.MapPath("~/Report/RequisitionNoPrice.rpt");
                else if (price == 1)
                    Template = Server.MapPath("~/Report/RequisitionPrice.rpt");
                report.Load(Template);
                report.SetDatabaseLogon("sa", "m1c4system",
                              "172.16.17.41", "PurchaseSystem");
                //idReq
                ParameterValues idReqValues;
                ParameterDiscreteValue idReqDV = new ParameterDiscreteValue();
                ParameterFieldDefinition idReqFD = report.DataDefinition.ParameterFields["@idReq"];
                idReqDV.Value = req.ID_REQ;
                idReqValues = idReqFD.CurrentValues;
                idReqValues.Add(idReqDV);
                idReqFD.ApplyCurrentValues(idReqValues);

                //reqType
                ParameterValues reqTypeValues;
                ParameterDiscreteValue reqTypeDV = new ParameterDiscreteValue();
                ParameterFieldDefinition reqTypeFD = report.DataDefinition.ParameterFields["@reqType"];
                reqTypeDV.Value = req.REQ_TYPE;
                reqTypeValues = reqTypeFD.CurrentValues;
                reqTypeValues.Add(reqTypeDV);
                reqTypeFD.ApplyCurrentValues(reqTypeValues);

                CrystalDecisions.CrystalReports.Engine.TextObject ReqNoField;
                CrystalDecisions.CrystalReports.Engine.TextObject labelGANo;
                CrystalDecisions.CrystalReports.Engine.TextObject ReqDeptNo;
                CrystalDecisions.CrystalReports.Engine.LineObject lineGA;
                CrystalDecisions.CrystalReports.Engine.TextObject AppField;
                ReqNoField = report.ReportDefinition.ReportObjects["ReqNoField"] as TextObject;
                labelGANo = report.ReportDefinition.ReportObjects["lblGANo"] as TextObject;
                ReqDeptNo = report.ReportDefinition.ReportObjects["txtDeptReqNo"] as TextObject;
                lineGA = report.ReportDefinition.ReportObjects["lineGANo"] as LineObject;
                AppField = report.ReportDefinition.ReportObjects["AppField"] as TextObject;
                if (reqNo == 0)
                {
                    labelGANo.Text = "";
                    lineGA.LineColor = Color.White;
                    ReqDeptNo.Text = req.DEPT_REQ_NO;
                }
                else if (reqNo == 1)
                {
                    ReqDeptNo.Text = req.DEPT_REQ_NO;
                    ReqNoField.Text = req.GA_REQ_NO;
                }

                AppField.Text = ApprovalHelper.getApprovalListReport(req.ID_REQ, Variable.ReqType.RequisitionRequest);

                CrystalDecisions.CrystalReports.Engine.TextObject DateField;
                DateField = report.ReportDefinition.ReportObjects["DateField"] as TextObject;
                DateField.Text = req.DATE_CREATED.ToString("dd-MMM-yyyy");

                CrystalDecisions.CrystalReports.Engine.TextObject JobCodeField;
                JobCodeField = report.ReportDefinition.ReportObjects["JobCodeField"] as TextObject;
                JobCodeField.Text = req.JOBCODE;

                CrystalDecisions.CrystalReports.Engine.TextObject FromField;
                FromField = report.ReportDefinition.ReportObjects["FromField"] as TextObject;
                FromField.Text = req.REQ_BY_NAME;

                CrystalDecisions.CrystalReports.Engine.TextObject DeptField;
                DeptField = report.ReportDefinition.ReportObjects["DeptField"] as TextObject;
                DeptField.Text = req.REQ_BY_DEPT;//.Substring(req.REQ_BY_DEPT.IndexOf("-")+1);

                CrystalDecisions.CrystalReports.Engine.TextObject AdditioanlInfoField;
                AdditioanlInfoField = report.ReportDefinition.ReportObjects["AdditionalInfoField"] as TextObject;
                AdditioanlInfoField.Text = req.DESCRIPTION == null ? "" : req.DESCRIPTION;

                Stream = report.ExportToStream(ExportFormatType.PortableDocFormat);
                report.Close();
                report.Dispose();

                int Length = (int)Stream.Length;
                byte[] Buffer = new byte[Length];
                Stream.Read(Buffer, 0, Length);
                Stream.Close();

                string reportName = "";

                if (reqNo == 0)
                    reportName = req.DEPT_REQ_NO.Replace("/", "-").Replace("\\", "-");
                else
                    reportName = req.GA_REQ_NO.Replace("/", "-").Replace("\\", "-");
                ViewState[flName] = reportName;
                ViewState[dlFile] = Buffer;
            }
            catch (Exception e)
            {
                onViewState();
                ScriptManager.RegisterStartupScript(this, GetType(), "errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(e.Message) + "')", true);
            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            exportVehicleReportToPDF();
            ScriptManager.RegisterStartupScript(this, GetType(), "btnPrint download file", "downloadFile();", true);

        }
        #endregion


        protected void gridItem_PreRender(object sender, EventArgs e)
        {
            bool isAnyAdvance = false, isAnyQuo = false;
            for (int i = 0; i < gridItem.Rows.Count; i++)
            {
                var price = (Label)gridItem.Rows[i].FindControl("lblPrice");
                var linkQuo = (LinkButton)gridItem.Rows[i].FindControl("linkQuotation");
                var linkAdv = (LinkButton)gridItem.Rows[i].FindControl("linkAdvance");
                if (price != null)
                    price.Text = string.Format("{0:#,##0.00}", double.Parse(price.Text));
                if (linkQuo != null)
                    if (linkQuo.Text.Any(c => char.IsDigit(c)))
                        isAnyQuo = true;
                if (linkAdv != null)
                    if (linkAdv.Text.Any(c => char.IsDigit(c)))
                        isAnyAdvance = true;

                CheckBox chkRow = (gridItem.Rows[i].FindControl("checkAdvance") as CheckBox);
                if (chkRow != null)
                {
                    bool chkVisible = false;
                    if (!(linkAdv.Text.Any(c => char.IsDigit(c))) && !(linkQuo.Text.Any(c => char.IsDigit(c))))
                        chkVisible = true;
                    chkRow.Visible = chkVisible;
                }

                gridItem.Columns[8].Visible = isAnyAdvance;
                gridItem.Columns[7].Visible = isAnyQuo;

            }


        }

        protected void btnAdvance_Click(object sender, EventArgs e)
        {
            try
            {
                gridItem.Columns[10].Visible = true;
                submitAdvance.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "hide progress", "closeLoading();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "set Jquery button", "setButton();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnAdvance errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
        }

        protected void btnSubmitToAdvance_Click(object sender, EventArgs e)
        {
            try
            {
                List<RequisitionItems> itemAdvance = new List<RequisitionItems>();
                List<RequisitionItems> items = (List<RequisitionItems>)ViewState[curItem];
                RequisitionObject req = (RequisitionObject)ViewState[curReq];
                logInUser = (Employee)ViewState[curUser];
                int idAdvance = 0;
                foreach (GridViewRow row in gridItem.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (row.Cells[8].FindControl("checkAdvance") as CheckBox);
                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                if (idAdvance == 0)
                                    idAdvance = RequisitionHelper.insertIntoAdvance(req.ID_REQ, logInUser.EMP_ID, req.JOBCODE);

                                if (idAdvance > 0)
                                {
                                    int idItem = 0;
                                    Label lblID = (row.Cells[2].FindControl("lblItemID") as Label);
                                    if (lblID != null)
                                        if (int.TryParse(lblID.Text, out idItem))
                                        {
                                            RequisitionItems item = items.FirstOrDefault(x => x.PART_ID == idItem);
                                            itemAdvance.Add(item);
                                        }
                                }
                            }
                        }
                    }
                }

                RequisitionHelper.updateReqDetailAdvance(itemAdvance, idAdvance);
                items = Helper.RequisitionHelper.getRequisitionDetail(req.ID_REQ);
                ViewState[curItem] = items;
                loadGridViewItem();
                ScriptManager.RegisterStartupScript(this, GetType(), "set Jquery button", "setButton();", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnSubmitToAdvance errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
        }

        protected void linkPO_Click(object sender, EventArgs e)
        {
            RequisitionObject req = (RequisitionObject)ViewState[curReq];
            if (req.ID_PO > 0)
                Response.Redirect("PurchaseOrder.aspx?ViewPO=" + req.ID_PO);
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {

                RequisitionObject req = (RequisitionObject)ViewState[curReq];

                string reportName = ViewState[flName].ToString();

                byte[] Buffer = (byte[])ViewState[dlFile];

                Response.BinaryWrite(Buffer);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + reportName + ".pdf");
                Response.Flush(); 
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnDownload errorMsg", "showErrorMsg('Error','" + HttpUtility.HtmlEncode(ex.Message) + "')", true);
            }
            finally
            {
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Thread.Sleep(1);
            }

        }

        private void setView()
        {
            if (ViewState[curState].ToString().Equals("edit") || ViewState[curState].ToString().Equals("new"))
                onEditState();
            else if (ViewState[curState].ToString().Equals("view"))
                onViewState();
        }


    }
}