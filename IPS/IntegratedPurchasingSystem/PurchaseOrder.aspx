﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOrder.aspx.cs" Inherits="IntegratedPurchasingSystem.PurchaseOrder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase System</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width" />

    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery/jquery-ui.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/jquery/jquery.ui.theme.css" rel="stylesheet" />
    <link href="Content/ValidationEngine.css" rel="stylesheet" />
    <script src="Script/modernizr-2.5.3.js"></script>
    <script src="Script/jquery.min.js"></script>
    <script src="Script/jquery-ui.min.js"></script>
    <script src="Script/jquery-1.7.1.min.js"></script>
    <script src="Script/jquery-1.7.1.js"></script>
    <script src="Script/jquery-ui-1.8.20.min.js"></script>
    <script src="Script/jquery.ui.timepicker.js"></script>
    <script src="Script/jquery.validationEngine.js"></script>
    <script src="Script/jquery.validationEngine-en.js"></script>
    <link href="Content/jqsimplemenu.css" rel="stylesheet" />
    <script src="Script/jqsimplemenu.js"></script>
    <script src="Script/PageScript/poPage.js"></script>
    <script src="Script/autoNumeric.js"></script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off" enctype="MULTIPART/FORM-DATA">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div class="imgLoading"><img src="Content/images/loading-bar.gif" alt='loading image' height="30" /></div>
        <div class="header">
            <div class="content-wrapper">
                <div class="float-left">
                    <span class="site-title">Integrated Purchasing System</span>
                </div>
                <div class="float-right">
                    <img alt="Logo" src="Content/images/JGC-Icon.png" />
                </div>
            </div>
        </div>
        <div id="main">
            <div id="loadingLayoutIndicator" class="ui-loading-bar" style="display: none;">
            </div>
            <div class="content-wrapper">
                <div class="float-left">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <div class="float-right">
                    <section id="login">
                        Hello,
                        <asp:Label ID="lblUsername" runat="server" Text="username" CssClass="username"></asp:Label>
                    </section>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="content-wrapper main-content clear-fix">
                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Purchase Order&nbsp; Data
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail">
                        <table class="contentForm" contenteditable="false">
                            <tr class="hideOnCreateNew">
                                <td class="contentFormField">REQUEST No</td>
                                <td class="contentFormData">
                                    <%--<asp:Label ID="lblRequestNo" runat="server" Text=""></asp:Label>--%>
                                    <asp:LinkButton ID="linkRequestNo" runat="server" OnClick="linkRequestNo_Click"></asp:LinkButton>
                                </td>
                                <td class="contentFormField">Date Created</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblDateCreated" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">JobCode</td>
                                <td class="contentFormData">
                                    <div id="Div2" class="onView" runat="server">
                                        <asp:Label ID="lblJobCode" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                                <td class="contentFormField">Project Name</td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Vendor</td>
                                <td class="contentFormData">
                                    <div runat="server" id="vendorSearch">
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lblVendorID" runat="server" Text="" CssClass="hidden"></asp:Label>
                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="validate[required]"></asp:TextBox>
                                                &nbsp
                                            <asp:Button ID="btnVendor" runat="server" Text=" ... " />
                                                &nbsp
                                            <asp:Button ID="btnResetVendor" runat="server" Text="Reset" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="gridVendorSearch" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>

                                    <asp:Label ID="lblVendor" runat="server" Text=""></asp:Label>

                                </td>
                                <td class="contentFormField">Vendor Contact
                                </td>
                                <td class="contentFormData">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblVendorPIC" runat="server" Text=""></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="gridVendorSearch" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Vendor Address</td>
                                <td class="contentFormData">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblVendorAddress" runat="server" Text=""></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="gridVendorSearch" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="contentFormField">Place</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtPlace" runat="server" TextMode="MultiLine" CssClass="validate[required]"></asp:TextBox>
                                    <asp:Label ID="lblPlace" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField">Delivery Conditions</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtDelTime" runat="server" CssClass="validate[required]"></asp:TextBox>
                                    <asp:Label ID="lblDelTime" runat="server" Text=""></asp:Label>
                                    day/days
                                </td>
                                <td class="contentFormField">Payment Terms</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlPayTerms" runat="server" CssClass="validate[required]">
                                        <asp:ListItem Text="-- Select Option --" Value="" />
                                        <asp:ListItem Text="Cash On Delivery" Value="Cash On Delivery" />
                                        <asp:ListItem Text="14 days" Value="14 days" />
                                        <asp:ListItem Text="30 days" Value="30 days" />
                                        <asp:ListItem Text="Others" Value="Others" />
                                    </asp:DropDownList>
                                    <br />
                                    <asp:TextBox ID="txtPayTermOther" runat="server" CssClass="hidden"></asp:TextBox>
                                    <asp:Label ID="lblPayTerm" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField" style="font-weight: bold">Activity</td>
                                <td class="contentFormData" style="font-weight: bold">
                                    <asp:Label ID="lblActivity" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="contentFormField" style="font-weight: bold">SOFI Status</td>
                                <td class="contentFormData" style="font-weight: bold">
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div class="contentPanel">
                    <div class="contentPanelHeader">
                        <div class="contentPanelText">
                            Purchase Order Item
                        </div>
                        <div class="contentPanelExpand">
                        </div>
                    </div>
                    <div class="contentPanelDetail" style="padding: 20px;">

                        <table style="margin-top:0;padding:0px;">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="contentFormField" style="vertical-align: top">Currency</td>
                                            <td class="contentFormData">
                                                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="validate[required]"></asp:DropDownList>
                                                <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td class="contentFormField" style="vertical-align: top">Exchange Rate</td>
                                            <td class="contentFormData">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblExchangeRate" runat="server" Text=""></asp:Label>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnGetExchangeRate" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="contentFormField" style="vertical-align: top">Item Description</td>
                                            <td class="contentFormData">
                                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView CssClass="mGrid"
                                        ID="gridItem" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False"
                                        ShowHeaderWhenEmpty="true" OnRowEditing="gridItem_RowEditing" OnRowUpdating="gridItem_RowUpdating"
                                        OnRowCancelingEdit="gridItem_RowCancelingEdit" OnPreRender="gridItem_PreRender" OnRowDeleting="gridItem_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemID" runat="server" Text='<%# Eval("ID_ITEM")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Quantity">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("QUANTITY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Metric">
                                                <EditItemTemplate>
                                                    <div class="metricSingular">
                                                        <asp:DropDownList ID="ddlMetricPlural" runat="server">
                                                            <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                            <asp:ListItem Text="pcs"></asp:ListItem>
                                                            <asp:ListItem Text="boxes"></asp:ListItem>
                                                            <asp:ListItem Text="packs"></asp:ListItem>
                                                            <asp:ListItem Text="units"></asp:ListItem>
                                                            <asp:ListItem Text="seats"></asp:ListItem>
                                                            <asp:ListItem Text="kg"></asp:ListItem>
                                                            <asp:ListItem Text="grams"></asp:ListItem>
                                                            <asp:ListItem Text="meters"></asp:ListItem>
                                                            <asp:ListItem Text="cm"></asp:ListItem>
                                                            <asp:ListItem Text="lot"></asp:ListItem>
                                                            <asp:ListItem Text="dozens"></asp:ListItem>
                                                            <asp:ListItem Text="reams"></asp:ListItem>
                                                            <asp:ListItem Text="sets"></asp:ListItem>
                                                            <asp:ListItem Text="pairs"></asp:ListItem>
                                                            <asp:ListItem Text="pails"></asp:ListItem>
                                                            <asp:ListItem Text="rolls"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="metricPlural">
                                                        <asp:DropDownList ID="ddlMetricSingular" runat="server">
                                                            <asp:ListItem Text="-- Select Metric --"></asp:ListItem>
                                                            <asp:ListItem Text="pc"></asp:ListItem>
                                                            <asp:ListItem Text="box"></asp:ListItem>
                                                            <asp:ListItem Text="pack"></asp:ListItem>
                                                            <asp:ListItem Text="unit"></asp:ListItem>
                                                            <asp:ListItem Text="seat"></asp:ListItem>
                                                            <asp:ListItem Text="kg"></asp:ListItem>
                                                            <asp:ListItem Text="gram"></asp:ListItem>
                                                            <asp:ListItem Text="meter"></asp:ListItem>
                                                            <asp:ListItem Text="cm"></asp:ListItem>
                                                            <asp:ListItem Text="dozen"></asp:ListItem>
                                                            <asp:ListItem Text="ream"></asp:ListItem>
                                                            <asp:ListItem Text="set"></asp:ListItem>
                                                            <asp:ListItem Text="pair"></asp:ListItem>
                                                            <asp:ListItem Text="pail"></asp:ListItem>
                                                            <asp:ListItem Text="roll"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMetric" runat="server" Text='<%# Eval("METRIC")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Price">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remark">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRemarks" runat="server" Text='<%# Eval("REMARK")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <span><%# Eval("REMARK")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="actionLink">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="linkEditItem" runat="server" CommandArgument='<%# Eval("ID_ITEM")%>'
                                                        Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                    <asp:LinkButton ID="linkDeleteItem" runat="server" CommandArgument='<%# Eval("ID_ITEM")%>'
                                                        Text="Delete" CommandName="Delete" OnClientClick="return confirm('Do you Want to Delete this Record?');"></asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="linkUpdate" runat="server" CausesValidation="False" CommandName="Update" Text="Update" CommandArgument='<%# Eval("ID_ITEM")  %>'></asp:LinkButton>
                                                    <asp:LinkButton ID="linkCancelUpdate" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CommandArgument='<%# Eval("ID_ITEM")  %>'></asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                            <table style="float: right;">
                            <tr>
                                <td class="contentFormField" style="width:200px;">Total Amount of Item</td>
                                <td class="contentFormData">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblTotAmountOfItem" runat="server" Text=""></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnGenerateTotalAmount" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                        </div>

                        
                        <br />
                        <table style="margin-top:0;padding:0px;">
                            <tr>
                                <td class="contentFormField" >Total Net Price <div style="font-size:9px;">(from Requisition / Quotation)</div></td>
                                <td class="contentFormData">
                                    <asp:Label ID="lblQuotedPrice" runat="server" Text=""></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField" >Discount</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtDiscount" runat="server" CssClass="validate[required,custom[number]]"></asp:TextBox>
                                    <asp:Label ID="lblDiscount" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td class="contentFormField" >VAT</td>
                                <td class="contentFormData">
                                    <asp:DropDownList ID="ddlVAT" runat="server" CssClass="validate[required]">
                                        <asp:ListItem Text="-- Select Option --" Value="" />
                                        <asp:ListItem Text="No VAT" Value="No VAT" />
                                        <asp:ListItem Text="Include" Value="Include" />
                                        <asp:ListItem Text="Exclude" Value="Exclude" />
                                    </asp:DropDownList>
                                    <asp:Label ID="lblVAT" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="contentFormField" >Total Amount</td>
                                <td class="contentFormData">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnGenerateTotalAmount" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentFormField" style="vertical-align: top">Remark</td>
                                <td class="contentFormData">
                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <asp:Label ID="lblRemark" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>



                <div id="actionHistory" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action History
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnHiddenAction" runat="server" Text="Button" CausesValidation="false" Style="display: none;" />
                                    <asp:GridView ShowHeaderWhenEmpty="true"
                                        CssClass="mGrid" ID="gridActionHistory" EmptyDataText="No records!" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Participant" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("PARTICIPANT")%> - <%# Eval("PARTICIPANT_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Completed By" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Eval("COMPLETED_BY")%> - <%# Eval("COMPLETED_BY_NAME")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Activity" DataField="ACTIVITY_TEXT" />
                                            <asp:BoundField runat="server" HeaderText="Action" DataField="ACTION_TEXT" />
                                            <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%#  Eval("START_DATE", "{0:dd MMM yyyy - HH:mm }") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span><%# Convert.ToString(Eval("END_DATE")).Contains("1/1/1975")?"":Eval("END_DATE", "{0:dd MMM yyyy - HH:mm}") %></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField runat="server" HeaderText="Comment" DataField="COMMENT" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="actionDiv" runat="server">
                    <div class="contentPanel">
                        <div class="contentPanelHeader">
                            <div class="contentPanelText">
                                Action 
                            </div>
                            <div class="contentPanelExpand">
                            </div>
                        </div>
                        <div class="contentPanelDetail" style="padding: 20px;">
                            <table>
                                <tr>
                                    <td>
                                        <table class="onEdit">
                                            <tr id="actionField" runat="server">
                                                <td class="contentFormField">Action</td>
                                                <td class="contentFormData">
                                                    <asp:DropDownList ID="ddlAction" runat="server" CssClass="validate[required]">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentFormField">Comment</td>
                                                <td class="contentFormData">
                                                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentFormField"></td>
                                                <td class="contentFormData">
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Execute" OnClick="btnSubmit_Click" />

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <div id="dialogVendor">
                        <div>
                            <table>
                                <tr>
                                    <td class="contentFormField">Search Vendor By</td>
                                    <td class="contentFormData">
                                        <asp:DropDownList ID="ddlVendor" runat="server">
                                            <asp:ListItem Text="Name"></asp:ListItem>
                                            <asp:ListItem Text="Code"></asp:ListItem>
                                            <asp:ListItem Text="Address"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:TextBox ID="txtSearchVendor" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="contentFormData">
                                        <asp:Button ID="btnSearchVendor" runat="server" Text="Search" OnClick="btnSearchVendor_Click" CausesValidation="false" /></td>
                                </tr>
                            </table>
                            <div>
                                <asp:UpdatePanel ID="pudtPaneVendor" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView AllowPaging="True" PageSize="10"
                                            ID="gridVendorSearch" EmptyDataText="No records found!" CssClass="mGrid" runat="server" AutoGenerateColumns="False"
                                            OnSelectedIndexChanged="gridVendorSearch_SelectedIndexChanged" OnPageIndexChanging="gridVendorSearch_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="vendorID" HeaderText="Vendor ID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                                <asp:BoundField DataField="vendorCD" HeaderText="Vendor Code" />
                                                <asp:BoundField DataField="vndName" HeaderText="Vendor Name" />
                                                <asp:BoundField DataField="vndAddress" HeaderText="Address" />
                                                <asp:BoundField DataField="vndContact" HeaderText="Contact" />
                                                <asp:BoundField DataField="vndPhone" HeaderText="Phone" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearchVendor" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div id="divError">
                        <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                    </div>

                    <div id="divSuccess">
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <asp:Button ID="btnGenerateTotalAmount" runat="server" Text="Button" OnClick="btnGenerateTotalAmount_Click" />
                    <asp:Button ID="btnGetExchangeRate" runat="server" Text="Button" CausesValidation="false" OnClick="btnGetExchangeRate_Click" />
                    <asp:Button ID="btnReloadPage" runat="server" Text="Button" OnClick="btnReloadPage_Click" />
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="content-wrapper">
                <div class="float-right">
                    <span>Copyright &copy; 2015 PT JGC Indonesia. All rights reserved.</span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
