﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IntegratedPurchasingSystem.Helper;
using IntegratedPurchasingSystem.Entities;

namespace IntegratedPurchasingSystem
{
    public partial class Vendor : System.Web.UI.Page
    {
        Employee logInUser = new Employee();
        const string curUser = "current_Log_In_User";
        const string curState = "current_State";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                logInUser = Helper.PFNATTHelper.getEmpDataByUsername(Request.LogonUserIdentity.Name);
                litMenu.Text = Helper.MenuHelper.getMenu();
                lblUsername.Text = logInUser.FULL_NAME;
                ViewState[curUser] = logInUser;
                ViewState[curState] = "view";
                //loadVendor("");
            }
        }

        //private void loadVendor(string search)
        //{
        //    gridVendor.DataSource = Helper.VendorHelper.getVendor(search);
        //    gridVendor.DataBind();
        //}

        //protected void gridVendor_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        int id = int.Parse(e.CommandArgument.ToString());
        //        if (e.CommandName.Equals("Delete Vendor"))
        //        {
        //            VendorHelper.deleteVendor(id);
        //            loadVendor(txtSearchVendor.Text);
        //            ScriptManager.RegisterStartupScript(this, GetType(), "delete", "showMsg('Success', 'Selected vendor record had been deleted');", true);
        //        }
        //        else
        //        {
        //            ViewState[curState] = "edit";
        //            VENDOR vendor = VendorHelper.getVendorByID(id);
        //            ScriptManager.RegisterStartupScript(this, GetType(), "setEdit", "openDialogEditVendor('" + id.ToString() + "', '" + vendor.CODE + "', '" + vendor.VENDOR_NAME + "', '" + vendor.ADDRESS + "')", true);
        //        }
        //    }
        //    catch (Exception acExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "actionError", "showMsg('Error','" + HttpUtility.HtmlEncode(acExp.Message) + "')", true);
        //    }
        //}

        //protected void gridVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gridVendor.PageIndex = e.NewPageIndex;
        //    loadVendor(txtSearchVendor.Text);
        //}

        //protected void btnSaveVendor_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ViewState[curState].ToString().Equals("new"))
        //        {
        //            if (VendorHelper.isCodeAlreadyExist(txtVendorCode.Text))
        //                ScriptManager.RegisterStartupScript(this, GetType(), "vendor code exist", "showMsg('Warning!','Vendor code already exist! <br />Please choose another code!')", true);
        //            else
        //            {
        //                VendorHelper.insertNewVendor(txtVendorCode.Text, txtVendorName.Text, txtVendorAddress.Text);
        //                loadVendor("");
        //                ScriptManager.RegisterStartupScript(this, GetType(), "close dialog vendor", "closeDialogVendor();", true);
        //                ScriptManager.RegisterStartupScript(this, GetType(), "new vendor Success", "showMsg('Success','Succesfully added new Vendor data!');", true);
        //            }
        //        }
        //        else if (ViewState[curState].ToString().Equals("edit"))
        //        {
        //            VENDOR vendor = VendorHelper.getVendorByID(int.Parse(txtVendorID.Text));
        //            if (vendor.CODE.Equals(txtVendorCode.Text))
        //            {
        //                VendorHelper.updateVendor(int.Parse(txtVendorID.Text), txtVendorCode.Text, txtVendorName.Text, txtVendorAddress.Text);
        //                loadVendor("");
        //                ScriptManager.RegisterStartupScript(this, GetType(), "close dialog vendor", "closeDialogVendor();", true);
        //                ScriptManager.RegisterStartupScript(this, GetType(), "edit vendor success", "showMsg('Success','Vendor record had been updated!')", true);
        //            }
        //            else
        //                ScriptManager.RegisterStartupScript(this, GetType(), "vendorcodeexist", "showMsg('Warning!','Vendor code already exist! <br />Please choose another code!')", true);
        //        }

        //    }
        //    catch (Exception sExp)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "saveError", "showMsg('Error','" + HttpUtility.HtmlEncode(sExp.Message) + "')", true);
        //    }
        //}

        //protected void btnSearchVendor_Click(object sender, EventArgs e)
        //{
        //    loadVendor(txtSearchVendor.Text);
        //}

        //protected void btnNewVendor_Click(object sender, EventArgs e)
        //{
        //    ViewState[curState] = "new";
        //    ScriptManager.RegisterStartupScript(this, GetType(), "new vendor", "openDialogNewVendor();", true);
        //}

        //protected void btnCheckCode_Click(object sender, EventArgs e)
        //{
        //    if (VendorHelper.isCodeAlreadyExist(txtVendorCode.Text) || txtVendorCode.Text.Equals(string.Empty)) // already exist, not allowed
        //        imgCheckCode.ImageUrl = "~/Content/images/incorrect.png";
        //    else // allowed
        //        imgCheckCode.ImageUrl = "~/Content/images/correct.png";
        //    ScriptManager.RegisterStartupScript(this, GetType(), "show dialog", "openDialogVendor();", true);
        //}
    }
}