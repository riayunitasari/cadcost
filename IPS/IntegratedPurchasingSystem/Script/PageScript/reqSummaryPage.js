﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $('#btnSubmit').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });

    $("input[type=submit], button, input[type=button]").button();
    $("#txtExchangeRate").autoNumeric({ aSep: ',', aDec: '.' });
    $("input[id*='txtQuantity']").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewQuantity").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewPrice").autoNumeric({ aSep: ',', aDec: '.' });
    $("input[id*='txtPrice']").autoNumeric({ aSep: ',', aDec: '.' });

    $(function () {
        $(document).on('keypress', function (event) {
            if (event.keyCode == 13) {
                if ($("#dialogItem").dialog("isOpen") === true) {
                    $("#btnSearchItem").click();
                    return false;
                }
                else if ($("#dialogJobCode").dialog("isOpen") === true) {
                    $("#btnSearchJobcode").click();
                    return false;
                }
                else {
                    closeLoading();
                    return false;
                }
            }

        });
    });

});

$("#txtSearchItem").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchItem").click();
    }
})

$("#txtSearchJobcode").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchJobcode").click();
    }
})

function openDialogRequisition() {
    $("#dialogRequisition").dialog({
        title: "Requisition",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogRequisition").parent().appendTo($("form:first"));
}

function openDialogItemReq() {
    $("#dialogItemReq").dialog({
        title: "Requisition",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
        //OK: function () {
        //    $(this).dialog("close");
        //    return false;
        //}
    }
        
    });
    $("#dialogItemReq").parent().appendTo($("form:first"));
}

function closeItemReq() {
    $("#lblIDReq").text("");
    $("#dialogItemReq").dialog("close");
}

$("#btnCancelItemReq").live("click", function () {
    closeItemReq();
})

//$("#btnSubmitItem").live("click", function () {
//    $('#btnGetSelectedItem').click();
//    //return false;
//})

function openDialogJobCode() {
    $("#dialogJobCode").dialog({
        title: "JobCode",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogJobCode").parent().appendTo($("form:first"));
}

$("#btnJobCodeReq").live("click", function () {
    openDialogJobCode();
    return false;
});

$("#txtJobCodeReq").live("focus", function () {
    openDialogJobCode();
    return false;
})

$("#btnJobCode").live("click", function () {
    openDialogJobCode();
    return false;
});

$("#txtJobCode").live("focus", function () {
    openDialogJobCode();
    return false;
})

function setCenter(dialogName) {
    $("#" + dialogName).dialog({
        width: 'auto',
    });
    $("#" + dialogName).dialog({
        position: ['center'],
    });
    return false;
}

$("#btnResetJobCode").live("click", function () {
    $("#txtJobCode").val("");
    return false;
})

$("#btnResetJobCodeReq").live("click", function () {
    $("#txtJobCodeReq").val("");
    return false;
})


function closeJobCode(jobcode, name, state) {
    $(function () {
        $("#dialogJobCode").parent().appendTo($("form:first"));
        $("#dialogJobCode").dialog("close");
    });
    if (state === 'onDialog') {
        $('#txtJobCodeReq').val(jobcode);
        $("#dialogRequisition").dialog({ width: 'auto' });
        setCenter('dialogRequisition');
        
    }
    else {
        $('#lblProjectName').text(name);
        $('#txtJobCode').val(jobcode);
        $('#txtJobCode').validationEngine("hide");
    }
    $('#txtSearchJobCode').val("");
        
}

function showMsg(titles, msg) {

    if ($("#dialogItem").dialog("isOpen") === true) {
        $("#dialogItem").dialog("close");
    }

    if ($("#dialogAdditionalItem").dialog("isOpen") === true) {
        $("#dialogAdditionalItem").dialog("close");
    }

    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}


function openItem() {
    $("#ddlItemCategory").hide();
    $("#ddlItemType").hide();
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));

}

function openEditItem() {
 
    $("#dialogEditItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogEditItem").parent().appendTo($("form:first"));

}

//$("#btnAddItem").live("click", function () {
//    openItem();
//    return false;
//})

$("#ddlItem").live("change", function () {
    var value = $('#ddlItem :selected').text();
    if (value === 'Category') {
        $("#ddlItemCategory").show();
        $("#txtSearchItem").hide();
        $("#ddlItemType").hide();
    }
    else if (value === 'Type') {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").hide();
        $("#ddlItemType").show();
    }
    else {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").show();
        $("#ddlItemType").hide();
    }
    $("#dialogItem").dialog({
        position: ['center'],
    });
    $("#dialogItem").parent().appendTo($("form:first"));
    return false;
});

function ddlMetricOnNew() {
    var value = $("#txtNewQuantity").val().replace(/,/g, '');
    if ($.isNumeric(value)) {
        if (parseFloat(value) > 1) {
            $(".metricPlural").show();
            $(".metricSingular").hide();
        }
        else {
            $(".metricSingular").show();
            $(".metricPlural").hide();
        }
    }
    else {
        $(".metricPlural").hide();
        $(".metricSingular").hide();
    }
}

function ddlMetricMergedItem() {
    if ($.isNumeric($("span[id*='gridMergedItem_lblQuantity']").text())) {
        if (parseFloat($("span[id*='gridMergedItem_lblQuantity']").text()) > 1) {
            $(".metricPlural").show();
            $(".metricSingular").hide();
        }
        else {
            $(".metricSingular").show();
            $(".metricPlural").hide();
        }
    }
    else {
        $(".metricPlural").hide();
        $(".metricSingular").hide();
    }
}

$("input[id*='gridItem_txtQuantity']").live("change", function () {
    if ($.isNumeric($("input[id*='gridItem_txtQuantity']").val())) {
        if (parseFloat($("input[id*='gridItem_txtQuantity']").val()) > 1) {
            if ($("select[id*='gridItem_ddlMetricPlural']").hasClass("hidden"))
                $("select[id*='gridItem_ddlMetricPlural']").removeClass("hidden");
            $("select[id*='gridItem_ddlMetricSingular']").addClass("hidden");
           // $(".metricPlural").show();
           // $(".metricSingular").hide();
        }
        else {
            if ($("select[id *='gridItem_ddlMetricSingular']").hasClass("hidden"))
                $("select[id *='gridItem_ddlMetricSingular']").removeClass("hidden");
            $("select[id *='gridItem_ddlMetricPlural']").addClass("hidden");

           // $(".metricSingular").show();
          //  $(".metricPlural").hide();
        }
    }
    else {
       // $(".metricPlural").hide();
        // $(".metricSingular").hide();
        $("select[id *='gridItem_ddlMetricPlural']").addClass("hidden");
        $("select[id*='gridItem_ddlMetricSingular']").addClass("hidden");
    }
});

//$(function () {

//    $(".metricSingular").show();
//    $(".metricPlural").hide();

//});

$("input[id*='txtItem']").live("focus", function () {
    openItem();
    return false;
});

$("input[id*='txtNewItem']").live("click", function () {
    openItem();
    return false;
});

$("input[id*='txtNewItemID']").live("focus", function () {
    openItem();
    return false;
});

function closeDialogItem() {
    ddlMetricOnNew();
    $("#dialogItem").dialog("close");
}

function setDLLMetricOnEdit(gridName, quantity, metric, index) {

    var pluralID = "#"+gridName+"_ddlMetricPlural_" + index;
    var singularID = "#"+gridName+"_ddlMetricSingular_" + index;

    if (parseFloat(quantity) > 1) {
        //$(pluralID).show();
        //$(singularID).hide();
        if ($(pluralID).hasClass("hidden"))
            $(pluralID).removeClass("hidden");
        $(singularID).addClass("hidden");
        $(pluralID).val(metric);
    }
    else {
        //$(singularID).show();
       // $(pluralID).hide();
        if ($(singularID).hasClass("hidden"))
            $(singularID).removeClass("hidden");
        $(pluralID).addClass("hidden");
        $(singularID).val(metric);
    }
}

$('#ddlCurrency').live('change', function (event) {
    var value = $('#ddlCurrency :selected').val();
    if (value === 'IDR') {
        $('#txtExchangeRate').val('0');
        $('#txtExchangeRate').attr("disabled", "disabled");
        if ($('#txtExchangeRate').hasClass("validate[required]")) {
            $('#txtExchangeRate').validationEngine("hide");
            $('#txtExchangeRate').removeClass("validate[required]");
        }
    } else {
        $("#txtExchangeRate").removeAttr("disabled");
        $('#txtExchangeRate').addClass("validate[required]");
    }
    $('#lblCurrencyEst').text($('#ddlCurrency :selected').val());
});

$('#dialogItem').live('dialogclose', function (event) {
    $('#btnResetGridItem').click();
    //return false;
});

$('#dialogRequisition').live('dialogclose', function (event) {
    $('#btnResetJobCodeState').click();
});

function openDetailItem() {
 
    $("#dialogDetailItem").dialog({
        title: "Detail Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogDetailItem").parent().appendTo($("form:first"));
}

function setOnView() {

    if (!$(".onEdit").hasClass("hidden"))
        $(".onEdit").addClass("hidden");
    if ($(".onView").hasClass("hidden"))
        $(".onView").removeClass("hidden");
}

function setOnEdit() {

    if (!$(".onView").hasClass("hidden"))
        $(".onView").addClass("hidden");
    if ($(".onEdit").hasClass("hidden"))
        $(".onEdit").removeClass("hidden");
}

function showSuccessMsg() {

    if ($("#dialogItem").dialog("isOpen") === true) {
        $("#dialogItem").dialog("close");
    }

    $("#lblErrorMsg").html('Your change has been saved successfully!');
    $("#divError").dialog({
        title: 'Save Success!',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                $('#btnReloadPage').click();
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function onRowEditing()
{
    $("a[id*='linkEditItem']").attr("disabled", true);
    $("a[id*='linkDeleteItem']").attr("disabled", true);
    $("input[id*='btnJobCode']").attr("disabled", true);
    $("input[id*='btnResetJobCode']").attr("disabled", true);
    $("input[id*='btnSubmit']").attr("disabled", true);
    $("input[id*='btnCancel']").attr("disabled", true);
    $("input[id*='btnAddRequisition']").attr("disabled", true);
    $("input[id*='btnNewItem']").attr("disabled", true);
    $("a[id*='linkSeeDetail']").attr("disabled", true);
    
    //alert("masuk");
}

function onRowCompleteEditing() {
    $("a[id*='linkEditItem']").attr("disabled", false);
    $("a[id*='linkDeleteItem']").attr("disabled", false);
    $("input[id*='btnJobCode']").attr("disabled", false);
    $("input[id*='btnResetJobCode']").attr("disabled", false);
    $("input[id*='btnSubmit']").attr("disabled", false);
    $("input[id*='btnCancel']").attr("disabled", false);
    $("input[id*='btnAddRequisition']").attr("disabled", false);
    $("input[id*='btnNewItem']").attr("disabled", false);
    $("a[id*='linkSeeDetail']").attr("disabled", false);
}

function onRowInsert() {
    $("a[id*='linkUpdate']").attr("disabled", true);
    $("a[id*='linkCancelUpdate']").attr("disabled", true);
    onRowEditing();
    //alert("masuk");
}

function onRowCancelInsert() {
    $("a[id*='linkUpdate']").attr("disabled", false);
    $("a[id*='linkCancelUpdate']").attr("disabled", false);
    onRowCompleteEditing();
}

$('#btnNewItem').live('click', function (event) {

    $("#txtNewItem").val("");
    $("#txtNewQuantity").val("");
    $("#ddlMetricPlural").val("");
    $("#ddlMetricSingular").val("");
    $("#txtNewPrice").val("");
    $("#txtNewRemark").val("");
    ddlMetricOnNew();
    $("#dialogAdditionalItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogAdditionalItem").parent().appendTo($("form:first"));
    return false;
});

$("#dialogAdditionalItem").live("dialogclose", function () {
    ddlMetricOnNew();
    $("#txtNewItem").val("");
    $("#txtNewQuantity").val("");
    $("#txtNewPrice").val("");
    $("#txtNewRemark").val("");

});

function closeDialogAdditional() {
    $("#dialogAdditionalItem").dialog("close");
    ddlMetricOnNew();
}

$("#btnCancelAdditional").live("click", function () {
    closeDialogAdditional();
    return false;
})

function addCommas(x) {
    var parts;
    var retVal;
    if (x.indexOf('.') !== -1) {
        var parts = x.toString().split(".");

        parts[0] = parts[0] ? parseFloat(parts[0].replace(/,/g, '')) : 0;
        parts[0] = parts[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        if (parts.length > 2)
            return parts[0] + "." + parts[1];
        else
            return parts.join(".");
    }
    else {
        retVal = x ? parseFloat(x.replace(/,/g, '')) : 0;
        return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}


function setButton() {
    $("input[type=submit], input[type=button], button").button();
}

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});

function closeLoading() {
    $('.overlay').hide();
    $('.imgLoading').hide();
}

function downloadFile() {
    closeLoading();
    $("#btnDownload").click();
}