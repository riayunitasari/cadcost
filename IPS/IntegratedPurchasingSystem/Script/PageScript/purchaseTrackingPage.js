﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $("input[type=submit], button").button();

});

function setButton() {
    $("input[type=submit], button").button();
}

function showMsg(titles, msg) {
    setButton();
    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function showException(titles, msg) {
   // closeAllDialog();
    $("#lblErrorMsg").text(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function setCenter(dialogName) {
    $("#" + dialogName).dialog({
        position: ['center'],
    });
}


function openDialogDetail() {
    $("#dialogDetail").dialog({
        title: 'Detail Purchase',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#dialogDetail").parent().appendTo($("form:first"));
    return false;
}

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});


function closeLoading() {
        $('.overlay').hide();
        $('.imgLoading').hide();
}

$("#ddlLastUpdateOn").live("change", function () {
    $("#btnInitiateActivity").click();
})