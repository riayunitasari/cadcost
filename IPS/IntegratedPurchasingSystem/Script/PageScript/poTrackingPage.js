﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $("input[type=submit], button").button();

    $(function () {
        $(document).on('keypress', function (event) {
            if (event.keyCode == 13) {
                if ($("#dialogVendor").dialog("isOpen") === true) {
                    $("#btnSearchVendor").click();
                    return false;
                }
                else if ($("#dialogJobCode").dialog("isOpen") === true) {
                    $("#btnSearchJobCode").click();
                    return false;
                }
                else if ($("#dialogItem").dialog("isOpen") === true) {
                    $("#btnSearchItem").click();
                    return false;
                }
                else {
                    closeLoading();
                    return false;
                }
            }

        });
    });

});

$("#txtSearchJobCode").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchJobCode").click();
    }
})

$("#txtSearchVendor").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchVendor").click();
    }
})


$("#txtSearchItem").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchItem").click();
    }
})

function setButton() {
    $("input[type=submit], button").button();
}

function showMsg(titles, msg) {
    setButton();
    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function showException(titles, msg) {
    closeAllDialog();
    $("#lblErrorMsg").text(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function openDialogJobCode() {
    $("#dialogJobCode").dialog({
        title: "JobCode",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });

    $("#dialogJobCode").parent().appendTo($("form:first"));
}

$("#btnJobCode").live("click", function () {
    openDialogJobCode();
    return false;
});

$("#txtJobCode").live("focus", function () {
    openDialogJobCode();
    return false;
})

$("#btnResetJobCode").live("click", function () {
    $("#txtJobCode").val("");

    return false;
})

function closeJobCode(jobcode) {
    $(function () {
        $("#dialogJobCode").parent().appendTo($("form:first"));
        $("#dialogJobCode").dialog("close");
    });

    $('#txtJobCode').val(jobcode);
    $('#txtSearchJobCode').val("");
}

function setCenter(dialogName) {
    $("#" + dialogName).dialog({
        position: ['center'],
    });

}

function openDialogVendor() {
    $("#dialogVendor").dialog({
        title: 'Quotation - Vendor',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
    });
    $("#dialogVendor").parent().appendTo($("form:first"));
    return false;
}

$("#txtVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnResetVendor").live("click", function () {
    $("#txtVendor").val("");
    return false;
})

function closeDialogVendor(idVendor, vendorName) {
    $("#txtVendorID").val(idVendor);
    $("#txtVendor").val(vendorName);
    $("#dialogVendor").dialog("close");
}

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});

function closeLoading() {
    $('.overlay').hide();
    $('.imgLoading').hide();
}

function openItem() {
    $("#ddlItemCategory").hide();
    $("#ddlItemType").hide();
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));

}

$("#txtItem").live("click", function () {
    openItem();
    return false;
});

$("#btnItem").live("click", function () {
    openItem();
    return false;
});
