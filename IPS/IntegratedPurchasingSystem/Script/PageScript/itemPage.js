﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });

    $("#ddlItemCategory").hide();
    $("#txtSearchItem").hide();
    $("#ddlItemType").hide();

    $("input[type=submit], button").button();

    $('#btnSaveItem').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });

    $('#btnSaveCategory').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });

});

$("#ddlItem").live("change",function () {
    var value = $('#ddlItem :selected').text();

    if (value === '-- Select --') {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").hide();
        $("#ddlItemType").hide();
    }
    else if (value === 'Category') {
        $("#ddlItemCategory").show();
        $("#txtSearchItem").hide();
        $("#ddlItemType").hide();
    }
    else if (value === 'Type') {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").hide();
        $("#ddlItemType").show();
    }
    else {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").show();
        $("#ddlItemType").hide();
    }

    $("#txtSearchItem").val("");
    $("#ddlItemCategory").val("");
    $("#ddlItemType").val("");

    return false;
});

function openDialogItem() {
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));
    return false;
}


function openEditItemDialog(id, name, category, type, desc) {
    $("#txtItemID").val(id);
    $("#txtItemName").val(name);
    $("#ddlCategory").val(category);
    $("#ddlType").val(type);
    $("#txtItemDesc").val(desc);

    openDialogItem();

}

function showMsg(titles, msg) {

    if ($("#dialogItem").dialog("isOpen") === true) {
        $("#dialogItem").dialog("close");
    }
    else if ($("#dialogCategory").dialog("isOpen") === true) {
        $("#dialogCategory").dialog("close");
    }

    $("#lblErrorMsg").text(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

$("#btnCancelItem").live("click", function () {
    $("#dialogItem").dialog("close");
    return false;
})


function openDialogCategory() {
    $("#dialogCategory").dialog({
        title: "Category",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogCategory").parent().appendTo($("form:first"));
    return false;
}

function openEditCategoryDialog(id, name, desc) {
    $("#txtCategoryID").val(id);
    $("#txtCategoryName").val(name);
    $("#txtCategoryDesc").val(desc);

    openDialogCategory();

}

