﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $("input[type=submit], button").button();

    $(function () {
        $(document).on('keypress', function (event) {
            if (event.keyCode == 13) {
                if ($("#dialogItem").dialog("isOpen") === true ) {
                    $("#btnSearchItem").click();
                    return false;
                }
                else if ($("#dialogUser").dialog("isOpen") === true) {
                    $("#btnSearchUser").click();
                    return false;
                }
                else if ($("#dialogJobCode").dialog("isOpen") === true) {
                    $("#btnSearchJobCode").click();
                    return false;
                }
                else {
                    closeLoading();
                    return false;
                }
            }

        });
    });
});

$("#txtSearchJobCode").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchJobCode").click();
    }
})

$("#txtUser").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchUser").click();
    }
})

$("#txtSearchItem").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchItem").click();
    }
})

//function onKeyPress() {
//    $(document).on('keyup keydown keypress', function (event) {
//        if (event.keyCode == 13) {
//            if ($("#dialogJobCode").dialog("isOpen"))
//            {
//                $("#btnSearchJobcode").click();
//            }
//            else if ($("#dialogUser").dialog("isOpen"))
//            {
//                $("#btnSearchUser").click();
//            }
//            else if ($("#dialogItem").dialog("isOpen"))
//            {
//                $("#btnSearchItem").click();
//            }
//        }
//    });
//}

function setButton() {
    $("input[type=submit], button").button();
}

function showErrorMsg(titles, error) {

    $("#lblErrorMsg").text(error);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function openDialogJobCode() {
    $("#dialogJobCode").dialog({
        title: "JobCode",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
   
    $("#dialogJobCode").parent().appendTo($("form:first"));
}

$("#btnJobCode").live("click", function () {
    openDialogJobCode();
    return false;
});

$("#txtJobCode").live("focus", function () {
    openDialogJobCode();
    return false;
})

$("#btnResetJobCode").live("click", function () {
    $("#txtJobCode").val("");
   
    return false;
})

function closeJobCode(jobcode) {
    $(function () {
        $("#dialogJobCode").parent().appendTo($("form:first"));
        $("#dialogJobCode").dialog("close");
    });
    
    $('#txtJobCode').val(jobcode);
    $('#txtSearchJobCode').val("");
}

function setCenter(dialogName) {
    $("#" + dialogName).dialog({
        position: ['center'],
    });
   
}

function openDialogUser() {
    $("#dialogUser").dialog({
        title: "Employee",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
       
    });
    
    $("#dialogUser").parent().appendTo($("form:first"));
    return false;
}

$("#txtRequestedBy").live("focus", function () {
    openDialogUser();
    return false;
})

$("#btnRequesteBy").live("click", function () {
    openDialogUser();
    return false;
});

$("#btnResetRequestedBy").live("click", function () {
    $("#txtRequestedBy").val("");
    
    return false;
})

function closeDialogUser(emp) {
    $("#dialogUser").dialog("close");
    $('#txtRequestedBy').val(emp);
}

function openItem() {
    $("#ddlItemCategory").hide();
    $("#ddlItemType").hide();
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));

}

$("#txtItem").live("click", function () {
    openItem();
    return false;
});

$("#btnItem").live("click", function () {
    openItem();
    return false;
});

function showItem(name) {
    if ($(name).hasClass("hidden"))
        $(name).removeClass("hidden");
}

function closeDialogItem(id, name) {
    $("#dialogItem").dialog("close");
    $("#txtIDItem").val(id);
    $("#txtItem").val(name);
}

$("#btnResetItem").live("click", function () {
    $("#txtIDItem").val("");
    $("#txtItem").val("");
    return false;
})

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});


function closeLoading() {
    if ($('.overlay').is(':visible'))
        $('.overlay').hide();
    if ($('.imgLoading').is(':visible'))
        $('.imgLoading').hide();
}
