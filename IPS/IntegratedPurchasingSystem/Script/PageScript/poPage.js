﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $('#btnSubmit').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });

    $("input[type=submit], button, input[type=button]").button();
    $("#txtDiscount").autoNumeric({ aSep: ',', aDec: '.' });
});

function setCenter(dialogName) {
    //setButton();

    $("#" + dialogName).dialog({
        width: 'auto',
    });
    $("#" + dialogName).dialog({
        position: ['center'],
    });
}

$('#ddlCurrency').live('change', function (event) {
        $("#btnGetExchangeRate").click();
});

function openDialogVendor() {
    $("#dialogVendor").dialog({
        title: 'Vendor',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
    });
    $("#dialogVendor").parent().appendTo($("form:first"));
    return false;
}

$("#txtVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnResetVendor").live("click", function () {
    $("#txtVendor").val("");
    return false;
})

function setButton() {
    $("input[type=submit], input[type=button], button").button();
    $("#txtDiscount").autoNumeric({ aSep: ',', aDec: '.' });
}

function closeDialogVendor() {
    //$("#lblVendorID").text(idVendor);
    //$("#txtVendor").val(vendorName);
    //$("#lblVendorAddress").html(address);
    //$("#lblVendorPIC").html(contact);
    setButton();
    $("#dialogVendor").dialog("close");
}

function gridItemOnEdit(quantity, metric, index) {
    $("input[id*='txtPrice']").autoNumeric({ aSep: ',', aDec: '.' });
    var pluralID = "#gridItem_ddlMetricPlural_" + index;
    var singularID = "#gridItem_ddlMetricSingular_" + index;

    if (parseFloat(quantity) > 1) {
        $(pluralID).show();
        $(singularID).hide();

        $(pluralID).val(metric);
    }
    else {
        $(singularID).show();
        $(pluralID).hide();

        $(singularID).val(metric);
    }
    $("#gridItem_txtPrice_" + index).autoNumeric({ aSep: ',', aDec: '.' });
    $("#gridItem_txtPrice_" + index).val($("#gridItem_txtPrice_" + index).val());
}

function resetMetric() {
    $(".metricPlural").hide();
    $(".metricSingular").hide();
}

function showMsg(titles, msg) {
    setButton();
    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function showException(titles, msg) {
    if ($("#dialogVendor").dialog("isOpen") === true) {
        $("#dialogVendor").dialog("close");
    }

    $("#lblErrorMsg").text(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

$("input[id*='txtQuantity']").live("change", function () {
    ddlMetricOnEdit($(this).attr('id'));
});

function ddlMetricOnEdit(id) {

    var index = id.substring(id.lastIndexOf("_") + 1, id.length)

    var pluralID = "#gridItem_ddlMetricPlural_" + index;
    var singularID = "#gridItem_ddlMetricSingular_" + index;

    var value = $("input[id*='txtQuantity']").val().replace(/,/g, '')

    if ($.isNumeric(value)) {

        if (parseFloat(value) > 1) {
            $(pluralID).show();
            $(singularID).hide();
        }
        else {
            $(singularID).show();
            $(pluralID).hide();
        }
    }
    else {
        $(pluralID).hide();
        $(singularID).hide();
    }
}

$("#ddlPaymentTerms").live("change", function () {
    var value = $('#ddlPaymentTerms :selected').val();

    if (value == 'Others') {
        if ($("#txtPayTermOthers").hasClass("hidden")) {
            $("#txtPayTermOthers").removeClass("hidden");
        }
    }
    else {
        if (!$("#txtPayTermOthers").hasClass("hidden"))
            $("#txtPayTermOthers").addClass("hidden");
    }
    return false;
})

function saveSuccess(titles, msg) {

    $("#lblMsg").html(msg);
    $("#divSuccess").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divSuccess").parent().appendTo($("form:first"));
    return false;
}

$("#divSuccess").live("dialogclose", function () {
    $("#btnReloadPage").click();
})

$("#ddlVAT").live("change", function () {
    var value = $('#ddlVAT :selected').text();
    //alert(value);
    if (value == 'Exclude') {
        var a = parseFloat($("#lblTotAmountOfItem").html().replace(/,/g, ''));
        //alert(a);
        if (!isNaN(a)) {
            $("#btnGenerateTotalAmount").click();
        }
    } else
        $("#btnGenerateTotalAmount").click();
})

$("#txtDiscount").live("change", function () {
    setButton();
    $("#btnGenerateTotalAmount").click();
})