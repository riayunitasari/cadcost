﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();
    
    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });

    $("#ddlItem").change(function () {
        var value = $('#ddlItem :selected').text();
        if (value === 'Type') {
            $("#txtSearchItem").hide();
            $("#ddlItemType").show();
        }
        else {
            $("#txtSearchItem").show();
            $("#ddlItemType").hide();
        }
        $("#dialogItem").dialog({
            position: ['center'],
        });
        $("#dialogItem").parent().appendTo($("form:first"));
        return false;
    });

    $('#btnSubmit').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });

    $("input[type=submit], input[type=button] , button").button();

    $("#txtExchangeRate").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewQuantity").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewPrice").autoNumeric({ aSep: ',', aDec: '.' });
    $("#input[id*='txtQuantity']").autoNumeric({ aSep: ',', aDec: '.' });
    $("input[id*='txtPrice']").autoNumeric({ aSep: ',', aDec: '.' });
    
    $(function () {
        $(document).on('keypress', function (event) {
            if (event.keyCode == 13) {
                if ($("#dialogItem").dialog("isOpen") === true && $("#dialogDetail").dialog("isOpen") === true) {
                    $("#btnSearchItem").click();
                    return false;
                }
                else if ($("#dialogDetail").dialog("isOpen") === true &&
                    $("#dialogItem").dialog("isOpen") === false) {
                    $("#btnSaveDetail").click();
                    return false;
                }
                else if ($("#dialogJobCode").dialog("isOpen") === true) {
                    $("#btnSearchJobCode").click();
                    return false;
                }
                else {
                    closeLoading();
                    return false;
                }
            }

        });
    });

})

$("input[id*='txtQuantity']").live("change", function () {
    ddlMetricOnEdit($(this).attr('id'));
});


$(function () {
  
    $(".metricSingular").show();
    $(".metricPlural").hide();

});

$("#txtSearchItem").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchItem").click();
    }
})

$("#txtSearchJobCode").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchJobCode").click();
    }
})

$("#txtNewQuantity").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSaveDetail").click();
    }
})

$("#txtNewPrice").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSaveDetail").click();
    }
})

$("#txtNewRemark").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSaveDetail").click();
    }
})

function openItem() {
    $("#ddlItemCategory").hide();
    $("#ddlItemType").hide();
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));

}


function addCommas(x) {
    var parts;
    var retVal;
    if (x.indexOf('.') !== -1) {
        var parts = x.toString().split(".");

        parts[0] = parts[0] ? parseFloat(parts[0].replace(/,/g, '')) : 0;
        parts[0] = parts[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        if (parts.length > 2)
                return parts[0]+"."+parts[1];
        else
            return parts.join(".");
    }
    else {
        retVal = x ? parseFloat(x.replace(/,/g, '')) : 0;
        return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

function showErrorMsg(titles, error) {

    if ($("#dialogItem").dialog("isOpen") === true) {
        $("#dialogItem").dialog("close");
    }
    if ($("#dialogJobCode").dialog("isOpen") === true) {
        $("#dialogJobCode").dialog("close");
    }
    if ($("#dialogDetail").dialog("isOpen") === true) {
        $("#dialogDetail").dialog("close");
    }

    $("#lblErrorMsg").text(error);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function ddlMetricOnNew() {
    var value = $("#txtNewQuantity").val().replace(/,/g, '');
    if ($.isNumeric(value)) {
        if (parseFloat(value) > 1) {
            $(".metricPlural").show();
            $(".metricSingular").hide();
        }
        else {
            $(".metricSingular").show();
            $(".metricPlural").hide();
        }
    }
    else {
        $(".metricPlural").hide();
        $(".metricSingular").hide();
    }
    
}


function ddlMetricOnEdit(id) {
    
    var index = id.substring(id.lastIndexOf("_")+1,id.length)

    var pluralID = "#gridItem_ddlMetricPlural_" + index;
    var singularID = "#gridItem_ddlMetricSingular_" + index;

    var value = $("input[id*='txtQuantity']").val().replace(/,/g, '')

    if ($.isNumeric(value)) {

        if (parseFloat(value) > 1) {
            $(pluralID).show();
            $(singularID).hide();
        }
        else {
            $(singularID).show();
            $(pluralID).hide();
        }
    }
    else {
        $(pluralID).hide();
        $(singularID).hide();
    }
}

function setDDLMetricOnEdit(quantity, metric, index) {

    var pluralID = "#gridItem_ddlMetricPlural_" + index;
    var singularID = "#gridItem_ddlMetricSingular_" + index;

    if (parseFloat(quantity) > 1) {
        $(pluralID).show();
        $(singularID).hide();

        $(pluralID).val(metric);
    }
    else {
        $(singularID).show();
        $(pluralID).hide();

        $(singularID).val(metric);
    }
    var priceVal = addCommas($("#gridItem_txtPrice_" + index).val());
    var quanVal = addCommas($("#gridItem_txtQuantity_" + index).val());
    $("#gridItem_txtPrice_" + index).val(priceVal);
    $("#gridItem_txtQuantity_" + index).val(quanVal);
}

function closeDialogAddDetail() {
    $("#dialogDetail").dialog("close");
    ddlMetricOnNew();
}

function closeDialogItem() {
    $("#dialogItem").dialog("close");
    $("#txtNewQuantity").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewPrice").autoNumeric({ aSep: ',', aDec: '.' });
    ddlMetricOnNew();
}

$("#dialogItem").live("dialogclose", function () {
    ddlMetricOnNew();
});

$("#dialogDetail").live("dialogclose", function () {
    ddlMetricOnNew();
    $("#txtNewItem").val("");
    $("#txtNewQuantity").val("");
    $("#txtNewPrice").val("");
    $("#txtNewRemark").val("");
    
});

function openDialogJobCode() {
    $("#dialogJobCode").dialog({
        title: "JobCode",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogJobCode").parent().appendTo($("form:first"));
}

$("#btnJobCode").live("click", function () {
    openDialogJobCode();
    return false;
});

$("#txtJobCode").live("focus", function () {
    openDialogJobCode();
    return false;
})

function setCenter(dialogName) {
    $("#"+dialogName).dialog({
        position: ['center'],
    });
}

$("#btnResetJobCode").live("click", function () {
    $("#txtJobCode").val("");
    return false;
})


function closeJobCode(jobcode, name) {
    $(function () {
        $("#dialogJobCode").parent().appendTo($("form:first"));
        $("#dialogJobCode").dialog("close");
    });

    $('#lblProjectName').text(name);
    $('#txtJobCode').validationEngine("hide");
    $('#txtJobCode').val(jobcode);
    $('#txtSearchJobCode').val("");
    
}

function saveSuccess(msg) {
    $(".ui-dialog-content").dialog("close");

    $('#lblMsg').text(msg);
    $("#divSuccess").dialog({
        title: "Success!",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        closeOnEscape: false,
        buttons: {
            OK: function () {
                $(this).dialog("close");
               // $('#btnReloadPage').click();
               // return false;
            }
        },
        create: function (event, ui) { },
    });

    $("#divSuccess").parent().appendTo($("form:first"));
}

$('#divSuccess').live('dialogclose', function (event) {
    $('#btnHiddenSave').click();
});


$('#ddlCurrency').live('change', function (event) {
    var value = $('#ddlCurrency :selected').val();
    if (value === 'IDR') {
        $('#txtExchangeRate').val('1');
        $('#txtExchangeRate').attr("disabled", "disabled");
        if ($('#txtExchangeRate').hasClass("validate[required]")) {
            $('#txtExchangeRate').validationEngine("hide");
            $('#txtExchangeRate').removeClass("validate[required]");
        }
    } else {
        $("#txtExchangeRate").removeAttr("disabled");
        $('#txtExchangeRate').addClass("validate[required]");
    }
    $('#lblCurrencyEst').text($('#ddlCurrency :selected').val());
});

$('#btnAddItem').live('click', function (event) {
    $("#txtNewQuantity").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewPrice").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtNewItem").val("");
    $("#txtNewQuantity").val("");
    $("#ddlMetricPlural").val("");
    $("#ddlMetricSingular").val("");
    $("#txtNewPrice").val("");
    $("#txtNewRemark").val("");
    hideItem("#gridItemSearch");

    ddlMetricOnNew();

    $("#dialogDetail").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogDetail").parent().appendTo($("form:first"));
    $("#btnSaveDetail").focus();
    return false;
});

$("#txtNewItem").live("click", function () {
    openItem();
    return false;
});

$("#txtNewQuantity").live("change", function () {
    ddlMetricOnNew();
});

$("#btnCancelDetail").live("click", function () {
    closeDialogAddDetail();
    return false;
})

function hideItem(name) {
    if (!$(name).hasClass("hidden"))
    $(name).addClass("hidden");
}

function showItem(name) {
    if ($(name).hasClass("hidden"))
        $(name).removeClass("hidden");
}

$('input[type="file"]').live("change", function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'pdf':
            $('#btnSubmit').attr('disabled', false);
            break;
        default:
            alert('This is not an allowed file type.');
            this.value = '';
    }
});

$('#linkReport').live('click', function () {
    openDialogPrint();
    if ($('#lblGAReqNo').val('None'))

    return false;
})

$('#btnPrint').live('click', function () {
    $("#dialogPrint").dialog('close');
})

function openDialogPrint() {
    $("#dialogPrint").dialog({
        title: "Print Requisition",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'], 
        create: function (event, ui) { }
    });
    $("#dialogPrint").parent().appendTo($("form:first"));
}



function setButton() {
    $("input[type=submit], input[type=button], button").button();
}

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});


function closeLoading() {
    $('.overlay').hide();
    $('.imgLoading').hide();
}

function GotoDownloadPage()
{
    window.location = "FileDownload.aspx";
}

function downloadFile() {
    closeLoading();
    $("#btnDownload").click();
}

