﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });
    $("input[type=submit], input[type=button], button").button();

    $(function () {
        attachDate();
    });
    $("#txtQuoPrice").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtDiscount").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtExchangeRate").autoNumeric({ aSep: ',', aDec: '.' });

    $(function () {
        $(document).on('keypress', function (event) {
            if (event.keyCode == 13) {
                if ($("#dialogVendor").dialog("isOpen") === true && $("#dialogDetail").dialog("isOpen") === true) {
                    $("#btnSearchVendor").click();
                    return false;
                }
                else if ($("#dialogDetail").dialog("isOpen") === true &&
                    $("#dialogVendor").dialog("isOpen") === false) {
                    $("#btnSaveDetail").click();
                    return false;
                }
                else if ($("#dialogItem").dialog("isOpen") === true) {
                    $("#btnSearchItem").click();
                    return false;
                }
                else {
                    closeLoading();
                    return false;
                }
            }

        });
    });


});

$("#txtSearchVendor").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchVendor").click();
    }
})

$("#txtSearchItem").live("keypress", function () {
    if (event.keyCode == 13) {
        $("#btnSearchItem").click();
    }
})

$("#btnSubmit").live("click", function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
        if ($("#form1").validationEngine('validate') == false)
            return false;
    });

function setButton() {
    $("input[type=submit], input[type=button], button").button();
    $("#txtQuoPrice").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtDiscount").autoNumeric({ aSep: ',', aDec: '.' });
    $("#txtExchangeRate").autoNumeric({ aSep: ',', aDec: '.' });
}

function openDialogDetail() {
    setButton();

    if (!$("#txtPayTermOthers").hasClass("hidden")) {
        $("#txtPayTermOthers").val("");
        $("#txtPayTermOthers").addClass("hidden");
    }
    if (!$("#txtVATAmount").hasClass("hidden")) {
        $("#txtVATAmount").val("");
        $("#txtVATAmount").addClass("hidden");
    }
    

    $("#dialogDetail").dialog({
        title: 'Quotation - Vendor',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
    });
    $("#btnSaveDetail").focus();
    $("#txtWarranty").focus();
    $("#dialogDetail").parent().appendTo($("form:first"));
    //return false;
}


function openDialogVendor() {
    $("#dialogVendor").dialog({
        title: 'Quotation - Vendor',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
    });
    $("#dialogVendor").parent().appendTo($("form:first"));
    return false;
}

function closeAllDialog() {
    if ($("#dialogItem").dialog("isOpen") === true) {
        $("#dialogItem").dialog("close");
    }

    if ($("#dialogVendor").dialog("isOpen") === true) {
        $("#dialogVendor").dialog("close");
    }

    if ($("#dialogDetail").dialog("isOpen") === true) {
        $("#dialogDetail").dialog("close");
    }

    if ($("#dialogJobCode").dialog("isOpen") === true) {
        $("#dialogJobCode").dialog("close");
    }

    if ($("#dialogRequisition").dialog("isOpen") === true) {
        $("#dialogRequisition").dialog("close");
    }

    if ($("#dialogItemReq").dialog("isOpen") === true) {
        $("#dialogItemReq").dialog("close");
    }
}

function showMsg(titles, msg) {
    setButton();
    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

function showException(titles, msg) {
    closeAllDialog();
    $("#lblErrorMsg").text(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}

$("#btnAddVendor").live("click", function () {
    openDialogDetail();
    attachDate();
    return false;
})

$("#ddlRequestor").live("change", function () {
    var value = $('#ddlRequestor :selected').val();
    if (value == 'Others') {
        if ($("#txtRequestor").hasClass("hidden"))
            $("#txtRequestor").removeClass("hidden");
    }
    else {
        if (!$("#txtRequestor").hasClass("hidden"))
            $("#txtRequestor").addClass("hidden");
    }
        //return false;
})

$("#txtVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnVendor").live("click", function () {
    openDialogVendor();
    return false;
})

$("#btnResetVendor").live("click", function () {
    $("#txtVendor").val("");
    return false;
})

function setCenter(dialogName) {
    setButton();
    
    $("#" + dialogName).dialog({
        width: 'auto',
    });
    $("#" + dialogName).dialog({
        position: ['center'],
    });
}

function closeDialogVendor(idVendor, vendorName) {
    $("#txtVendorID").val(idVendor);
    $("#txtVendor").val(vendorName);
    $("#dialogVendor").dialog("close");
}

$("#dialogVendor").live("dialogclose", function () {
    setButton();
    attachDate();
})

$("#dialogVendor").live("dialogopen", function () {

});

$("#divError").live("dialogclose", function () {
    setButton();
    attachDate();
});

function attachDate() {
    $("#txtQuoDate").datepicker({
        numberOfMonths: 1
    });
};

$("#ddlPaymentTerms").live("change", function () {
    var value = $('#ddlPaymentTerms :selected').val();
    
    if (value == 'Others') {
        if ($("#txtPayTermOthers").hasClass("hidden")) {
            $("#txtPayTermOthers").removeClass("hidden");
        }
    }
    else {
        if (!$("#txtPayTermOthers").hasClass("hidden"))
            $("#txtPayTermOthers").addClass("hidden");
    }
    return false;
})

$("#ddlVAT").live("change", function () {
    var value = $('#ddlVAT :selected').text();
    //alert(value);
    if (value == 'Exclude') {
        var a = parseFloat($("#txtQuoPrice").val().replace(/,/g, ''));
        
        if (!isNaN(a)) {
            $("#btnGeneratePrice").click();
        }
        else
            showMsg('Error', 'Please input quoted price correctly!');
    }else
        $("#btnGeneratePrice").click();
})


function openDialogRequisition() {
    $("#dialogRequisition").dialog({
        title: "Requisition",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogRequisition").parent().appendTo($("form:first"));
    return false;
}

$("#dialogItemReq").live("dialogopen", function () {
    $("#dialogRequisition").dialog("close");
})

function openDialogItemReq() {
    $("#dialogItemReq").dialog({
        title: "Requisition Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
        }
    });
    $("#dialogItemReq").parent().appendTo($("form:first"));
    return false;
}

function openDialogJobCode() {
    $("#dialogJobCode").dialog({
        title: "JobCode",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogJobCode").parent().appendTo($("form:first"));
}

function closeJobCode(jobcode) {
    $(function () {
        $("#dialogJobCode").parent().appendTo($("form:first"));
        $("#dialogJobCode").dialog("close");
    });
    $('#txtJobCodeReq').val(jobcode);
    $("#dialogRequisition").dialog({ width: 'auto' });
    setCenter('dialogRequisition');
    $('#txtSearchJobCode').val("");
}

$("#btnJobCodeReq").live("click", function () {
    openDialogJobCode();
    return false;
})

$("#txtJobCodeReq").live("focus", function () {
    openDialogJobCode();
    return false;
})

$("#btnResetJobCodeReq").live("click", function () {
    $("#txtJobCodeReq").val("");
    return false;
})

function openDialogItem() {
    $("#ddlItemType").hide();
    $("#dialogItem").dialog({
        title: "Item",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogItem").parent().appendTo($("form:first"));

}

$("#btnItem").live("click", function () {
    openDialogItem();
    return false;
})

$("#txtItem").live("focus", function () {
    openDialogItem();
    return false;
})

$("#btnResetItem").live("click", function () {
    $("#txtItem").val("");
    return false;
})

function closeDialogItem(itemID, itemName) {
    $("#dialogItem").dialog("close");
}

$("#ddlItem").live("change", function () {
    var value = $('#ddlItem :selected').text();

    if (value === '-- Select --') {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").hide();
        $("#ddlItemType").hide();
    }
    else if (value === 'Category') {
        $("#ddlItemCategory").show();
        $("#txtSearchItem").hide();
        $("#ddlItemType").hide();
    }
    else if (value === 'Type') {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").hide();
        $("#ddlItemType").show();
    }
    else {
        $("#ddlItemCategory").hide();
        $("#txtSearchItem").show();
        $("#ddlItemType").hide();
    }

    $("#txtSearchItem").val("");
    $("#ddlItemCategory").val("");
    $("#ddlItemType").val("");

    return false;
});


$("#dialogRequisition").live("dialogclose", function () {
    setButton();
})

$("#dialogItemReq").live("dialogclose", function () {
    setButton();
})

function closeItemReq() {
    $("#lblIDReq").text("");
    $("#dialogItemReq").dialog("close");
    $("#dialogRequisition").dialog("close");
    
};

$("#btnCancelItemReq").live("click", function () {
    closeItemReq();
})

function attachDateOnGrid(index) {
    var txtDate = "#gridSelectedVendor_txtDate_" + index;
    $(txtDate).datepicker({
        numberOfMonths: 1
    });
}

function dialogDetailOnEdit(id, idVendor, vendorName, quoNo, date, techAspect,
    quoPrice, dics, vat, vatAmount, netPrice, currency, delTime, payTerms, warranty) {
    
    //setButton();
    //attachDate();

    $("#txtIDDetail").val(id);
    $("#txtVendorID").val(idVendor);
    $("#txtVendor").val(vendorName);
    $("#txtQuotationNo").val(quoNo);

    if (date != '01/01/1753')
        $("#txtQuoDate").datepicker("setDate", date);
    else
        $("#txtQuoDate").val('');
    $("#txtTechAspect").val(techAspect);
    $("#txtQuoPrice").val(quoPrice);
    $("#txtDiscount").val(dics);
    $("#ddlVAT").val(vat);
    $("#lblNetPrice").text(netPrice);
    $("#ddlCurrency").val(currency);
    $("#txtDeliveryTime").val(delTime);
    
    if (payTerms != 'Cash On Delivery' && payTerms != '14 days' && payTerms != '30 days') {
        if ($("#txtPayTermOthers").hasClass("hidden")) {
            $("#txtPayTermOthers").removeClass("hidden");
        }
        $("#txtPayTermOthers").val(payTerms);
        payTerms = 'Others';
    }
    else {
        if (!$("#txtPayTermOthers").hasClass("hidden"))
            $("#txtPayTermOthers").addClass("hidden");
    }
    $("#ddlPaymentTerms").val(payTerms);
    $("#txtWarranty").val(warranty);
    //openDialogDetail();
};

function editDialogDetail(date, payTerms, exchangeRate) {

    if (date != '01/01/1753')
        $("#txtQuoDate").datepicker("setDate", date);
    else
        $("#txtQuoDate").val('');
 
    if (payTerms != 'Cash On Delivery' && payTerms != '14 days' && payTerms != '30 days') {
        if ($("#txtPayTermOthers").hasClass("hidden")) {
            $("#txtPayTermOthers").removeClass("hidden");
        }
        $("#txtPayTermOthers").val(payTerms);
        payTerms = 'Others';
    }
    else {
        if (!$("#txtPayTermOthers").hasClass("hidden"))
            $("#txtPayTermOthers").addClass("hidden");
    }
    setExchangeRate();
};

function closeDialogDetail() {
    $("#dialogDetail").dialog("close");
}

//$("#dialogDetail").live("dialogopen", function () {
//})

$("#dialogDetail").live("dialogclose", function () {
    $("#txtIDDetail").val("");
    $("#txtVendorID").val("");
    $("#txtVendor").val("");
    $("#txtQuotationNo").val("");
    $("#txtQuoDate").val("");
    $("#txtTechAspect").val("");
    $("#txtQuoPrice").val("");
    $("#txtDiscount").val("");
    $("#ddlVAT").val("");
    if (!$("#txtVATAmount").hasClass("hidden"))
        $("#txtVATAmount").val("");
    $("#lblNetPrice").text("");
    $("#ddlCurrency").val("");
    $("#txtDeliveryTime").val("");
    if (!$("#txtPayTermOthers").hasClass("hidden"))
        $("#txtPayTermOthers").val("");
    $("#ddlPaymentTerms").val("");
    $("#txtWarranty").val("");
    $("#txtExchangeRate").val("");
    if ($('#txtQuoDate').is(':disabled'))
        $('#txtQuoDate').removeAttr("disabled");
    if ($('#txtTechAspect').is(':disabled'))
        $('#txtTechAspect').removeAttr("disabled");
    if ($('#txtQuoPrice').is(':disabled'))
        $('#txtQuoPrice').removeAttr("disabled");
    if ($('#txtDiscount').is(':disabled'))
        $('#txtDiscount').removeAttr("disabled");
    if ($('#ddlVAT').is(':disabled'))
        $('#ddlVAT').removeAttr("disabled");
    if ($('#ddlCurrency').is(':disabled'))
        $('#ddlCurrency').removeAttr("disabled");
    if ($('#ddlPaymentTerms').is(':disabled'))
        $('#ddlPaymentTerms').removeAttr("disabled");
    if ($('#txtWarranty').is(':disabled'))
        $('#txtWarranty').removeAttr("disabled");
    if ($('#txtExchangeRate').is(':disabled'))
        $('#txtExchangeRate').removeAttr("disabled");
    if ($('#txtDeliveryTime').is(':disabled'))
        $('#txtDeliveryTime').removeAttr("disabled");

    setButton()
})

$("#txtQuoPrice").live("change", function () {
    setButton();
    $("#btnGeneratePrice").click();
    //alert($("#txtQuoPrice").val());
})

$("#txtDiscount").live("change", function () {
    setButton();
    $("#btnGeneratePrice").click();
})

$("#btnCancelDetail").live("click", function () {
    $("#dialogDetail").dialog("close");
    return false;
})

function saveSuccess(titles, msg) {

    $("#lblMsg").html(msg);
    $("#divSuccess").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divSuccess").parent().appendTo($("form:first"));
    return false;
}

$("#divSuccess").live("dialogclose", function () {
    $("#btnReloadPage").click();
})

function clickGeneratePrice() {
    $("#btnGeneratePrice").click();
}

function setExchangeRate() {
    var value = $('#ddlCurrency :selected').val();
    if (value === 'IDR') {
        $('#txtExchangeRate').val('1');
        $('#txtExchangeRate').attr("disabled", "disabled");
        if ($('#txtExchangeRate').hasClass("validate[required]")) {
            $('#txtExchangeRate').validationEngine("hide");
            $('#txtExchangeRate').removeClass("validate[required]");
        }
    } else {
        $("#txtExchangeRate").removeAttr("disabled");
        $('#txtExchangeRate').addClass("validate[required]");
    }
    $('#lblCurrencyEst').text($('#ddlCurrency :selected').val());
}

$('#ddlCurrency').live('change', function (event) {
    setExchangeRate();
});

$('#linkPrint').live('click', function () {
    $("#dialogPrint").dialog({
        title: 'Quotation - Print',
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogPrint").parent().appendTo($("form:first"));
    return false;
})

function closeDialogPrint() {
    $("#dialogPrint").dialog("close");
   // return false;
}

$('form').live("submit", function () {
    ShowProgress();
    //return false;
});

function ShowProgress() {
    setTimeout(function () {
        var modal = $('<div />');
        modal.addClass("overlay");
        $('body').append(modal);
        var loading = $(".imgLoading");
        loading.show();
        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        loading.css({ top: top, left: left });

    }, 1);
}

function closeLoading() {
    $('.overlay').hide();
    $('.imgLoading').hide();
}

function downloadFile() {
    closeDialogPrint();
    closeLoading();
    $("#btnDownloadDoc").click();
}

$('form').live("submit", function () {
    ShowProgress();
});
