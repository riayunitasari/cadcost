﻿$(document).ready(function () {
    $('.menu').jqsimplemenu();

    $('.contentPanelExpand').click(function () {
        $('.contentPanelExpand').attr('disabled', 'disabled');
        $(this).parent().parent().children().last().toggle('blind', 500, function () {
            $('.contentPanelExpand').removeAttr('disabled');
        });
    });

    $("input[type=submit], button").button();

    $('#btnSaveVendor').click(function () {
        $("#form1").validationEngine('attach', { promptPosition: "topRight" });
    });
});


function openDialogNewVendor() {
    
    $("#txtVendorID").val("");
    $("#txtVendorCode").val("");
    $("#txtVendorName").val("");
    $("#txtVendorAddress").val("");
    openDialogVendor();
    return false;
};

function openDialogVendor() {
    $("#dialogVendor").dialog({
        title: "Vendor",
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { }
    });
    $("#dialogVendor").parent().appendTo($("form:first"));
    return false;
}

function closeDialogVendor() {
    $("#txtVendorID").val("");
    $("#txtVendorCode").val("");
    $("#txtVendorName").val("");
    $("#txtVendorAddress").val("");
    $("#dialogVendor").dialog("close");
}

function openDialogEditVendor(id, code, name, address) {
    $("#txtVendorID").val(id);
    $("#txtVendorCode").val(code);
    $("#txtVendorName").val(name);
    $("#txtVendorAddress").val(address);
    openDialogVendor();
}


function showMsg(titles, msg) {

    $("#lblErrorMsg").html(msg);
    $("#divError").dialog({
        title: titles,
        modal: true,
        resizable: false,
        width: 'auto',
        position: ['center'],
        create: function (event, ui) { },
        buttons: {
            OK: function () {
                $(this).dialog("close");
                return false;
            }
        },
    });
    $("#divError").parent().appendTo($("form:first"));
    return false;
}
