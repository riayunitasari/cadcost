﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_create.aspx.cs" Inherits="CADcost.CADcost_create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <asp:Table ID="TableCreateCADcost" runat="server" Width="100%" GridLines="Horizontal" ForeColor="Black" CellPadding="10" CssClass="table table-striped table-hover">
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">CAD Cost Request NO : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelCADcostNO" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">Requester : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelRequester" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Department : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:Label ID="LabelDepartment" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">Project : </asp:TableCell>
            <asp:TableCell >
                <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                    <ContentTemplate>
                <asp:DropDownList ID="DropDownListProject" runat="server" VerticalAlign="Middle" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                <asp:Label ID="LabelProject" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="Button1" runat="server" Text="..." CssClass="btn btn-warning" OnClick="ButtonSearchJobcode_Click"/>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">For Month : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="DropDownListMonth" runat="server" CssClass="form-control"></asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">For Year : </asp:TableCell>
            <asp:TableCell ColumnSpan="2"><asp:DropDownList ID="DropDownListYear" runat="server" CssClass="form-control"></asp:DropDownList></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC" HorizontalAlign="Right" VerticalAlign="Middle">
                Item :
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                 <asp:GridView ID="GridViewItem" runat="server" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#999999" HorizontalAlign="Center" Font-Size="Small" ShowFooter="True" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" OnDataBound="GridViewItem_DataBound"  Width="90%" OnRowEditing="GridViewItem_RowEditing" OnRowUpdating="GridViewItem_RowUpdating" OnRowDeleting="GridViewItem_RowDeleting" OnRowDataBound="GridViewItem_RowDataBound" OnRowCancelingEdit="GridViewItem_RowCancelingEdit" RowStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="Black" Font-Names="Trebuchet MS">


        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>

        
        <Columns>
            <asp:TemplateField HeaderText="No.">
                <FooterTemplate>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary"/>
                </FooterTemplate>
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Software Name">
                <ItemTemplate>

                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>' Width="80%"></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameEdit" runat="server" Width="80%" CssClass="form-control"></asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="DropDownListSoftwareNameAdd" runat="server" Width="80%" CssClass="form-control"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <ItemTemplate>

                        <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>

                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBoxQuantityEdit" runat="server" Text='<%# Eval("QUANTITY") %>' Width="80%" CssClass="form-control input-sm"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="TextBoxQuantityAdd" runat="server" Width="80%"  CssClass="form-control input-sm"></asp:TextBox>
                    </FooterTemplate>

                </asp:TemplateField>
            <asp:TemplateField HeaderText="Remark">
                <ItemTemplate>
                    <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxRemarkEdit" runat="server" Text='<%# Eval("REMARK") %>'  CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxRemarkAdd" runat="server" Text='<%# Eval("REMARK") %>' Width="100%"  CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdate" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>

                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
            <FooterStyle BackColor="#666666" />

    <HeaderStyle BackColor="#999999" Font-Size="Larger" ForeColor="Black" HorizontalAlign="Center"></HeaderStyle>

<RowStyle HorizontalAlign="Center"></RowStyle>
<FooterStyle HorizontalAlign="Center" />
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    
   
        <asp:TableRow>
            <asp:TableCell BackColor="#CCCCCC"  HorizontalAlign="Right" Width="30%" VerticalAlign="Middle">
                Description:
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:TextBox ID="TextBoxDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                <asp:Label ID="LabelDescription" runat="server" Visible="false"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="3" HorizontalAlign="Center">
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" class="btn btn-primary" OnClick="ButtonSubmit_Click"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonSaveAsDraft" runat="server" Text="SaveAsDraft" class="btn btn-warning" OnClick="ButtonSaveAsDraft_Click"/>
                </span>
                <span class="button-border button-radius-50 p1">
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="ButtonCancel_Click"/>
                </span>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div id="dialogSummary">
        <asp:Panel ID="PanelSummary" runat="server" Visible="false">
                <asp:Table ID="Table1" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Summary</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewItemSummary" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment: <asp:TextBox ID="TextBoxComment" runat="server"></asp:TextBox></h5>
                        </asp:TableCell>
                    </asp:TableRow>
            
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                                <asp:Button runat="server" class="btn btn-success" Text="OK" ID="ButtonConfirmOK" OnClick="ButtonConfirmOK_Click"/>
                                <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonConfirmCancel" OnClick="ButtonConfirmCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
              </asp:Panel>  
    </div>
     <div id="dialogProject">
        <asp:Panel ID="PanelProject" runat="server" Visible="false">
            <asp:Table ID="Table2" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass=""></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" CssClass="table-hover" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
    </div>
</asp:Content>
