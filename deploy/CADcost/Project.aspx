﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Project.aspx.cs" Inherits="CADcost.Project" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
    <legend>List of Active Projects</legend>
    <asp:GridView ID="GridViewProject" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="True" OnRowEditing="GridViewProject_RowEditing" OnRowDeleting="GridViewProject_RowDeleting" OnRowUpdating="GridViewProject_RowUpdating" OnRowCancelingEdit="GridViewProject_RowCancelingEdit" HorizontalAlign="Center" Width="80%">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddProject" runat="server" Text="Add" OnClick="ButtonAddProject_Click" CssClass="btn btn-primary"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Project Name">
                <ItemTemplate>
                    <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxProjectNameAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Jobcode">
                <ItemTemplate>
                    <asp:Label ID="LabelJobcode" runat="server" Text='<%# Eval("JOBCODE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxJobcode" runat="server" Text='<%# Eval("JOBCODE") %>' CssClass="form-control input-sm" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxJobcodeAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateCost" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelCost" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditCost" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteCost" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#999999" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="#666666" />
    </asp:GridView>
    </div>
</asp:Content>
