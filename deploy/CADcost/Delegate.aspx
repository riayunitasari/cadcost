﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Delegate.aspx.cs" Inherits="CADcost.Delegate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h5>Set Delegate</h5>
    <asp:Table runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <h6>Employee to be delegated :</h6>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBoxEmployeeDelegated" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <h6>Set delegate to:</h6>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBoxEmployeeDelegating" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="ButtonSetDelegate" runat="server" Text="Set Delegate" OnClick="ButtonSetDelegate_Click"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    
    <div id="dialogDelegate">
        <asp:UpdatePanel runat="server" ID="PanelDelegate" Visible="false" >
            <ContentTemplate>
                <asp:Table ID="Table3" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Set Delegation</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate for:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegated" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Set Delegate to:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="LabelDelegating" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Proceed?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="button-green button-radius-50" Text="Set Delegate" ID="ButtonDelegateOK" OnClick="ButtonDelegateOK_Click"/>
                            <asp:Button runat="server" class="button-red button-radius-50" Text="Cancel" ID="ButtonDelegateCancel" OnClick="ButtonDelegateCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </ContentTemplate>
                
        </asp:UpdatePanel>
    </div>
</asp:Content>
