﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_cost.aspx.cs" Inherits="CADcost.Software_cost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <div class="well ">
    <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
        
    <legend>Last Update Software Cost</legend>
    <asp:GridView ID="GridViewSoftwareCost" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" ShowFooter="true" OnDataBound="GridViewSoftwareCost_DataBound" OnRowEditing="GridViewSoftwareCost_RowEditing" OnRowDeleting="GridViewSoftwareCost_RowDeleting" OnRowUpdating="GridViewSoftwareCost_RowUpdating" OnRowCancelingEdit="GridViewSoftwareCost_RowCancelingEdit" HorizontalAlign="Center" Width="80%">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="ButtonAddCost" runat="server" Text="Add" OnClick="ButtonAddCost_Click" CssClass="btn btn-primary"/>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Software">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDCost" runat="server"  Value='<%# Eval("ID_COST") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="DropDownListSoftwareAdd" runat="server" CssClass="form-control"></asp:DropDownList>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelCADCost" runat="server" Text='<%# Eval("CAD_COST") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDCostEdit" runat="server"  Value='<%# Eval("ID_COST") %>'/>
                    <asp:TextBox ID="TextBoxCADCost" runat="server" Text='<%# Eval("CAD_COST") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxCADCostAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="REQ Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelREQCost" runat="server" Text='<%# Eval("REQ_COST") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxREQCost" runat="server" Text='<%# Eval("REQ_COST") %>' CssClass="form-control input-sm"></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="TextBoxREQCostAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateCost" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelCost" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditCost" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteCost" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#999999" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="#666666" />
    </asp:GridView>
    <legend>Software Cost History</legend>
    <asp:DropDownList runat="server" id="DropDownListSoftware" OnSelectedIndexChanged="DropDownListSoftware_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
    <asp:GridView ID="GridViewSoftwareCostHistory" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Yet" HorizontalAlign="Center" Width="50%">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Update">
                <ItemTemplate>
                    <asp:Label ID="LabelDateUpdate" runat="server" Text='<%# Eval("CAD_STARTDATE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelCADCost" runat="server" Text='<%# Eval("CAD_COST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="REQ Cost">
                <ItemTemplate>
                    <asp:Label ID="LabelREQCost" runat="server" Text='<%# Eval("REQ_COST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#999999" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
        <FooterStyle BackColor="#666666" />
    </asp:GridView>
    </div>
</asp:Content>
