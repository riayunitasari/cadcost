﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="CADcost.Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
    <div class="well ">
    <legend><h2>Print CAD Cost Evidence</h2></legend>
    <asp:Table ID="Table1" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                Project :
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="DropDownListProject" runat="server" Width="80%"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Month :
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="DropDownListMonthCADcost" runat="server" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Year :
            </asp:TableCell>
            <asp:TableCell>
                 <asp:DropDownList ID="DropDownListYearCADcost" runat="server" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell >
                <asp:Button ID="ButtonCreateCADcostEvidence" runat="server" Text="Generate Excel" OnClick="ButtonCreateCADcostEvidence_Click"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:ImageButton ID="ImageFileCADcostEvidence" runat="server" OnClick="ImageFileCADcostEvidence_Click" Visible="false" ImageUrl="~/images/excel.png"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-6">
    <div class="well ">
    <legend><h2>Print JIND Using Software</h2></legend>
    <asp:Table ID="Table2" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                From :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListStartMonth" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListStartYear" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                To :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListEndMonth" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListEndYear" runat="server" Width="100%" OnSelectedIndexChanged="DropDownListStartMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Highlighted :
            </asp:TableCell>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListHighlightMonth" runat="server" Width="100%"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListHighlightYear" runat="server" Width="100%"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell >
                <asp:Button ID="ButtonCreateJINDusingSoftware" runat="server" Text="Generate Excel" OnClick="ButtonCreateJINDusingSoftware_Click"/>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2">
                <asp:ImageButton ID="ImageButtonJINDusingSoftware" runat="server" OnClick="ImageFileButtonJINDusingSoftware_Click" Visible="false" ImageUrl="~/images/excel.png"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        <div class="col-lg-6">
    <div class="well ">
    <legend><h2>Print Integraph License Distribution</h2></legend>
    <asp:Table ID="Table3" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                Month: <asp:DropDownList ID="DropDownListMonthIntegraph" runat="server" Width="100%"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                Year: <asp:DropDownList ID="DropDownListYearIntegraph" runat="server" Width="100%"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:Button ID="ButtonIntegraphLicenseRequest" runat="server" Text="Generate Excel" OnClick="ButtonIntegraphLicenseRequest_Click"/>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:ImageButton ID="ImageButtonIntegraphLicenseRequest" runat="server" OnClick="ImageButtonIntegraphLicenseRequest_Click" Visible="false" ImageUrl="~/images/excel.png"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
            </div>
        </div>
</asp:Content>
