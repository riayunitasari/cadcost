﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Beranda.aspx.cs" Inherits="CADcost.Beranda" EnableEventValidation="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/stylesheet">
        #contents {
        color: black;
        }

        .header-center{
          text-align:center !important;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-danger">
    <div class="overlow post-holder-sm extra">
    <div class="date"> 
                <span class="day"><asp:Label ID="lblDate" runat="server"></asp:Label></span>
                <span class="month"><asp:Label ID="lblMonth" runat="server"></asp:Label></span>
            </div>
    <div id="contents">
            <h5><asp:Label ID="lblUsername" runat="server" Text="Welcome "></asp:Label></h5>
            </div>
        <asp:Table runat="server" HorizontalAlign="Center" Width="100%">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
            </asp:UpdatePanel>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="ButtonSearchJobcode" runat="server" Text="..." CssClass="btn btn-warning" OnClick="ButtonSearchJobcode_Click"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        
        
    <div class="well ">
    
    <asp:Panel ID="PanelApproveENIT" runat="server">
    <legend>To Be Approved - ENIT</legend>
    <asp:GridView ID="GridViewApproveENIT" runat="server" AutoGenerateColumns="False" OnRowCommand="GridViewApproveENIT_RowCommand" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewApproveENIT_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" Width="100%" ForeColor="Black">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Bold="False" Font-Names="Trebuchet MS" ForeColor="Black" HorizontalAlign="Center" Width="100%"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requester" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request" HeaderStyle-CssClass="header-center">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action"  HeaderStyle-CssClass="header-center">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemTemplate>
                
                    <asp:LinkButton ID="LinkButtonApproveENITView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>'>View</asp:LinkButton>
                
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />
    </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="PanelApproveENITManager" runat="server">
    <legend>To Be Approved - ENIT Manager</legend>
    <asp:GridView ID="GridViewApproveENITManager" runat="server" AutoGenerateColumns="False" OnRowCommand="GridViewApproveENITManager_RowCommand" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewApproveENITManager_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" Width="100%" ForeColor="Black" EnableViewState="false">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Names="Trebuchet MS" ForeColor="Black" Width="100%"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requester">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request" >
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                
                    <asp:LinkButton ID="LinkButtonApproveENITManagerView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>'>View</asp:LinkButton>
                
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />

<RowStyle HorizontalAlign="Center" Width="100%"></RowStyle>
    </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="PanelApproveGroupManager" runat="server">
    <legend>To Be Approved - Group Manager</legend>
    <asp:GridView ID="GridViewApproveGroupManager" runat="server" AutoGenerateColumns="False" OnRowCommand="GridViewApproveGroupManager_RowCommand" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewApproveGroupManager_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" Width="100%" ForeColor="Black">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Names="Trebuchet MS" ForeColor="Black" Width="100%"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requester">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request" >
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                
                    <asp:LinkButton ID="LinkButtonApproveGroupManagerView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>'>View</asp:LinkButton>
                
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />

<RowStyle HorizontalAlign="Center" Width="100%"></RowStyle>
    </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="PanelApproveDeptManager" runat="server">
        <legend>To Be Approved - Department Manager</legend>
    <asp:GridView ID="GridViewApproveDeptManager" runat="server" AutoGenerateColumns="False" OnRowCommand="GridViewApproveDeptManager_RowCommand" RowStyle-HorizontalAlign="Center" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewApproveDeptManager_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" Width="100%" ForeColor="Black">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Names="Trebuchet MS" ForeColor="Black" Width="100%"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>

                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requester">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("SECT_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request" >
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                
                    <asp:LinkButton ID="LinkButtonApproveDeptManagerView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") %>'>View</asp:LinkButton>
                
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" Font-Overline="False" HorizontalAlign="Right" />

<RowStyle HorizontalAlign="Center" Width="100%"></RowStyle>
    </asp:GridView>
    </asp:Panel>
    
    </div>
    </div>
    </div>
    <div id="dialogApprove">
               <asp:Panel ID="PanelApprove" runat="server" Visible="false">
                <asp:Table ID="Table1" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewApprove" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentApprove" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment for Approve : </h5>
                            <asp:TextBox ID="TextBoxCommentApprove" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Approve?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="btn btn-primary" Text="Approve" ID="ButtonApproveOK" CommandArgument="" OnClick="ButtonApproveOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Reject" ID="ButtonApproveReject" CommandArgument="" OnClick="ButtonApproveReject_Click"/>
                            <asp:Button runat="server" class="btn btn-warning" Text="Cancel" ID="ButtonApproveCancel" OnClick="ButtonApproveCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
    </div>
    <div id="dialogServer">
            <asp:Panel ID="PanelServer" runat="server" Visible="false">
                <asp:Table ID="Table2" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>License Server / Cost Must Be Added</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewAddedServer" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Add Server?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="btn btn-primary" Text="Go To List Software Server" ID="ButtonAddServer" CommandArgument="" OnClick="ButtonAddServer_Click"/>
                            <asp:Button runat="server" class="btn btn-warning" Text="Go To List Software Cost" ID="ButtonAddCost" CommandArgument="" OnClick="ButtonAddCost_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddCancel" OnClick="ButtonAddCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
    </div>
    <div id="dialogProject">
        <asp:Panel ID="PanelProject" runat="server" Visible="false">
            <asp:Table runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass=""></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" CssClass="table-hover" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
    </div>
    
</asp:Content>
