﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="CADcost_record.aspx.cs" Inherits="CADcost.CADcost_record" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
        <legend>
            <asp:Table runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell>
                        <h4>Project :&nbsp</h4>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:UpdatePanel runat="server" ID="updatePanelDropdown" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:DropDownList ID="DropDownListProject" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListProject_SelectedIndexChanged" Width="100%" CssClass="form-control"></asp:DropDownList>
                        </ContentTemplate>
                            </asp:UpdatePanel>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonSearchJobcode" runat="server" Text="..." CssClass="btn btn-warning" OnClick="ButtonSearchJobcode_Click"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </legend>
    <div class="panel panel-primary">
    <div class="panel-heading"><h4>Request History</h4></div>
        <div class="panel-body">
    <asp:GridView ID="GridViewLicenseRequest" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" OnRowCommand="GridViewLicenseRequest_RowCommand" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewLicenseRequest_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" CellPadding="10" Width="100%">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Names="Trebuchet MS" ForeColor="Black" HorizontalAlign="Center"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Request Number">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Request">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("DATE_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Time Request">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("NAME_ACTIVITY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonRequestHistoryView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ")%>'>View</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryEdit" runat="server" CommandName="editrequest" CommandArgument='<%# Eval("ID_CADREQ")%>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonRequestHistoryCancel" runat="server" CommandName="cancelrequest" CommandArgument='<%# Eval("ID_CADREQ")%>'>Cancel</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BorderStyle="Solid" HorizontalAlign="Right"/>
        <RowStyle HorizontalAlign="Center" Font-Names="Trebuchet MS" />
    </asp:GridView>
            </div>
    </div>
    <div class="panel panel-danger">
    <div class="panel-heading"><h4>Action History</h4></div>
    <div class="panel-body">
    <asp:GridView ID="GridViewActionHistory" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" OnRowCommand="GridViewActionHistory_RowCommand" AllowPaging="True" EmptyDataText="No Records Yet" OnPageIndexChanging="GridViewActionHistory_PageIndexChanging" PageSize="3" ShowHeaderWhenEmpty="True" CellPadding="10" Width="100%">
        <HeaderStyle BackColor="#999999" Font-Size="Larger" Font-Names="Trebuchet MS" ForeColor="Black" HorizontalAlign="Center"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req Number">
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CADREQ_NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req By">
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("CADREQ_BY") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dept">
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("DEPT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CAD Req Time">
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("TIME_REQUEST") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("NAME_ACTION") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action Time">
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("TIME_END") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonActionHistoryView" runat="server" CommandName="view" CommandArgument='<%# Eval("ID_CADREQ") +","+ Eval("COMMENT")%>'>View</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle HorizontalAlign="Right"/>

<RowStyle HorizontalAlign="Center" Font-Names="Trebuchet MS"></RowStyle>
    </asp:GridView>
    </div>
    </div>
    </div>

    <div id="dialogView">
        <asp:Panel ID="PanelView" runat="server" Visible="false">
                <asp:Table ID="Table2" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewView" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                        
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Total Price : </h5><asp:Label ID="LabelTotalPrice" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentView" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center"><asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonViewCancel" OnClick="ButtonViewCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
            </asp:Panel>
    </div>

    <div id="dialogCancel">
        <asp:Panel ID="PanelCancel" runat="server" Visible="false">
                <asp:Table ID="Table3" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Software License Request Detail</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:GridView ID="GridViewCancel" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-BackColor="Silver" HeaderStyle-BackColor="#666666">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Software Name">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" Text='<%# Eval("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRemark" runat="server" Text='<%# Eval("REMARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment : </h5><asp:Label ID="LabelCommentCancel" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h5>Comment for Cancel Request : </h5>
                            <asp:TextBox ID="TextBoxCommentCancel" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <h4>Do You Want to Cancel Request?</h4>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Button runat="server" class="btn btn-primary" Text="Cancel Request" ID="ButtonCancelOK" OnClick="ButtonCancelOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonCancelCancel" OnClick="ButtonCancelCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
    </div>

    <div id="dialogProject">
        <asp:Panel ID="PanelProject" runat="server" Visible="false">
            <asp:Table ID="Table1" runat="server" Width="100%">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell ColumnSpan="2">
                        <h3>Search Project</h3>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search By:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="DropDownListSearchBy" runat="server" CssClass="form-control"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Search:
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass=""></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <div>
                        <asp:UpdatePanel ID="UpdatePanelProject" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                                    <asp:GridView ID="GridViewProjectSearch" runat="server" AutoGenerateColumns="False" CssClass="table-hover" AutoGenerateSelectButton="True" EnablePersistedSelection="True" OnSelectedIndexChanged="GridViewProjectSearch_SelectedIndexChanged" DataKeyNames="JOBCODE_ID" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Jobcode ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelJobcodeID" runat="server" Text='<%# Eval("JOBCODE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelProjectName" runat="server" Text='<%# Eval("PROJECT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#3399FF" />
                                    </asp:GridView>
                                </ContentTemplate>
                        </asp:UpdatePanel>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
    </div>
</asp:Content>
