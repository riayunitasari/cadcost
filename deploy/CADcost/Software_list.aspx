﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CAD.Master" AutoEventWireup="true" CodeBehind="Software_list.aspx.cs" Inherits="CADcost.Software_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well ">
    <legend>Software List</legend>
    <asp:Button ID="ButtonAddSoftware" runat="server" Text="Add Software" OnClick="ButtonAddSoftware_Click" CssClass="btn btn-primary"/>
    <asp:GridView ID="GridViewSoftwareList" runat="server"  AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewSoftwareList_RowEditing" OnRowDeleting="GridViewSoftwareList_RowDeleting" OnRowUpdating="GridViewSoftwareList_RowUpdating" OnRowCancelingEdit="GridViewSoftwareList_RowCancelingEdit" OnRowDataBound="GridViewSoftwareList_RowDataBound" Width="50%" HorizontalAlign="Center">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Software Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftware" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:Label ID="LabelSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDSoftwareEdit" runat="server"  Value='<%# Eval("ID_SOFTWARE") %>'/>
                    <asp:TextBox ID="TextBoxSoftwareName" runat="server" Text='<%# Eval("NAME_SOFTWARE") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Renewal">
                <ItemTemplate>
                    <asp:Label ID="LabelDateRenewal" runat="server" Text='<%# Eval("DATE_RENEWAL") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxDateRenewal" runat="server" Text='<%# Eval("DATE_RENEWAL") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vendor Name">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendor" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:Label ID="LabelVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendorEdit" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:DropDownList ID="DropDownListVendorName" runat="server" ></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateSoftware" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelSoftware" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditSoftware" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteSoftware" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#999999" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
<RowStyle HorizontalAlign="Center"></RowStyle>

    </asp:GridView>

    <legend>Vendor List</legend>
    <asp:Button ID="ButtonAddVendor" runat="server" Text="Add Vendor" OnClick="ButtonAddVendor_Click" CssClass="btn btn-primary"/>
    <asp:GridView ID="GridViewVendor" runat="server"  AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" EmptyDataText="No Records Yet" ShowHeaderWhenEmpty="True" OnRowEditing="GridViewVendor_RowEditing" OnRowDeleting="GridViewVendor_RowDeleting" OnRowUpdating="GridViewVendor_RowUpdating" OnRowCancelingEdit="GridViewVendor_RowCancelingEdit" Width="75%" HorizontalAlign="Center">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name Vendor">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendor" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:Label ID="LabelVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:Label>

                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="HiddenFieldIDVendorEdit" runat="server"  Value='<%# Eval("ID_VENDOR") %>'/>
                    <asp:TextBox ID="TextBoxVendorName" runat="server" Text='<%# Eval("NAME_VENDOR") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact">
                <ItemTemplate>
                    <asp:Label ID="LabelContact" runat="server" Text='<%# Eval("CONTACT") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxContact" runat="server" Text='<%# Eval("CONTACT") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <asp:Label ID="LabelEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Website">
                <ItemTemplate>
                    <asp:Label ID="LabelWebsite" runat="server" Text='<%# Eval("WEBSITE") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxWebsite" runat="server" Text='<%# Eval("WEBSITE") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vendor Description">
                <ItemTemplate>
                    <asp:Label ID="LabelVendorDescription" runat="server" Text='<%# Eval("VENDOR_DESCRIPTION") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBoxVendorDescription" runat="server" Text='<%# Eval("VENDOR_DESCRIPTION") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButtonUpdateVendor" runat="server" CommandName="Update">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonCancelVendor" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButtonEditVendor" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonDeleteVendor" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BackColor="#999999" Font-Size="Larger"></HeaderStyle>
        <AlternatingRowStyle BackColor="Silver"></AlternatingRowStyle>
        <RowStyle HorizontalAlign="Center"></RowStyle>
    </asp:GridView>
    
    </div>

    <div id="dialogAddSoftware">
        <asp:Panel ID="PanelAddSoftware" runat="server" Visible="false">
                <asp:Table ID="Table2" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Add Software</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Software Name :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxSoftwareName" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Software Vendor :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="DropDownListVendor" runat="server"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Date Renewal :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxDateRenewal" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="ButtonAddSoftwareOK" OnClick="ButtonAddSoftwareOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddSoftwareCancel" OnClick="ButtonAddSoftwareCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
            </asp:Panel>
    </div>
    
    <div id="dialogAddVendor">
        <asp:Panel ID="PanelAddVendor" runat="server" Visible="false">
                <asp:Table ID="Table1" runat="server" BackColor="White">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <h3>Add Vendor</h3>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Vendor Name :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxVendorName" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Contact :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxContact" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Email :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Website :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxWebsite" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Vendor Description :
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="TextBoxDescription" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Button runat="server" class="btn btn-primary" Text="Add" ID="ButtonAddVendorOK" OnClick="ButtonAddVendorOK_Click"/>
                            <asp:Button runat="server" class="btn btn-danger" Text="Cancel" ID="ButtonAddVendorCancel" OnClick="ButtonAddVendorCancel_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
            
                </asp:Table>
            </asp:Panel>
    </div>
</asp:Content>
