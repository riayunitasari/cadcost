/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     11/19/2015 4:02:02 PM                        */
/*==============================================================*/


use CADcost
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ACTION_HISTORY') and o.name = 'FK_ACTION_H_RELATIONS_ACTIVITY')
alter table ACTION_HISTORY
   drop constraint FK_ACTION_H_RELATIONS_ACTIVITY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ACTION_HISTORY') and o.name = 'FK_ACTION_H_RELATIONS_ACTION')
alter table ACTION_HISTORY
   drop constraint FK_ACTION_H_RELATIONS_ACTION
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ACTION_HISTORY') and o.name = 'FK_ACTION_H_RELATIONS_CADREQ')
alter table ACTION_HISTORY
   drop constraint FK_ACTION_H_RELATIONS_CADREQ
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CADDETAIL') and o.name = 'FK_CADDETAI_RELATIONS_SOFTWARE')
alter table CADDETAIL
   drop constraint FK_CADDETAI_RELATIONS_SOFTWARE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CADDETAIL') and o.name = 'FK_CADDETAI_RELATIONS_CADREQ')
alter table CADDETAIL
   drop constraint FK_CADDETAI_RELATIONS_CADREQ
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CADREQ') and o.name = 'FK_CADREQ_RELATIONS_ACTIVITY')
alter table CADREQ
   drop constraint FK_CADREQ_RELATIONS_ACTIVITY
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DOCUMENT_DETAIL') and o.name = 'FK_DOCUMENT_RELATIONS_CADREQ')
alter table DOCUMENT_DETAIL
   drop constraint FK_DOCUMENT_RELATIONS_CADREQ
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DOCUMENT_DETAIL') and o.name = 'FK_DOCUMENT_RELATIONS_DOCUMENT')
alter table DOCUMENT_DETAIL
   drop constraint FK_DOCUMENT_RELATIONS_DOCUMENT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DOCUMENT_DETAIL') and o.name = 'FK_DOCUMENT_RELATIONS_TYPE_DOC')
alter table DOCUMENT_DETAIL
   drop constraint FK_DOCUMENT_RELATIONS_TYPE_DOC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SOFTWARE_VENDOR') and o.name = 'FK_SOFTWARE_SOFTWARE__SOFTWARE')
alter table SOFTWARE_VENDOR
   drop constraint FK_SOFTWARE_SOFTWARE__SOFTWARE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SOFTWARE_VENDOR') and o.name = 'FK_SOFTWARE_SOFTWARE__VENDOR')
alter table SOFTWARE_VENDOR
   drop constraint FK_SOFTWARE_SOFTWARE__VENDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ACTION')
            and   type = 'U')
   drop table ACTION
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ACTION_HISTORY')
            and   name  = 'RELATIONSHIP_7_FK'
            and   indid > 0
            and   indid < 255)
   drop index ACTION_HISTORY.RELATIONSHIP_7_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ACTION_HISTORY')
            and   name  = 'RELATIONSHIP_6_FK'
            and   indid > 0
            and   indid < 255)
   drop index ACTION_HISTORY.RELATIONSHIP_6_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ACTION_HISTORY')
            and   name  = 'RELATIONSHIP_5_FK'
            and   indid > 0
            and   indid < 255)
   drop index ACTION_HISTORY.RELATIONSHIP_5_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ACTION_HISTORY')
            and   type = 'U')
   drop table ACTION_HISTORY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ACTIVITY')
            and   type = 'U')
   drop table ACTIVITY
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CADDETAIL')
            and   name  = 'RELATIONSHIP_3_FK'
            and   indid > 0
            and   indid < 255)
   drop index CADDETAIL.RELATIONSHIP_3_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CADDETAIL')
            and   name  = 'RELATIONSHIP_2_FK'
            and   indid > 0
            and   indid < 255)
   drop index CADDETAIL.RELATIONSHIP_2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CADDETAIL')
            and   type = 'U')
   drop table CADDETAIL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CADREQ')
            and   name  = 'RELATIONSHIP_4_FK'
            and   indid > 0
            and   indid < 255)
   drop index CADREQ.RELATIONSHIP_4_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CADREQ')
            and   type = 'U')
   drop table CADREQ
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DOCUMENT')
            and   type = 'U')
   drop table DOCUMENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DOCUMENT_DETAIL')
            and   name  = 'RELATIONSHIP_9_FK'
            and   indid > 0
            and   indid < 255)
   drop index DOCUMENT_DETAIL.RELATIONSHIP_9_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DOCUMENT_DETAIL')
            and   name  = 'RELATIONSHIP_10_FK'
            and   indid > 0
            and   indid < 255)
   drop index DOCUMENT_DETAIL.RELATIONSHIP_10_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('DOCUMENT_DETAIL')
            and   name  = 'RELATIONSHIP_8_FK'
            and   indid > 0
            and   indid < 255)
   drop index DOCUMENT_DETAIL.RELATIONSHIP_8_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DOCUMENT_DETAIL')
            and   type = 'U')
   drop table DOCUMENT_DETAIL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SOFTWARE')
            and   type = 'U')
   drop table SOFTWARE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SOFTWARE_VENDOR')
            and   name  = 'SOFTWARE_VENDOR2_FK'
            and   indid > 0
            and   indid < 255)
   drop index SOFTWARE_VENDOR.SOFTWARE_VENDOR2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SOFTWARE_VENDOR')
            and   name  = 'SOFTWARE_VENDOR_FK'
            and   indid > 0
            and   indid < 255)
   drop index SOFTWARE_VENDOR.SOFTWARE_VENDOR_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SOFTWARE_VENDOR')
            and   type = 'U')
   drop table SOFTWARE_VENDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TYPE_DOCUMENT')
            and   type = 'U')
   drop table TYPE_DOCUMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VENDOR')
            and   type = 'U')
   drop table VENDOR
go

/*==============================================================*/
/* Table: ACTION                                                */
/*==============================================================*/
create table ACTION (
   ID_ACTION            int                  not null,
   NAME_ACTION          varchar(100)         null,
   constraint PK_ACTION primary key nonclustered (ID_ACTION)
)
go

/*==============================================================*/
/* Table: ACTION_HISTORY                                        */
/*==============================================================*/
create table ACTION_HISTORY (
   ID_HISTORY           int                  not null,
   ID_ACTION            int                  null,
   ID_ACTIVITY          int                  null,
   ID_CADREQ            int                  null,
   PARTICIPANT          int                  null,
   PARTICIPANT_ONBEHALF int                  null,
   COMPLETED_BY         int                  null,
   TIME_START           datetime             null,
   TIME_END             datetime             null,
   COMMENT              varchar(8000)        null,
   constraint PK_ACTION_HISTORY primary key nonclustered (ID_HISTORY)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_5_FK on ACTION_HISTORY (
ID_ACTIVITY ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_6_FK on ACTION_HISTORY (
ID_ACTION ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_7_FK on ACTION_HISTORY (
ID_CADREQ ASC
)
go

/*==============================================================*/
/* Table: ACTIVITY                                              */
/*==============================================================*/
create table ACTIVITY (
   ID_ACTIVITY          int                  not null,
   NAME_ACTIVITY        varchar(100)         null,
   constraint PK_ACTIVITY primary key nonclustered (ID_ACTIVITY)
)
go

/*==============================================================*/
/* Table: CADDETAIL                                             */
/*==============================================================*/
create table CADDETAIL (
   ID_SOFTWARE          int                  not null,
   ID_CADREQ            int                  not null,
   QUANTITY             int                  null,
   constraint PK_CADDETAIL primary key (ID_SOFTWARE, ID_CADREQ)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on CADDETAIL (
ID_SOFTWARE ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_3_FK on CADDETAIL (
ID_CADREQ ASC
)
go

/*==============================================================*/
/* Table: CADREQ                                                */
/*==============================================================*/
create table CADREQ (
   ID_CADREQ            int                  not null,
   ID_ACTIVITY          int                  null,
   CADREQ_NO            varchar(100)         null,
   JOBCODE              varchar(100)         null,
   DESCRIPTION          varchar(8000)        null,
   REMARK               varchar(8000)        null,
   CADREQ_BY            int                  null,
   PIC                  int                  null,
   LASTACTIVITY_BY      int                  null,
   TIME_REQUEST         datetime             null,
   TIME_LASTACTIVITY    datetime             null,
   TOTAL_AMOUNT         decimal              null,
   ID_REQ               int                  null,
   constraint PK_CADREQ primary key nonclustered (ID_CADREQ)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_4_FK on CADREQ (
ID_ACTIVITY ASC
)
go

/*==============================================================*/
/* Table: DOCUMENT                                              */
/*==============================================================*/
create table DOCUMENT (
   ID_DOCUMENT          int                  not null,
   NAME_DOCUMENT        varchar(100)         null,
   NO_DOCUMENT          varchar(100)         null,
   TIME_CREATED         datetime             null,
   PATH                 varchar(8000)        null,
   DOCUMENT             tinyint              null,
   constraint PK_DOCUMENT primary key nonclustered (ID_DOCUMENT)
)
go

/*==============================================================*/
/* Table: DOCUMENT_DETAIL                                       */
/*==============================================================*/
create table DOCUMENT_DETAIL (
   ID_DOCUMENT          int                  not null,
   ID_CADREQ            int                  not null,
   ID_TYPE              int                  null,
   constraint PK_DOCUMENT_DETAIL primary key (ID_DOCUMENT, ID_CADREQ)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_8_FK on DOCUMENT_DETAIL (
ID_DOCUMENT ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_10_FK on DOCUMENT_DETAIL (
ID_CADREQ ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_9_FK on DOCUMENT_DETAIL (
ID_TYPE ASC
)
go

/*==============================================================*/
/* Table: SOFTWARE                                              */
/*==============================================================*/
create table SOFTWARE (
   ID_SOFTWARE          int                  not null,
   ID_SOFI              int                  null,
   NAME_SOFTWARE        varchar(100)         null,
   PRICE_CAD            decimal              null,
   PRICE_REQ            decimal              null,
   DATE_RENEWAL         datetime             null,
   IP_SERVER            varchar(50)          null,
   NAME_SERVER          varchar(100)         null,
   constraint PK_SOFTWARE primary key nonclustered (ID_SOFTWARE)
)
go

/*==============================================================*/
/* Table: SOFTWARE_VENDOR                                       */
/*==============================================================*/
create table SOFTWARE_VENDOR (
   ID_SOFTWARE          int                  not null,
   ID_VENDOR            int                  not null,
   constraint PK_SOFTWARE_VENDOR primary key (ID_SOFTWARE, ID_VENDOR)
)
go

/*==============================================================*/
/* Index: SOFTWARE_VENDOR_FK                                    */
/*==============================================================*/
create index SOFTWARE_VENDOR_FK on SOFTWARE_VENDOR (
ID_SOFTWARE ASC
)
go

/*==============================================================*/
/* Index: SOFTWARE_VENDOR2_FK                                   */
/*==============================================================*/
create index SOFTWARE_VENDOR2_FK on SOFTWARE_VENDOR (
ID_VENDOR ASC
)
go

/*==============================================================*/
/* Table: TYPE_DOCUMENT                                         */
/*==============================================================*/
create table TYPE_DOCUMENT (
   ID_TYPE              int                  not null,
   NAME_TYPE            varchar(100)         null,
   constraint PK_TYPE_DOCUMENT primary key nonclustered (ID_TYPE)
)
go

/*==============================================================*/
/* Table: VENDOR                                                */
/*==============================================================*/
create table VENDOR (
   ID_VENDOR            int                  not null,
   NAME_VENDOR          varchar(100)         null,
   CONTACT              varchar(100)         null,
   EMAIL                varchar(100)         null,
   WEBSITE              varchar(100)         null,
   VENDOR_DESCRIPTION   varchar(8000)       null,
   constraint PK_VENDOR primary key nonclustered (ID_VENDOR)
)
go

alter table ACTION_HISTORY
   add constraint FK_ACTION_H_RELATIONS_ACTIVITY foreign key (ID_ACTIVITY)
      references ACTIVITY (ID_ACTIVITY)
go

alter table ACTION_HISTORY
   add constraint FK_ACTION_H_RELATIONS_ACTION foreign key (ID_ACTION)
      references ACTION (ID_ACTION)
go

alter table ACTION_HISTORY
   add constraint FK_ACTION_H_RELATIONS_CADREQ foreign key (ID_CADREQ)
      references CADREQ (ID_CADREQ)
go

alter table CADDETAIL
   add constraint FK_CADDETAI_RELATIONS_SOFTWARE foreign key (ID_SOFTWARE)
      references SOFTWARE (ID_SOFTWARE)
go

alter table CADDETAIL
   add constraint FK_CADDETAI_RELATIONS_CADREQ foreign key (ID_CADREQ)
      references CADREQ (ID_CADREQ)
go

alter table CADREQ
   add constraint FK_CADREQ_RELATIONS_ACTIVITY foreign key (ID_ACTIVITY)
      references ACTIVITY (ID_ACTIVITY)
go

alter table DOCUMENT_DETAIL
   add constraint FK_DOCUMENT_RELATIONS_CADREQ foreign key (ID_CADREQ)
      references CADREQ (ID_CADREQ)
go

alter table DOCUMENT_DETAIL
   add constraint FK_DOCUMENT_RELATIONS_DOCUMENT foreign key (ID_DOCUMENT)
      references DOCUMENT (ID_DOCUMENT)
go

alter table DOCUMENT_DETAIL
   add constraint FK_DOCUMENT_RELATIONS_TYPE_DOC foreign key (ID_TYPE)
      references TYPE_DOCUMENT (ID_TYPE)
go

alter table SOFTWARE_VENDOR
   add constraint FK_SOFTWARE_SOFTWARE__SOFTWARE foreign key (ID_SOFTWARE)
      references SOFTWARE (ID_SOFTWARE)
go

alter table SOFTWARE_VENDOR
   add constraint FK_SOFTWARE_SOFTWARE__VENDOR foreign key (ID_VENDOR)
      references VENDOR (ID_VENDOR)
go

